using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
    [CorpseName("corps d'une slime corrosive")]
	public class CorrosiveSlime : Slime
	{
		[Constructable]
		public CorrosiveSlime() : base()
		{
			Name = "une slime corrosive";
		}
		
		public CorrosiveSlime( Serial serial ) : base( serial )
		{
		}	
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
}
