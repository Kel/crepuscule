using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
    [CorpseName("dryade enrag�e")]
	public class InsaneDryad : DryadA
	{
		public override bool InitialInnocent{ get{ return false; } }

		[Constructable]
		public InsaneDryad() : base()
		{
			Name = "dryade enrag�e";	
			Hue = 2127;
		}
		
		public InsaneDryad( Serial serial ) : base( serial )
		{
		}
		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
}
