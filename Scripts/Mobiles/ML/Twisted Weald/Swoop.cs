using System;
using Server.Items;
using Server.Mobiles;

namespace Server.Mobiles
{
	[CorpseName( "corps d'un aigle swoop" )]
	public class Swoop : BaseCreature
	{		
		[Constructable]
		public Swoop() : base( AIType.AI_Animal, FightMode.Agressor, 10, 1, 0.05, 0.2 )
		{
			Name = "aigle swoop";
			Body = 0x5;
			Hue = 0xE0;

			SetStr( 113, 135 );
			SetDex( 421, 477 );
			SetInt( 69, 86 );

			SetHits( 516, 571 );
			SetStam( 421, 477 );
			SetMana( 11, 14 );

			SetDamage( 15, 25 );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, 60, 74 );
			SetResistance( ResistanceType.Fire, 30, 35 );
			SetResistance( ResistanceType.Cold, 40, 44 );
			SetResistance( ResistanceType.Poison, 25, 29 );
			SetResistance( ResistanceType.Energy, 25, 30 );

			SetSkill( SkillName.Wrestling, 84.4, 94.3 );
			SetSkill( SkillName.Tactics, 98.0, 113.7 );
			SetSkill( SkillName.MagicResist, 95.4, 102.9 );

		}
		
		public Swoop( Serial serial ) : base( serial )
		{
		}
		
		public override int GetIdleSound() { return 0x2EF; }
		public override int GetAttackSound() { return 0x2EE; }
		public override int GetAngerSound() { return 0x2EF; }
		public override int GetHurtSound() { return 0x2F1; }
		public override int GetDeathSound()	{ return 0x2F2; }
		
		
		public override int Feathers{ get{ return 72; } }
		public override int Meat{ get{ return 1; } }
		
		public override void GenerateLoot()
		{
			//AddLoot( LootPack.AosUltraRich, 3 );
		}		
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
}
