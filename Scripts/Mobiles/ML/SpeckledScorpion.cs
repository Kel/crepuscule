using System;
using System.Collections;
using Server.Items;
using Server.Targeting;

namespace Server.Mobiles
{
    [CorpseName("corps d'un scorpion enrag�")]
	public class SpeckledScorpion : Scorpion
	{
		[Constructable]
		public SpeckledScorpion() : base()
		{
			Name = "scorpion enrag�";
		}

		public SpeckledScorpion( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
}