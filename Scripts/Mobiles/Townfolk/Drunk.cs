
// Script Name: The Village Drunk  ( This is my first script )
// Created By: Sheepondrugs  
// Created On: 14 February 2008
// Big Thanks to: RUNON Forum    
//				
// Feel Free to Modify the script


using System; 
using Server.Items; 
using Server; 
using Server.Misc; 


namespace Server.Mobiles 
{ 
	public class Drunk : BaseCreature 
	{ 

        


		[Constructable] 
		public Drunk () : base( AIType.AI_Animal, FightMode.None, 10, 1, 0.2, 0.4 ) 
		{
            m_SayingsEnabled = true;
            SayingsSecondsDelay = 25;
            m_Saying1 = "ohh, j'ai trop bu.. ";
            m_Saying2 = "plus d'hydromel!..";
            m_Saying3 = "donne hydromel, donne...!";
            m_Saying4 = "ma femme va me tuer...";
            m_Saying5 = "ma t�te..";

			SpeechHue = Utility.RandomDyedHue(); // speach colour
	

			// NPC Details
			Title = "L'alcoolique";
			this.Name = NameList.RandomName( "male" );
			this.Body = 0x190; 
			InitStats( 30, 40, 50 ); 
	    	
			Hue = Utility.RandomSkinHue();    // Random Skin on NPC

			// NPC Items
			AddItem( new LongPants( Utility.RandomNeutralHue() ) ); 
			AddItem( new FancyShirt( Utility.RandomDyedHue() ) );
	    	AddItem( new Boots( Utility.RandomNeutralHue() ) );

			// NPC Bag
			Container pack = new Backpack(); 
			pack.DropItem( new Gold( 250, 300 ) ); 
			pack.Movable = false; 
			AddItem( pack ); 
		} 


		public override bool ClickTitle{ get{ return false; } }

		public Drunk( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 
			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 
			int version = reader.ReadInt(); 
		} 






	} 
} 
