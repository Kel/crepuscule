using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	public class SolenHelper
	{
		public static void PackPicnicBasket( BaseCreature solen )
		{
			if ( 1 > Utility.Random( 100 ) )
			{
				PicnicBasket basket = new PicnicBasket();

				basket.DropItem( new BeverageBottle( BeverageType.Wine ) );
				basket.DropItem( new CheeseWedge() );

				solen.PackItem( basket );
			}
		}
	}
}