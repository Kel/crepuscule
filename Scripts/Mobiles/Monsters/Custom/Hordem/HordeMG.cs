using System;
using System.Collections;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Horde Minion g�ant" )]
	public class HordeMG : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.WhirlwindAttack;
		}
		[Constructable]
		public HordeMG() : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.6, 1.2 )
		{
			Name = "Horde Minion g�ant";
			Body = 796;
			NameHue = 18;

			SetStr( 720, 900 );
			SetDex( 110, 150 );
			SetInt( 520, 585 );

			SetHits( 750, 1120 );
			SetMana( 12, 24 );

			SetDamage( 9, 18 );

			SetDamageType( ResistanceType.Physical, 90 );
			SetDamageType( ResistanceType.Energy, 10 );

			SetResistance( ResistanceType.Physical, 45, 90 );
			SetResistance( ResistanceType.Fire, 50, 125 );
			SetResistance( ResistanceType.Cold, 50, 70 );
			SetResistance( ResistanceType.Poison, 50, 350 );
			SetResistance( ResistanceType.Energy, 100, 250 );

			SetSkill( SkillName.MagicResist, 120.0, 125.0 );
			SetSkill( SkillName.Tactics, 120.0, 125.0 );
			SetSkill( SkillName.Wrestling, 80.0, 85.0 );
			

			Fame = 8000;
			Karma = -8000;

			VirtualArmor = 29;

		}

		public override void GenerateLoot()
		{
			//AddLoot( LootPack.Average );
		}

		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lethal; } }
		public override bool AutoDispel{ get{ return true; } }

		public HordeMG( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}

		public void SpawnHordeME( Mobile m )
		{
			Map map = this.Map;

			if ( map == null )
				return;

			HordeME spawned = new HordeME();

			spawned.Team = this.Team;

			bool validLocation = false;
			Point3D loc = this.Location;

			for ( int j = 0; !validLocation && j < 10; ++j )
			{
				int x = X + Utility.Random( 3 ) - 1;
				int y = Y + Utility.Random( 3 ) - 1;
				int z = map.GetAverageZ( x, y );

				if ( validLocation = map.CanFit( x, y, this.Z, 16, false, false ) )
					loc = new Point3D( x, y, Z );
				else if ( validLocation = map.CanFit( x, y, z, 16, false, false ) )
					loc = new Point3D( x, y, z );
			}

			spawned.MoveToWorld( loc, map );
			spawned.Combatant = m;
		}

		public void EatHordeMEs()
		{
			ArrayList toEat = new ArrayList();
  
			foreach ( Mobile m in this.GetMobilesInRange( 2 ) )
			{
				if ( m is HordeME )
					toEat.Add( m );
			}

			if ( toEat.Count > 0 )
			{
				PlaySound( Utility.Random( 0x3B, 2 ) ); // Eat sound

				foreach ( Mobile m in toEat )
				{
					Hits += (m.Hits / 2);
					m.Delete();
				}
			}
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			if ( this.Hits > (this.HitsMax / 4) )
			{
				if ( 0.25 >= Utility.RandomDouble() )
					SpawnHordeME( attacker );
			}
			else if ( 0.25 >= Utility.RandomDouble() )
			{
				EatHordeMEs();
			}
		}
	}
}