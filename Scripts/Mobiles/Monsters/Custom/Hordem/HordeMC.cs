using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Horde Minion Color�" )]
	public class HordeMC : BaseCreature
	{
public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.ArmorIgnore;
}
		[Constructable]
		public HordeMC() : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4 )
		{
			Name = "Un Horde Minion color�";
			Body = 795;
			NameHue = 18;
			BaseSoundID = 422;
			if ( 1.0 > Utility.RandomDouble() ) 
            		Hue = Utility.RandomList( 2068, 1954, 1938, 1923, 2054, 3, 38, 53, 73, 388 ); 

			SetStr( 145, 210 );
			SetDex( 145, 225 );
			SetInt( 210, 450 );

			SetHits( 200, 375 );

			SetDamage( 9, 23 );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, 20, 25 );
			SetResistance( ResistanceType.Fire, 10, 20 );
			SetResistance( ResistanceType.Cold, 15, 25 );
			SetResistance( ResistanceType.Poison, 150, 250 );
			SetResistance( ResistanceType.Energy, 150, 250 );

			SetSkill( SkillName.MagicResist, 88.1, 100.0 );
			SetSkill( SkillName.Tactics, 90.0, 92.0 );
			SetSkill( SkillName.Wrestling, 85.0, 91.0 );

			Fame = 450;
			Karma = -450;

			VirtualArmor = 32;

		}

		public override void GenerateLoot()
		{
			//AddLoot( LootPack.Meager );
		}

		public override int Hides{ get{ return 0; } }
		public override int Meat{ get{ return 0; } }

		public HordeMC( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}