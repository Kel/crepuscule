using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Un Exadar" )]
	public class Exadar : BaseCreature
	{
		[Constructable]
		public Exadar() : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4 )
		{
			Name = "Un Exadar";
			Body = 757;
			BaseSoundID = 422;
			NameHue = 18;

			SetStr( 120, 180 );
			SetDex( 120, 185 );
			SetInt( 21, 45 );

			SetHits( 325, 345 );

			SetDamage( 10, 26 );

			SetDamageType( ResistanceType.Energy, 100 );

			SetResistance( ResistanceType.Physical, 20, 25 );
			SetResistance( ResistanceType.Fire, 10, 20 );
			SetResistance( ResistanceType.Cold, 15, 25 );
			SetResistance( ResistanceType.Poison, 150, 250 );
			SetResistance( ResistanceType.Energy, 150, 250 );

			SetSkill( SkillName.MagicResist, 88.1, 100.0 );
			SetSkill( SkillName.Tactics, 90.0, 92.0 );
			SetSkill( SkillName.Wrestling, 85.0, 91.0 );

			Fame = 450;
			Karma = -450;

			VirtualArmor = 32;

		}

		public override void GenerateLoot()
		{
			//AddLoot( LootPack.Meager );
            PackMagicJewel();
		}

		public override int Hides{ get{ return 0; } }
		public override int Meat{ get{ return 0; } }

		public Exadar( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}