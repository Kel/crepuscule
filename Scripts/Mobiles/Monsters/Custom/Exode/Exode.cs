using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Une Exode" )]
	public class Exode : BaseCreature
	{
		[Constructable]
		public Exode() : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4 )
		{
			Name = "Une Exode";
			Body = 756;
			BaseSoundID = 422;
			NameHue = 18;

			SetStr( 96, 120 );
			SetDex( 91, 115 );
			SetInt( 21, 45 );

			SetHits( 72, 125 );

			SetDamage( 8, 18 );

			SetDamageType( ResistanceType.Energy, 100 );

			SetResistance( ResistanceType.Physical, 20, 25 );
			SetResistance( ResistanceType.Fire, 10, 20 );
			SetResistance( ResistanceType.Cold, 15, 25 );
			SetResistance( ResistanceType.Poison, 150, 250 );
			SetResistance( ResistanceType.Energy, 150, 250 );

			SetSkill( SkillName.MagicResist, 75.1, 100.0 );
			SetSkill( SkillName.Tactics, 70.0, 88.0 );
			SetSkill( SkillName.Wrestling, 75.0, 85.0 );

			Fame = 450;
			Karma = -450;

			VirtualArmor = 28;

		}

		public override void GenerateLoot()
		{
			//AddLoot( LootPack.Meager );
		}

		public override int Hides{ get{ return 0; } }
		public override int Meat{ get{ return 0; } }

		public Exode( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}