
using System;
using System.Collections;
using Server;
using Server.Items;
using Server.ContextMenus;
using Server.Misc;
using Server.Network;

namespace Server.Mobiles
{
    [CorpseName("Un Zhack")]
	public class Zak : BaseCreature
	{
		private static bool m_Talked;
		string[] ZakSay = new string[]
		{
	   "Un jour je vais gouverner le monde!",
            "Ah ah! Biscuit!",
            "Je suis meilleur que toi, sale concombre!",
		};

		public override bool ShowFameTitle{ get{ return false; } }

        [Constructable]
        public Zak()
            : base(AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4)
        {
            SpeechHue = Utility.RandomDyedHue();
            Name = "Perroquet Zhack";
			Body = 5;
			BaseSoundID = 229;
			Hue = 1909;

			SetStr( 250, 275 );
			SetDex( 120, 135 );
			SetInt( 60, 160 );

			SetHits( 266, 325 );

			SetDamage( 1, 35 );

			SetDamageType( ResistanceType.Physical, 20 );
			SetDamageType( ResistanceType.Fire, 80 );

			SetResistance( ResistanceType.Physical, 75, 135 );
			SetResistance( ResistanceType.Fire, 30, 40 );
			SetResistance( ResistanceType.Poison, 10, 20 );
			SetResistance( ResistanceType.Energy, 10, 20 );


			SetSkill( SkillName.MagicResist, 120.0, 125.0 );
			SetSkill( SkillName.Tactics, 120.0, 125.0 );
			SetSkill( SkillName.Wrestling, 80.0, 85.0 );
			SetSkill( SkillName.Magery, 100.0, 180.0 );
			SetSkill( SkillName.EvalInt, 90.2, 95.2 );

			Fame = 3400;
			Karma = -3400;

			VirtualArmor = 30;

			Tamable = false;
			ControlSlots = 1;
			MinTameSkill = 85.5;

			PackItem( new SulfurousAsh( 5 ) );
			PackItem( new PlumeZak() );
            


        }
		public override void OnMovement( Mobile m, Point3D oldLocation )
		{
			if( m_Talked == false )
			{
				if ( m.InRange( this, 3 ) && m is PlayerMobile)
				{
					m_Talked = true;
					SayRandom( ZakSay, this );
					this.Move( GetDirectionTo( m.Location ) );
					SpamTimer t = new SpamTimer();
					t.Start();
				}
			}
		}

		private class SpamTimer : Timer
		{
			public SpamTimer() : base( TimeSpan.FromSeconds( 2 ) )
			{
				Priority = TimerPriority.OneSecond;
			}

			protected override void OnTick()
			{
				m_Talked = false;
			}
		}

		private static void SayRandom( string[] say, Mobile m )
		{
			m.Say( say[Utility.Random( say.Length )] );
		}

        public Zak(Serial serial): base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
