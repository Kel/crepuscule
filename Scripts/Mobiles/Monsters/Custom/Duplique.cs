using System;
using Server;
using Server.Items;
using Server.Engines.CannedEvil;

namespace Server.Mobiles
{
	public class Duplique : BaseChampion
	{
public override bool AlwaysMurderer{ get{ return true; } }

		private Mobile m_Duplique1;
		private bool m_SpawnedDuplique1;
	public override ChampionSkullType SkullType{ get{ return ChampionSkullType.Enlightenment; } }

		[Constructable]
		public Duplique() : base( AIType.AI_Melee, FightMode.Closest )
		{
			Body = 970;
			Name = "Duplicata";
			Hue = 1937;
			NameHue = 18;

			

			SetStr( 403, 650 );
			SetDex( 50, 75 );
			SetInt( 503, 800 );

			SetHits( 750 );
			SetStam( 202, 400 );

			SetDamage( 10, 25 );

			SetDamageType( ResistanceType.Physical, 75 );
			SetDamageType( ResistanceType.Fire, 25 );

			SetResistance( ResistanceType.Physical, 85, 90 );
			SetResistance( ResistanceType.Fire, 60, 70 );
			SetResistance( ResistanceType.Cold, 60, 70 );
			SetResistance( ResistanceType.Poison, 80, 90 );
			SetResistance( ResistanceType.Energy, 80, 90 );

			SetSkill( SkillName.Anatomy, 75.1, 100.0 );
			SetSkill( SkillName.EvalInt, 120.1, 130.0 );
			SetSkill( SkillName.Magery, 120.0 );
			SetSkill( SkillName.Meditation, 120.1, 130.0 );
			SetSkill( SkillName.MagicResist, 100.5, 150.0 );
			SetSkill( SkillName.Tactics, 100.0 );
			SetSkill( SkillName.Wrestling, 100.0 );

			Fame = 200;
			Karma = -200;

			VirtualArmor = 100;
		}

		public override void GenerateLoot()
		{
			//AddLoot( LootPack.Rich, 3 );
		}

		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Deadly; } }
			public override bool AutoDispel{ get{ return true; } }

		public void SpawnPixies( Mobile target )
		{
			Map map = this.Map;

			if ( map == null )
				return;

			this.Say( "Vous ne m'aurez pas tant que j'ai mon Duplique! m'houhaha" ); // You shall never defeat me as long as I have my Duplique1!

			int newPixies = Utility.RandomMinMax( 0, 0 );

			for ( int i = 0; i < newPixies; ++i )
			{
				Pixie pixie = new Pixie();

				pixie.Team = this.Team;
				pixie.FightMode = FightMode.Closest;

				bool validLocation = false;
				Point3D loc = this.Location;

				for ( int j = 0; !validLocation && j < 10; ++j )
				{
					int x = X + Utility.Random( 3 ) - 1;
					int y = Y + Utility.Random( 3 ) - 1;
					int z = map.GetAverageZ( x, y );

					if ( validLocation = map.CanFit( x, y, this.Z, 16, false, false ) )
						loc = new Point3D( x, y, Z );
					else if ( validLocation = map.CanFit( x, y, z, 16, false, false ) )
						loc = new Point3D( x, y, z );
				}

				pixie.MoveToWorld( loc, map );
				pixie.Combatant = target;
			}
		}

		public override int GetAngerSound()
		{
			return 0x2F8;
		}

		public override int GetIdleSound()
		{
			return 0x2F8;
		}

		public override int GetAttackSound()
		{
			return Utility.Random( 0x2F5, 2 );
		}

		public override int GetHurtSound()
		{
			return 0x2F9;
		}

		public override int GetDeathSound()
		{
			return 0x2F7;
		}

		public void CheckDuplique1()
		{
			if ( !m_SpawnedDuplique1 )
			{
				this.Say( "*Se d�double*" ); // Come forth my Duplique1!

				m_Duplique1 = new Duplique1();

				((BaseCreature)m_Duplique1).Team = this.Team;

				m_Duplique1.MoveToWorld( this.Location, this.Map );

				m_SpawnedDuplique1 = true;
			}
			else if ( m_Duplique1 != null && m_Duplique1.Deleted )
			{
				m_Duplique1 = null;
			}
		}

		public override void AlterDamageScalarFrom( Mobile caster, ref double scalar )
		{
			CheckDuplique1();

			if ( m_Duplique1 != null )
			{
				scalar *= 0.1;

				if ( 0.1 >= Utility.RandomDouble() )
					SpawnPixies( caster );
			}
		}

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			defender.Damage( Utility.Random( 20, 10 ), this );
			defender.Stam -= Utility.Random( 20, 10 );
			defender.Mana -= Utility.Random( 20, 10 );
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			CheckDuplique1();

			if ( m_Duplique1 != null && 0.1 >= Utility.RandomDouble() )
				SpawnPixies( attacker );

			attacker.Damage( Utility.Random( 20, 10 ), this );
			attacker.Stam -= Utility.Random( 20, 10 );
			attacker.Mana -= Utility.Random( 20, 10 );
		}

		public Duplique( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version

			writer.Write( m_Duplique1 );
			writer.Write( m_SpawnedDuplique1 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
				case 0:
				{
					m_Duplique1 = reader.ReadMobile();
					m_SpawnedDuplique1 = reader.ReadBool();

					break;
				}
			}
		}
	}
}