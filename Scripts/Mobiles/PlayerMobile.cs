using System;
using System.Collections;
using Server;
using Server.Misc;
using Server.Items;
using Server.Gumps;
using Server.Multis;
using Server.Engines.Help;
using Server.ContextMenus;
using Server.Network;
using Server.Spells;
using Server.Spells.Fifth;
using Server.Spells.Seventh;
using Server.Targeting;
using Server.BountySystem;
using Server.Guilds;
using Server.Engines;
using Server.SkillHandlers;
using System.Collections.Generic;
using Server.Engines.Quests;
//using Server.Factions;


namespace Server.Mobiles
{
    #region Enumerations
    [Flags]
    public enum PlayerFlag // First 16 bits are reserved for default-distro use, start custom flags at 0x00010000
    {
        None = 0x00000000,
        Glassblowing = 0x00000001,
        Masonry = 0x00000002,
        SandMining = 0x00000004,
        StoneMining = 0x00000008,
        ToggleMiningStone = 0x00000010,
        KarmaLocked = 0x00000020,
        AutoRenewInsurance = 0x00000040,
        UseOwnFilter = 0x00000080,
        PublicMyRunUO = 0x00000100,
        PagingSquelched = 0x00000200,
        NecroGolem = 0x00001000,
        Sick = 0x00010000,
        Sickness = 0x00020000,
        SickContagious = 0x00040000,
        AccessGuild = 0x00060000,
        IsInGuild = 0x00070000
    }
    public enum SickFlag
    {
        None,
        TempImmunity,
        PermImmunity,
        Cold,
        Flu,
        Headache,
        Virus,
        VampTurn
    }
    public enum AccessGuildFlag
    {
        None = 0,
        Premier = 1,
        Deuxieme = 2,
        Troisieme = 3,
        Quatrieme = 4,
        Cinquieme = 5,
        Sixieme = 6,
        Septieme = 7,
        Huitieme = 8,
        Neuvieme = 9,
        GuildMaster = 10,
        GuildMastersS = 11


    }
    public enum VampType
    {
        None,
        Vampire,
        Master,
        Ancient
    }

    public enum NpcGuild
    {
        None,
        MagesGuild,
        WarriorsGuild,
        ThievesGuild,
        RangersGuild,
        HealersGuild,
        MinersGuild,
        MerchantsGuild,
        TinkersGuild,
        TailorsGuild,
        FishermensGuild,
        BardsGuild,
        BlacksmithsGuild
    }
    #endregion

    public class PlayerMobile : Mobile
    {
        #region Fields
        private bool m_LastProtectedMessage;
        private int m_NextProtectionCheck = 10;
        protected string m_SecondIdentity = "";
        protected short m_UseIdentity = 0;

        private bool m_Timers_Started;
        private bool m_ShowBountyUpdate;
        private ArrayList m_BountyUpdateList = new ArrayList();

        private DesignContext m_DesignContext;
        private NpcGuild m_NpcGuild;
        private DateTime m_NpcGuildJoinTime;
        private TimeSpan m_NpcGuildGameTime;
        private int m_StepsTaken;
        private int m_Profession;
        private short m_LCP;
        private short m_LCS;
        private short m_LCB;
        private string m_Sexe;
        private string m_Apparence;
        private short m_Taille;
        private short m_Poids;
        protected short m_Age;
        private short m_Chance;
        private int m_LastGlobalLight = -1;
        private int m_LastPersonalLight = -1;
        private string m_Resumer_BG;
        private string m_Titre_Nouveu;
        protected double m_TicksXP;
        private short m_Soif;
        private short m_Incognito;
        private short m_Fatigue;
        private bool m_Telepathy;
        private int m_TelNum = 0;
        private int m_PermNum = 0;
        private int m_TempNum = 0;
        public VampType m_VampType;
        private bool m_Vampirism;
        private bool m_VampBurn;
        private bool m_VampHunger;
        private bool m_FenetreOuverte;
        public int m_Blood;
        private AccessGuildFlag m_AccessGuild;
        private PlayerFlag m_Flags;
        private SickFlag flag;
        public SickTimer m_SickTimer;
        public ImmunTimer m_ImmunTimer;
        public ArrayList TelepathieVictim = new ArrayList();
        public ArrayList MindFollowTemporaryVictim = new ArrayList();
        public ArrayList MindFollowTemporaryVictimDates = new ArrayList();
        public ArrayList MindFollowPermanentVictim = new ArrayList();
        public ArrayList MindFollowPermanentVictimDates = new ArrayList();
        private ArrayList m_PermaFlags;
        private ArrayList m_VisList;
        private Hashtable m_AntiMacroTable;
        private TimeSpan m_GameTime;
        private TimeSpan m_ShortTermElapse;
        private TimeSpan m_LongTermElapse;
        private DateTime m_SessionStart;
        private DateTime m_LastEscortTime;
        private DateTime m_SavagePaintExpiration;
        private SkillName m_Learning = (SkillName)(-1);
        private ArrayList liste_nom;
        private ArrayList liste_nom2;
        private Mobile m_InsuranceAward;
        private int m_InsuranceCost;
        private int m_InsuranceBonus;
        private bool m_NoDeltaRecursion;

        private static string[] m_AccessGuildNames = new string[]
			{
				"Aucun",
				"Premier Niveau de la Hierachie",
				"Deuxieme Niveau de la Hierachie",
				"Troisieme Niveau de la Hierachie",
				"Quatrieme Niveau de la Hierachie",
				"Cinquieme Niveau de la Hierachie",
				"Sixieme Niveau de la Hierachie",
				"Huitieme Niveau de la Hierachie",
				"Neuvieme Niveau de la Hierachie",
                "Diexeme Niveau de la Hierachie",
                "Niveau Supreme de la Hierachie"
			};

        public GuildInfo GuildInfo;
        public SalaryInfo SalaryInfo;
        public ClassInfo ClassInfo;
        public EvolutionInfo EvolutionInfo;
        #endregion

        #region Vampires

        public bool GetGFlag(PlayerFlag m_AccessGuild)
        {
            return ((m_Flags & m_AccessGuild) != 0);
        }

        public void SetGFlag(PlayerFlag m_AccessGuild, bool value)
        {
            if (value)
                m_Flags |= m_AccessGuild;
            else
                m_Flags &= ~m_AccessGuild;
        }



        public bool GetDFlag(PlayerFlag flag)
        {
            return ((m_Flags & flag) != 0);
        }

        public void SetDFlag(PlayerFlag flag, bool value)
        {
            if (value)
                m_Flags |= flag;
            else
                m_Flags &= ~flag;
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public VampType VampireType
        {
            get { return m_VampType; }
            set { m_VampType = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Blood
        {
            get { return m_Blood; }
            set { m_Blood = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Vampirism
        {
            get { return m_Vampirism; }
            set { m_Vampirism = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool VampHunger
        {
            get { return m_VampHunger; }
            set { m_VampHunger = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool VampBurn
        {
            get { return m_VampBurn; }
            set { m_VampBurn = value; }
        }




        public class VampBurnTimer : Timer
        {
            public const double SecondsPerUOMinute = 5.0;
            public const double MinutesPerUODay = SecondsPerUOMinute * 24;
            private static DateTime WorldStart = new DateTime(1997, 9, 1);

            public static void Initialize()
            {
                // m_ServerStart = DateTime.Now;
                new VampBurnTimer().Start();
            }

            public VampBurnTimer()
                : base(TimeSpan.FromMinutes(.10), TimeSpan.FromMinutes(.10))
            {
                Priority = TimerPriority.OneSecond;
            }
            protected override void OnTick()
            {
                CheckBurn(); //Every tick run function burn
            }
            private bool GetBurning(int XPos)
            {
                TimeSpan timeSpan = DateTime.Now - WorldStart;
                int totalMinutes = (int)(timeSpan.TotalSeconds / SecondsPerUOMinute);
                totalMinutes += Map.Felucca.MapIndex * 320;
                totalMinutes += XPos / 16;
                int currentHour = (totalMinutes / 60) % 24;
                int currentMinute = totalMinutes % 60;

                if (currentHour <= 18 && currentHour >= 6)
                    //if (( currentHour >= 18 && currentMinute >= 01 ) ||(currentHour <= 5 && currentMinute <= 59))
                    return true;
                //if ( currentHour <= 7 && ( currentHour >= 18 /*&& currentMinute <= 59 */))
                // return false;
                return false;
            }
            private void CheckBurn()
            {
                ArrayList mobs = new ArrayList(World.Mobiles.Values);
                for (int i = 0; i < mobs.Count; i++)
                {
                    Mobile m = mobs[i] as Mobile;

                    if (m is PlayerMobile && m.Alive && m.Map != Map.Internal)
                    {
                        PlayerMobile mplayer = m as PlayerMobile;
                        if (mplayer.Vampirism == true && mplayer.VampBurn == true)
                        {
                            BaseHouse house = BaseHouse.FindHouseAt(m);
                            //StaticHouseSign staticHouse = StaticHouseSign.StaticFindHouseAt( m );
                            if (house == null /*&& staticHouse == null && Validate(m) == true*/)
                            {
                                if (GetBurning(m.X))
                                {
                                    m.Stam -= 1; //tick away some stamina
                                    //m.FixedParticles( 0x3735, 1, 30, 9503, EffectLayer.Waist );
                                    //Effects.SendLocationEffect(m.Location, m.Map,0x371C, 20); //0x36BD,0x398C
                                    //m.PrivateOverheadMessage(0, 0, false, "Vous souffrez � cause du soleil", m.NetState );
                                    //m.PlaySound( m.Female ? 814 : 1088 );
                                    //m.Say( "*ahhhh!*" );
                                    //m.Damage( Utility.Random( 40, 75 ) );
                                    m.Damage(Utility.Random(1, 3));
                                }
                                else
                                    //m.PrivateOverheadMessage(0, 0, false, "Vous n'etez pas un vampire", m.NetState );
                                    return;
                            }
                        }
                    }
                }
            }

            public bool Validate(Mobile from)
            {
                if (from.Region.Name != "Wind" &&   //towns
                    from.Region.Name != "Covetous" && from.Region.Name != "Deceit" &&//dungeons
                    from.Region.Name != "Despise" && from.Region.Name != "Destard" &&//dungeons
                    from.Region.Name != "Hythloth" && from.Region.Name != "Shame" &&//dungeons
                    from.Region.Name != "Wrong" && from.Region.Name != "Terathan Keep" &&//dungeons
                    from.Region.Name != "Fire" && from.Region.Name != "Ice" &&//dungeons
                    from.Region.Name != "Rock Dungeon" && from.Region.Name != "Spider Cave" &&//dungeons
                    from.Region.Name != "Spectre Dungeon" && from.Region.Name != "Blood Dungeon" &&//dungeons
                    from.Region.Name != "Wisp Dungeon" && from.Region.Name != "Ankh Dungeon" &&//dungeons
                    from.Region.Name != "Exodus Dungeon" && from.Region.Name != "Sorcerer's Dungeon" &&//dungeons
                    from.Region.Name != "Ancient Lair" && from.Region.Name != "Doom" &&//dungeons
                    from.Region.Name != "Jail")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region Sicknesses

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Sick
        {
            get { return GetDFlag(PlayerFlag.Sick); }
            set
            {
                if (this.Sickness != SickFlag.TempImmunity && this.Sickness != SickFlag.PermImmunity && this.Sickness != SickFlag.None && this.Sickness != SickFlag.VampTurn && !value)
                    this.SendMessage("Vous vous sentez mieux!");

                if (this.Sickness != SickFlag.None) //&& this.Sickness != SickFlag.VampTurn
                    SetDFlag(PlayerFlag.Sick, value);
                else
                {
                    SetDFlag(PlayerFlag.Sick, false);
                    this.SickContagious = false;
                }
                if (!value)
                {
                    this.Sickness = SickFlag.None;
                }
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public bool SickContagious
        {
            get { return GetDFlag(PlayerFlag.SickContagious); }
            set
            {
                if (!this.Sick || this.Sickness == SickFlag.TempImmunity || this.Sickness == SickFlag.PermImmunity || this.Sickness == SickFlag.VampTurn)
                    SetDFlag(PlayerFlag.SickContagious, false);
                else
                    SetDFlag(PlayerFlag.SickContagious, value);
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public SickFlag Sickness
        {
            get { return flag; }
            set
            {
                flag = value;
                ChangeSickness(this, flag);
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public AccessGuildFlag AccessGuild
        {
            get { return m_AccessGuild; }
            set
            {
                m_AccessGuild = value;
                //  ChangeInGuild(this, m_AccessGuild);
            }
        }
        public class ImmunTimer : Timer
        {
            private Mobile m_defender;
            private PlayerMobile m_pmdefender;
            private int cnt = 0;
            public ImmunTimer(Mobile defender)
                : base(TimeSpan.Zero, TimeSpan.FromSeconds(0.6), 480)
            {
                m_defender = defender;
                m_pmdefender = (PlayerMobile)defender;
            }
            protected override void OnTick()
            {
                cnt += 1;
                if (!m_pmdefender.Sick && m_pmdefender.Sickness == SickFlag.VampTurn)
                    Stop();
                if (m_pmdefender.Sickness == SickFlag.TempImmunity && m_pmdefender.Sickness != SickFlag.VampTurn && cnt == 480)//&& m_pmdefender.Vampirism == false
                    m_pmdefender.Sick = false;
            }
        }
        public class SickTimer : Timer
        {
            private Mobile m_defender;
            private int cnt = 0;
            private int m_count = 0;
            private int m_countmax;
            private PlayerMobile m_pmdefender;
            private SickFlag m_flag;
            public SickTimer(Mobile defender, int count, SickFlag flag)
                : base(TimeSpan.Zero, TimeSpan.FromSeconds(20), count)
            {
                m_defender = defender;
                m_countmax = count;
                m_pmdefender = (PlayerMobile)defender;
                m_flag = flag;
            }
            protected override void OnTick()
            {
                if (!m_defender.Alive && m_pmdefender.Sickness != SickFlag.VampTurn)
                {
                    m_pmdefender.Sick = false;
                    m_pmdefender.SickContagious = false;
                    Stop();
                }

                if (((!m_pmdefender.Sick) || (m_pmdefender.Sickness == SickFlag.None) || (m_pmdefender.Sickness == SickFlag.TempImmunity) || (m_pmdefender.Sickness == SickFlag.PermImmunity)) && (m_pmdefender.Sickness != SickFlag.VampTurn))
                    Stop();

                if (m_flag == SickFlag.Cold)
                {
                    cnt += 1;
                    m_count += 1;
                    m_defender.Damage(Utility.Random(5, 10));
                    //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 3 );
                    if (cnt > 2)
                    {
                        m_defender.SendMessage("Vous avez la tiberculose!");
                        m_defender.Emote("toux");

                        if (!m_defender.Female)
                            m_defender.PlaySound(0x420);
                        else
                            m_defender.PlaySound(0x311);

                        cnt = 0;
                    }
                }
                if (m_flag == SickFlag.Flu)
                {
                    cnt += 1;
                    m_count += 1;
                    m_defender.Damage(Utility.Random(10, 15));
                    //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 5 );
                    if (cnt > 2)
                    {
                        m_defender.SendMessage("Vous avez la variole!");
                        m_defender.Emote("*toux*");
                        if (!m_defender.Female)
                            m_defender.PlaySound(0x42B);
                        else
                            m_defender.PlaySound(0x31B);
                        cnt = 0;
                    }
                }
                if (m_flag == SickFlag.Headache)
                {
                    cnt += 1;
                    m_count += 1;
                    m_defender.Damage(Utility.Random(1, 5));
                    //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 2 );
                    if (cnt > 2)
                    {
                        m_defender.SendMessage("Vous avez une migrene!");
                        m_defender.Emote("*toux*");

                        if (!m_defender.Female)
                            m_defender.PlaySound(0x42B);
                        else
                            m_defender.PlaySound(0x31B);

                        cnt = 0;
                    }
                }
                if (m_flag == SickFlag.Virus)
                {
                    cnt += 1;
                    m_count += 1;
                    m_defender.Damage(Utility.Random(15, 20));
                    //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 4 );
                    if (cnt > 2)
                    {
                        m_defender.SendMessage("Vous avez la Fi�vre Jaune!");
                        m_defender.Emote("*toux*");

                        if (!m_defender.Female)
                            m_defender.PlaySound(0x43F);
                        else
                            m_defender.PlaySound(0x32D);

                        cnt = 0;
                    }
                }
                if (m_flag == SickFlag.VampTurn)
                {
                    cnt += 1;
                    m_count += 12;

                    if (cnt == 2)
                    {
                        m_defender.SendMessage("Vous sentez que votre peau change!");
                        m_defender.Emote("*toux*");
                        m_defender.Damage(Utility.Random(1, 1));
                        m_defender.PlaySound(m_defender.Female ? 813 : 1087);
                        m_defender.Say("*crache le sang*");
                        if (!m_defender.Mounted)
                        {
                            m_defender.Animate(32, 5, 1, true, false, 0);
                        }
                        Point3D p = new Point3D(m_defender.Location);
                        switch (m_defender.Direction)
                        {
                            case Direction.North:
                            p.Y--; break;
                            case Direction.South:
                            p.Y++; break;
                            case Direction.East:
                            p.X++; break;
                            case Direction.West:
                            p.X--; break;
                            case Direction.Right:
                            p.X++; p.Y--; break;
                            case Direction.Down:
                            p.X++; p.Y++; break;
                            case Direction.Left:
                            p.X--; p.Y++; break;
                            case Direction.Up:
                            p.X--; p.Y--; break;
                            default:
                            break;
                        }
                        p.Z = m_defender.Map.GetAverageZ(p.X, p.Y);

                        Puke puke = new Puke();
                        puke.Map = m_defender.Map;
                        puke.Location = p;
                        //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 1 );
                    }
                    if (cnt == 4)
                    {
                        m_defender.SendMessage("Vous sentez que votre corps seche!");
                        m_defender.Emote("*toux*");
                        m_defender.Damage(Utility.Random(1, 2));
                        m_defender.PlaySound(m_defender.Female ? 813 : 1087);
                        m_defender.Say("*crache le sang*");
                        if (!m_defender.Mounted)
                        {
                            m_defender.Animate(32, 5, 1, true, false, 0);
                        }
                        Point3D p = new Point3D(m_defender.Location);
                        switch (m_defender.Direction)
                        {
                            case Direction.North:
                            p.Y--; break;
                            case Direction.South:
                            p.Y++; break;
                            case Direction.East:
                            p.X++; break;
                            case Direction.West:
                            p.X--; break;
                            case Direction.Right:
                            p.X++; p.Y--; break;
                            case Direction.Down:
                            p.X++; p.Y++; break;
                            case Direction.Left:
                            p.X--; p.Y++; break;
                            case Direction.Up:
                            p.X--; p.Y--; break;
                            default:
                            break;
                        }
                        p.Z = m_defender.Map.GetAverageZ(p.X, p.Y);

                        Puke puke = new Puke();
                        puke.Map = m_defender.Map;
                        puke.Location = p;
                        //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 2 );
                    }
                    if (cnt == 6)
                    {
                        m_defender.SendMessage("Vous vous sentez mourrant!");
                        m_defender.Emote("*agonise*");
                        m_defender.Damage(Utility.Random(1, 3));
                        m_defender.PlaySound(m_defender.Female ? 813 : 1087);
                        m_defender.Say("*crache le sang*");
                        if (!m_defender.Mounted)
                        {
                            m_defender.Animate(32, 5, 1, true, false, 0);
                        }
                        Point3D p = new Point3D(m_defender.Location);
                        switch (m_defender.Direction)
                        {
                            case Direction.North:
                            p.Y--; break;
                            case Direction.South:
                            p.Y++; break;
                            case Direction.East:
                            p.X++; break;
                            case Direction.West:
                            p.X--; break;
                            case Direction.Right:
                            p.X++; p.Y--; break;
                            case Direction.Down:
                            p.X++; p.Y++; break;
                            case Direction.Left:
                            p.X--; p.Y++; break;
                            case Direction.Up:
                            p.X--; p.Y--; break;
                            default:
                            break;
                        }
                        p.Z = m_defender.Map.GetAverageZ(p.X, p.Y);

                        Puke puke = new Puke();
                        puke.Map = m_defender.Map;
                        puke.Location = p;
                        //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 3 );
                    }
                    if (cnt == 8)
                    {
                        m_defender.SendMessage("Vous vous sentez mourrant......");
                        m_defender.Emote("*agonise*");
                        m_defender.Damage(Utility.Random(1, 4));
                        m_defender.PlaySound(m_defender.Female ? 813 : 1087);
                        m_defender.Say("*crache le sang*");
                        if (!m_defender.Mounted)
                        {
                            m_defender.Animate(32, 5, 1, true, false, 0);
                        }
                        Point3D p = new Point3D(m_defender.Location);
                        switch (m_defender.Direction)
                        {
                            case Direction.North:
                            p.Y--; break;
                            case Direction.South:
                            p.Y++; break;
                            case Direction.East:
                            p.X++; break;
                            case Direction.West:
                            p.X--; break;
                            case Direction.Right:
                            p.X++; p.Y--; break;
                            case Direction.Down:
                            p.X++; p.Y++; break;
                            case Direction.Left:
                            p.X--; p.Y++; break;
                            case Direction.Up:
                            p.X--; p.Y--; break;
                            default:
                            break;
                        }
                        p.Z = m_defender.Map.GetAverageZ(p.X, p.Y);

                        Puke puke = new Puke();
                        puke.Map = m_defender.Map;
                        puke.Location = p;
                        //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 4 );
                    }
                    if (cnt == 10)
                    {
                        m_defender.SendMessage("Vous pouvez difficilement vous controler!");
                        m_defender.Emote("*toux*");
                        m_defender.Damage(Utility.Random(1, 5));
                        m_defender.PlaySound(m_defender.Female ? 813 : 1087);
                        m_defender.Say("*crache le sang*");
                        if (!m_defender.Mounted)
                        {
                            m_defender.Animate(32, 5, 1, true, false, 0);
                        }
                        Point3D p = new Point3D(m_defender.Location);
                        switch (m_defender.Direction)
                        {
                            case Direction.North:
                            p.Y--; break;
                            case Direction.South:
                            p.Y++; break;
                            case Direction.East:
                            p.X++; break;
                            case Direction.West:
                            p.X--; break;
                            case Direction.Right:
                            p.X++; p.Y--; break;
                            case Direction.Down:
                            p.X++; p.Y++; break;
                            case Direction.Left:
                            p.X--; p.Y++; break;
                            case Direction.Up:
                            p.X--; p.Y--; break;
                            default:
                            break;
                        }
                        p.Z = m_defender.Map.GetAverageZ(p.X, p.Y);

                        Puke puke = new Puke();
                        puke.Map = m_defender.Map;
                        puke.Location = p;
                        //break;
                        //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 5 );
                    }
                    if (cnt == 12)
                    {
                        m_defender.SendMessage("Vous agonisez!");
                        m_defender.PlaySound(m_defender.Female ? 814 : 1088);
                        m_defender.Say("*ahhhh!*");
                        m_defender.PublicOverheadMessage(MessageType.Spell, m_defender.SpeechHue, true, "*Vous perdez votre humanit�, pour atteindre l'immortalit�*", false);
                        m_defender.Damage(Utility.Random(150, 150));
                        //m_defender.Hits = m_defender.Hits - Utility.Random( 1, 150 );
                        m_pmdefender.Vampirism = true;
                        m_pmdefender.VampHunger = true;
                        m_pmdefender.VampBurn = true;
                        //m_pmdefender.Title = "The Vampire";
                        //m_pmdefender.Hue = 1154;
                        m_pmdefender.VampireType = VampType.Vampire;
                        m_pmdefender.Sickness = SickFlag.PermImmunity;
                        m_pmdefender.Resurrect();
                        //m_defender.AddToBackpack( new VampireOrb() );
                        m_defender.Hunger = 3;
                        m_defender.Kills = 5;
                        m_defender.SendMessage("Vous �tez un vampire maintenant, et vous avez besoin du sang pour survivre");
                        /////////////////////////////////////////////////////////////////////////////////////////
                        ///////THIS IS FOR RESETTING SKILLS AND CAP ON TURN//////////////////////////////////////
                        /////////////////////////////////////////////////////////////////////////////////////////
                        //Server.Skills skills = m_defender.Skills;
                        //
                        //for ( int i = 0; i < skills.Length; ++i )
                        //{
                        //skills[i].Base = 0.0;
                        //skills[i].Cap = 100.0;
                        //}
                        ///////////////////////////////////////////////////////////////////////////////////////
                    }
                }

                if (m_count == m_countmax)//&& m_pmdefender.Vampirism != true
                {
                    m_pmdefender.Sickness = SickFlag.TempImmunity;
                    return;
                }
                int randominfect = Utility.Random(1, 99);

                ArrayList li = new ArrayList();
                foreach (Mobile o in m_defender.Map.GetMobilesInRange(m_defender.Location, 5))
                {
                    li.Add(o);
                }
                for (int i = 0; i < li.Count; i++)
                {
                    Mobile o = li[i] as Mobile;

                    if (o is PlayerMobile && m_defender != o)
                    {
                        PlayerMobile mob = (PlayerMobile)o;

                        if ((mob.Sickness == SickFlag.TempImmunity || mob.Sickness == SickFlag.PermImmunity || !mob.SickContagious) || (mob.Vampirism == true))
                        {
                        }
                        else if (mob.Sickness == SickFlag.None)
                        {
                            if (randominfect > 43 && randominfect < 50)
                                mob.Sickness = m_flag;
                        }
                    }
                }
            }
        }

        public virtual void ChangeSickness(PlayerMobile m, SickFlag newsick)
        {
            int count = Utility.Random(5, 20);
            Mobile pm = (Mobile)m;

            switch (newsick)
            {
                case SickFlag.None:
                {
                    if (m.Sick)
                    {
                        m.Sick = false;
                        m.SickContagious = false;
                    }
                    break;
                }
                case SickFlag.TempImmunity:
                {
                    m.SendMessage("You feel better!");
                    m.Sick = true;
                    m.SickContagious = false;
                    m_ImmunTimer = new ImmunTimer(pm);
                    m_ImmunTimer.Start();
                    break;
                }
                case SickFlag.PermImmunity:
                {
                    m.SendMessage("Vous ne serez plus jamais malade!");
                    m.Sick = true;
                    m.SickContagious = false;
                    break;
                }
                case SickFlag.Cold:
                {
                    m.SendMessage("Vous avez attrap� froid!");
                    m.Sick = true;
                    m.SickContagious = true;
                    m_SickTimer = new SickTimer(pm, count, SickFlag.Cold);
                    m_SickTimer.Start();
                    break;
                }
                case SickFlag.Flu:
                {
                    m.SendMessage("Vous avez une grippe!");
                    m.Sick = true;
                    m.SickContagious = true;
                    m_SickTimer = new SickTimer(pm, count, SickFlag.Flu);
                    m_SickTimer.Start();
                    break;
                }
                case SickFlag.Headache:
                {
                    m.SendMessage("Vous avez mal � la t�te!");
                    m.Sick = true;
                    m.SickContagious = false;
                    m_SickTimer = new SickTimer(pm, count, SickFlag.Headache);
                    m_SickTimer.Start();
                    break;
                }
                case SickFlag.Virus:
                {
                    m.SendMessage("Vous avez un virus!");
                    m.Sick = true;
                    m.SickContagious = true;
                    m_SickTimer = new SickTimer(pm, count, SickFlag.Virus);
                    m_SickTimer.Start();
                    break;
                }
                case SickFlag.VampTurn:
                {
                    m.SendMessage("Vous �tiez infect� par un vampire!");
                    m.Sick = true;
                    m.SickContagious = false;
                    m_SickTimer = new SickTimer(pm, count, SickFlag.VampTurn);
                    m_SickTimer.Start();
                    break;
                }
            }
        }
        #endregion

        #region Properties
        public bool FenetreOuverte
        {
            get { return m_FenetreOuverte; }
            set { m_FenetreOuverte = value; }
        }

        public int Profession
        {
            get { return m_Profession; }
            set { m_Profession = value; }
        }

        public int StepsTaken
        {
            get { return m_StepsTaken; }
            set { m_StepsTaken = value; }
        }

        public bool ShowBountyUpdate
        {
            get { return m_ShowBountyUpdate; }
            set { m_ShowBountyUpdate = value; }
        }
        public ArrayList BountyUpdateList
        {
            get { return m_BountyUpdateList; }
            set { m_BountyUpdateList = value; }
        }

         [CommandProperty(AccessLevel.GameMaster)]
        public override int MaxWeight { get { return WeightOverloading.GetMaxWeight(this); }}

        [CommandProperty(AccessLevel.GameMaster)]
        public Capacities Capacities { get { return ClassInfo.Capacities; } set { } }

        [CommandProperty(AccessLevel.GameMaster)]
        public SalaryInfo SalaryProperty { get { return SalaryInfo; } set { } }

        [CommandProperty(AccessLevel.GameMaster)]
        public GuildInfo GuildProperty { get { return GuildInfo; } set { } }

        [CommandProperty(AccessLevel.GameMaster)]
        public ClassInfo ClassProperty { get { return ClassInfo; } set { } }

        [CommandProperty(AccessLevel.GameMaster)]
        public EvolutionInfo EvolutionProperty { get { return EvolutionInfo; } set { } }

        [CommandProperty(AccessLevel.Administrator)]
        public short UseIdentity
        {
            get { return m_UseIdentity; }
            set
            { m_UseIdentity = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string SecondIdentity
        {
            get { return m_SecondIdentity; }
            set
            { m_SecondIdentity = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool Timers_Started
        {
            get { return m_Timers_Started; }
            set { m_Timers_Started = value; }
        }

        public bool IsInGuild
        {
            get { return GetGFlag(PlayerFlag.IsInGuild); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Telepathy
        {
            get { return m_Telepathy; }
            set { m_Telepathy = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string Titre_Nouveu
        {
            get { return m_Titre_Nouveu; }
            set
            {
                m_Titre_Nouveu = value;
                InvalidateProperties();
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string Resumer_BGnce
        {
            get { return m_Resumer_BG; }
            set { m_Resumer_BG = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string Apparence
        {
            get { return m_Apparence; }
            set { m_Apparence = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string Sexe
        {
            get { return m_Sexe; }
            set { m_Sexe = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Fatigue
        {
            get { return m_Fatigue; }
            set { m_Fatigue = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Incognito
        {
            get { return m_Incognito; }
            set { m_Incognito = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Soif
        {
            get { return m_Soif; }
            set { m_Soif = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public NpcGuild NpcGuild
        {
            get { return m_NpcGuild; }
            set { m_NpcGuild = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public DateTime NpcGuildJoinTime
        {
            get { return m_NpcGuildJoinTime; }
            set { m_NpcGuildJoinTime = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan NpcGuildGameTime
        {
            get { return m_NpcGuildGameTime; }
            set { m_NpcGuildGameTime = value; }
        }

        public PlayerFlag Flags
        {
            get { return m_Flags; }
            set { m_Flags = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool PagingSquelched
        {
            get { return GetFlag(PlayerFlag.PagingSquelched); }
            set { SetFlag(PlayerFlag.PagingSquelched, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Glassblowing
        {
            get { return GetFlag(PlayerFlag.Glassblowing); }
            set { SetFlag(PlayerFlag.Glassblowing, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Masonry
        {
            get { return GetFlag(PlayerFlag.Masonry); }
            set { SetFlag(PlayerFlag.Masonry, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool SandMining
        {
            get { return GetFlag(PlayerFlag.SandMining); }
            set { SetFlag(PlayerFlag.SandMining, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool StoneMining
        {
            get { return GetFlag(PlayerFlag.StoneMining); }
            set { SetFlag(PlayerFlag.StoneMining, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ToggleMiningStone
        {
            get { return GetFlag(PlayerFlag.ToggleMiningStone); }
            set { SetFlag(PlayerFlag.ToggleMiningStone, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool KarmaLocked
        {
            get { return GetFlag(PlayerFlag.KarmaLocked); }
            set { SetFlag(PlayerFlag.KarmaLocked, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool AutoRenewInsurance
        {
            get { return GetFlag(PlayerFlag.AutoRenewInsurance); }
            set { SetFlag(PlayerFlag.AutoRenewInsurance, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool UseOwnFilter
        {
            get { return GetFlag(PlayerFlag.UseOwnFilter); }
            set { SetFlag(PlayerFlag.UseOwnFilter, value); }
        }

        public bool PublicMyRunUO
        {
            get { return GetFlag(PlayerFlag.PublicMyRunUO); }
            set { SetFlag(PlayerFlag.PublicMyRunUO, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool NecroGolem
        {
            get { return GetFlag(PlayerFlag.NecroGolem); }
            set { SetFlag(PlayerFlag.NecroGolem, value); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan GameTime
        {
            get
            {
                if (NetState != null)
                    return m_GameTime + (DateTime.Now - m_SessionStart);

                else
                    return m_GameTime;
            }
        }

        #region NameLists
        public ArrayList NomListe
        {
            get
            {
                if (UseIdentity == 0)
                    return liste_nom;
                else
                    return liste_nom2;
            }
            set
            {
                if (UseIdentity == 0)
                    liste_nom = value;
                else
                    liste_nom2 = value;
            }

        }

        public ArrayList NomListe2
        {
            get { return liste_nom2; }
            set { liste_nom2 = value; }

        }
        #endregion

        public SkillName Learning
        {
            get { return m_Learning; }
            set { m_Learning = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan SavagePaintExpiration
        {
            get
            {
                TimeSpan ts = m_SavagePaintExpiration - DateTime.Now;

                if (ts < TimeSpan.Zero)
                    ts = TimeSpan.Zero;

                return ts;
            }
            set
            {
                m_SavagePaintExpiration = DateTime.Now + value;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public DateTime LastEscortTime
        {
            get { return m_LastEscortTime; }
            set { m_LastEscortTime = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool ResetNameLists
        {
            get { return false; }
            set { liste_nom = new ArrayList(); liste_nom2 = new ArrayList(); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ResetBodyMod
        {
            get { return false; }
            set { BodyMod = 0x0; }
        }
        #endregion

        #region Initialize and constructor
        public static void Initialize()
        {
            if (FastwalkPrevention)
            {
                PacketHandler ph = PacketHandlers.GetHandler(0x02);

                ph.ThrottleCallback = new ThrottlePacketCallback(MovementThrottle_Callback);
            }

            EventSink.Login += new LoginEventHandler(OnLogin);
            EventSink.Logout += new LogoutEventHandler(OnLogout);
            EventSink.Connected += new ConnectedEventHandler(EventSink_Connected);
            EventSink.Disconnected += new DisconnectedEventHandler(EventSink_Disconnected);
        }



        public PlayerMobile()
        {
            m_VisList = new ArrayList();
            liste_nom = new ArrayList();
            liste_nom2 = new ArrayList();
            m_PermaFlags = new ArrayList();
            m_AntiMacroTable = new Hashtable();
            m_GameTime = TimeSpan.Zero;
            m_ShortTermElapse = TimeSpan.FromHours(8.0);
            m_LongTermElapse = TimeSpan.FromHours(40.0);

            m_JusticeProtectors = new ArrayList();
            SalaryInfo = new SalaryInfo();
            GuildInfo = new GuildInfo(this);
            ClassInfo = new ClassInfo(this, 0);
            EvolutionInfo = new EvolutionInfo(this);
        }

        public override string GetKnownName(Mobile m)
        {
            if (m is RacePlayerMobile && this is RacePlayerMobile)
            {
                return (this as RacePlayerMobile).GetKnownName(m as RacePlayerMobile);
            }
            else
            {
                return this.Female ? "Inconnue" : "Inconnu";
            }
        }
        #endregion

        #region CheckSkill Modifications

        new public bool CheckSkill(SkillName skill, double chance)
        {
            if (Mobile.SkillCheckDirectLocationHandler == null)
            {
                return false;
            }
            double postih = 1.0;
            if (Hunger < 5)
                postih -= 0.3;
            else if (Hunger < 10)
                postih -= 0.2;
            else if (Hunger < 15)
                postih -= 0.1;
            else if (Hunger < 20)
                postih += 0.1;
            else
                postih -= 0.1;

            if (Thirst < 5)
                postih -= 0.3;
            else if (Thirst < 10)
                postih -= 0.2;
            else if (Thirst < 15)
                postih -= 0.1;
            else if (Thirst < 20)
                postih += 0.1;
            else
                postih -= 0.1;

            return Mobile.SkillCheckDirectLocationHandler(this, skill, chance * postih);
        }

        new public bool CheckSkill(SkillName skill, double minSkill, double maxSkill)
        {
            if (Mobile.SkillCheckLocationHandler == null)
            {
                return false;
            }
            double postih = 1.0;
            if (Hunger < 5)
                postih += 0.3;
            else if (Hunger < 10)
                postih += 0.2;
            else if (Hunger < 15)
                postih += 0.1;
            else if (Hunger < 20)
                postih -= 0.1;
            else
                postih += 0.1;

            if (Thirst < 5)
                postih += 0.3;
            else if (Thirst < 10)
                postih += 0.2;
            else if (Thirst < 15)
                postih += 0.1;
            else if (Thirst < 20)
                postih -= 0.1;
            else
                postih += 0.1;
            return Mobile.SkillCheckLocationHandler(this, skill, minSkill * postih, maxSkill * postih);
        }

        #endregion

        public static string GetAccessGuildName(AccessGuildFlag AccessGuild)
        {
            return m_AccessGuildNames[(int)AccessGuild];
        }

        public static Direction GetDirection4(Point3D from, Point3D to)
        {
            int dx = from.X - to.X;
            int dy = from.Y - to.Y;

            int rx = dx - dy;
            int ry = dx + dy;

            Direction ret;

            if (rx >= 0 && ry >= 0)
                ret = Direction.West;
            else if (rx >= 0 && ry < 0)
                ret = Direction.South;
            else if (rx < 0 && ry < 0)
                ret = Direction.East;
            else
                ret = Direction.North;

            return ret;
        }

        public bool GetFlag(PlayerFlag flag)
        {
            return ((m_Flags & flag) != 0);
        }

        public void SetFlag(PlayerFlag flag, bool value)
        {
            if (value)
                m_Flags |= flag;
            else
                m_Flags &= ~flag;
        }

        public DesignContext DesignContext
        {
            get { return m_DesignContext; }
            set { m_DesignContext = value; }
        }

        public override bool OnDroppedItemToWorld(Item item, Point3D location)
        {
            if (!base.OnDroppedItemToWorld(item, location))
                return false;

            BounceInfo bi = item.GetBounce();

            if (bi != null)
            {
                Type type = item.GetType();

                if (type.IsDefined(typeof(FurnitureAttribute), true) || type.IsDefined(typeof(DynamicFlipingAttribute), true))
                {
                    object[] objs = type.GetCustomAttributes(typeof(FlipableAttribute), true);

                    if (objs != null && objs.Length > 0)
                    {
                        FlipableAttribute fp = objs[0] as FlipableAttribute;

                        if (fp != null)
                        {
                            int[] itemIDs = fp.ItemIDs;

                            /* Check if current item id is in flip list
                             * if not, do nothing
                             */

                            bool contains = false;

                            for (int i = 0; i < itemIDs.Length; ++i)
                            {
                                if (item.ItemID == itemIDs[i])
                                {
                                    contains = true;
                                    break;
                                }
                            }

                            if (contains)
                            {

                                Point3D oldWorldLoc = bi.m_WorldLoc;
                                Point3D newWorldLoc = location;

                                if (oldWorldLoc.X != newWorldLoc.X || oldWorldLoc.Y != newWorldLoc.Y)
                                {
                                    Direction dir = GetDirection4(oldWorldLoc, newWorldLoc);

                                    if (itemIDs.Length == 2)
                                    {
                                        switch (dir)
                                        {
                                            case Direction.North:
                                            case Direction.South: item.ItemID = itemIDs[0]; break;
                                            case Direction.East:
                                            case Direction.West: item.ItemID = itemIDs[1]; break;
                                        }
                                    }
                                    else if (itemIDs.Length == 4)
                                    {
                                        switch (dir)
                                        {
                                            case Direction.South: item.ItemID = itemIDs[0]; break;
                                            case Direction.East: item.ItemID = itemIDs[1]; break;
                                            case Direction.North: item.ItemID = itemIDs[2]; break;
                                            case Direction.West: item.ItemID = itemIDs[3]; break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        public override void OnSkillInvalidated(Skill skill)
        {
            if (Core.AOS && skill.SkillName == SkillName.MagicResist)
                UpdateResistances();
        }

        public override int GetMaxResistance(ResistanceType type)
        {
            int max = base.GetMaxResistance(type);

            if (type != ResistanceType.Physical && 60 < max && Spells.Fourth.CurseSpell.UnderEffect(this))
                max = 60;

            return max;
        }

        public override void OnNetStateChanged()
        {
            m_LastGlobalLight = -1;
            m_LastPersonalLight = -1;
        }

        public override void ComputeBaseLightLevels(out int global, out int personal)
        {
            global = LightCycle.ComputeLevelFor(this);
            personal = this.LightLevel;
        }

        public override void CheckLightLevels(bool forceResend)
        {
            NetState ns = this.NetState;

            if (ns == null)
                return;

            int global, personal;

            ComputeLightLevels(out global, out personal);

            if (!forceResend)
                forceResend = (global != m_LastGlobalLight || personal != m_LastPersonalLight);

            if (!forceResend)
                return;

            m_LastGlobalLight = global;
            m_LastPersonalLight = personal;

            ns.Send(GlobalLightLevel.Instantiate(global));
            ns.Send(new PersonalLightLevel(this, personal));
        }

        public override int GetMinResistance(ResistanceType type)
        {
            int magicResist = (int)(Skills[SkillName.MagicResist].Value * 10);
            int min = int.MinValue;

            if (magicResist >= 1000)
                min = 40 + ((magicResist - 1000) / 50);
            else if (magicResist >= 400)
                min = (magicResist - 400) / 15;

            if (min > MaxPlayerResistance)
                min = MaxPlayerResistance;

            int baseMin = base.GetMinResistance(type);

            if (min < baseMin)
                min = baseMin;

            return min;
        }

        private static void OnLogin(LoginEventArgs e)
        {
            Mobile from = e.Mobile;

            //Timer timer_exp = new Timer_EXP((RacePlayerMobile)e.Mobile);
            //timer_exp.Start();
            //((RacePlayerMobile)from).Timers_Started = (bool)true;

            SacrificeVirtue.CheckAtrophy(from);
            JusticeVirtue.CheckAtrophy(from);
            CompassionVirtue.CheckAtrophy(from);

            if (AccountHandler.LockdownLevel > AccessLevel.Player)
            {
                string notice;

                Accounting.Account acct = from.Account as Accounting.Account;

                if (acct == null || !acct.HasAccess(from.NetState))
                {
                    if (from.AccessLevel == AccessLevel.Player)
                        notice = "The server is currently under lockdown. No players are allowed to log in at this time.";
                    else
                        notice = "The server is currently under lockdown. You do not have sufficient access level to connect.";

                    Timer.DelayCall(TimeSpan.FromSeconds(1.0), new TimerStateCallback(Disconnect), from);
                }
                else if (from.AccessLevel == AccessLevel.Administrator)
                {
                    notice = "The server is currently under lockdown. As you are an administrator, you may change this from the [Admin gump.";
                }
                else
                {
                    notice = "The server is currently under lockdown. You have sufficient access level to connect.";
                }

                from.SendGump(new NoticeGump(1060637, 30720, notice, 0xFFC000, 300, 140, null, null));
            }

            //bounty system
            PlayerMobile player = (PlayerMobile)from;

            if (player.ShowBountyUpdate)
            {
                from.SendGump(new BountyStatusGump(from, player.BountyUpdateList));
                player.BountyUpdateList.Clear();
                player.ShowBountyUpdate = false;
            }
            //end bounty system
        }

        public void ValidateEquipment()
        {
            if (m_NoDeltaRecursion || Map == null || Map == Map.Internal)
                return;

            if (this.Items == null)
                return;

            m_NoDeltaRecursion = true;
            Timer.DelayCall(TimeSpan.Zero, new TimerCallback(ValidateEquipment_Sandbox));
        }

        private void ValidateEquipment_Sandbox()
        {
            try
            {
                if (Map == null || Map == Map.Internal)
                    return;

                ArrayList items = this.Items;

                if (items == null)
                    return;

                bool moved = false;

                int str = this.Str;
                int dex = this.Dex;
                int intel = this.Int;

                Mobile from = this;

                for (int i = items.Count - 1; i >= 0; --i)
                {
                    if (i >= items.Count)
                        continue;

                    Item item = (Item)items[i];

                    if (item is BaseWeapon)
                    {
                        BaseWeapon weapon = (BaseWeapon)item;

                        bool drop = false;

                        if (dex < weapon.DexRequirement)
                            drop = true;
                        else if (str < AOS.Scale(weapon.StrRequirement, 100 - weapon.GetLowerStatReq()))
                            drop = true;
                        else if (intel < weapon.IntRequirement)
                            drop = true;

                        if (drop)
                        {
                            string name = weapon.Name;

                            if (name == null)
                                name = String.Format("#{0}", weapon.LabelNumber);

                            from.SendLocalizedMessage(1062001, name); // You can no longer wield your ~1_WEAPON~
                            from.AddToBackpack(weapon);
                            moved = true;
                        }
                    }
                    else if (item is BaseArmor)
                    {
                        BaseArmor armor = (BaseArmor)item;

                        bool drop = false;

                        if (!armor.AllowMaleWearer && from.Body.IsMale && from.AccessLevel < AccessLevel.GameMaster)
                        {
                            drop = true;
                        }
                        else if (!armor.AllowFemaleWearer && from.Body.IsFemale && from.AccessLevel < AccessLevel.GameMaster)
                        {
                            drop = true;
                        }
                        else
                        {
                            int strBonus = armor.ComputeStatBonus(StatType.Str), strReq = armor.ComputeStatReq(StatType.Str);
                            int dexBonus = armor.ComputeStatBonus(StatType.Dex), dexReq = armor.ComputeStatReq(StatType.Dex);
                            int intBonus = armor.ComputeStatBonus(StatType.Int), intReq = armor.ComputeStatReq(StatType.Int);

                            if (dex < dexReq || (dex + dexBonus) < 1)
                                drop = true;
                            else if (str < strReq || (str + strBonus) < 1)
                                drop = true;
                            else if (intel < intReq || (intel + intBonus) < 1)
                                drop = true;
                        }

                        if (drop)
                        {
                            string name = armor.Name;

                            if (name == null)
                                name = String.Format("#{0}", armor.LabelNumber);

                            if (armor is BaseShield)
                                from.SendLocalizedMessage(1062003, name); // You can no longer equip your ~1_SHIELD~
                            else
                                from.SendLocalizedMessage(1062002, name); // You can no longer wear your ~1_ARMOR~

                            from.AddToBackpack(armor);
                            moved = true;
                        }
                    }
                }

                if (moved)
                    from.SendLocalizedMessage(500647); // Some equipment has been moved to your backpack.
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                m_NoDeltaRecursion = false;
            }
        }

        public override void Delta(MobileDelta flag)
        {
            base.Delta(flag);

            if ((flag & MobileDelta.Stat) != 0)
                ValidateEquipment();
        }

        private static void Disconnect(object state)
        {
            NetState ns = ((Mobile)state).NetState;
            ((RacePlayerMobile)state).Timers_Started = (bool)false;

            if (ns != null)
                ns.Dispose();
        }

        private static void OnLogout(LogoutEventArgs e)
        {
            //Timer timer_exp = new Timer_EXP((RacePlayerMobile)e.Mobile);
            //timer_exp.Stop();
            //((RacePlayerMobile)e.Mobile).Timers_Started = (bool)false;
        }

        public static void EventSink_Connected(ConnectedEventArgs e)
        {
            PlayerMobile pm = e.Mobile as PlayerMobile;

            if (pm != null)
            {
                pm.m_SessionStart = DateTime.Now;

            }
        }

        public static void EventSink_Disconnected(DisconnectedEventArgs e)
        {
            Mobile from = e.Mobile;
            DesignContext context = DesignContext.Find(from);

            if (context != null)
            {
                /* Client disconnected
                 *  - Remove design context
                 *  - Eject client from house
                 */

                // Remove design context
                DesignContext.Remove(from);

                // Eject client from house
                from.RevealingAction();

                from.MoveToWorld(context.Foundation.BanLocation, context.Foundation.Map);
            }

            PlayerMobile pm = e.Mobile as PlayerMobile;

            if (pm != null)
            {
                pm.m_GameTime += (DateTime.Now - pm.m_SessionStart);
            }
        }

        public override void RevealingAction()
        {
            if (m_DesignContext != null)
                return;

            Spells.Sixth.InvisibilitySpell.RemoveTimer(this);

            base.RevealingAction();
        }

        public override void OnSubItemAdded(Item item)
        {
            if (AccessLevel < AccessLevel.GameMaster && item.IsChildOf(this.Backpack))
            {
                int maxWeight = WeightOverloading.GetMaxWeight(this);
                int curWeight = Mobile.BodyWeight + this.TotalWeight;

                if (curWeight > maxWeight)
                    this.SendLocalizedMessage(1019035, true, String.Format(" : {0} / {1}", curWeight, maxWeight));
            }
        }

        public override bool CanBeHarmful(Mobile target, bool message, bool ignoreOurBlessedness)
        {
            if (m_DesignContext != null || (target is PlayerMobile && ((PlayerMobile)target).m_DesignContext != null))
                return false;

            if ((target is BaseVendor && ((BaseVendor)target).IsInvulnerable) || target is PlayerVendor || target is TownCrier)
            {
                if (message)
                {
                    if (target.Title == null)
                        SendMessage("{0} the vendor cannot be harmed.", target.Name);
                    else
                        SendMessage("{0} {1} cannot be harmed.", target.Name, target.Title);
                }

                return false;
            }

            return base.CanBeHarmful(target, message, ignoreOurBlessedness);
        }

        public override bool CanBeBeneficial(Mobile target, bool message, bool allowDead)
        {
            if (m_DesignContext != null || (target is PlayerMobile && ((PlayerMobile)target).m_DesignContext != null))
                return false;

            return base.CanBeBeneficial(target, message, allowDead);
        }

        public override bool CheckContextMenuDisplay(IEntity target)
        {
            return (m_DesignContext == null);
        }

        public override void OnItemAdded(Item item)
        {
            base.OnItemAdded(item);

            if (item is BaseArmor || item is BaseWeapon)
            {
                Hits = Hits; Stam = Stam; Mana = Mana;
            }
        }

        public override void OnItemRemoved(Item item)
        {
            base.OnItemRemoved(item);

            if (item is BaseArmor || item is BaseWeapon)
            {
                Hits = Hits; Stam = Stam; Mana = Mana;
            }
        }

        public override int HitsMax
        {
            get
            {
                int strBase;
                int strOffs = GetStatOffset(StatType.Str);

                if (Core.AOS)
                {
                    strBase = this.Str;
                    strOffs += AosAttributes.GetValue(this, AosAttribute.BonusHits);
                }
                else
                {
                    strBase = this.RawStr;
                }

                return (strBase / 2) + 50 + strOffs;
            }
        }

        public override int StamMax
        {
            get { return base.StamMax + AosAttributes.GetValue(this, AosAttribute.BonusStam); }
        }

        public override int ManaMax
        {
            get { return base.ManaMax + AosAttributes.GetValue(this, AosAttribute.BonusMana); }
        }

        public override bool Move(Direction d)
        {
            NetState ns = this.NetState;

            if (ns != null)
            {
                var gumps = ns.Gumps;
                foreach (Gump gump in gumps)
                {
                    if (gump is ResurrectGump)
                    {
                        if (Alive)
                        {
                            CloseGump(typeof(ResurrectGump));
                        }
                        else
                        {
                            SendLocalizedMessage(500111); // You are frozen and cannot move.
                            return false;
                        }
                    }
                }
            }

            TimeSpan speed = ComputeMovementSpeed(d);

            if (!base.Move(d))
                return false;

            m_NextMovementTime += speed;
            return true;
        }

        public override bool CheckMovement(Direction d, out int newZ)
        {
            DesignContext context = m_DesignContext;

            if (context == null)
                return base.CheckMovement(d, out newZ);

            HouseFoundation foundation = context.Foundation;

            newZ = foundation.Z + HouseFoundation.GetLevelZ(context.Level);

            int newX = this.X, newY = this.Y;
            Movement.Movement.Offset(d, ref newX, ref newY);

            int startX = foundation.X + foundation.Components.Min.X + 1;
            int startY = foundation.Y + foundation.Components.Min.Y + 1;
            int endX = startX + foundation.Components.Width - 1;
            int endY = startY + foundation.Components.Height - 2;

            return (newX >= startX && newY >= startY && newX < endX && newY < endY && Map == foundation.Map);
        }

        public override bool AllowItemUse(Item item)
        {
            return DesignContext.Check(this);
        }

        public override bool AllowSkillUse(SkillName skill)
        {
            return DesignContext.Check(this);
        }

        public virtual void RecheckTownProtection()
        {
            m_NextProtectionCheck = 10;

            Regions.GuardedRegion reg = this.Region as Regions.GuardedRegion;
            bool isProtected = (reg != null && !reg.IsDisabled());

            if (isProtected != m_LastProtectedMessage)
            {
                if (isProtected)
                    SendLocalizedMessage(500112); // You are now under the protection of the town guards.
                else
                    SendLocalizedMessage(500113); // You have left the protection of the town guards.

                m_LastProtectedMessage = isProtected;
            }
        }

        public override void MoveToWorld(Point3D loc, Map map)
        {
            base.MoveToWorld(loc, map);

            RecheckTownProtection();
        }

        public override void SetLocation(Point3D loc, bool isTeleport)
        {
            base.SetLocation(loc, isTeleport);

            if (isTeleport || --m_NextProtectionCheck == 0)
                RecheckTownProtection();
        }

        public override void GetContextMenuEntries(Mobile from, List<ContextMenuEntry> list)
        {
            base.GetContextMenuEntries(from, list);

            if (from == this)
            {

                if (Alive && InsuranceEnabled)
                {
                    list.Add(new CallbackEntry(6201, new ContextCallback(ToggleItemInsurance)));

                    if (AutoRenewInsurance)
                        list.Add(new CallbackEntry(6202, new ContextCallback(CancelRenewInventoryInsurance)));
                    else
                        list.Add(new CallbackEntry(6200, new ContextCallback(AutoRenewInventoryInsurance)));
                }

                // TODO: Toggle champ titles

                BaseHouse house = BaseHouse.FindHouseAt(this);

                if (house != null && house.IsAosRules)
                    list.Add(new CallbackEntry(6207, new ContextCallback(LeaveHouse)));

                if (m_JusticeProtectors.Count > 0)
                    list.Add(new CallbackEntry(6157, new ContextCallback(CancelProtection)));
            }
        }

        private void CancelProtection()
        {
            for (int i = 0; i < m_JusticeProtectors.Count; ++i)
            {
                Mobile prot = (Mobile)m_JusticeProtectors[i];

                string args = String.Format("{0}\t{1}", this.Name, prot.Name);

                prot.SendLocalizedMessage(1049371, args); // The protective relationship between ~1_PLAYER1~ and ~2_PLAYER2~ has been ended.
                this.SendLocalizedMessage(1049371, args); // The protective relationship between ~1_PLAYER1~ and ~2_PLAYER2~ has been ended.
            }

            m_JusticeProtectors.Clear();
        }

        private void ToggleItemInsurance()
        {
            if (!CheckAlive())
                return;

            BeginTarget(-1, false, TargetFlags.None, new TargetCallback(ToggleItemInsurance_Callback));
            SendLocalizedMessage(1060868); // Target the item you wish to toggle insurance status on <ESC> to cancel
        }

        private bool CanInsure(Item item)
        {
            if (item is Container)
                return false;

            if (item is Spellbook || item is Runebook || item is PotionKeg)
                return false;

            if (item.Stackable)
                return false;

            if (item.LootType == LootType.Cursed)
                return false;

            if (item.ItemID == 0x204E) // death shroud
                return false;

            return true;
        }

        private void ToggleItemInsurance_Callback(Mobile from, object obj)
        {
            if (!CheckAlive())
                return;

            Item item = obj as Item;

            if (item == null || !item.IsChildOf(this))
            {
                BeginTarget(-1, false, TargetFlags.None, new TargetCallback(ToggleItemInsurance_Callback));
                SendLocalizedMessage(1060871, "", 0x23); // You can only insure items that you have equipped or that are in your backpack
            }
            else if (item.Insured)
            {
                item.Insured = false;

                SendLocalizedMessage(1060874, "", 0x35); // You cancel the insurance on the item

                BeginTarget(-1, false, TargetFlags.None, new TargetCallback(ToggleItemInsurance_Callback));
                SendLocalizedMessage(1060868, "", 0x23); // Target the item you wish to toggle insurance status on <ESC> to cancel
            }
            else if (!CanInsure(item))
            {
                BeginTarget(-1, false, TargetFlags.None, new TargetCallback(ToggleItemInsurance_Callback));
                SendLocalizedMessage(1060869, "", 0x23); // You cannot insure that
            }
            else if (item.LootType == LootType.Blessed || item.LootType == LootType.Newbied || item.BlessedFor == from)
            {
                BeginTarget(-1, false, TargetFlags.None, new TargetCallback(ToggleItemInsurance_Callback));
                SendLocalizedMessage(1060870, "", 0x23); // That item is blessed and does not need to be insured
                SendLocalizedMessage(1060869, "", 0x23); // You cannot insure that
            }
            else
            {
                if (!item.PayedInsurance)
                {
                    if (Banker.Withdraw(from, 600))
                    {
                        SendLocalizedMessage(1060398, "600"); // ~1_AMOUNT~ gold has been withdrawn from your bank box.
                        item.PayedInsurance = true;
                    }
                    else
                    {
                        SendLocalizedMessage(1061079, "", 0x23); // You lack the funds to purchase the insurance
                        return;
                    }
                }

                item.Insured = true;

                SendLocalizedMessage(1060873, "", 0x23); // You have insured the item

                BeginTarget(-1, false, TargetFlags.None, new TargetCallback(ToggleItemInsurance_Callback));
                SendLocalizedMessage(1060868, "", 0x23); // Target the item you wish to toggle insurance status on <ESC> to cancel
            }
        }

        private void AutoRenewInventoryInsurance()
        {
            if (!CheckAlive())
                return;

            SendLocalizedMessage(1060881, "", 0x23); // You have selected to automatically reinsure all insured items upon death
            AutoRenewInsurance = true;
        }

        private void CancelRenewInventoryInsurance()
        {
            if (!CheckAlive())
                return;

            SendLocalizedMessage(1061075, "", 0x23); // You have cancelled automatically reinsuring all insured items upon death
            AutoRenewInsurance = false;
        }

        private void LeaveHouse()
        {
            BaseHouse house = BaseHouse.FindHouseAt(this);

            if (house != null)
                this.Location = house.BanLocation;
        }

        private delegate void ContextCallback();

        public override void OnDoubleClick(Mobile from)
        {
            if (this == from && !Warmode)
            {
                IMount mount = Mount;
                if (mount != null && !DesignContext.Check(this))
                    return;
            }

            base.OnDoubleClick(from);
        }

        public override void DisplayPaperdollTo(Mobile to)
        {
            if (DesignContext.Check(this))
                base.DisplayPaperdollTo(to);
        }

        private static bool m_NoRecursion;

        protected override void OnLocationChange(Point3D oldLocation)
        {
            CheckLightLevels(false);

            DesignContext context = m_DesignContext;

            if (context == null || m_NoRecursion)
                return;

            m_NoRecursion = true;

            HouseFoundation foundation = context.Foundation;

            int newX = this.X, newY = this.Y;
            int newZ = foundation.Z + HouseFoundation.GetLevelZ(context.Level);

            int startX = foundation.X + foundation.Components.Min.X + 1;
            int startY = foundation.Y + foundation.Components.Min.Y + 1;
            int endX = startX + foundation.Components.Width - 1;
            int endY = startY + foundation.Components.Height - 2;

            if (newX >= startX && newY >= startY && newX < endX && newY < endY && Map == foundation.Map)
            {
                if (Z != newZ)
                    Location = new Point3D(X, Y, newZ);

                m_NoRecursion = false;
                return;
            }

            Location = new Point3D(foundation.X, foundation.Y, newZ);
            Map = foundation.Map;

            m_NoRecursion = false;
        }

        public virtual void SetName(int v_Serial, string v_Nom, Mobile from)
        {
            NameAndSerial temptn = new NameAndSerial();
            temptn.Serial = v_Serial;
            temptn.Name = (string)v_Nom;


            foreach (NameAndSerial elem in NomListe)
            {
                if (elem.Serial == v_Serial)
                {
                    NomListe.Remove(elem);
                    break;
                }
            }


            this.NomListe.Add(temptn);

        }

        public virtual void EraseName(int v_Serial)
        {
            foreach (NameAndSerial elem in NomListe)
            {
                if (elem.Serial == v_Serial)
                {
                    NomListe.Remove(elem);
                    break;
                }
            }

        }

        public virtual string FindName(int v_Serial)
        {
            var temptn = new NameAndSerial();
            string temp_string = "";

            if (this.Incognito != 1)
            {
                foreach (NameAndSerial elem in NomListe)
                {
                    if (elem.Serial == v_Serial)
                    {
                        temp_string = elem.Name;
                        break;
                    }
                }
            }

            if (temp_string == "")
            {
                if (this.Female)
                    temp_string = "Inconnue";
                else
                    temp_string = "Inconnu";
            }


            return temp_string;

        }

        protected override void OnMapChange(Map oldMap)
        {
            DesignContext context = m_DesignContext;

            if (context == null || m_NoRecursion)
                return;

            m_NoRecursion = true;

            HouseFoundation foundation = context.Foundation;

            if (Map != foundation.Map)
                Map = foundation.Map;

            m_NoRecursion = false;
        }

        public override void OnDamage(int amount, Mobile from, bool willKill)
        {
            if (amount > (Core.AOS ? 25 : 0))
            {
                BandageContext c = BandageContext.GetContext(this);

                if (c != null)
                    c.Slip();
            }

            WeightOverloading.FatigueOnDamage(this, amount);

            base.OnDamage(amount, from, willKill);
        }

        public static int ComputeSkillTotal(Mobile m)
        {
            int total = 0;

            for (int i = 0; i < m.Skills.Length; ++i)
                total += m.Skills[i].BaseFixedPoint;

            return (total / 10);
        }

        public override void Resurrect()
        {
            bool wasAlive = this.Alive;

            base.Resurrect();

            if (this.Alive && !wasAlive)
            {
                Item deathRobe = new DeathRobe();

                if (!EquipItem(deathRobe))
                    deathRobe.Delete();
            }
        }

        public override bool OnBeforeDeath()
        {
            m_InsuranceCost = 0;
            m_InsuranceAward = base.FindMostRecentDamager(false);

            if (m_InsuranceAward != null && !m_InsuranceAward.Player)
                m_InsuranceAward = null;

            if (m_InsuranceAward is PlayerMobile)
                ((PlayerMobile)m_InsuranceAward).m_InsuranceBonus = 0;

            return PossessCommand.UnpossessDeath((Mobile)this);
        }

        private bool CheckInsuranceOnDeath(Item item)
        {
            if (InsuranceEnabled && item.Insured)
            {
                if (AutoRenewInsurance)
                {
                    int cost = (m_InsuranceAward == null ? 600 : 300);

                    if (Banker.Withdraw(this, cost))
                    {
                        m_InsuranceCost += cost;
                        item.PayedInsurance = true;
                    }
                    else
                    {
                        SendLocalizedMessage(1061079, "", 0x23); // You lack the funds to purchase the insurance
                        item.PayedInsurance = false;
                        item.Insured = false;
                    }
                }
                else
                {
                    item.PayedInsurance = false;
                    item.Insured = false;
                }

                if (m_InsuranceAward != null)
                {
                    if (Banker.Deposit(m_InsuranceAward, 300))
                    {
                        if (m_InsuranceAward is PlayerMobile)
                            ((PlayerMobile)m_InsuranceAward).m_InsuranceBonus += 300;
                    }
                }

                return true;
            }

            return false;
        }

        public override DeathMoveResult GetParentMoveResultFor(Item item)
        {

            //VISAGES
            if (item is BaseSkin)
                return DeathMoveResult.RemainEquiped;
            if (item is BankBox)
                return DeathMoveResult.RemainEquiped;
            if (item.Parent is Hair)
                return DeathMoveResult.RemainEquiped;
            if (item.Parent is Beard)
                return DeathMoveResult.RemainEquiped;
            if (item is Hair)
                return DeathMoveResult.RemainEquiped;
            if (item is Beard)
                return DeathMoveResult.RemainEquiped;
            if (item.Layer == Layer.Hair)
                return DeathMoveResult.RemainEquiped;
            if (item.Layer == Layer.FacialHair)
                return DeathMoveResult.RemainEquiped;
            if (item.Layer == Layer.Shirt)
                return DeathMoveResult.RemainEquiped;



            // LE LOOT INTERDIT 
            //
            //
            //if ( CheckInsuranceOnDeath( item ) )
            return DeathMoveResult.MoveToBackpack;

            //return base.GetParentMoveResultFor( item );
        }

        public override DeathMoveResult GetInventoryMoveResultFor(Item item)
        {
            //if ( CheckInsuranceOnDeath( item ) )
            //	return DeathMoveResult.MoveToBackpack;




            /*if (item is Gold)
            {
                int ordonne = item.Amount;
                if (ordonne > 100)
                {
                    Gold g = new Gold(1);

                    ordonne = (int)((ordonne / 100) * 25);
                    g.Amount = ordonne;
                    item.Amount -= ordonne;

                    this.AddToBackpack(g);

                    return DeathMoveResult.MoveToCorpse;
                }
                else
                {
                    return DeathMoveResult.MoveToBackpack;
                }
            }*/


            return DeathMoveResult.MoveToBackpack;

            //return base.GetInventoryMoveResultFor( item );
        }

        public override void OnDeath(Container c)
        {
            base.OnDeath(c);

            HueMod = -1;
            NameMod = null;
            SavagePaintExpiration = TimeSpan.Zero;

            SetHairMods(-1, -1);

            PolymorphSpell.StopTimer(this);
            IncognitoSpell.StopTimer(this);
            DisguiseGump.StopTimer(this);

            EndAction(typeof(PolymorphSpell));
            EndAction(typeof(IncognitoSpell));

            MeerMage.StopEffect(this, false);

            if (m_PermaFlags.Count > 0)
            {
                m_PermaFlags.Clear();

                if (c is Corpse)
                    ((Corpse)c).Criminal = true;

                if (SkillHandlers.Stealing.ClassicMode)
                    Criminal = true;
            }

            if (this.Kills >= 5 && DateTime.Now >= m_NextJustAward)
            {
                Mobile m = FindMostRecentDamager(false);

                if (m != null && m.Player)
                {
                    bool gainedPath = false;

                    int theirTotal = ComputeSkillTotal(m);
                    int ourTotal = ComputeSkillTotal(this);

                    int pointsToGain = 1 + ((theirTotal - ourTotal) / 50);

                    if (pointsToGain < 1)
                        pointsToGain = 1;
                    else if (pointsToGain > 4)
                        pointsToGain = 4;

                    if (VirtueHelper.Award(m, VirtueName.Justice, pointsToGain, ref gainedPath))
                    {
                        if (gainedPath)
                            m.SendLocalizedMessage(1049367); // You have gained a path in Justice!
                        else
                            m.SendLocalizedMessage(1049363); // You have gained in Justice.

                        m.FixedParticles(0x375A, 9, 20, 5027, EffectLayer.Waist);
                        m.PlaySound(0x1F7);

                        m_NextJustAward = DateTime.Now + TimeSpan.FromMinutes(pointsToGain * 2);
                    }
                }
            }

            if (m_InsuranceCost > 0)
                SendLocalizedMessage(1060398, m_InsuranceCost.ToString()); // ~1_AMOUNT~ gold has been withdrawn from your bank box.

            if (m_InsuranceAward is PlayerMobile)
            {
                PlayerMobile pm = (PlayerMobile)m_InsuranceAward;

                if (pm.m_InsuranceBonus > 0)
                    pm.SendLocalizedMessage(1060397, pm.m_InsuranceBonus.ToString()); // ~1_AMOUNT~ gold has been deposited into your bank box.
            }
        }


        public override bool MutateSpeech(ArrayList hears, ref string text, ref object context)
        {
            if (Alive)
                return false;

            if (Core.AOS)
            {
                for (int i = 0; i < hears.Count; ++i)
                {
                    object o = hears[i];

                    if (o != this && o is Mobile && ((Mobile)o).Skills[SkillName.SpiritSpeak].Value >= 100.0)
                        return false;
                }
            }

            return base.MutateSpeech(hears, ref text, ref context);
        }

        public override void Damage(int amount, Mobile from)
        {
            if (Spells.Necromancy.EvilOmenSpell.CheckEffect(this))
                amount = (int)(amount * 1.25);

            Mobile oath = Spells.Necromancy.BloodOathSpell.GetBloodOath(from);

            if (oath == this)
            {
                amount = (int)(amount * 1.1);
                from.Damage(amount, from);
            }

            base.Damage(amount, from);
        }

        public override ApplyPoisonResult ApplyPoison(Mobile from, Poison poison)
        {
            if (!Alive)
                return ApplyPoisonResult.Immune;

            if (Spells.Necromancy.EvilOmenSpell.CheckEffect(this))
                return base.ApplyPoison(from, PoisonImpl.IncreaseLevel(poison));

            return base.ApplyPoison(from, poison);
        }

        public PlayerMobile(Serial s)
            : base(s)
        {
            liste_nom = new ArrayList();
            liste_nom2 = new ArrayList();
            m_VisList = new ArrayList();
            m_AntiMacroTable = new Hashtable();
        }

        public ArrayList VisibilityList
        {
            get { return m_VisList; }
        }

        public ArrayList PermaFlags
        {
            get { return m_PermaFlags; }
        }

        public override int Luck { get { return AosAttributes.GetValue(this, AosAttribute.Luck); } }

        public override bool IsHarmfulCriminal(Mobile target)
        {
            if (SkillHandlers.Stealing.ClassicMode && target is PlayerMobile && ((PlayerMobile)target).m_PermaFlags.Count > 0)
            {
                int noto = Notoriety.Compute(this, target);

                if (noto == Notoriety.Innocent)
                    target.Delta(MobileDelta.Noto);

                return false;
            }

            if (target is BaseCreature && ((BaseCreature)target).InitialInnocent)
                return false;

            //bounty system
            if (BountyBoard.Attackable(this, target))
                return false;
            //end bounty system

            return base.IsHarmfulCriminal(target);
        }

        public bool AntiMacroCheck(Skill skill, object obj)
        {
            if (obj == null || m_AntiMacroTable == null || this.AccessLevel != AccessLevel.Player)
                return true;

            Hashtable tbl = (Hashtable)m_AntiMacroTable[skill];
            if (tbl == null)
                m_AntiMacroTable[skill] = tbl = new Hashtable();

            CountAndTimeStamp count = (CountAndTimeStamp)tbl[obj];
            if (count != null)
            {
                if (count.TimeStamp + SkillCheck.AntiMacroExpire <= DateTime.Now)
                {
                    count.Count = 1;
                    return true;
                }
                else
                {
                    ++count.Count;
                    if (count.Count <= SkillCheck.Allowance)
                        return true;
                    else
                        return false;
                }
            }
            else
            {
                tbl[obj] = count = new CountAndTimeStamp();
                count.Count = 1;

                return true;
            }
        }

        private void RevertHair()
        {
            SetHairMods(-1, -1);
        }

        public void ResetKillTime()
        {
            m_ShortTermElapse = this.GameTime + TimeSpan.FromHours(8);
            m_LongTermElapse = this.GameTime + TimeSpan.FromHours(40);
        }

        #region Serialize /Deserialize
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            if (version == 25)
            {
                ClassInfo = new ClassInfo(this, 0);
                EvolutionInfo = new EvolutionInfo(this);
            }

            // Perform initialization
            MindFollowPermanentVictim = new ArrayList();
            MindFollowPermanentVictimDates = new ArrayList();
            MindFollowTemporaryVictim = new ArrayList();
            MindFollowTemporaryVictimDates = new ArrayList();
            TelepathieVictim = new ArrayList();

            switch (version)
            {
                case 27:
                case 26:
                {
                    ClassInfo = new ClassInfo(this, reader);
                    EvolutionInfo = new EvolutionInfo(this, reader);
                    goto case 25;
                }
                case 25:
                {
                    GuildInfo = new GuildInfo(reader, this);
                    SalaryInfo = new SalaryInfo(reader);
                    goto case 23;
                }
                case 23:
                {
                    m_PermNum = (int)reader.ReadInt();
                    if (m_PermNum > 0)
                    {
                        for (int i = 0; i < m_PermNum; i++)
                        {
                            MindFollowPermanentVictim.Add(reader.ReadMobile()); // Kel: removed the RacePlayerMobile cast to avoid crash on deserialize
                            MindFollowPermanentVictimDates.Add(reader.ReadDateTime());
                        }
                    }

                    m_TempNum = (int)reader.ReadInt();
                    if (m_TempNum > 0)
                    {
                        for (int i = 0; i < m_TempNum; i++)
                        {
                            MindFollowTemporaryVictim.Add(reader.ReadMobile());  // Kel: removed the RacePlayerMobile cast to avoid crash on deserialize
                            MindFollowTemporaryVictimDates.Add(reader.ReadDateTime());
                        }
                    }

                    goto case 21;
                }
                case 21:
                {
                    liste_nom2 = new ArrayList();

                    int countLN2 = reader.ReadInt();
                    for (int i = 0; i < countLN2; i++)
                    {
                        NameAndSerial temp_struct = new NameAndSerial();
                        temp_struct.Serial = reader.ReadInt();
                        temp_struct.Name = reader.ReadString();
                        liste_nom2.Add(temp_struct);
                    }

                    UseIdentity = reader.ReadShort();
                    SecondIdentity = reader.ReadString();
                    goto case 20;
                }
                case 20:
                {
                    m_TelNum = (int)reader.ReadInt();
                    if (m_TelNum > 0)
                    {
                        for (int i = 0; i < m_TelNum; i++)
                        {
                            TelepathieVictim.Add((RacePlayerMobile)reader.ReadMobile());
                        }
                    }

                    goto case 19;
                }
                case 19:
                {
                    m_AccessGuild = (AccessGuildFlag)reader.ReadInt();
                    goto case 18;
                }

                case 18:
                {
                    m_FenetreOuverte = (bool)reader.ReadBool();
                    goto case 17;
                }

                case 17:
                {
                    if (version < 27)
                    {
                        //TODO: Cleanup
                        reader.ReadInt();
                    }
                    goto case 16;
                }
                case 16:
                {
                    if (version < 27)
                    {
                        //TODO: Cleanup
                        var m_Quest = QuestSerializer.DeserializeQuest(reader);
                        if (m_Quest != null)
                            m_Quest.From = this;

                        int count = reader.ReadEncodedInt();
                        if (count > 0)
                        {
                            var m_DoneQuests = new ArrayList();

                            for (int i = 0; i < count; ++i)
                            {
                                Type questType = QuestSerializer.ReadType(QuestSystem.QuestTypes, reader);
                                DateTime restartTime;

                                if (version < 17)
                                    restartTime = DateTime.MaxValue;
                                else
                                    restartTime = reader.ReadDateTime();

                                m_DoneQuests.Add(new QuestRestartInfo(questType, restartTime));
                            }
                        }
                    }

                    liste_nom = new ArrayList();

                    int count1 = reader.ReadInt();
                    for (int i = 0; i < count1; i++)
                    {
                        NameAndSerial temp_struct = new NameAndSerial();
                        temp_struct.Serial = reader.ReadInt();
                        temp_struct.Name = reader.ReadString();
                        liste_nom.Add(temp_struct);
                    }


                    m_Profession = reader.ReadEncodedInt();
                    goto case 15;
                }
                case 15:
                {
                    m_LastCompassionLoss = reader.ReadDeltaTime();
                    goto case 14;
                }
                case 14:
                {
                    m_CompassionGains = reader.ReadEncodedInt();

                    if (m_CompassionGains > 0)
                        m_NextCompassionDay = reader.ReadDeltaTime();

                    goto case 11;
                }
                case 11:
                {
                    if (version < 13)
                    {
                        ArrayList payed = reader.ReadItemList();

                        for (int i = 0; i < payed.Count; ++i)
                            ((Item)payed[i]).PayedInsurance = true;
                    }

                    goto case 10;
                }
                case 10:
                {
                    if (reader.ReadBool())
                    {
                        m_HairModID = reader.ReadInt();
                        m_HairModHue = reader.ReadInt();
                        m_BeardModID = reader.ReadInt();
                        m_BeardModHue = reader.ReadInt();

                        // We cannot call SetHairMods( -1, -1 ) here because the items have not yet loaded
                        Timer.DelayCall(TimeSpan.Zero, new TimerCallback(RevertHair));
                    }

                    goto case 9;
                }
                case 9:
                {
                    SavagePaintExpiration = reader.ReadTimeSpan();

                    if (SavagePaintExpiration > TimeSpan.Zero)
                    {
                        BodyMod = (Female ? 184 : 183);
                        HueMod = 0;
                    }

                    goto case 8;
                }
                case 8:
                {
                    m_NpcGuild = (NpcGuild)reader.ReadInt();
                    m_NpcGuildJoinTime = reader.ReadDateTime();

                    m_NpcGuildGameTime = reader.ReadTimeSpan();
                    goto case 7;
                }
                case 7:
                {
                    m_PermaFlags = reader.ReadMobileList();
                    goto case 5;
                }
                case 5:
                {

                    m_Telepathy = reader.ReadBool();

                    m_LCP = reader.ReadShort();
                    m_LCS = reader.ReadShort();
                    m_LCB = reader.ReadShort();
                    m_Sexe = reader.ReadString();
                    m_Apparence = reader.ReadString();
                    m_Taille = reader.ReadShort();
                    m_Poids = reader.ReadShort();
                    m_Age = reader.ReadShort();
                    m_Chance = reader.ReadShort();
                    m_Resumer_BG = reader.ReadString();
                    m_Titre_Nouveu = reader.ReadString();
                    m_TicksXP = reader.ReadDouble();
                    m_Soif = reader.ReadShort();
                    m_Incognito = reader.ReadShort();
                    m_Fatigue = reader.ReadShort();


                    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //Start Sickness Edit///////////////////////////////////////////////////////////////////////////////////////////////////

                    m_VampType = (VampType)reader.ReadInt();

                    m_Vampirism = (bool)reader.ReadBool();

                    m_VampHunger = (bool)reader.ReadBool();

                    m_VampBurn = (bool)reader.ReadBool();

                    m_Blood = reader.ReadInt();

                    m_Timers_Started = (bool)reader.ReadBool();
                    //End Sickness Edit/////////////////////////////////////////////////////////////////////////////////////////////////////
                    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    goto case 4;
                }
                case 4:
                {
                    m_LastJusticeLoss = reader.ReadDeltaTime();
                    m_JusticeProtectors = reader.ReadMobileList();
                    goto case 3;
                }
                case 3:
                {
                    m_LastSacrificeGain = reader.ReadDeltaTime();
                    m_LastSacrificeLoss = reader.ReadDeltaTime();
                    m_AvailableResurrects = reader.ReadInt();
                    goto case 2;
                }
                case 2:
                {
                    m_Flags = (PlayerFlag)reader.ReadInt();
                    goto case 1;
                }
                case 1:
                {
                    m_LongTermElapse = reader.ReadTimeSpan();
                    m_ShortTermElapse = reader.ReadTimeSpan();
                    m_GameTime = reader.ReadTimeSpan();
                    goto case 0;
                }
                case 0:
                {
                    break;
                }
            }

            if (m_PermaFlags == null)
                m_PermaFlags = new ArrayList();

            if (m_JusticeProtectors == null)
                m_JusticeProtectors = new ArrayList();

            //			if ( m_BOBFilter == null )
            //				m_BOBFilter = new Engines.BulkOrders.BOBFilter();

            ArrayList list = this.Stabled;

            for (int i = 0; i < list.Count; ++i)
            {
                BaseCreature bc = list[i] as BaseCreature;

                if (bc != null)
                    bc.IsStabled = true;
            }

        }

        public override void Serialize(GenericWriter writer)
        {

            //cleanup our anti-macro table 
            foreach (Hashtable t in m_AntiMacroTable.Values)
            {
                ArrayList remove = new ArrayList();
                foreach (CountAndTimeStamp time in t.Values)
                {
                    if (time.TimeStamp + SkillCheck.AntiMacroExpire <= DateTime.Now)
                        remove.Add(time);
                }

                for (int i = 0; i < remove.Count; ++i)
                    t.Remove(remove[i]);
            }

            //decay our kills
            if (m_ShortTermElapse < this.GameTime)
            {
                m_ShortTermElapse += TimeSpan.FromHours(8);
                if (ShortTermMurders > 0)
                    --ShortTermMurders;
            }

            if (m_LongTermElapse < this.GameTime)
            {
                m_LongTermElapse += TimeSpan.FromHours(40);
                if (Kills > 0)
                    --Kills;
            }

            base.Serialize(writer);

            writer.Write((int)27); // version

            // 27 
            // Removal of quest stuff

            // 26
            ClassInfo.Serialize(writer);
            EvolutionInfo.Serialize(writer);

            // 25
            GuildInfo.Serialize(writer);
            SalaryInfo.Serialize(writer);

            // 23
            if (MindFollowPermanentVictim != null)
                m_PermNum = MindFollowPermanentVictim.Count;

            writer.Write((int)m_PermNum);

            if (MindFollowPermanentVictim != null)
            {
                for (int i = 0; i < MindFollowPermanentVictim.Count; i++)
                {
                    writer.Write((Mobile)MindFollowPermanentVictim[i]);
                    writer.Write((DateTime)MindFollowPermanentVictimDates[i]);
                }
            }

            if (MindFollowTemporaryVictim != null)
                m_TempNum = MindFollowTemporaryVictim.Count;

            writer.Write((int)m_TempNum);

            if (MindFollowTemporaryVictim != null)
            {
                for (int i = 0; i < MindFollowTemporaryVictim.Count; i++)
                {
                    writer.Write((Mobile)MindFollowTemporaryVictim[i]);
                    writer.Write((DateTime)MindFollowTemporaryVictimDates[i]);
                }
            }

            writer.Write(liste_nom2.Count);
            foreach (NameAndSerial nl in liste_nom2)
            {
                //this.SendMessage("total : "+liste_nom.Count+" : "+nl.Serial+" : "+nl.Nom );
                writer.Write((int)nl.Serial);
                writer.Write((string)nl.Name);
            }

            writer.Write((short)UseIdentity);
            writer.Write((string)SecondIdentity);


            if (TelepathieVictim != null)
                m_TelNum = TelepathieVictim.Count;

            writer.Write((int)m_TelNum);


            if (TelepathieVictim != null)
            {
                for (int i = 0; i < TelepathieVictim.Count; i++)
                {
                    writer.Write((Mobile)TelepathieVictim[i]);
                }
            }

            writer.Write((int)m_AccessGuild);
            writer.Write((bool)m_FenetreOuverte);

            writer.Write(liste_nom.Count);
            foreach (NameAndSerial nl in liste_nom)
            {
                //this.SendMessage("total : "+liste_nom.Count+" : "+nl.Serial+" : "+nl.Nom );
                writer.Write((int)nl.Serial);
                writer.Write((string)nl.Name);
            }


            writer.WriteEncodedInt((int)m_Profession);

            writer.WriteDeltaTime(m_LastCompassionLoss);

            writer.WriteEncodedInt(m_CompassionGains);

            if (m_CompassionGains > 0)
                writer.WriteDeltaTime(m_NextCompassionDay);

            //m_BOBFilter.Serialize( writer );

            bool useMods = (m_HairModID != -1 || m_BeardModID != -1);

            writer.Write(useMods);

            if (useMods)
            {
                writer.Write((int)m_HairModID);
                writer.Write((int)m_HairModHue);
                writer.Write((int)m_BeardModID);
                writer.Write((int)m_BeardModHue);
            }

            writer.Write(SavagePaintExpiration);

            writer.Write((int)m_NpcGuild);
            writer.Write((DateTime)m_NpcGuildJoinTime);
            writer.Write((TimeSpan)m_NpcGuildGameTime);

            writer.WriteMobileList(m_PermaFlags, true);







            writer.Write((bool)m_Telepathy);


            writer.Write(m_LCP);
            writer.Write(m_LCS);
            writer.Write(m_LCB);
            writer.Write(m_Sexe);
            writer.Write(m_Apparence);
            writer.Write(m_Taille);
            writer.Write(m_Poids);
            writer.Write(m_Age);
            writer.Write(m_Chance);
            writer.Write(m_Resumer_BG);
            writer.Write(m_Titre_Nouveu);
            writer.Write(m_TicksXP);
            writer.Write(m_Soif);
            writer.Write(m_Incognito);
            writer.Write(m_Fatigue);


            // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Start Sickness Edit////////////////////////////////////////////////////////////////////////////////////////////

            writer.Write((int)m_VampType);

            writer.Write(m_Vampirism);

            writer.Write(m_VampHunger);

            writer.Write(m_VampBurn);

            writer.Write(m_Blood);

            writer.Write(m_Timers_Started);

            //End Sickness Edit//////////////////////////////////////////////////////////////////////////////////////////////
            // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //writer.Write( NextSmithBulkOrder );

            writer.WriteDeltaTime(m_LastJusticeLoss);
            writer.WriteMobileList(m_JusticeProtectors, true);

            writer.WriteDeltaTime(m_LastSacrificeGain);
            writer.WriteDeltaTime(m_LastSacrificeLoss);
            writer.Write(m_AvailableResurrects);

            writer.Write((int)m_Flags);

            writer.Write(m_LongTermElapse);
            writer.Write(m_ShortTermElapse);
            writer.Write(this.GameTime);
        }

        #endregion

        #region CanSee
        public override bool CanSee(Mobile m)
        {
            if ((m is PlayerMobile && ((PlayerMobile)m).m_VisList.Contains(this)) ||
                (m is PlayerMobile &&
                this.AccessLevel >= AccessLevel.Counselor &&
                m.AccessLevel <= this.AccessLevel))
                return true;

            return base.CanSee(m);
        }

        public override bool CanSee(Item item)
        {
            if (m_DesignContext != null && m_DesignContext.Foundation.IsHiddenToCustomizer(item))
                return false;

            return base.CanSee(item);
        }
        #endregion

        #region Fastwalk Prevention
        private static bool FastwalkPrevention = true; // Is fastwalk prevention enabled?
        private static TimeSpan FastwalkThreshold = TimeSpan.FromSeconds(0.4); // Fastwalk prevention will become active after 0.4 seconds

        private DateTime m_NextMovementTime;

        public virtual bool UsesFastwalkPrevention { get { return (AccessLevel < AccessLevel.GameMaster); } }

        public virtual TimeSpan ComputeMovementSpeed(Direction dir)
        {
            if ((dir & Direction.Mask) != (this.Direction & Direction.Mask))
                return TimeSpan.Zero;

            bool running = ((dir & Direction.Running) != 0);

            bool onHorse = (this.Mount != null);

            if (onHorse)
                return (running ? TimeSpan.FromSeconds(0.1) : TimeSpan.FromSeconds(0.2));

            return (running ? TimeSpan.FromSeconds(0.2) : TimeSpan.FromSeconds(0.4));
        }

        public static bool MovementThrottle_Callback(NetState ns)
        {
            PlayerMobile pm = ns.Mobile as PlayerMobile;

            if (pm == null || !pm.UsesFastwalkPrevention)
                return true;

            if (pm.m_NextMovementTime == DateTime.MinValue)
            {
                // has not yet moved
                pm.m_NextMovementTime = DateTime.Now;
                return true;
            }

            TimeSpan ts = pm.m_NextMovementTime - DateTime.Now;

            if (ts < TimeSpan.Zero)
            {
                // been a while since we've last moved
                pm.m_NextMovementTime = DateTime.Now;
                return true;
            }

            return (ts < FastwalkThreshold);
        }
        #endregion

        #region OnMove
        protected override bool OnMove(Server.Direction d)
        {
        	if (Hidden && AccessLevel == Server.AccessLevel.Player && (d & Server.Direction.Running) != Server.Direction.North || this.Mounted)
        		this.RevealingAction();
        	// On v�rifie qu'on est bien cach�, qu'on n'est pas GM et qu'il reste 4 pas ou moins, sinon, vive le spam...
            else if (Hidden && AccessLevel == Server.AccessLevel.Player  && (AllowedStealthSteps-- <= 0))
            	{
            	Stealth.Initialize();
            	Stealth.OnUse((Mobile)this);
                }
        	//else if (Hidden && AccessLevel == Server.AccessLevel.Player && AllowedStealthSteps < 5)
            //    this.SendMessage("Il vous reste {0} pas furtif(s)", AllowedStealthSteps);
            return true;
        }

        #endregion

        #region Enemy of One
        private Type m_EnemyOfOneType;
        private bool m_WaitingForEnemy;

        public Type EnemyOfOneType
        {
            get { return m_EnemyOfOneType; }
            set
            {
                Type oldType = m_EnemyOfOneType;
                Type newType = value;

                if (oldType == newType)
                    return;

                m_EnemyOfOneType = value;

                DeltaEnemies(oldType, newType);
            }
        }

        public bool WaitingForEnemy
        {
            get { return m_WaitingForEnemy; }
            set { m_WaitingForEnemy = value; }
        }

        private void DeltaEnemies(Type oldType, Type newType)
        {
            foreach (Mobile m in this.GetMobilesInRange(18))
            {
                Type t = m.GetType();

                if (t == oldType || t == newType)
                    Send(new MobileMoving(m, Notoriety.Compute(this, m)));
            }
        }
        #endregion

        #region Hair and beard mods
        private int m_HairModID = -1, m_HairModHue;
        private int m_BeardModID = -1, m_BeardModHue;

        public void SetHairMods(int hairID, int beardID)
        {
            if (hairID == -1)
                InternalRestoreHair(true, ref m_HairModID, ref m_HairModHue);
            else if (hairID != -2)
                InternalChangeHair(true, hairID, ref m_HairModID, ref m_HairModHue);

            if (beardID == -1)
                InternalRestoreHair(false, ref m_BeardModID, ref m_BeardModHue);
            else if (beardID != -2)
                InternalChangeHair(false, beardID, ref m_BeardModID, ref m_BeardModHue);
        }

        private Item CreateHair(bool hair, int id, int hue)
        {
            if (hair)
                return Server.Items.Hair.CreateByID(id, hue);
            else
                return Server.Items.Beard.CreateByID(id, hue);
        }

        private void InternalRestoreHair(bool hair, ref int id, ref int hue)
        {
            if (id == -1)
                return;

            Item item = FindItemOnLayer(hair ? Layer.Hair : Layer.FacialHair);

            if (item != null)
                item.Delete();

            if (id != 0)
                AddItem(CreateHair(hair, id, hue));

            id = -1;
            hue = 0;
        }

        private void InternalChangeHair(bool hair, int id, ref int storeID, ref int storeHue)
        {
            Item item = FindItemOnLayer(hair ? Layer.Hair : Layer.FacialHair);

            if (item != null)
            {
                if (storeID == -1)
                {
                    storeID = item.ItemID;
                    storeHue = item.Hue;
                }

                item.Delete();
            }
            else if (storeID == -1)
            {
                storeID = 0;
                storeHue = 0;
            }

            if (id == 0)
                return;

            AddItem(CreateHair(hair, id, 0));
        }
        #endregion

        #region Virtue stuff
        private DateTime m_LastSacrificeGain;
        private DateTime m_LastSacrificeLoss;
        private int m_AvailableResurrects;

        public DateTime LastSacrificeGain { get { return m_LastSacrificeGain; } set { m_LastSacrificeGain = value; } }
        public DateTime LastSacrificeLoss { get { return m_LastSacrificeLoss; } set { m_LastSacrificeLoss = value; } }
        public int AvailableResurrects { get { return m_AvailableResurrects; } set { m_AvailableResurrects = value; } }

        private DateTime m_NextJustAward;
        private DateTime m_LastJusticeLoss;
        private ArrayList m_JusticeProtectors;

        public DateTime LastJusticeLoss { get { return m_LastJusticeLoss; } set { m_LastJusticeLoss = value; } }
        public ArrayList JusticeProtectors { get { return m_JusticeProtectors; } set { m_JusticeProtectors = value; } }

        private DateTime m_LastCompassionLoss;
        private DateTime m_NextCompassionDay;
        private int m_CompassionGains;

        public DateTime LastCompassionLoss { get { return m_LastCompassionLoss; } set { m_LastCompassionLoss = value; } }
        public DateTime NextCompassionDay { get { return m_NextCompassionDay; } set { m_NextCompassionDay = value; } }
        public int CompassionGains { get { return m_CompassionGains; } set { m_CompassionGains = value; } }
        #endregion

        #region Nested Classes
        private class CallbackEntry : ContextMenuEntry
        {
            private ContextCallback m_Callback;

            public CallbackEntry(int number, ContextCallback callback)
                : this(number, -1, callback)
            {
            }

            public CallbackEntry(int number, int range, ContextCallback callback)
                : base(number, range)
            {
                m_Callback = callback;
            }

            public override void OnClick()
            {
                if (m_Callback != null)
                    m_Callback();
            }
        }

        private class CountAndTimeStamp
        {
            private int m_Count;
            private DateTime m_Stamp;

            public CountAndTimeStamp()
            {
            }

            public DateTime TimeStamp { get { return m_Stamp; } }
            public int Count
            {
                get { return m_Count; }
                set { m_Count = value; m_Stamp = DateTime.Now; }
            }
        }
        #endregion
    }
}
