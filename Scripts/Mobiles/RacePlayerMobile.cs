using System;
using System.IO;
using System.Text;
using System.Collections;
using Server;
using Server.Gumps;
using Server.Menus;
using Server.Prompts;
using Server.Guilds;
using Server.Network;
using Server.Targeting;
using Server.HuePickers;
using Server.Items;
using Server.Mobiles;
using Server.ContextMenus;
using Server.Accounting;
using System.Collections.Generic;
using Server.Engines;


namespace Server.Mobiles
{
	#region Listes Enum
	public enum RaceType
	{
		None = 0,	
		Elfe = 1,
		Gnome,
		Petite_Personne,
		Demi_Elfe,
		Humain,
		Nain,
		Demi_Orque,
		Elfe_Noir,
		Tiefling 
	}

    public enum AlignementType
    {
        None = 0,
        Loyal_Bon = 1,
        Neutre_Bon,
        Chaotique_Bon,
        Loyal_Neutre,
        Neutre_Strict,
        Chaotique_Neutre,
        Loyal_Mauvais,
        Neutre_Mauvais,
        Chaotique_Mauvais 
    }


	#endregion

	public class RacePlayerMobile : PlayerMobile
    {
        public void MigrateEvolutionSystem()
        {
            // Migrate evolution
            EvolutionInfo.m_Race = m_Race;
            EvolutionInfo.Alignement = m_Alignement;
            EvolutionInfo.Experience = (int)m_Experience;
            EvolutionInfo.DestinyPoints = m_Karma;
            EvolutionInfo.Grade1 = m_Cote1;
            EvolutionInfo.Grade2 = m_Cote2;
            EvolutionInfo.Grade3 = m_Cote3;
            EvolutionInfo.Grade4 = m_Cote4;
            EvolutionInfo.Grade1By = m_Cote1_Par;
            EvolutionInfo.Grade2By = m_Cote2_Par;
            EvolutionInfo.Grade3By = m_Cote3_Par;
            EvolutionInfo.Grade4By = m_Cote4_Par;
            EvolutionInfo.XPTicks = (int)m_TicksXP;
            EvolutionInfo.GradeTime = m_CoteTime;
            EvolutionInfo.Age = m_Age;


            // Migrate the capacities
            ClassInfo.Capacities[CapacityName.Acrobatics].Value = m_Acrobatie;
            ClassInfo.Capacities[CapacityName.Alchemy].Value = m_Alchimie;
            ClassInfo.Capacities[CapacityName.Assassination].Value = m_Assassinat;
            ClassInfo.Capacities[CapacityName.Bardic].Value = m_Bardisme;
            ClassInfo.Capacities[CapacityName.Tinkering].Value = m_Bricolage;
            ClassInfo.Capacities[CapacityName.Bows].Value = m_Arcs;
            ClassInfo.Capacities[CapacityName.CloseCombat].Value = m_Armes_de_Melee;
            ClassInfo.Capacities[CapacityName.DistanceCombat].Value = m_Armes_a_Distance;
            ClassInfo.Capacities[CapacityName.BetterAgility].Value = m_Armes_de_Jets;
            ClassInfo.Capacities[CapacityName.Wrestling].Value = m_Lutte;
            ClassInfo.Capacities[CapacityName.Tailoring].Value = m_Couture;
            ClassInfo.Capacities[CapacityName.Stealth].Value = m_Deplacement_Silencieux;
            ClassInfo.Capacities[CapacityName.Enchanting].Value = m_Enchantement;
            ClassInfo.Capacities[CapacityName.BetterEndurance].Value = m_Endurance_Accrue;
            ClassInfo.Capacities[CapacityName.Lore].Value = m_Erudition;
            ClassInfo.Capacities[CapacityName.AnimalEmpathy].Value = m_Empathie_Animale;
            ClassInfo.Capacities[CapacityName.Blacksmithing].Value = m_Ferraillerie;
            ClassInfo.Capacities[CapacityName.Hunting].Value = m_Furtivite;
            ClassInfo.Capacities[CapacityName.Herblore].Value = m_Herboristerie;
            ClassInfo.Capacities[CapacityName.Wizardry].Value = m_Magie_arcanique;
            ClassInfo.Capacities[CapacityName.Sorcery].Value = m_Sorcellerie;
            ClassInfo.Capacities[CapacityName.Necromancy].Value = m_Necromancie;
            ClassInfo.Capacities[CapacityName.Theology].Value = m_Magie_divine;
            ClassInfo.Capacities[CapacityName.Nature].Value = m_Magie_naturelle;
            ClassInfo.Capacities[CapacityName.Healing].Value = m_Medecine;
            ClassInfo.Capacities[CapacityName.LockPicking].Value = m_Crochetage + m_Pose_de_Pieges;
            ClassInfo.Capacities[CapacityName.Rage].Value = m_Rage;
            ClassInfo.Capacities[CapacityName.Sabotage].Value = m_Sabotage;
            ClassInfo.Capacities[CapacityName.Telepathy].Value = m_Telepathie;
            ClassInfo.Capacities[CapacityName.PickPocket].Value = m_Vol_a_la_Tire;

            if(m_Classe == ClasseType.None) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(1));
            if (m_Classe == ClasseType.Guerrier) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(1));
            if (m_Classe == ClasseType.Mage) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(2));
            if (m_Classe == ClasseType.Bricoleur) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(3));
            if (m_Classe == ClasseType.Sorcier) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(4));
            if (m_Classe == ClasseType.Paladin) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(5));
            if (m_Classe == ClasseType.Necromancien) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(6));
            if (m_Classe == ClasseType.Druide) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(7));
            if(m_Classe == ClasseType.Alchimiste) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(8));
            if(m_Classe == ClasseType.Archer) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(9));
            if(m_Classe == ClasseType.Barde) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(10));
            if(m_Classe == ClasseType.Moine) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(11));
            if(m_Classe == ClasseType.Voleur) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(12));
            if(m_Classe == ClasseType.Sage) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(13));
            if(m_Classe == ClasseType.Rodeur)ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(14));
            if(m_Classe == ClasseType.Enchanteur) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(15));
            if(m_Classe == ClasseType.Forgeron) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(16));
            if(m_Classe == ClasseType.Clerc) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(17));
            if(m_Classe == ClasseType.Couturier) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(18));
            if(m_Classe == ClasseType.Assasin) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(19));
            if (m_Classe == ClasseType.Barbare) ClassConfig.MigrateClass(this, ClassConfig.GetClassByNumber(20));


        }

        #region Classes Internes
        protected class EvoTimerDeath : Timer
        {
            private RacePlayerMobile m_from;
            public EvoTimerDeath(RacePlayerMobile from)
                : base(TimeSpan.FromMinutes(30), TimeSpan.FromMinutes(30))
            {
                m_from = from;
            }

            protected override void OnTick()
            {
                if (m_from != null)
                {
                    if (m_from.PenaliteMort == true)
                    {
                        m_from.PenaliteMort = false;
                    }

                    var evo = m_from.EvolutionInfo;
                    if (evo.DestinyPoints < evo.MaxDestinyPoints)
                    {
                        // Increase destiny points capacity
                        evo.DestinyPoints++;
                    }                
                }
            }

        }
        #endregion

        #region Variables

        private int Pen_Str = 0;
        private int Pen_Dex = 0;
        private int Pen_Int = 0;
        private bool m_GuildeChoisie = false;

        private int Pen_MaxStr = 305;
        private int Pen_MaxDex = 305;
        private int Pen_MaxInt = 305;

        protected EvoTimerDeath TimerDeath;
        private bool m_AfficheTiefling;
        private bool m_PenaliteMort;

		private short m_Acrobatie;
		private short m_Alchimie;
		private short m_Assassinat;
		private short m_Bardisme;
		private short m_Bricolage;
		private short m_Arcs;
		private short m_Crochetage;
		private short m_Armes_de_Melee ;
		private short m_Armes_a_Distance; 
		private short m_Armes_de_Jets ;
		private short m_Lutte ;
		private short m_Couture; 
		private short m_Deplacement_Silencieux;
		private short m_Enchantement;
		private short m_Endurance_Accrue;
		private short m_Erudition;
		private short m_Empathie_Animale;
		private short m_Ferraillerie;
		private short m_Furtivite;
		private short m_Herboristerie;
		private short m_Magie_arcanique;
		private short m_Sorcellerie;
		private short m_Necromancie;
		private short m_Magie_divine;
		private short m_Magie_naturelle;
		private short m_Medecine;
		private short m_Pose_de_Pieges;
		private short m_Rage;
		private short m_Sabotage;
		private short m_Telepathie;
		private short m_Vol_a_la_Tire;
        private bool m_ResetSkillsCaps;


		private InconscientBody m_Body;
		private bool m_Blessed;

        private AlignementType m_Alignement;
		private RaceType m_Race;
		private ClasseType m_Classe;

		private double m_Experience;
		private double m_ExperienceBesoin;
		private int m_Niveau;

		public int m_MaxStr = 305;
		public int m_MaxDex = 305;
		public int m_MaxInt = 305;
       
		private short m_Derniere_Cote;
		private short m_Cote1;
		private short m_Cote2;
		private short m_Cote3;
		private short m_Cote4;
      
        private string m_Cote1_Par;
        private string m_Cote2_Par;
        private string m_Cote3_Par;
        private string m_Cote4_Par;

		private short m_CoteMoyenne;
        private short m_CotesLimite;
        private short m_PAResetCount = 0;

        private DateTime m_CoteTime;

		private short m_Karma;
        public bool Hallucinating;
        private bool m_AstralTravel = false;
        private bool m_IsAddicted = false;
        private Dictionary<PotionAddiction, DateTime> m_Addictions = new Dictionary<PotionAddiction, DateTime>();
        private AddictionTimer NextAddictionTimer = null;

		public Races r = new Races();

		#endregion

		#region Propri�t�s

		public RacePlayerMobile()
		{
            TimerDeath = new EvoTimerDeath(this);
            WorldContext.RacePlayerMobiles.Add(this);
            TimerDeath.Start();
		}

		public RacePlayerMobile( Serial s ) : base( s )
		{
            TimerDeath = new EvoTimerDeath(this);
            TimerDeath.Start();
		}

        [CommandProperty(AccessLevel.GameMaster)]
        public string PlayerName
        {
            get { return m_RealName; }
            set { m_RealName = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string CurrentPlayerName
        {
            get { return this.CurrentIdentityType == IdentityType.Second ? this.SecondIdentity : this.PlayerName; }
        }
        
        [CommandProperty(AccessLevel.GameMaster)]
        public bool AstralTravel
        {
            get { return m_AstralTravel; }
            set {
                m_AstralTravel = value;
                Squelched = value;
            }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool PenaliteMort
        {
            get 
            { 

                return m_PenaliteMort; 
            }
            set 
            {
                if (value == true)
                {
                	Pen_Str = RawStr;
                    Pen_Dex = RawDex;
                    Pen_Int = RawInt;
                    Pen_MaxStr = m_MaxStr;
                    Pen_MaxDex = m_MaxDex;
                    Pen_MaxInt = m_MaxInt;

                    RawStr = (int)(RawStr / 2);
                    RawDex = (int)(RawDex / 2);
                    RawInt = (int)(RawInt / 2);
                    m_MaxStr = RawStr;
                    m_MaxDex = RawDex;
                    m_MaxInt = RawInt;
                }
                else
                {
                    if (Pen_Str > 1) RawStr = Pen_Str;
                    if (Pen_Dex > 1) RawDex = Pen_Dex;
                    if (Pen_Int > 1) RawInt = Pen_Int;
                    if (Pen_MaxStr > 1) m_MaxStr = Pen_MaxStr;
                    if (Pen_MaxInt > 1) m_MaxDex = Pen_MaxDex;
                    if (Pen_MaxInt > 1) m_MaxInt = Pen_MaxInt;
                }
                m_PenaliteMort = value; 
            }
        }

		[CommandProperty( AccessLevel.GameMaster )]
		public int MaxStr
		{
			get{ return m_MaxStr; }
			set{ m_MaxStr = value; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int MaxDex
		{
			get{ return m_MaxDex; }
			set{ m_MaxDex = value; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int MaxInt
		{
			get{ return m_MaxInt; }
			set{ m_MaxInt = value; }
		}

        public IdentityType CurrentIdentityType
        {
            get {
                if (this.UseIdentity == 0) return IdentityType.First;
                else if (this.UseIdentity == 1) return IdentityType.Second;
                else return IdentityType.First;
            }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool IsHallucinating
        {
            get { return Hallucinating; }
            set { Hallucinating = value; InvalidateProperties(); }
        }

        public Dictionary<PotionAddiction, DateTime> Addictions
        {
            get { return m_Addictions; }
            set { m_Addictions = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool IsAddicted
        {
            get { return m_IsAddicted; }
            set
            {
                if (m_IsAddicted == value)
                    return;
                m_IsAddicted = value;

                if (value)
                { // lower stats
                    //this.StatMods.Add(new StatMod(StatType.All,"Addictions",)
                    StatMod modStr = new StatMod(StatType.Str, "[Str] Addiction", -((int)(RawStr / 5)), TimeSpan.FromDays(1));
                    StatMod modInt = new StatMod(StatType.Int, "[Int] Addiction", -((int)(RawInt / 5)), TimeSpan.FromDays(1));
                    StatMod modDex = new StatMod(StatType.Dex, "[Dex] Addiction", -((int)(RawDex / 5)), TimeSpan.FromDays(1));

                    this.AddStatMod(modStr);
                    this.AddStatMod(modInt);
                    this.AddStatMod(modDex);
                }
                else
                { //higher stats
                    StatMod mod = null;

                    mod = this.GetStatMod("[Str] Addiction");
                    if (mod != null && mod.Offset < 0)
                    {
                        this.RemoveStatMod("[Str] Addiction");
                    }

                    mod = this.GetStatMod("[Int] Addiction");
                    if (mod != null && mod.Offset < 0)
                    {
                        this.RemoveStatMod("[Int] Addiction");
                    }

                    mod = this.GetStatMod("[Dex] Addiction");
                    if (mod != null && mod.Offset < 0)
                    {
                        this.RemoveStatMod("[Dex] Addiction");
                    }

                }
            }
        }
		#endregion
        
		#region Serialize
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

            
            writer.Write((int)18); //VERSION

            //Version 17
            writer.Write(m_PAResetCount);

            //Version 16
            writer.Write(m_IsAddicted);
            writer.Write(Addictions.Keys.Count);
            foreach (PotionAddiction key in Addictions.Keys)
            {
                writer.Write(key.Type.FullName);
                writer.Write(key.Name);
                writer.Write(key.Delay);
                writer.Write(Addictions[key]);
            }

            //Version 15
            writer.Write(m_AstralTravel);


            //Version 11
            writer.Write(m_ResetSkillsCaps);
            
            //Version 10
            writer.Write((bool)m_GuildeChoisie);

            //Version 9

            //writer.Write((int)SalaireMerite);
            //writer.Write((int)SalarySum);
            //writer.Write((int)Salary);
            //writer.Write((string)EmployeeName);

            //Version 8

            writer.Write((int)Pen_Str);
            writer.Write((int)Pen_Dex);
            writer.Write((int)Pen_Int);
            writer.Write((int)Pen_MaxStr);
            writer.Write((int)Pen_MaxDex);
            writer.Write((int)Pen_MaxInt);
            writer.Write((bool)PenaliteMort);
            
            //Version 7

            writer.Write((string)m_Cote1_Par);
            writer.Write((string)m_Cote2_Par);
            writer.Write((string)m_Cote3_Par);
            writer.Write((string)m_Cote4_Par);

            //Version 6
            writer.Write((int)0);
            writer.Write((bool)m_AfficheTiefling);
            writer.Write((bool)false);

            //version 5
            writer.Write((short)m_CotesLimite);

            //Version 4
            writer.Write((bool)false);

            // Version 3
            writer.Write((int)m_Alignement);

            // Version 2
            writer.Write((DateTime)m_CoteTime);

            // Version 1 
            writer.Write((string)m_RealName);

			writer.Write( (int) m_Race );
			writer.Write( (int) m_Classe );
       
			writer.Write( (double) m_Experience );
			writer.Write( (double) m_ExperienceBesoin );
			writer.Write( m_Niveau );

			writer.Write( m_MaxStr );
			writer.Write( m_MaxDex );
			writer.Write( m_MaxInt );

			writer.Write( m_Derniere_Cote );
			writer.Write( m_Cote1 );
			writer.Write( m_Cote2 );
			writer.Write( m_Cote3 );
			writer.Write( m_Cote4 );
			writer.Write( m_CoteMoyenne );

			writer.Write( m_Karma );

			writer.Write( m_Acrobatie );
			writer.Write( m_Alchimie );
			writer.Write( m_Assassinat );
			writer.Write( m_Bardisme );
			writer.Write( m_Bricolage );
			writer.Write( m_Arcs );
			writer.Write( m_Crochetage );
			writer.Write( m_Armes_de_Melee  );
			writer.Write( m_Armes_a_Distance ); 
			writer.Write( m_Armes_de_Jets  );
			writer.Write( m_Lutte  );
			writer.Write( m_Couture ); 
			writer.Write( m_Deplacement_Silencieux );
			writer.Write( m_Enchantement );
			writer.Write( m_Endurance_Accrue );
			writer.Write( m_Erudition );
			writer.Write( m_Empathie_Animale );
			writer.Write( m_Ferraillerie );
			writer.Write( m_Furtivite );
			writer.Write( m_Herboristerie );
			writer.Write( m_Magie_arcanique );
			writer.Write( m_Sorcellerie );
			writer.Write( m_Necromancie );
			writer.Write( m_Magie_divine );
			writer.Write( m_Magie_naturelle );
			writer.Write( m_Medecine );
			writer.Write( m_Pose_de_Pieges );
			writer.Write( m_Rage );
			writer.Write( m_Sabotage );
			writer.Write( m_Telepathie );
			writer.Write( m_Vol_a_la_Tire );
            

			

		}
		#endregion

		#region Deserialize
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
            int version = reader.ReadInt();

            try
            {
                if (!WorldContext.RacePlayerMobiles.Contains(this))
                    WorldContext.RacePlayerMobiles.Add(this);

                TimerDeath = new EvoTimerDeath(this);
                TimerDeath.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            switch(version)
            {
                case 18:
                    {
                        goto case 17;
                    }
                case 17:
                    {
                        m_PAResetCount = reader.ReadShort();
                        goto case 16;
                    }
                case 16:
                    {
                        m_IsAddicted = reader.ReadBool();
                        int AddictionsCount = reader.ReadInt();
                        for (int i = 0; i < AddictionsCount; i++)
                        {
                            Type AddictionType = Type.GetType(reader.ReadString());
                            string AddictionName = reader.ReadString();
                            TimeSpan AddictionDelay = reader.ReadTimeSpan();
                            DateTime AddictionNext = reader.ReadDateTime();

                            PotionAddiction addiction = new PotionAddiction(AddictionName, AddictionType, AddictionDelay);
                            Addictions.Add(addiction, AddictionNext);
                        }
                        if (Addictions.Count > 0)
                        {
                            NextAddictionTimer = new AddictionTimer(this);
                            NextAddictionTimer.Start();
                        }
                        goto case 15;
                    }
                case 15:
                    {
                        m_AstralTravel = reader.ReadBool();
                        goto case 14;
                    }
                case 14:
                    {
                        // TODO: Remove for cleanup
                        if (version == 17)
                        {
                            int LastActionsCount = reader.ReadInt();
                            for (int i = 0; i < LastActionsCount; i++)
                            {
                                reader.ReadString();
                                reader.ReadDateTime();
                            }
                        
                            int KnowledgeCount = reader.ReadInt();
                            for (int i = 0; i < KnowledgeCount; i++)
                                reader.ReadInt();
                            reader.ReadInt();
                        }

                        goto case 11;
                    }
                case 11:
                    {
                        m_ResetSkillsCaps = reader.ReadBool();
                        goto case 10;
                    }
                case 10:
                    {
                        m_GuildeChoisie = reader.ReadBool();
                        goto case 9;
                    }
                case 9:
                    {
                        // TODO: Delete after cleanup
                        //reader.ReadInt();
                        //reader.ReadInt();
                        //reader.ReadInt();
                        //reader.ReadString();

                        goto case 8;
                    }
                case 8:
                    {
                        Pen_Str= reader.ReadInt();
                        Pen_Dex= reader.ReadInt();
                        Pen_Int= reader.ReadInt();
                        Pen_MaxStr = reader.ReadInt();
                        Pen_MaxDex = reader.ReadInt();
                        Pen_MaxInt = reader.ReadInt();

 

                        m_PenaliteMort = reader.ReadBool();

                        goto case 7;
                    }
                case 7:
                    {
                        m_Cote1_Par = reader.ReadString();
                        m_Cote2_Par = reader.ReadString();
                        m_Cote3_Par = reader.ReadString();
                        m_Cote4_Par = reader.ReadString();

                        goto case 6;
                    }
                case 6:
                    {
                        reader.ReadInt();
                        reader.ReadBool();
                        reader.ReadBool();
                        goto case 5;
                    }
                case 5:
                    {
                        m_CotesLimite = reader.ReadShort();
                        goto case 4;
                    }
               case 4:
                    {
                        reader.ReadBool();
                        goto case 3;
                    }
                case 3:
                    {
                        m_Alignement = (AlignementType)reader.ReadInt();
                        goto case 2;
                    }
                case 2:
                    {
                        m_CoteTime = (DateTime)reader.ReadDateTime();
                        goto case 1;
                    }
                case 1 :
                    {
                        m_RealName = (string)reader.ReadString();


                        m_Race = (RaceType)reader.ReadInt();
                        m_Classe = (ClasseType)reader.ReadInt();


                        m_Experience = reader.ReadDouble();
                        m_ExperienceBesoin = reader.ReadDouble();
                        m_Niveau = reader.ReadInt();

                        m_MaxStr = reader.ReadInt();
                        m_MaxDex = reader.ReadInt();
                        m_MaxInt = reader.ReadInt();

                        m_Derniere_Cote = reader.ReadShort();
                        m_Cote1 = reader.ReadShort();
                        m_Cote2 = reader.ReadShort();
                        m_Cote3 = reader.ReadShort();
                        m_Cote4 = reader.ReadShort();
                        m_CoteMoyenne = reader.ReadShort();

                        m_Karma = reader.ReadShort();


                        m_Acrobatie = reader.ReadShort();
                        m_Alchimie = reader.ReadShort();
                        m_Assassinat = reader.ReadShort();
                        m_Bardisme = reader.ReadShort();
                        m_Bricolage = reader.ReadShort();
                        m_Arcs = reader.ReadShort();
                        m_Crochetage = reader.ReadShort();
                        m_Armes_de_Melee = reader.ReadShort();
                        m_Armes_a_Distance = reader.ReadShort();
                        m_Armes_de_Jets = reader.ReadShort();
                        m_Lutte = reader.ReadShort();
                        m_Couture = reader.ReadShort();
                        m_Deplacement_Silencieux = reader.ReadShort();
                        m_Enchantement = reader.ReadShort();
                        m_Endurance_Accrue = reader.ReadShort();
                        m_Erudition = reader.ReadShort();
                        m_Empathie_Animale = reader.ReadShort();
                        m_Ferraillerie = reader.ReadShort();
                        m_Furtivite = reader.ReadShort();
                        m_Herboristerie = reader.ReadShort();
                        m_Magie_arcanique = reader.ReadShort();
                        m_Sorcellerie = reader.ReadShort();
                        m_Necromancie = reader.ReadShort();
                        m_Magie_divine = reader.ReadShort();
                        m_Magie_naturelle = reader.ReadShort();
                        m_Medecine = reader.ReadShort();
                        m_Pose_de_Pieges = reader.ReadShort();
                        m_Rage = reader.ReadShort();
                        m_Sabotage = reader.ReadShort();
                        m_Telepathie = reader.ReadShort();
                        m_Vol_a_la_Tire = reader.ReadShort();
                        

                        break;
                    }
            }
		}

        #endregion

        #region Fonction On Death
        public override bool OnBeforeDeath()
		{
			
			BaseMount.Dismount(this);
            
			if (this.EvolutionInfo.DestinyPoints == 0)
			{
				this.SendMessage("Vous �tes mort...");
                this.EvolutionInfo.DestinyPoints = 1;
				return true;
			}
			else
			{
                this.EvolutionInfo.DestinyPoints--;
				this.SendMessage("Vous �tes inconscient");
				this.Hidden = true;
				this.Frozen=true;
				this.Squelched=true;
				m_Blessed=this.Blessed;
				this.Blessed=true;
				InconscientBody body = new InconscientBody(this, m_Blessed);
				body.Map=this.Map;
				body.Location=this.Location;
				m_Body=body;
				//this.Z-=100;
				m_Body.AddItem(new Shirt());
				CopyFromLayer(this, m_Body, Layer.FirstValid);
				CopyFromLayer(this, m_Body, Layer.TwoHanded);
				CopyFromLayer(this, m_Body, Layer.Shoes);
				CopyFromLayer(this, m_Body, Layer.Pants);
				CopyFromLayer(this, m_Body, Layer.Shirt);
				CopyFromLayer(this, m_Body, Layer.Helm);
				CopyFromLayer(this, m_Body, Layer.Gloves);
				CopyFromLayer(this, m_Body, Layer.Ring);
				CopyFromLayer(this, m_Body, Layer.Neck);
				CopyFromLayer(this, m_Body, Layer.Hair);
				CopyFromLayer(this, m_Body, Layer.Waist);
				CopyFromLayer(this, m_Body, Layer.InnerTorso);
				CopyFromLayer(this, m_Body, Layer.Bracelet);
				CopyFromLayer(this, m_Body, Layer.FacialHair);
				CopyFromLayer(this, m_Body, Layer.MiddleTorso);
				CopyFromLayer(this, m_Body, Layer.Earrings);
				CopyFromLayer(this, m_Body, Layer.Arms);
				CopyFromLayer(this, m_Body, Layer.Cloak);
				CopyFromLayer(this, m_Body, Layer.OuterTorso);
				CopyFromLayer(this, m_Body, Layer.OuterLegs);
				CopyFromLayer(this, m_Body, Layer.LastUserValid);
				
				
				RemoveTimer( this );
				TimeSpan duration = TimeSpan.FromSeconds( 60 ); // 60 secondes
				Timer t = new InternalTimer( this, duration, m_Body, m_Blessed );
				m_Table[this] = t;
				t.Start();
				return false;
			}

		}

        public override void OnBeforeResurrect()
        {
            if (AstralTravel)
            {
                MindAstralTravel.StopTimer(this);
            }
            base.OnBeforeResurrect();
        }


		public override void OnDeath( Container c )
		{
			
			//base.OnDeath(c);
			//this.Corpse.Delete();
			if ( this.LastKiller is RacePlayerMobile ) // si le tueur est un raceplayermobile
			{
                //int chance = Utility.Random(0, 10);
                //if (chance >= 1)
                //{
                    if (PenaliteMort == false) PenaliteMort = true;
                //}
			}
			else  //tu� par un monstreuh ou autre
			{

                //int chance = Utility.Random(0, 10);
                //if (chance >= 1)
                //{
                    if(PenaliteMort == false) PenaliteMort = true;
                //}
			}
			
			base.OnDeath(c);
			
			

		}
		
		
		
		private void CopyFromLayer( Mobile from, InconscientBody sleeping, Layer layer ) 
		{
			Item worn = from.FindItemOnLayer(layer);
			if (worn != null)
			{
     		
				Item cloth = new Item(); 
				cloth.ItemID=worn.ItemID;
				cloth.Hue = worn.Hue;
				cloth.Layer=layer;
				cloth.ItemID = worn.ItemID;
    
				sleeping.AddItem(cloth); 
			}
      	
     
		}
		private static Hashtable m_Table = new Hashtable();

		public static void RemoveTimer( Mobile m )
		{
			Timer t = (Timer)m_Table[m];

			if ( t != null )
			{
				t.Stop();
				m_Table.Remove( m );
			}
		}

		private class InternalTimer : Timer
		{
			private Mobile m_Mobile;
			private Item m_Body;
			private bool m_Blessed;

			public InternalTimer( Mobile m, TimeSpan duration, Item body, bool blessed ) : base( duration )
			{
				m_Mobile = m;
				m_Body=body;
			}

			protected override void OnTick()
			{
				m_Mobile.RevealingAction();
				m_Mobile.Frozen=false;
				m_Mobile.Squelched=false;
				m_Mobile.Blessed=m_Blessed;
				
			
				if(m_Body!=null)
				{
					m_Body.Delete();
					m_Mobile.SendMessage("Vous reprenez conscience!");
					m_Mobile.Z=m_Body.Z;
					m_Mobile.Animate(21, 6, 1, false, false, 0);
                    ((RacePlayerMobile)m_Mobile).Sick = false;
                    m_Mobile.Hits = 20;
                    m_Mobile.Stam = 30;
				}
				RemoveTimer( m_Mobile );
			}
		}

#endregion

		#region Fonction ObjectPropertyList

        public override void AddNameProperties(ObjectPropertyList list)
        {
            string name = this.Name;
            if (name == null)
            {
                name = string.Empty;
            }
            list.Add(0x1005bd, "{0} \t{1}\t {2}", "", name, "");
        }

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

		}

        public override void SetName(int v_Serial, string v_Nom, Mobile from)
        {
            base.SetName(v_Serial, v_Nom, from);
            SendPropertiesTo( from );
        }

        public override void SendPropertiesTo( Mobile from )
		{
			
			string race_texte     = Races.GetRaceName(this);
			string age_texte      = FormatHelper.GetAgeDescription(this.EvolutionInfo.Race, this.Female, this.EvolutionInfo.Age);
            string guilde_texte   = GuildDescriptions.GetGuildDescription(this.GuildInfo.PlayerGuild);
            string guilde_title   = "";
            string noblesse_texte = "";
            guilde_title = this.GuildTitle;

            #region Noblesse
            if (this.GuildInfo.ShowNoble)
            {
                if (!this.Female)
                {
                    switch (this.GuildInfo.NobleType)
                    {
                        case NobleType.Citoyen:
                            noblesse_texte = "\n[Citoyen]";
                            break;
                        case NobleType.Ecuyer:
                            noblesse_texte = "\n[�cuyer]";
                            break;
                        case NobleType.Chevalier:
                            noblesse_texte = "\n[Chevalier]";
                            break;
                        case NobleType.Duc:
                            noblesse_texte = "\n[Duc]";
                            break;
                        case NobleType.Marquis:
                            noblesse_texte = "\n[Marquis]";
                            break;
                        case NobleType.Baron:
                            noblesse_texte = "\n[Baron]";
                            break;
                        case NobleType.Comte:
                            noblesse_texte = "\n[Comte]";
                            break;
                        case NobleType.Custom:
                            noblesse_texte = "\n[" + Title + "]";
                        break;
                        default :
                            noblesse_texte = "\n[Citoyen]";
                        break;
                    }
                }
                else
                {
                    switch (this.GuildInfo.NobleType)
                    {
                        case NobleType.Citoyen:
                            noblesse_texte = "\n[Citoyenne]";
                            break;
                        case NobleType.Ecuyer:
                            noblesse_texte = "\n[�cuy�re]";
                            break;
                        case NobleType.Chevalier:
                            noblesse_texte = "\n[Chevali�re]";
                            break;
                        case NobleType.Duc:
                            noblesse_texte = "\n[Duchesse]";
                            break;
                        case NobleType.Marquis:
                            noblesse_texte = "\n[Marquise]";
                            break;
                        case NobleType.Baron:
                            noblesse_texte = "\n[Baronne]";
                            break;
                        case NobleType.Comte:
                            noblesse_texte = "\n[Comtesse]";
                            break;
                        case NobleType.Custom:
                            noblesse_texte = "\n[" + Title + "]";
                        break;
                        default:
                            noblesse_texte = "\n[Citoyenne]";
                        break;
                    }
                }
            }
            #endregion
            ObjectPropertyList opl = new ObjectPropertyList(this); 
            
            string name = Name;
            if (this.AccessLevel == AccessLevel.Player)
            {
                if (this.EvolutionInfo.Age != 0)
                {
                    if( from is RacePlayerMobile)
                    {
                        RacePlayerMobile pmf = (RacePlayerMobile)from;

                        if (pmf.AccessLevel > AccessLevel.Player) // GM is observing the player
                        {
                            if (guilde_title != null)
                            {
                                if (UseIdentity == 0) opl.Add(1050045, "{0} \t{1}\t {2}", "", this.PlayerName, ""); 
                                if (UseIdentity == 1) opl.Add(1050045, "{0} \t{1}\t {2}", "", this.SecondIdentity, ""); 
                                opl.Add("[" + race_texte + ", " + age_texte + "]\n[" + guilde_title + ", " + guilde_texte + "]" + noblesse_texte);
                            }
                            else
                            {
                                if (UseIdentity == 0) opl.Add(1050045, "{0} \t{1}\t {2}", "", this.PlayerName, "");
                                if (UseIdentity == 1) opl.Add(1050045, "{0} \t{1}\t {2}", "", this.SecondIdentity, ""); 
                                opl.Add("[" + race_texte + ", " + age_texte + "]\n[" + guilde_texte + "]" + noblesse_texte);
                            }
                        }
                        else
                        {
                            if (pmf != this) // Another player
                            {
                                name = this.FindName(from.Serial.Value);
                            }
                            else // Ourselves
                            {
                                if (UseIdentity == 0) name = PlayerName;
                                if (UseIdentity == 1) name = SecondIdentity;
                            }

                            if (name == "-1")
                            {
                                name = this.Female ? "Inconnue" : "Inconnu";
                            }

                            var firstTag = String.Format("{0}, {1}", race_texte, age_texte);
                            if (this.GuildInfo.ShowRace == false) firstTag = String.Format("{0}", race_texte);

                            if (this.GuildInfo.ShowGuild)
                            {
                                if (guilde_title != null)
                                {
                                    opl.Add(1050045, "{0} \t{1}\t {2}", "", name, ""); // ~1_PREFIX~~2_NAME~~3_SUFFIX~
                                    opl.Add("[" + firstTag + "]\n[" + guilde_title + ", " + guilde_texte + "]" + noblesse_texte);
                                }
                                else
                                {
                                    opl.Add(1050045, "{0} \t{1}\t {2}", "", name, ""); // ~1_PREFIX~~2_NAME~~3_SUFFIX~
                                    opl.Add("[" + firstTag + "]\n[" + guilde_texte + "]" + noblesse_texte);
                                }
                                }
                            else
                            {
                                opl.Add(1050045, "{0} \t{1}\t {2}", "", name, ""); // ~1_PREFIX~~2_NAME~~3_SUFFIX~
                                opl.Add("[" + firstTag + "]" + noblesse_texte);
                            }
                        }
                    }
                }
                else
                {
                    //opl.Add( name+"");
                    if (this.Female)
                        opl.Add(1050045, "{0} \t{1}\t {2}", "", "Inconnue", ""); // ~1_PREFIX~~2_NAME~~3_SUFFIX~
                    else
                        opl.Add(1050045, "{0} \t{1}\t {2}", "", "Inconnu", ""); // ~1_PREFIX~~2_NAME~~3_SUFFIX~
                }
            }
            else // if GM
            {
                opl.Add(1050045, "{0} \t{1}\t {2}", "", this.PlayerName.ToString(), ""); // ~1_PREFIX~~2_NAME~~3_SUFFIX~   
            }

            


			from.Send( opl );

			//base.SendPropertiesTo( from );
		}
       

        private static ArrayList m_Hears;
        private static ArrayList m_OnSpeech;

        private static bool m_NoSpeechLOS1;
        [CommandProperty(AccessLevel.GameMaster)]
        public static bool NoSpeechLOS1 { get { return m_NoSpeechLOS1; } set { m_NoSpeechLOS1 = value; } }

        public override bool CanSee(Mobile m)
        {
            //SendPropertiesTo(this);
            //SendPropertiesTo(m);
            return base.CanSee(m);
        }
        public override void DoSpeech(string text, int[] keywords, MessageType type, int hue)
        {
           if ( Commands.Handle(this, text))
                return;

            int range = 12;
            var rage = ClassInfo.Capacities[CapacityName.Rage].Value;

            if ((int)(rage / 5) > 0)
            {
                range = 12 + ((int)(rage / 5));
            }
            

            switch (type)
            {
                case MessageType.Regular: this.SpeechHue = hue; break;
                case MessageType.Emote: this.EmoteHue = hue; break;
                case MessageType.Whisper: this.WhisperHue = hue; range = 2; break;
                case MessageType.Yell: 
                    this.YellHue = hue; range = 18;
                    if ((int)(rage / 5) > 0)
                    {
                        range = 18 + ((int)(rage / 5));
                    }
                break;
                default: type = MessageType.Regular; break;
            }

            SpeechEventArgs regArgs = new SpeechEventArgs(this, text, type, hue, keywords);

            EventSink.InvokeSpeech(regArgs);
            this.Region.OnSpeech(regArgs);
            OnSaid(regArgs);

            if (regArgs.Blocked)
                return;

            text = regArgs.Speech;

            if (text == null || text.Length == 0)
                return;

            if (m_Hears == null)
                m_Hears = new ArrayList();
            else if (m_Hears.Count > 0)
                m_Hears.Clear();

            if (m_OnSpeech == null)
                m_OnSpeech = new ArrayList();
            else if (m_OnSpeech.Count > 0)
                m_OnSpeech.Clear();

            ArrayList hears = m_Hears;
            ArrayList onSpeech = m_OnSpeech;

            if (this.Map != null)
            {
                IPooledEnumerable eable = this.Map.GetObjectsInRange(this.Location, range);

                foreach (object o in eable)
                {
                    if (o is Mobile)
                    {
                        Mobile heard = (Mobile)o;

                        if (heard.CanSee(this) && (m_NoSpeechLOS1 ||  !heard.Player || heard.InLOS(this)))
                        {
                            if (heard.NetState != null)
                                hears.Add(heard);

                            if (heard.HandlesOnSpeech(this))
                                onSpeech.Add(heard);

                            for (int i = 0; i < heard.Items.Count; ++i)
                            {
                                Item item = (Item)heard.Items[i];

                                if (item.HandlesOnSpeech)
                                    onSpeech.Add(item);

                                //if (item is Container)
                                //    AddSpeechItemsFrom(onSpeech, (Container)item);
                            }
                        }
                    }
                    else if (o is Item)
                    {
                        if (((Item)o).HandlesOnSpeech)
                            onSpeech.Add(o);

                        //if (o is Container)
                        //    AddSpeechItemsFrom(onSpeech, (Container)o);
                    }
                }

                //eable.Free();

                object mutateContext = null;
                string mutatedText = text;
                SpeechEventArgs mutatedArgs = null;

                if (MutateSpeech(hears, ref mutatedText, ref mutateContext))
                    mutatedArgs = new SpeechEventArgs(this, mutatedText, type, hue, new int[0]);

                CheckSpeechManifest();

                ProcessDelta();

                Packet regp = null;
                Packet mutp = null;

                for (int i = 0; i < hears.Count; ++i)
                {
                    Mobile heard = (Mobile)hears[i];
                    //SendPropertiesTo(heard);

                    
                    
                    if (mutatedArgs == null || !CheckHearsMutatedSpeech(heard, mutateContext))
                    {
                        heard.OnSpeech(regArgs);

                        NetState ns = heard.NetState;

                        if (ns != null)
                        {

                            string name = Name;

                            // To Self and GMs, send always the good name
                            if (this == heard || heard.AccessLevel >= AccessLevel.Counselor)
                            {
                                name = CurrentPlayerName;
                            }
                            else if (this.Incognito == 1)
                            {
                                name = this.Female ? "Inconnue" : "Inconnu";
                            }
                            else if (this.AccessLevel == AccessLevel.Player)
                            {
                                name = this.GetKnownName(heard);
                            }

                            if (type == MessageType.Yell)
                            {
                                regp = new UnicodeMessage(this.Serial, Body, type, this.EmoteHue, 3, this.Language, name, "*crie*");
                                regp = new UnicodeMessage(this.Serial, Body, type, hue, 3, this.Language, name, text);
                            }
                            else if (type == MessageType.Whisper)
                            {
                                regp = new UnicodeMessage(this.Serial, Body, type, this.EmoteHue, 3, this.Language, name, "*murmure*");
                                regp = new UnicodeMessage(this.Serial, Body, type, hue, 3, this.Language, name, text);
                            }
                            else
                                regp = new UnicodeMessage(this.Serial, Body, type, hue, 3, this.Language, name, text);

                            ns.Send(regp);
                        }
                    }
                    else
                    {
                        heard.OnSpeech(mutatedArgs);

                        NetState ns = heard.NetState;

                        if (ns != null)
                        {
                            //if (mutp == null)
                           // {
                            string name = Name;
                            if (this.AccessLevel == AccessLevel.Player)
                            {
                                name = this.GetKnownName(heard);
                            }
                            mutp = new UnicodeMessage(this.Serial, Body, type, hue, 3, this.Language, name, mutatedText);
                           // }
                            ns.Send(mutp);
                        }
                    }
                }

                //if (onSpeech.Count > 1)
               //     onSpeech.Sort(LocationComparer.GetInstance(this));

                for (int i = 0; i < onSpeech.Count; ++i)
                {
                    object obj = onSpeech[i];

                    if (obj is Mobile)
                    {
                        Mobile heard = (Mobile)obj;

                        if (mutatedArgs == null || !CheckHearsMutatedSpeech(heard, mutateContext))
                            heard.OnSpeech(regArgs);
                        else
                            heard.OnSpeech(mutatedArgs);
                    }
                    else
                    {
                        Item item = (Item)obj;

                        item.OnSpeech(regArgs);
                    }
                }
            }
            
       
        }

		#endregion

        #region Fonction OnDelete
        public override void OnDelete()
        {
            base.OnDelete();
            foreach (Mobile m in World.Mobiles.Values)
            {
                if (m is RacePlayerMobile)
                {
                    RacePlayerMobile pm = m as RacePlayerMobile;
                    pm.EraseName(this.Serial.Value);
                }
            }
            WorldContext.RacePlayerMobiles.Remove(this);
        }

        #endregion

        #region Fonction OnSingleClick
        public override void OnAosSingleClick(Mobile from)
        {
            OnSingleClick(from);
        }

        public override void OnSingleClick(Mobile from)
        {
            if (GuildInfo.ShowName)
            {
                if (!this.Deleted && (((this.AccessLevel != Server.AccessLevel.Player) || !DisableHiddenSelfClick) || (!this.Hidden || (from != this))))
                {
                    int nameHue;
                    string name;

                    // Set the name hue
                    if (!this.GuildInfo.ShowGuild)
                        nameHue = 0x59;
                    else if (this.NameHue != -1)
                        nameHue = this.NameHue;
                    else if (this.AccessLevel > Server.AccessLevel.Player)
                        nameHue = 11;
                    else
                        nameHue = Notoriety.GetHue(Notoriety.Compute(from, this));

                    // Check the name
                    if (this.Incognito == 1)
                        name = "Inconnu";
                    else if (!this.GuildInfo.ShowRace && this.GuildInfo.ShowName)
                    {
                        name = Races.GetRaceName(this);
                    }
                    else
                    {
                        name = this.FindName(from.Serial.Value);
                    }
                    if (name == null)
                        name = string.Empty;

                    // Always send the good name
                    if (from.AccessLevel > AccessLevel.Counselor)
                    {
                        if (UseIdentity == 0) name = this.PlayerName;
                        else name = this.SecondIdentity;
                    }

                    // Send the packet
                    this.PrivateOverheadMessage(MessageType.Label, nameHue, AsciiClickMessage, name, from.NetState);
                }
                SendPropertiesTo(from);
            }
        }

        public override void DisplayPaperdollTo(Mobile to)
        {
            string oldname = Name;
            Name = GetKnownName(to);
            EventSink.InvokePaperdollRequest(new PaperdollRequestEventArgs(to, this));
            Name = oldname;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// R�cup�re le nom connu par un joueur X
        /// </summary>
        public virtual string GetKnownName1(RacePlayerMobile mobile)
        {
            Serial mSerial = mobile.Serial;
            foreach (NameAndSerial NAS in NomListe)
                if (NAS.Serial == mSerial) return NAS.Name;
            return "Inconnu";
        }

        /// <summary>
        /// R�cup�re le nom secondaire connu par un joueur X
        /// </summary>
        public virtual string GetKnownName2(RacePlayerMobile mobile)
        {
            Serial mSerial = mobile.Serial;
            foreach (NameAndSerial NAS in NomListe2)
                if (NAS.Serial == mSerial) return NAS.Name;
            return "Inconnu";
        }

        /// <summary>
        /// R�cup�re un nom connu suivant l'id�ntit� pr�cis�e
        /// </summary>
        public virtual string GetKnownName1(RacePlayerMobile mobile, IdentityType identity)
        {
            if (identity == IdentityType.First) return GetKnownName1(mobile);
            else return GetKnownName2(mobile);
        }

        /// <summary>
        /// R�cup�re un nom connu suivant l'id�ntit� actuelle
        /// </summary>
        public virtual string GetKnownName(RacePlayerMobile mobile)
        {
            if (this == mobile || mobile.AccessLevel > AccessLevel.Counselor)
            {
                return CurrentPlayerName;
            }
            if (this.Incognito == 0)
            {
                if (UseIdentity == 0) return GetKnownName1(mobile);
                else if (UseIdentity == 1) return GetKnownName2(mobile);
            }
            return this.Female ? "Inconnue" : "Inconnu";
        }

        /// <summary>
        /// R�cup�re un nom connu suivant l'id�ntit� actuelle
        /// </summary>
        public override string GetKnownName(Mobile m)
        {
            if (m is RacePlayerMobile)
            {
                return GetKnownName(m as RacePlayerMobile);
            }
            else
            {
                return this.Female ? "Inconnue" : "Inconnu";
            }
        }

        /// <summary>
        /// Force le personnage � vomir
        /// </summary>
        public void DoPuke()
        {
            this.PlaySound(this.Female ? 813 : 1087);
            this.Emote("*vomit*");
            if (!this.Mounted)
                this.Animate(32, 5, 1, true, false, 0);
            Point3D p = new Point3D(this.Location);
            switch (this.Direction)
            {
                case Direction.North:
                    p.Y--; break;
                case Direction.South:
                    p.Y++; break;
                case Direction.East:
                    p.X++; break;
                case Direction.West:
                    p.X--; break;
                case Direction.Right:
                    p.X++; p.Y--; break;
                case Direction.Down:
                    p.X++; p.Y++; break;
                case Direction.Left:
                    p.X--; p.Y++; break;
                case Direction.Up:
                    p.X--; p.Y--; break;
                default:
                    break;
            }
            p.Z = this.Map.GetAverageZ(p.X, p.Y);

            Puke puke = new Puke();
            puke.Map = this.Map;
            puke.Location = p; 
        }
        #endregion

        #region Addictions

        public void AddAddiction(PotionAddiction addiction)
        {
            Addictions.Add(addiction, DateTime.Now + addiction.Delay);
            if (Addictions.Count > 0 && NextAddictionTimer == null)
            {
                NextAddictionTimer = new AddictionTimer(this);
                NextAddictionTimer.Start();
            }
        }

        public void RemoveAddictions()
        {
            Addictions.Clear();
            if (NextAddictionTimer != null)
            {
                NextAddictionTimer.Stop();
                NextAddictionTimer = null;
            }
            if (this.IsAddicted)
                IsAddicted = false;
        }

        public void RestartAddictionsTimer()
        {
            if (NextAddictionTimer != null)
            {
                NextAddictionTimer.Stop();
                NextAddictionTimer = null;
            }
            if (Addictions.Count > 0)
            {
                NextAddictionTimer = new AddictionTimer(this);
                NextAddictionTimer.Start();
            }
        }

        private class AddictionTimer : Timer
        {
            private RacePlayerMobile Owner;
            private PotionAddiction CurrentAddiction;

            public AddictionTimer(RacePlayerMobile owner) : base(TimeSpan.FromSeconds(0))
            {
                Owner = owner;
                //Get next
                DateTime NextTime = new DateTime(1,1,1);
                PotionAddiction NextAddiction = null;
                foreach(PotionAddiction addiction in Owner.Addictions.Keys)
                {
                    if (NextAddiction == null)
                    { // 1st
                        NextAddiction = addiction;
                        NextTime = Owner.Addictions[addiction];
                        if (NextTime < DateTime.Now)
                        {
                            NextTime = NextTime + addiction.Delay;
                            if (NextTime < DateTime.Now)
                            {
                                NextTime = DateTime.Now.AddSeconds(2);
                            }
                        }
                    }
                    else
                    { // Other
                        DateTime OtherNextTime = Owner.Addictions[addiction];
                        if (OtherNextTime < DateTime.Now)
                        {
                            OtherNextTime = OtherNextTime + addiction.Delay;
                            if (OtherNextTime < DateTime.Now)
                            {
                                OtherNextTime = DateTime.Now.AddSeconds(2);
                            }
                        }
                        if (OtherNextTime < NextTime) 
                        { // if sooner
                            NextAddiction = addiction;
                            NextTime = OtherNextTime;
                        }
                    }
                }

                if (NextAddiction != null) CurrentAddiction = NextAddiction;

                Delay = NextTime - DateTime.Now; // until next
                Priority = TimerPriority.TwoFiftyMS;
            }

            

            protected override void OnTick()
            {
                // do harm
                Owner.SendMessage("Vous �tes en manque de " + CurrentAddiction.Name + " ...");
                Owner.DoPuke();
                Owner.IsAddicted = true;

                // readapt delay
                Owner.Addictions[CurrentAddiction] = Owner.Addictions[CurrentAddiction] + CurrentAddiction.Delay;

                // launch new timer
                Owner.RestartAddictionsTimer();
            }
        }
        #endregion

    }
}
