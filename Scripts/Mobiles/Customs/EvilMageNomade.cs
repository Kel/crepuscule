using System;
using Server.Misc;
using Server.Network;
using System.Collections;
using Server.Items;
using Server.Targeting;

namespace Server.Mobiles
{
	public class EvilMageNomade : BaseCreature
	{






		private static bool m_Talked;
		string[] NomadeMaSay = new string[]
		{
	   "*Incante un sort*",
            "Mahallah, bahlam chakram!",
            "Mashakram, allahm, vas manim! ",
		};






		public override bool ShowFameTitle{ get{ return false; } }

		[Constructable]
		public EvilMageNomade():base( AIType.AI_Mage, FightMode.Closest, 10, 1, 0.2, 0.4 )
		{
			Body = 0x190;
			Name = "Nomade Mage";
			Hue = 643;

			this.InitStats(Utility.Random(359,399), Utility.Random(138,151), Utility.Random(760,970));

			this.Skills[SkillName.Wrestling].Base = Utility.Random(74,80);
			this.Skills[SkillName.Magery].Base = Utility.Random(90,95);
			this.Skills[SkillName.Anatomy].Base = Utility.Random(120,125);
			this.Skills[SkillName.MagicResist].Base = Utility.Random(90,94);
			this.Skills[SkillName.EvalInt].Base = Utility.Random(90,95);

			this.Fame = Utility.Random(5000,9999);
			this.Karma = Utility.Random(-5000,-9999);
			this.VirtualArmor = 40;

		

			AddItem( new BoneGlovesN() );
			AddItem( new FoulardNEvil());
			AddItem( new RobeNEvil());
			AddItem( new TrenchNEvil());
			AddItem( new TurbanNomadeE());
			AddItem( new shoesNomadeM());
			AddItem( new Spellbook());

			PackItem( new BagOfReagents(3) );


			
		}



public override void OnMovement( Mobile m, Point3D oldLocation )
		{
			if( m_Talked == false )
			{
				if ( m.InRange( this, 3 ) && m is PlayerMobile)
				{
					m_Talked = true;
					SayRandom( NomadeMaSay, this );
					this.Move( GetDirectionTo( m.Location ) );
					SpamTimer t = new SpamTimer();
					t.Start();
				}
			}
		}

		private class SpamTimer : Timer
		{
			public SpamTimer() : base( TimeSpan.FromSeconds( 7 ) )
			{
				Priority = TimerPriority.OneSecond;
			}

			protected override void OnTick()
			{
				m_Talked = false;
			}
		}

		private static void SayRandom( string[] say, Mobile m )
		{
			m.Say( say[Utility.Random( say.Length )] );
		}











		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool Unprovokable{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Deadly; } }

		[CommandProperty( AccessLevel.GameMaster )]
		public override int HitsMax { get { return 769; } }

		public EvilMageNomade( Serial serial ) : base( serial )
		{
		}

		public override bool OnBeforeDeath()
		{
			Lich rm = new Lich();

			rm.Team = this.Team;
			rm.MoveToWorld( this.Location, this.Map );

			Effects.SendLocationEffect( Location,Map, 0x3709, 13, 0x3B2, 0 );

			Container bag = new Bag();
			switch ( Utility.Random( 9 ))
			{
				case 0: bag.DropItem( new Amber() ); break;
				case 1: bag.DropItem( new Amethyst() ); break;
				case 2: bag.DropItem( new Citrine() ); break;
				case 3: bag.DropItem( new Diamond() ); break;
				case 4: bag.DropItem( new Emerald() ); break;
				case 5: bag.DropItem( new Ruby() ); break;
				case 6: bag.DropItem( new Sapphire() ); break;
				case 7: bag.DropItem( new StarSapphire() ); break;
				case 8: bag.DropItem( new Tourmaline() ); break;
			}

			switch ( Utility.Random( 8 ))
			{
				case 0: bag.DropItem( new SpidersSilk( 3 ) ); break;
				case 1: bag.DropItem( new Ossature( 1 ) ); break;
				case 2: bag.DropItem( new Bloodmoss( 3 ) ); break;
				case 3: bag.DropItem( new Garlic( 3 ) ); break;
				case 4: bag.DropItem( new MandrakeRoot( 3 ) ); break;
				case 5: bag.DropItem( new Ossature( 1 ) ); break;
				case 6: bag.DropItem( new SulfurousAsh( 3 ) ); break;
				case 7: bag.DropItem( new Ginseng( 3 ) ); break;
			}

			bag.DropItem( new Gold( 0, 0 ));
			rm.AddItem( bag );

			this.Delete();

			return false;
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}