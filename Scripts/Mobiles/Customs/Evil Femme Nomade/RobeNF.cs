using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x302F, 0x302F )]
	public class RobeNF : BaseWaist
	{

		[Constructable]
		public RobeNF() : base( 0x302F )
		{
			Weight = 1.0;
			Layer = Layer.InnerTorso;
			Name = "Robe Gitanne";
			Hue = 2166;
			
			Movable = false;
		}

		public RobeNF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}