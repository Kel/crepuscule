using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3108, 0x3108 )]
	public class PiedsNF : BaseWaist
	{

		[Constructable]
		public PiedsNF() : base( 0x3108 )
		{
			Weight = 1.0;
			Layer = Layer.Shoes;
			Name = "Bandages";
			Hue= 448;
			
			Movable = false;
		}

		public PiedsNF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}