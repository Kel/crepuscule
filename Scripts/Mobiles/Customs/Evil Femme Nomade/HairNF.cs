using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x35b6 , 0x35b6 )]
	public class HairNF : BaseWaist
	{

		[Constructable]
		public HairNF() : base( 0x35b6 )
		{
			Weight = 1.0;
			Layer = Layer.Helm;
			Name = "Cheveux";
			Hue = 1945;
			
			Movable = false;
		}

		public HairNF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}