using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3103, 0x3103 )]
	public class WaistNF : BaseWaist
	{

		[Constructable]
		public WaistNF() : base( 0x3103 )
		{
			Weight = 1.0;
			Layer = Layer.Waist;
			Name = "Bourse";
			Hue = 448;
			
			Movable = false;
		}

		public WaistNF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}