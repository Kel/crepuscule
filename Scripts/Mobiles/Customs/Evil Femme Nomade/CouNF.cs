using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30f6, 0x30f6 )]
	public class CouNF : BaseWaist
	{

		[Constructable]
		public CouNF() : base( 0x30f6 )
		{
			Weight = 1.0;
			Layer = Layer.Neck;
			Name = "Foulard";
			Hue = 448;
			
			Movable = false;
		}

		public CouNF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}