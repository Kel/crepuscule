using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30bd, 0x30bd )]
	public class EarringNomadeF : BaseWaist
	{

		[Constructable]
		public EarringNomadeF() : base( 0x30bd )
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Name = "Voile";
			Hue = 1701;
		}

		public EarringNomadeF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}