using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3109, 0x3109 )]
	public class BadageNomade : BaseWaist
	{

		[Constructable]
		public BadageNomade() : base( 0x3109 )
		{
			Weight = 1.0;
			Layer = Layer.InnerTorso;
			Name = "Bandage";
			Hue = 1701;
		}

		public BadageNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}