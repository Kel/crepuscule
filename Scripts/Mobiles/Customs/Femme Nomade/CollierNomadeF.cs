using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30b6, 0x30b6 )]
	public class CollierNomadeF : BaseWaist
	{

		[Constructable]
		public CollierNomadeF() : base( 0x30b6 )
		{
			Weight = 1.0;
			Layer = Layer.Neck;
			Name = "Collier";
			Hue = 1701;
		}

		public CollierNomadeF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}