using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3107, 0x3107 )]
	public class BracerNomadeF : BaseWaist
	{

		[Constructable]
		public BracerNomadeF() : base( 0x3107 )
		{
			Weight = 1.0;
			Layer = Layer.Bracelet;
			Name = "Bandages";
			Hue = 1816; 
		}

		public BracerNomadeF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}