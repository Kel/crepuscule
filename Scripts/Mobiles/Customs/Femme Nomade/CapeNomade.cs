using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x1515, 0x1515 )]
	public class CapeNomade : BaseWaist
	{

		[Constructable]
		public CapeNomade() : base( 0x1515 )
		{
			Weight = 1.0;
			Layer = Layer.Cloak;
			Name = "Cape Nomade";
			Hue = 1701;
		}

		public CapeNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}