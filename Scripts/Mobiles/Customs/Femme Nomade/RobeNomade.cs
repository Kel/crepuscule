using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x35fd, 0x35fd )]
	public class RobeNomade : BaseWaist
	{

		[Constructable]
		public RobeNomade() : base( 0x35fd )
		{
			Weight = 1.0;
			Layer = Layer.Waist;
			Name = "Robe Nomade";
			Hue = 1816;
		}

		public RobeNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}