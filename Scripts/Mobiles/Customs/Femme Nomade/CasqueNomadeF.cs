using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x302E, 0x302E )]
	public class CasqueNomadeF : BaseWaist
	{

		[Constructable]
		public CasqueNomadeF() : base( 0x302E )
		{
			Weight = 1.0;
			Layer = Layer.Helm;
			Name = "Foulard";
			Hue = 1701;
		}

		public CasqueNomadeF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}