using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	public class NomadeM : BaseVendor
	{
		private ArrayList m_SBInfos = new ArrayList();
		protected override ArrayList SBInfos{ get { return m_SBInfos; } }

		[Constructable]
		public NomadeM() : base( "Le Nomade" )
		{
			SetSkill( SkillName.ItemID, 60.0, 83.0 );
			Hue = 643;
			Body = 0x190;
				Name = NameList.RandomName( "male" );
		}


		public override void InitOutfit()
		{
			AddItem( new BraceletNomM( ) );
			AddItem( new BrasNomM( ));
			AddItem( new CouNomade( ));
			AddItem( new PantsNomade( ));
			AddItem( new shoesNomadeM( ));
			AddItem( new TabarNomade( ));
			AddItem( new TurbanNomade( ));
			

			
			}



			


		public NomadeM( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}