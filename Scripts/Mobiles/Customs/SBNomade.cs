using System;
using System.Collections;
using Server.Items;

namespace Server.Mobiles
{
	public class SBNomade: SBInfo
	{
		private ArrayList m_BuyInfo = new InternalBuyInfo();
		private IShopSellInfo m_SellInfo = new InternalSellInfo();

		public SBNomade()
		{
		}

		public override IShopSellInfo SellInfo { get { return m_SellInfo; } }
		public override ArrayList BuyInfo { get { return m_BuyInfo; } }

		public class InternalBuyInfo : ArrayList
		{
			public InternalBuyInfo()
			{
				

				Add( new GenericBuyInfo( typeof( PottedCactus ), 1500,  2, 0x1E0F, 0 ) );
				Add( new GenericBuyInfo( typeof( PottedTree ), 2500,  1, 0x11C8, 0 ) );
				Add( new GenericBuyInfo( typeof( PottedTree1 ), 1500,  3, 0x11C9, 0 ) );
				Add( new GenericBuyInfo( typeof( PottedCactus1 ), 3500,  2, 0x1E10, 0 ) );
				Add( new GenericBuyInfo( typeof( PottedPlant2 ), 2500,  2, 0x11CC, 0 ) );
               






			}
		}

		public class InternalSellInfo : GenericSellInfo
		{
			public InternalSellInfo()
			{
				
				
			}
		}
	}
}