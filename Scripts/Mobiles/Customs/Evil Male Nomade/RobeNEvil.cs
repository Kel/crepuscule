using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x35fe, 0x35fe )]
	public class RobeNEvil : BaseWaist
	{

		[Constructable]
		public RobeNEvil() : base( 0x35fe )
		{
			Weight = 1.0;
			Layer = Layer.OuterTorso;
			Name = "Robe";
			Hue = 2144;
			Movable = false;
		}

		public RobeNEvil( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}