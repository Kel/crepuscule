using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30f7, 0x30f7 )]
	public class FoulardNEvil : BaseWaist
	{

		[Constructable]
		public FoulardNEvil() : base( 0x30f7 )
		{
			Weight = 1.0;
			Layer = Layer.Neck;
			Name = "Foulard";
			
			Movable = false;
		}

		public FoulardNEvil( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}