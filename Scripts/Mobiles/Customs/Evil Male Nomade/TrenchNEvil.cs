using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x369a, 0x369a )]
	public class TrenchNEvil : BaseWaist
	{

		[Constructable]
		public TrenchNEvil() : base( 0x369a )
		{
			Weight = 1.0;
			Layer = Layer.InnerTorso;
			Name = "Trench";
			Hue = 2146;
			Movable = false;
		}

		public TrenchNEvil( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}