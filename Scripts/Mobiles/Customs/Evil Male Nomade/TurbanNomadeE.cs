using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3606, 0x3606 )]
	public class TurbanNomadeE : BaseWaist
	{

		[Constructable]
		public TurbanNomadeE() : base( 0x3606 )
		{
			Weight = 1.0;
			Layer = Layer.Helm;
			Name = "Turban";
			Hue = 2110;
			Movable = false;
		}

		public TurbanNomadeE( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}