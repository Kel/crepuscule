using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	public class NomadeF : BaseVendor
	{
		private ArrayList m_SBInfos = new ArrayList();
		protected override ArrayList SBInfos{ get { return m_SBInfos; } }

		[Constructable]
		public NomadeF() : base( "La Nomade" )
		{
			SetSkill( SkillName.ItemID, 60.0, 83.0 );
			Hue = 643;
			Body = 0x191;
				Name = NameList.RandomName( "female" );
		}


		public override void InitOutfit()
		{
			AddItem( new BadageNomade( ) );
			AddItem( new BracerNomadeF( ));
			AddItem( new CapeNomade( ));
			AddItem( new CasqueNomadeF( ));
			AddItem( new CollierNomadeF( ));
			AddItem( new EarringNomadeF( ));
			AddItem( new RobeNomade( ));
			AddItem( new ShoeNomadeF( ));

			
			}



			


		public NomadeF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}