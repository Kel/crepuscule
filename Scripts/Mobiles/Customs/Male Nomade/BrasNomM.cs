using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30f4, 0x30f4 )]
	public class BrasNomM : BaseWaist
	{

		[Constructable]
		public BrasNomM() : base( 0x30f4 )
		{
			Weight = 1.0;
			Layer = Layer.Arms;
			Name = "Prot�ge Bras";
			Hue = 1883;
		}

		public BrasNomM( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}