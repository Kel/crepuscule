using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x35f3, 0x35f3 )]
	public class CouNomade : BaseWaist
	{

		[Constructable]
		public CouNomade() : base( 0x35f3 )
		{
			Weight = 1.0;
			Layer = Layer.Neck;
			Name = "Foulard";
			Hue = 1878;
		}

		public CouNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}