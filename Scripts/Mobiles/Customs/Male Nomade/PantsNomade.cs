using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x36aa, 0x36aa )]
	public class PantsNomade : BaseWaist
	{

		[Constructable]
		public PantsNomade() : base( 0x36aa )
		{
			Weight = 1.0;
			Layer = Layer.Pants;
			Name = "Pantalons amples";
			Hue = 1883;
		}

		public PantsNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}