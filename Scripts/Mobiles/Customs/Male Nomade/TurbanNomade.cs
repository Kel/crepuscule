using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3606, 0x3606 )]
	public class TurbanNomade : BaseWaist
	{

		[Constructable]
		public TurbanNomade() : base( 0x3606 )
		{
			Weight = 1.0;
			Layer = Layer.Helm;
			Name = "Turban";
			Hue = 1878;
		}

		public TurbanNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}