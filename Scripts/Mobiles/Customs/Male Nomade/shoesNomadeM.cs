using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30f2, 0x30f2 )]
	public class shoesNomadeM : BaseWaist
	{

		[Constructable]
		public shoesNomadeM() : base( 0x30f2 )
		{
			Weight = 1.0;
			Layer = Layer.Shoes;
			Name = "Sandales";
			Hue = 1888;
		}

		public shoesNomadeM( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}