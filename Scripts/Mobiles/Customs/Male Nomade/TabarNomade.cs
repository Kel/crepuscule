using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x35ff, 0x35ff )]
	public class TabarNomade : BaseWaist
	{

		[Constructable]
		public TabarNomade() : base( 0x35ff )
		{
			Weight = 1.0;
			Layer = Layer.InnerTorso;
			Name = "Tabar";
			Hue = 1876;
		}

		public TabarNomade( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}