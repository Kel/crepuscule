using System;
using System.Collections;
using Server.Items;
using Server.Network;
using Server.ContextMenus;
using Server.Mobiles;
using Server.Misc;
using Server.Engines;
using System.Collections.Generic;
//using Server.Engines.BulkOrders;

namespace Server.Mobiles
{
	public enum VendorShoeType
	{
		None,
		Shoes,
		Boots,
		Sandals,
		ThighBoots
	}

    [Description("Vendeur PNJ")]
	public abstract class BaseVendor : BaseCreature, IVendor
	{
		private const int MaxSell = 500;

		protected abstract ArrayList SBInfos{ get; }

        public string OldSBInfosName
        {
            get
            {
                if (SBInfos != null && SBInfos.Count > 0)
                    return SBInfos[0].GetType().GetShortName();
                return null;
            }
        }

        
		private ArrayList m_ArmorBuyInfo = new ArrayList();
		private ArrayList m_ArmorSellInfo = new ArrayList();
		private DateTime m_LastRestock;

		public override bool CanTeach{ get{ return true; } }
		public override bool PlayerRangeSensitive{ get{ return true; } }
		public virtual bool IsActiveVendor{ get{ return true; } }
		public virtual bool IsActiveBuyer{ get{ return IsActiveVendor; } } // response to vendor SELL
		public virtual bool IsActiveSeller{ get{ return IsActiveVendor; } } // repsonse to vendor BUY
		public virtual NpcGuild NpcGuild{ get{ return NpcGuild.None; } }
		public virtual bool IsInvulnerable{ get{ return true; } }
		public override bool ShowFameTitle{ get{ return false; } }

		public BaseVendor( string title ) : base( AIType.AI_Vendor, FightMode.None, 2, 1, 0.5, 2 )
		{

			LoadSBInfo();

			this.Title = title;
			InitBody();
			InitOutfit();

			Container pack;

			//these packs MUST exist, or the client will crash when the packets are sent
			pack = new Backpack();
			pack.Layer = Layer.ShopBuy;
			pack.Movable = false;
			pack.Visible = false;
			AddItem( pack );

			pack = new Backpack();
			pack.Layer = Layer.ShopResale;
			pack.Movable = false;
			pack.Visible = false;
			AddItem( pack );

			m_LastRestock = DateTime.Now;
		}
		
		public override void OnSpeech( SpeechEventArgs e )
		{
			base.OnSpeech( e );

			Mobile from = e.Mobile;

            if (from.Z - 10 < this.Z && from.Z + 10 > this.Z)
            {
                if (from.InRange(this, 4))
                {
                    var speech = e.Speech.ToLower();
                    var trades = Trades.HasValue;
                    if (speech.IndexOf("bonjour") >= 0 || e.Speech.ToLower().IndexOf("salut") >= 0)
                    {
                        string message = trades ? "Bonjour � vous! Approchez, regardez mes marchandises! " : "Salutations!";
                        this.Say(message);
                    }
                    if (speech.IndexOf("acheter") >= 0 ||
                        speech.IndexOf("vendez") >= 0 ||
                        speech.IndexOf("acquerir") >= 0)
                    {
                        if (trades)
                            this.VendorBuy(from);
                        else this.Say("Oh, non, d�sol�...");
                    }
                    if (speech.IndexOf("vendre") >= 0 ||
                        speech.IndexOf("achetez") >= 0)
                    {
                        if (trades)
                            this.VendorSell(from);
                        else this.Say("Oh, non, d�sol�...");
                    }
                }
            }
		}

		public BaseVendor( Serial serial ) : base( serial )
		{
		}

        private WeakChoice<TradeInfo> m_Trades = new WeakChoice<TradeInfo>("TradesInternalName", "TradesList", "InternalName");
        public string TradesInternalName { get; set; }
        public List<TradeInfo> TradesList { get { return ManagerConfig.Trades; } }

        [Description("Achat/Vente")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Trades
        {
            get { return m_Trades; }
            set { LoadSBInfo(); }
        }

		public DateTime LastRestock
		{
			get
			{
				return m_LastRestock;
			}
			set
			{
				m_LastRestock = value;
			}
		}

		public virtual TimeSpan RestockDelay
		{
			get
			{
				return TimeSpan.FromHours( 1 );
			}
		}

		public Container BuyPack
		{
			get
			{
				Container pack = FindItemOnLayer( Layer.ShopBuy ) as Container;

				if ( pack == null )
				{
					pack = new Backpack();
					pack.Layer = Layer.ShopBuy;
					pack.Visible = false;
					AddItem( pack );
				}

				return pack;
			}
		}


		public virtual void LoadSBInfo()
		{
			m_LastRestock = DateTime.Now;

			m_ArmorBuyInfo.Clear();
			m_ArmorSellInfo.Clear();

            if (Trades.HasValue)
            {
                m_ArmorBuyInfo.AddRange(Trades.Value.BuyInfo);
                m_ArmorSellInfo.Add(Trades.Value.SellInfo);
            }
		}


		public virtual bool GetGender()
		{
			return Utility.RandomBool();
		}

		public virtual void InitBody()
		{
			InitStats( 100, 100, 25 );

			SpeechHue = Utility.RandomDyedHue();
			Hue = Utility.RandomSkinHue();

			if ( IsInvulnerable && !Core.AOS )
				NameHue = 0x35;

			if ( Female = GetGender() )
			{
				Body = 0x191;
				Name = NameList.RandomName( "female" );
			}
			else
			{
				Body = 0x190;
				Name = NameList.RandomName( "male" );
			}
		}

		public virtual int GetRandomHue()
		{
			switch ( Utility.Random( 5 ) )
			{
				default:
				case 0: return Utility.RandomBlueHue();
				case 1: return Utility.RandomGreenHue();
				case 2: return Utility.RandomRedHue();
				case 3: return Utility.RandomYellowHue();
				case 4: return Utility.RandomNeutralHue();
			}
		}

		public virtual int GetShoeHue()
		{
			if ( 0.1 > Utility.RandomDouble() )
				return 0;

			return Utility.RandomNeutralHue();
		}

		public virtual VendorShoeType ShoeType
		{
			get{ return VendorShoeType.Shoes; }
		}

		public virtual int RandomBrightHue()
		{
			if ( 0.1 > Utility.RandomDouble() )
				return Utility.RandomList( 0x62, 0x71 );

			return Utility.RandomList( 0x03, 0x0D, 0x13, 0x1C, 0x21, 0x30, 0x37, 0x3A, 0x44, 0x59 );
		}

		public virtual void CheckMorph()
		{
			if ( CheckGargoyle() )
				return;

			CheckNecromancer();
		}

		public virtual bool CheckGargoyle()
		{
			Map map = this.Map;

			if ( map != Map.Ilshenar )
				return false;

			if ( Region.Name != "Gargoyle City" )
				return false;

			if ( Body != 0x2F6 || (Hue & 0x8000) == 0 )
				TurnToGargoyle();

			return true;
		}

		public virtual bool CheckNecromancer()
		{
			Map map = this.Map;

			if ( map != Map.Malas )
				return false;

			if ( Region.Name != "Umbra" )
				return false;

			if ( Hue != 0x83E8 )
				TurnToNecromancer();

			return true;
		}

		protected override void OnLocationChange( Point3D oldLocation )
		{
			base.OnLocationChange( oldLocation );

			CheckMorph();
		}

		protected override void OnMapChange( Map oldMap )
		{
			base.OnMapChange( oldMap );

			CheckMorph();
		}

		public virtual int GetRandomNecromancerHue()
		{
			switch ( Utility.Random( 20 ) )
			{
				case 0: return 0;
				case 1: return 0x4E9;
				default: return Utility.RandomList( 0x485, 0x497 );
			}
		}

		public virtual void TurnToNecromancer()
		{
			ArrayList items = new ArrayList( this.Items );

			for ( int i = 0; i < items.Count; ++i )
			{
				Item item = (Item)items[i];

				if ( item is Hair || item is Beard )
					item.Hue = 0;
				else if ( item is BaseClothing || item is BaseWeapon || item is BaseArmor || item is BaseTool )
					item.Hue = GetRandomNecromancerHue();
			}

			Hue = 0x83E8;
		}

		public virtual void TurnToGargoyle()
		{
			ArrayList items = new ArrayList( this.Items );

			for ( int i = 0; i < items.Count; ++i )
			{
				Item item = (Item)items[i];

				if ( item is BaseClothing || item is Hair || item is Beard )
					item.Delete();
			}

			Body = 0x2F6;
			Hue = RandomBrightHue() | 0x8000;
			Name = NameList.RandomName( "gargoyle vendor" );

			CapitalizeTitle();
		}

		public virtual void CapitalizeTitle()
		{
			string title = this.Title;

			if ( title == null )
				return;

			string[] split = title.Split( ' ' );

			for ( int i = 0; i < split.Length; ++i )
			{
				if ( Insensitive.Equals( split[i], "the" ) )
					continue;

				if ( split[i].Length > 1 )
					split[i] = Char.ToUpper( split[i][0] ) + split[i].Substring( 1 );
				else if ( split[i].Length > 0 )
					split[i] = Char.ToUpper( split[i][0] ).ToString();
			}

			this.Title = String.Join( " ", split );
		}

		public virtual int GetHairHue()
		{
			return Utility.RandomHairHue();
		}

		public virtual void InitOutfit()
		{
			switch ( Utility.Random( 3 ) )
			{
				case 0: AddItem( new FancyShirt( GetRandomHue() ) ); break;
				case 1: AddItem( new Doublet( GetRandomHue() ) ); break;
				case 2: AddItem( new Shirt( GetRandomHue() ) ); break;
			}

			switch ( ShoeType )
			{
				case VendorShoeType.Shoes: AddItem( new Shoes( GetShoeHue() ) ); break;
				case VendorShoeType.Boots: AddItem( new Boots( GetShoeHue() ) ); break;
				case VendorShoeType.Sandals: AddItem( new Sandals( GetShoeHue() ) ); break;
				case VendorShoeType.ThighBoots: AddItem( new ThighBoots( GetShoeHue() ) ); break;
			}

			int hairHue = GetHairHue();

			if ( Female )
			{
				switch ( Utility.Random( 6 ) )
				{
					case 0: AddItem( new ShortPants( GetRandomHue() ) ); break;
					case 1:
					case 2: AddItem( new Kilt( GetRandomHue() ) ); break;
					case 3:
					case 4:
					case 5: AddItem( new Skirt( GetRandomHue() ) ); break;
				}

				switch ( Utility.Random( 9 ) )
				{
					case 0: AddItem( new Afro( hairHue ) ); break;
					case 1: AddItem( new KrisnaHair( hairHue ) ); break;
					case 2: AddItem( new PageboyHair( hairHue ) ); break;
					case 3: AddItem( new PonyTail( hairHue ) ); break;
					case 4: AddItem( new ReceedingHair( hairHue ) ); break;
					case 5: AddItem( new TwoPigTails( hairHue ) ); break;
					case 6: AddItem( new ShortHair( hairHue ) ); break;
					case 7: AddItem( new LongHair( hairHue ) ); break;
					case 8: AddItem( new BunsHair( hairHue ) ); break;
				}
			}
			else
			{
				switch ( Utility.Random( 2 ) )
				{
					case 0: AddItem( new LongPants( GetRandomHue() ) ); break;
					case 1: AddItem( new ShortPants( GetRandomHue() ) ); break;
				}

				switch ( Utility.Random( 8 ) )
				{
					case 0: AddItem( new Afro( hairHue ) ); break;
					case 1: AddItem( new KrisnaHair( hairHue ) ); break;
					case 2: AddItem( new PageboyHair( hairHue ) ); break;
					case 3: AddItem( new PonyTail( hairHue ) ); break;
					case 4: AddItem( new ReceedingHair( hairHue ) ); break;
					case 5: AddItem( new TwoPigTails( hairHue ) ); break;
					case 6: AddItem( new ShortHair( hairHue ) ); break;
					case 7: AddItem( new LongHair( hairHue ) ); break;
				}

				switch ( Utility.Random( 5 ) )
				{
					case 0: AddItem( new LongBeard( hairHue ) ); break;
					case 1: AddItem( new MediumLongBeard( hairHue ) ); break;
					case 2: AddItem( new Vandyke( hairHue ) ); break;
					case 3: AddItem( new Mustache( hairHue ) ); break;
					case 4: AddItem( new Goatee( hairHue ) ); break;
				}
			}

			PackGold( 100, 200 );
		}

		public virtual void Restock()
		{
			m_LastRestock = DateTime.Now;

			IBuyItemInfo[] buyInfo = this.GetBuyInfo();

			foreach ( IBuyItemInfo bii in buyInfo )
				bii.OnRestock();
		}

		private static TimeSpan InventoryDecayTime = TimeSpan.FromHours( 1.0 );

#if false
		public virtual void VendorBuy( Mobile from )
		{
			if ( !IsActiveSeller )
				return;

			if ( !from.CheckAlive() )
				return;

			if ( DateTime.Now - m_LastRestock > RestockDelay )
				Restock();

			int count = 0;
			ArrayList list;
			IBuyItemInfo[] buyInfo = this.GetBuyInfo();
			IShopSellInfo[] sellInfo = this.GetSellInfo();

            if (Trades.HasValue && buyInfo.Length == 0 && sellInfo.Length == 1)
            {
                LoadSBInfo();
                buyInfo = this.GetBuyInfo();
                sellInfo = this.GetSellInfo();
            }

			list = new ArrayList( buyInfo.Length );
			Container cont = this.BuyPack;

			for (int idx=0;idx<buyInfo.Length;idx++)
			{
				IBuyItemInfo buyItem = (IBuyItemInfo)buyInfo[idx];
				if ( buyItem.Amount > 0 )
				{
					if ( list.Count < 250 )
					{
						list.Add( new BuyItemState( buyItem.Name, cont.Serial, 0x7FFFFFFF - idx, buyItem.Price, buyItem.Amount, buyItem.ItemID, buyItem.Hue ) );
						count++;
					}
				}
			}

			ArrayList playerItems = cont.Items;

			for ( int i = playerItems.Count - 1; i >= 0; --i )
			{
				if ( i >= playerItems.Count )
					continue;

				Item item = (Item)playerItems[i];

				if ( (item.LastMoved + InventoryDecayTime) <= DateTime.Now )
					item.Delete();
			}

			for ( int i = 0; i < playerItems.Count; ++i )
			{
				Item item = (Item)playerItems[i];

				int price = 0;
				string name = null;

				foreach( IShopSellInfo ssi in sellInfo )
				{
					if ( ssi.IsSellable( item ) )
					{
						price = ssi.GetBuyPriceFor( item );
						name = ssi.GetNameFor( item );
						break;
					}
				}

				if ( name != null && list.Count < 250 )
				{
					list.Add( new BuyItemState( name, cont.Serial, item.Serial, price, item.Amount, item.ItemID, item.Hue ) );
					count++;
				}
			}

			//one (not all) of the packets uses a byte to describe number of items in the list.  Osi = dumb.
			//if ( list.Count > 255 )
			//	Console.WriteLine( "Vendor Warning: Vendor {0} has more than 255 buy items, may cause client errors!", this );

			if ( list.Count > 0 )
			{
				list.Sort( new BuyItemStateComparer() );

				SendPacksTo( from );

                // KEL TEMP DISABLED
				//from.Send( new VendorBuyContent( list ) );
				//from.Send( new VendorBuyList( this, list ) );
				from.Send( new DisplayBuyList( this ) );
				from.Send( new MobileStatusExtended( from ) );//make sure their gold amount is sent

				SayTo( from, 500186 ); // Greetings.  Have a look around.
			}
		}
#endif
        public virtual void VendorBuy(Mobile from)
        {
            if (!IsActiveSeller)
                return;

            if (!from.CheckAlive())
                return;

            if (DateTime.Now - m_LastRestock > RestockDelay)
                Restock();

            int count = 0;
            IBuyItemInfo[] buyInfo = this.GetBuyInfo();
            IShopSellInfo[] sellInfo = this.GetSellInfo();

            if (Trades.HasValue && buyInfo.Length == 0 && sellInfo.Length == 1)
            {
                LoadSBInfo();
                buyInfo = this.GetBuyInfo();
                sellInfo = this.GetSellInfo();
            }

            List<BuyItemState> list = new List<BuyItemState>(buyInfo.Length);
            Container cont = this.BuyPack;

            for (int idx = 0; idx < buyInfo.Length; idx++)
            {
                IBuyItemInfo buyItem = (IBuyItemInfo)buyInfo[idx];

                if (buyItem.Amount > 0)
                {
                    if (list.Count < 250)
                    {
                        list.Add(new BuyItemState(buyItem.Name, cont.Serial, 0x7FFFFFFF - idx, buyItem.Price, buyItem.Amount, buyItem.ItemID, buyItem.Hue));
                        count++;
                    }
                }
            }

            var playerItems = cont.Items;
            for (int i = playerItems.Count - 1; i >= 0; --i)
            {
                if (i >= playerItems.Count)
                    continue;

                Item item = (Item)playerItems[i];

                if ((item.LastMoved + InventoryDecayTime) <= DateTime.Now)
                    item.Delete();
            }

            for (int i = 0; i < playerItems.Count; ++i)
            {
                Item item = (Item)playerItems[i];

                int price = 0;
                string name = null;

                foreach (IShopSellInfo ssi in sellInfo)
                {
                    if (ssi.IsSellable(item))
                    {
                        price = ssi.GetBuyPriceFor(item);
                        name = ssi.GetNameFor(item);
                        break;
                    }
                }

                if (name != null && list.Count < 250)
                {
                    list.Add(new BuyItemState(name, cont.Serial, item.Serial, price, item.Amount, item.ItemID, item.Hue));
                    count++;
                }
            }

            //one (not all) of the packets uses a byte to describe number of items in the list.  Osi = dumb.
            //if ( list.Count > 255 )
            //	Console.WriteLine( "Vendor Warning: Vendor {0} has more than 255 buy items, may cause client errors!", this );

            if (list.Count > 0)
            {
                list.Sort(new BuyItemStateComparer());

                SendPacksTo(from);

                NetState ns = from.NetState;

                if (ns == null)
                    return;

                if (ns.ContainerGridLines)
                    from.Send(new VendorBuyContent6017(list));
                else
                    from.Send(new VendorBuyContent(list));

                from.Send(new VendorBuyList(this, list));

                if (ns.HighSeas)
                    from.Send(new DisplayBuyListHS(this));
                else
                    from.Send(new DisplayBuyList(this));

                from.Send(new MobileStatusExtended(from));//make sure their gold amount is sent

                /*if (opls != null)
                {
                    for (int i = 0; i < opls.Count; ++i)
                    {
                        from.Send(opls[i]);
                    }
                }*/

                SayTo(from, 500186); // Greetings.  Have a look around.
            }
        }



		public virtual void SendPacksTo( Mobile from )
		{
			Item pack = FindItemOnLayer( Layer.ShopBuy );

			if ( pack == null )
			{
				pack = new Backpack();
				pack.Layer = Layer.ShopBuy;
				pack.Movable = false;
				pack.Visible = false;
				AddItem( pack );
			}

			from.Send( new EquipUpdate( pack ) );

			pack = FindItemOnLayer( Layer.ShopSell );

			if ( pack != null )
				from.Send( new EquipUpdate( pack ) );

			pack = FindItemOnLayer( Layer.ShopResale );

			if ( pack == null )
			{
				pack = new Backpack();
				pack.Layer = Layer.ShopResale;
				pack.Movable = false;
				pack.Visible = false;
				AddItem( pack );
			}

			from.Send( new EquipUpdate( pack ) );
		}

		public virtual void VendorSell( Mobile from )
		{
			if ( !IsActiveBuyer )
				return;

			if ( !from.CheckAlive() )
				return;

			Container pack = from.Backpack;

			if ( pack != null )
			{
				IShopSellInfo[] info = GetSellInfo();

                if (Trades.HasValue && info.Length == 1 && GetBuyInfo().Length == 0)
                {
                    LoadSBInfo();
                    info = this.GetSellInfo();
                }

				Hashtable table = new Hashtable();

				foreach ( IShopSellInfo ssi in info )
				{
					Item[] items = pack.FindItemsByType( ssi.Types );

					foreach ( Item item in items )
					{
						if ( item is Container && ((Container)item).Items.Count != 0 )
							continue;

						if ( item.IsStandardLoot() && item.Movable && ssi.IsSellable( item ) )
							table[item] = new SellItemState( item, ssi.GetSellPriceFor( item ), ssi.GetNameFor( item ) );
					}
				}

				if ( table.Count > 0 )
				{
					SendPacksTo( from );

					from.Send( new VendorSellList( this, table ) );
				}
				else
				{
					Say( false, "Vous n'avez rien d'interessant pour moi." );
				}
			}
		}

        public virtual bool OnBuyItems(Mobile buyer, ArrayList list)
        {
            var typed = new List<BuyItemResponse>(list.Count);
            foreach(BuyItemResponse item in list)
                typed.Add(item);
            return OnBuyItems(buyer, typed);
        }

        public virtual bool OnBuyItems(Mobile buyer, List<BuyItemResponse> list)
		{
            if (!Trades.HasValue)
                return false;

			if ( !IsActiveSeller )
				return false;

			if ( !buyer.CheckAlive() )
				return false;

			IBuyItemInfo[] buyInfo = this.GetBuyInfo();
			IShopSellInfo[] info = GetSellInfo();
			int totalCost = 0;
			ArrayList validBuy = new ArrayList( list.Count );
			Container cont;
			bool bought = false;
			bool fromBank = false;
			bool fullPurchase = true;
			int controlSlots = buyer.FollowersMax - buyer.Followers;

			foreach ( BuyItemResponse buy in list )
			{
				Serial ser = 0x7FFFFFFF - buy.Serial;
				int amount = buy.Amount;
				if ( ser >= 0 && ser <= buyInfo.Length )
				{
					IBuyItemInfo bii = buyInfo[ser];
					if ( amount > bii.Amount )
						amount = bii.Amount;
					if ( amount <= 0 )
						continue;

					int slots = bii.ControlSlots * amount;

					if ( controlSlots >= slots )
					{
						controlSlots -= slots;
					}
					else
					{
						fullPurchase = false;
						continue;
					}

					totalCost += bii.Price * amount;
					validBuy.Add( buy );
				}
				else
				{
					Item item = World.FindItem( buy.Serial );

					if ( item == null || item.RootParent != this )
						continue;

					if ( amount > item.Amount )
						amount = item.Amount;
					if ( amount <= 0 )
						continue;

					foreach( IShopSellInfo ssi in info )
					{
						if ( ssi.IsSellable( item ) )
						{
							if ( ssi.IsResellable( item ) )
							{
								totalCost += ssi.GetBuyPriceFor( item ) * amount;
								validBuy.Add( buy );
								break;
							}
						}
					}
				}
			}//foreach

			if ( fullPurchase && validBuy.Count == 0 )
				SayTo( buyer, 500190 ); // Thou hast bought nothing!
			else if ( validBuy.Count == 0 )
				SayTo( buyer, 500187 ); // Your order cannot be fulfilled, please try again.

			if ( validBuy.Count == 0 )
				return false;

			bought = ( buyer.AccessLevel >= AccessLevel.GameMaster );

			cont = buyer.Backpack;
			if ( !bought && cont != null )
			{
				if ( cont.ConsumeTotal( typeof( Gold ), totalCost ) )
					bought = true;
				else if ( totalCost < 2000 )
					SayTo( buyer, 500192 );//Begging thy pardon, but thou casnt afford that.
			}

			if ( !bought && totalCost >= 2000 )
			{
				cont = buyer.BankBox;
				if ( cont != null && cont.ConsumeTotal( typeof( Gold ), totalCost ) )
				{
					bought = true;
					fromBank = true;
				}
				else
				{
					SayTo( buyer, 500191 ); //Begging thy pardon, but thy bank account lacks these funds.
				}
			}

			if ( !bought )
				return false;
			else
				buyer.PlaySound( 0x32 );

			cont = buyer.Backpack;
			if ( cont == null )
				cont = buyer.BankBox;

			foreach ( BuyItemResponse buy in validBuy )
			{
				Serial ser = 0x7FFFFFFF - buy.Serial;
				int amount = buy.Amount;

				if ( ser >= 0 && ser <= buyInfo.Length )
				{
					IBuyItemInfo bii = buyInfo[ser];

					if ( amount > bii.Amount )
						amount = bii.Amount;

					bii.Amount -= amount;

					object o = bii.GetObject();

					if ( o is Item )
					{
						Item item = (Item)o;
                        if (item is CrepusculeItem)
                            (item as CrepusculeItem).OnAssignProperties();

						if ( item.Stackable )
						{
							item.Amount = amount;

							if ( cont == null || !cont.TryDropItem( buyer, item, false ) )
								item.MoveToWorld( buyer.Location, buyer.Map );
						}
						else
						{
							item.Amount = 1;

							if ( cont == null || !cont.TryDropItem( buyer, item, false ) )
								item.MoveToWorld( buyer.Location, buyer.Map );

							for (int i=1;i<amount;i++)
							{
								item = bii.GetObject() as Item;

								if ( item != null )
								{
									item.Amount = 1;

									if ( cont == null || !cont.TryDropItem( buyer, item, false ) )
										item.MoveToWorld( buyer.Location, buyer.Map );
								}
							}
						}
					}
					else if ( o is Mobile )
					{
						Mobile m = (Mobile)o;

						m.Direction = (Direction)Utility.Random( 8 );
						m.MoveToWorld( buyer.Location, buyer.Map );
						m.PlaySound( m.GetIdleSound() );

						if ( m is BaseCreature )
							((BaseCreature)m).SetControlMaster( buyer );

						for ( int i = 1; i < amount; ++i )
						{
							m = bii.GetObject() as Mobile;

							if ( m != null )
							{
								m.Direction = (Direction)Utility.Random( 8 );
								m.MoveToWorld( buyer.Location, buyer.Map );

								if ( m is BaseCreature )
									((BaseCreature)m).SetControlMaster( buyer );
							}
						}
					}
				}
				else
				{
					Item item = World.FindItem( buy.Serial );

					if ( item == null )
						continue;

					if ( amount > item.Amount )
						amount = item.Amount;

					foreach( IShopSellInfo ssi in info )
					{
						if ( ssi.IsSellable( item ) )
						{
							if ( ssi.IsResellable( item ) )
							{
								Item buyItem;
								if ( amount >= item.Amount )
								{
									buyItem = item;
								}
								else
								{
                                    buyItem = Mobile.LiftItemDupe(item, item.Amount - amount);
									item.Amount -= amount;
								}

								if ( cont == null || !cont.TryDropItem( buyer, buyItem, false ) )
									buyItem.MoveToWorld( buyer.Location, buyer.Map );

								break;
							}
						}
					}
				}
			}//foreach

			if ( fullPurchase )
			{
				if ( buyer.AccessLevel >= AccessLevel.GameMaster )
					SayTo( buyer, false, "Kel m'as dit de vous donner �a gratuitement. Il es g�n�reux hein?" );
				else if ( fromBank )
                    SayTo(buyer, false, "Le total est de {0} �cus, �a �t� retir� de votre compte en banque. Je vous en remercie.", totalCost);
				else
                    SayTo(buyer, false, "Le total est de {0} �cus. Je vous en remercie.", totalCost);
			}
			else
			{
				if ( buyer.AccessLevel >= AccessLevel.GameMaster )
                    SayTo(buyer, false, "Kel m'as dit de vous donner �a gratuitement. Il est g�n�reux hein?");
				else if ( fromBank )
                    SayTo(buyer, false, "Le total est de {0} �cus, �a �t� retir� de votre compte en banque. Je vous en remercie. Par contre, je n'ai pas pu vous vendre tout ce que vous vouliez.", totalCost);
				else
                    SayTo(buyer, false, "Le total est de {0} �cus. Je vous en remercie. Par contre, je n'ai pas pu vous vendre tout ce que vous vouliez.", totalCost);
			}

			return true;
		}

        public virtual bool OnSellItems(Mobile buyer, ArrayList list)
        {
            var typed = new List<SellItemResponse>(list.Count);
            foreach (SellItemResponse item in list)
                typed.Add(item);
            return OnSellItems(buyer, typed);
        }

        public virtual bool OnSellItems(Mobile seller, List<SellItemResponse> list)
		{
            if (!Trades.HasValue)
                return false;

			if ( !IsActiveBuyer )
				return false;

			if ( !seller.CheckAlive() )
				return false;

			seller.PlaySound( 0x32 );

			IShopSellInfo[] info = GetSellInfo();
			IBuyItemInfo[] buyInfo = this.GetBuyInfo();
			int GiveGold = 0;
			int Sold = 0;
			Container cont;
			ArrayList delete = new ArrayList();
			ArrayList drop = new ArrayList();

			foreach ( SellItemResponse resp in list )
			{
				if ( resp.Item.RootParent != seller || resp.Amount <= 0 )
					continue;

				foreach( IShopSellInfo ssi in info )
				{
					if ( ssi.IsSellable( resp.Item ) )
					{
						Sold++;
						break;
					}
				}
			}

			if ( Sold > MaxSell )
			{
				SayTo( seller, true, "Vous ne pouvez vendre que {0} objets � la fois!", MaxSell );
				return false;
			} 
			else if ( Sold == 0 )
			{
				return true;
			}

			foreach ( SellItemResponse resp in list )
			{
				if ( resp.Item.RootParent != seller || resp.Amount <= 0 )
					continue;

				foreach( IShopSellInfo ssi in info )
				{
					if ( ssi.IsSellable( resp.Item ) )
					{
						int amount = resp.Amount;

						if ( amount > resp.Item.Amount )
							amount = resp.Item.Amount;

						if ( ssi.IsResellable( resp.Item ) )
						{
							bool found = false;

							foreach ( IBuyItemInfo bii in buyInfo )
							{
								if ( bii.Restock( resp.Item, amount ) )
								{
									resp.Item.Consume( amount );
									found = true;

									break;
								}
							}

							if ( !found )
							{
								cont = this.BuyPack;

								if ( amount < resp.Item.Amount )
								{
									resp.Item.Amount -= amount;
									Item item = Mobile.LiftItemDupe(resp.Item, amount );
									item.SetLastMoved();
									cont.DropItem( item );
								}
								else
								{
									resp.Item.SetLastMoved();
									cont.DropItem( resp.Item );
								}
							}
						}
						else
						{
							if ( amount < resp.Item.Amount )
								resp.Item.Amount -= amount;
							else
								resp.Item.Delete();
						}

						GiveGold += ssi.GetSellPriceFor( resp.Item )*amount;
						break;
					}
				}
			}

			if ( GiveGold > 0 )
			{
				while ( GiveGold > 60000 )
				{
					seller.AddToBackpack( new Gold( 60000 ) );
					GiveGold -= 60000;
				}

				seller.AddToBackpack( new Gold( GiveGold ) );

				seller.PlaySound( 0x0037 );//Gold dropping sound
			}
			//no cliloc for this?
			//SayTo( seller, true, "Thank you! I bought {0} item{1}. Here is your {2}gp.", Sold, (Sold > 1 ? "s" : ""), GiveGold );
			
			return true;
		}

        #region Serialize & Deserialize
        public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 2 ); // version

            // Version 2
            writer.Write(TradesInternalName);
        }

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

			switch ( version )
			{
                case 2:
                {
                    TradesInternalName = reader.ReadString();
                    Trades.SetValueByKey(this, TradesInternalName);
                    LoadSBInfo();

                    break;
                }
			}

			if ( Core.AOS && NameHue == 0x35 )
				NameHue = -1;

			CheckMorph();
		}
        #endregion

        public override void AddCustomContextEntries(Mobile from, List<ContextMenuEntry> list)
		{
			list.Clear();
		}
		public override void GetContextMenuEntries( Mobile from, List<ContextMenuEntry> list )
		{
			list.Clear();
		}

		public virtual IShopSellInfo[] GetSellInfo()
		{
			return (IShopSellInfo[])m_ArmorSellInfo.ToArray( typeof( IShopSellInfo ) );
		}

		public virtual IBuyItemInfo[] GetBuyInfo()
		{
			return (IBuyItemInfo[])m_ArmorBuyInfo.ToArray( typeof( IBuyItemInfo ) );
		}

		public override bool CanBeDamaged()
		{
			return !IsInvulnerable;
		}


    }
}


namespace Server.ContextMenus
{
	public class VendorBuyEntry : ContextMenuEntry
	{
		private BaseVendor m_Vendor;

		public VendorBuyEntry( Mobile from, BaseVendor vendor ) : base( 6103, 8 )
		{
			m_Vendor = vendor;
		}

		public override void OnClick()
		{
			m_Vendor.VendorBuy( this.Owner.From );
		}
	}

	public class VendorSellEntry : ContextMenuEntry
	{
		private BaseVendor m_Vendor;

		public VendorSellEntry( Mobile from, BaseVendor vendor ) : base( 6104, 8 )
		{
			m_Vendor = vendor;
		}

		public override void OnClick()
		{
			m_Vendor.VendorSell( this.Owner.From );
		}
	}
}

namespace Server
{
	public interface IShopSellInfo
	{
		//get display name for an item
		string GetNameFor( Item item );

        //get a raw sell price for a type of an item
        int GetSellPriceFor(Type type);

		//get price for an item which the player is selling
		int GetSellPriceFor( Item item );

		//get price for an item which the player is buying
		int GetBuyPriceFor( Item item );

		//can we sell this item to this vendor?
		bool IsSellable( Item item );

		//What do we sell?
		Type[] Types{ get; }

		//does the vendor resell this item?
		bool IsResellable( Item item );
	}

	public interface IBuyItemInfo
	{
		//get a new instance of an object (we just bought it)
		object GetObject();

		int ControlSlots{ get; }

		//display price of the item
		int Price{ get; }

		//display name of the item
		string Name{ get; }

		//display hue
		int Hue{ get; }

		//display id
		int ItemID{ get; }

		//amount in stock
		int Amount{ get; set; }

		//max amount in stock
		int MaxAmount{ get; }

		//Attempt to restock with item, (return true if restock sucessful)
		bool Restock( Item item, int amount );

		//called when its time for the whole shop to restock
		void OnRestock();
	}
}