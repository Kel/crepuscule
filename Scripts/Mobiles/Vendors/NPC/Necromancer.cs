using System;
using System.Collections;
using Server;
using Server.Engines;
using System.Collections.Generic;
using Server.Items;
using Server.Network;

namespace Server.Mobiles
{
    [TypeAlias("Server.Mobiles.Necromancer2")]
	public class Necromancer : BaseVendor
	{
		private ArrayList m_SBInfos = new ArrayList();
		protected override ArrayList SBInfos{ get { return m_SBInfos; } }
		public override NpcGuild NpcGuild{ get{ return NpcGuild.MagesGuild; } }

        #region Secret Tradelist Property
        private WeakChoice<TradeInfo> m_SecretTrades = new WeakChoice<TradeInfo>("SecretTradesInternalName", "SecretTradesList", "InternalName");
        public string SecretTradesInternalName { get; set; }
        public List<TradeInfo> SecretTradesList { get { return ManagerConfig.Trades; } }

        [Description("Achat/Vente (Secret)")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> SecretTrades
        {
            get { return m_SecretTrades; }
            set { LoadSBInfo(); }
        }
        #endregion

        [Constructable]
		public Necromancer() : base( "l'Herboriste" )
		{
			SetSkill( SkillName.EvalInt, 65.0, 88.0 );
			SetSkill( SkillName.Inscribe, 60.0, 83.0 );
			SetSkill( SkillName.Magery, 64.0, 100.0 );
			SetSkill( SkillName.Meditation, 60.0, 83.0 );
			SetSkill( SkillName.MagicResist, 65.0, 88.0 );
			SetSkill( SkillName.Wrestling, 36.0, 68.0 );
		}

        public Necromancer(Serial serial)
            : base(serial)
        {
        }

		public override VendorShoeType ShoeType
		{
			get{ return Utility.RandomBool() ? VendorShoeType.Shoes : VendorShoeType.Sandals; }
		}

		public override void InitOutfit()
		{
			base.InitOutfit();

			AddItem( new Server.Items.Robe( Utility.RandomBlueHue() ) );
			AddItem( new Server.Items.GnarledStaff() );
		}

        public override void OnSpeech(SpeechEventArgs e)
        {
            Mobile from = e.Mobile;
            if (from.Z - 10 < this.Z && from.Z + 10 > this.Z)
            {
                if (from.InRange(this, 4))
                {
                    var speech = e.Speech.ToLower();
                    var trades = SecretTrades.HasValue;
                    if (speech.IndexOf("bonjour") >= 0 || e.Speech.ToLower().IndexOf("salut") >= 0)
                    {
                        string message = trades ? "Bonjour � vous! Approchez, regardez mes marchandises! " : "Salutations!";
                        this.Say(message);
                    }
                    if (speech.IndexOf("acheter") >= 0 || 
                        speech.IndexOf("vendez") >= 0 || 
                        speech.IndexOf("acquerir") >= 0)
                    {
                    	if (from is RacePlayerMobile)
                    	{
                    		RacePlayerMobile acheteur = (RacePlayerMobile)from;
                    		if (acheteur.Capacities[CapacityName.Necromancy].Value >= 5 || acheteur.Capacities[CapacityName.BloodMagic].Value >= 5)
                    		{
                        		if (trades)
                            		this.SecretVendorBuy(from);
                        		else this.Say("Oh, non, d�sol�...");
                   	 		}
                    		else
                    		{
                    			if (trades)
                            		this.VendorBuy(from);
                        		else this.Say("Oh, non, d�sol�...");
                    		}
                    	}
                    }
                    if (speech.IndexOf("vendre") >= 0 ||
                        speech.IndexOf("achetez") >= 0)
                    {
                        if (trades)
                            this.VendorSell(from);
                        else this.Say("Oh, non, d�sol�...");
                    }
                }
            }   
        }

        public virtual void SecretVendorBuy(Mobile from)
        {
            if (!IsActiveSeller)
                return;

            if (!from.CheckAlive())
                return;

            int count = 0;


            if (!SecretTrades.HasValue)
                return; // No list!

            var buyInfo  = (IBuyItemInfo[])SecretTrades.Value.BuyInfo.ToArray(typeof(IBuyItemInfo));
            var sellInfo = new IShopSellInfo[] { Trades.Value.SellInfo };
            List<BuyItemState> list = new List<BuyItemState>(buyInfo.Length);
            Container cont = this.BuyPack;

            for (int idx = 0; idx < buyInfo.Length; idx++)
            {
                IBuyItemInfo buyItem = (IBuyItemInfo)buyInfo[idx];
                if (buyItem.Amount > 0)
                {
                    if (list.Count < 250)
                    {
                        list.Add(new BuyItemState(buyItem.Name, cont.Serial, 0x7FFFFFFF - idx, buyItem.Price, buyItem.Amount, buyItem.ItemID, buyItem.Hue));
                        count++;
                    }
                }
            }

            var playerItems = cont.Items;
            for (int i = 0; i < playerItems.Count; ++i)
            {
                Item item = (Item)playerItems[i];

                int price = 0;
                string name = null;

                foreach (IShopSellInfo ssi in sellInfo)
                {
                    if (ssi.IsSellable(item))
                    {
                        price = ssi.GetBuyPriceFor(item);
                        name = ssi.GetNameFor(item);
                        break;
                    }
                }

                if (name != null && list.Count < 250)
                {
                    list.Add(new BuyItemState(name, cont.Serial, item.Serial, price, item.Amount, item.ItemID, item.Hue));
                    count++;
                }
            }

            if (list.Count > 0)
            {
   
                list.Sort(new BuyItemStateComparer());

                SendPacksTo(from);

                NetState ns = from.NetState;

                if (ns == null)
                    return;

                if (ns.ContainerGridLines)
                    from.Send(new VendorBuyContent6017(list));
                else
                    from.Send(new VendorBuyContent(list));

                from.Send(new VendorBuyList(this, list));

                if (ns.HighSeas)
                    from.Send(new DisplayBuyListHS(this));
                else
                    from.Send(new DisplayBuyList(this));

                from.Send(new MobileStatusExtended(from));//make sure their gold amount is sent

                /*if (opls != null)
                {
                    for (int i = 0; i < opls.Count; ++i)
                    {
                        from.Send(opls[i]);
                    }
                }*/

                SayTo(from, "Pssst. Regardez ici...");
            }
        }



		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 1 ); // version

            // Version 1
            writer.Write(SecretTradesInternalName);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
            switch (version)
            {
                case 1:
                    {
                        SecretTradesInternalName = reader.ReadString();
                        SecretTrades.SetValueByKey(this, SecretTradesInternalName);
                        break;
                    }
            }

		}
	}
}