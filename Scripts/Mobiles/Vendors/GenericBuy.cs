using System;
using System.Collections;
using Server.Items;
using Server.Engines;

namespace Server.Mobiles
{
    [Description("Achat")]
	public class GenericBuyInfo : IBuyItemInfo
	{
		private Type m_Type;
		private string m_Name;
		private int m_Price;
		private int m_MaxAmount, m_Amount;
		private int m_Hue;
        private int m_ItemID;

		public virtual int ControlSlots{ get{ return 0; } }

		public Type Type
		{
			get{ return m_Type; }
			set{ m_Type = value; }
		}

		public string Name
		{
			get{ return m_Name; }
			set{ m_Name = value; }
		}

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Prix")]
		public int Price
		{
			get{ return m_Price; }
			set{ m_Price = value; }
		}

        [CommandProperty(AccessLevel.GameMaster)]
		public int ItemID
		{
			get{ return m_ItemID; }
		}
        [CommandProperty(AccessLevel.GameMaster)]
		public int Hue
		{
			get{ return m_Hue; }
			set{ m_Hue = value; }
		}
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Quantit�")]
		public int Amount
		{
			get{ return m_Amount; }
			set{ if ( value < 0 ) value = 0; m_Amount = value; }
		}
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Quantit� Max.")]
		public int MaxAmount
		{
			get{ return m_MaxAmount; }
			set{ m_MaxAmount = value; }
		}


        public GenericBuyInfo() : this("", null, 1, 1, 1, 0, 0)
        {
        }

		public GenericBuyInfo( Type type, int price, int amount, int itemID, int hue ) : this( null, type, price, amount, itemID, amount, hue )
		{
		}

        public GenericBuyInfo(string name, Type type, int price, int amount, int itemID, int hue): this(name, type, price, amount, amount, itemID, hue)
		{
		}

        public GenericBuyInfo(Type type, int price, int amount, int itemID, int hue, object[] args): this(null, type, price, amount, amount, itemID, hue)
		{
		}

        public GenericBuyInfo(string name, Type type, int price, int amount, int maxAmount, int itemID, int hue)
        {
            amount = 20;

            m_Type = type;
            m_Price = price;
            m_MaxAmount = maxAmount;
            m_Amount = amount;
            //m_Args = args;

            //if ( name == null )
            //	m_Name = (1020000 + (itemID & 0x3FFF)).ToString();
            //else
            //	m_Name = name;

            var git = GameItemType.FindType(type);
            if (git != null)
            {
                m_Name = git.Name;
                m_ItemID = git.ItemID;
                m_Hue = git.Hue;
            }
            else
            {
                m_Name = name;
                m_ItemID = itemID;
                m_Hue = hue;
            }
        }

		//get a new instance of an object (we just bought it)
		public virtual object GetObject()
		{
            object instance = Activator.CreateInstance(m_Type);
            if (instance != null && instance is CrepusculeItem)
                (instance as CrepusculeItem).OnAssignProperties();
            return instance;
		}

		//Attempt to restock with item, (return true if restock sucessful)
		public bool Restock( Item item, int amount )
		{
			return false;
			/*if ( item.GetType() == m_Type )
			{
				if ( item is BaseWeapon )
				{
					BaseWeapon weapon = (BaseWeapon)item;

					if ( weapon.Quality == WeaponQuality.Low || weapon.Quality == WeaponQuality.Exceptional || (int)weapon.DurabilityLevel > 0 || (int)weapon.DamageLevel > 0 || (int)weapon.AccuracyLevel > 0 )
						return false;
				}

				if ( item is BaseArmor )
				{
					BaseArmor armor = (BaseArmor)item;

					if ( armor.Quality == ArmorQuality.Low || armor.Quality == ArmorQuality.Exceptional || (int)armor.Durability > 0 || (int)armor.ProtectionLevel > 0 )
						return false;
				}

				m_Amount += amount;

				return true;
			}
			else
			{
				return false;
			}*/
		}

		public void OnRestock()
		{
			if ( m_Amount <= 0 )
			{
				m_MaxAmount *= 2;

				if ( m_MaxAmount >= 999 )
					m_MaxAmount = 999;
			}
			else
			{
				/* NOTE: According to UO.com, the quantity is halved if the item does not reach 0
				 * Here we implement differently: the quantity is halved only if less than half
				 * of the maximum quantity was bought. That is, if more than half is sold, then
				 * there's clearly a demand and we should not cut down on the stock.
				 */

				int halfQuantity = m_MaxAmount;

				if ( halfQuantity >= 999 )
					halfQuantity = 640;
				else if ( halfQuantity > 20 )
					halfQuantity /= 2;

				if ( m_Amount >= halfQuantity )
					m_MaxAmount = halfQuantity;
			}

			m_Amount = m_MaxAmount;
		}
	}
}