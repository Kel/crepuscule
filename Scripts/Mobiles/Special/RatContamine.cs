using System;
using Server.Mobiles;

namespace Server.Mobiles
{
	[CorpseName( "corps d'un rat contaminé" )]
	public class RatContamine : BaseCreature
	{
		[Constructable]
		public RatContamine() : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4 )
		{
			Name = "rat contaminé";
			Body = 238;
			BaseSoundID = 0xCC;

			SetStr( 9 );
			SetDex( 35 );
			SetInt( 6, 10 );

			SetHits( 6 );
			SetMana( 0 );

			SetDamage( 1, 2 );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, 5, 10 );
			SetResistance( ResistanceType.Poison, 15, 25 );
			SetResistance( ResistanceType.Energy, 5, 10 );

			SetSkill( SkillName.MagicResist, 5.0 );
			SetSkill( SkillName.Tactics, 5.0 );
			SetSkill( SkillName.Wrestling, 5.0 );

			

			VirtualArmor = 6;

			Tamable = true;
			ControlSlots = 1;
			MinTameSkill = -0.9;
		}

		public override void GenerateLoot()
		{
			
		}

        public override bool HasBreath { get { return false; } } // fire breath enabled
        public override int Meat { get { return 1; } }
        public override FoodType FavoriteFood { get { return FoodType.Meat; } }
        public override PackInstinct PackInstinct { get { return PackInstinct.Canine; } }

        public override void OnDamage(int amount, Mobile from, bool willKill)
        {
            if (from is RacePlayerMobile)
            {

                int chance = Utility.Random(0, 100);

                if (chance > 35)
                {
                    RacePlayerMobile pm = (RacePlayerMobile)from;
                    pm.Sickness = SickFlag.Virus;
                    pm.Sick = true;
                }
            }
            base.OnDamage(amount, from, willKill);
        }
		public RatContamine(Serial serial) : base(serial)
		{
		}

		public override void Serialize(GenericWriter writer)
		{
			base.Serialize(writer);

			writer.Write((int) 0);
		}

		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize(reader);

			int version = reader.ReadInt();
		}
	}
}