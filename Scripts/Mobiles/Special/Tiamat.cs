using System;
using Server;
using Server.Items;
using System.Collections;

namespace Server.Mobiles
{
	[CorpseName( "un cadavre de monstre" )]
	public class Tiamat : BaseCreature
	{

		public override WeaponAbility GetWeaponAbility()
		{
			switch ( Utility.Random( 2 ) )
			{
				default:
				case 0: return WeaponAbility.DoubleStrike;
				case 1: return WeaponAbility.WhirlwindAttack;
			}
		}


		[Constructable]
		public Tiamat() : base( AIType.AI_Mage, FightMode.Closest, 10, 1, 0.1, 0.2 )
		{
			Name = "Tiamat";
			Body = 172;
			Hue = 0x87C;
			BaseSoundID = 362;

			SetStr( 1800, 2000 );
			SetDex( 250, 350 );
			SetInt( 500, 600 );

			SetHits( 35000 );
			SetStam( 200, 350 );
			SetMana( 500, 600 );

			SetDamage( 45, 50 );

			SetDamageType( ResistanceType.Cold, 25 );
			SetDamageType( ResistanceType.Fire, 25 );
			SetDamageType( ResistanceType.Energy, 25 );
			SetDamageType( ResistanceType.Poison, 25 );

			SetResistance( ResistanceType.Physical, 80, 90 );
			SetResistance( ResistanceType.Fire, 80, 90 );
			SetResistance( ResistanceType.Cold, 70, 80 );
			SetResistance( ResistanceType.Poison, 80, 90 );
			SetResistance( ResistanceType.Energy, 80, 90 );

			SetSkill( SkillName.EvalInt, 160.1, 180.0 );
			SetSkill( SkillName.Magery, 160.1, 180.0 );
			SetSkill( SkillName.Focus, 300.1, 310.0 );
			SetSkill( SkillName.Meditation, 140.1, 150.0 );
			SetSkill( SkillName.MagicResist, 160.1, 180.0 );
			SetSkill( SkillName.Tactics, 120.1, 130.0 );
			SetSkill( SkillName.Anatomy, 120.1, 130.0 );
			SetSkill( SkillName.Wrestling, 120.1, 130.0 );

			Fame = 50000;
			Karma = 50000;

			VirtualArmor = 200;
		}


		public override int GetIdleSound()
		{
			return 0x2D5;
		}

		public override int GetHurtSound()
		{
			return 0x2D1;
		}

		public override bool HasBreath{ get{ return true; } } // fire breath enabled
		public override bool AutoDispel{ get{ return true; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lethal; } }
		public override Poison HitPoison{ get{ return Poison.Lethal; } }
		public override bool IsScaryToPets{ get{ return true; } }
		
		public override void CheckReflect( Mobile caster, ref bool reflect )
		{
			reflect = true; // Every spell is reflected back to the caster
		}


		public void DrainLife()
		{
			ArrayList list = new ArrayList();

			foreach ( Mobile m in this.GetMobilesInRange( 10 ) )
			{
				if ( m == this || !CanBeHarmful( m ) )
					continue;

				if ( m is BaseCreature && (((BaseCreature)m).Controled || ((BaseCreature)m).Summoned || ((BaseCreature)m).Team != this.Team) )
					list.Add( m );
				else if ( m.Player )
					list.Add( m );
			}

			foreach ( Mobile m in list )
			{
				DoHarmful( m );

				m.FixedParticles( 0x374A, 10, 15, 5013, 0x496, 0, EffectLayer.Waist );
				m.PlaySound( 0x231 );

				m.SendMessage( "Tiamat semble absorber votre vie!" );

				int toDrain = Utility.RandomMinMax( 40, 50 );

				Hits += toDrain;
				m.Damage( toDrain, this );
			}
		}
		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.25 >= Utility.RandomDouble() )
				DrainLife();
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			if ( 0.25 >= Utility.RandomDouble() )
				DrainLife();
		}


		public override int Meat{ get{ return 40; } }
		public override int Hides{ get{ return 100; } }
		public override int Scales{ get{ return 20; } }
		public override ScaleType ScaleType{ get{ return ScaleType.All; } }
		public override HideType HideType{ get{ return HideType.Barbed; } }

		public Tiamat( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}