using System;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server.Mobiles
{
	[CorpseName( "une poule morte" )]
	public class Poule : BaseCreature
	{
		[Constructable]
		public Poule() : base( AIType.AI_Animal, FightMode.Agressor, 10, 1, 0.2, 0.4 )
		{
			Name = "une poule";
			Body = 0xD0;
			Hue = 2062;
			BaseSoundID = 0x6E;

			SetStr( 5 );
			SetDex( 15 );
			SetInt( 5 );

			SetHits( 3 );
			SetMana( 0 );

			SetDamage( 1 );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, 1, 5 );

			SetSkill( SkillName.MagicResist, 4.0 );
			SetSkill( SkillName.Tactics, 5.0 );
			SetSkill( SkillName.Wrestling, 5.0 );

			Fame = 150;
			Karma = 0;

			VirtualArmor = 2;

			Tamable = true;
			ControlSlots = 1;
			MinTameSkill = 5;
			
			timer_Pond = new Timer_Pond(this);
            timer_Pond.Debuter();
		}

		public override int Meat{ get{ return 2; } }
		public override MeatType MeatType{ get{ return MeatType.Chicken; } }
		public override int Feathers{ get{ return 5; } }
		public override FoodType FavoriteFood{ get{ return FoodType.GrainsAndHay; } }

        private double m_Temps;
        private Timer_Pond timer_Pond;

       [CommandProperty(AccessLevel.GameMaster)]
       public double Temps
       {
           get { return m_Temps; }
           set { m_Temps = value; }
       }
		
		public Poule(Serial serial) : base(serial)
		{
		}

		public override void Serialize(GenericWriter writer)
		{
			base.Serialize(writer);

			writer.Write((int) 0);
			writer.Write(m_Temps);
		}

		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize(reader);

			int version = reader.ReadInt();
			m_Temps = reader.ReadDouble();
			timer_Pond = new Timer_Pond(this);
         	timer_Pond.Debuter();
		}
		
		public class Timer_Pond : Timer
        {
           private Poule m_item;

           public void Arreter()
           {
               this.Stop();
           }
           public void Debuter()
           {
               this.Start();
           }

           public Timer_Pond(Poule from)
               : base(TimeSpan.Zero, TimeSpan.FromHours(1.0))
           {
               m_item = (Poule)from;
               m_item.m_Temps = Utility.Random(2, 2);
               this.Start();
           }
           
           protected override void OnTick()
           {
           	 	if( m_item == null || m_item.Deleted )
            	{
                	Stop();
                	return;
            	}
                if (m_item.m_Temps <= 0)
                {
           			m_item.m_Temps = Utility.Random(2,2);
           			Item item = new Eggs();
					item.MoveToWorld(m_item.Location, m_item.Map);
                }
                else
                {
                   m_item.m_Temps -= 1;
                }

           }
        }
	}
}
	
	
