using System;
using Server;
using Server.Mobiles;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "un cadavre de vache" )]
	public class Cow : BaseCreature
	{
		[Constructable]
		public Cow() : base( AIType.AI_Animal, FightMode.Agressor, 10, 1, 0.2, 0.4 )
		{
			Name = "une vache";
			Body = Utility.RandomList( 0xD8, 0xE7 );
			BaseSoundID = 0x78;

			SetStr( 30 );
			SetDex( 15 );
			SetInt( 5 );

			SetHits( 18 );
			SetMana( 0 );

			SetDamage( 1, 4 );

			SetDamage( 1, 4 );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, 5, 15 );

			SetSkill( SkillName.MagicResist, 5.5 );
			SetSkill( SkillName.Tactics, 5.5 );
			SetSkill( SkillName.Wrestling, 5.5 );

			Fame = 300;
			Karma = 0;

			VirtualArmor = 10;

			Tamable = true;
			ControlSlots = 1;
			MinTameSkill = 20;
			
			timer_Traite = new Timer_Traite(this);
            timer_Traite.Debuter();

			if ( Core.AOS && Utility.Random( 1000 ) == 0 ) // 0.1% chance to have mad cows
				FightMode = FightMode.Closest;
		}

		public override int Meat{ get{ return 8; } }
		public override int Hides{ get{ return 12; } }
		public override FoodType FavoriteFood{ get{ return FoodType.FruitsAndVegies | FoodType.GrainsAndHay; } }
		
		private bool m_Traite;
        private double m_Temps;
        private Timer_Traite timer_Traite;
        
        [CommandProperty(AccessLevel.GameMaster)]
        public bool Traite
        {
           get { return m_Traite; }
           set { m_Traite = value; }
        }


       [CommandProperty(AccessLevel.GameMaster)]
       public double Temps
       {
           get { return m_Temps; }
           set { m_Temps = value; }
       }

		public override void OnDoubleClick( Mobile from )
		{
			base.OnDoubleClick( from );

			int random = Utility.Random( 100 );

			if ( random < 5 )
				Tip();
			else if ( random < 20 )
				PlaySound( 120 );
			else if ( random < 40 )
				PlaySound( 121 );
			if( from.InRange(this.Location,1) )
			{
				if (this.m_Traite == true)
				{
					foreach ( Item vide in from.Backpack.Items )
					{
						if (vide is Pitcher)
						{
							Pitcher pichet = (Pitcher)vide; 
							if (pichet.IsEmpty == true)
							{
								if (this.m_Traite == true)
								{
					   				pichet.Delete();
									Pitcher itemadd = new Pitcher( BeverageType.Milk );
									from.AddToBackpack(itemadd);
									this.m_Traite = false;
									from.SendMessage("Un chaud liquide blanch�tre remplit votre pichet!"); 
									this.timer_Traite.Debuter();
									break;
								}
							}
						}
					}
					if (this.m_Traite == true) from.SendMessage("Vous devez avoir un pichet vide.");
				}
				else from.SendMessage("L'animal semble ne pas �tre pr�t pour la traite.");
			}
		}

		public void Tip()
		{
			PlaySound( 121 );
			Animate( 8, 0, 3, true, false, 0 );
		}

		public Cow(Serial serial) : base(serial)
		{
		}

		public override void Serialize(GenericWriter writer)
		{
			base.Serialize(writer);

			writer.Write((int) 1);
			writer.Write(m_Traite);
			writer.Write(m_Temps);
		}

		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize(reader);

			int version = reader.ReadInt();
			
			if (version == 1) 
			{
				m_Traite = reader.ReadBool();
				m_Temps = reader.ReadDouble();
				timer_Traite = new Timer_Traite(this);
         		timer_Traite.Debuter();
			}
		}
		public class Timer_Traite : Timer
       {
           private Cow who;
           private Cow m_item;

           public void Arreter()
           {
               this.Stop();
           }
           public void Debuter()
           {
               m_item = (Cow)who;
               
               this.Start();
           }

           // Apres 2.5 secondes, le timer sera declanch� tout les secondes. apres 5 ticks, �a s'arretera
           public Timer_Traite(Cow from)
               : base(TimeSpan.Zero, TimeSpan.FromHours(1.0))
           {
               m_item = (Cow)from;
               who = (Cow)from;
               m_item.m_Temps = Utility.Random(2,7);
               this.Start();
           }

          

 

           protected override void OnTick()
           {
           	    if( m_item == null || m_item.Deleted )
            	{
                	Stop();
                	return;
            	}
               if (m_item.m_Temps <= 0)
               {

                   m_item.m_Temps = Utility.Random(2,7);
                   if (m_item.m_Traite == false)
                   {
							 this.Stop();
                       m_item.m_Traite = true;
                   }  
               }
               else
               {
                   m_item.m_Temps -= 1;
               }

           }
       }
	}
}
