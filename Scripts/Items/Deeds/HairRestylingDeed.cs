using System;
using Server.Mobiles;
using Server.Network;
using Server.Prompts;
using Server.Items;
using Server.Targeting;
using Server.Gumps;

namespace Server.Items
{
	public class HairRestylingDeed: CrepusculeItem
	{
		public override int LabelNumber{ get{ return 1041061; } } // a coupon for a free hair restyling

		[Constructable]
		public HairRestylingDeed() : base( 0x14F0 )
		{
			Name="Changement de coiffure. Rem: faitez le en rp";
			Weight = 1.0;
			LootType = LootType.Blessed;
		}

		public HairRestylingDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
			else
				from.SendGump( new InternalGump( from, this ) );
		}

		private class InternalGump : Gump
		{
			private Mobile m_From;
			private HairRestylingDeed m_Deed;

			public InternalGump( Mobile from, HairRestylingDeed deed ) : base( 50, 50 )
			{
				m_From = from;
				m_Deed = deed;

				from.CloseGump( typeof( InternalGump ) );

				AddBackground( 100, 10, 400, 385, 0xA28 );

				AddHtmlLocalized( 100, 25, 400, 35, 1013008, false, false );
				AddButton( 175, 340, 0xFA5, 0xFA7, 0x0, GumpButtonType.Reply, 0 ); // CANCEL

				AddHtmlLocalized( 210, 342, 90, 35, 1011012, false, false );// <CENTER>HAIRSTYLE SELECTION MENU</center>
				
				/*
				AddBackground( 220, 60, 50, 50, 0xA3C );
				AddBackground( 220, 115, 50, 50, 0xA3C );
				AddBackground( 220, 170, 50, 50, 0xA3C );
				AddBackground( 220, 225, 50, 50, 0xA3C );
				AddBackground( 425, 60, 50, 50, 0xA3C );
				AddBackground( 425, 115, 50, 50, 0xA3C );
				AddBackground( 425, 170, 50, 50, 0xA3C );
				AddBackground( 425, 225, 50, 50, 0xA3C );
				AddBackground( 425, 280, 50, 50, 0xA3C );
				AddBackground( 425, 335, 50, 50, 0xA3C );

				AddHtmlLocalized( 150, 75, 80, 35, 1011052, false, false ); // Short
				AddHtmlLocalized( 150, 130, 80, 35, 1011053, false, false ); // Long
				AddHtmlLocalized( 150, 185, 80, 35, 1011054, false, false ); // Ponytail
				AddHtmlLocalized( 150, 240, 80, 35, 1011055, false, false ); // Mohawk
				AddHtmlLocalized( 355, 75, 80, 35, 1011047, false, false ); // Pageboy
				AddHtmlLocalized( 355, 130, 80, 35, 1011048, false, false ); // Receding
				AddHtmlLocalized( 355, 185, 80, 35, 1011049, false, false ); // 2-tails
				AddHtmlLocalized( 355, 240, 80, 35, 1011050, false, false ); // Topknot
				AddHtmlLocalized( 355, 295, 80, 35, 1011064, false, false ); // Bald
				AddHtmlLocalized( 355, 250, 80, 35, 1011064, false, false ); // Bald
				
				AddImage( 153, 20, 0xC60C );
				AddImage( 153, 65, 0xED24 );
				AddImage( 153, 120, 0xED1E );
				AddImage( 153, 185, 0xC60F );
				AddImage( 358, 18, 0xED26 );
				AddImage( 358, 75, 0xEDE5 );
				AddImage( 358, 120, 0xED23 );
				AddImage( 362, 190, 0xED29 );
				AddImage( 362, 260, 0x368C );
				*/

				AddHtml( 170, 70, 80, 35, String.Format("Chauve"), false, false ); // Bald
				AddHtml( 170, 90, 80, 35, String.Format("Longues"), false, false ); // Bald
				AddHtml( 170, 110, 80, 35, String.Format("Ponytail"), false, false ); // Bald
				AddHtml( 170, 130, 80, 35, String.Format("Mohawk"), false, false ); // Bald
				AddHtml( 170, 150, 80, 35, String.Format("Pageboy"), false, false ); // Bald
				AddHtml( 170, 170, 80, 35, String.Format("Receding"), false, false ); // Bald
				AddHtml( 170, 190, 80, 35, String.Format("2-tails"), false, false ); // Bald
				AddHtml( 170, 210, 80, 35, String.Format("Topknot"), false, false ); // Bald
				AddHtml( 170, 230, 80, 35, String.Format("Tres Longues"), false, false ); // Bald
				AddHtml( 170, 250, 80, 35, String.Format("Courtes "), false, false ); // Bald
	
				AddHtml( 300, 70, 80, 35, String.Format("Plats"), false, false ); // Bald
				AddHtml( 300, 90, 80, 35, String.Format("Piques "), false, false ); // Bald	
				AddHtml( 300, 110, 80, 35, String.Format("CalvLong"), false, false ); // Bald	
				AddHtml( 300, 130, 80, 35, String.Format("Elegants"), false, false ); // Bald	
				AddHtml( 300, 150, 80, 35, String.Format("Lulu"), false, false ); // Bald	
				AddHtml( 300, 170, 80, 35, String.Format("Raides"), false, false ); // Bald	
				AddHtml( 300, 190, 80, 35, String.Format("Lisses"), false, false ); // Bald	
				AddHtml( 300, 210, 80, 35, String.Format("Couette"), false, false ); // Bald	
				AddHtml( 300, 230, 80, 35, String.Format("Toupet"), false, false ); // Bald	

				AddButton( 130, 70, 0xFA5, 0xFA7, 1, GumpButtonType.Reply, 0 );
				AddButton( 130, 90, 0xFA5, 0xFA7, 3, GumpButtonType.Reply, 0 );
				AddButton( 130, 110, 0xFA5, 0xFA7, 4, GumpButtonType.Reply, 0 );
				AddButton( 130, 130, 0xFA5, 0xFA7, 5, GumpButtonType.Reply, 0 );
				AddButton( 130, 150, 0xFA5, 0xFA7, 6, GumpButtonType.Reply, 0 );
				AddButton( 130, 170, 0xFA5, 0xFA7, 7, GumpButtonType.Reply, 0 );
				AddButton( 130, 190, 0xFA5, 0xFA7, 8, GumpButtonType.Reply, 0 );
				AddButton( 130, 210, 0xFA5, 0xFA7, 9, GumpButtonType.Reply, 0 );
				AddButton( 130, 230, 0xFA5, 0xFA7, 10, GumpButtonType.Reply, 0 );

				AddButton( 260, 70, 0xFA5, 0xFA7, 11, GumpButtonType.Reply, 0 );
				AddButton( 260, 90, 0xFA5, 0xFA7, 12, GumpButtonType.Reply, 0 );
				AddButton( 260, 110, 0xFA5, 0xFA7, 13, GumpButtonType.Reply, 0 );
				AddButton( 260, 130, 0xFA5, 0xFA7, 14, GumpButtonType.Reply, 0 );
				AddButton( 260, 150, 0xFA5, 0xFA7, 15, GumpButtonType.Reply, 0 );
				AddButton( 260, 170, 0xFA5, 0xFA7, 16, GumpButtonType.Reply, 0 );
				AddButton( 260, 190, 0xFA5, 0xFA7, 17, GumpButtonType.Reply, 0 );
				AddButton( 260, 210, 0xFA5, 0xFA7, 18, GumpButtonType.Reply, 0 );
				AddButton( 260, 230, 0xFA5, 0xFA7, 19, GumpButtonType.Reply, 0 );

				AddButton( 130, 250, 0xFA5, 0xFA7, 2, GumpButtonType.Reply, 0 );
			}

			public override void OnResponse( NetState sender, RelayInfo info )
			{
				if ( m_Deed.Deleted )
					return;

				Item newHair = null;

				switch ( info.ButtonID )
				{
					case 0: return;
					case 2: newHair = new ShortHair(); break;
					case 3: newHair = new LongHair(); break;
					case 4: newHair = new PonyTail(); break;
					case 5: newHair = new Mohawk(); break;
					case 6: newHair = new PageboyHair(); break;
					case 7: newHair = new ReceedingHair(); break;
					case 8: newHair = new TwoPigTails(); break;
					case 9: newHair = new KrisnaHair(); break;
					case 10: newHair = new TresLongues(); break;
					case 11: newHair = new PlatChev(); break;
					case 12: newHair = new PiquesChev(); break;
					case 13: newHair = new CalvLong(); break;
					case 14: newHair = new Albator(); break;
					case 15: newHair = new Lulu(); break;
					case 16: newHair = new Raide(); break;
					case 17: newHair = new ChevLisse(); break;
					case 18: newHair = new ChevCouette(); break;
					case 19: newHair = new ChevToupet(); break;
				}

				if ( m_From is PlayerMobile )
				{
					PlayerMobile pm = (PlayerMobile)m_From;

					pm.SetHairMods( -1, -1 ); // clear any hairmods (disguise kit, incognito)
				}

				Item oldHair = m_From.FindItemOnLayer( Layer.Hair );

				if ( oldHair != null )
					oldHair.Delete();

				if ( newHair != null )
				{
					if ( oldHair != null )
						newHair.Hue = oldHair.Hue;

					m_From.AddItem( newHair );
				}

				m_Deed.Delete();
			}
		}
	}
}