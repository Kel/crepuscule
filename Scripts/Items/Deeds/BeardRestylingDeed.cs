/////////////////////////////////
// Beard Restyling Deed        //
// Scripteur:  Apoc            //
// Serveur: Cr�puscule         //
// 27 Juillet 2008             //
// Bas� sur HairRestylingDeed  //
/////////////////////////////////
using System;
using Server.Mobiles;
using Server.Network;
using Server.Prompts;
using Server.Items;
using Server.Targeting;
using Server.Gumps;

namespace Server.Items
{
	public class BeardRestylingDeed: CrepusculeItem
	{
		[Constructable]
		public BeardRestylingDeed() : base( 0x14F0 )
		{
			Name="Changement de Barbe. Rem: faitez le en rp";
			Weight = 1.0;
			LootType = LootType.Blessed;
		}

		public BeardRestylingDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
			else
				from.SendGump( new InternalGump( from, this ) );
		}

		private class InternalGump : Gump
		{
			private Mobile m_From;
			private BeardRestylingDeed m_Deed;

			public InternalGump( Mobile from, BeardRestylingDeed deed ) : base( 50, 50 )
			{
				m_From = from;
				m_Deed = deed;

				from.CloseGump( typeof( InternalGump ) );

				AddBackground( 100, 10, 500, 385, 0xA28 );

				AddHtml( 200, 25, 400, 35, String.Format("Choisissez votre nouvelle barbe"), false, false );
				AddButton( 175, 340, 0xFA5, 0xFA7, 0x0, GumpButtonType.Reply, 0 );
				AddHtml( 210, 342, 90, 35, String.Format("ANNULER"), false, false );
				
				AddHtml( 170, 70, 180, 35, String.Format("Ras�"), false, false );
				AddHtml( 170, 90, 180, 35, String.Format("Longue"), false, false ); 
				AddHtml( 170, 110, 180, 35, String.Format("Longue avec moustache"), false, false ); 
				AddHtml( 170, 130, 180, 35, String.Format("Courte avec moustache"), false, false ); 
				AddHtml( 170, 150, 180, 35, String.Format("Courte"), false, false ); 
				AddHtml( 170, 170, 180, 35, String.Format("Moustache"), false, false ); 
				AddHtml( 170, 190, 180, 35, String.Format("Goatee"), false, false ); // En Francais?
				AddHtml( 170, 210, 180, 35, String.Format("Vandyke"), false, false );
				
				AddHtml( 170, 230, 180, 35, String.Format("Jeune"), false, false ); // Noms?
				AddHtml( 170, 250, 180, 35, String.Format("Ancestrale"), false, false ); 
				AddHtml( 400, 70, 180, 35, String.Format("Mince"), false, false ); 
				AddHtml( 400, 90, 180, 35, String.Format("Tresses et barbe"), false, false ); 	
				AddHtml( 400, 110, 180, 35, String.Format("Tresses"), false, false ); 	
				AddHtml( 400, 130, 180, 35, String.Format("Moustache Mouske"), false, false ); // Noms?	

				AddButton( 130, 70, 0xFA5, 0xFA7, 1, GumpButtonType.Reply, 0 );
				AddButton( 130, 90, 0xFA5, 0xFA7, 2, GumpButtonType.Reply, 0 );
				AddButton( 130, 110, 0xFA5, 0xFA7, 3, GumpButtonType.Reply, 0 );
				AddButton( 130, 130, 0xFA5, 0xFA7, 4, GumpButtonType.Reply, 0 );
				AddButton( 130, 150, 0xFA5, 0xFA7, 5, GumpButtonType.Reply, 0 );
				AddButton( 130, 170, 0xFA5, 0xFA7, 6, GumpButtonType.Reply, 0 );
				AddButton( 130, 190, 0xFA5, 0xFA7, 7, GumpButtonType.Reply, 0 );
				AddButton( 130, 210, 0xFA5, 0xFA7, 8, GumpButtonType.Reply, 0 );
				
				AddButton( 130, 230, 0xFA5, 0xFA7, 9, GumpButtonType.Reply, 0 );
				AddButton( 130, 250, 0xFA5, 0xFA7, 10, GumpButtonType.Reply, 0 );
				AddButton( 360, 70, 0xFA5, 0xFA7, 11, GumpButtonType.Reply, 0 );
				AddButton( 360, 90, 0xFA5, 0xFA7, 12, GumpButtonType.Reply, 0 );
				AddButton( 360, 110, 0xFA5, 0xFA7, 13, GumpButtonType.Reply, 0 );
				AddButton( 360, 130, 0xFA5, 0xFA7, 14, GumpButtonType.Reply, 0 );
			}

			public override void OnResponse( NetState sender, RelayInfo info )
			{
				if ( m_Deed.Deleted )
					return;

				Item newBeard = null;

				switch ( info.ButtonID )
				{
					case 0: return;
					case 2: newBeard = new LongBeard(); break;
					case 3: newBeard = new MediumLongBeard(); break;
					case 4: newBeard = new MediumShortBeard(); break;
					case 5: newBeard = new ShortBeard(); break;
					case 6: newBeard = new Mustache(); break;
					case 7: newBeard = new Goatee(); break;
					case 8: newBeard = new Vandyke(); break;
					
					case 9: newBeard = new JeuneBarbe(); break;
					case 10: newBeard = new BarbeAncestrale(); break;
					case 11: newBeard = new BarbeMince(); break;
					case 12: newBeard = new BarbeTresse(); break;
					case 13: newBeard = new BarbeTresse2(); break;
					case 14: newBeard = new BarbeMouske(); break;
				}

				if ( m_From is PlayerMobile )
				{
					PlayerMobile pm = (PlayerMobile)m_From;

					pm.SetHairMods( -1, -1 ); // clear any hairmods (disguise kit, incognito)
				}

				Item oldBeard = m_From.FindItemOnLayer( Layer.FacialHair );

				if ( oldBeard != null )
					oldBeard.Delete();

				if ( newBeard != null )
				{
					if ( oldBeard != null )
						newBeard.Hue = oldBeard.Hue;

					m_From.AddItem( newBeard );
				}

				m_Deed.Delete();
			}
		}
	}
}
