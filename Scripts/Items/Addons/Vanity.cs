using System;
using Server;

namespace Server.Items
{
	public class VanityEastAddon : BaseAddon
	{
		public override BaseAddonDeed Deed{ get{ return new VanityEastDeed(); } }

		[Constructable]
		public VanityEastAddon()
		{
			AddComponent( new AddonComponent( 0xA45 ), 0, 0, 0 );
			AddComponent( new AddonComponent( 0xA44 ), 0, 1, 0 );
		}

        public VanityEastAddon(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class VanityEastDeed : BaseAddonDeed
	{
		public override BaseAddon Addon{ get{ return new VanityEastAddon(); } }
		public override int LabelNumber{ get{ return 1074027; } } // Vanity

		[Constructable]
		public VanityEastDeed()
		{
		}

        public VanityEastDeed(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

    public class VanitySouthAddon : BaseAddon
    {
        public override BaseAddonDeed Deed { get { return new VanitySouthDeed(); } }

        [Constructable]
        public VanitySouthAddon()
        {
            AddComponent( new AddonComponent( 0xA3D ), 0, 0, 0);
            AddComponent( new AddonComponent( 0xA3C ), 1, 0, 0);
        }

        public VanitySouthAddon(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class VanitySouthDeed : BaseAddonDeed
    {
        public override BaseAddon Addon { get { return new VanitySouthAddon(); } }
        public override int LabelNumber { get { return 1074027; } } // Vanity

        [Constructable]
        public VanitySouthDeed()
        {
        }

        public VanitySouthDeed(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}