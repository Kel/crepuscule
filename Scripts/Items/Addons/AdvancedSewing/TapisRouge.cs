using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0xAC8, 0xAc8 )]
	public class TapisRouge : BaseShirt
	{

		[Constructable]
		public TapisRouge() : base( 0xAC8 )
		{
			Weight = 5.0;
			
			Name = "Tapis";
		}

		public TapisRouge( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}