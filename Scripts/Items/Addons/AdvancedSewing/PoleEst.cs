using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x12E2, 0x12E2 )]
	public class PoleEst : BaseShirt
	{

		[Constructable]
		public PoleEst() : base( 0x12E2 )
		{
			Weight = 5.0;
			
			Name = "P�le";
		}

		public PoleEst( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}