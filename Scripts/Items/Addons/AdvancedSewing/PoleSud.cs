using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x12ED, 0x12ED )]
	public class PoleSud : BaseShirt
	{

		[Constructable]
		public PoleSud() : base( 0x12ED )
		{
			Weight = 5.0;
			
			Name = "P�le";
		}

		public PoleSud( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}