using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0xAc6, 0xAc6 )]
	public class TapisRougeM : BaseShirt
	{

		[Constructable]
		public TapisRougeM() : base( 0xAc6 )
		{
			Weight = 5.0;
			
			Name = "Tapis";
		}

		public TapisRougeM( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}