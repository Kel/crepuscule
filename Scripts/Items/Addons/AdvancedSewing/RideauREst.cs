using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x160D, 0x160D )]
	public class RideauREst : BaseShirt
	{

		[Constructable]
		public RideauREst() : base( 0x160D )
		{
			Weight = 5.0;
			Name = "Rideau";
		}

		public RideauREst( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}