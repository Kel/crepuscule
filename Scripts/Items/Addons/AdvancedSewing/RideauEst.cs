using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x12DF, 0x12DF )]
	public class RideauEst : BaseShirt
	{

		[Constructable]
		public RideauEst() : base( 0x12Df )
		{
			Weight = 5.0;
			
			Name = "Rideau";
		}

		public RideauEst( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}