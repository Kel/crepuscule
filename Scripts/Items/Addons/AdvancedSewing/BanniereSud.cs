using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x159f, 0x159f )]
	public class BanniereSud : BaseShirt
	{

		[Constructable]
		public BanniereSud() : base( 0x159f )
		{
			Weight = 5.0;
			
			Name = "Banniere";
		}

		public BanniereSud( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}