using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x12E6, 0x12E6 )]
	public class RideauSud : BaseShirt
	{

		[Constructable]
		public RideauSud() : base( 0x12E6 )
		{
			Weight = 5.0;
			
			Name = "Rideau";
		}

		public RideauSud( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}