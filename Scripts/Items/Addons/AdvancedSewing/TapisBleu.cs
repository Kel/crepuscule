using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0xABD, 0xABD )]
	public class TapisBleu : BaseShirt
	{

		[Constructable]
		public TapisBleu() : base( 0xABD )
		{
			Weight = 5.0;
			
			Name = "Tapis";
		}

		public TapisBleu( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}