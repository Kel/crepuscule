using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0xAEC, 0xAEC )]
	public class TapisBleuC : BaseShirt
	{

		[Constructable]
		public TapisBleuC() : base( 0xAEC )
		{
			Weight = 5.0;
			
			Name = "Tapis";
		}

		public TapisBleuC( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}