using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x1098, 0x1098 )]
	public class TapisGris : BaseShirt
	{

		[Constructable]
		public TapisGris() : base( 0x1098 )
		{
			Weight = 5.0;
			
			Name = "Tapis";
		}

		public TapisGris( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}