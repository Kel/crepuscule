using System;
using System.Collections;
using Server;
using Server.Multis;
using Server.Regions;
using Server.Mobiles;

namespace Server.Items
{
	public enum AddonFitResult
	{
		Valid,
		Blocked,
		NotInHouse,
		DoorsNotClosed,
		DoorTooClose
	}

	public abstract class BaseAddon: CrepusculeItem, IChopable
	{
		private ArrayList m_Components;

        [CommandProperty(AccessLevel.GameMaster)]
        public Mobile Owner {get;set;}

		public void AddComponent( AddonComponent c, int x, int y, int z )
		{
			if ( Deleted )
				return;

			m_Components.Add( c );

            c.OnAssignProperties();
			c.Addon = this;
			c.Offset = new Point3D( x, y, z );
			c.MoveToWorld( new Point3D( X + x, Y + y, Z + z ), Map );
		}

		public BaseAddon() : base( 1 )
		{
			Movable = false;
			Visible = false;

			m_Components = new ArrayList();
		}

		public virtual bool RetainDeedHue{ get{ return true; } }

		public void OnChop( Mobile from )
		{
			BaseHouse house = BaseHouse.FindHouseAt( this );

			if ( house != null && house.IsOwner( from ) && house.Addons.Contains( this ) )
			{
				Effects.PlaySound( GetWorldLocation(), Map, 0x3B3 );
				from.SendLocalizedMessage( 500461 ); // You destroy the item.

				int hue = 0;

				if ( RetainDeedHue )
				{
					for ( int i = 0; hue == 0 && i < m_Components.Count; ++i )
					{
						AddonComponent c = (AddonComponent)m_Components[i];

						if ( c.Hue != 0 )
							hue = c.Hue;
					}
				}

				Delete();

				house.Addons.Remove( this );

				BaseAddonDeed deed = Deed;

				if ( deed != null )
				{
					if ( RetainDeedHue )
						deed.Hue = hue;

					from.AddToBackpack( deed );
				}
			}
		}
		
		public override void OnDoubleClick( Mobile from )
		{
            if (Owner != null && from == Owner)
            {
                int hue = 0;
                Type cRes = null;
                if (RetainDeedHue)
                {
                    for (int i = 0; hue == 0 && i < m_Components.Count; ++i)
                    {
                        AddonComponent c = (AddonComponent)m_Components[i];
                        CrepusculeItem m = (CrepusculeItem)m_Components[i];

                        if (m.CraftResource != null)
                            cRes = m.CraftResource;
                        if (c.Hue != 0)
                            hue = c.Hue;
                    }
                }
                Delete();
                BaseAddonDeed deed = Deed;
                deed.OnAssignProperties();
                if (deed != null)
                {
                    deed.CraftResource = cRes;
                    deed.Hue = hue;
                    from.AddToBackpack(deed);
                }
            }
            else
            {
                from.SendMessage("Cet objet ne vous appartient pas");
            }
		}

		public virtual BaseAddonDeed Deed{ get{ return null; } }

		public ArrayList Components
		{
			get
			{
				return m_Components;
			}
		}

		public BaseAddon( Serial serial ) : base( serial )
		{
		}

		public AddonFitResult CouldFit( IPoint3D p, Map map, Mobile from, ref ArrayList houseList )
		{
			if ( Deleted )
				return AddonFitResult.Blocked;

			ArrayList houses = new ArrayList();

			foreach ( AddonComponent c in m_Components )
			{
				Point3D p3D = new Point3D( p.X + c.Offset.X, p.Y + c.Offset.Y, p.Z + c.Offset.Z );

				if ( !map.CanFit( p3D.X, p3D.Y, p3D.Z, c.ItemData.Height, false, true, ( c.Z == 0 ) ) )
					return AddonFitResult.Blocked;
				else if ( !CheckHouse( from, p3D, map, c.ItemData.Height, houses ) )
					return AddonFitResult.NotInHouse;
			}

			foreach ( BaseHouse house in houses )
			{
				ArrayList doors = house.Doors;

				for ( int i = 0; i < doors.Count; ++i )
				{
					BaseDoor door = doors[i] as BaseDoor;

					if ( door != null && door.Open )
						return AddonFitResult.DoorsNotClosed;

					Point3D doorLoc = door.GetWorldLocation();
					int doorHeight = door.ItemData.CalcHeight;

					foreach ( AddonComponent c in m_Components )
					{
						Point3D addonLoc = new Point3D( p.X + c.Offset.X, p.Y + c.Offset.Y, p.Z + c.Offset.Z );
						int addonHeight = c.ItemData.CalcHeight;

						if ( Utility.InRange( doorLoc, addonLoc, 1 ) && (addonLoc.Z == doorLoc.Z || ((addonLoc.Z + addonHeight) > doorLoc.Z && (doorLoc.Z + doorHeight) > addonLoc.Z)) )
							return AddonFitResult.DoorTooClose;
					}
				}
			}

			houseList = houses;
			return AddonFitResult.Valid;
		}

		public bool CheckHouse( Mobile from, Point3D p, Map map, int height, ArrayList list )
		{
			
				return true;

                //Disabled by Crepuscule
                //if ( from.AccessLevel >= AccessLevel.GameMaster )
                //return true;

                //            BaseHouse house = BaseHouse.FindHouseAt( p, map, height );

                //            if ( house == null || !house.IsOwner( from ) )
                //                return false;

                //            if ( !list.Contains( house ) )
                //                list.Add( house );

                //            return true;
		}

		public virtual void OnComponentLoaded( AddonComponent c )
		{
		}

		public override void OnLocationChange( Point3D oldLoc )
		{
			if ( Deleted )
				return;

			foreach ( AddonComponent c in m_Components )
				c.Location = new Point3D( X + c.Offset.X, Y + c.Offset.Y, Z + c.Offset.Z );
		}

		public override void OnMapChange()
		{
			if ( Deleted )
				return;

			foreach ( AddonComponent c in m_Components )
				c.Map = Map;
		}

		public override void OnAfterDelete()
		{
			base.OnAfterDelete();

			foreach ( AddonComponent c in m_Components )
				c.Delete();
		}

		public virtual bool ShareHue{ get{ return true; } }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 1 ); // version

            writer.Write(Owner);
			writer.WriteItemList( m_Components );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
                case 1:
                {
                    Owner = reader.ReadMobile();
                    m_Components = reader.ReadItemList();
                    break;
                }
				case 0:
				{
                    // TODO: simply remove
					m_Components = reader.ReadItemList();
					break;
				}
			}
		}
	}
}
