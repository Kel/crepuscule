using System;
using Server;

namespace Server.Items
{
	public class DecoAnkhEastAddon : BaseAddon
	{
		public override BaseAddonDeed Deed{ get{ return new DecoAnkhEastDeed(); } }

		[Constructable]
		public DecoAnkhEastAddon()
		{
			AddComponent( new AddonComponent( 0x3 ), 0, 0, 0 );
			AddComponent( new AddonComponent( 0x2 ), 0, 1, 0 );
		}

        public DecoAnkhEastAddon(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class DecoAnkhEastDeed : BaseAddonDeed
	{
		public override BaseAddon Addon{ get{ return new DecoAnkhEastAddon(); } }
		public override int LabelNumber{ get{ return 1049739; } } // Ankh

		[Constructable]
		public DecoAnkhEastDeed()
		{
		}

        public DecoAnkhEastDeed(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

    public class DecoAnkhSouthAddon : BaseAddon
    {
        public override BaseAddonDeed Deed { get { return new DecoAnkhSouthDeed(); } }

        [Constructable]
        public DecoAnkhSouthAddon()
        {
            AddComponent( new AddonComponent( 0x4 ), 0, 0, 0);
            AddComponent( new AddonComponent( 0x5 ), 1, 0, 0);
        }

        public DecoAnkhSouthAddon(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class DecoAnkhSouthDeed : BaseAddonDeed
    {
        public override BaseAddon Addon { get { return new DecoAnkhSouthAddon(); } }
        public override int LabelNumber { get { return 1049739; } } // Ankh

        [Constructable]
        public DecoAnkhSouthDeed()
        {
        }

        public DecoAnkhSouthDeed(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}