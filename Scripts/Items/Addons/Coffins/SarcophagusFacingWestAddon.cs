using System;
using Server;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Items
{
	public class SarcophagusFacingWestAddon : BaseAddon
	{
		public override BaseAddonDeed Deed
		{
			get
			{
				return new SarcophagusFacingWestAddonDeed();
			}
		}

		[ Constructable ]
		public SarcophagusFacingWestAddon()
		{
			AddonComponent ac;
			ac = new AddonComponent( 7267 );
			AddComponent( ac, 0, 0, 0 );
			ac = new AddonComponent( 7268 );
			AddComponent( ac, 1, 0, 0 );
			ac = new AddonComponent( 7269 );
			AddComponent( ac, 2, 0, 0 );
			ac = new AddonComponent( 7266 );
			AddComponent( ac, 0, 1, 0 );
			ac = new AddonComponent( 7265 );
			AddComponent( ac, 1, 1, 0 );
			ac = new AddonComponent( 7264 );
			AddComponent( ac, 2, 1, 0 );

		}

		public SarcophagusFacingWestAddon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

	public class SarcophagusFacingWestAddonDeed : BaseAddonDeed
	{


		public override BaseAddon Addon
		{
			get
			{
				return new SarcophagusFacingWestAddon();
			}
		}

		[Constructable]
		public SarcophagusFacingWestAddonDeed()
		{
            Name = "Sarcofage, Ouest";
		}

		public SarcophagusFacingWestAddonDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void	Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
