using System;
using Server;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Items
{
	public class CoffinFacingNorthAddon : BaseAddon
	{
		public override BaseAddonDeed Deed
		{
			get
			{
				return new CoffinFacingNorthAddonDeed();
			}
		}

		[ Constructable ]
		public CoffinFacingNorthAddon()
		{
			AddonComponent ac;
			ac = new AddonComponent( 7247 );
			AddComponent( ac, 0, 0, 0 );
			ac = new AddonComponent( 7248 );
			AddComponent( ac, 0, 1, 0 );
			ac = new AddonComponent( 7249 );
			AddComponent( ac, 0, 2, 0 );
		}

		public CoffinFacingNorthAddon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

	public class CoffinFacingNorthAddonDeed : BaseAddonDeed
	{


		public override BaseAddon Addon
		{
			get
			{
				return new CoffinFacingNorthAddon();
			}
		}

		[Constructable]
		public CoffinFacingNorthAddonDeed()
		{
            Name = "Grand Cercueil, Nord";
		}

		public CoffinFacingNorthAddonDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void	Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
