using System;
using Server;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Items
{
	public class CoffinFacingWestAddon : BaseAddon
	{
		public override BaseAddonDeed Deed
		{
			get
			{
				return new CoffinFacingWestAddonDeed();
			}
		}

		[ Constructable ]
		public CoffinFacingWestAddon()
		{
			AddonComponent ac;
			ac = new AddonComponent( 7233 );
			AddComponent( ac, 0, 0, 0 );
			ac = new AddonComponent( 7234 );
			AddComponent( ac, 1, 0, 0 );
			ac = new AddonComponent( 7235 );
			AddComponent( ac, 2, 0, 0 );
		}

		public CoffinFacingWestAddon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

	public class CoffinFacingWestAddonDeed : BaseAddonDeed
	{

		public override BaseAddon Addon
		{
			get
			{
				return new CoffinFacingWestAddon();
			}
		}

		[Constructable]
		public CoffinFacingWestAddonDeed()
		{
            Name = "Grand Cercueil, Ouest";
		}

		public CoffinFacingWestAddonDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void	Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
