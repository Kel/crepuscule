using System;
using Server;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Items
{
	public class SarcophagusFacingNorthAddon : BaseAddon
	{


		public override BaseAddonDeed Deed
		{
			get
			{
				return new SarcophagusFacingNorthAddonDeed();
			}
		}

		[ Constructable ]
		public SarcophagusFacingNorthAddon()
		{
			AddonComponent ac;
			ac = new AddonComponent( 7315 );
			AddComponent( ac, 0, 0, 0 );
			ac = new AddonComponent( 7316 );
			AddComponent( ac, 0, 1, 0 );
			ac = new AddonComponent( 7317 );
			AddComponent( ac, 0, 2, 0 );
			ac = new AddonComponent( 7314 );
			AddComponent( ac, 1, 0, 0 );
			ac = new AddonComponent( 7313 );
			AddComponent( ac, 1, 1, 0 );
			ac = new AddonComponent( 7312 );
			AddComponent( ac, 1, 2, 0 );

		}

		public SarcophagusFacingNorthAddon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

	public class SarcophagusFacingNorthAddonDeed : BaseAddonDeed
	{


		public override BaseAddon Addon
		{
			get
			{
				return new SarcophagusFacingNorthAddon();
			}
		}

		[Constructable]
		public SarcophagusFacingNorthAddonDeed()
		{
            Name = "Sarcofage, Nord";
		}

		public SarcophagusFacingNorthAddonDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void	Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
