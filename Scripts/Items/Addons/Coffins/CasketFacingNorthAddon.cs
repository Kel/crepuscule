using System;
using Server;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Items
{
	public class CasketFacingNorthAddon : BaseAddon
	{
		public override BaseAddonDeed Deed
		{
			get
			{
				return new CasketFacingNorthAddonDeed();
			}
		}

		[ Constructable ]
		public CasketFacingNorthAddon()
		{
			AddonComponent ac;
			ac = new AddonComponent( 7218 );
			AddComponent( ac, 0, 0, 0 );
			ac = new AddonComponent( 7219 );
			AddComponent( ac, 0, 1, 0 );
			ac = new AddonComponent( 7220 );
			AddComponent( ac, 0, 2, 0 );
			ac = new AddonComponent( 7217 );
			AddComponent( ac, 1, 0, 0 );
			ac = new AddonComponent( 7216 );
			AddComponent( ac, 1, 1, 0 );
			ac = new AddonComponent( 7215 );
			AddComponent( ac, 1, 2, 0 );

		}

		public CasketFacingNorthAddon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

	public class CasketFacingNorthAddonDeed : BaseAddonDeed
	{

		public override BaseAddon Addon
		{
			get
			{
				return new CasketFacingNorthAddon();
			}
		}

		[Constructable]
		public CasketFacingNorthAddonDeed()
		{
            Name = "Cercueil, Nord";
		}

		public CasketFacingNorthAddonDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void	Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
