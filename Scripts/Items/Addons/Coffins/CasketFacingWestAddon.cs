using System;
using Server;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Items
{
	public class CasketFacingWestAddon : BaseAddon
	{
		public override BaseAddonDeed Deed
		{
			get
			{
				return new CasketFacingWestAddonDeed();
			}
		}

		[ Constructable ]
		public CasketFacingWestAddon()
		{
			AddonComponent ac;
			ac = new AddonComponent( 7205 );
			AddComponent( ac, 0, 0, 0 );
			ac = new AddonComponent( 7206 );
			AddComponent( ac, 1, 0, 0 );
			ac = new AddonComponent( 7207 );
			AddComponent( ac, 2, 0, 0 );
			ac = new AddonComponent( 7204 );
			AddComponent( ac, 0, 1, 0 );
			ac = new AddonComponent( 7203 );
			AddComponent( ac, 1, 1, 0 );
			ac = new AddonComponent( 7202 );
			AddComponent( ac, 2, 1, 0 );

		}

		public CasketFacingWestAddon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

	public class CasketFacingWestAddonDeed : BaseAddonDeed
	{


		public override BaseAddon Addon
		{
			get
			{
				return new CasketFacingWestAddon();
			}
		}

		[Constructable]
		public CasketFacingWestAddonDeed()
		{
            Name = "Cercueil, Ouest";
		}

		public CasketFacingWestAddonDeed( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( 0 ); // Version
		}

		public override void	Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
