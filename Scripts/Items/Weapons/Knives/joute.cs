using System;
using Server;
using Server.Gumps;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;

namespace Server.Items
{
public class LanceJoute: Spear
{
 private Mobile m_Jouteur;
 private Mobile m_Adversaire;
 private Mobile m_Temoin;
 private bool m_EnJoute;
 private bool m_JouteurFallen;
 private bool m_AdversaireFallen;
 private int m_xx;
 private int m_yy;
 private int m_counter;

 [CommandProperty( AccessLevel.GameMaster )]
 public Mobile Jouteur{ get{ return m_Jouteur; } set{ m_Jouteur = value; } }

 [CommandProperty( AccessLevel.GameMaster )]
 public Mobile Adversaire{ get{ return m_Adversaire; } set{ m_Adversaire = value; } }

 [CommandProperty( AccessLevel.GameMaster )]
 public Mobile Temoin{ get{ return m_Temoin; } set{ m_Temoin = value; } }

 [Constructable]
 public LanceJoute()
 {
  Hue = 926;
  Name="Lance de Joute";
 }

 public LanceJoute( Serial serial ) : base( serial )
 {
 }


 public override void OnDoubleClick( Mobile from )
 {
  m_JouteurFallen=false;
  m_AdversaireFallen=false;
  if ( from.FindItemOnLayer(Layer.TwoHanded) != this )
   {
   from.SendMessage("Vous devez porter la lance de joute pour l'utiliser.");
   return;
  }
  if ( !from.Mounted )
   {
   from.SendMessage("Vous devez �tre sur une monture pour utiliser cette lance.");
   return;
  }
  m_Jouteur = from;
  m_EnJoute = false;
  from.SendMessage( "Avec qui voulez-vous jouter ?" );
  from.Target = new JouteSelector(this,false);
 }


 public void ChoisirAdversaire(Mobile adverse)
 {
  m_Jouteur.SendMessage( "{0} va donner sa r�ponse...");
  m_Jouteur.Say("*Touche la lance de {0}*",adverse.Name);
  m_Adversaire=adverse;
  m_Adversaire.SendGump( new JouteGump( this ) );
 }

 public void ValiderTemoin()
 {
  m_EnJoute = true;
  m_Jouteur.SendMessage( "Choisissez le t�moin de cette joute." );
  m_Jouteur.Target = new JouteSelector(this,true);
 }

 public void ChoisirTemoin(Mobile temoin)
 {
  m_Temoin=temoin;
  m_Jouteur.SendMessage( "Pr�parez-vous..." );
  m_Adversaire.SendMessage( "{0} a d�sign� {1} comme t�moin.",m_Jouteur.Name,temoin.Name );
  m_Adversaire.SendMessage( "Pr�parez-vous..." );
  temoin.SendGump( new JouteStartGump( this ) );
 }

 public void Jouter()
 {
  m_Jouteur.SendMessage("{0} a donn� le d�part!!!",m_Temoin.Name);
  m_Adversaire.SendMessage("{0} a donn� le d�part!!!",m_Temoin.Name);
  m_Temoin.NonlocalOverheadMessage(MessageType.Yell,1157,501054);
  if ( !m_Temoin.Mounted )
   m_Temoin.Animate( 17, 5, 1, true, false, 0 );
  m_Adversaire.Direction=m_Adversaire.GetDirectionTo(m_Jouteur.Location);
  m_Jouteur.Direction=m_Jouteur.GetDirectionTo(m_Adversaire.Location);
  switch (m_Jouteur.Direction)
  {
  case Direction.North:
   m_xx = 0;
   m_yy = -1;
   break;
  case Direction.South:
   m_xx = 0;
   m_yy = 1;
   break;
  case Direction.East:
   m_xx = 1;
   m_yy = 0;
   break;
  case Direction.West:
   m_xx = -1;
   m_yy = 0;
   break;
  case Direction.Left:
   m_xx = -1;
   m_yy = 1;
   break;
  case Direction.Right:
   m_xx = 1;
   m_yy = -1;
   break;
  case Direction.Down:
   m_xx = 1;
   m_yy = 1;
   break;
  default:
   m_xx = -1;
   m_yy = -1;
   break;
  }

  JouteTimer m_Timer;
  int distance;
  int distancex = m_Jouteur.X - m_Adversaire.X;
  int distancey = m_Jouteur.Y - m_Adversaire.Y;
  if (distancex < 0)
   distancex=-distancex;
  if (distancey < 0)
   distancey=-distancey;
  if ( distancex > distancey )
   distance = distancex;
  else
   distance = distancey;
  m_counter = distance / 2 + 2;
  m_Timer=new JouteTimer(this,distance);
  m_Timer.Start();
 }

    public void Courir()
{
 if ((int)(m_Jouteur.Direction)<128)
  m_Jouteur.Direction=128+m_Jouteur.Direction;
 if ((int)(m_Adversaire.Direction)<128)
  m_Adversaire.Direction=128+m_Adversaire.Direction;

 if (!m_JouteurFallen)
 {
  m_Jouteur.X += m_xx;
  m_Jouteur.Y += m_yy;
 }
 if (!m_AdversaireFallen)
 {
   m_Adversaire.X -= m_xx;
  m_Adversaire.Y -= m_yy;
 }

 if (m_counter == 0)
 {
  double Jouteur_joute    = m_Jouteur.Skills[SkillName.Chivalry].Value;
  double Adversaire_joute = m_Adversaire.Skills[SkillName.Chivalry].Value;
  if ((Utility.RandomDouble()*Jouteur_joute-Utility.RandomDouble()*Adversaire_joute) > 0)
  {
   Dismount(m_Adversaire);
   m_AdversaireFallen=true;
  }
  if ((Utility.RandomDouble()*Jouteur_joute-Utility.RandomDouble()*Adversaire_joute) > 0)
  {
   Dismount(m_Jouteur);
   m_JouteurFallen=true;
  }
  m_counter--;
 }
 else if (m_counter == -3)
 {
  if (m_JouteurFallen)
   m_Jouteur.Animate( 21, 5, 1, true, false, 0 );
  if (m_AdversaireFallen)
   m_Adversaire.Animate( 21, 5, 1, true, false, 0 );
 }
 m_counter--;
}

private void Dismount(Mobile m)
{
 if ( m.Mounted )
 {
  IMount mount = (IMount)m.Mount;
  mount.Rider = null;
  if ( mount is BaseMount )
  {
   ((BaseMount)mount).Direction = m.Direction;
   ((BaseMount)mount).Home=m.Location;
  }
  m.Say("*Tombe violement de sa monture*");
  m.PlaySound( 1063 );
  AOS.Damage( m, m, Utility.RandomMinMax( 1, 5 ), 0, 100, 0, 0, 0 );
 }
}

    private class JouteTimer : Timer
     {
LanceJoute m_Lance;

        public JouteTimer(LanceJoute lance, int distance) : base( TimeSpan.FromSeconds( 0.1 ), TimeSpan.FromSeconds( 0.1 ), distance )
        {
         m_Lance = lance;
 Priority = TimerPriority.TenMS;
        }

        protected override void OnTick()
        {
 m_Lance.Courir();
        }
     }

///// //// /// // / DEBUT GUMP / // /// //// /////
public class JouteGump : Gump
{
 LanceJoute m_lance;

 public JouteGump( LanceJoute lance)
  : base( 80, 150 )
 {
  m_lance=lance;
  this.Closable=false;
  this.Disposable=false;
  this.Dragable=true;
  this.Resizable=false;
  this.AddPage(0);
  this.AddBackground(2, 0, 289, 207, 9200);
  this.AddImage(-22, -28, 50936);
  this.AddLabel(48, 16, 0, m_lance.Jouteur.Name);
  this.AddLabel(48, 40, 0, @"Vous d�fie de la lance pour une joute.");
  this.AddButton(184, 152, 247, 248, (int)Buttons.non, GumpButtonType.Reply, 0);
  this.AddButton(80, 152, 241, 242, (int)Buttons.oui, GumpButtonType.Reply, 0);
  this.AddLabel(71, 122, 0, @"Acceptez-vous cette joute ?");
  //this.AddHtml( 49, 86, 217, 16, "<center><a href=\"http://loiseaudejade.free.fr/joute.html\">Lire les r�gles</a></center>", (bool)false, (bool)false);

 }
 
 public enum Buttons
 {
  oui,
  non
 }
 public override void OnResponse( NetState sender, RelayInfo info )
 {
  Mobile from = sender.Mobile;
  switch (info.ButtonID)
  {
   case (int)Buttons.oui:
    m_lance.Jouteur.SendMessage("{0} a refus� de jouter avec vous.",from.Name);
   break;
   case (int)Buttons.non:
    m_lance.Jouteur.SendMessage("{0} a accept� de jouter avec vous!",from.Name);
    from.SendMessage("{0} va � pr�sent d�signer le t�moin de la joute.",m_lance.Jouteur.Name);
    from.SendMessage("Pr�parez-vous!");
    m_lance.ValiderTemoin();
   break;
   default:
   break;
  }
 }
}
////////////////// FIN GUMP /////////////////

///// //// /// // / DEBUT GUMP / // /// //// /////
public class JouteStartGump : Gump
{
 LanceJoute m_lance;

 public JouteStartGump( LanceJoute lance)
  : base( 0, 0 )
 {
  m_lance=lance;
  this.Closable=false;
  this.Disposable=false;
  this.Dragable=true;
  this.Resizable=false;
  this.AddPage(0);
  this.AddBackground(165, 168, 273, 199, 9200);
  this.AddPage(1);
  this.AddLabel(232, 177, 0, @"Vous avez �t� d�sign� comme");
  this.AddLabel(232, 201, 0, @"t�moin d'un joute. Vous pouvez");
  this.AddLabel(232, 232, 0, @"");
  this.AddLabel(232, 225, 0, @"refuser ou donner le d�part de");
  this.AddLabel(232, 249, 0, @"la joute une fois les deux");
  this.AddLabel(232, 273, 0, @"protagonistes en place.");
  this.AddImage(142, 136, 50936);
  this.AddImage(157, 219, 109);
  this.AddButton(344, 297, 5547, 5548, (int)Buttons.start, GumpButtonType.Reply, 0);
  this.AddButton(264, 317, 241, 242, (int)Buttons.cancel, GumpButtonType.Reply, 0);

 }
 
 public enum Buttons
 {
  cancel,
  start
 }

 public override void OnResponse( NetState sender, RelayInfo info )
 {
  Mobile from = sender.Mobile;
  switch (info.ButtonID)
  {
   case (int)Buttons.cancel:
    m_lance.Jouteur.SendMessage("{0} a refus� d'�tre le t�moin de cette joute.",from.Name);
    m_lance.Adversaire.SendMessage("{0} a refus� d'�tre le t�moin de cette joute.",from.Name);
   break;
   case (int)Buttons.start:
    m_lance.Jouter();
   break;
   default:
   break;
  }
 }
}
////////////////// FIN GUMP /////////////////



 ///// //// /// // / DEBUT TARGET / // /// //// /////
 private class JouteSelector : Target
 {
  private LanceJoute m_LanceJoute;
  private bool est_temoin;

  public JouteSelector( LanceJoute lance, bool temoin ) : base( -1, false, TargetFlags.None )
  {
   m_LanceJoute = lance;
   est_temoin = temoin;
  }

  protected override void OnTarget( Mobile from, object targeted )
  {
   if ( targeted is PlayerMobile )
   {
    if ( !est_temoin )
    {
     if ( (Mobile)targeted == from )
      from.SendMessage("Vous ne pouvez jouter avec vous-m�me");
     else
     {
      if ( ((PlayerMobile)targeted).Mounted )
      {
       if ( ((PlayerMobile)targeted).FindItemOnLayer(Layer.FirstValid) is LanceJoute )
       {
        m_LanceJoute.ChoisirAdversaire((Mobile)targeted);
       }
       else
       {
        from.SendMessage("Cette personne n'a pas de lance de joute en main.");
       }
      }
      else
      {
       from.SendMessage("Cette personne n'est pas � cheval.");
      }
     }
    }
    else
    {
     if ( (Mobile)targeted == m_LanceJoute.Jouteur )
     {
      from.SendMessage("Vous ne pouvez �tre t�moin de votre joute.");
     }
     else
     {
      if ( (Mobile)targeted == m_LanceJoute.Adversaire )
       from.SendMessage("Votre adversaire ne peut �tre le t�moin.");
      else
       m_LanceJoute.ChoisirTemoin((Mobile)targeted);
     }
    }
   }
   else
   {
    from.SendMessage( "Vous devez s�lectionner un joueur." );
   }
  }
 }
 ///////// FIN SELECTOR ////////////

 public override void Serialize( GenericWriter writer )
 {
  base.Serialize( writer );
  writer.Write( (int) 0 );
 }
 
 public override void Deserialize(GenericReader reader)
 {
  base.Deserialize( reader );
  int version = reader.ReadInt();
 }
}
}