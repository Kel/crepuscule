using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
    public class BatonSage : BaseStaff
    {
        public override WeaponAbility PrimaryAbility { get { return WeaponAbility.WhirlwindAttack; } }
        public override WeaponAbility SecondaryAbility { get { return WeaponAbility.ParalyzingBlow; } }

        public override int AosStrengthReq { get { return 35; } }
        public override int AosMinDamage { get { return 1; } }
        public override int AosMaxDamage { get { return 5; } }
        public override int AosSpeed { get { return 39; } }

        public override int OldStrengthReq { get { return 35; } }
        public override int OldMinDamage { get { return 8; } }
        public override int OldMaxDamage { get { return 33; } }
        public override int OldSpeed { get { return 35; } }

        public override int InitMinHits { get { return 31; } }
        public override int InitMaxHits { get { return 70; } }

        [Constructable]
        public BatonSage() : base(0x366F)
        {
            Name = "B�ton de sage";
            Weight = 6.0;
            Layer = Layer.TwoHanded;
Attributes.SpellChanneling = 1;
        }

        public BatonSage(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}
