//Arc de chasse: Arc peu rapide, grande distance et bons d�gats.
using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
        public class ArcDeChasse : BaseRanged
    {
        public override int EffectID { get { return 0xF42; } }
        public override Type AmmoType { get { return typeof(Arrow); } }
        public override Item Ammo { get { return new Arrow(); } }

        public override WeaponAbility PrimaryAbility { get { return WeaponAbility.ShadowStrike; } }
        public override WeaponAbility SecondaryAbility { get { return WeaponAbility.MortalStrike; } }

        public override int AosStrengthReq { get { return 100; } }
        public override int AosDexterityReq { get { return 90; } }
        public override int AosMinDamage { get { return 12; } }
        public override int AosMaxDamage { get { return 18; } }
        public override int AosSpeed { get { return 30; } }

        public override int OldStrengthReq { get { return 20; } }
        public override int OldMinDamage { get { return 9; } }
        public override int OldMaxDamage { get { return 41; } }
        public override int OldSpeed { get { return 20; } }

        public override int DefMaxRange { get { return 12; } }

        public override int InitMinHits { get { return 31; } }
        public override int InitMaxHits { get { return 60; } }

        public override WeaponAnimation DefAnimation { get { return WeaponAnimation.ShootBow; } }

        [Constructable]
        public ArcDeChasse()
            : base(0x3671)
        {
            Name = "Arc De Chasse";
            Weight = 5.0;
            Layer = Layer.TwoHanded;
        }

        public ArcDeChasse(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();

        }
    }
}