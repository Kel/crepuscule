//ArcSombre: Arc pr�cis et rapide, peu de d�gats et distance mod�r�e.
using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
    public class ArcSombre : BaseRanged
    {
        public override int EffectID { get { return 0xF42; } }
        public override Type AmmoType { get { return typeof(Arrow); } }
        public override Item Ammo { get { return new Arrow(); } }

        public override WeaponAbility PrimaryAbility { get { return WeaponAbility.ParalyzingBlow; } }
        public override WeaponAbility SecondaryAbility { get { return WeaponAbility.InfectiousStrike; } }

        public override int AosStrengthReq { get { return 55; } }
        public override int AosDexterityReq { get { return 100; } }
        public override int AosMinDamage { get { return 12; } }
        public override int AosMaxDamage { get { return 13; } }
        public override int AosSpeed { get { return 35; } }

        public override int OldStrengthReq { get { return 20; } }
        public override int OldMinDamage { get { return 9; } }
        public override int OldMaxDamage { get { return 41; } }
        public override int OldSpeed { get { return 20; } }

        public override int DefMaxRange { get { return 11; } }

        public override int InitMinHits { get { return 31; } }
        public override int InitMaxHits { get { return 60; } }

        public override WeaponAnimation DefAnimation { get { return WeaponAnimation.ShootBow; } }

        [Constructable]
        public ArcSombre(): base(0x3672)
        {
            Name = "ArcSombre";
            Weight = 5.0;
            Layer = Layer.TwoHanded;
        }

        public ArcSombre(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}