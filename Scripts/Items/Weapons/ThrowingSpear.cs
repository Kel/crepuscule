///////////////////////////////////
//Cr�� par Willy pour cr�puscule//
///////////////////////////////////
using System;
using Server;
using Server.Items;
using Server.Network;
using Server.Targeting;
using System.Collections;

namespace Server.Items
{
	[FlipableAttribute( 0xF62, 0xF63 )]
    public class ThrowingSpear : Item
    {
        [Constructable]
        public ThrowingSpear()
            : base(0xF62)
        {
            Name = "Lance L�g�re";
            Stackable = false;
            Weight = 2.0;
        }

        public ThrowingSpear(Serial serial)
            : base(serial)
        {
        }
        
        public override void OnDoubleClick( Mobile m )
        {
	        if ( !IsChildOf( m.Backpack ) )
				m.SendLocalizedMessage( 1042010 ); // You must have the object in your backpack to use it.
			else if ( !Core.AOS && (m.FindItemOnLayer( Layer.OneHanded ) != null || m.FindItemOnLayer( Layer.TwoHanded ) != null) )
				m.SendLocalizedMessage( 1040015 ); // Your hands must be free to use this
			else if ( m.CanBeginAction( typeof( ThrowingSpear ) ) )
			{
				m.SendMessage( "Quelle Cr�ature voulez-vous attaquer?" );
	    		m.Target = new SpearTarget( m, this );
    		}
			else
				m.SendMessage( "Vous devez attendre 2 secondes pour relancer une Lance" );
    	}
        
        private class InternalTimer : Timer
		{
			private Mobile m_From;

			public InternalTimer( Mobile from ) : base( TimeSpan.FromSeconds( 2.0 ) )
			{
				m_From = from;
			}

			protected override void OnTick()
			{
				m_From.EndAction( typeof( ThrowingSpear ) );
			}
		}
        
        private class SpearTarget : Target 
        {
	        private Mobile m_Thrower;
			private Item m_Spear;

			public SpearTarget( Mobile thrower, Item spear ) : base ( 10, false, TargetFlags.None )
			{
				m_Thrower = thrower;
				m_Spear = spear;
			}
	        
	     	protected override void OnTarget( Mobile from, object target )
	     	{		     		     	
		     	if( target == from )
		     	   	from.SendLocalizedMessage( 1005576 );
				else if( target is Mobile )
				{	
					Mobile targ = (Mobile) target;
							
					
					 if( targ.Blessed == true )
						from.SendMessage( "Cr�ature ne peut pas �tre touch�e" );
					else if( targ.Alive != true )
						from.SendMessage( "D�ja mort" );
					else
					{
						if ( from.BeginAction( typeof( ThrowingSpear ) ) )
						{
							new InternalTimer( from ).Start();
										
							if( m_Thrower.Skills[SkillName.Tactics].BaseFixedPoint < 1 )
							{
								if( m_Thrower.Skills[SkillName.Stealth].BaseFixedPoint >= 1000 && m_Thrower.Skills[SkillName.Hiding].BaseFixedPoint >= 1000 )
								{
									from.SendMessage( "Vous lancez, ratez, mais restez cach�." );
								}
								else
								{
									from.SendMessage( "Vous lancez et ratez la cible." );
									from.RevealingAction();	
								}	
								from.Animate( 31, 5, 1, true, false, 0 );
								targ.SendMessage( "Vous esquivez agilement." );
								
								m_Spear.Consume();
							}
							else
							{													
								switch ( Utility.Random( 2000 / m_Thrower.Skills[SkillName.Tactics].BaseFixedPoint ) )
								{
									case 0:
									{	
										if( m_Thrower.Skills[SkillName.Stealth].BaseFixedPoint >= 1000 && m_Thrower.Skills[SkillName.Hiding].BaseFixedPoint >= 1000 && m_Thrower.Hidden == true )
										{
											from.SendMessage( "Vous lancez, touchez et restez cach�!" );
										}
										else
										{
											from.SendMessage( "Vous lancez....et touchez!" );
											from.RevealingAction();	
										}						
										from.PlaySound( 0x23C );
					
										from.Animate( 31, 5, 1, true, false, 0 );
					
										targ.SendMessage( "Vous �te atteint par une lance" );
										
										from.DoHarmful( targ );
										
										int dbonus = Utility.RandomMinMax( 0,10 );
										
										int damage = m_Thrower.Skills[SkillName.Tactics].BaseFixedPoint / 90 + dbonus;
										
										if( targ.Hits - damage < 0 )
											targ.Kill();
										else
											targ.Hits = targ.Hits - damage;
																										
										Effects.SendMovingEffect( from, targ, 0xF62 , 7, 0, false, false, 0x0, 0 );
												
										m_Spear.Consume();
										
										break;
									}
									case 1:
									{
										if( m_Thrower.Skills[SkillName.Stealth].BaseFixedPoint >= 1000 && m_Thrower.Skills[SkillName.Hiding].BaseFixedPoint >= 1000 && m_Thrower.Hidden == true )
											from.SendMessage( "Vous lancez, ratez et restez cach�." );
										else
										{
											from.SendMessage( "Vous lancez et manquez mis�rablement" );
											from.RevealingAction();	
										}	
	
										from.Animate( 31, 5, 1, true, false, 0 );
										targ.SendMessage( "Vous esquivez la lance" );
										
										Effects.SendMovingEffect( from, targ, 0xF62 , 7, 0, false, false, 0x0, 0 );
										
										m_Spear.Consume();
										
										break;
									}
								}
							}					
						}
						else
							from.SendMessage( "Vous devez attendre 2 secondes avant de relancer une lance." );
					}
				}
				else
					from.SendMessage( "Vous ne pouvez pas lancer la lance sur ceci!" );
	     	}   
        }       

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}