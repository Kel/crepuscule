///////////////////////////////////
//Cr�� par Willy pour cr�puscule//
///////////////////////////////////
using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x2350, 0x2350 )]
	public class Boomerang : BaseBashing
	{
		

		public override int AosStrengthReq{ get{ return 45; } }
		public override int AosMinDamage{ get{ return 6; } }
		public override int AosMaxDamage{ get{ return 8; } }
		public override int AosSpeed{ get{ return 30; } }

		public override int OldStrengthReq{ get{ return 45; } }
		public override int OldMinDamage{ get{ return 6; } }
		public override int OldMaxDamage{ get{ return 8; } }
		public override int OldSpeed{ get{ return 30; } }


		public override int DefHitSound{ get{ return 0x530; } }
		public override int DefMissSound{ get{ return 0x531; } }

		public override int DefMaxRange{ get{ return 8; } }

		public override int InitMinHits{ get{ return 3; } }
		public override int InitMaxHits{ get{ return 5; } }

		public override SkillName DefSkill{ get{ return SkillName.Archery; } }

		[Constructable]
		public Boomerang() : base( 0x2350 )
		{
			Weight = 4.0;
			Layer = Layer.TwoHanded;
			Name = "Un Boomerang";
		}

		public override void OnMiss( Mobile attacker, Mobile defender )
		{
			double archery = attacker.Skills[SkillName.Archery].Value;

			double dropChance = (archery + 20) * 0.008;
			
			if ( attacker.Player && dropChance <= Utility.RandomDouble() )
				this.MoveToWorld( new Point3D( defender.X + Utility.RandomMinMax( -1, 1 ), defender.Y + Utility.RandomMinMax( -1, 1 ), defender.Z ), defender.Map );

			base.OnMiss( attacker, defender );
			
		}

		public Boomerang( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.WriteEncodedInt( 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadEncodedInt();
		}
	}
}