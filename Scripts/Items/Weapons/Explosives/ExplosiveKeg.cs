using System;
using Server;

namespace Server.Items
{
	public class ExplosiveKeg : BaseExplosiveKeg
	{
		public override int MinDamage { get { return 400; } }
		public override int MaxDamage { get { return 550; } }



		[Constructable]
		public ExplosiveKeg() : base( PotionEffect.ExplosiveKeg )
		{
			Name = "Baril explosif";
			Hue = 1945; 
			Weight = 100.0; 
		}

		public ExplosiveKeg( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}