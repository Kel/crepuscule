using System;


namespace Server.Items
{
	public class AssaultBolt : Bolt, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( Amount == 1 ? "{0} Assault bolt" : "{0} assault bolts", Amount );
			}
		}

		[Constructable]
		public AssaultBolt() : this( 1 )
		{
		}

		[Constructable]
		public AssaultBolt( int amount ) : base( 0x1BFB )
		{
			Name = "Carreaux D'assault";
			Hue = 1953;
			Stackable = true;
			Weight = 1;
			Amount = amount;
		}

		public AssaultBolt( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
