using System;
using System.Text;
using Server.Network;
using System.Collections;
using Server.Engines.Craft;
using Server.Engines;
using System.Collections.Generic;
using Server.Items;
using Server.Mobiles;
using System.Linq;
using Server.Engines.Helpers;

namespace Server
{
    [Description("Objet - Cr�puscule")]
    public class CrepusculeItem: Item
    {

        protected List<string> m_Identification = new List<string>();
        protected string m_PlayerDescription = "";
        protected string m_HiddenDescription = "";
        protected int m_EruditionNeeded = 0;
        protected Type m_CraftResource;
        private string m_BroadCastMessage = "";
        private bool m_Enchanted = false;
        private int m_EnchantmentID = 0;
        private bool m_DecaysEnabled = true;
        private bool m_FirstLift = true;
        private string m_CustomLiftEmote;

        #region Propri�t�s
        public List<string> Identification
        {
            set 
            { 
                m_Identification = value;
                this.InvalidateProperties();
            }
        }
        
        [CommandProperty(AccessLevel.Counselor)]
        public int EruditionNeeded
        {
            get { return m_EruditionNeeded; }
            set { 
                m_EruditionNeeded = value;
                this.InvalidateProperties();
            }
        }

        public bool DecaysEnabled
        {
            get { return m_DecaysEnabled; }
            set { m_DecaysEnabled = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string BroadCastMessage
        {
            get { return m_BroadCastMessage; }
            set { m_BroadCastMessage = value; }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public string PlayerDescription
        {
            get { return m_PlayerDescription; }
            set 
            { 
                m_PlayerDescription = value;
                this.InvalidateProperties();
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public string HiddenDescription
        {
            get { return m_HiddenDescription; }
            set {
                m_HiddenDescription = value;
                this.InvalidateProperties();
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Enchanted
        {
            get{return m_Enchanted;}
            set{ m_Enchanted = value;}
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int EnchantmentID
        {
            get { return m_EnchantmentID; }
            set { m_EnchantmentID = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool FirstLift
        {
            get { return m_FirstLift; }
            set { m_FirstLift = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string CustomLiftEmote
        {
            get { return m_CustomLiftEmote; }
            set { m_CustomLiftEmote = value; }
        }

        /// <summary>
        /// Utilis� dans le syst�me de craft (de base et dynamique) 
        /// </summary>
        public virtual int ScienceProjectRequired
        {
            get { return 0; } //0 veut dire qu'il n'y a pas de projet requis
        }
        #endregion

        #region Constructeurs
        public CrepusculeItem() : base()
        {}

        public CrepusculeItem(Serial serial)
            : base(serial)
        {}


        public CrepusculeItem(int itemID)
            : base(itemID)
        { }

        #endregion

        public override void AddItem(Item item)
        {
            base.AddItem(item);
        }

        #region Surcharges

        public override void AddNameProperties(ObjectPropertyList list)
        {
            base.AddNameProperties(list);

            if (!String.IsNullOrEmpty(Name))
            {
                if (Amount <= 1)
                    list.Add(Name);
                else
                    list.Add(0x1005b7, "{0}\t{1}", Amount, Name);
            }
        }

        public override void GetProperties(ObjectPropertyList list)
        {
            base.GetProperties(list);

            // Other, custom props
            //
            // Empty Spaces:
            //1049644 ~1_stuff~
            //1060658-1060663 ~1_val~:~2_val~
            //1060847 ~1_val~ ~2_val~
            //1070722 ~1_nothing~

            if (CraftResourceType != null && !CraftResourceType.Craft.IsStandard)
                list.Add(1060659, "Mat�riel\t{0}", CraftResourceType.Craft.CraftAttributes.LabelName);

            string description = string.Empty;

            if (m_Enchanted)
            {
                description += (String.IsNullOrEmpty(description) ? string.Empty : "\n") + "Enchant�"; 
            }

            if (!String.IsNullOrEmpty(PlayerDescription))
            {
                description += (String.IsNullOrEmpty(description) ? string.Empty : "\n") + PlayerDescription;
            }

            if (m_Identification.Count > 0)
            {
                description += (String.IsNullOrEmpty(description) ? string.Empty : "\n") + m_Identification.Aggregate((a, b) => a + "\n" + b);
            }

            if (!String.IsNullOrEmpty(description))
            {
                list.Add(1050045, " \t{0}\t ", description);
            }

            // if (UseIdentity == 0) opl.Add(1050045, "{0} \t{1}\t {2}", "", this.PlayerName, "");
            /*if (!String.IsNullOrEmpty(PlayerDescription))
                list.Add(1050045, " \t{0}\t ", PlayerDescription);
            else if(m_Identification.Count > 0)
                list.Add(1050045, " \t{0}\t ", (m_Identification.Aggregate((a, b) => a + "\n" + b)));*/

            //if (m_Identification.Count > 0)
            //    list.Add(1049644, (m_Identification.Aggregate((a, b) => a + "\n" + b)));

            // Player Description
           /* if (!String.IsNullOrEmpty(PlayerDescription))
                Properties += "\n" + Name;

            // Identification
            if (m_Identification.Count > 0)
                Properties += "\n" + (m_Identification.Aggregate((a, b) => a + "\n" + b));

            // Set Main Properties
            list.Add(1053099, "{0}\t{1}", Properties, ""); // ~1_oretype~ ~2_armortype~*/

            //Enchantements
            /*if (m_Enchanted)
            {
                if (m_EnchantmentID == 0)
                    list.Add(1050045, " \tEnchantement de force\t ");

                else if (m_EnchantmentID == 1)
                    list.Add(1050045, " \tEnchantement de dexterit�\t ");

                else if (m_EnchantmentID == 2)
                    list.Add(1050045, " \tEnchantement de l'intelligence\t ");

                else if (m_EnchantmentID == 3)
                    list.Add(1050045, " \tEnchantement de constitution\t ");

                else if (m_EnchantmentID == 4)
                    list.Add(1050045, " \tEnchantement de l'endurance\t ");

                else if (m_EnchantmentID == 5)
                    list.Add(1050045, " \tEnchantement de flux magique\t ");

                else if (m_EnchantmentID == 6)
                    list.Add(1050045, " \tEnchantement de r�sistance\t ");

                else if (m_EnchantmentID == 7)
                    list.Add(1050045, " \tEnchantement de prot�ction de feu\t ");

                else if (m_EnchantmentID == 8)
                    list.Add(1050045, " \tEnchantement de prot�ction de froid\t ");

                else if (m_EnchantmentID == 9)
                    list.Add(1050045, " \tEnchantement de prot�ction de poison\t ");

                else if (m_EnchantmentID == 10)
                    list.Add(1050045, " \tEnchantement de prot�ction de l'�nergie\t ");

                else if (m_EnchantmentID == 11)
                    list.Add(1050045, " \tEnchantement de m�tabolisme\t ");

                else if (m_EnchantmentID == 12)
                    list.Add(1050045, " \tEnchantement de r�g�n�ration magique\t ");

                else if (m_EnchantmentID == 13)
                    list.Add(1050045, " \tEnchantement de respiration\t ");

                else if (m_EnchantmentID == 14)
                    list.Add(1050045, " \tEnchantement de concentration\t ");

                else if (m_EnchantmentID == 15)
                    list.Add(1050045, " \tEnchantement de rapidit� magique\t ");

                else if (m_EnchantmentID == 16)
                    list.Add(1050045, " \tEnchantement de co�t magique\t ");

                else if (m_EnchantmentID == 17)
                    list.Add(1050045, " \tEnchantement de prix magique\t ");

                else if (m_EnchantmentID == 18)
                    list.Add(1050045, " \tEnchantement de l'armure magique\t ");

                else if (m_EnchantmentID == 19)
                    list.Add(1050045, " \tEnchantement de l'arme magique\t ");

                else if (m_EnchantmentID == 20)
                    list.Add(1050045, " \tEnchantement de flux arcaniques\t ");

                else if (m_EnchantmentID == 21)
                    list.Add(1050045, " \tEnchantement des d�gats arcaniques\t ");

                else if (m_EnchantmentID == 22)
                    list.Add(1050045, " \tEnchantement de zone de froid\t ");

                else if (m_EnchantmentID == 23)
                    list.Add(1050045, " \tEnchantement de zone de l'energie\t ");

                else if (m_EnchantmentID == 24)
                    list.Add(1050045, " \tEnchantement de zone de feu\t ");

                else if (m_EnchantmentID == 25)
                    list.Add(1050045, " \tEnchantement de zone\t ");

                else if (m_EnchantmentID == 26)
                    list.Add(1050045, " \tEnchantement de zone de poison\t ");

                else if (m_EnchantmentID == 27)
                    list.Add(1050045, " \tEnchantement de dissipation\t ");

                else if (m_EnchantmentID == 28)
                    list.Add(1050045, " \tEnchantement de boule de feu\t ");

                else if (m_EnchantmentID == 29)
                    list.Add(1050045, " \tEnchantement de blessure\t ");

                else if (m_EnchantmentID == 30)
                    list.Add(1050045, " \tEnchantement de l'�clair\t ");

                else if (m_EnchantmentID == 31)
                    list.Add(1050045, " \tEnchantement de fl�che magique\t ");

                else if (m_EnchantmentID == 32)
                    list.Add(1050045, " \tEnchantement d'affaiblissement d'attaque\t ");

                else if (m_EnchantmentID == 33)
                    list.Add(1050045, " \tEnchantement d'affaiblissement de defense\t ");

                else if (m_EnchantmentID == 34)
                    list.Add(1050045, " \tEnchantement de vampirisation\t ");

                else if (m_EnchantmentID == 35)
                    list.Add(1050045, " \tEnchantement de drain de mana\t ");

                else if (m_EnchantmentID == 36)
                    list.Add(1050045, " \tEnchantement de drain de fatigue\t ");

                else if (m_EnchantmentID == 37)
                    list.Add(1050045, " \tEnchantement d'utilisation d'arme\t ");

                else if (m_EnchantmentID == 38)
                    list.Add(1050045, " \tEnchantement de d�gats d'arme\t ");

                else if (m_EnchantmentID == 39)
                    list.Add(1050045, " \tEnchantement de rapidit� d'arme\t ");

                else if (m_EnchantmentID == 40)
                    list.Add(1050045, " \tEnchantement de chance offensive\t ");

                else if (m_EnchantmentID == 41)
                    list.Add(1050045, " \tEnchantement de chance deffensive\t ");

                else if (m_EnchantmentID == 42)
                    list.Add(1050045, " \tEnchantement des potions\t ");

                else if (m_EnchantmentID == 43)
                    list.Add(1050045, " \tEnchantement des pr�requis\t ");

                else if (m_EnchantmentID == 44)
                    list.Add(1050045, " \tEnchantement de chance\t ");

                else if (m_EnchantmentID == 45)
                    list.Add(1050045, " \tEnchantement de reflexion physique\t ");

                else if (m_EnchantmentID == 46)
                    list.Add(1050045, " \tEnchantement d'auto-r�paration\t ");

            }*/
        }

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);
            if (!String.IsNullOrEmpty(m_BroadCastMessage))
            {
                BroadcastMessage(AccessLevel.Player, 34, m_BroadCastMessage);
            }
        }

        public override void OnItemLifted(Mobile from, Item item)
        {
            base.OnItemLifted(from, item);

            if (m_FirstLift)
            {
                m_FirstLift = false;

                if (this.Parent is Corpse && from is PlayerMobile)
                {

                    // Lets role the dices !
                    if (RogueHelper.NinjaLoot(from, this))
                    {
                        if (!String.IsNullOrEmpty(m_CustomLiftEmote))
                        {
                            from.Emote("*" + m_CustomLiftEmote + "*");
                        }
                        else
                        {
                            // Build message
                            string target = string.Empty;

                            if (this is BaseReagent)
                            {
                                target = this.Amount > 1 ? "quelques r�actifs" : "un r�actif";
                            }
                            else if (this is BaseArmor)
                            {
                                target = "une pi�ce d'armure";
                            }
                            else if (this is BaseWeapon)
                            {
                                target = "une arme";
                            }
                            else if (this is BaseJewel)
                            {
                                target = "un bijoux";
                            }
                            else if (this is BaseBook)
                            {
                                target = "un livre";
                            }
                            else if (this is SpellScroll)
                            {
                                target = this.Amount > 1 ? "des parchemins" : "un parchemin";
                            }
                            else if (this is BaseOre)
                            {
                                target = Amount > 1 ? "des minerais" : "un minerai";
                            }
                            else if (this.Name != null)
                            {
                                target = this.Amount > 1 ? string.Format("des objets ({0} {1})", this.Amount.ToString(), this.Name) : string.Format("un object ({0})", this.Name);
                            }
                            else
                            {
                                target = "un objet";
                            }

                            if (!String.IsNullOrEmpty(target))
                            {
                                from.Emote("*Ramasse " + target + " sur un corps*");
                            }
                        }
                    }
                }
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public sealed override bool Decays
        {
            get { return (Movable && Visible && m_DecaysEnabled); }
        }

        #endregion

        #region Template & Craft System
        public sealed override void OnAssignProperties()
        {
            var type = this.GetType();
            if(ManagerConfig.Items.ContainsKey(type))
            {
                var template = ManagerConfig.Items[type];
                template.SetToItem(this);
            }
            ShouldAssignProperties = false;
        }
       
        [CommandProperty(AccessLevel.GameMaster), Nullable]
        public virtual Type CraftResource
        {
            get { return m_CraftResource; }
            set
            {
                if (value != null && value.IsSubclassOf(typeof(Item)))
                {
                    var type = GameItemType.FindType(this);
                    if (type != null)
                    {
                        m_CraftResource = value;
                        if (type.HueFromRes || !type.Craft.IsStandard)
                        {
                            Hue = type.Hue;
                            //Hue = GameCraftSystem.GetHueFor(this, value);
                        }
                    }
                }
            }
        }

        public GameItemType GetItemType()
        {
            return GameItemType.FindType(this.GetType());
        }

        public GameItemType CraftResourceType
        {
            get
            {
                if (m_CraftResource == null) return null;
                return GameItemType.FindType(m_CraftResource);
            }
        }

        public CraftAttributeInfo GetResourceAttributes()
        {
            if (CraftResourceType == null)
                return null;

            var attrs = CraftResourceType.Craft.CraftAttributes;

            if (attrs == null || attrs.IsBlank)
                return null;

            return attrs;
        }

        public virtual Item Dupe(Item item, int amount)
        {
            item.Visible = Visible;
            item.Movable = Movable;
            item.LootType = LootType;
            item.Direction = Direction;
            item.Hue = Hue;
            item.ItemID = ItemID;
            item.Location = Location;
            item.Layer = Layer;
            item.Name = Name;
            item.Weight = Weight;
            item.Amount = amount;
            item.Map = Map;

            if (Parent is Mobile)
            {
                ((Mobile)Parent).AddItem(item);
            }
            else if (Parent is Item)
            {
                ((Item)Parent).AddItem(item);
            }

            item.Delta(ItemDelta.Update);

            return item;
        }

        public virtual Item Dupe(int amount)
        {
            var item = GameItemType.CreateItem(GetItemType());
            item.Visible = Visible;
            item.Movable = Movable;
            item.LootType = LootType;
            item.Direction = Direction;
            item.Hue = Hue;
            item.ItemID = ItemID;
            item.Location = Location;
            item.Layer = Layer;
            item.Name = Name;
            item.Weight = Weight;
            item.Amount = amount;
            item.Map = Map;

            if (Parent is Mobile)
            {
                ((Mobile)Parent).AddItem(item);
            }
            else if (Parent is Item)
            {
                ((Item)Parent).AddItem(item);
            }

            item.Delta(ItemDelta.Update);
            return item;
        }

        #endregion

        #region Methodes virtuelles
        public virtual void OnAfterCraft(GameItemType resource)
        {
            
        }
        #endregion

        #region Serialisation

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)7); // version
            writer.Write((string)m_PlayerDescription);
            writer.Write((string)m_HiddenDescription);
            writer.Write((int)m_EruditionNeeded);
            writer.Write((string)m_BroadCastMessage);
            writer.Write((bool)m_Enchanted);
            writer.Write(m_EnchantmentID);
            writer.Write(m_DecaysEnabled);
            writer.Write(m_Identification.Count);
            for (int i=0;i<m_Identification.Count;++i)
              writer.Write((string)m_Identification[i]);
            writer.Write(m_CraftResource);
            writer.Write(m_FirstLift);
            writer.Write(m_CustomLiftEmote);
        }

        #endregion

        #region Deserialisation

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();


            if (version >= 0 )
            {
                m_PlayerDescription = (string)reader.ReadString();
                m_HiddenDescription = (string)reader.ReadString();
                m_EruditionNeeded = (int)reader.ReadInt();
            }
            if (version >= 1)
            {
                m_BroadCastMessage = (string)reader.ReadString();
            }
            if (version >= 2)
            {
                m_Enchanted = reader.ReadBool();
            }
            if (version >= 3)
            {
                m_EnchantmentID = reader.ReadInt();
            }
            if (version >= 4)
            {
                m_DecaysEnabled = reader.ReadBool();
            }
            if (version >= 5)
            {
                int count = reader.ReadInt();
                for (int i=0; i < count;++i)
                  m_Identification.Add(reader.ReadString());
            }
            if (version >= 6)
            {
                m_CraftResource = reader.ReadType();
            }
            if (version >= 7)
            {
                m_FirstLift = reader.ReadBool();
                m_CustomLiftEmote = reader.ReadString();
            }
        }
        #endregion

        #region Methodes Statiques
        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;

                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }
        #endregion

        #region Reprocessing (Scissor)

        public virtual bool Scissor(Mobile from, Scissors scissors)
        {
            return Scissor(from, scissors, false);
        }


        public bool Scissor(Mobile from, Scissors scissors, bool playerConstructed)
        {
            if (!(this is IScissorable))
                return false;

            if ( Deleted || !from.CanSee( this ) ) return false;
            if (!IsChildOf(from.Backpack))
            {
                from.SendLocalizedMessage(502437); // Items you wish to cut must be in your backpack.
                return false;
            }

            var typeofItem = GetType();
            var itemType = GameItemType.FindType(typeofItem);
            if (itemType == null)
                return false;

            if (itemType.Craft.ScissorTo != null) // Scissor to specified
            {
                try
                {
                    Item res = GameItemType.CreateItem(itemType.Craft.ScissorTo);
                    if (res != null)
                    {
                        var amount = itemType.Craft.ScissorToAmount;
                        ScissorHelper(from, res, amount < 1 ? Amount : amount);
                    }
                }
                catch { }

                from.SendLocalizedMessage(502440); // Scissors can not be used on that to produce anything.
                return false;
            }
            else // Scissor to craft resource
            {
                var resourceType = CraftResourceType;
                if (resourceType == null)
                    return false;

                // The item type has resource attached
                var craftResource = itemType.Craft.Ressource1;

                // Check the amount
                if (craftResource == null ||
                    craftResource.Amount < 2 ||
                    !itemType.Craft.Craftable)
                {
                    from.SendLocalizedMessage(502440); // Scissors can not be used on that to produce anything.
                    return false;
                }

                try
                {

                    Item res = GameItemType.CreateItem(craftResource.ItemType);
                    if (res != null)
                    {
                        ScissorHelper(from, res, playerConstructed ? (craftResource.Amount / 2) : 1 );

                        // Scissoring adds xp!
                        var player = from as RacePlayerMobile;
                        if (player != null)
                            player.AddCraftXP(typeofItem, 1);
                        return true;
                    }
                }
                catch { }

                from.SendLocalizedMessage(502440); // Scissors can not be used on that to produce anything.
                return false;
            }
        }
        #endregion
    }
}
