using System;
using System.Text;
using System.Collections;
using Server.Network;
using Server.Targeting;
using Server.Spells;

namespace Server.Items
{
	public enum WandEffect
	{
		Clumsiness,			
		Identification,		
		Healing,			
		Feeblemindedness,	
		Weakness,			
		MagicArrow,			
		Harming,			
		Fireball,			
		GreaterHealing,		
		Lightning,			
		Nourriture,			
		Nightsight,			
		ReactiveArmor,		
		Agilite,			
		Clairvoyance,		
		Antidote,			
		MagicTrap,			
		MagicUntrap,		
		Protection,			
		Force,				
		Benediction,		
		MagicLock,			
		Poison,				
		Telekinesis,		
		Teleportation,		
		Sesame,				
		Muraille,			
		SuperAntidote,		
		SuperProtection,	
		Malediction,		
		FireWall,		
		Rappel,			
		Tornade,		
		PlaineDuN�ant,		
		Incognito,			
		Reflexion,		
		MindBlast,		
		Paralyse,		
		PoissonWall,		
		SummonAnimal,		
		Dispel,				
		EnergyBolt,			
		Explosion,			
		Invisibilite,		
		Mark,				
		Mal�dictionG�n�rale, 
		ParalyseField,		
		Revelateur,			
		ChainLightning,		
		EnergyFiel,		
		GateTraval,			
		ManaVampire,		
		MassDispel,		
		Meteor,		
		Polymorph,		
		Earthquake,			
		EnergyVortex,		
		Ressurect,		
		SummonAir,		
		SummonDeamon,		
		SummonEarth,		
		SummonWater,
		ManaDraining,
		SummonFire	
			
	}

	public abstract class BaseWand : BaseBashing
	{
		private WandEffect m_WandEffect;
		private int m_Charges;

		public virtual TimeSpan GetUseDelay{ get{ return TimeSpan.FromSeconds( 4.0 ); } }

		[CommandProperty( AccessLevel.GameMaster )]
		public WandEffect Effect
		{
			get{ return m_WandEffect; }
			set{ m_WandEffect = value; InvalidateProperties(); }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int Charges
		{
			get{ return m_Charges; }
			set{ m_Charges = value; InvalidateProperties(); }
		}

		public BaseWand( WandEffect effect, int minCharges, int maxCharges ) : base( Utility.RandomList( 0xDF2, 0xDF3, 0xDF4, 0xDF5 ) )
		{
			Weight = 1.0;
			Effect = effect;
			Charges = Utility.RandomMinMax( minCharges, maxCharges );
		}

		public void ConsumeCharge( Mobile from )
		{
			--Charges;

			if ( Charges == 0 )
				from.SendLocalizedMessage( 1019073 ); // This item is out of charges.

			ApplyDelayTo( from );
		}

		public BaseWand( Serial serial ) : base( serial )
		{
		}

		public virtual void ApplyDelayTo( Mobile from )
		{
			from.BeginAction( typeof( BaseWand ) );
			Timer.DelayCall( GetUseDelay, new TimerStateCallback( ReleaseWandLock_Callback ), from );
		}

		public virtual void ReleaseWandLock_Callback( object state )
		{
			((Mobile)state).EndAction( typeof( BaseWand ) );
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !from.CanBeginAction( typeof( BaseWand ) ) )
				return;

			if ( Parent == from )
			{
				if ( Charges > 0 )
					OnWandUse( from );
				else
					from.SendLocalizedMessage( 1019073 ); // This item is out of charges.
			}
			else
			{
				from.SendLocalizedMessage( 502641 ); // You must equip this item to use it.
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version

			writer.Write( (int) m_WandEffect );
			writer.Write( (int) m_Charges );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
				case 0:
				{
					m_WandEffect = (WandEffect)reader.ReadInt();
					m_Charges = (int)reader.ReadInt();

					break;
				}
			}
		}

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

			switch ( m_WandEffect )
			{
				case WandEffect.Clumsiness:			list.Add( 1017326, m_Charges.ToString() ); break; // clumsiness charges: ~1_val~
				case WandEffect.Identification:		list.Add( 1017350, m_Charges.ToString() ); break; // identification charges: ~1_val~
				case WandEffect.Healing:			list.Add( 1017329, m_Charges.ToString() ); break; // healing charges: ~1_val~
				case WandEffect.Feeblemindedness:	list.Add( 1017327, m_Charges.ToString() ); break; // feeblemind charges: ~1_val~
				case WandEffect.Weakness:			list.Add( 1017328, m_Charges.ToString() ); break; // weakness charges: ~1_val~
				case WandEffect.MagicArrow:			list.Add( 1060492, m_Charges.ToString() ); break; // magic arrow charges: ~1_val~
				case WandEffect.Harming:			list.Add( 1017334, m_Charges.ToString() ); break; // harm charges: ~1_val~
				case WandEffect.Fireball:			list.Add( 1060487, m_Charges.ToString() ); break; // fireball charges: ~1_val~
				case WandEffect.GreaterHealing:		list.Add( 1017330, m_Charges.ToString() ); break; // greater healing charges: ~1_val~
				case WandEffect.Lightning:			list.Add( 1060491, m_Charges.ToString() ); break; // lightning charges: ~1_val~
				case WandEffect.ManaDraining:		list.Add( 1017339, m_Charges.ToString() ); break; // mana drain charges: ~1_val~
				case WandEffect.Nourriture:			list.Add( "Charges de Cr�ation de nourriture", m_Charges.ToString() ); break;
				case WandEffect.Nightsight:			list.Add( 1017368, m_Charges.ToString() ); break;
				case WandEffect.ReactiveArmor:		list.Add( "Charges d'armure r�fl�chissante", m_Charges.ToString() ); break;
				case WandEffect.Agilite:			list.Add( 1017363, m_Charges.ToString() ); break;
				case WandEffect.Clairvoyance:		list.Add( "Charges de Clairvoyance", m_Charges.ToString() ); break;
				case WandEffect.Antidote:			list.Add( "Charges d'Antidote", m_Charges.ToString() ); break;
				case WandEffect.MagicTrap:			list.Add( "Charges de pi�ge magique", m_Charges.ToString() ); break;
				case WandEffect.MagicUntrap:		list.Add( "Charges de d�samor�age des pi�ge magique", m_Charges.ToString() ); break;
				case WandEffect.Protection:			list.Add( "Charges de Protection", m_Charges.ToString() ); break;
				case WandEffect.Force:				list.Add( "Charges de Force", m_Charges.ToString() ); break;
				case WandEffect.Benediction:		list.Add(	"Charges de B�nediction", m_Charges.ToString() ); break;
				case WandEffect.MagicLock:			list.Add( "Charges de Verouillage", m_Charges.ToString() ); break;
				case WandEffect.Poison:				list.Add( "Charges de Poison", m_Charges.ToString() ); break;
				case WandEffect.Telekinesis:		list.Add( "Charges de Telekinesis", m_Charges.ToString() ); break;
				case WandEffect.Teleportation:		list.Add( "Charges de T�l�portation", m_Charges.ToString() ); break;
				case WandEffect.Sesame:				list.Add( "Charges de S�same", m_Charges.ToString() ); break;
				case WandEffect.Muraille:			list.Add( "Charges de Muraille", m_Charges.ToString() ); break;
				case WandEffect.SuperAntidote:		list.Add( "Charges de SuperAntidote", m_Charges.ToString() ); break;
				case WandEffect.SuperProtection:	list.Add( "Charges de SuperProtection", m_Charges.ToString() ); break;
				case WandEffect.Malediction:		list.Add( "Charges de Mal�diction", m_Charges.ToString() ); break;
				case WandEffect.FireWall:			list.Add( "Charges de Mur de Feu", m_Charges.ToString() ); break;
				case WandEffect.Rappel:				list.Add( "Charges de Rappel", m_Charges.ToString() ); break;
				case WandEffect.Tornade:			list.Add( "Charges de Tornade", m_Charges.ToString() ); break;
				case WandEffect.PlaineDuN�ant:		list.Add( "Charges de Plaine du N�ant", m_Charges.ToString() ); break;
				case WandEffect.Incognito:			list.Add( "Charges d'Incognito", m_Charges.ToString() ); break;
				case WandEffect.Reflexion:			list.Add( "Charges de R�flexion", m_Charges.ToString() ); break;
				case WandEffect.MindBlast:			list.Add( "Charges de Choc Mental", m_Charges.ToString() ); break;
				case WandEffect.Paralyse:			list.Add( "Charges de Paralysie", m_Charges.ToString() ); break;
				case WandEffect.PoissonWall:		list.Add( "Charges de Mur de Poison", m_Charges.ToString() ); break;
				case WandEffect.SummonAnimal:		list.Add( "Charges d'Invocation d'animal", m_Charges.ToString() ); break;
				case WandEffect.Dispel:				list.Add( "Charges de dissipation", m_Charges.ToString() ); break;
				case WandEffect.EnergyBolt:			list.Add( "Charges de fl�ches d'�nergie", m_Charges.ToString() ); break;
				case WandEffect.Explosion:			list.Add( "Charges d'explosion", m_Charges.ToString() ); break;
				case WandEffect.Invisibilite:		list.Add( "Charges d'invisibilit�", m_Charges.ToString() ); break;
				case WandEffect.Mark:				list.Add( "Charges de Marquage", m_Charges.ToString() ); break;
				case WandEffect.Mal�dictionG�n�rale: list.Add( "Charges de Mal�diction G�n�rale", m_Charges.ToString() ); break;
				case WandEffect.ParalyseField:		list.Add( "Charges de Champ de Paralysie", m_Charges.ToString() ); break;
				case WandEffect.Revelateur:			list.Add( "Charges de R�v�lateur", m_Charges.ToString() ); break;
				case WandEffect.ChainLightning:		list.Add( "Charges d'�clair en chaine", m_Charges.ToString() ); break;
				case WandEffect.EnergyFiel	:		list.Add( "Charges de champ d'�nergie", m_Charges.ToString() ); break;
				case WandEffect.GateTraval:			list.Add( "Charges de porte c�leste", m_Charges.ToString() ); break;
				case WandEffect.ManaVampire:		list.Add( "Charges de vampirisme de l'�nergie magique", m_Charges.ToString() ); break;
				case WandEffect.MassDispel	:		list.Add( "Charges de dissipation majeure", m_Charges.ToString() ); break;
				case WandEffect.Meteor		:		list.Add( "Charges de m�t�orite", m_Charges.ToString() ); break;
				case WandEffect.Polymorph	:		list.Add( "Charges de m�tamorphose", m_Charges.ToString() ); break;
				case WandEffect.Earthquake:			list.Add( "Charges de s�isme", m_Charges.ToString() ); break;
				case WandEffect.EnergyVortex:		list.Add( "Charges de vortez d'�nergie", m_Charges.ToString() ); break;
				case WandEffect.Ressurect:			list.Add( "Charges de ressurection", m_Charges.ToString() ); break;
				case WandEffect.SummonAir:			list.Add( "Charges de cr�ation d'�l�mental d'air", m_Charges.ToString() ); break;
				case WandEffect.SummonDeamon:		list.Add( "Charges de cr�ation de d�mon", m_Charges.ToString() ); break;
				case WandEffect.SummonEarth:		list.Add( "Charges de cr�ation d'�l�mental de terre", m_Charges.ToString() ); break;
				case WandEffect.SummonWater:		list.Add( "Charges de cr�ation d'�l�mental d'eau", m_Charges.ToString() ); break;
				case WandEffect.SummonFire:	 		list.Add( "Charges de cr�ation d'�l�mental de feu", m_Charges.ToString() ); break;
			}
		}

		public override void OnSingleClick( Mobile from )
		{
			ArrayList attrs = new ArrayList();

			if ( DisplayLootType )
			{
				if ( LootType == LootType.Blessed )
					attrs.Add( new EquipInfoAttribute( 1038021 ) ); // blessed
				else if ( LootType == LootType.Cursed )
					attrs.Add( new EquipInfoAttribute( 1049643 ) ); // cursed
			}

			if ( !Identified )
			{
				attrs.Add( new EquipInfoAttribute( 1038000 ) ); // Unidentified
			}
			else
			{
				int num = 0;

				switch ( m_WandEffect )
				{	
					case WandEffect.ManaDraining:			num = 3002041; break;
					case WandEffect.Clumsiness:			num = 3002011; break;
					case WandEffect.Identification:		num = 1044063; break;
					case WandEffect.Healing:			num = 3002014; break;
					case WandEffect.Feeblemindedness:	num = 3002013; break;
					case WandEffect.Weakness:			num = 3002018; break;
					case WandEffect.MagicArrow:			num = 3002015; break;
					case WandEffect.Harming:			num = 3002022; break;
					case WandEffect.Fireball:			num = 3002028; break;
					case WandEffect.GreaterHealing:		num = 3002039; break;
					case WandEffect.Lightning:			num = 3002040; break;
					case WandEffect.Nourriture:			num = 3002012; break;
					case WandEffect.Nightsight:			num = 3002016; break;
					case WandEffect.ReactiveArmor:		num = 3002017; break;
					case WandEffect.Agilite:			num = 3002019; break;
					case WandEffect.Clairvoyance:		num = 3002020; break;
					case WandEffect.Antidote:			num = 3002021; break;
					case WandEffect.MagicTrap:			num = 3002023; break;
					case WandEffect.MagicUntrap:		num = 3002024; break;
					case WandEffect.Protection:			num = 3002025; break;
					case WandEffect.Force:				num = 3002026; break;
					case WandEffect.Benediction:		num = 3002027; break;
					case WandEffect.MagicLock:			num = 3002029; break;
					case WandEffect.Poison:				num = 3002030; break;
					case WandEffect.Telekinesis:		num = 3002031; break;
					case WandEffect.Teleportation:		num = 3002032; break;
					case WandEffect.Sesame:				num = 3002033; break;
					case WandEffect.Muraille:			num = 3002034; break;
					case WandEffect.SuperAntidote:		num = 3002035; break;
					case WandEffect.SuperProtection:	num = 3002036; break;
					case WandEffect.Malediction:		num = 3002037; break;
					case WandEffect.FireWall:			num = 3002038; break;
					case WandEffect.Rappel:				num = 3002042; break;
					case WandEffect.Tornade:			num = 3002043; break;
					case WandEffect.PlaineDuN�ant:		num = 3002044; break;
					case WandEffect.Incognito:			num = 3002045; break;
					case WandEffect.Reflexion:			num = 3002046; break;
					case WandEffect.MindBlast:			num = 3002047; break;
					case WandEffect.Paralyse:			num = 3002048; break;
					case WandEffect.PoissonWall:		num = 3002049; break;
					case WandEffect.SummonAnimal:		num = 3002050; break;
					case WandEffect.Dispel:				num = 3002051; break;
					case WandEffect.EnergyBolt:			num = 3002052; break;
					case WandEffect.Explosion:			num = 3002053; break;
					case WandEffect.Invisibilite:		num = 3002054; break;
					case WandEffect.Mark:				num = 3002055; break;
					case WandEffect.Mal�dictionG�n�rale: num = 3002056; break;
					case WandEffect.ParalyseField:		num = 3002057; break;
					case WandEffect.Revelateur:			num = 3002058; break;
					case WandEffect.ChainLightning:		num = 3002059; break;
					case WandEffect.EnergyFiel	:		num = 3002060; break;
					case WandEffect.GateTraval:			num = 3002062; break;
					case WandEffect.ManaVampire:		num = 3002063; break;
					case WandEffect.MassDispel	:		num = 3002064; break;
					case WandEffect.Meteor		:		num = 3002065; break;
					case WandEffect.Polymorph	:		num = 3002066; break;
					case WandEffect.Earthquake:			num = 3002067; break;
					case WandEffect.EnergyVortex:		num = 3002068; break;
					case WandEffect.Ressurect:		num = 3002069; break;
					case WandEffect.SummonAir:		num = 3002070; break;
					case WandEffect.SummonDeamon:		num = 3002071; break;
					case WandEffect.SummonEarth:		num = 3002072; break;
					case WandEffect.SummonWater:		num = 3002073; break;
					case WandEffect.SummonFire:		num = 3002074; break;
					
					
					
					
				}

				if ( num > 0 )
					attrs.Add( new EquipInfoAttribute( num, m_Charges ) );
			}

			int number;

			if ( Name == null )
			{
				number = 1017085;
			}
			else
			{
				this.LabelTo( from, Name );
				number = 1041000;
			}

			if ( attrs.Count == 0 && Crafter == null && Name != null )
				return;

			EquipmentInfo eqInfo = new EquipmentInfo( number, Crafter, false, (EquipInfoAttribute[])attrs.ToArray( typeof( EquipInfoAttribute ) ) );

			from.Send( new DisplayEquipmentInfo( this, eqInfo ) );
		}

		public void Cast( Spell spell )
		{
			bool m = Movable;

			Movable = false;
			spell.Cast();
			Movable = m;
		}

		public virtual void OnWandUse( Mobile from )
		{
			from.Target = new WandTarget( this );
		}

		public virtual void DoWandTarget( Mobile from, object o )
		{
			if ( Deleted || Charges <= 0 || Parent != from || o is StaticTarget || o is LandTarget )
				return;

			if ( OnWandTarget( from, o ) )
				ConsumeCharge( from );
		}

		public virtual bool OnWandTarget( Mobile from, object o )
		{
			return true;
		}
	}
}
