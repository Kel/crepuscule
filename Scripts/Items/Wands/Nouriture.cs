using System;
using Server;
using Server.Spells;
using Server.Spells.First;
using Server.Targeting;

namespace Server.Items
{
	public class NourritureWand : BaseWand 
	{
		[Constructable]
		public NourritureWand( ) : base( WandEffect.Nourriture, 5, 30 )
		{
		}

		public NourritureWand( Serial serial ) : base( serial )
		{
		}
		
		private static FoodInfo[] m_Food = new FoodInfo[]
			{
				new FoodInfo( typeof( Grapes ), "a grape bunch" ),
				new FoodInfo( typeof( Ham ), "a ham" ),
				new FoodInfo( typeof( CheeseWedge ), "a wedge of cheese" ),
				new FoodInfo( typeof( Muffins ), "muffins" ),
				new FoodInfo( typeof( FishSteak ), "a fish steak" ),
				new FoodInfo( typeof( Ribs ), "cut of ribs" ),
				new FoodInfo( typeof( CookedBird ), "a cooked bird" ),
				new FoodInfo( typeof( Sausage ), "sausage" ),
				new FoodInfo( typeof( Apple ), "an apple" ),
				new FoodInfo( typeof( Peach ), "a peach" )
			};
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
		
		

		public override void OnWandUse( Mobile from )
		{
					
				FoodInfo foodInfo = m_Food[Utility.Random( m_Food.Length )];
				Item food = foodInfo.Create();

				if ( food != null )
				{
					from.AddToBackpack( food );

					// You magically create food in your backpack:
					from.SendLocalizedMessage( 1042695, true, " " + foodInfo.Name );

					from.FixedParticles( 0, 10, 5, 2003, EffectLayer.RightHand );
					from.PlaySound( 0x1E2 );
				}
			}

			
		}
	}
	
