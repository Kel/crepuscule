using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x3002, 0x3002)]
	public class bottes_modestes : BaseShoes
	{

		[Constructable]
		public bottes_modestes() : this( 0 )
		{
		}

		[Constructable]
        public bottes_modestes(int hue)
            : base(0x3002, hue)
		{
			Name = "Bottes";
			Weight = 3.0;
		}

		public bottes_modestes( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
