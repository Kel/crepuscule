using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x36A7, 0x36A7 )]
	public class bottes_musket : BaseShoes
	{
		[Constructable]
		public bottes_musket() : this( 0 )
		{
		}

		[Constructable]
		public bottes_musket( int hue ) : base( 0x36A7, hue )
		{
			Name = "Bottes";
			Weight = 3.0;
		}

		public bottes_musket( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
