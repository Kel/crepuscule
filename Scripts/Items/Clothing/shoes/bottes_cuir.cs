using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x3005, 0x3005)]
	public class bottes_cuir : BaseShoes
	{

		[Constructable]
		public bottes_cuir() : this( 0 )
		{
		}

		[Constructable]
        public bottes_cuir(int hue)
            : base(0x3005, hue)
		{
			Name = "Bottes";
			Weight = 3.0;
		}

		public bottes_cuir( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
