using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x36A2, 0x36A2 )]
	public class bottes_poil : BaseShoes
	{
		[Constructable]
		public bottes_poil() : this( 0 )
		{
		}

		[Constructable]
		public bottes_poil( int hue ) : base( 0x36A2, hue )
		{
			Name = "Bottes en poil";
			Weight = 3.0;
		}

		public bottes_poil( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
