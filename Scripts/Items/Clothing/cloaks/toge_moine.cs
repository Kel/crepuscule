using System;
using Server.Misc;

namespace Server.Items
{
	
	public class toge_moine : BaseOuterTorso
	{
		

		[Constructable]
		public toge_moine() : this( 0 )
		{
		}

		[Constructable]
        public toge_moine(int hue)
            : base(0x302D)
		{
			Name = "Toge"; 
			Weight = 5.0;
            Layer = Layer.Earrings;
		}

		public toge_moine( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 1 ); // version

		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();



			if ( Weight == 4.0 )
				Weight = 5.0;
		}
	}



} 
