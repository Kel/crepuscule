using System;
using Server.Misc;

namespace Server.Items
{

    public class foulard_Voleur : Base10
	{
		[Constructable]
		public foulard_Voleur() : base( 0x35F3 )
		{
			Name="Foulard";
			Weight = 0.1;
            m_Uses = 50;
		}

        private int m_Uses;

        public int Uses
        {
            get { return m_Uses; }
            set { m_Uses = value; }
        }

        public foulard_Voleur(Serial serial)
            : base(serial)
		{

		}

        public override bool OnEquip(Mobile from)
        {
            if (this.Uses > 0)
            {
                this.Uses--;
                return base.OnEquip(from);
            }
            else
            {
                from.SendMessage("Le foulard est us�.");
                this.Delete();
                return false;
            }

        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version

            writer.Write((int)m_Uses);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

            m_Uses = reader.ReadInt();
		}
	}



}