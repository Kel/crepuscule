using System;
using Server.Misc;

namespace Server.Items
{
	
	public class foulard_noblet : Base10
	{
		[Constructable]
		public foulard_noblet() : base( 0x30F5 )
		{
			Name="Foulard";
			Weight = 0.1;
		}

		public foulard_noblet( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
