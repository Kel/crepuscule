using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3028, 0x3028 )]
	public class Fourreau : BaseWaist
	{

		[Constructable]
		public Fourreau() : base( 0x3028 )
		{
			Weight = 1.0;
			Layer = Layer.Waist;
			Name = "Fourreau";
		}

		public Fourreau( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}