using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x300d, 0x300d )]
	public class PantalonDruide : BasePants
	{

		[Constructable]
		public PantalonDruide() : base( 0x300d )
		{
			Weight = 4.0;
			Layer = Layer.Pants;
			Name = "Pantalon Druide";
		}

		public PantalonDruide( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}