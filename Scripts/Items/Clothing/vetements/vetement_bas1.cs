using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x3000, 0x3000)]
    public class vetement_bas1 : BasePants
	{
		

		[Constructable]
		public vetement_bas1() : this( 0 )
		{
		}

		[Constructable]
        public vetement_bas1(int hue)
            : base(0x3000, hue)
		{
			Name = "Vetement";
			Weight = 1.0;
		}

		public vetement_bas1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
