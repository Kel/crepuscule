using System;
using Server.Misc;

namespace Server.Items
{

	public class vetement_a3 : BasePants
	{
		[Constructable]
		public vetement_a3() : base( 0x30C0 )
		{
			Name="Vetement";
			Weight = 0.1;
		}

		public vetement_a3( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}


} 
