using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x300e, 0x300e )]
	public class BrasDruide : BasePants
	{

		[Constructable]
		public BrasDruide() : base( 0x300e )
		{
			Weight = 3.0;
			Layer = Layer.Arms;
			Name = "Brassiers Druide";
		}

		public BrasDruide( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}