using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x302A, 0x302A)]
	public class vetement_haut1 : BasePants
	{
		

		[Constructable]
		public vetement_haut1() : this( 0 )
		{
		}

		[Constructable]
        public vetement_haut1(int hue)
            : base(0x302A, hue)
		{
			Name = "Vetement";
			Weight = 1.0;
		}

		public vetement_haut1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
