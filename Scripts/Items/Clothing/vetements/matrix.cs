using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x3060, 0x3060)]
    public class matrix : BaseOuterTorso
	{
		

		[Constructable]
		public matrix() : this( 0 )
		{
		}

		[Constructable]
        public matrix(int hue)
            : base(0x3060, hue)
		{
			Name = "Costume";
			Weight = 1.0;
            Hue = 2048;
		}

		public matrix( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
