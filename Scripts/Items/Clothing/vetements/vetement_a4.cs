using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x36AD, 0x36AD )]
	public class vetement_a4 : BaseMiddleTorso
	{
		

		[Constructable]
		public vetement_a4() : this( 0 )
		{
		}

		[Constructable]
		public vetement_a4( int hue ) : base( 0x36AD, hue )
		{
			Name = "Vetement";
			Weight = 3.0;
		}

		public vetement_a4( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
