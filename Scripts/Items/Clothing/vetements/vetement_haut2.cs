using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x302B, 0x302B)]
    public class vetement_haut2 : BasePants
	{
		

		[Constructable]
		public vetement_haut2() : this( 0 )
		{
		}

		[Constructable]
        public vetement_haut2(int hue)
            : base(0x302B, hue)
		{
			Name = "Vetement";
			Weight = 1.0;
		}

		public vetement_haut2( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
