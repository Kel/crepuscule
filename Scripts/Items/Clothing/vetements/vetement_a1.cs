using System;
using Server.Misc;

namespace Server.Items
{

	public class vetement_a1 : BasePants
	{
		[Constructable]
		public vetement_a1() : base( 0x30BE )
		{
			Name="Vetement";
			Weight = 0.1;
		}

		public vetement_a1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}


} 
