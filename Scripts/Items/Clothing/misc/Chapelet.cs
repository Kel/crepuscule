using System;
using Server.Misc;

namespace Server.Items
{
	public class Chapelet : BaseWaist
	{

		[Constructable]
		public Chapelet() : this( 0 )
		{
		}
		
		[Constructable]
		public Chapelet( int hue) : base( 0x30B4, hue )
		{
			Weight = 0.2;
			Layer = Layer.Waist;
			Name = "Chapelet Thaarien";
		}

		public Chapelet( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
