using System;

namespace Server.Items
{

[FlipableAttribute( 0x3105, 0x3105 )]
public class misc_bourse2 : BaseWaist
{
	[Constructable]
	public misc_bourse2() : this( 0 )
	{
	}

	[Constructable]
	public misc_bourse2( int hue ) : base( 0x3105, hue )
	{
		Name="Bourse";
		Weight = 1.0;
	}

	public misc_bourse2( Serial serial ) : base( serial )
	{
	}

	public override void Serialize( GenericWriter writer )
	{
		base.Serialize( writer );

		writer.Write( (int) 0 ); // version
	}

	public override void Deserialize( GenericReader reader )
	{
		base.Deserialize( reader );

		int version = reader.ReadInt();
	}
}
}
