using System;
using Server.Misc;

namespace Server.Items
{
	[Flipable( 0x1545, 0x1546 )]
	public class TeteOurs : BaseHat
	{

		[Constructable]
		public TeteOurs() : this( 0 )
		{
		}
		
		[Constructable]
		public TeteOurs( int hue ) : base( 0x1545, hue )
		{
			Weight = 20;
			Layer = Layer.Helm;
			Name = "T�te d'ours";
		}

		public TeteOurs( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
