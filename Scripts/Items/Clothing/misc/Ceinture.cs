using System;
using Server.Misc;

namespace Server.Items
{
	public class Ceinture : BaseWaist
	{

		[Constructable]
		public Ceinture() : this( 0 )
		{
		}
		
		[Constructable]
		public Ceinture( int hue ) : base( 0x3102, hue )
		{
			Weight = 0.5;
			Layer = Layer.Waist;
			Name = "Ceinture";
		}

		public Ceinture( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
