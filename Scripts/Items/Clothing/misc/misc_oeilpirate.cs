using System;
using Server.Misc;

namespace Server.Items
{

	public class misc_oeilpirate : BaseNecklace
	{
		[Constructable]
		public misc_oeilpirate() : base( 0x30B3 )
		{
			Name="Oueil pirate";
			Weight = 0.1;
		}

		public misc_oeilpirate( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
