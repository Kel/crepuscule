using System;
using Server.Misc;

namespace Server.Items
{

	public class misc_collier2 : BaseNecklace
	{
		[Constructable]
		public misc_collier2() : base( 0x30B6 )
		{
			Name="collier";
			Weight = 0.1;
		}

		public misc_collier2( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
