using System;
using Server.Misc;

namespace Server.Items
{

	public class misc_collier1 : BaseNecklace
	{
		[Constructable]
		public misc_collier1() : base( 0x30B5 )
		{
			Name="collier";
			Weight = 0.1;
		}

		public misc_collier1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
