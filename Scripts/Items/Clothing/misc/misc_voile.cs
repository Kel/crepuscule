using System;
using Server.Misc;

namespace Server.Items
{

	public class misc_voile : BaseNecklace
	{
		[Constructable]
		public misc_voile() : base( 0x30BD )
		{
			Name="Voile";
			Weight = 0.1;
		}

		public misc_voile( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
