using System;

namespace Server.Items
{

[FlipableAttribute( 0x3103, 0x3103 )]
public class misc_bourse1 : BaseWaist
{
	[Constructable]
	public misc_bourse1() : this( 0 )
	{
	}

	[Constructable]
	public misc_bourse1( int hue ) : base( 0x3103, hue )
	{
		Name="Bourse";
		Weight = 1.0;
	}

	public misc_bourse1( Serial serial ) : base( serial )
	{
	}

	public override void Serialize( GenericWriter writer )
	{
		base.Serialize( writer );

		writer.Write( (int) 0 ); // version
	}

	public override void Deserialize( GenericReader reader )
	{
		base.Deserialize( reader );

		int version = reader.ReadInt();
	}
}
}
