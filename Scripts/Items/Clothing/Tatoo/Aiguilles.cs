using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0x2806, 0x2806 )]
	public class Aiguilles : BaseTool
	{

		[Constructable]
		public Aiguilles() : base( 0x2806 )
		{
			Weight = 2.0;
			Hue = 0x79C;
			Name= "Aiguilles de tatoueur";
		}

		[Constructable]
		public Aiguilles( int uses ) : base( uses, 0x2806 )
		{
			Weight = 2.0;
			Hue = 0x79C;
			Name= "Aiguilles de tatoueur";
		}

		public Aiguilles( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}