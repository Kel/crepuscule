using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x376, 0x376 )]
	public class PoissonRouge : CrepusculeItem
	{

		[Constructable]
		public PoissonRouge() : base( 0x376)
		{
			Weight = 1.0;
			Hue = 0x26;
			Name = "Oursin Rouge";
		}

		public PoissonRouge( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}