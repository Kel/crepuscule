using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x1848, 0x1848 )]
	public class EncreRareRouge : CrepusculeItem
	{

		[Constructable]
		public EncreRareRouge() : base( 0x1848)
		{
			Weight = 1.0;
			Hue = 0x26;
			Name = "Encre Rare Rouge";
		}

		public EncreRareRouge( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}