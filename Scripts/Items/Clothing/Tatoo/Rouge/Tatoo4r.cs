using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307D, 0x307D )]
	public class Tatoo4r : CrepusculeItem
	{

		[Constructable]
		public Tatoo4r() : base( 0x307D)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x26;
			Name = "Tribal";
		}

		public Tatoo4r( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}