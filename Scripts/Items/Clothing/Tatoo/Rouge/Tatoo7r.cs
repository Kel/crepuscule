using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3080, 0x3080 )]
	public class Tatoo7r : CrepusculeItem
	{

		[Constructable]
		public Tatoo7r() : base( 0x3080)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x26;
			Name = "Tribal";
		}

		public Tatoo7r( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}