using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307A, 0x307A )]
	public class Tatoo1r : CrepusculeItem
	{

		[Constructable]
		public Tatoo1r() : base( 0x307A)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x26;
			Name = "Tribal";
		}

		public Tatoo1r( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}