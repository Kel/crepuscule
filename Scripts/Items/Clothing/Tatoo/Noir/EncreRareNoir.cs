using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x1848, 0x1848 )]
	public class EncreRareNoir : CrepusculeItem
	{

		[Constructable]
		public EncreRareNoir() : base( 0x1848)
		{
			Weight = 1.0;
			Hue = 0x8f3;
			Name = "Encre Rare noire";
		}

		public EncreRareNoir( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}