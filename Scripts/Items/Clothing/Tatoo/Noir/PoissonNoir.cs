using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3bFF, 0x3bFF )]
	public class PoissonNoir : CrepusculeItem
	{

		[Constructable]
		public PoissonNoir() : base( 0x3bFF)
		{
			Weight = 1.0;
			Hue = 0x8f3;
			Name = "Calmar Noir";
		}

		public PoissonNoir( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}