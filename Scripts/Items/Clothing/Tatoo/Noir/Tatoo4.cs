using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307D, 0x307D )]
	public class Tatoo4 : CrepusculeItem
	{

		[Constructable]
		public Tatoo4() : base( 0x307D)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			
			Name = "Tribal";
		}

		public Tatoo4( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}