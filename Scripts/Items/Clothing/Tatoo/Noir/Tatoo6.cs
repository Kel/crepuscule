using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307f, 0x307f )]
	public class Tatoo6 : CrepusculeItem
	{

		[Constructable]
		public Tatoo6() : base( 0x307f)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			
			Name = "Tribal";
		}

		public Tatoo6( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}