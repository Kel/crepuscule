using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3080, 0x3080 )]
	public class Tatoo7v : CrepusculeItem
	{

		[Constructable]
		public Tatoo7v() : base( 0x3080)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x233;
			Name = "Tribal";
		}

		public Tatoo7v( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}