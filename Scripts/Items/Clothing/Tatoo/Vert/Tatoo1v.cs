using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307A, 0x307A )]
	public class Tatoo1v : CrepusculeItem
	{

		[Constructable]
		public Tatoo1v() : base( 0x307A)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x233;
			Name = "Tribal";
		}

		public Tatoo1v( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}