using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3AD1, 0x3AD1 )]
	public class PoissonVert : CrepusculeItem
	{

		[Constructable]
		public PoissonVert() : base( 0x3AD1)
		{
			Weight = 1.0;
			Hue = 0x233;
			Name = "Fougere Marine";
		}

		public PoissonVert( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}