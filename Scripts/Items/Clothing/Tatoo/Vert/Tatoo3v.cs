using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307C, 0x307C )]
	public class Tatoo3v : CrepusculeItem
	{

		[Constructable]
		public Tatoo3v() : base( 0x307C)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x233;
			Name = "Tribal";
		}

		public Tatoo3v( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}