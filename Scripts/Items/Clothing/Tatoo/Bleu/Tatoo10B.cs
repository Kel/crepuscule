using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3083, 0x3083 )]
	public class Tatoo10B : CrepusculeItem
	{

		[Constructable]
		public Tatoo10B() : base( 0x3083)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x3;
			Name = "Tribal";
		}

		public Tatoo10B( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}