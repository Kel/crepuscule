using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307E, 0x307E )]
	public class Tatoo5B : CrepusculeItem
	{

		[Constructable]
		public Tatoo5B() : base( 0x307E)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x3;
			Name = "Tribal";
		}

		public Tatoo5B( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}