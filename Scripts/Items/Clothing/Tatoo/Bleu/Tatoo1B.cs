using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x307A, 0x307A )]
	public class Tatoo1B : CrepusculeItem
	{

		[Constructable]
		public Tatoo1B() : base( 0x307A)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x3;
			Name = "Tribal";
		}

		public Tatoo1B( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}