using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3080, 0x3080 )]
	public class Tatoo7B : CrepusculeItem
	{

		[Constructable]
		public Tatoo7B() : base( 0x3080)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			Hue = 0x3;
			Name = "Tribal";
		}

		public Tatoo7B( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}