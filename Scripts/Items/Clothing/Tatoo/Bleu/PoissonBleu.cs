using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x3bFF, 0x3bFF )]
	public class PoissonBleu : CrepusculeItem
	{

		[Constructable]
		public PoissonBleu() : base( 0x3bFF)
		{
			Weight = 1.0;
			Hue = 0x3;
			Name = "Poisson Bleu";
		}

		public PoissonBleu( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}