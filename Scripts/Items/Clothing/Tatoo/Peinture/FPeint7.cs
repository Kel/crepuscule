using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30a6, 0x30a6 )]
	public class FPeint7 : CrepusculeItem
	{

		[Constructable]
		public FPeint7() : base( 0x30a6)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			
			Name = "Tatouage";
		}

		public FPeint7( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}