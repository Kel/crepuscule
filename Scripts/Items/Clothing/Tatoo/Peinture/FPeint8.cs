using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30a7, 0x30a7 )]
	public class FPeint8 : CrepusculeItem
	{

		[Constructable]
		public FPeint8() : base( 0x30a7)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			
			Name = "Tatouage";
		}

		public FPeint8( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}