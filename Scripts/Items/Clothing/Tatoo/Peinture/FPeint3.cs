using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x30a2, 0x30a2 )]
	public class FPeint3 : CrepusculeItem
	{

		[Constructable]
		public FPeint3() : base( 0x3a2)
		{
			Weight = 1.0;
			Layer = Layer.Earrings;
			
			Name = "Tatouage";
		}

		public FPeint3( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}