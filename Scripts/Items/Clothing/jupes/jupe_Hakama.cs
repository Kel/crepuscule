using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x36A9, 0x36A9 )]
	public class jupe_Hakama : BaseOuterLegs
	{

		[Constructable]
		public jupe_Hakama() : this( 0 )
		{
		}

		[Constructable]
		public jupe_Hakama( int hue ) : base( 0x36A9, hue )
		{
			Name="Jupe";
			Weight = 1.0;
		}

		public jupe_Hakama( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
