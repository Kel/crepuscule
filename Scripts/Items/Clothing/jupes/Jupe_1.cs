using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x35F5, 0x35F5 )]
	public class Jupe_1 : BaseOuterLegs
	{

		[Constructable]
		public Jupe_1() : this( 0 )
		{
		}

		[Constructable]
		public Jupe_1( int hue ) : base( 0x35F5, hue )
		{
			Name="Jupe";
			Weight = 1.0;
		}

		public Jupe_1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
