using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x35C5, 0x35C5)]
    public class Pantalon2 : BasePants
	{

		[Constructable]
		public Pantalon2() : this( 0 )
		{
		}

		[Constructable]
        public Pantalon2(int hue)
            : base(0x35C5, hue)
		{
			Name="Pantalon";
			Weight = 1.0;
		}

		public Pantalon2( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
