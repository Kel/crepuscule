using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x36AA, 0x36AA)]
    public class Pantalon1 : BasePants
	{

		[Constructable]
		public Pantalon1() : this( 0 )
		{
		}

		[Constructable]
        public Pantalon1(int hue)
            : base(0x36AA, hue)
		{
			Name="Pantalon";
			Weight = 1.0;
		}

		public Pantalon1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
