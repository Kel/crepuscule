using System;
using Server.Misc;

namespace Server.Items
{

    [Flipable(0x3018, 0x3018)]
    public class Pantalon3 : BaseOuterLegs
	{

		[Constructable]
		public Pantalon3() : this( 0 )
		{
		}

		[Constructable]
        public Pantalon3(int hue)
            : base(0x3018, hue)
		{
			Name="Pantalon";
			Weight = 1.0;
		}

		public Pantalon3( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
