using System;
using Server.Misc;

namespace Server.Items
{

	



	public class bandage_torse : BaseOuterTorso
	{
		[Constructable]
        public bandage_torse()
            : base(0x3109)
		{
			Name="Bandages";
			Weight = 0.1;
		}

		public bandage_torse( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
