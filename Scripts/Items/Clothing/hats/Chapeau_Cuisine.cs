using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x36A6, 0x36A6 )]
	public class Chapeau_Cuisine : BaseHat
	{

		[Constructable]
		public Chapeau_Cuisine() : this( 0 )
		{
		}

		[Constructable]
		public Chapeau_Cuisine( int hue ) : base( 0x36A6, hue )
		{
			Name="Chapeau Cuisine";
			Weight = 1.0;
		}

		public Chapeau_Cuisine( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
