using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x35A8, 0x35A8 )]
	public class Chapeau_tricorne1 : BaseHat
	{

		[Constructable]
		public Chapeau_tricorne1() : this( 0 )
		{
		}

		[Constructable]
		public Chapeau_tricorne1( int hue ) : base( 0x35A8, hue )
		{
			Name="Chapeau";
			Weight = 1.0;
		}

		public Chapeau_tricorne1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
