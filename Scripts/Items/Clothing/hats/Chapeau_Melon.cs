using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x35AD, 0x35AD )]
	public class Chapeau_Melon : BaseHat
	{

		[Constructable]
		public Chapeau_Melon() : this( 0 )
		{
		}

		[Constructable]
		public Chapeau_Melon( int hue ) : base( 0x35AD, hue )
		{
			Name="Chapeau Melon";
			Weight = 1.0;
		}

		public Chapeau_Melon( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
