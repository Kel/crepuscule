// created on 22/03/2003 at 12:15
// Masque d'anonymat par Morlock

using System;
using Server;
using Server.Mobiles;
using Server.Network;

namespace Server.Items
{
	public class Cagoule : BaseHat
	{
		private string m_title;
 
		[Constructable]
		public Cagoule() : this( 0 )
		{
		}

		[Constructable]
		public Cagoule( int hue ) : base( 0x1409, 0x387 )
		{
			Weight = 2.0;
			Name = "Cagoule";
		}

		public Cagoule( Serial serial ) : base( serial )
		{
		}

		public override bool OnEquip (Mobile from)
		{
			m_title = from.Title;
			if (from.Female)
			{
				from.NameMod = "Femme en cagoule";
				from.Title = "";
			}
			else
			{
				from.NameMod = "Homme en cagoule";
				from.Title = "";
			}
			return base.OnEquip( from );
		}

		public override void OnRemoved ( object parent )
		{
			if (parent is Mobile)
			{
				((Mobile) parent).NameMod = null;
				((Mobile) parent).Title = m_title;
			}
			base.OnRemoved(parent);
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 1 ); // version
			writer.Write( (string) m_title );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
			if ( version == 0 )
			{
				m_title = reader.ReadString();
			}
			m_title = reader.ReadString();
		}
	}
}