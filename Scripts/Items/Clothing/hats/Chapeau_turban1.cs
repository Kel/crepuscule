using System;
using Server.Misc;

namespace Server.Items
{

	[Flipable( 0x3606, 0x3606 )]
	public class Chapeau_turban1 : BaseHat
	{

		[Constructable]
		public Chapeau_turban1() : this( 0 )
		{
		}

		[Constructable]
		public Chapeau_turban1( int hue ) : base( 0x3606, hue )
		{
			Name="Chapeau";
			Weight = 1.0;
		}

		public Chapeau_turban1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
} 
