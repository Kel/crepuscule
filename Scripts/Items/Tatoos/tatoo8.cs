using System;
using Server.Misc;

namespace Server.Items
{





    public class tatoo8 : BaseTatoo
	{
		[Constructable]
		public tatoo8() : base( 0x3081 )
		{
			Name="Tatouage";
			Weight = 0.1;
		}

		public tatoo8( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
