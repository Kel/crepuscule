using System;
using Server.Misc;

namespace Server.Items
{





    public class tatoo7 : BaseTatoo
	{
		[Constructable]
		public tatoo7() : base( 0x3080 )
		{
			Name="Tatouage";
			Weight = 0.1;
		}

		public tatoo7( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
