using System;
using Server.Misc;

namespace Server.Items
{





    public class tatoo5 : BaseTatoo
	{
		[Constructable]
		public tatoo5() : base( 0x307E )
		{
			Name="Tatouage";
			Weight = 0.1;
		}

		public tatoo5( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}



} 
