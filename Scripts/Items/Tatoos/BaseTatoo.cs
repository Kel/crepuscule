﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Items
{
    public abstract class BaseTatoo : BaseClothing
    {
        public BaseTatoo(int itemID)
            : this(itemID, 0)
        {
        }

        public BaseTatoo(int itemID, int hue)
            : base(itemID, Layer.Talisman, hue)
        {
        }

        public BaseTatoo(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}
