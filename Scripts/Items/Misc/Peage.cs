using System;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
		public class Peage : Moongate
		{
			[Constructable]
			public Peage() : base( false )
				{
					Movable = false;
					Hue = 0x480;
					Dispellable = false;
					Name = "Peage";
					Visible = false;
					TargetMap = Map.Felucca;
					Light = LightType.Empty;
				}
			private int m_Taxe;
			[CommandProperty( AccessLevel.GameMaster )]
			public int Taxe
			{
				get{ return m_Taxe; }
				set{ m_Taxe = value; }
			}
	
			public override bool OnMoveOver( Mobile m )
			{
				if (m.Backpack.ConsumeTotal(typeof(Gold), Taxe))
				{
					BaseCreature.TeleportPets( m, Target, TargetMap );
	
					m.MoveToWorld( Target, TargetMap );
				}
				else
				{
					m.SendMessage("Vous n'avez pas ass� d'argent pour passer ici, il vous faut " + Taxe + " couronnes");
					return false;
				}
				
				return true;
				
			}
			
					
			public Peage( Serial serial ) : base( serial )
				{
				}
	
			public override void Serialize( GenericWriter writer )
				{
					base.Serialize( writer );
	
					writer.Write( (int) 1 ); // version
					writer.Write( (int)m_Taxe );
				}
	
			public override void Deserialize( GenericReader reader )
				{
					base.Deserialize( reader );
	
					int version = reader.ReadInt();
					switch ( version )
					{
						case 0:
						{
							m_Taxe = reader.ReadInt();
							break;
						}
					}
				}
		}
}
