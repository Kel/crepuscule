using System;
using Server;
using Server.Mobiles;

namespace Server.Items
{
	public class Gold: CrepusculeItem
	{
		[Constructable]
		public Gold() : this( 1 )
		{
		}

		[Constructable]
		public Gold( int amountFrom, int amountTo ) : this( Utility.Random( amountFrom, amountTo - amountFrom ) )
		{
		}

		[Constructable]
		public Gold( int amount ) : base( 0xEED )
		{
			Stackable = true;
			Weight = 0.001;
			Amount = amount;
			Name = " �cus";
		}

		public Gold( Serial serial ) : base( serial )
		{
		}

        public override void OnDoubleClick(Mobile from)
        {
            PlayerMobile pm = from as PlayerMobile;

            base.OnDoubleClick(from);
            if ( IsChildOf( from.Backpack ))
            {
            	if (this.Amount == 1000)
            	{
                	this.Delete();
                	pm.AddToBackpack(new GoldBar(1));
                	//	pm.SendMessage("*Message*");
            	}
            	else if (this.Amount >= 1001)
            	{
                	this.Amount -= 1000;
                	pm.AddToBackpack(new GoldBar(1));
                	//	pm.SendMessage("*Message*");
            	}
            }
            else
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
        }

        public override int GetDropSound()
        {
            if (Amount <= 1)
                return 0x2E4;
            else if (Amount <= 5)
                return 0x2E5;
            else
                return 0x2E6;
        }

        protected override void OnAmountChange(int oldValue)
        {
            int newValue = this.Amount;

            UpdateTotal(this, TotalType.Gold, newValue - oldValue);
        }

        public override int GetTotal(TotalType type)
        {
            int baseTotal = base.GetTotal(type);

            if (type == TotalType.Gold)
                baseTotal += this.Amount;

            return baseTotal;
        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}



namespace Server.Items
{
    [FlipableAttribute(0x1BF2, 0x1BEF)]
    public class GoldBar: CrepusculeItem
    {
        [Constructable]
        public GoldBar()
            : this(1)
        {
        }

        [Constructable]
        public GoldBar(int amount)
            : base(0x1BF2)
        {
            Stackable = true;
            Hue = 53;
            Amount = amount;
            Weight = 1.0;
        }

        public override void AddNameProperty(ObjectPropertyList list)
        {
            list.Add(Amount == 1 ? "{0} Lingot d'or" : "{0} Lingots d'or", Amount);
        }

        public override void OnSingleClick(Mobile from)
        {
            LabelTo(from, Amount == 1 ? "{0} Lingot d'or" : "{0} Lingots d'or", Amount);
        }

        public override Item Dupe(int amount)
        {
            return base.Dupe(new GoldBar(amount), amount);
        }

        public override void OnDoubleClick(Mobile from)
        {
            PlayerMobile pm = from as PlayerMobile;

            base.OnDoubleClick(from);
			if ( IsChildOf( from.Backpack ))
            {
            	if (this.Amount == 1)
            	{
                	this.Delete();
               		pm.AddToBackpack(new Gold(1000));
            	}
            	else if (this.Amount >= 2)
            	{
                	this.Amount -= 1;
                	pm.AddToBackpack(new Gold(1000));
            	}
            }
            else
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
        }

        public GoldBar(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}
