using System;
using Server.Items;
using Server.Mobiles;
using Server.Targeting;
using MoveImpl = Server.Movement.MovementImpl;

namespace Server.Items
{
    public class Pfosten : Item
    {
        [Constructable]
        public Pfosten()
            : this(1)
        {
        }

        [Constructable]
        public Pfosten(int amount)
            : base(0x14E8)
        {
            Movable = false;
        }

        public Pfosten(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }

        public override void OnDoubleClick(Mobile from)
        {
            from.SendMessage("Quelle monture voulez-vous attacher?");
            from.BeginTarget(-1, true, TargetFlags.None, new TargetCallback(Get_OnTarget));
        }

        public virtual void Get_OnTarget(Mobile from, object targeted)
        {
            if (targeted is BaseMount)
            {
                BaseMount mou = (BaseMount)targeted;
                if (mou.ControlMaster == from)
                {
                    if (from.InRange(this.GetWorldLocation(), 3) == true)
                    {
                        if (mou.ControlOrder != OrderType.Stay)
                        {
                            from.SendMessage("Vous attachez votre monture");
                            ((BaseCreature)mou).ControlOrder = OrderType.Stay;
                            return;
                        }

                        else
                        {
                            from.SendMessage("Vous d�tachez votre monture");
                            ((BaseCreature)mou).ControlOrder = OrderType.Guard;
                            return;
                        }
                    }
                    else
                    {
                        from.SendMessage("Vous �tes trop loin pour attacher cet animal");
                    }
                }
                else
                {
                    from.SendMessage("Ce n'est pas votre monture");
                }
            }
            else
            {
                from.SendMessage("Ce n'est pas une monture");
            }
        }
    }
}

