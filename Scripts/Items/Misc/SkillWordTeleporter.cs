using System;
using System.Text;
using Server.Items;
using Server.Mobiles;
using Server.Network;

namespace Server.Custom.items.Misc
{

    public class SkillWordTeleporter : SkillTeleporter
    {
        private string m_Substring;
        private int m_Keyword;
        private int m_Range;

        private short m_Acrobatie;
        private short m_Alchimie;
        private short m_Assassinat;
        private short m_Bardisme;
        private short m_Bricolage;
        private short m_Arcs;
        private short m_Crochetage;
        private short m_Armes_de_Melee;
        private short m_Armes_a_Distance;
        private short m_Armes_de_Jets;
        private short m_Lutte;
        private short m_Couture;
        private short m_Deplacement_Silencieux;
        private short m_Enchantement;
        private short m_Endurance_Accrue;
        private short m_Erudition;
        private short m_Empathie_Animale;
        private short m_Ferraillerie;
        private short m_Furtivite;
        private short m_Herboristerie;
        private short m_Magie_arcanique;
        private short m_Sorcellerie;
        private short m_Necromancie;
        private short m_Magie_divine;
        private short m_Magie_naturelle;
        private short m_Medecine;
        private short m_Pose_de_Pieges;
        private short m_Rage;
        private short m_Sabotage;
        private short m_Telepathie;
        private short m_Vol_a_la_Tire;

        #region Aptitudes

        [CommandProperty(AccessLevel.GameMaster)]
        public short Acrobatie
        {
            get { return m_Acrobatie; }
            set { m_Acrobatie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Alchimie
        {
            get { return m_Alchimie; }
            set { m_Alchimie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Assassinat
        {
            get { return m_Assassinat; }
            set { m_Assassinat = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Bardisme
        {
            get { return m_Bardisme; }
            set { m_Bardisme = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Bricolage
        {
            get { return m_Bricolage; }
            set { m_Bricolage = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Arcs
        {
            get { return m_Arcs; }
            set { m_Arcs = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Crochetage
        {
            get { return m_Crochetage; }
            set { m_Crochetage = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Armes_de_Melee
        {
            get { return m_Armes_de_Melee; }
            set { m_Armes_de_Melee = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Armes_a_Distance
        {
            get { return m_Armes_a_Distance; }
            set { m_Armes_a_Distance = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Armes_de_Jets
        {
            get { return m_Armes_de_Jets; }
            set { m_Armes_de_Jets = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Lutte
        {
            get { return m_Lutte; }
            set { m_Lutte = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Couture
        {
            get { return m_Couture; }
            set { m_Couture = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Deplacement_Silencieux
        {
            get { return m_Deplacement_Silencieux; }
            set { m_Deplacement_Silencieux = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Enchantement
        {
            get { return m_Enchantement; }
            set { m_Enchantement = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Endurance_Accrue
        {
            get { return m_Endurance_Accrue; }
            set { m_Endurance_Accrue = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Erudition
        {
            get { return m_Erudition; }
            set { m_Erudition = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Empathie_Animale
        {
            get { return m_Empathie_Animale; }
            set { m_Empathie_Animale = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Ferraillerie
        {
            get { return m_Ferraillerie; }
            set { m_Ferraillerie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Furtivite
        {
            get { return m_Furtivite; }
            set { m_Furtivite = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Herboristerie
        {
            get { return m_Herboristerie; }
            set { m_Herboristerie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Magie_arcanique
        {
            get { return m_Magie_arcanique; }
            set { m_Magie_arcanique = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Sorcellerie
        {
            get { return m_Sorcellerie; }
            set { m_Sorcellerie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Necromancie
        {
            get { return m_Necromancie; }
            set { m_Necromancie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Magie_divine
        {
            get { return m_Magie_divine; }
            set { m_Magie_divine = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Magie_naturelle
        {
            get { return m_Magie_naturelle; }
            set { m_Magie_naturelle = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Medecine
        {
            get { return m_Medecine; }
            set { m_Medecine = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Pose_de_Pieges
        {
            get { return m_Pose_de_Pieges; }
            set { m_Pose_de_Pieges = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Rage
        {
            get { return m_Rage; }
            set { m_Rage = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Sabotage
        {
            get { return m_Sabotage; }
            set { m_Sabotage = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Telepathie
        {
            get { return m_Telepathie; }
            set { m_Telepathie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public short Vol_a_la_Tire
        {
            get { return m_Vol_a_la_Tire; }
            set { m_Vol_a_la_Tire = value; }
        }
        #endregion

        [CommandProperty(AccessLevel.GameMaster)]
        public string Substring
        {
            get { return m_Substring; }
            set { m_Substring = value; InvalidateProperties(); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Keyword
        {
            get { return m_Keyword; }
            set { m_Keyword = value; InvalidateProperties(); }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Range
        {
            get { return m_Range; }
            set { m_Range = value; InvalidateProperties(); }
        }

        public override bool HandlesOnSpeech { get { return true; } }

        public override void OnSpeech(SpeechEventArgs e)
        {
            if (!e.Handled && Active)
            {
                Mobile mo = e.Mobile;
                if(!(mo is RacePlayerMobile))return;

                RacePlayerMobile m = (RacePlayerMobile)mo;

                if (!Creatures && !m.Player)
                    return;

                if (!m.InRange(GetWorldLocation(), m_Range))
                    return;


                        /*if (m.Acrobatie  < m_Acrobatie) return;
                        if (m.Alchimie  < m_Alchimie) return;
                        if (m.Assassinat  < m_Assassinat) return;
                        if (m.Capacities[CapacityName.Bardic].Value  < m_Bardisme) return;
                        if (m.Bricolage  < m_Bricolage) return;
                        if (m.Arcs  < m_Arcs) return;
                        if (m.Crochetage  < m_Crochetage) return;
                        if (m.Armes_de_Melee  < m_Armes_de_Melee) return;
                        if (m.Armes_a_Distance  < m_Armes_a_Distance) return;
                        if (m.Armes_de_Jets  < m_Armes_de_Jets) return;
                        if (m.Lutte  < m_Lutte) return;
                        if (m.Couture  < m_Couture) return;
                        if (m.Deplacement_Silencieux  < m_Deplacement_Silencieux) return;
                        if (m.Enchantement  < m_Enchantement) return;
                        if (m.Endurance_Accrue  < m_Endurance_Accrue) return;
                        if (m.Capacities[CapacityName.Lore].Value  < m_Erudition) return;
                        if (m.Empathie_Animale  < m_Empathie_Animale) return;
                        if (m.Ferraillerie  < m_Ferraillerie) return;
                        if (m.Furtivite  < m_Furtivite) return;
                        if (m.Herboristerie  < m_Herboristerie) return;
                        if (m.Capacities[CapacityName.Wizardry].Value  < m_Magie_arcanique) return;
                        if (m.Capacities[CapacityName.Sorcery].Value  < m_Sorcellerie) return;
                        if (m.Capacities[CapacityName.Necromancy].Value  < m_Necromancie) return;
                        if (m.Capacities[CapacityName.Theology].Value  < m_Magie_divine) return;
                        if (m.Capacities[CapacityName.Nature].Value  < m_Magie_naturelle) return;
                        if (m.Capacities[CapacityName.Healing].Value  < m_Medecine) return;
                        if (m.Pose_de_Pieges  < m_Pose_de_Pieges) return;
                        if (m.Rage  < m_Rage) return;
                        if (m.Sabotage  < m_Sabotage) return;
                        if (m.Capacities[CapacityName.Telepathy].Value  < m_Telepathie) return;
                        if (m.Vol_a_la_Tire  < m_Vol_a_la_Tire) return;*/


                        if (Active)
                        {
                            if (!Creatures && !m.Player)
                                return;

                            Skill sk = m.Skills[m_Skill];

                            if (sk == null || sk.Base < m_Required)
                            {
                                if (m.BeginAction(this))
                                {
                                    if (m_MessageString != null)
                                        m.Send(new UnicodeMessage(Serial, ItemID, MessageType.Regular, 0x3B2, 3, "ENU", null, m_MessageString));
                                    else if (m_MessageNumber != 0)
                                        m.Send(new MessageLocalized(Serial, ItemID, MessageType.Regular, 0x3B2, 3, m_MessageNumber, null, ""));

                                    Timer.DelayCall(TimeSpan.FromSeconds(5.0), new TimerStateCallback(EndMessageLock), m);
                                }

                                return;
                            }

                            bool isMatch = false;

                            if (m_Keyword >= 0 && e.HasKeyword(m_Keyword))
                                isMatch = true;
                            else if (m_Substring != null && e.Speech.ToLower().IndexOf(m_Substring.ToLower()) >= 0)
                                isMatch = true;

                            if (!isMatch)
                                return;




                            e.Handled = true;
                            StartTeleport(m);


                        }
            }
        }

        public override bool OnMoveOver(Mobile m)
        {
            return true;
        }

        public override void GetProperties(ObjectPropertyList list)
        {
            base.GetProperties(list);

            list.Add(1060661, "Range\t{0}", m_Range);

            if (m_Keyword >= 0)
                list.Add(1060662, "Keyword\t{0}", m_Keyword);

            if (m_Substring != null)
                list.Add(1060663, "Substring\t{0}", m_Substring);
        }

        [Constructable]
        public SkillWordTeleporter()
        {
            m_Keyword = -1;
            m_Substring = null;
        }

        public SkillWordTeleporter(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version


            writer.Write(m_Acrobatie);
            writer.Write(m_Alchimie);
            writer.Write(m_Assassinat);
            writer.Write(m_Bardisme);
            writer.Write(m_Bricolage);
            writer.Write(m_Arcs);
            writer.Write(m_Crochetage);
            writer.Write(m_Armes_de_Melee);
            writer.Write(m_Armes_a_Distance);
            writer.Write(m_Armes_de_Jets);
            writer.Write(m_Lutte);
            writer.Write(m_Couture);
            writer.Write(m_Deplacement_Silencieux);
            writer.Write(m_Enchantement);
            writer.Write(m_Endurance_Accrue);
            writer.Write(m_Erudition);
            writer.Write(m_Empathie_Animale);
            writer.Write(m_Ferraillerie);
            writer.Write(m_Furtivite);
            writer.Write(m_Herboristerie);
            writer.Write(m_Magie_arcanique);
            writer.Write(m_Sorcellerie);
            writer.Write(m_Necromancie);
            writer.Write(m_Magie_divine);
            writer.Write(m_Magie_naturelle);
            writer.Write(m_Medecine);
            writer.Write(m_Pose_de_Pieges);
            writer.Write(m_Rage);
            writer.Write(m_Sabotage);
            writer.Write(m_Telepathie);
            writer.Write(m_Vol_a_la_Tire);

            writer.Write(m_Substring);
            writer.Write(m_Keyword);
            writer.Write(m_Range);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        m_Acrobatie = reader.ReadShort();
                        m_Alchimie = reader.ReadShort();
                        m_Assassinat = reader.ReadShort();
                        m_Bardisme = reader.ReadShort();
                        m_Bricolage = reader.ReadShort();
                        m_Arcs = reader.ReadShort();
                        m_Crochetage = reader.ReadShort();
                        m_Armes_de_Melee = reader.ReadShort();
                        m_Armes_a_Distance = reader.ReadShort();
                        m_Armes_de_Jets = reader.ReadShort();
                        m_Lutte = reader.ReadShort();
                        m_Couture = reader.ReadShort();
                        m_Deplacement_Silencieux = reader.ReadShort();
                        m_Enchantement = reader.ReadShort();
                        m_Endurance_Accrue = reader.ReadShort();
                        m_Erudition = reader.ReadShort();
                        m_Empathie_Animale = reader.ReadShort();
                        m_Ferraillerie = reader.ReadShort();
                        m_Furtivite = reader.ReadShort();
                        m_Herboristerie = reader.ReadShort();
                        m_Magie_arcanique = reader.ReadShort();
                        m_Sorcellerie = reader.ReadShort();
                        m_Necromancie = reader.ReadShort();
                        m_Magie_divine = reader.ReadShort();
                        m_Magie_naturelle = reader.ReadShort();
                        m_Medecine = reader.ReadShort();
                        m_Pose_de_Pieges = reader.ReadShort();
                        m_Rage = reader.ReadShort();
                        m_Sabotage = reader.ReadShort();
                        m_Telepathie = reader.ReadShort();
                        m_Vol_a_la_Tire = reader.ReadShort();


                        m_Substring = reader.ReadString();
                        m_Keyword = reader.ReadInt();
                        m_Range = reader.ReadInt();

                        break;
                    }
            }
        }
    }
}
