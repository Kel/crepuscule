using System;
using Server.Network;

namespace Server.Items
{
	[FlipableAttribute( 0x182E, 0x182F, 0x1830, 0x1831 )]
	public class SmallFlask: CrepusculeItem
	{
		[Constructable]
		public SmallFlask() : base( 0x182E )
		{
			Weight = 1.0;
			Movable = true;
		}

		public SmallFlask( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x182A, 0x182B, 0x182C, 0x182D )]
	public class MediumFlask: CrepusculeItem
	{
		[Constructable]
		public MediumFlask() : base( 0x182A )
		{
			Weight = 1.0;
			Movable = true;
		}

		public MediumFlask( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x183B, 0x183C, 0x183D )]
	public class LargeFlask: CrepusculeItem
	{
		[Constructable]
		public LargeFlask() : base( 0x183B )
		{
			Weight = 1.0;
			Movable = true;
		}

		public LargeFlask( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x1832, 0x1833, 0x1834, 0x1835, 0x1836, 0x1837 )]
	public class CurvedFlask: CrepusculeItem
	{
		[Constructable]
		public CurvedFlask() : base( 0x1832 )
		{
			Weight = 1.0;
			Movable = true;
		}

		public CurvedFlask( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x1838, 0x1839, 0x183A )]
	public class LongFlask: CrepusculeItem
	{
		[Constructable]
		public LongFlask() : base( 0x1838 )
		{
			Weight = 1.0;
			Movable = true;
		}

		public LongFlask( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[Flipable( 0x1810, 0x1811 )]
	public class SpinningHourglass: CrepusculeItem
	{
		[Constructable]
		public SpinningHourglass() : base( 0x1810 )
		{
			Weight = 1.0;
			Movable = true;
		}

		public SpinningHourglass( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class GreenBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public GreenBottle() : base( 0x0EFB ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public GreenBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class RedBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public RedBottle() : base( 0x0EFC ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public RedBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallBrownBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallBrownBottle() : base( 0x0EFD ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallBrownBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallGreenBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallGreenBottle() : base( 0x0F01 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallGreenBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallVioletBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallVioletBottle() : base( 0x0F02 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallVioletBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class TinyYellowBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public TinyYellowBottle() : base( 0x0F03 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public TinyYellowBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 
	//remove 
	public class SmallBlueFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallBlueFlask() : base( 0x182A ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallBlueFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallYellowFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallYellowFlask() : base( 0x182B ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallYellowFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallRedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallRedFlask() : base( 0x182C ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallRedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallEmptyFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallEmptyFlask() : base( 0x182D ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallEmptyFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class YellowBeaker: CrepusculeItem 
	{ 
		[Constructable] 
		public YellowBeaker() : base( 0x182E ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public YellowBeaker( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class RedBeaker: CrepusculeItem 
	{ 
		[Constructable] 
		public RedBeaker() : base( 0x182F ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public RedBeaker( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class BlueBeaker: CrepusculeItem 
	{ 
		[Constructable] 
		public BlueBeaker() : base( 0x1830 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public BlueBeaker( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class GreenBeaker: CrepusculeItem 
	{ 
		[Constructable] 
		public GreenBeaker() : base( 0x1831 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public GreenBeaker( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyCurvedFlaskW: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyCurvedFlaskW() : base( 0x1832 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public EmptyCurvedFlaskW( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class RedCurvedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public RedCurvedFlask() : base( 0x1833 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public RedCurvedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class LtBlueCurvedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public LtBlueCurvedFlask() : base( 0x1834 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public LtBlueCurvedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyCurvedFlaskE: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyCurvedFlaskE() : base( 0x1835 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public EmptyCurvedFlaskE( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class BlueCurvedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public BlueCurvedFlask() : base( 0x1836 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public BlueCurvedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class GreenCurvedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public GreenCurvedFlask() : base( 0x1837 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public GreenCurvedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class RedRibbedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public RedRibbedFlask() : base( 0x1838 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public RedRibbedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class VioletRibbedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public VioletRibbedFlask() : base( 0x1839 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public VioletRibbedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyRibbedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyRibbedFlask() : base( 0x183A ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public EmptyRibbedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class LargeYellowFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public LargeYellowFlask() : base( 0x183B ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public LargeYellowFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class LargeVioletFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public LargeVioletFlask() : base( 0x183C ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public LargeVioletFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class LargeEmptyFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public LargeEmptyFlask() : base( 0x183D ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public LargeEmptyFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class AniRedRibbedFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public AniRedRibbedFlask() : base( 0x183E ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public AniRedRibbedFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class AniLargeVioletFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public AniLargeVioletFlask() : base( 0x1841 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public AniLargeVioletFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class AniSmallBlueFlask: CrepusculeItem 
	{ 
		[Constructable] 
		public AniSmallBlueFlask() : base( 0x1844 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public AniSmallBlueFlask( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallBlueBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallBlueBottle() : base( 0x1847 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallBlueBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class SmallGreenBottle2: CrepusculeItem 
	{ 
		[Constructable] 
		public SmallGreenBottle2() : base( 0x1848 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public SmallGreenBottle2( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0x185B, 0x185C )] 
	public class EmptyVialsWRack: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyVialsWRack() : base( 0x185B ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public EmptyVialsWRack( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0x185D, 0x185E )] 
	public class FullVialsWRack: CrepusculeItem 
	{ 
		[Constructable] 
		public FullVialsWRack() : base( 0x185D ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public FullVialsWRack( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyJar: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyJar() : base( 0x1005 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public EmptyJar( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class FullJar: CrepusculeItem 
	{ 
		[Constructable] 
		public FullJar() : base( 0x1006 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public FullJar( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class HalfEmptyJar: CrepusculeItem 
	{ 
		[Constructable] 
		public HalfEmptyJar() : base( 0x1007 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public HalfEmptyJar( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class VioletStemmedBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public VioletStemmedBottle() : base( 0x0F00 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public VioletStemmedBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyJars3: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyJars3() : base( 0x0E46 ) 
		{ 
			Weight = 3.0;
			Movable = true; 
		} 

		public EmptyJars3( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyJars4: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyJars4() : base( 0x0E47 ) 
		{ 
			Weight = 4.0;
			Movable = true; 
		} 

		public EmptyJars4( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class FullJars3: CrepusculeItem 
	{ 
		[Constructable] 
		public FullJars3() : base( 0x0E4A ) 
		{ 
			Weight = 3.0;
			Movable = true; 
		} 

		public FullJars3( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class FullJars4: CrepusculeItem 
	{ 
		[Constructable] 
		public FullJars4() : base( 0x0E4B ) 
		{ 
			Weight = 4.0;
			Movable = true; 
		} 

		public FullJars4( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0x0E44, 0x0E45 )] 
	public class EmptyJars2: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyJars2() : base( 0x0E44 ) 
		{ 
			Weight = 2.0;
			Movable = true; 
		} 

		public EmptyJars2( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0x0E48, 0x0E49 )] 
	public class FullJars2: CrepusculeItem 
	{ 
		[Constructable] 
		public FullJars2() : base( 0x0E48 ) 
		{ 
			Weight = 2.0;
			Movable = true; 
		} 

		public FullJars2( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0x0E4C, 0x0E4D )] 
	public class HalfEmptyJars2: CrepusculeItem 
	{ 
		[Constructable] 
		public HalfEmptyJars2() : base( 0x0E4C ) 
		{ 
			Weight = 2.0;
			Movable = true; 
		} 

		public HalfEmptyJars2( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class EmptyVial: CrepusculeItem 
	{ 
		[Constructable] 
		public EmptyVial() : base( 0x0E24 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public EmptyVial( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class HourglassAni: CrepusculeItem 
	{ 
		[Constructable] 
		public HourglassAni() : base( 0x1811 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public HourglassAni( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class Hourglass: CrepusculeItem 
	{ 
		[Constructable] 
		public Hourglass() : base( 0x1810 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public Hourglass( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	public class TinyRedBottle: CrepusculeItem 
	{ 
		[Constructable] 
		public TinyRedBottle() : base( 0x0F04 ) 
		{ 
			Weight = 1.0;
			Movable = true; 
		} 

		public TinyRedBottle( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt();
		}
	}
}