using System;
using Server.Mobiles;

namespace Server.Items
{
	public class BodyCrystal: CrepusculeItem
	{
        internal int m_MinutesDelay = 0;
        internal int m_Hue;
        internal int m_BodyMod;
        internal bool m_Female;
        internal bool m_OneUse;
        internal Mobile m_User;
        internal int m_OriginalHue;
        internal int m_OriginalBodyMod;
        internal bool m_OriginalFemale;
        internal DateTime m_TimerStarted;

		[Constructable]
		public BodyCrystal() : base( 0x1F1C )
		{
			Weight = 1.0;
			Name = "Cristal lumineux";
            m_TimerStarted = DateTime.MinValue;
		}

		public BodyCrystal( Serial serial ) : base( serial )
		{
		}
        [CommandProperty(AccessLevel.GameMaster)]
        public int OriginalHue
        {
            get { return m_OriginalHue; }
            set { m_OriginalHue = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int OriginalBodyMod
        {
            get { return m_OriginalBodyMod; }
            set { m_OriginalBodyMod = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool OriginalFemale
        {
            get { return m_OriginalFemale; }
            set { m_OriginalFemale = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MinutesDelay
        {
            get { return m_MinutesDelay; }
            set { m_MinutesDelay = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int NewHue
        {
            get { return m_Hue; }
            set { m_Hue = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int BodyMod
        {
            get { return m_BodyMod; }
            set { m_BodyMod = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Female
        {
            get { return m_Female; }
            set { m_Female = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool OneUse
        {
            get { return m_OneUse; }
            set { m_OneUse = value; }
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (from is RacePlayerMobile)
            {
                if (this.IsChildOf(from.Backpack))
                {
                    if (m_OneUse && m_MinutesDelay > 0)
                    {
                        var timer = new InternalTimer(m_MinutesDelay, this, from as RacePlayerMobile);
                        DoTransform(from as RacePlayerMobile);
                        this.Movable = false;
                        this.LootType = LootType.Blessed;
                    }
                    else if (!m_OneUse)
                    {
                        if (m_User == null)
                        {
                            DoTransform(from as RacePlayerMobile);
                        }
                        else
                        {
                            DoTransformBack(from as RacePlayerMobile);
                        }
                    }

                    base.OnDoubleClick(from);
                }
                else
                {
                    from.SendMessage("Vous devez l'avoir dans le sac");
                }
            }
        }

        private void DoTransform(RacePlayerMobile from)
        {
            from.SendMessage("Vous vous transformez...");
            m_User = from;
            m_OriginalHue = from.Hue;
            m_OriginalFemale = from.Female;
            m_OriginalBodyMod = from.BodyMod;
            from.Hue = m_Hue;
            from.BodyMod = m_BodyMod;
            from.Female = m_Female;
        }

        private void DoTransformBack(RacePlayerMobile from)
        {
            from.SendMessage("Vous vous transformez...");
            m_User = null;
            from.Hue = m_OriginalHue;
            from.BodyMod = m_OriginalBodyMod;
            from.Female = m_OriginalFemale;
        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write((int) 0 );
            writer.Write(m_MinutesDelay);
            writer.Write(m_Hue);
            writer.Write(m_BodyMod);
            writer.Write(m_Female);
            writer.Write(m_OneUse);
            writer.Write(m_OriginalHue);
            writer.Write(m_OriginalBodyMod);
            writer.Write(m_OriginalFemale);
            writer.Write(m_TimerStarted);
            writer.Write(m_User);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

            m_MinutesDelay = reader.ReadInt();
            m_Hue = reader.ReadInt();
            m_BodyMod = reader.ReadInt();
            m_Female = reader.ReadBool();
            m_OneUse = reader.ReadBool();
            m_OriginalHue = reader.ReadInt();
            m_OriginalBodyMod = reader.ReadInt();
            m_OriginalFemale = reader.ReadBool();
            m_TimerStarted = reader.ReadDateTime();
            m_User = reader.ReadMobile();

            if (m_User != null && m_User is RacePlayerMobile && m_OneUse && m_TimerStarted > DateTime.MinValue)
            {
                var diff = m_MinutesDelay - (int)DateTime.Now.Subtract(m_TimerStarted).TotalMinutes;
                if (diff > 0)
                {
                    var timer = new InternalTimer(diff, this, m_User as RacePlayerMobile);
                }
                else
                {
                    var timer = new InternalTimer(0, this, m_User as RacePlayerMobile);
                }
            }
		}


        private class InternalTimer : Timer
        {
            internal int m_MinutesDelay = 0;
            internal BodyCrystal m_Item;
            internal RacePlayerMobile m_Player;

            public InternalTimer(int delay, BodyCrystal item, RacePlayerMobile player)
                : base(TimeSpan.FromMinutes(delay), TimeSpan.FromMinutes(delay))
            {
                m_MinutesDelay = delay;
                m_Item = item;
                m_Player = player;
                this.Start();
            }

            protected override void OnTick()
            {
                base.OnTick();

                if (m_Item.m_TimerStarted.AddMinutes(m_Item.m_MinutesDelay) <= DateTime.Now)
                {
                    m_Item.DoTransformBack(m_Player);
                    m_Item.Delete();

                    this.Stop();
                }
            }

        }
	}


}