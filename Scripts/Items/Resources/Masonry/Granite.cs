using System;
using Server.Items;
using Server.Network;
using Server.Engines;

namespace Server.Items
{
	public abstract class BaseGranite: CrepusculeItem, ICommodity
	{		
		string ICommodity.Description
		{
			get
			{
                var type = GetItemType();
                if (type == null)
                    return "roche";
                return String.Format(Amount == 1 ? "{0} {1} (roche)" : "{0} {1} (roches)", Amount, type.Craft.CraftAttributes.LabelName);
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 1 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
                case 1:
                {
                    break;
                }
				case 0:
				{
					reader.ReadInt();
					break;
				}
			}
		}

		public BaseGranite(  ) : base( 0x1779 )
		{
			Weight = 10.0;
            var type = GetItemType();
            if (type != null)
                Hue = type.Hue;
		}

		public BaseGranite( Serial serial ) : base( serial )
		{
		}

		public override int LabelNumber{ get{ return 1044607; } } // high quality granite

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

            var itemType = GetItemType();
            if (itemType != null && !itemType.Craft.IsStandard)
            {
                list.Add(itemType.Craft.CraftAttributes.LabelName);
            }
		}


        public sealed override Item Dupe(int amount)
        {
            return base.Dupe(GameItemType.CreateItem(GetItemType()), amount);
        }
	}

	public class Granite : BaseGranite
	{
		[Constructable]
		public Granite() : base()
		{
		}

		public Granite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class AcierGranite : BaseGranite
	{
		[Constructable]
		public AcierGranite() : base()
		{
		}

		public AcierGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class ErunoxGranite : BaseGranite
	{
		[Constructable]
		public ErunoxGranite() : base()
		{
		}

		public ErunoxGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class FistahteGranite : BaseGranite
	{
		[Constructable]
		public FistahteGranite() : base( )
		{
		}

		public FistahteGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class ErofithGranite : BaseGranite
	{
		[Constructable]
		public ErofithGranite() : base( )
		{
		}

		public ErofithGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class LandorasGranite : BaseGranite
	{
		[Constructable]
		public LandorasGranite() : base()
		{
		}

		public LandorasGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class OrGranite : BaseGranite
	{
		[Constructable]
		public OrGranite() : base()
		{
		}

		public OrGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class SylveronGranite : BaseGranite
	{
		[Constructable]
		public SylveronGranite() : base( )
		{
		}

		public SylveronGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class SanguineGranite : BaseGranite
	{
		[Constructable]
		public SanguineGranite() : base()
		{
		}

		public SanguineGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
	public class MortineGranite : BaseGranite
	{
		[Constructable]
		public MortineGranite() : base()
		{
		}

		public MortineGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class NecrolitheGranite : BaseGranite
	{
		[Constructable]
		public NecrolitheGranite() : base()
		{
		}

		public NecrolitheGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class DevasGranite : BaseGranite
	{
		[Constructable]
		public DevasGranite() : base()
		{
		}

		public DevasGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class OnirineGranite : BaseGranite
	{
		[Constructable]
		public OnirineGranite() : base()
		{
		}

		public OnirineGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class PyrolitheGranite : BaseGranite
	{
		[Constructable]
		public PyrolitheGranite() : base()
		{
		}

		public PyrolitheGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class OmbryqueGranite : BaseGranite
	{
		[Constructable]
		public OmbryqueGranite() : base()
		{
		}

		public OmbryqueGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class LumercaGranite : BaseGranite
	{
		[Constructable]
		public LumercaGranite() : base()
		{
		}

		public LumercaGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class DamascusGranite : BaseGranite
	{
		[Constructable]
		public DamascusGranite() : base()
		{
		}

		public DamascusGranite( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}