using System;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.Engines.Craft;
using Server.Engines;

namespace Server.Items
{
	public abstract class BaseOre: CrepusculeItem, ICommodity
	{

        string ICommodity.Description
        {
            get
            {
                var type = GameItemType.FindType(this.GetType());
                if (type == null)
                    return "min�ral";
                return String.Format(Amount == 1 ? "{0} {1} (min�ral)" : "{0} {1} (min�raux)", Amount, type.Craft.CraftAttributes.LabelName);
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public Type IngotType { get; set; }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 2 ); // version
            writer.Write(IngotType);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
			switch ( version )
			{
                case 2:
                {
                    IngotType = reader.ReadType();
                    break;
                }
			}
		}

		public BaseOre() : this(typeof(IronIngot), 1 )
		{
		}

		public BaseOre(Type ingotType, int amount ) : base( 0x19B9 )
		{
            IngotType = ingotType;
			Stackable = true;
			Weight = 15.0;
			Amount = amount;
            Hue = GameItemType.GetHue(this);
		}

		public BaseOre( Serial serial ) : base( serial )
		{
		}


		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

            var itemType = GetItemType();
            if (itemType != null && !itemType.Craft.IsStandard)
            {
                list.Add(itemType.Craft.CraftAttributes.LabelName);
            }
		}

		public override int LabelNumber
		{
			get
			{
				return 1042853; // iron ore;
			}
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !Movable )
				return;

			if ( from.InRange( this.GetWorldLocation(), 2 ) )
			{
				from.SendLocalizedMessage( 501971 ); // Select the forge on which to smelt the ore, or another pile of ore with which to combine it.
				from.Target = new InternalTarget( this );
			}
			else
			{
				from.SendLocalizedMessage( 501976 ); // The ore is too far away.
			}
		}

        public override Item Dupe(int amount)
        {
            return base.Dupe(GameItemType.CreateItem(GetItemType()), amount);
        }


		private class InternalTarget : Target
		{
			private BaseOre m_Ore;

			public InternalTarget( BaseOre ore ) :  base ( 2, false, TargetFlags.None )
			{
				m_Ore = ore;
			}

			private bool IsForge( object obj )
			{
				if ( obj.GetType().IsDefined( typeof( ForgeAttribute ), false ) )
					return true;

				int itemID = 0;

				if ( obj is Item )
					itemID = ((Item)obj).ItemID;
				else if ( obj is StaticTarget )
					itemID = ((StaticTarget)obj).ItemID & 0x3FFF;

				return ( itemID == 4017 || (itemID >= 6522 && itemID <= 6569) );
			}

			protected override void OnTarget( Mobile from, object targeted )
			{
				if ( m_Ore.Deleted )
					return;

				if ( !from.InRange( m_Ore.GetWorldLocation(), 2 ) )
				{
					from.SendLocalizedMessage( 501976 ); // The ore is too far away.
					return;
				}

				if ( IsForge( targeted ) )
				{
					double difficulty = 30;

                    var itemType = GameItemType.FindType(m_Ore);
                    if (itemType != null && itemType.Craft.HarvestDifficulty > 0)
                        difficulty = itemType.Craft.HarvestDifficulty;

					double minSkill = difficulty - 25.0;
					double maxSkill = difficulty + 25.0;
					
					if ( difficulty > 50.0 && difficulty > from.Skills[SkillName.Mining].Value )
					{
						from.SendLocalizedMessage( 501986 ); // You have no idea how to smelt this strange ore!
						return;
					}

					if ( from.CheckTargetSkill( SkillName.Mining, targeted, minSkill, maxSkill ) )
					{
						int toConsume = m_Ore.Amount;

						if ( toConsume <= 0 )
						{
							from.SendLocalizedMessage( 501987 ); // There is not enough metal-bearing ore in this pile to make an ingot.
						}
						else
						{
							if ( toConsume > 30000 )
								toConsume = 30000;

                            if (itemType != null && m_Ore.IngotType != null)
                            {
                                var ingot = GameItemType.CreateItem(m_Ore.IngotType);
                                ingot.Amount = toConsume * 2;

                                m_Ore.Consume(toConsume);
                                from.AddToBackpack(ingot);
                                from.PlaySound( 0x57 );
                            }

							from.SendLocalizedMessage( 501988 ); // You smelt the ore removing the impurities and put the metal in your backpack.
						}
					}
					else if ( m_Ore.Amount < 2 )
					{
						from.SendLocalizedMessage( 501989 ); // You burn away the impurities but are left with no useable metal.
						m_Ore.Delete();
					}
					else
					{
						from.SendLocalizedMessage( 501990 ); // You burn away the impurities but are left with less useable metal.
						m_Ore.Amount /= 2;
					}
				}
			}
		}
	}

	public class IronOre : BaseOre
	{
		[Constructable]
		public IronOre() : this( 1 )
		{
		}

		[Constructable]
		public IronOre( int amount ) : base( typeof(IronIngot), amount )
		{
		}

		public IronOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class AcierOre : BaseOre
	{
		[Constructable]
		public AcierOre() : this( 1 )
		{
		}

		[Constructable]
		public AcierOre( int amount ) : base( typeof(AcierIngot), amount )
		{
		}

		public AcierOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class ErunoxOre : BaseOre
	{
		[Constructable]
		public ErunoxOre() : this( 1 )
		{
		}

		[Constructable]
		public ErunoxOre( int amount ) : base( typeof(ErunoxIngot), amount )
		{
		}

		public ErunoxOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class FistahteOre : BaseOre
	{
		[Constructable]
		public FistahteOre() : this( 1 )
		{
		}

		[Constructable]
        public FistahteOre(int amount)
            : base(typeof(FistahteIngot), amount)
		{
		}

		public FistahteOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class ErofithOre : BaseOre
	{
		[Constructable]
		public ErofithOre() : this( 1 )
		{
		}

		[Constructable]
        public ErofithOre(int amount)
            : base(typeof(ErofithIngot), amount)
		{
		}

		public ErofithOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class LandorasOre : BaseOre
	{
		[Constructable]
		public LandorasOre() : this( 1 )
		{
		}

		[Constructable]
        public LandorasOre(int amount)
            : base(typeof(LandorasIngot), amount)
		{
		}

		public LandorasOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class OrOre : BaseOre
	{
		[Constructable]
		public OrOre() : this( 1 )
		{
		}

		[Constructable]
        public OrOre(int amount)
            : base(typeof(OrIngot), amount)
		{
		}

		public OrOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class SylveronOre : BaseOre
	{
		[Constructable]
		public SylveronOre() : this( 1 )
		{
		}

		[Constructable]
        public SylveronOre(int amount)
            : base(typeof(SylveronIngot), amount)
		{
		}

		public SylveronOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class SanguineOre : BaseOre
	{
		[Constructable]
		public SanguineOre() : this( 1 )
		{
		}

		[Constructable]
		public SanguineOre( int amount ) : base( typeof(SanguineIngot), amount )
		{
		}

		public SanguineOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	public class MortineOre : BaseOre
	{
		[Constructable]
		public MortineOre() : this( 1 )
		{
		}

		[Constructable]
        public MortineOre(int amount)
            : base(typeof(MortineIngot), amount)
		{
		}

		public MortineOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

	public class NecrolitheOre : BaseOre
	{
		[Constructable]
		public NecrolitheOre() : this( 1 )
		{
		}

		[Constructable]
        public NecrolitheOre(int amount)
            : base(typeof(NecrolitheIngot), amount)
		{
		}

		public NecrolitheOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

	public class DevasOre : BaseOre
	{
		[Constructable]
		public DevasOre() : this( 1 )
		{
		}

		[Constructable]
		public DevasOre( int amount ) : base( typeof(DevasIngot), amount )
		{
		}

		public DevasOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class OnirineOre : BaseOre
	{
		[Constructable]
		public OnirineOre() : this( 1 )
		{
		}

		[Constructable]
        public OnirineOre(int amount)
            : base(typeof(OnirineIngot), amount)
		{
		}

		public OnirineOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	public class PyrolitheOre : BaseOre
	{
		[Constructable]
		public PyrolitheOre() : this( 1 )
		{
		}

		[Constructable]
		public PyrolitheOre( int amount ) : base( typeof(PyrolitheIngot), amount )
		{
		}

		public PyrolitheOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

	public class OmbryqueOre : BaseOre
	{
		[Constructable]
		public OmbryqueOre() : this( 1 )
		{
		}

		[Constructable]
        public OmbryqueOre(int amount)
            : base(typeof(OmbryqueIngot), amount)
		{
		}

		public OmbryqueOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

	public class LumercaOre : BaseOre
	{
		[Constructable]
		public LumercaOre() : this( 1 )
		{
		}

		[Constructable]
        public LumercaOre(int amount)
            : base(typeof(LumercaIngot), amount)
		{
		}

		public LumercaOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

	public class DamascusOre : BaseOre
	{
		[Constructable]
		public DamascusOre() : this( 1 )
		{
		}

		[Constructable]
		public DamascusOre( int amount ) : base( typeof(DamascusIngot), amount )
		{
		}

		public DamascusOre( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

}