using System;
using Server.Items;
using Server.Network;
using Server.Engines;

namespace Server.Items
{
	public abstract class BaseIngot: CrepusculeItem, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                var type = GetItemType();
                if(type == null)
                    return "lingot";
                return String.Format(Amount == 1 ? "{0} {1} (lingot)" : "{0} {1} (lingots)", Amount, type.Craft.CraftAttributes.LabelName);
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 1 ); // version

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
				case 1:
				{
					reader.ReadInt();
					break;
				}
			}
		}

		public BaseIngot() : this(1 )
		{
		}

        public BaseIngot(int amount): base(0x1BF2)
		{
			Stackable = true;
			Weight = 3;
			Amount = amount;
            Hue = GameItemType.GetHue(this);
		}

		public BaseIngot( Serial serial ) : base( serial )
		{
		}

        public sealed override Item Dupe(int amount)
        {
            return base.Dupe(GameItemType.CreateItem(GetItemType()), amount);
        }

		public override void AddNameProperty( ObjectPropertyList list )
		{
			if ( Amount > 1 )
				list.Add( 1050039, "{0}\t#{1}", Amount, 1027154 ); // ~1_NUMBER~ ~2_ITEMNAME~
			else
				list.Add( 1027154 ); // ingots
		}

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

            var itemType = GetItemType();
            if (itemType != null && !itemType.Craft.IsStandard)
            {
                list.Add(itemType.Craft.CraftAttributes.LabelName);
            }
		}

		public override int LabelNumber
		{
			get
			{
				return 1042692;
			}
		}
	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class IronIngot : BaseIngot
	{
		[Constructable]
		public IronIngot() : this( 1 )
		{
		}

		[Constructable]
        public IronIngot(int amount): base(amount)
		{
		}

		public IronIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class AcierIngot : BaseIngot
	{
		[Constructable]
		public AcierIngot() : this( 1 )
		{
		}

		[Constructable]
		public AcierIngot( int amount ) : base(amount )
		{
		}

		public AcierIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class ErunoxIngot : BaseIngot
	{
		[Constructable]
		public ErunoxIngot() : this( 1 )
		{
		}

		[Constructable]
		public ErunoxIngot( int amount ) : base(amount )
		{
		}

		public ErunoxIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class FistahteIngot : BaseIngot
	{
		[Constructable]
		public FistahteIngot() : this( 1 )
		{
		}

		[Constructable]
		public FistahteIngot( int amount ) : base(amount )
		{
		}

		public FistahteIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class ErofithIngot : BaseIngot
	{
		[Constructable]
		public ErofithIngot() : this( 1 )
		{
		}

		[Constructable]
		public ErofithIngot( int amount ) : base(amount )
		{
		}

		public ErofithIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class LandorasIngot : BaseIngot
	{
		[Constructable]
		public LandorasIngot() : this( 1 )
		{
		}

		[Constructable]
		public LandorasIngot( int amount ) : base( amount )
		{
		}

		public LandorasIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class OrIngot : BaseIngot
	{
		[Constructable]
		public OrIngot() : this( 1 )
		{
		}

		[Constructable]
		public OrIngot( int amount ) : base( amount )
		{
		}

		public OrIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}


	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class SylveronIngot : BaseIngot
	{
		[Constructable]
		public SylveronIngot() : this( 1 )
		{
		}

		[Constructable]
		public SylveronIngot( int amount ) : base( amount )
		{
		}

		public SylveronIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class SanguineIngot : BaseIngot
	{
		[Constructable]
		public SanguineIngot() : this( 1 )
		{
		}

		[Constructable]
		public SanguineIngot( int amount ) : base(amount )
		{
		}

		public SanguineIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}
	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class MortineIngot : BaseIngot
	{
		[Constructable]
		public MortineIngot() : this( 1 )
		{
		}

		[Constructable]
		public MortineIngot( int amount ) : base(amount )
		{
		}

		public MortineIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class NecrolitheIngot : BaseIngot
	{
		[Constructable]
		public NecrolitheIngot() : this( 1 )
		{
		}

		[Constructable]
		public NecrolitheIngot( int amount ) : base(amount )
		{
		}

		public NecrolitheIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class DevasIngot : BaseIngot
	{
		[Constructable]
		public DevasIngot() : this( 1 )
		{
		}

		[Constructable]
		public DevasIngot( int amount ) : base(amount )
		{
		}

		public DevasIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class OnirineIngot : BaseIngot
	{
		[Constructable]
		public OnirineIngot() : this( 1 )
		{
		}

		[Constructable]
		public OnirineIngot( int amount ) : base( amount )
		{
		}

		public OnirineIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class PyrolitheIngot : BaseIngot
	{
		[Constructable]
		public PyrolitheIngot() : this( 1 )
		{
		}

		[Constructable]
		public PyrolitheIngot( int amount ) : base(amount )
		{
		}

		public PyrolitheIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class OmbryqueIngot : BaseIngot
	{
		[Constructable]
		public OmbryqueIngot() : this( 1 )
		{
		}

		[Constructable]
        public OmbryqueIngot(int amount)
            : base( amount)
		{
		}

		public OmbryqueIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class LumercaIngot : BaseIngot
	{
		[Constructable]
		public LumercaIngot() : this( 1 )
		{
		}

		[Constructable]
		public LumercaIngot( int amount ) : base(amount )
		{
		}

		public LumercaIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}

	[FlipableAttribute( 0x1BF2, 0x1BEF )]
	public class DamascusIngot : BaseIngot
	{
		[Constructable]
		public DamascusIngot() : this( 1 )
		{
		}

		[Constructable]
		public DamascusIngot( int amount ) : base(amount )
		{
		}

		public DamascusIngot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

	}
}