using System;
using Server.Items;

namespace Server.Items
{
	public class ReinforcedDaemonBone: CrepusculeItem, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( Amount == 1 ? "{0} os de d�mon" : "{0} os de d�mon", Amount );
			}
		}

		[Constructable]
		public ReinforcedDaemonBone() : this( 1 )
		{
		}

		[Constructable]
		public ReinforcedDaemonBone( int amount ) : base( 0xf7e )
		{
            Name = "Mal�fiques Ossements de D�mon";
            Hue = 2160;
			Stackable = true;
			Amount = amount;
			Weight = 1.0;
		}

        public ReinforcedDaemonBone(Serial serial)
            : base(serial)
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Bone( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}