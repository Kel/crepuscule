using System;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Items
{
	public class PlumeZak: CrepusculeItem
	{
		[Constructable]
		public PlumeZak() : this( 1 )
		{
		}
		
		[Constructable]
		public PlumeZak( int amount ) : base( 0x1bd1 )
		{
			Name = "Plume de Zhack";
                        Weight = 1.0;
			Hue = 1909;
			
		}

		public PlumeZak( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new PlumeZak( amount ), amount );
		}
	}
}