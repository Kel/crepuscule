using System;
using Server.Items;
using Server.Engines;

namespace Server.Items
{
	[FlipableAttribute( 0x1bdd, 0x1be0 )]
	public class Log: CrepusculeItem, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format(Amount == 1 ? "{0} rondin" : "{0} rondins", Amount);
			}
		}

		[Constructable]
		public Log() : this( 1 )
		{
		}

		[Constructable]
		public Log( int amount ) : base( 0x1BDD )
		{
			Stackable = true;
			Weight = 3.0;
			Amount = amount;
		}

		public Log( Serial serial ) : base( serial )
		{
		}

        public sealed override Item Dupe(int amount)
        {
            return base.Dupe(GameItemType.CreateItem(GetItemType()), amount);
        }


		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}