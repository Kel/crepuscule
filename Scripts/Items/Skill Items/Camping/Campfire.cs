using System;
using Server.Network;

namespace Server.Items
{
	public class Campfire: CrepusculeItem
	{
		private Timer m_Timer;

		[Constructable]
		public Campfire() : base( 0xDE3 )
		{
			Movable = false;
			Light = LightType.Circle300;

			m_Timer = new DecayTimer( this );
			m_Timer.Start();
		}

		public Campfire( Serial serial ) : base( serial )
		{
			m_Timer = new DecayTimer( this );
			m_Timer.Start();
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override void OnAfterDelete()
		{
			m_Timer.Stop();
		}

		private class DecayTimer : Timer
		{
			private Campfire m_Owner;

			public DecayTimer( Campfire owner ) : base( TimeSpan.FromMinutes( 2.0 ) )
			{
				Priority = TimerPriority.FiveSeconds;

				m_Owner = owner;
			}

			protected override void OnTick()
			{
				m_Owner.Delete();
			}
		}
	}
}