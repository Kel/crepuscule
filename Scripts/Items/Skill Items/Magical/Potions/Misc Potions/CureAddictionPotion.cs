using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CureAddictionPotion : BasePotion
	{
		[Constructable]
        public CureAddictionPotion()
            : base(0xF06, PotionEffect.CureAddictions)
		{
            Name = "Potion de soin d'addictions";
		}

		public CureAddictionPotion( Serial serial ) : base( serial )
		{
		}


		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override void Drink( Mobile from )
		{
            if (from is RacePlayerMobile)
                new CureAddiction(from as RacePlayerMobile, this).OnCast();
		}


	}
}