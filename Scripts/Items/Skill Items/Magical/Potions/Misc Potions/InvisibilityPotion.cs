using System;
using Server.Network;
using Server;
namespace Server.Items
{
    public class InvisibilityPotion : BasePotion, IAddictive
	{
		[Constructable]
		public InvisibilityPotion() : base( 0xF0B, PotionEffect.Invisibility )
		{
			Weight = 1.0;
			Movable = true;
			Hue = 553;
			Name = "Potion d'invisibilité";
		}

        
		public InvisibilityPotion( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	  
	  	public override void Drink( Mobile m )
        {
         	if ( m.InRange( this.GetWorldLocation(), 1 ) ) 
         	{ 
                       
                m.Hidden = true ;
                AddictionsHelper.DrinkAndCheck(m, this);
                this.Delete();
                m.AddToBackpack( new Bottle() );
         	} 
         	else 
         	{ 
            	m.LocalOverheadMessage( MessageType.Regular, 906, 1019045 ); // I can't reach that. 
         	} 
		}

        #region IAddictive Members

        public PotionAddiction GetAddiction()
        {
            return new PotionAddiction("Potion d'invisibilité", typeof(InvisibilityPotion), TimeSpan.FromDays(1));
        }

        public virtual double AddictionChance { get { return .05; } }

        #endregion
	}
}
