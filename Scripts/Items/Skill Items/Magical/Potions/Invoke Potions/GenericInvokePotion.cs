using System;
using Server.Network;
using Server;
using Server.Targets;
using Server.Targeting;
using Server.Spells;
using Server.Mobiles;
using System.Runtime.Serialization;
namespace Server.Items
{
    public class InvokePotion<T> : BasePotion where T : BaseCreature, new()
    {
        [Constructable]
        public InvokePotion()
            : base(0xF0C, PotionEffect.InvokePotion)
        {
            Weight = 1.0;
            Movable = true;
            Hue = 88;
        }

        public InvokePotion(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }

        public override void Drink(Mobile m)
        {
            if (Core.AOS && (m.Paralyzed || m.Frozen || (m.Spell != null && m.Spell.IsCasting)))
            {
                m.SendMessage("Vous ne pouvez rien faire maintenant!");
                return;
            }
            if (m.Followers >= m.FollowersMax)
            {
                m.SendMessage("Vous avez d�j� trop de cr�atures pour en invoquer davantage.");
                return;
            }   
            if (m.InRange(this.GetWorldLocation(), 1))
            {
                TimeSpan duration = TimeSpan.FromMinutes(10);
                SpellHelper.Summon(new T(), m, 0x217, duration, false, false);
                m.RevealingAction();
                this.Delete();
            }
            else
            {
                m.LocalOverheadMessage(MessageType.Regular, 906, 1019045); // I can't reach that. 
            }
        }
    }


}
