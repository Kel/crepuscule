﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Items
{
    public class JwilsonInvokePotion : InvokePotion<Jwilson>
    {
        [Constructable]
        public JwilsonInvokePotion() : base() { Name = "Potion d'invocation de blop";}
        public JwilsonInvokePotion(Serial serial) : base(serial) { }

        #region Serialize
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class AirElementalInvokePotion : InvokePotion<SummonedAirElemental>
    {
        [Constructable]
        public AirElementalInvokePotion() : base() { Name = "Potion d'invocation d'élémental d'air"; }
        public AirElementalInvokePotion(Serial serial) : base(serial) { }

        #region Serialize
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class WispInvokePotion : InvokePotion<Wisp>
    {
        [Constructable]
        public WispInvokePotion() : base() { Name = "Potion d'invocation de wisp"; }
        public WispInvokePotion(Serial serial) : base(serial) { }

        #region Serialize
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class SnowElementalInvokePotion : InvokePotion<SnowElemental>
    {
        [Constructable]
        public SnowElementalInvokePotion() : base() { Name = "Potion d'invocation d'élémental de neiges"; }
        public SnowElementalInvokePotion(Serial serial) : base(serial) { }

        #region Serialize
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }
}
