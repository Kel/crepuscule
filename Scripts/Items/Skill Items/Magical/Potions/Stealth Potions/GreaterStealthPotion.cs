using System;
using Server.Network;
using Server;
namespace Server.Items
{
    public class GreaterStealthPotion : BaseStealthPotion
    {
        [Constructable]
        public GreaterStealthPotion() : base("Potion d'infiltration supérieure", PotionEffect.SuperStealth) { }
        public GreaterStealthPotion(Serial serial) : base(serial) { }

        public override int Steps { get { return 12; } }
        public override double AddictionChance { get { return 0.02; } }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }


    }
}
