using System;
using Server.Network;
using Server;
using Server.Mobiles;
namespace Server.Items
{
	public abstract class BaseStealthPotion : BasePotion, IAddictive
	{
        public abstract int Steps { get; }

        public BaseStealthPotion(string name, PotionEffect effect)
            : base(0xF0C,PotionEffect.Stealth)
		{
			//Weight = 1.0;
			//Movable = true;
			Hue = 1048;
            Name = name;
		}

		public BaseStealthPotion( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	  
	  	public override void Drink( Mobile from )
      	{
            if (from.InRange(this.GetWorldLocation(), 1)) 
         	{
                
                
                BasePotion.PlayDrinkEffect(from);
                AddictionsHelper.DrinkAndCheck(from, this);

                //SkillMod skillMod = new TimedSkillMod(SkillName.Stealth, true, Convert.ToDouble(Steps * 2), TimeSpan.FromSeconds(Steps * 3));
                //(from as RacePlayerMobile).AddSkillMod(skillMod);
                this.Delete();
                from.AddToBackpack(new Bottle());
                from.Hidden = true;
                from.AllowedStealthSteps = Steps;
         	} 
         	else 
         	{
                from.LocalOverheadMessage(MessageType.Regular, 906, 1019045); // I can't reach that. 
         	} 
		}

        #region IAddictive Members

        public PotionAddiction GetAddiction()
        {
            return new PotionAddiction("Potion d'infiltration", typeof(BaseStealthPotion), TimeSpan.FromDays(1));
            //return new PotionAddiction("Potion d'infiltration", typeof(BaseStealthPotion), TimeSpan.FromSeconds(90));
        }

        public virtual double AddictionChance
        {
            get { return 0.02; }
        }

        #endregion
    }
}
