using System;
using Server.Network;
using Server;
namespace Server.Items
{
	public class StealthPotion : BaseStealthPotion
	{
		[Constructable]
		public StealthPotion() : base( "Potion d'infiltration", PotionEffect.Stealth  ){}
		public StealthPotion( Serial serial ) : base( serial ){}

        public override int Steps { get { return 7; } }
        public override double AddictionChance { get { return 0.01; } }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	      

    }
}
