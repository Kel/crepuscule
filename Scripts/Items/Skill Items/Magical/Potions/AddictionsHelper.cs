﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Items
{
    public interface IAddictive
    {
        PotionAddiction GetAddiction();
        double AddictionChance { get; }
    }

    public class PotionAddiction
    {
        public string Name;
        public Type Type;
        public TimeSpan Delay;
        public double Difficulty = 0.75;

        public PotionAddiction(string name, Type type, TimeSpan delay)
        {
            Name = name;
            Type = type;
            Delay = delay;
        }
    }

    public static class AddictionsHelper
    {
        
        public static void DrinkAndCheck(Mobile from, object potion)
        {
            RacePlayerMobile addict = from as RacePlayerMobile;
            addict.RemoveAddictions();
            /*if (from != null && from is RacePlayerMobile && potion != null && potion is IAddictive && potion is BasePotion)
            {
                RacePlayerMobile rpm = from as RacePlayerMobile;
                BasePotion Potion = potion as BasePotion;
                PotionAddiction Addiction = (potion as IAddictive).GetAddiction();

                if (Utility.RandomDouble() <= (potion as IAddictive).AddictionChance) 
                { // Addict (but say nothing to player :))
                    bool alreadyContains = false;
                    foreach (PotionAddiction pa in rpm.Addictions.Keys)
                    { // if one of them is the current potion
                        if (Potion.GetType().IsSubclassOf(Addiction.Type))
                        {
                            alreadyContains = true;
                            break;
                        }
                    }
                    if(!alreadyContains)
                        rpm.AddAddiction(Addiction);
                }


                if (rpm.IsAddicted)
                {
                    foreach (PotionAddiction pa in rpm.Addictions.Keys)
                    { // if one of them is the current potion
                        if (Potion.GetType().IsSubclassOf(Addiction.Type))
                        {
                            rpm.SendMessage("Vous soulagez votre besoin de potion, vous n'êtes plus en manque.");
                            rpm.IsAddicted = false;
                            break;
                        }
                    }
                }
            }*/
        }

        public static bool TryCure(RacePlayerMobile doctor, RacePlayerMobile patient)
        {
            if (doctor != null && patient != null &&
               !doctor.Squelched && !patient.Squelched &&
               !doctor.Deleted && !patient.Deleted &&
                //doctor.AccessLevel == AccessLevel.Player && patient.AccessLevel == AccessLevel.Player &&
                patient.Addictions.Count > 0)
            {

                double healing = doctor.Skills[SkillName.Healing].Value + (doctor.Skills[SkillName.Poisoning].Value / 3) + (doctor.Skills[SkillName.Alchemy].Value / 2);
                healing /= 150;

                if (doctor.AccessLevel >= AccessLevel.Counselor)
                    healing = 200.0;

                if (Utility.RandomDouble() < healing)
                {
                    // cure succeded
                    doctor.SendMessage("Vous avez réussi à guérir le patient de ses addictions.");
                    patient.SendMessage("Vous n'êtes plus en manque et n'avez plus d'addictions!");
                    patient.RemoveAddictions();
                    return true;
                }
                else
                {
                    doctor.SendMessage("Vous n'avez pas réussi à guérir le patient de ses addictions.");
                    patient.SendMessage("Vous ne vous sentez pas mieux...");
                }
            }
            return false;
        }
    }
}
