using System;
using Server;

namespace Server.Items
{
    public abstract class BaseRefreshPotion : BasePotion, IAddictive
	{
		public abstract double Refresh{ get; }

		public BaseRefreshPotion( PotionEffect effect ) : base( 0xF0B, effect )
		{
		}

		public BaseRefreshPotion( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override void Drink( Mobile from )
		{
			if ( from.Stam < from.StamMax )
			{
				from.Stam += Scale( from, (int)(Refresh * from.StamMax) );

				BasePotion.PlayDrinkEffect( from );
                AddictionsHelper.DrinkAndCheck(from, this);
				this.Delete();
			}
			else
			{
				from.SendMessage( "You decide against drinking this potion, as you are already at full stamina." );
			}
		}

        #region IAddictive Members

        public PotionAddiction GetAddiction()
        {
            return new PotionAddiction("Potion de raifraichissement", typeof(BaseRefreshPotion), TimeSpan.FromDays(1));
        }

        public virtual double AddictionChance
        {
            get { return 0.0; }
        }
        #endregion
	}
}