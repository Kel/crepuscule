using System;
using Server;
using Server.Network;

namespace Server.Items
{
    public abstract class BaseManaRefreshPotion : BasePotion, IAddictive
	{
		public abstract double Refresh{ get; }

		public BaseManaRefreshPotion( PotionEffect effect/*, int amount*/ ) : base( 0xF0B, effect/*, amount*/ )
		{
		}

		public BaseManaRefreshPotion( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override void Drink( Mobile from )
		{
			if ( from.Mana < from.ManaMax )
			{
				if ( from.BeginAction( typeof( BaseManaRefreshPotion ) ) )
				{
					from.Mana += Scale( from, (int)(Refresh * from.ManaMax) );

					BasePotion.PlayDrinkEffect( from );

					this.Amount--;
                    AddictionsHelper.DrinkAndCheck(from, this);
					if (this.Amount <= 0)
						this.Delete();

					Timer.DelayCall( TimeSpan.FromSeconds( 10.0 ), new TimerStateCallback( ReleaseManaLock ), from );
				}
				else
					from.LocalOverheadMessage( MessageType.Regular, 0x22, true, "Vous devez attendre 10 secondes avant d'utiliser une autre potion!" );

			}
			else
				from.SendMessage( "Vous �tes d�j� spirituellement repos�." );
		}

		private static void ReleaseManaLock( object state )
		{
			((Mobile)state).EndAction( typeof( BaseManaRefreshPotion ) );
		}

        #region IAddictive Members

        public PotionAddiction GetAddiction()
        {
            return new PotionAddiction("Potion de mana", typeof(BaseManaRefreshPotion), TimeSpan.FromDays(1));
        }

        public virtual double AddictionChance
        {
            get { return 0.0; }
        }
        #endregion
	}
}
