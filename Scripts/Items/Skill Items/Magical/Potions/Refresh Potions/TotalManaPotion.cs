using System; 
using Server; 

namespace Server.Items 
{ 
   public class TotalManaPotion : BaseManaRefreshPotion
   { 
      //public override double Mana{ get{ return 0.25; } }
        public override double Refresh{ get{ return 0.25; } }
        public override double AddictionChance { get { return 0.02; } }

      [Constructable] 
      public TotalManaPotion() : base( PotionEffect.TotalManaRefresh )
      { 
         Name = "Potion de Mana majeure"; 
         Hue = 0xbaD; 

      } 

      public TotalManaPotion( Serial serial ) : base( serial ) 
      { 
      } 

      public override void Serialize( GenericWriter writer ) 
      { 
         base.Serialize( writer ); 

         writer.Write( (int) 0 ); // version 
      } 

      public override void Deserialize( GenericReader reader ) 
      { 
         base.Deserialize( reader ); 

         int version = reader.ReadInt(); 
      } 
   } 
}
