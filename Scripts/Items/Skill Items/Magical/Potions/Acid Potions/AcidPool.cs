﻿
using System;
using Server.Items;
using Server.Mobiles;

namespace Server.Items {
    public class AcidPool : CrepusculeItem {
        [Constructable]
        public AcidPool()
            : base(0x122A)
        {
            Movable = false;
            Hue = 0x3F;
            Name = "Flaque d'acide";
            Available = true;
            DecaysEnabled = false;
            m_RespawnTimer = new RespawnTimer(this);
            m_RespawnTimer.Start();
        }

        private bool m_Available;
        private double m_Ticks;
        private RespawnTimer m_RespawnTimer;
        private int m_MinDamage = 2;
        private int m_MaxDamage = 5;

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Available
        {
            get { return m_Available; }
            set { m_Available = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MinDamage
        {
            get
            {
                return m_MinDamage;
            }
            set
            {
                if (value < 1)
                    value = 1;

                m_MinDamage = value;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxDamage
        {
            get
            {
                return m_MaxDamage;
            }
            set
            {
                if (value < 1)
                    value = 1;

                if (value < MinDamage)
                    value = MinDamage;

                m_MaxDamage = value;
            }
        }


        [CommandProperty(AccessLevel.GameMaster)]
        public double Ticks
        {
            get { return m_Ticks; }
            set { m_Ticks = value; }
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (from.InRange(this.Location, 2) && from is RacePlayerMobile)
            {
                var owner = from as RacePlayerMobile;
                if (this.Available == true)
                {
                    if (owner.CheckSkill(SkillName.Poisoning, 10, 50))
                    {
                        var itemadd = new WeakAcidPotion();
                        from.AddToBackpack(itemadd);
                        this.ItemID = 1;
                        this.Available = false;

                        from.SendMessage("Vous arrivez à récolter un peu d'acide...");
                        this.m_RespawnTimer.Start();
                    }
                    else
                    {
                        from.SendMessage("Vous n'arrivez pas à récolter l'acide...");
                        this.ItemID = 1;
                        this.Available = false;
                        this.m_RespawnTimer.Start();
                    }
                }
            }

        }

        public AcidPool(Serial serial)
            : base(serial)
        {
        }

        public override bool OnMoveOver(Mobile m)
        {
            if(Available)
                Damage(m);
            return true;
        }

        public void Damage(Mobile m)
        {
            m.Damage(Utility.RandomMinMax(MinDamage, MaxDamage));
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write(m_Available);
            writer.Write(m_Ticks);
            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            m_Available = reader.ReadBool();
            m_Ticks = reader.ReadDouble();
            int version = reader.ReadInt();

            DecaysEnabled = false;
            m_RespawnTimer = new RespawnTimer(this);
            m_RespawnTimer.Start();
        }




        public class RespawnTimer : Timer {
            private AcidPool m_Item;

            // Apres 2.5 secondes, le timer sera declanché tout les secondes. apres 5 ticks, ça s'arretera
            public RespawnTimer(AcidPool from)
                : base(TimeSpan.Zero, TimeSpan.FromHours(1.0))
            {
                m_Item = (AcidPool)from;
                m_Item.Ticks = Utility.Random(2, 7);
                this.Start();
            }

            protected override void OnTick()
            {
                if (m_Item.Ticks <= 0)
                {
                    m_Item.Ticks = Utility.Random(2, 7);
                    if (m_Item.Available == false)
                    {
                        this.Stop();
                        m_Item.ItemID = 0x122A;
                        m_Item.Available = true;
                    }
                }
                else
                {
                    m_Item.Ticks -= 1;
                }

            }
        }

    }
}