﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Items 
{
    public class AcidCitronicPotion : BaseAcidPotion {
        public override int MinDamage { get { return 1; } }
        public override int MaxDamage { get { return 1; } }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidCitronicPotion()
            : base()
        {
            Name = "Une potion d'acide citronique";
            Hue = 0x32;
        }

                #region Serialization

        public AcidCitronicPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class AcidChlorePotion : BaseAcidPotion 
    {
        public override int MinDamage { get {return 1;} }
        public override int MaxDamage { get {return 2;} }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidChlorePotion() : base()
        {
            Name = "Une potion d'acide sulfurique";
            Hue = 0x37;
        }

                #region Serialization

        public AcidChlorePotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class AcidVaranitPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 1; } }
        public override int MaxDamage { get { return 3; } }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidVaranitPotion()
            : base()
        {
            Name = "Une potion d'acide de varanite";
            Hue = 0x2F3;
        }

                #region Serialization

        public AcidVaranitPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class AcidAllenicPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 1; } }
        public override int MaxDamage { get { return 4; } }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidAllenicPotion() : base()
        {
            Name = "Une potion d'acide allénique";
            Hue = 0xC;
        }

                #region Serialization

        public AcidAllenicPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class AcidCitixicPotion : BaseAcidPotion {
        public override int MinDamage { get { return 1; } }
        public override int MaxDamage { get { return 5; } }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidCitixicPotion() : base()
        {
            Name = "Une potion d'acide citixique";
            Hue = 0x9F;
        }

                #region Serialization

        public AcidCitixicPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class AcidFluorPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 1; } }
        public override int MaxDamage { get { return 6; } }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidFluorPotion()
            : base()
        {
            Name = "Une potion d'acide fluorhydrique";
            Hue = 0x3DB;
        }

                #region Serialization

        public AcidFluorPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class AcidVitrolPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 3; } }
        public override int MaxDamage { get { return 7; } }
        public override int Pool { get { return 1; } }

        [Constructable]
        public AcidVitrolPotion()
            : base()
        {
            Name = "Une potion d'acide de vitrol";
            Hue = 0x131;
        }

                #region Serialization

        public AcidVitrolPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }
}
