﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Items 
{
    public class GreaterAcidCitronicPotion : BaseAcidPotion {
        public override int MinDamage { get { return 1; } }
        public override int MaxDamage { get { return 1; } }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidCitronicPotion()
            : base()
        {
            Name = "Une potion d'acide citronique supérieure";
            Hue = 0x32;
        }

        #region Serialization

        public GreaterAcidCitronicPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class GreaterAcidChlorePotion : BaseAcidPotion 
    {
        public override int MinDamage { get {return 2;} }
        public override int MaxDamage { get {return 4;} }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidChlorePotion() : base()
        {
            Name = "Une potion d'acide sulfurique supérieure";
            Hue = 0x37;
        }

                #region Serialization

        public GreaterAcidChlorePotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class GreaterAcidVaranitPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 2; } }
        public override int MaxDamage { get { return 6; } }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidVaranitPotion()
            : base()
        {
            Name = "Une potion d'acide de varanite supérieure";
            Hue = 0x2F3;
        }
                #region Serialization

        public GreaterAcidVaranitPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class GreaterAcidAllenicPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 2; } }
        public override int MaxDamage { get { return 7; } }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidAllenicPotion()
            : base()
        {
            Name = "Une potion d'acide allénique supérieure";
            Hue = 0xC;
        }
                #region Serialization

        public GreaterAcidAllenicPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class GreaterAcidCitixicPotion : BaseAcidPotion {
        public override int MinDamage { get { return 2; } }
        public override int MaxDamage { get { return 8; } }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidCitixicPotion() : base()
        {
            Name = "Une potion d'acide citixique supérieure";
            Hue = 0x9F;
        }
                #region Serialization

        public GreaterAcidCitixicPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class GreaterAcidFluorPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 3; } }
        public override int MaxDamage { get { return 9; } }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidFluorPotion()
            : base()
        {
            Name = "Une potion d'acide fluorhydrique supérieure";
            Hue = 0x3DB;
        }
                #region Serialization

        public GreaterAcidFluorPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }

    public class GreaterAcidVitrolPotion : BaseAcidPotion 
    {
        public override int MinDamage { get { return 5; } }
        public override int MaxDamage { get { return 10; } }
        public override int Pool { get { return 3; } }

        [Constructable]
        public GreaterAcidVitrolPotion()
            : base()
        {
            Name = "Une potion d'acide de vitrol supérieure";
            Hue = 0x131;
        }
                #region Serialization

        public GreaterAcidVitrolPotion(Serial serial) : base(serial) { }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
        #endregion
    }
}
