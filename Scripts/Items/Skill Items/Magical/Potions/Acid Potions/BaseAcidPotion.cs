using System;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Items
{

    public abstract class BaseAcidPotion : Item
	{
		public abstract int MinDamage { get ;}
        public abstract int MaxDamage { get; }
        public virtual int Pool { get { return 1; } }
		
		[Constructable]
		public BaseAcidPotion() : base(0xF0A)
		{
			Name = "Une potion d'acide";
			Hue = 0x89;
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !Movable )
				return;

			if ( from.InRange( this.GetWorldLocation(), 1 ) )
			{
				if ( BasePotion.HasFreeHand( from ) )
					Launch( from );
				else
					
					from.SendLocalizedMessage( 502172 ); // You must have a free hand to drink a potion.
			}
			else
			{
				from.SendLocalizedMessage( 502138 ); // That is too far away for you to use
			}
		}
		
		private void Launch( Mobile from )
		{
			if ( Core.AOS && (from.Paralyzed || from.Frozen || (from.Spell != null && from.Spell.IsCasting)) )
			{
				
				from.SendLocalizedMessage( 1062725 ); // You can not use a purple potion while paralyzed.
				return;
			}

			ThrowTarget targ = from.Target as ThrowTarget;

			if ( targ != null && targ.Potion == this )
				return;

			from.RevealingAction();
			from.Target = new ThrowTarget( this );
		}
		
		private class ThrowTarget : Target
		{
			public BaseAcidPotion m_Potion;
			public BaseAcidPotion Potion
			{
				get{ return m_Potion; }
			}

			public ThrowTarget( BaseAcidPotion potion ) : base( 6, true, TargetFlags.None )
			{
				m_Potion = potion;
			}

			protected override void OnTarget( Mobile from, object targeted )
			{
				if ( m_Potion.Deleted || m_Potion.Map == Map.Internal )
					return;

				IPoint3D p = targeted as IPoint3D;

				if ( p == null )
					return;

				Map map = from.Map;

				if ( map == null )
					return;

				from.RevealingAction();
				m_Potion.MoveToWorld( new Point3D ( p ), map);
				Timer.DelayCall( TimeSpan.FromMilliseconds( 200.0 ), new TimerCallback( m_Potion.SpillAcid_OnTick ));
			}
		}
		
		public void SpillAcid_OnTick()
		{
			IPoint3D p = Location;
			
			 for ( int x = p.X - Pool; x <= p.X ; ++x )
				{
					for ( int y = p.Y - Pool; y <= p.Y ; ++y )
					{
						switch ( Utility.Random( 2 ))
						{
						case 0: PoolOfAcid acid = new PoolOfAcid( TimeSpan.FromSeconds(10), MinDamage , MaxDamage );
						acid.MoveToWorld( new Point3D( x, y, p.Z ),Map);
						break;			
						case 1: 	
						break;
						}
					}
				}
			this.Delete();

		}
		
		public BaseAcidPotion( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

}

