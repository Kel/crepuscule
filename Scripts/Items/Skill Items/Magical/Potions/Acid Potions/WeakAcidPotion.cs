using System;
using Server;

namespace Server.Items
{
	public class WeakAcidPotion : BasePoisonPotion
	{
		public override Poison Poison{ get{ return Poison.Lesser; } }

		public override double MinPoisoningSkill{ get{ return 90.0; } }
		public override double MaxPoisoningSkill{ get{ return 120.0; } }

		[Constructable]
		public WeakAcidPotion() : base( PotionEffect.PoisonLesser )
		{
            Name = "Potion d'acide faible";
		}

        public WeakAcidPotion(Serial serial)
            : base(serial)
		{
            Name = "Potion d'acide faible";
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
            Name = "Potion d'acide faible";
			int version = reader.ReadInt();
		}
	}
}