using System;
using Server;

namespace Server.Items
{
	public class HugeStrengthPotion : BaseStrengthPotion
	{
		public override int StrOffset{ get{ return 40; } }
		public override TimeSpan Duration{ get{ return TimeSpan.FromMinutes( 4.0 ); } }
        public override double AddictionChance { get { return .10; } }

		[Constructable]
        public HugeStrengthPotion()
            : base(PotionEffect.StrengthGreater)
		{
            Name = "Potion de force de Bastion Berguenois";
		}

        public HugeStrengthPotion(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
