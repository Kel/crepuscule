using System;
using Server;

namespace Server.Items
{
	public class HugeAgilityPotion : BaseAgilityPotion
	{
		public override int DexOffset{ get{ return 40; } }
		public override TimeSpan Duration{ get{ return TimeSpan.FromMinutes( 4.0 ); } }
        public override double AddictionChance { get { return .10; } }

		[Constructable]
		public HugeAgilityPotion() : base( PotionEffect.AgilityGreater )
		{
            Name = "Potion d'agilit� de T'Sen";
		}

        public HugeAgilityPotion(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
