using System;
using Server.Network;
using Server;
namespace Server.Items
{
    public class HugeStealthPotion : BaseStealthPotion
    {
        [Constructable]
        public HugeStealthPotion() : base("Potion d'infiltration des Ombres", PotionEffect.SuperStealth) { }
        public HugeStealthPotion(Serial serial) : base(serial) { }

        public override int Steps { get { return 35; } }
        public override double AddictionChance { get { return 0.10; } }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }


    }
}
