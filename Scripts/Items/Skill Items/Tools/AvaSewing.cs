using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0xF9D, 0xF9D )]
	public class AvaSewing : BaseTool
	{
		[Constructable]
		public AvaSewing() : base( 0xF9D )
		{
			Weight = 2.0;
			Hue = 0x332;
			Name= "Trousse de couture avanc�e";
		}

		[Constructable]
		public AvaSewing( int uses ) : base( uses, 0xF9D )
		{
			Weight = 2.0;
			Hue = 0x332;
			Name= "Trousse de couture avanc�e";
		}

		public AvaSewing( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}