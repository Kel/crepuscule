using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0xF71, 0xF74 )]
	public class Chevalet : BaseTool
	{

		[Constructable]
		public Chevalet() : base( 0xF71 )
		{
			Weight = 25.0;
		}

		[Constructable]
		public Chevalet( int uses ) : base( uses, 0xF71 )
		{
			Weight = 25.0;
		}

		public Chevalet( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}