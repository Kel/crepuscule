using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0x13E4, 0x13E3 )]
	public class RunicHammer : BaseRunicTool
	{

		public override void AddNameProperties( ObjectPropertyList list )
		{
			base.AddNameProperties( list );

            if (CraftResourceType != null && !CraftResourceType.Craft.IsStandard)
            {
                var attrs = GetResourceAttributes();
                if (attrs != null)
                {
                    list.Add(attrs.LabelName);
                }
            }
		}

		[Constructable]
		public RunicHammer( Type resource ) : base( resource, 0x13E4 )
		{
			Weight = 8.0;
			Layer = Layer.OneHanded;
            Name = "Marteau Runique";
		}

		[Constructable]
		public RunicHammer( Type resource, int uses ) : base( resource, uses, 0x13E4 )
		{
			Weight = 8.0;
			Layer = Layer.OneHanded;
            Name = "Marteau Runique";
		}

		public RunicHammer( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}