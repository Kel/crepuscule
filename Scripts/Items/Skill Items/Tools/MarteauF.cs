using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0xfb4, 0xfb4 )]
	public class MarteauF : BaseTool
	{

		[Constructable]
		public MarteauF() : base( 0xFB4 )
		{
			Weight = 2.0;
		}

		[Constructable]
		public MarteauF( int uses ) : base( uses, 0xFB4 )
		{
			Weight = 2.0;
		}

		public MarteauF( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}