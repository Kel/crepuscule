using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	public class MortarPestle : BaseTool
	{
		[Constructable]
		public MortarPestle() : base( 0xE9B )
		{
			Weight = 1.0;
		}

		[Constructable]
		public MortarPestle( int uses ) : base( uses, 0xE9B )
		{
			Weight = 1.0;
		}

		public MortarPestle( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

    public class MortarPestlePoisonning : BaseTool 
    {

        [Constructable]
        public MortarPestlePoisonning()
            : base(0xE9B)
        {
            Weight = 1.0;
            Name = "Outils de cr�ation de potions dangereuses";
        }

        [Constructable]
        public MortarPestlePoisonning(int uses)
            : base(uses, 0xE9B)
        {
            Weight = 1.0;
            Name = "Outils de cr�ation de potions dangereuses";
        }

        public MortarPestlePoisonning(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            Name = "Outils de cr�ation de potions dangereuses";
            int version = reader.ReadInt();
        }
    }
}