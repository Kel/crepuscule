using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class BagOfNecroReagents : Bag
	{
		[Constructable]
		public BagOfNecroReagents() : this( 50 )
		{
		}

		[Constructable]
		public BagOfNecroReagents( int amount )
		{
            this.Movable = true;
            this.Hue = 2277;
            this.Name = "Sac de réactifs de nécromancien";

            DropReagents(amount);
		}

        public void DropReagents(int amount)
        {
            DropOrStackItem(new BatWing(amount));
            DropOrStackItem(new GraveDust(amount));
            DropOrStackItem(new DaemonBlood(amount));
            DropOrStackItem(new NoxCrystal(amount));
            DropOrStackItem(new PigIron(amount));
        }

		public BagOfNecroReagents( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}