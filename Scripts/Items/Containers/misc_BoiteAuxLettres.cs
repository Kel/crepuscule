using System;
using Server;
using Server.Gumps;
using Server.Targeting;
using Server.Mobiles;
using Server.Prompts;
using Server.Engines;
using System.Collections;

namespace Server.Items
{
  [Flipable( 0x1ED4, 0x1ED7 )]
  public class BoiteAuLettreComponent : AddonComponent
  {
    private bool m_Commerce = false,
                 owned = false,
                 m_ShowOwnerName = false;
    private Mobile owner = null;
    private BaseDoor m_Doors1 = null,
                     m_Doors2 = null;
    private string m_OwnerName = "A vendre",
                   m_District;
    private int m_Number,
                m_SellPrice = 1000;

    private Mobile[] m_CoProprio = new Mobile[]
                        {
                          null,
                          null,
                          null
                        };

    [CommandProperty(AccessLevel.GameMaster)]
    public Mobile[] CoProprio
    {
      get { return m_CoProprio; }
      set { m_CoProprio = value; }
    }

    [CommandProperty(AccessLevel.GameMaster)]
    public string NomProprio
    {
      get { return m_OwnerName != null ? m_OwnerName.Replace("\"","") : ""; }
      set { if (m_OwnerName != value && m_District != "X") DistrictLog.ChangeOwnerName(ComputeId(), m_OwnerName, value); m_OwnerName = value; }
    }

    [CommandProperty(AccessLevel.GameMaster)]
    public BaseDoor Porte1
    {
      get { return m_Doors1; }
      set { m_Doors1 = value; }
    }
    
    [CommandProperty(AccessLevel.GameMaster)]
    public BaseDoor Porte2
    {
      get { return m_Doors2; }
      set { m_Doors2 = value; }
    }

    [CommandProperty(AccessLevel.GameMaster)]
    public int PrixVente
    {
      get { return m_SellPrice; }
      set { if (value != m_SellPrice && m_District != "X") DistrictLog.ChangePrice(ComputeId(), m_SellPrice, value); m_SellPrice = value; }
    }
    
    [CommandProperty(AccessLevel.GameMaster)]
    public bool MontrerProprio
    {
      get { return m_ShowOwnerName; }
      set { m_ShowOwnerName = value; }
    }
    
    [CommandProperty( AccessLevel.GameMaster )]
    public Mobile Proprio
    {
      get { return owner; }
      set { if (owner != value && m_District != "X") DistrictLog.ChangeOwner(ComputeId(), owner, value); owner = value; if (owner == null) NomProprio = "A vendre"; }
    }
    
    [CommandProperty( AccessLevel.GameMaster )]
    public bool IsCommerce
    {
      get { return m_Commerce; }
      set { m_Commerce = value; if (m_District !="X") DistrictLog.ChangeCommerce(ComputeId(), m_Commerce); }
    }
    
// ----------------------------------------------------------------------------   
// D�but gestion du quartier
// ---------------------------------------------------------------------------- 
// La d�limitation des quartier est fixe. L'attribut quartier n'est donc qu'en
// lecture et est g�r� automatiquement selon la position du panneau.    
    public string GetDistrictId()
    {
      return m_District;
    }
    
    private void SetDistrict()
    {
      District district = District.Find(this.Location);
      if (district != null)
        m_District = district.GetId();
      else
        m_District = "X";
    }  
// ---------------------------------------------------------------------------- 
// Fin gestion du quartier
// ---------------------------------------------------------------------------- 
// Le num�ro de maison est dynamique. Pour �viter que plusieurs maisons aient
// le m�me num�ro, l'attribution du num�ro est g�r�e auomatiquement selon le
// quartier et n'est donc qu'en lecture.
    public int GetNumber()
    {
      return m_Number;
    }
    
    private void SetNumber()
    {
      ArrayList list = new ArrayList();
      int max = 0;
      BoiteAuLettreComponent temp;
      foreach (Item house in World.Items.Values)
      {
        if (house is BoiteAuLettreComponent)
        {
          temp = (BoiteAuLettreComponent) house;
          if (temp.GetDistrictId() == this.GetDistrictId() && temp != this)
          {
          list.Add(temp);
          if (temp.GetNumber() > max)
            max = temp.GetNumber();
          }
        }
      }
      if (max > list.Count)
      {
        int[] array = new int[max];
        for (int i = 0; i < list.Count; ++i)
            array[((BoiteAuLettreComponent)list[i]).GetNumber() - 1] = 1;
        for (int i = 0; i < max; ++i)
        {
          if (array[i] == 0)
          {
            m_Number = i + 1;
            break;
          }
        }
      }
      else
        m_Number = max + 1;
    }
// ---------------------------------------------------------------------------- 
// Fin gestion du num�ro
// ----------------------------------------------------------------------------
    public override void OnLocationChange(Point3D oldLocation)
    {
      string olddistrict = m_District;
      string oldid = ComputeId();
      SetDistrict();
      if (olddistrict != m_District)
      {
        SetNumber();
        SetName();
        if (m_District != "X")
            DistrictLog.MoveHouse(oldid, ComputeId());
      }
    }
    
    public override void OnDelete()
    {
        if (m_District != "X")
        {
            DistrictLog.RemoveHouse(ComputeId());
            District.RemoveHouse(this);
        }
    }

    public BoiteAuLettreComponent(int dir) : base(0x1ED4)
    {
      if (dir == 1)
          ItemID = 0x1ED7;
          
      SetDistrict();
      SetNumber();
      SetName();
      Proprio = null;
      if (m_District != "X")
      {
          DistrictLog.CreateHouse(ComputeId());
          District.AddHouse(this);
      }
    }
 
    private void SetName()
    {
      Name = m_District != "X" ? "B�timent " + ComputeId() : "B�timent";
    }
    
    public string ComputeId()
    {
      return GetDistrictId() + "-" + GetNumber();
    }
 
    public BoiteAuLettreComponent( Serial serial ) : base( serial )
    {   
       Proprio = null;
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );

      writer.Write( (int) 3 ); // version
      for (int i = 0; i < CoProprio.Length; i++)
        writer.Write((Mobile)CoProprio[i]);
      writer.Write((string)m_OwnerName);
      writer.Write((int)m_SellPrice);
      writer.Write((BaseDoor)m_Doors2);
      writer.Write((BaseDoor)m_Doors1);
      writer.Write((Mobile) owner);
      writer.Write((bool) owned);
      writer.Write((string) m_District);
      writer.Write((int) m_Number);
      writer.Write((bool) m_Commerce);
      writer.Write((bool) m_ShowOwnerName);
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );

      int version = reader.ReadInt();
      for (int i = 0; i < CoProprio.Length; i++)
        CoProprio[i] = reader.ReadMobile();
      if (version == 0)
      {
        reader.ReadInt();
        reader.ReadInt();
        reader.ReadMobile();
      }
      m_OwnerName = reader.ReadString();
      m_SellPrice = reader.ReadInt();
      if (version == 0)
        reader.ReadInt();
      if (version < 2)
        reader.ReadInt();
      if (version == 0)
        reader.ReadString();
      m_Doors2 = (BaseDoor)reader.ReadItem();
      m_Doors1 = (BaseDoor)reader.ReadItem();
      owner = reader.ReadMobile();
      owned = reader.ReadBool();
      if (version < 2)
      {
        for(int i=0;i<3;i++)
          reader.ReadMobile();
      }
      if (version == 0)
      {
        reader.ReadInt();
        SetDistrict();
        SetNumber();
        m_Commerce = false;
      }
      else if (version < 3)
      {
        reader.ReadInt();
        SetDistrict();
        reader.ReadInt();
        SetNumber();
      }
      else
      {
        string olddistrict = reader.ReadString();
        SetDistrict();
        if (olddistrict != m_District && m_District != "X")
        {
          SetNumber();
          DistrictLog.MoveHouse(string.Format("{0}-{1}",olddistrict,reader.ReadInt()), ComputeId());
        }
        else
          m_Number = reader.ReadInt();
      }
      
      SetName();
      
      if (version > 0)
        m_Commerce = reader.ReadBool();
      
      
      if (version < 2)
        m_ShowOwnerName = false;
      else
        m_ShowOwnerName = reader.ReadBool();
      
      if (owned && owner == null)
      {
          if (m_District != "X")
            DistrictLog.ChangeOwner(ComputeId(), null, null);
        owned = false;
      }
        if (m_District != "X")
            District.AddHouse(this);
    }

    public void Acheter(Mobile from)
    {
      if (from is RacePlayerMobile)
      {
        RacePlayerMobile rpm = (RacePlayerMobile)from;
        if (!Banker.Withdraw(from, this.PrixVente))
          from.SendMessage("Vous n'avez pas assez d'argent sur votre compte.");
        else
        {
          from.SendMessage("Vous avez achet� le b�timent.");
          Proprio = from;
          owned = true;
          if (m_District != "X")
          {
              from.SendMessage("Sous quel nom voulez-vous l'enregistrer ?");
              from.Prompt = new OwnerNamePrompt(this);
          }
        }
      }
    }

    public override void OnDoubleClick(Mobile from)
    {
      from.CloseGump(typeof(InternalGump));
      from.SendGump( new InternalGump( from,this ) );
    }
    
    public void AddCoproprio(Mobile target, Mobile from)
    {
      for (int i = 0; i < CoProprio.Length; i++)
      {
        if (CoProprio[i] == null)
        {
          CoProprio[i] = (Mobile)target;
          return;
        }
      }
    }
  }
  
  public class OwnerNamePrompt : Prompt
  {
    private BoiteAuLettreComponent m_Boite;

    public OwnerNamePrompt( BoiteAuLettreComponent boite )
    {
      m_Boite = boite;
    }

    public override void OnCancel(Mobile from)
    {
      from.SendMessage("Le nom du propri�taire n'a pas �t� chang�.");
    }

    public override void OnResponse( Mobile from, string text )
    {
      string oldowner = m_Boite.NomProprio;   
      m_Boite.NomProprio = Utility.FixHtml( text );
    }
  }

  ////////////////////////////
  // Gump Boite aux lettres //  
  ////////////////////////////
  public class InternalGump : Gump
  {
    Mobile m_from;
    BoiteAuLettreComponent m_item;
 
    public void AddButtonLabeled( int x, int y, int buttonID, string text )
    {
      AddButton( x, y - 1, 4005, 4007, buttonID, GumpButtonType.Reply, 0 );
      AddHtml( x + 35, y, 240, 20,""+text+"",false, false );
    }
    public void AddButtonLabeledDel( int x, int y, int buttonID, string text )
    {
      AddButton( x, y - 1, 4017, 4019, buttonID, GumpButtonType.Reply, 0 );
      AddHtml( x + 35, y, 240, 20,""+text+"",false, false );
    }
 
    public InternalGump(Mobile from, BoiteAuLettreComponent item) : base(100,100)
    {
      m_from = from;
      m_item = item;
      
      int upmargin = 90, num = 0; 
      
      RacePlayerMobile rpm = (RacePlayerMobile)from;

      AddBackground(55, 60, 260, 320, 9200);
      AddLabel( 140, 70, 0x26, "Gestion de la maison" );
      
      AddLabel( 80, upmargin + num * 20, 0x00, string.Format("Prix : {0}", item.PrixVente)); num++;

      if (m_item.Proprio == null)
      {
        AddButtonLabeled(80, upmargin + num * 20, 1, "Acheter la maison"); num++;
      }
      else if (item.MontrerProprio && item.GetDistrictId() != "X")
      {
        AddLabel( 80, upmargin + num * 20, 0x00, string.Format("Propri�taire : {0}", item.NomProprio)); num++;
        if (m_from == item.Proprio)
        {
          AddButtonLabeled(80, upmargin + num * 20, 2, "Cacher votre nom"); num++;
        }
      }
      else if (item.GetDistrictId() != "X")
      {
        AddLabel( 80, upmargin + num * 20, 0x00, "Propri�taire : Anonyme"); num++;
        if (m_from == item.Proprio)
        {
          AddButtonLabeled(80, upmargin + num * 20, 2, "Montrer votre nom"); num++;
        }
      }
      
      if (m_item.Proprio == from && item.GetDistrictId() != "X")
      {
        AddButtonLabeled(80, upmargin + num * 20, 3, "Changer votre nom"); num++;
      }
        
      num++;
        
      if (m_from == m_item.Proprio || m_from.AccessLevel > AccessLevel.Player || m_from == m_item.CoProprio[0] || m_from == m_item.CoProprio[1] || m_from == m_item.CoProprio[2])
      {
        AddButtonLabeled(80, upmargin + num * 20,4, "Ouvrir les portes"); num++;

        if (m_item.Proprio == m_from)
        {
          AddButtonLabeled(80, upmargin + num * 20, 5, "Ajouter Coproprietaire"); num++;
        }
        
        num++;
        
        for (int i = 0; i < 3; ++i)
        {
          AddImageTiled(80, upmargin + num * 20, 182, 23, 0x52);
          AddImageTiled(81, upmargin + num * 20 + 1, 180, 21, 0xBBC);
          if (m_item.CoProprio[i] != null)
          {
              AddLabelCropped(91, upmargin + num * 20 + 1, 180, 21, 0, m_item.CoProprio[i].GetKnownName(rpm)); // rpm.FindName(m_item.CoProprio[i].Serial.Value));
            if (m_item.Proprio == m_from)
              AddButtonLabeledDel(270, upmargin + num * 20, 6+i, "");
          }
          num++;
        }
                    
        num++;
        if (m_item.Proprio == m_from)
          AddButtonLabeled(80, upmargin + num * 20, 9, "Transferer la propri�t�");
      }
    }
    
    public override void OnResponse( Server.Network.NetState sender, RelayInfo info )
    {
      int val = info.ButtonID;
 
      switch(val)
      {
        default: break;
        case 1:
        {
          m_item.Acheter(m_from);
          break;
        }
        case 2:
        {
          m_item.MontrerProprio = !m_item.MontrerProprio;
          break;
        }
        case 3:
        {
          m_from.SendMessage("Sous quel nom voulez-vous enregistrer le b�timent ?");
          m_from.Prompt = new OwnerNamePrompt( m_item );
          break;
        }
        case 4:
        {
          try
          {
            if (m_item.Porte1 != null)
            {
              m_item.Porte1.Open = !m_item.Porte1.Open;
              m_from.SendMessage("Vous ouvrez les portes");
              if (m_item.Porte2 != null)
              {
                m_item.Porte2.Open = !m_item.Porte2.Open;
              }
            }
          }
          catch { }
          break;
        }
        case 5:
        {
          m_from.Target = new AddTargetCoproprio(m_item);
          m_from.SendMessage("Qui desirez vous ajouter comme coproprietaire?");
          break;
        }
        case 6:
        {
          m_item.CoProprio[0] = null;
          break;
        }
        case 7:
        {
          m_item.CoProprio[1] = null;
          break;
        }
        case 8:
        {
          m_item.CoProprio[2] = null;
          break;
        }
        case 9:
        {
          m_from.Target = new AddTargetChangeProprio(m_item);
          m_from.SendMessage("A qui voulez vous transferer la maison?");
          break;
        }
      }
    }
  }
  
  public class AddTargetCoproprio : Target
  {
    BoiteAuLettreComponent m_item;

    public AddTargetCoproprio(BoiteAuLettreComponent item) : base(3, false, TargetFlags.None)
    {
      m_item = item;
    }

    protected override void OnTarget(Mobile from, object targ)
    {
      if (targ is PlayerMobile)
      {
        Mobile target = (Mobile)targ;
        m_item.AddCoproprio(target, from);
      }
    }
  }

  public class AddTargetChangeProprio : Target
  {
    BoiteAuLettreComponent m_item;

    public AddTargetChangeProprio(BoiteAuLettreComponent item) : base(3, false, TargetFlags.None)
    {
      m_item = item;
    }

    protected override void OnTarget(Mobile from, object targ)
    {
      if (targ is PlayerMobile)
      {
        Mobile target = (Mobile)targ;
        m_item.Proprio = target;
        if (m_item.GetDistrictId() != "X")
            target.Prompt = new OwnerNamePrompt( m_item );
      }
    }
  }

  public class BoiteAuLettreAddon : BaseAddon
  {
    public int m_dir;
 
    [Constructable]
    public BoiteAuLettreAddon(int dir)
    {
      AddComponent( new BoiteAuLettreComponent(dir), 0, 0, 0 );
      Name = "Bo�te aux lettres";
    }

    public BoiteAuLettreAddon( Serial serial ) : base( serial )
    {
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );

      writer.Write( (int) 0 ); // version
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );

      int version = reader.ReadInt();
    }
  }

  public class GestionMaisonSudDeed : BaseAddonDeed
  {
   
    public override BaseAddon Addon{ get{ return new BoiteAuLettreAddon(0); } }

    [Constructable]
    public GestionMaisonSudDeed()
    {
      Name = "Bo�te aux lettres Sud";
    }

    public GestionMaisonSudDeed( Serial serial ) : base( serial )
    {
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );

      writer.Write( (int) 0 ); // version
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );

      int version = reader.ReadInt();
    }
  }

  public class GestionMaisonEstDeed : BaseAddonDeed
  {
    public override BaseAddon Addon{ get{ return new BoiteAuLettreAddon(1); } }

    [Constructable]
    public GestionMaisonEstDeed()
    {
      Name = "Bo�te aux lettres Est";
    }

    public GestionMaisonEstDeed( Serial serial ) : base( serial )
    {
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );

      writer.Write( (int) 0 ); // version
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );

      int version = reader.ReadInt();
    }
  }

  public class PapierALettre : BaseBook
  {
    Mobile sender;
 
    [CommandProperty( AccessLevel.GameMaster )]
    public Mobile Auteur
    {
      get { return sender; }
      set { sender = value; InvalidateProperties(); }
    }
 
    [Constructable]
    public PapierALettre() : base(0x14ED)
    {
      Name = "Papier a lettre";
    }
 
    public PapierALettre( Serial serial ) : base( serial )
    {
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );

      int version = reader.ReadInt();
      sender = reader.ReadMobile();
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );

      writer.Write( (int)0 ); // version
      writer.Write( (Mobile) sender);
    }
  }
}