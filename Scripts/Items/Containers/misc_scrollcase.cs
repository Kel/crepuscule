/*
* Created by SharpDevelop.
* User: Hughlander
* Date: 04/10/2004
* Time: 05:46
*
* To change this template use Tools | Options | Coding | Edit Standard Headers.
*/

using System;
using System.Collections;
using Server;
using Server.Targeting;
using Server.Network;
using Server.Spells;
using Server.Scripts.Commands;
using Server.Gumps;

namespace Server.Items
{
	public class ScrollCase: CrepusculeItem
	{
		private int[] m_Scrolls = new int[64];

		[Constructable]
		public ScrollCase() : base( 0xEFA )
		{
			Hue = 0x7A0;
			Name = "classeur de parchemin";
		}

		[Constructable]
		public ScrollCase( Serial serial ) : base( serial )
		{
		}

		public override bool OnDragDrop( Mobile from, Item dropped )
		{
			if ( dropped is SpellScroll )
			{
				SpellScroll scroll = (SpellScroll)dropped;

				int spellID = scroll.SpellID;
				if ( spellID >= 0 && spellID < 64 )
				{
					if ( m_Scrolls[spellID] <= 10 )
					{
						if ( m_Scrolls[spellID] + scroll.Amount > 10 )
						{
							scroll.Consume( 10 - m_Scrolls[spellID] );
							m_Scrolls[spellID] = 10;
						}
						else
						{
							m_Scrolls[spellID] += scroll.Amount;
							scroll.Delete();
						}

						InvalidateProperties();

						from.Send( new PlaySound( 0x249, GetWorldLocation() ) );
					}
					else
					{
						from.SendMessage( "Vous ne pouvez mettre plus de 10 parchemins du m�me type." );
					}
				}
				else
				{
					from.SendMessage( "Vous ne pouvez ranger ce type de parchemin." );
				}
			}
			return false;
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.PlaySound( 85 );
			from.SendGump( new ScrollCaseGump( from, ref m_Scrolls ) );
		}

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

			int tot = 0;
			for ( int i = 0; i < 64; i++)
				tot += m_Scrolls[i];

			list.Add( 1070722, string.Format( "{0} parchemin{1}", tot, ( tot > 1) ? "s" : "" ) );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
			for ( int i = 0; i < m_Scrolls.Length; ++i )
			{
				writer.Write( (int) m_Scrolls[i] );
			}
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
			for ( int i = 0; i < m_Scrolls.Length; ++i )
			{
				m_Scrolls[i] = reader.ReadInt();
			}
		}

		public class ScrollCaseGump : Gump
		{
			private Mobile m_Owner;
			public Mobile Owner{ get{ return m_Owner; } set{ m_Owner = value; } }
			private int[] m_Scrolls;

			public ScrollCaseGump( Mobile owner, ref int[] m_Scrolls ) : base( 10, 10 )
			{
				this.m_Scrolls = m_Scrolls;
				owner.CloseGump( typeof( ScrollCaseGump ) );

				int gumpX = 0; int gumpY = 0; 
				//bool initialState = false;

				m_Owner = owner;

				Closable = true;
				Disposable = true;
				Dragable = true;
				Resizable = true;

				AddPage( 0 );

				AddImage( 10, 10, 0x89B );

				for( int i = 0; i < 8; ++i )
				{
					int ButID = ((i * 5) / 10) + 2;
					AddButton( 40 + (35*i) + (30*(i/4)), 185, 0x8B1 + i, 0x8B1 + i, 0, GumpButtonType.Page, ButID );
				}
				AddPage( 1 );

				AddImage( 85, 90, 0x71 );

				AddLabel( 42, 40, 497, "Classeur de parchemin" );

				AddHtml( 65, 152, 92, 19, "Hughlander (C)", false, false );

				// next
				AddButton( 304, 14, 0x89E, 0x89E, 0, GumpButtonType.Page, 2 );

				// previous
				//AddButton( 33, 15, 0x89D, 0x89D, 0, GumpButtonType.Page, 3 );

				for( int i = 0; i < 8; ++i )
				{
					int ButID = ((i * 5) / 10) + 2;
					AddLabel( 230, 40 + i*17, 0, (i+1) + (i == 0 ? "er" : "eme") + " cercle" );
					AddButton( 214, 42 + i*17, 0x4B9, 0x4BA, 0, GumpButtonType.Page, ButID );
				}

				int tot = 0;
				for ( int i = 0; i < 64; i++)
					tot += m_Scrolls[i];

				AddLabel( 65, 59, 0, string.Format( "{0} parchemin{1}", tot, ( tot < 1) ? "s" : "" ) );

				for ( int i = 0; i < 4; i++ )
				{
					AddPage( 2 + i );

					if ( 2 + i + 1 < 6 )
					{
						// next page
						AddButton( 304, 14, 0x89E, 0x89E, 0, GumpButtonType.Page, 2 + i + 1 );
					}

					// previous page
					AddButton( 33, 15, 0x89D, 0x89D, 0, GumpButtonType.Page, 2 + i - 1 );

					int s, s2;
					int gumpX2 = 230;
					gumpX = 65;
					gumpY = 40 - 17;

					for ( int j = 0; j < 8; ++j )
					{
						gumpY += 17;
     
						s = 16 * i + j;
						s2 = 16 * i + 8 + j;
						AddLabel( gumpX, gumpY, 0, string.Format( "{0} {1}", m_Scrolls[s], SpellRegistry.NewSpell( s, owner, null ).Name ) );
						AddButton( gumpX - 16, gumpY + 2, 0x4B9, 0x4BA, s + 1, GumpButtonType.Reply, 2 );

						AddLabel( gumpX2, gumpY, 0, string.Format( "{0} {1}", m_Scrolls[s2], SpellRegistry.NewSpell( s2, owner, null ).Name ) );
						AddButton( gumpX2 - 16, gumpY + 2, 0x4B9, 0x4BA, s2 + 1, GumpButtonType.Reply, 2 );
					}
				}
			}

			SpellScroll[] scrollTable = new SpellScroll[]
  {
	  new ClumsyScroll(), new CreateFoodScroll(), new FeeblemindScroll(), new HealScroll(), new MagicArrowScroll(), new NightSightScroll(), new ReactiveArmorScroll(), new WeakenScroll(),
	  new AgilityScroll(), new CunningScroll(), new CureScroll(), new HarmScroll(), new MagicTrapScroll(), new MagicUnTrapScroll(), new ProtectionScroll(), new StrengthScroll(),
	  new BlessScroll(), new FireballScroll(), new MagicLockScroll(), new PoisonScroll(), new TelekinisisScroll(), new TeleportScroll(), new UnlockScroll(), new WallOfStoneScroll(),
	  new ArchCureScroll(), new ArchProtectionScroll(), new CurseScroll(), new FireFieldScroll(), new GreaterHealScroll(), new LightningScroll(), new ManaDrainScroll(), new RecallScroll(),
	  new BladeSpiritsScroll(), new DispelFieldScroll(), new IncognitoScroll(), new MagicReflectScroll(), new MindBlastScroll(), new ParalyzeScroll(), new PoisonFieldScroll(), new SummonCreatureScroll(),
	  new DispelScroll(), new EnergyBoltScroll(), new ExplosionScroll(), new InvisibilityScroll(), new MarkScroll(), new MassCurseScroll(), new ParalyzeFieldScroll(), new RevealScroll(),
	  new ChainLightningScroll(), new EnergyFieldScroll(), new FlamestrikeScroll(), new GateTravelScroll(), new ManaVampireScroll(), new MassDispelScroll(), new MeteorSwarmScroll(), new PolymorphScroll(),
	  new EarthquakeScroll(), new EnergyVortexScroll(), new ResurrectionScroll(), new SummonAirElementalScroll(), new SummonDaemonScroll(), new SummonEarthElementalScroll(), new SummonFireElementalScroll(), new SummonWaterElementalScroll()
  };

			public override void OnResponse( NetState state, RelayInfo info )
			{
				Mobile from = state.Mobile;

				if ( info.ButtonID == 0 )
					return;

				int spellID = info.ButtonID - 1;
				if ( m_Scrolls[spellID] != 0 )
				{
					SpellScroll scroll = (SpellScroll) Activator.CreateInstance( scrollTable[spellID].GetType() );

					from.SendMessage( "Vous tentez de retirer {0} du classeur...", scroll.Name );
					if ( from.Backpack.TryDropItem( from, scroll, false ) )
					{
						from.SendLocalizedMessage( 502243 ); // ...and place it into your backpack.
						from.PlaySound( 0x249 );
						m_Scrolls[spellID]--;
					}
					else
					{
						scroll.Delete();
						from.SendMessage( "...mais il n'y a plus assez de place dans votre sac." );
					}
				}
				else
				{
					from.SendMessage( "Il n'y a plus de parchemin de ce type." );
				}
			}
		}
	}
}