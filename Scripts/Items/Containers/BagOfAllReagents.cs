using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class BagOfAllReagents : Bag
	{
		[Constructable]
		public BagOfAllReagents() : this( 50 )
		{
		}

		[Constructable]
		public BagOfAllReagents( int amount )
		{

		     DropReagents(amount);
		}

        public void DropReagents(int amount)
        {
            DropOrStackItem(new BlackPearl(amount));
            DropOrStackItem(new Bloodmoss(amount));
            DropOrStackItem(new Garlic(amount));
            DropOrStackItem(new Ginseng(amount));
            DropOrStackItem(new MandrakeRoot(amount));
            DropOrStackItem(new Nightshade(amount));
            DropOrStackItem(new SulfurousAsh(amount));
            DropOrStackItem(new SpidersSilk(amount));
            DropOrStackItem(new BatWing(amount));
            DropOrStackItem(new GraveDust(amount));
            DropOrStackItem(new DaemonBlood(amount));
            DropOrStackItem(new NoxCrystal(amount));
            DropOrStackItem(new PigIron(amount));
        }



		public BagOfAllReagents( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}