using System;
using Server;
using Server.Items;

namespace Server.Items
{
    public class BagOfReagents : Bag
    {
        [Constructable]
        public BagOfReagents()
            : this(50)
        {
        }

        [Constructable]
        public BagOfReagents(int amount)
        {
            this.Movable = true;
            this.Hue = 1236;
            this.Name = "Sac de r�actifs arcanniques";

            DropReagents(amount);
        }

        public void DropReagents(int amount)
        {
            DropOrStackItem(new BlackPearl(amount));
            DropOrStackItem(new Bloodmoss(amount));
            DropOrStackItem(new Garlic(amount));
            DropOrStackItem(new Ginseng(amount));
            DropOrStackItem(new MandrakeRoot(amount));
            DropOrStackItem(new Nightshade(amount));
            DropOrStackItem(new SulfurousAsh(amount));
            DropOrStackItem(new SpidersSilk(amount));
        }

        public BagOfReagents(Serial serial)
            : base(serial)
        {

        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}