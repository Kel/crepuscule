// A foutre a la suite de Container.cs
// Sur de rien lol, a tester ^^
// Serieux doute sur le ====> get{ return new Rectangle2D[ 44, 35, 165, 75 ); }

using System;
using System.Collections;
using Server.Multis;
using Server.Mobiles;
using Server.Network;

namespace Server.Items
{
	[DynamicFliping]
	[Flipable( 0x2CF0, 0x2CF1 )]
	public class CadeauA : LockableContainer
	{
		public override int DefaultGumpID{ get{ return 0x102; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 35, 10, 155, 85 ); }
		}

		[Constructable]
		public CadeauA() : base( 0x2CF0 )
		{
			Weight = 1.0;
		}

		public CadeauA( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[DynamicFliping]
	[Flipable( 0x2CF5, 0x2CF4 )]
	public class CadeauB : LockableContainer
	{
		public override int DefaultGumpID{ get{ return 0x102; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 35, 10, 155, 85 ); }
		}

		[Constructable]
		public CadeauB() : base( 0x2CF5 )
		{
			Weight = 1.0;
		}

		public CadeauB( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
	
			int version = reader.ReadInt();
		}
	}
	
	[DynamicFliping]
	[Flipable( 0x2CF9, 0x2CFA )]
	public class CadeauC : LockableContainer
	{
		public override int DefaultGumpID{ get{ return 0x102; } }
		public override int DefaultDropSound{ get{ return 0x42; } }
	
		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 35, 10, 155, 85 ); }
		}
	
		[Constructable]
		public CadeauC() : base( 0x2CF9 )
		{
			Weight = 1.0;
		}
	
		public CadeauC( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
	
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
	
			int version = reader.ReadInt();
		}
	}

	[DynamicFliping]
	[Flipable( 0x2CFC, 0x2CFB )]
	public class CadeauD : LockableContainer
	{
		public override int DefaultGumpID{ get{ return 0x102; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 35, 10, 155, 85 ); }
		}
	
		[Constructable]
		public CadeauD() : base( 0x2CFC )
		{
			Weight = 1.0;
		}

		public CadeauD( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

	[DynamicFliping]
	[Flipable( 0x2D03, 0x2D04 )]
	public class CadeauE : LockableContainer
	{
		public override int DefaultGumpID{ get{ return 0x102; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 35, 10, 155, 85 ); }
		}
	
		[Constructable]
		public CadeauE() : base( 0x2D03 )
		{
			Weight = 1.0;
		}
	
		public CadeauE( Serial serial ) : base( serial )
		{
		}
	
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
	
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
	
			int version = reader.ReadInt();
		}
	}
}