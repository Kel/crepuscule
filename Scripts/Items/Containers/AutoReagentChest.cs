﻿using System;
using System.Collections;
using Server.Multis;
using Server.Mobiles;
using Server.Network;
using System.Collections.Generic;

namespace Server.Items.Containers
{
    [DynamicFliping]
    [Flipable(0x9AB, 0xE7C)]
    public abstract class BaseAutoReagentChest : LockableContainer
    {
        #region Attributes, Properties
        private int m_qty;

        [CommandProperty(AccessLevel.GameMaster)]
        public int Qty
        {
            get { return m_qty; }
            set { m_qty = value; }
        }

        private bool m_normalReagents;

        [CommandProperty(AccessLevel.GameMaster)]
        public bool NormalReagents
        {
            get { return m_normalReagents; }
            set { m_normalReagents = value; }
        }

        private bool m_druidReagents;

        [CommandProperty(AccessLevel.GameMaster)]
        public bool DruidReagents
        {
            get { return m_druidReagents; }
            set { m_druidReagents = value; }
        }

        private bool m_necroReagents;

        [CommandProperty(AccessLevel.GameMaster)]
        public bool NecroReagents
        {
            get { return m_necroReagents; }
            set { m_necroReagents = value; }
        }
        #endregion

        public static List<BaseAutoReagentChest> ReagentChests = new List<BaseAutoReagentChest>();

        public BaseAutoReagentChest(int itemID) : base(itemID)
        {
            Locked = true;
            Movable = false;
            Name = "Generateur de réactifs";
            ReagentChests.Add(this);
            MaxItems = 0;
        }

        public BaseAutoReagentChest(Serial serial)
            : base(serial)
		{
		}

        public override void OnDelete()
        {
            ReagentChests.Remove(this);
            base.OnDelete();
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (from.InRange(this.Location, 2))
            {
                base.OnDoubleClick(from);
            }
            else
            {
                from.SendMessage("Vous êtes trop loin");
            }
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)m_qty);
            writer.Write((bool)m_necroReagents);
            writer.Write((bool)m_normalReagents);
            writer.Write((bool)m_druidReagents);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            if (ReagentChests == null) ReagentChests = new List<BaseAutoReagentChest>();
            ReagentChests.Add(this);

            base.Deserialize(reader);
            m_qty = reader.ReadInt();
            m_necroReagents = reader.ReadBool();
            m_normalReagents = reader.ReadBool();
            m_druidReagents = reader.ReadBool();
            int version = reader.ReadInt();
        }
    }

    [FlipableAttribute(0xe41, 0xe40)]
    public class AutoReagentChest : BaseAutoReagentChest
    {
        public override int DefaultGumpID { get { return 0x42; } }
        public override int DefaultDropSound { get { return 0x42; } }

        public override Rectangle2D Bounds
        {
            get { return new Rectangle2D(20, 105, 150, 180); }
        }

        [Constructable]
        public AutoReagentChest()
            : base(0xE41)
        {
        }

        public AutoReagentChest(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    } 

    public class AutoReagentChestTimer : Timer
    {
        public static TimeSpan DistroDelay = new TimeSpan(0, 0, 1, 0);
        public static TimeSpan TimerDelay = new TimeSpan(0, 1, 0, 0);
        public static AutoReagentChestTimer Timer = null;

        public static void Initialize()
        {
            Timer = new AutoReagentChestTimer();
        }

        public AutoReagentChestTimer(): base(TimerDelay, TimerDelay)
        {
            this.Start();
        }

        protected override void OnTick()
        {
            // Distribute at 10 O'Clock, every day
            if (DateTime.Now.Hour == 10)
            {
                GenerateReagents();
            }
            
        }

        /// <summary>
        /// Generates the reagents
        /// </summary>
        public static void GenerateReagents()
        {
            var chests = BaseAutoReagentChest.ReagentChests;
            foreach (var chest in chests)
            {
                if (chest.Qty > 0)
                {
                    if (chest.NecroReagents)
                    {
                        var bag = chest.FindItemByType(typeof(BagOfNecroReagents));
                        if (bag == null) chest.AddItem(new BagOfNecroReagents(chest.Qty));
                        else
                        {
                            (bag as BagOfNecroReagents).DropReagents(chest.Qty);
                        }
                    }

                    if (chest.NormalReagents)
                    {
                        var bag = chest.FindItemByType(typeof(BagOfReagents));
                        if (bag == null) chest.AddItem(new BagOfReagents(chest.Qty));
                        else
                        {
                            (bag as BagOfReagents).DropReagents(chest.Qty);
                        }
                    }

                    if (chest.DruidReagents)
                    {
                        var bag = chest.FindItemByType(typeof(BagOfDruidReagents));
                        if (bag == null) chest.AddItem(new BagOfDruidReagents(chest.Qty));
                        else
                        {
                            (bag as BagOfDruidReagents).DropReagents(chest.Qty);
                        }
                    }

                }
            }
        }


     }
}
