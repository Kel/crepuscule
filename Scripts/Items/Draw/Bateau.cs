using System;
using Server;

namespace Server.Items
{
	public class Bateau : CrepusculeItem
	{
		

		[Constructable]
		public Bateau() : this( 1 ){
                Name = "Bateau";
		{
		}

}		[Constructable]
		public Bateau( int amount ) : base( 0x3CE6 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Bateau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}