using System;
using Server;

namespace Server.Items
{
	public class Lumiere : CrepusculeItem
	{
		

		[Constructable]
		public Lumiere() : this( 1 ){
                Name = "Sainte Lumiere";
		{
		}

}		[Constructable]
		public Lumiere( int amount ) : base( 0x3CE4 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Lumiere( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}