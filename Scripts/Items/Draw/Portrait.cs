using System;
using Server;

namespace Server.Items
{
	public class Portrait : CrepusculeItem
	{
		

		[Constructable]
		public Portrait() : this( 1 ){
                Name = "Portrait";
		{
		}

}		[Constructable]
		public Portrait( int amount ) : base( 0xE9F )
		{
			Stackable = false;
			Amount = amount;
		}

		public Portrait( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}