using System;
using Server;

namespace Server.Items
{
	public class Serin : CrepusculeItem
	{
		

		[Constructable]
		public Serin() : this( 1 ){
                Name = "Serin";
		{
		}

}		[Constructable]
		public Serin( int amount ) : base( 0x3D05 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Serin( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}