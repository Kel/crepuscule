using System;
using Server;

namespace Server.Items
{
	public class Marin : CrepusculeItem
	{
		

		[Constructable]
		public Marin() : this( 1 ){
                Name = "Paysage Marin";
		{
		}

}		[Constructable]
		public Marin( int amount ) : base( 0x3CDF )
		{
			Stackable = false;
			Amount = amount;
		}

		public Marin( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}