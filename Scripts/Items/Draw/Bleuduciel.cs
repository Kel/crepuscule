using System;
using Server;

namespace Server.Items
{
	public class Bleuduciel : CrepusculeItem
	{
		

		[Constructable]
		public Bleuduciel() : this( 1 ){
                Name = "Bleu du ciel";
		{
		}

}		[Constructable]
		public Bleuduciel( int amount ) : base( 0x3D11 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Bleuduciel( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}