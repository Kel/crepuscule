using System;
using Server;

namespace Server.Items
{
	public class Loupdemer : CrepusculeItem
	{
		

		[Constructable]
		public Loupdemer() : this( 1 ){
                Name = "Vieux Loup de mer";
		{
		}

}		[Constructable]
		public Loupdemer( int amount ) : base( 0x3cf3)
		{
			Stackable = false;
			Amount = amount;
		}

		public Loupdemer( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}