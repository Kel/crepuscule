using System;
using Server;

namespace Server.Items
{
	public class ToileVierge : CrepusculeItem
	{
		

		[Constructable]
		public ToileVierge() : this( 1 ){
                Name = "Toile Vierge";
		{
		}

}		[Constructable]
		public ToileVierge( int amount ) : base( 0x3FB8 )
		{
			Stackable = false;
			Amount = amount;
		}

		public ToileVierge( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}