using System;
using Server;

namespace Server.Items
{
	public class Cuisine : CrepusculeItem
	{
		

		[Constructable]
		public Cuisine() : this( 1 ){
                Name = "Cuisine";
		{
		}

}		[Constructable]
		public Cuisine( int amount ) : base( 0x3CEA )
		{
			Stackable = false;
			Amount = amount;
		}

		public Cuisine( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}