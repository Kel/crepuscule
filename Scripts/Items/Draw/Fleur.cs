using System;
using Server;

namespace Server.Items
{
	public class Fleur : CrepusculeItem
	{
		

		[Constructable]
		public Fleur() : this( 1 ){
                Name = "Portrait";
		{
		}

}		[Constructable]
		public Fleur( int amount ) : base( 0x3CEE)
		{
			Stackable = false;
			Amount = amount;
		}

		public Fleur( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}