using System;
using Server;

namespace Server.Items
{
	public class Colline : CrepusculeItem
	{
		

		[Constructable]
		public Colline() : this( 1 ){
                Name = "Colline";
		{
		}

}		[Constructable]
		public Colline( int amount ) : base( 0x3CE1 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Colline( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}