using System;
using Server;

namespace Server.Items
{
	public class Verdure : CrepusculeItem
	{
		

		[Constructable]
		public Verdure() : this( 1 ){
                Name = "Verdure";
		{
		}

}		[Constructable]
		public Verdure( int amount ) : base( 0x3CE2 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Verdure( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}