using System;
using Server;

namespace Server.Items
{
	public class Labo : CrepusculeItem
	{
		

		[Constructable]
		public Labo() : this( 1 ){
                Name = "Labo";
		{
		}

}		[Constructable]
		public Labo( int amount ) : base( 0x3CFB)
		{
			Stackable = false;
			Amount = amount;
		}

		public Labo( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}