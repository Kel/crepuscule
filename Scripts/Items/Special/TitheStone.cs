using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;
using Server.Guilds;
using System.Collections.Generic;

namespace Server.Items {

    [Flipable(0x0EDB,0x0EDC)]
    public class TitheStone : CrepusculeItem 
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Rayon")]
        public int Radius { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nb. Dime")]
        public int Tithe { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("D�lais en minutes")]
        public int Cooldown { get; set; }

        private InternalTimer Timer;

        [Constructable]
        public TitheStone() : base(0x0EDB)
        {
            Movable = false;
            Name = "Lieu Sacr�";
            Radius = 3;
            Cooldown = 120;
            Tithe = 100;
            Timer = new InternalTimer(this);
            Timer.Start();
        }

        public TitheStone(Serial serial) : base(serial)
        {
            Timer = new InternalTimer(this);
            Timer.Start();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 
            writer.Write(Radius);
            writer.Write(Tithe);
            writer.Write(Cooldown);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            Radius = reader.ReadInt();
            Tithe = reader.ReadInt();
            Cooldown = reader.ReadInt();
        }

        public override void OnDelete()
        {
            Timer.Stop();
            base.OnDelete();
        }

        private class InternalTimer : Timer 
        {
            /// <summary>
            /// Table for cooldowns
            /// </summary>
            internal Dictionary<RacePlayerMobile, DateTime> CooldownsTable = new Dictionary<RacePlayerMobile, DateTime>();
            private TitheStone Stone;

            public InternalTimer(TitheStone stone) : base(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5))
            {
                Stone = stone;
                Priority = TimerPriority.FiftyMS;
            }

            protected override void OnTick()
            {
                var clients = Stone.GetMobilesInRange(Stone.Radius);
                foreach (Mobile m in clients)
                {
                    if (m == null || !(m is RacePlayerMobile) || m.AccessLevel != AccessLevel.Player)
                        continue;

                    // Player
                    var player = m as RacePlayerMobile;
                    var giveTithe = (CooldownsTable.ContainsKey(player) && DateTime.Now < CooldownsTable[player]) ? false : true;

                    //player.InRange(this.Location, Radius) 

                    if (giveTithe) // check if player in range.
                    {
                        var next = DateTime.Now + TimeSpan.FromMinutes(Stone.Cooldown); // set next time
                        if (CooldownsTable.ContainsKey(player))
                            CooldownsTable[player] = next;
                        else
                            CooldownsTable.Add(player, next);

                        switch (Utility.Random(5)) // 5 speech options
                        {
                            case 0: player.SendMessage("Vous penetrez dans un lieu sacr�."); break;
                            case 1: player.SendMessage("Vous ressentez la pr�sence divine."); break;
                            case 2: player.SendMessage("Votre divinit� est avec vous!"); break;
                            case 3: player.SendMessage("Vous contemplez le lieu sacr�"); break;
                            case 4: player.SendMessage("Un sentiment de paix divine vous envahit"); break;
                            default: player.SendMessage("Vous penetrez dans un lieu sacr�."); break;
                        };

                        // Add tithe
                        player.TithingPoints += Stone.Tithe;
                        if (player.TithingPoints > 100000)
                            player.TithingPoints = 100000;

                        // play effect
                        player.FixedParticles(0x373A, 10, 15, 5018, EffectLayer.Waist);
                        
                        player.PlaySound(0x212);
                        player.PlaySound(0x206);                        
                    }
                }

            }
        }

    }



}
