                                    
using System; 
using Server; 
using Server.Misc;
using Server.Items; 
using Server.Targeting;
using Server.Accounting; 

namespace Server.Items 
{
    public class GMTalisman : BaseNecklace
    {
        [Constructable]
        public GMTalisman()
            : base(0x1085)
        {
            Weight = 0.1;
            Name = "Talisman de Crépuscule";
            LootType = LootType.Blessed;
        }

        public GMTalisman(Serial serial)
            : base(serial)
        {
        }

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);

            if (!IsChildOf(from.Backpack))
            {
                from.SendMessage("Vous devez l'avoir dans le sac.");
            }
            else if ((from.NetState.Account as Account).AccessLevel >= AccessLevel.GameMaster)
            {
                if (from.AccessLevel == AccessLevel.Player)
                {
                    from.AccessLevel = (from.NetState.Account as Account).AccessLevel;
                }
                else
                {
                    from.AccessLevel = AccessLevel.Player;
                }
            }
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
       
   
}