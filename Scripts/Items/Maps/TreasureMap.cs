using System;
using System.IO;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;

namespace Server.Items
{
	public class TreasureMap : MapItem
	{
		private int m_Level;
		private bool m_Completed;
		private Mobile m_Decoder;
		private Map m_Map;
		private Point2D m_Location;

		[CommandProperty( AccessLevel.GameMaster )]
		public int Level{ get{ return m_Level; } set{ m_Level = value; InvalidateProperties(); } }

		[CommandProperty( AccessLevel.GameMaster )]
		public bool Completed{ get{ return m_Completed; } set{ m_Completed = value; InvalidateProperties(); } }

		[CommandProperty( AccessLevel.GameMaster )]
		public Mobile Decoder{ get{ return m_Decoder; } set{ m_Decoder = value; InvalidateProperties(); } }

		[CommandProperty( AccessLevel.GameMaster )]
		public Map ChestMap{ get{ return m_Map; } set{ m_Map = value; InvalidateProperties(); } }

		[CommandProperty( AccessLevel.GameMaster )]
		public Point2D ChestLocation{ get{ return m_Location; } set{ m_Location = value; } }

		private static Point2D[] m_Locations;

		private static Type[][] m_SpawnTypes = new Type[][]
			{
				new Type[]{ typeof( Mongbat ), typeof( Skeleton ) },
				new Type[]{ typeof( Mongbat ), typeof( Ratman ), typeof( HeadlessOne ), typeof( Skeleton ), typeof( Zombie ) },
				new Type[]{ typeof( OrcishMage ), typeof( Gargoyle ), typeof( Gazer ), typeof( HellHound ), typeof( EarthElemental ) },
				new Type[]{ typeof( Lich ), typeof( OgreLord ), typeof( DreadSpider ), typeof( AirElemental ), typeof( FireElemental ) },
				new Type[]{ typeof( DreadSpider ), typeof( LichLord ), typeof( Daemon ), typeof( ElderGazer ), typeof( OgreLord ) },
				new Type[]{ typeof( LichLord ), typeof( Daemon ), typeof( ElderGazer ), typeof( PoisonElemental ), typeof( BloodElemental ) }
			};

		public const double LootChance = 0.01; // 1% chance to appear as loot

		public static Point2D GetRandomLocation()
		{
			if ( m_Locations == null )
				LoadLocations();

			if ( m_Locations.Length > 0 )
				return m_Locations[Utility.Random( m_Locations.Length )];

			return Point2D.Zero;
		}

		private static void LoadLocations()
		{
			string filePath = Path.Combine( Core.BaseDirectory, "Data/treasure.cfg" );

			ArrayList list = new ArrayList();

			if ( File.Exists( filePath ) )
			{
				using ( StreamReader ip = new StreamReader( filePath ) )
				{
					string line;

					while ( (line = ip.ReadLine()) != null )
					{
						try
						{
							string[] split = line.Split( ' ' );

							int x = Convert.ToInt32( split[0] ), y = Convert.ToInt32( split[1] );

							list.Add( new Point2D( x, y ) );
						}
						catch
						{
						}
					}
				}
			}

			m_Locations = (Point2D[])list.ToArray( typeof( Point2D ) );
		}

		public static BaseCreature Spawn( int level )
		{
			if ( level >= 0 && level < m_SpawnTypes.Length )
			{
				try
				{
					return (BaseCreature)Activator.CreateInstance( m_SpawnTypes[level][Utility.Random( m_SpawnTypes[level].Length )] );
				}
				catch
				{
				}
			}

			return null;
		}

		public static void Spawn( int level, Point3D p, Map map, Mobile target )
		{
			if ( map == null )
				return;

			BaseCreature c = Spawn( level );

			if ( c != null )
			{
				bool spawned = false;

				for ( int i = 0; !spawned && i < 10; ++i )
				{
					int x = p.X - 3 + Utility.Random( 7 );
					int y = p.Y - 3 + Utility.Random( 7 );

					if ( map.CanSpawnMobile( x, y, p.Z ) )
					{
						c.MoveToWorld( new Point3D( x, y, p.Z ), map );
						spawned = true;
					}
					else
					{
						int z = map.GetAverageZ( x, y );

						if ( map.CanSpawnMobile( x, y, z ) )
						{
							c.MoveToWorld( new Point3D( x, y, z ), map );
							spawned = true;
						}
					}
				}

				if ( !spawned )
					c.Delete();
				else if ( target != null )
					c.Combatant = target;
			}
		}

		[Constructable]
		public TreasureMap( int level, Map map )
		{
			m_Level = level;
			m_Map = map;
			m_Location = GetRandomLocation();

			Width = 300;
			Height = 300;

			int width = 600;
			int height = 600;

			int x1 = m_Location.X - Utility.RandomMinMax( width / 4, (width / 4) * 3 );
			int y1 = m_Location.Y - Utility.RandomMinMax( height / 4, (height / 4) * 3 );

			if ( x1 < 0 )
				x1 = 0;

			if ( y1 < 0 )
				y1 = 0;

			int x2 = x1 + width;
			int y2 = y1 + height;

			if ( x2 >= 5120 )
				x2 = 5119;

			if ( y2 >= 4096 )
				y2 = 4095;

			x1 = x2 - width;
			y1 = y2 - height;

			Bounds = new Rectangle2D( x1, y1, width, height );
			Protected = true;

			AddWorldPin( m_Location.X, m_Location.Y );
		}

		public TreasureMap( Serial serial ) : base( serial )
		{
		}

		public void OnBeginDig( Mobile from )
		{
			if ( m_Completed )
			{
				from.SendLocalizedMessage( 503014 ); // This treasure hunt has already been completed.
			}
			else if ( from != m_Decoder )
			{
				from.SendLocalizedMessage( 503016 ); // Only the person who decoded this map may actually dig up the treasure.
			}
			else if ( !from.CanBeginAction( typeof( TreasureMap ) ) )
			{
				from.SendLocalizedMessage( 503020 ); // You are already digging treasure.
			}
			else
			{
				from.SendLocalizedMessage( 503033 ); // Where do you wish to dig?
				from.Target = new DigTarget( this );
			}
		}

		private class DigTarget : Target
		{
			private TreasureMap m_Map;

			public DigTarget( TreasureMap map ) : base( 6, true, TargetFlags.None )
			{
				m_Map = map;
			}

			protected override void OnTarget( Mobile from, object targeted )
			{
				if ( m_Map.Deleted )
					return;

				if ( m_Map.m_Completed )
				{
					from.SendLocalizedMessage( 503014 ); // This treasure hunt has already been completed.
				}
				else if ( from != m_Map.m_Decoder )
				{
					from.SendLocalizedMessage( 503016 ); // Only the person who decoded this map may actually dig up the treasure.
				}
				else if ( !from.CanBeginAction( typeof( TreasureMap ) ) )
				{
					from.SendLocalizedMessage( 503020 ); // You are already digging treasure.
				}
				else
				{
					IPoint3D p = targeted as IPoint3D;

					if ( p is Item )
						p = ((Item)p).GetWorldLocation();

					int maxRange;
					double skillValue = from.Skills[SkillName.Mining].Value;

					if ( skillValue >= 100.0 )
						maxRange = 4;
					else if ( skillValue >= 81.0 )
						maxRange = 3;
					else if ( skillValue >= 51.0 )
						maxRange = 2;
					else
						maxRange = 1;

					Point2D loc = m_Map.m_Location;
					int x = loc.X, y = loc.Y;
					Map map = m_Map.m_Map;

					if ( map == from.Map && Utility.InRange( new Point3D( p ), new Point3D( loc, 0 ), maxRange ) )
					{
						if ( from.Location.X == loc.X && from.Location.Y == loc.Y )
						{
							from.SendLocalizedMessage( 503030 ); // The chest can't be dug up because you are standing on top of it.
						}
						else if ( map != null )
						{
							int z = map.GetAverageZ( x, y );

							if ( !map.CanFit( x, y, z, 16, true, true ) )
							{
								from.SendLocalizedMessage( 503021 ); // You have found the treasure chest but something is keeping it from being dug up.
							}
							else if ( from.BeginAction( typeof( TreasureMap ) ) )
							{
								new DigTimer( from, m_Map, new Point3D( x, y, z - 14 ), map, z ).Start();
							}
							else
							{
								from.SendLocalizedMessage( 503020 ); // You are already digging treasure.
							}
						}
					}
					else if ( Utility.InRange( new Point3D( p ), from.Location, 8 ) ) // We're close, but not quite
					{
						from.SendLocalizedMessage( 503032 ); // You dig and dig but no treasure seems to be here.
					}
					else
					{
						from.SendLocalizedMessage( 503035 ); // You dig and dig but fail to find any treasure.
					}
				}
			}
		}

		private class DigTimer : Timer
		{
			private Mobile m_From;
			private TreasureMap m_TreasureMap;
			private Map m_Map;
			private TreasureMapChest m_Chest;
			private int m_Count;
			private int m_Z;
			private DateTime m_NextSkillTime;
			private DateTime m_NextSpellTime;
			private DateTime m_NextActionTime;
			private DateTime m_LastMoveTime;

			public DigTimer( Mobile from, TreasureMap treasureMap, Point3D p, Map map, int z ) : base( TimeSpan.Zero, TimeSpan.FromSeconds( 1.0 ) )
			{
				m_From = from;
				m_TreasureMap = treasureMap;
				m_Map = map;
				m_Z = z;

				m_Chest = new TreasureMapChest( from, treasureMap.m_Level );
				m_Chest.MoveToWorld( p, map );

				m_NextSkillTime = from.NextSkillTime;
				m_NextSpellTime = from.NextSpellTime;
				m_NextActionTime = from.NextActionTime;
				m_LastMoveTime = from.LastMoveTime;
			}

			protected override void OnTick()
			{
				if ( m_NextSkillTime != m_From.NextSkillTime || m_NextSpellTime != m_From.NextSpellTime || m_NextActionTime != m_From.NextActionTime )
				{
					Stop();
					m_From.EndAction( typeof( TreasureMap ) );
					m_Chest.Delete();
				}
				else if ( m_LastMoveTime != m_From.LastMoveTime )
				{
					m_From.SendLocalizedMessage( 503023 ); // You cannot move around while digging up treasure. You will need to start digging anew.

					Stop();
					m_From.EndAction( typeof( TreasureMap ) );
					m_Chest.Delete();
				}
				/*else if ( !m_Map.CanFit( m_Chest.X, m_Chest.Y, m_Z, 16, true, true ) )
				{
					m_From.SendLocalizedMessage( 503024 ); // You stop digging because something is directly on top of the treasure chest.

					Stop();
					m_From.EndAction( typeof( TreasureMap ) );
					m_Chest.Delete();
				}*/
				else
				{
					m_From.RevealingAction();

					m_Count++;

					m_Chest.Location = new Point3D( m_Chest.Location.X, m_Chest.Location.Y, m_Chest.Location.Z + 1 );
					m_From.Direction = m_From.GetDirectionTo( m_Chest.GetWorldLocation() );

					if ( m_Count == 14 )
					{
						Stop();
						m_From.EndAction( typeof( TreasureMap ) );

						m_TreasureMap.Completed = true;

						if ( m_TreasureMap.Level >= 2 )
						{
							for ( int i = 0; i < 4; ++i )
								Spawn( m_TreasureMap.Level, m_Chest.Location, m_Chest.Map, null );
						}
					}
					else
					{
						if ( m_From.Body.IsHuman && !m_From.Mounted )
							m_From.Animate( 11, 5, 1, true, false, 0 );

						new SoundTimer( m_From, 0x125 + (m_Count % 2) ).Start();
					}
				}
			}

			private class SoundTimer : Timer
			{
				private Mobile m_From;
				private int m_SoundID;

				public SoundTimer( Mobile from, int soundID ) : base( TimeSpan.FromSeconds( 0.9 ) )
				{
					m_From = from;
					m_SoundID = soundID;
				}

				protected override void OnTick()
				{
					m_From.PlaySound( m_SoundID );
				}
			}
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !m_Completed && m_Decoder == null )
			{
				double midPoint = 0.0;

				switch ( m_Level )
				{
					case 1: midPoint =  27.0; break;
					case 2: midPoint =  71.0; break;
					case 3: midPoint =  81.0; break;
					case 4: midPoint =  91.0; break;
					case 5: midPoint = 100.0; break;
				}

				double minSkill = midPoint - 30.0;
				double maxSkill = midPoint + 30.0;

				if ( from.Skills[SkillName.Cartography].Value < minSkill )
					from.SendLocalizedMessage( 503013 ); // The map is too difficult to attempt to decode.

				if ( from.CheckSkill( SkillName.Cartography, minSkill, maxSkill ) )
				{
					from.LocalOverheadMessage( MessageType.Regular, 0x3B2, 503019 ); // You successfully decode a treasure map!
					Decoder = from;

					from.PlaySound( 0x249 );
					base.OnDoubleClick( from );
				}
				else
				{
					from.LocalOverheadMessage( MessageType.Regular, 0x3B2, 503018 ); // You fail to make anything of the map.
				}
			}
			else if ( m_Completed )
			{
				from.SendLocalizedMessage( 503014 ); // This treasure hunt has already been completed.
				base.OnDoubleClick( from );
			}
			else
			{
				from.SendLocalizedMessage( 503017 ); // The treasure is marked by the red pin. Grab a shovel and go dig it up!
				base.OnDoubleClick( from );
			}
		}

		public override int LabelNumber{ get{ return (m_Decoder != null ? 1041516 + m_Level : 1041510 + m_Level); } }

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

			list.Add( m_Map == Map.Felucca ? 1041502 : 1041503 ); // for somewhere in Felucca : for somewhere in Trammel

			if ( m_Completed )
				list.Add( 1041507, m_Decoder == null ? "someone" : m_Decoder.Name ); // completed by ~1_val~
		}

		public override void OnSingleClick( Mobile from )
		{
			if ( m_Completed )
				from.Send( new MessageLocalizedAffix( Serial, ItemID, MessageType.Label, 0x3B2, 3, 1048030, "", AffixType.Append, String.Format( " completed by {0}", m_Decoder == null ? "someone" : m_Decoder.Name ), "" ) );
			else if ( m_Decoder != null )
				LabelTo( from, 1041516 + m_Level );
			else
				LabelTo( from, 1041522, String.Format( "#{0}\t \t#{1}", 1041510 + m_Level, m_Map == Map.Felucca ? 1041502 : 1041503 ) );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );

			writer.Write( m_Level );
			writer.Write( m_Completed );
			writer.Write( m_Decoder );
			writer.Write( m_Map );
			writer.Write( m_Location );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
				case 0:
				{
					m_Level = (int)reader.ReadInt();
					m_Completed = reader.ReadBool();
					m_Decoder = reader.ReadMobile();
					m_Map = reader.ReadMap();
					m_Location = reader.ReadPoint2D();

					break;
				}
			}
		}
	}
}