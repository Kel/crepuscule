﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Server.Misc;
using System.Collections.Concurrent;
using Server.Network;
using Server.Accounting;
using Server.Mobiles;
using System.IO;

namespace Server.Web.Rendering
{


    public class PaperdollGenerator : Timer
    {
        private static PaperdollGenerator Generator;
        private static bool UseFirstStore = true;
        private static ConcurrentDictionary<Serial, PaperdollElement> Store1;
        private static ConcurrentDictionary<Serial, PaperdollElement> Store2;

        public static void Initialize()
        {
            Store1 = new ConcurrentDictionary<Serial, PaperdollElement>();
            Store2 = new ConcurrentDictionary<Serial, PaperdollElement>();
            Generator = new PaperdollGenerator();
            Generator.Start();
        }

        public PaperdollGenerator()
            : base(TimeSpan.FromSeconds(1), TimeSpan.FromMinutes(1.0))
        {

        }

        protected override void OnTick()
        {
            ConcurrentDictionary<Serial, PaperdollElement> Store
                = UseFirstStore ? Store2 : Store1;
            lock (Store)
            {
                Store.Clear();

                // Test
                /*List<Mobile> mobs = new List<Mobile>();
                var ac = new string[] { "johanie", "faenya", "Gw3n", "brian01", "tolkien", "super86", "peluche", "kikoyu_13" }
                                    .Select(s => Accounts.GetAccount(s));
                foreach (var account in ac)
                for (int i = 0; i < 5; ++i)
                    if (account != null && account[i] != null) mobs.Add(account[i]);*/

                //foreach (Mobile m in mobs)
                foreach (NetState ns in NetState.Instances)
                {
                    try
                    {
                        Mobile m = ns.Mobile;
                        //if (m != null)
                        {
                            PaperdollElement doll = PaperdollGenerator.PreparePaperdoll(m as Mobile);
                            Store.TryAdd(m.Serial, doll);
                        }
                    }
                    catch { }
                }
            }

            // Switch the Store
            UseFirstStore = !UseFirstStore;
        }

        #region Static Methods

        public static PaperdollElement PreparePaperdoll(Mobile m)
        {
            PaperdollElement doll = new PaperdollElement();
            doll.BodyHue = (m.SolidHueOverride > 0) ? m.SolidHueOverride : m.Hue;
            doll.ItemHueOverride = (m.SolidHueOverride > 0) ? m.SolidHueOverride : 0;
            doll.BodyGump = GetBodyGumpID(m);
            doll.AccessLevel = m.AccessLevel;
            doll.Equipment = new List<ItemElement>();
            doll.IsIncognito = false;

            // Set the correct body
            if (m.Body == 400) doll.Female = false;
            else if (m.Body == 401) doll.Female = true;
            else doll.Female = m.Female;

            if (m is RacePlayerMobile)
            {
                var player = m as RacePlayerMobile;
                doll.IsIncognito = player.GuildInfo.IsInvisible;
            }

            if (m.Alive && doll.BodyGump != 50970 && doll.BodyGump != 60970)
            {
                List<Item> items = new List<Item>(m.Items.Cast<Item>());
                items.Sort(LayerComparer<Item>.Instance);
                List<Item> sortItems = new List<Item>(items);

                bool hidePants = false;
                for (int i = 0; i < items.Count; i++)
                {
                    Item item = items[i];
                    ItemElement data = new ItemElement();
                    if (item == null || item.Deleted)
                        continue;

                    if (!LayerComparer<Item>.IsValid(item))
                        continue;

                    if (item.Layer == Layer.OuterLegs)
                        hidePants = true;
                    else if (hidePants && (item.Layer == Layer.Pants || item.Layer == Layer.InnerLegs))
                        continue;

                    data.ItemID = item.ItemID;
                    data.Layer = item.Layer;
                    data.Hue = item.Hue;
                    doll.Equipment.Add(data);
                }

                items.Clear();
            }


            return doll;
        }

        /// <summary>
        /// Gets currently used store
        /// </summary>
        /// <returns></returns>
        public static ConcurrentDictionary<Serial, PaperdollElement> GetCurrentStore()
        {
            return UseFirstStore ? Store1 : Store2;
        }

        /// <summary>
        /// Gets paperdoll if one exits
        /// </summary>
        public static PaperdollElement? GetPaperdoll(Serial serial)
        {
            PaperdollElement value;
            if (GetCurrentStore().TryGetValue(serial, out value))
                return new PaperdollElement?(value);
            return new PaperdollElement?();
        }

        private static int[][] _BodyGumps = new int[][] { 
         new int[] { 400, 401, 402, 403, 605, 606, 666, 667, 183, 184, -1, -1, -1, -1, -1, -1 }, 
         new int[] { 12, 13, 50970, 60970, 14, 15, 666, 665, 120, 121, 1888, 1889, 1893, 1894, 1898, 1899 } };

        private static int GetBodyGumpID(Mobile m)
        {
            int myBody = (m.BodyMod.BodyID != 0x0) ? m.BodyMod.BodyID : m.Body.BodyID;

            if (myBody == 970)
                return m.Female ? 60970 : 50970;

            for (int i = 0; i < _BodyGumps[0].Length && i < _BodyGumps[1].Length; i++)
            {
                if (_BodyGumps[0][i] == myBody)
                    return _BodyGumps[1][i];
            }

            return m.Female ? 13 : 12;
        }

        #endregion

    }

    #region Paperdoll DOM
    [Serializable]
    public struct PaperdollElement
    {
        public AccessLevel AccessLevel;
        public int BodyGump;
        public int BodyHue;
        public int ItemHueOverride;
        public bool Female;
        public List<ItemElement> Equipment;
        public bool IsIncognito;
    }

    [Serializable]
    public struct ItemElement
    {
        public int ItemID;
        public int Hue;
        public Layer Layer;
    }
    #endregion

    #region LayerComparer
    public class LayerComparer<T> : IComparer<T> where T : Item
    {
        private static Layer PlateArms = (Layer)255;
        private static Layer ChainTunic = (Layer)254;
        private static Layer LeatherShorts = (Layer)253;

        private static Layer[] m_DesiredLayerOrder = new Layer[]
      {
        Layer.Cloak,
        Layer.Ring,
        Layer.Talisman,
        Layer.Shirt,
        Layer.Pants,
        Layer.Shoes,
        Layer.InnerLegs,
        LeatherShorts,
        Layer.Arms,
        Layer.InnerTorso,
        Layer.Earrings,
        Layer.Bracelet,
        LeatherShorts,
        PlateArms,
        Layer.Unused_xF,
        Layer.MiddleTorso,
        Layer.OuterLegs,
        Layer.OuterTorso,
        Layer.Neck,
        Layer.Waist,
        Layer.Gloves,
        Layer.Hair,
        Layer.FacialHair,
        Layer.Helm,
        Layer.OneHanded,
        Layer.TwoHanded,
      };

        private static int[] m_TranslationTable;

        public static int[] TranslationTable
        {
            get { return m_TranslationTable; }
        }

        static LayerComparer()
        {
            m_TranslationTable = new int[256];

            for (int i = 0; i < m_DesiredLayerOrder.Length; ++i)
                m_TranslationTable[(int)m_DesiredLayerOrder[i]] = m_DesiredLayerOrder.Length - i;
        }

        public static bool IsValid(Item item)
        {
            return (m_TranslationTable[(int)item.Layer] > 0);
        }

        public static readonly IComparer<T> Instance = new LayerComparer<T>();

        public LayerComparer()
        { }

        public Layer Fix(int itemID, Layer oldLayer)
        {
            if (itemID == 0x1410 || itemID == 0x1417) // platemail arms
                return PlateArms;

            if (itemID == 0x13BF || itemID == 0x13C4) // chainmail tunic
                return ChainTunic;

            if (itemID == 0x1C08 || itemID == 0x1C09) // leather skirt
                return LeatherShorts;

            if (itemID == 0x1C00 || itemID == 0x1C01) // leather shorts
                return LeatherShorts;

            return oldLayer;
        }

        public int Compare(T x, T y)
        {
            Item a = (Item)x;
            Item b = (Item)y;

            Layer aLayer = a.Layer;
            Layer bLayer = b.Layer;

            aLayer = Fix(a.ItemID, aLayer);
            bLayer = Fix(b.ItemID, bLayer);

            /*var layers = "layers.txt";
            if (File.Exists(layers))
            {
                var output = File.ReadAllLines(layers)
                    .Select(l => Int32.Parse(l))
                    .Select(l => (Layer)l)
                    .ToArray();
                m_DesiredLayerOrder = output;

                m_TranslationTable = new int[256];

                for (int i = 0; i < m_DesiredLayerOrder.Length; ++i)
                    m_TranslationTable[(int)m_DesiredLayerOrder[i]] = m_DesiredLayerOrder.Length - i;
            }*/

            //return bLayer - aLayer;
            return m_TranslationTable[(int)bLayer] - m_TranslationTable[(int)aLayer];
        }
    }
    #endregion

}
