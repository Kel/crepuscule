﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Server.Misc;

namespace Server.Web.Rendering
{
    public class PaperdollRenderer
    {
        public static void Initialize()
        {
            //Ultima.Client.Directories.Add(DataPath.CustomPath); 
            //Console.WriteLine(Ultima.Client.Directories.Dump());
            /*EventSink.PaperdollRequest += (e) =>
            {
                GeneratePaperdoll(e.Beheld).Save(@"D:\Test2.bmp");
            };*/
          
        }


        public static Bitmap GeneratePaperdoll(PaperdollElement doll, 
            int x=0, int y=0, int width=260, int height=237, string background = null)
        {
            if (doll.IsIncognito)
                return ImageCache.GetImage(doll.Female ? "AFemale.png" : "AMale.png", 260, 237);

            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if (background != null)
                {
                    g.DrawImage(ImageCache.GetImage(background), 0, 0, width, height);
                }
                using (Bitmap gmp = Ultima.Gumps.GetGump(doll.BodyGump))
                {
                    gmp.MakeTransparent(Color.Black);
                    if (doll.BodyHue > 0 && doll.BodyHue <= 65535)
                    {
                        Ultima.Hue hue = Ultima.Hues.GetHue(doll.BodyHue - 1);
                        if (hue != null)
                            hue.ApplyTo(gmp, true);
                    }

                    g.DrawImage(gmp, x, y, 260, 237 );
                    g.Save();
                }


                for (int i = 0; i < doll.Equipment.Count; i++)
                {
                    ItemElement item = doll.Equipment[i];

                    var data = Ultima.TileData.ItemTable[item.ItemID];
                    int animID = data.Animation;
                    int animHue = item.Hue;
                    bool partial = data.Flags.HasFlag(Ultima.TileFlag.PartialHue);

                    //Console.WriteLine("Item: {0}/{1}", item.ItemID, animID);
                    //Console.WriteLine(item.ItemData.Dump());

                    HotFix(ref animID, ref animHue);

                    int gumpID = (int)(animID & 0xFFFF);

                    if (gumpID < 1 || gumpID > 65535)
                        continue;

                    if (gumpID < 10000)
                    {
                        if (doll.Female)
                            gumpID += 60000;
                        else
                            gumpID += 50000;
                    }

                    Bitmap tmp = Ultima.Gumps.GetGump(gumpID);

                    if (tmp == null)
                    {
                        if (gumpID >= 60000)
                            gumpID -= 10000;

                        tmp = Ultima.Gumps.GetGump(gumpID);
                    }

                    if (tmp == null)
                        continue;

                    using (tmp)
                    {
                        int overallHue = 0;

                        if (doll.ItemHueOverride > 0)
                            overallHue = doll.ItemHueOverride;
                        else if (animHue > 0)
                            overallHue = animHue;
                        else if (item.Hue > 0)
                            overallHue = item.Hue;

                        tmp.MakeTransparent(Color.Black);
                        if (overallHue >= 1 && overallHue <= 3000)
                        {
                            Ultima.Hue hue = Ultima.Hues.GetHue(overallHue - 1);
                            if (hue != null)
                                hue.ApplyTo(tmp, partial);
                        }

                        g.DrawImage(tmp, new Point(x, y));
                        g.Save();
                    }
                }

            }

            return bmp;
        }


        private static void ApplyText(Mobile m, Graphics g)
        {
            /*StringFormat left = new StringFormat();
            left.Alignment = StringAlignment.Near;

            Font tahoma = new Font("Tahoma", 8.0f, FontStyle.Bold);
            Brush karmaColor = (m.Karma < 0) ? Brushes.Red : (m.Karma > 0) ? Brushes.Blue : Brushes.Yellow;

            string rawTitle = "";//Server.Misc.Titles.ComputeTitle(m, m, true);
            string[] splitTitle = rawTitle.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (splitTitle.Length != 2)
                splitTitle = new string[] { rawTitle, "" };

            TextEntry[] entries = new TextEntry[] { 
                new TextEntry(new PointF(170, 100), tahoma, karmaColor, left, m.Name), 
                new TextEntry(new PointF(170, 128), tahoma, Brushes.Yellow, left, (m.Guild != null) ? "<" + m.Guild.Abbreviation + ">" : "<No Guild>"), 
                new TextEntry(new PointF(170, 156), tahoma, karmaColor, left, "K: " + ((m.Karma != 0) ? m.Karma.ToString("#,#") : "0")), 
                new TextEntry(new PointF(170, 183), tahoma, Brushes.Yellow, left, "F: " + ((m.Fame != 0) ? m.Fame.ToString("#,#") : "0")),
                new TextEntry(new PointF(170, 210), tahoma, Brushes.Yellow, left, "C: " + ((m.Kills != 0) ? m.Kills.ToString("#,#") : "0")), 
                new TextEntry(new PointF(231, 210), tahoma, Brushes.Yellow, left, (m.NetState != null) ? ((m.NetState.Running) ? "[|]" : "[  ]") : "[ ]"), 
                new TextEntry(new PointF(40, 237), tahoma, Brushes.Yellow, left, splitTitle[0].Trim()), 
                new TextEntry(new PointF(40, 248), tahoma, Brushes.Yellow, left, splitTitle[1].Trim())
             };

            for (int i = 0; i < entries.Length; i++)
                entries[i].Draw(g);*/
        }

        internal class TextEntry
        {
            private PointF _Point;
            public PointF Point { get { return _Point; } set { _Point = value; } }

            private Font _Font;
            public Font Font { get { return _Font; } set { _Font = value; } }

            private Brush _Brush;
            public Brush Brush { get { return _Brush; } set { _Brush = value; } }

            private StringFormat _Format;
            public StringFormat Format { get { return _Format; } set { _Format = value; } }

            private string _Text;
            public string Text { get { return _Text; } set { _Text = value; } }

            public TextEntry(PointF point, Font font, Brush brush, StringFormat format, string text)
            {
                _Point = point;
                _Font = font;
                _Brush = brush;
                _Format = format;
                _Text = text;
            }

            public void Draw(Graphics g)
            {
                g.DrawString(_Text, _Font, _Brush, _Point, _Format);
                g.Save();
            }
        }

        private static void HotFix(ref int animID, ref int animHue)
        {
            //if (animID >= 900 && animID <= 999) //Return for AoS Equip
            //   return;

            /*int transID, transHue = animHue;

            transID = TranslateID(animID, ref transHue);

            if (transID != animID)
                animID = transID;

            if (transHue != animHue)
                animHue = transHue;*/
        }

        private static int TranslateID(int id, ref int hue)
        {
            int newID = id;
            Ultima.Animations.Translate(ref newID, ref hue);
            return newID;
        }



    }




}
