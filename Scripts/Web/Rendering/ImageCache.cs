﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.Concurrent;

namespace Server.Web.Rendering
{
    public static class ImageCache
    {
        private static string Directory = "./Data/Images/";
        private static ConcurrentDictionary<string, Bitmap> Cache;

        static ImageCache()
        {
            Cache = new ConcurrentDictionary<string, Bitmap>();

        }

        /// <summary>
        /// Gets an image from the cache or filesystem
        /// </summary>
        public static Bitmap GetImage(string imageName, int width, int height)
        {
            if (Cache.ContainsKey(imageName))
                return Cache[imageName];
            Bitmap image = new Bitmap(new Bitmap(Directory + imageName), width, height);
            Cache.TryAdd(imageName, image);
            if (Cache.ContainsKey(imageName))
                return Cache[imageName];
            return null;
        }

        /// <summary>
        /// Gets an image from the cache or filesystem
        /// </summary>
        public static Bitmap GetImage(string imageName)
        {
            if(Cache.ContainsKey(imageName))
                return Cache[imageName];
            Bitmap image = new Bitmap(Directory + imageName);
            Cache.TryAdd(imageName, image);
            if (Cache.ContainsKey(imageName))
                return Cache[imageName];
            return null;
        }

    }
}
