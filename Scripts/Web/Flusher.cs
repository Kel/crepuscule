using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spike
{

    public class Flusher : Spike.Timer
    {
        private static Flusher fInstance = null;
        public static void SpikeInitialize()
        {
            fInstance = new Flusher();
            fInstance.Start();
        }

        public Flusher() : base(TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(250))
        {

        }

        protected override void OnTick()
        {
            base.OnTick();
            NetState.FlushAll();
        }
    }
}
