using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Spike.Network.Http;
using Spike.ServiceModel;
using Spike.Collections;
using Spike;

namespace Server.Web
{
    [Serializable]
    public class WebSite : IHttpHandler
    {
        private List<IWebHandler> fHandlers;

        #region Constructors
        public WebSite(string virtualDirectory) : this(
            virtualDirectory,
            Path.Combine(WebServer.WebRoot, virtualDirectory.StartsWith("/") ? virtualDirectory.Remove(0, 1) : virtualDirectory)){}
        public WebSite(string virtualDirectory, string localDirectory)
        {
            this.fHandlers = new List<IWebHandler>();
            this.Active = true;
            if (!virtualDirectory.StartsWith("/"))
                virtualDirectory = "/" + virtualDirectory;
            this.Mime = new HttpMimeMap();
            this.LocalDirectory = localDirectory;
            this.VirtualDirectory = virtualDirectory.StartsWith("/") ? virtualDirectory.ToLower() : "/" + virtualDirectory.ToLower();
            this.DefaultPages = new List<string>();

            this.Register(new StaticResourceHandler());
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets/sets whether the website is active or not
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Gets the local (physical) path to the website
        /// </summary>
        public virtual string LocalDirectory { get; protected set; }

        /// <summary>
        /// Gets the virtual directory of the website (e.g: www.mywebsite.com/myVirtualDirectory/
        /// </summary>
        public virtual string VirtualDirectory { get; protected set; }

        /// <summary>
        /// Gets the map of Mime types handled by the website
        /// </summary>
        public virtual HttpMimeMap Mime { get; protected set; }

        /// <summary>
        /// Gets the registered web handlers
        /// </summary>
        public virtual IViewCollection<IWebHandler> Handlers 
        {
            get { return new ReadOnlyList<IWebHandler>(fHandlers); }
        }

        /// <summary>
        /// Gets the list of default pages to look for
        /// </summary>
        public virtual List<string> DefaultPages { get; protected set; }
        #endregion

        #region IHttpHandler Members

        public virtual bool CanHandle(HttpContext context, HttpVerb verb, string url)
        {
            if (!this.Active)
                return false;

            // Handle everything that concerns the website
            return url.StartsWith(this.VirtualDirectory); 
        }


        public virtual void ProcessRequest(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpRequest  request = context.Request;
            HttpResponse response = context.Response;
            HttpVerb verb = context.Request.HttpVerb;
            string path = context.Request.Path;
            string page;
            string query;

            if (!path.StartsWith(this.VirtualDirectory))
            {
                response.Status = "404";
                return;
            }

            // get local path
            path = path.Remove(0, this.VirtualDirectory.Length);
            HttpUtility.ParseUrl(path, out page, out query);
            if (String.IsNullOrEmpty(page))
            {
                for (int i = DefaultPages.Count - 1; i >= 0; --i)
                {
                    string index = Path.Combine(this.LocalDirectory, DefaultPages[i]);
                    if (File.Exists(index))
                    {
                        page = index;
                        break;
                    }
                }
            }

            if (!String.IsNullOrEmpty(page))
            {
                for (int i = fHandlers.Count - 1; i >= 0; --i)
                {
                    IWebHandler handler = fHandlers[i];
                    if (handler.CanHandle(this, context, page))
                    {
                        handler.ProcessRequest(this, context, page, query);
                        return;
                    }
                }
            }

            response.Status = "404";
            return;
        }


        #endregion

        #region WebHandlers Registration
        /// <summary>
        /// Registers a Web handler to handle a particular type of resource
        /// </summary>
        /// <param name="handler">Web handler to register</param>
        public void Register(IWebHandler handler)
        {
            if (!Handlers.Contains(handler))
            {
                handler.OnRegister(this);
                fHandlers.Add(handler);
            }
        }

        /// <summary>
        /// Unregisters a web handler that handles a particular type of resource
        /// </summary>
        public void Unregister(IWebHandler handler)
        {
            if (Handlers.Contains(handler))
            {
                handler.OnUnregister(this);
                fHandlers.Remove(handler);
            }
        }
        #endregion
    }

    #region IWebHandler
    public interface IWebHandler
    {

        /// <summary>
        /// Checks whether the handler can handle an incoming request or not
        /// </summary>
        /// <param name="context">An HttpContext object that provides references to the intrinsic
        /// server objects (for example, Request, Response, Session, and Server) used
        /// to service HTTP requests.</param>
        /// <param name="site">Website which handles the request</param>
        /// <param name="url">Local resource (page name)</param>
        /// <returns></returns>
        bool CanHandle(WebSite site, HttpContext context, string resource);

        /// <summary>
        /// Processes a web request for a given url
        /// </summary>
        /// <param name="site">Website which handles the request</param>
        /// <param name="context">An HttpContext object that provides references to the intrinsic
        /// server objects (for example, Request, Response, Session, and Server) used
        /// to service HTTP requests.</param>
        /// <param name="resource">Resource (e.g: page name) to access</param>
        /// <param name="query">Query parameters</param>
        void ProcessRequest(WebSite site, HttpContext context, string resource, string query);

        /// <summary>
        /// Invoked when the web handler is registered to a website
        /// </summary>
        void OnRegister(WebSite site);

        /// <summary>
        /// Invoked when the web handler is unregistered from a website
        /// </summary>
        void OnUnregister(WebSite site);

    }
    #endregion
}
