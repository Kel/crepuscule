using System;

namespace Server.Web.Csml
{
    public class CsmlScriptBlock
    {
        public static CsmlScriptBlock Parse(string block)
        {
            return new CsmlScriptBlock(block);
        }

        public string Contents { get; private set; }

        private CsmlScriptBlock(string block)
        {
            var ignoreNewLine = _ignoreNextNewLine;

            if (String.IsNullOrEmpty(block))
            {
                Contents = string.Empty;
                return;
            }

            var endOffset = 4;
            if (block.EndsWith("-%>"))
            {
                endOffset = 5;
                _ignoreNextNewLine = true;
            }
            else
            {
                _ignoreNextNewLine = false;
            }

            if (block.StartsWith("<%="))
            {
                var outputLength = block.Length - endOffset - 1;
                if (outputLength < 1)
                {
                    throw new InvalidOperationException("Started a '<%=' block without ending it.");
                }

                var output = block.Substring(3, outputLength).Trim();
                Contents = String.Format("Response.Write({0});", output).Trim();
                return;
            }

            if (block.StartsWith("<%"))
            {
                Contents = block.Substring(2, block.Length - endOffset).Trim();
                return;
            }

            if (ignoreNewLine)
            {
                block = block.Trim();
            }

            block = block.Replace(@"\", @"\\");
            block = block.Replace(Environment.NewLine, "\\r\\n");
            block = block.Replace(@"""", @"\""");

            if (block.Length > 0)
            {
                Contents = string.Format("Response.Write(\"{0}\");", block);
            }
        }

        [ThreadStatic]
        private static bool _ignoreNextNewLine;
    }
}
