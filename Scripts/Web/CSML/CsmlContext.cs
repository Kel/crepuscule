using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using Spike.Network.Http;
using System.IO;
using System.Collections.Specialized;
using CSScriptLibrary;

namespace Server.Web.Csml
{
    public class CsmlContext
    {
        private HttpContext fContext;
        private NameValueCollection fQueryParams;
        private string fDirectory;
        private string fResource;
        private string fQuery;
        private bool fIsRoot = true;
        private static string ReferencedAssemblies = null;

        static CsmlContext()
        {
            /*ReferencedAssemblies = ScriptCompiler
                .GetReferenceAssemblies()
                .Select(assembly => "//css_reference " + assembly + ";" + Environment.NewLine)
                .Aggregate((a,b) => a + b);*/

            ReferencedAssemblies =  "//css_ref System.dll;" + Environment.NewLine;
            ReferencedAssemblies += "//css_ref " + Spike.Kernel.BaseDirectory + "\\Spike.Runtime.dll;" + Environment.NewLine;
            ReferencedAssemblies += "//css_ref " + Spike.Kernel.BaseDirectory + "\\Scripts.CS.dll;" + Environment.NewLine;
            ReferencedAssemblies += "//css_ref " + Spike.Kernel.BaseDirectory + "\\RunUO.exe;" + Environment.NewLine;
            //CSScript.AssemblyResolvingEnabled = true;
            //CSScript.ShareHostRefAssemblies = true;
            //Console.WriteLine("b: " + Spike.Kernel.BaseDirectory);
            //CSScript.GlobalSettings.AddSearchDir(Spike.Kernel.BaseDirectory);
            //Console.WriteLine("b: " + CSScript.GlobalSettings.SearchDirs);
            CSScript.GlobalSettings.TargetFramework = "v4.0";
            //CSScript.GlobalSettings.SearchDirs = Spike.Kernel.BaseDirectory;
        }

        public CsmlContext(HttpContext context, string directory, string resource, string query)
        {
            fContext = context;
            fDirectory = directory;
            fQuery = query;
            fResource = resource;
            

            //fEngine.Runtime.IO.SetOutput(fContext.Response.GetUnderlyingStream(), Encoding.UTF8);
            fQueryParams = !String.IsNullOrEmpty(query) ? HttpUtility.ParseQueryString(query) : new NameValueCollection();



        }

        public HttpContext Context
        {
            get { return fContext; }
            set { fContext = value; }
        }

        public NameValueCollection QueryParams
        {
            get { return fQueryParams; }
            set { fQueryParams = value; }
        }

        /// <summary>
        /// Render RHTML page into the context
        /// </summary>
        /// <param name="pageFile">Renders RHTML page into the context</param>
        public void RenderPage(string pageFile)
        {
            string scriptText = File.ReadAllText(Path.Combine(fDirectory, pageFile));
            if (fIsRoot)
            {
                // Execute builtin definitions in the new scope
                //fBuiltin.Execute(fScope);
                
                fIsRoot = false;
            }
            
            var template = new CsmlTemplate(scriptText);
            var script = ReferencedAssemblies +
                template.ToScript();
            dynamic obj = CSScript.LoadCode(script)
                .CreateObject("*");
            obj.Context = fContext;
            obj.Request = fContext.Request;
            obj.Response = fContext.Response;
            obj.Params = fQueryParams;
            obj.Csml = this;
            obj.Execute();
        }


        /// <summary>
        /// Dumps the object's properties and fields to a string
        /// </summary>
        public string Dump(object o)
        {
            StringBuilder sb = new StringBuilder();

            // Include the type of the object
            System.Type type = o.GetType();
            sb.Append("Type: " + type.Name);

            // Include information for each Field
            sb.Append("<br /><br />Fields:");
            System.Reflection.FieldInfo[] fi = type.GetFields();
            if (fi.Length > 0)
            {
                foreach (FieldInfo f in fi)
                {
                    try
                    {
                        var value = f.GetValue(o);
                        if (value is string)
                        {
                            sb.Append("<br /> " + f.ToString() + " = " + value);
                        }
                        else if (value is IEnumerable)
                        {
                            sb.Append("<br /> " + f.ToString() + " = ");
                            foreach (var v in value as IEnumerable)
                                sb.Append("<br />&nbsp;&nbsp;&nbsp; * " + v.ToString());
                        }
                        else
                        {
                            sb.Append("<br /> " + f.ToString() + " = " + value);
                        }
                    }
                    catch { }
                }
            }
            else
                sb.Append("<br /> None");

            // Include information for each Property
            sb.Append("<br /><br />Properties:");
            System.Reflection.PropertyInfo[] pi = type.GetProperties();
            if (pi.Length > 0)
            {
                foreach (PropertyInfo f in pi)
                {
                    try
                    {
                        var value = f.GetValue(o, null);
                        if (value is string)
                        {
                            sb.Append("<br /> " + f.ToString() + " = " + value);
                        }
                        else if (value is IEnumerable)
                        {
                            sb.Append("<br /> " + f.ToString() + " = ");
                            foreach (var v in value as IEnumerable)
                                sb.Append("<br />&nbsp;&nbsp;&nbsp; * " + v.ToString());
                        }
                        else
                        {
                            sb.Append("<br /> " + f.ToString() + " = " + value);
                        }
                    }
                    catch { }
                }
            }
            else
                sb.Append("<br /> None");

            return sb.ToString();
        }

    }
}
