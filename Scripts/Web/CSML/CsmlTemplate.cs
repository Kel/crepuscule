using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Server.Web.Csml
{

    public class CsmlTemplate
    {
        private readonly string fTemplate;
        private readonly List<string> fUsings = new List<string>()
        {
            "System",
            "System.Collections",
            "System.Collections.Generic",
            "System.Collections.Specialized",
            "System.Text",
            "System.Text.RegularExpressions",
            "Server.Web.Csml",
            "Spike.Network.Http",
            "Server",
            "Server.Mobiles",
            "Server.Items"
        };

        public CsmlTemplate(string templateContents)
        {
            if (templateContents == null)
            {
                throw new ArgumentNullException("templateContents");
            }

            fTemplate = templateContents;
        }

        public void AddUsing(string require)
        {
            fUsings.Add(require);
        }

        public string ToScript()
        {
            var builder = new StringBuilder();
            string contents = fTemplate;

            // Usings
            while (contents.StartsWith("#using "))
            {
                int idx = contents.IndexOf(Environment.NewLine);
                AddUsing(contents.Substring(0, idx ).Remove(0, 7));
                contents = contents.Remove(0, idx + 2);
            }

//            contents = contents.Replace("%SITE%", );
            contents = contents.Replace("%SERVER%", HttpHandlers.ServerBase);

            builder.AppendLine("public class Script{");

            builder.AppendLine("public HttpResponse Response;");
            builder.AppendLine("public HttpRequest Request;");
            builder.AppendLine("public NameValueCollection Params;");
            builder.AppendLine("public HttpContext Context;");
            builder.AppendLine("public CsmlContext Csml;");

            builder.AppendLine("public void Execute(){");

            var scriptBlocks = new Regex("<%.*?%>", RegexOptions.Compiled | RegexOptions.Singleline);
            var matches = scriptBlocks.Matches(contents);

            var currentIndex = 0;

            foreach (Match match in matches)
            {
                var blockBeginIndex = match.Index;
                var csmlBlock = contents.Substring(currentIndex, blockBeginIndex - currentIndex);
                var csBlock = CsmlScriptBlock.Parse(csmlBlock);

                if (!String.IsNullOrEmpty(csBlock.Contents))
                {
                    builder.AppendLine(csBlock.Contents);
                }
                csBlock = CsmlScriptBlock.Parse(match.Value);
                builder.AppendLine(csBlock.Contents);
                currentIndex = match.Index + match.Length;
            }

            if (currentIndex < contents.Length - 1)
            {
                var endBlock = CsmlScriptBlock.Parse(contents.Substring(currentIndex));
                builder.Append(endBlock.Contents);
            }

            builder.AppendLine("}");
            builder.AppendLine("private void Echo(string text){ Response.Write(text); }");
            //builder.AppendLine("private void Include(string page){ Csml.RenderPage(page); }");
            builder.AppendLine("}");

            var finalBuilder = new StringBuilder();
            fUsings.ForEach(require => finalBuilder.AppendLine(string.Format("using {0};", require)));
            finalBuilder.Append(builder.ToString());
            return finalBuilder.ToString();
        }
    }
}
