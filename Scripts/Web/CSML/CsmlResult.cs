using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Server.Web.Csml
{
    public class CsmlResult
    {
        public CsmlResult(bool success, params string[] messages)
        {
            Success = success;
            Messages = new List<string>(messages).AsReadOnly();
        }

        public bool Success { get; private set; }

        public ReadOnlyCollection<string> Messages { get; private set; }
    }
}
