using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Spike.ServiceModel;
using Spike;

namespace Server.Web
{
    public static class WebServer
    {
        private static List<WebSite> fSitesToRegister = new List<WebSite>();
        private static Spike.Lazy<string> fWebRoot = new Spike.Lazy<string>(() =>
        {
            string root = Path.Combine(Kernel.BaseDirectory, "WebRoot/");
            try
            {
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);
            }
            catch { }
            return root;
        });


        public static void SpikeInitialize()
        {
            for (int i = 0; i < fSitesToRegister.Count; ++i)
                RegisterWebSite(fSitesToRegister[i]);
            fSitesToRegister.Clear();
        }

        /// <summary>
        /// Registers a website or marks it to be registered when server starts
        /// </summary>
        public static void RegisterWebSite(WebSite site)
        {
            if(Kernel.HttpHandlersProvider == null)
                fSitesToRegister.Add(site);
            else
                Kernel.HttpHandlersProvider.Register(site);
        }

        /// <summary>
        /// Registers a new website or marks it to be registered when server starts
        /// </summary>
        public static void RegisterNewWebSite(string virtualDirectory, params IWebHandler[] handlers)
        {
            WebSite site = new WebSite(virtualDirectory);
            if(handlers != null && handlers.Length > 0)
            {
                for(int i=0;i<handlers.Length;++i)
                    site.Register(handlers[i]);
            }
            RegisterWebSite(site);
        }

        /// <summary>
        /// Registers a new website or marks it to be registered when server starts
        /// </summary>
        public static void RegisterNewWebSite(string virtualDirectory, string localDirectory, params IWebHandler[] handlers)
        {
            WebSite site = new WebSite(virtualDirectory, localDirectory);
            if (handlers != null && handlers.Length > 0)
            {
                for (int i = 0; i < handlers.Length; ++i)
                    site.Register(handlers[i]);
            }
            RegisterWebSite(site);
        }

        /// <summary>
        /// Root for the web directory
        /// </summary>
        public static string WebRoot
        {
            get { return fWebRoot.Value;}
            set 
            {
                fWebRoot.Value = value;
                try
                {
                    if (!Directory.Exists(fWebRoot.Value))
                        Directory.CreateDirectory(fWebRoot.Value);
                }
                catch { }
            }
        }
    }
}
