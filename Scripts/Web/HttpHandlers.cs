﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Network;
using Server;
using System.IO;
using Server.Web;
using Server.Web.Csml;
using Server.Misc;

namespace Server.Web
{
    public class HttpHandlers
    {
        public static string ServerBase;
        public const string CharactersWebSite = "/chars/";


        public static void SpikeInitialize()
        {
            ServerBase = String.Format("http://{0}:8080", ServerList.Address);

            Spike.Kernel.HttpHandlersProvider.Register(new WebStatus());
            Spike.Kernel.HttpHandlersProvider.Register(new WebStatusImage());

            // lastly, register the web-site
            WebSite web = new WebSite(HttpHandlers.CharactersWebSite);
            web.Register(new CsmlHandler());
            Spike.Kernel.HttpHandlersProvider.Register(web);
        }
    }


}
