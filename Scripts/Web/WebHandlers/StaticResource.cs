using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Spike.Network.Http;

namespace Server.Web
{
    public class StaticResourceHandler : IWebHandler
    {

        public bool CanHandle(WebSite site, HttpContext context, string resource)
        {
            if (context.Request.HttpVerb != HttpVerb.Get)
                return false;

            if (!File.Exists(Path.Combine(site.LocalDirectory, resource)))
                return false;

            var info = new FileInfo(resource);
            if (site.Mime.GetMime(info.Extension) != null)
                return true;
            return false;
        }


        public void ProcessRequest(WebSite site, HttpContext context, string resource, string query)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            var file = new FileInfo(Path.Combine(site.LocalDirectory, resource));
            var mime = site.Mime.GetMime(file.Extension);
            if (!file.Exists || mime == null)
            {
                response.Status = "404";
                return;
            }

            var ifModifiedSince = request.Headers.Get("If-Modified-Since");
            if (!String.IsNullOrEmpty(ifModifiedSince))
            {
                DateTime cached;
                if (DateTime.TryParse(ifModifiedSince, out cached))
                {
                    if (cached >= file.LastWriteTime)
                    {
                        response.Status = "304";
                        return;
                    }
                }
            }

            // Write headers
            response.Headers.Set("Date", DateTime.UtcNow.ToString("R"));
            response.Headers.Set("Last-Modified", file.LastWriteTimeUtc.ToString("R"));
            response.ContentType = mime;

            // Write the resource body
            byte[] data = File.ReadAllBytes(file.FullName);
            response.Write(data, 0, data.Length);
        }


        public void OnRegister(WebSite site)
        {
            site.DefaultPages.Add("index.html");
            site.DefaultPages.Add("index.htm");
        }

        public void OnUnregister(WebSite site)
        {
            site.DefaultPages.Remove("index.html");
            site.DefaultPages.Remove("index.htm");
        }

    }
}
