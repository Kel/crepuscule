using System;
using System.IO;
using System.Text;
using System.Collections;
using Server.Mobiles;
using Server;
using Server.Network;
using Server.Guilds;
using System.Threading;
using System.Globalization;
using Spike.Network.Http;

namespace Server.Web
{
    public class WebStatus : IHttpHandler
    {
        private static TimeZoneInfo EST;
        private static TimeZoneInfo CET;
        public static string HtmlContent;
        public static int PlayersCount = 0;


        private static string Encode(string input)
        {
            StringBuilder sb = new StringBuilder(input);

            sb.Replace("&", "&amp;");
            sb.Replace("<", "&lt;");
            sb.Replace(">", "&gt;");
            sb.Replace("\"", "&quot;");
            sb.Replace("'", "&apos;");

            return sb.ToString();
        }

        public static void GenerateHtml(int players)
        {
            try
            {
                PlayersCount = players;
                using (StringWriter op = new StringWriter())
                {
                    op.WriteLine("<image src='{0}/status/image' />", HttpHandlers.ServerBase);
                    op.WriteLine("<br />");

                    op.WriteLine("Il y a actuellement " + PlayersCount);
                    op.WriteLine(" joueurs connect&eacute;s, rejoignez-les!");

                    //Set the time zone information to US Mountain Standard Time 
                    if(EST == null)
                        EST = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    if(CET == null)
                        CET = TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time");

                    if(CET != null)
                        op.WriteLine("<br />Date de derni&egrave;re mise &agrave; jour (FR) : " + TimeZoneInfo.ConvertTime(DateTime.Now, CET).ToString());

                    if(EST != null)
                        op.WriteLine("<br />Date de derni&egrave;re mise &agrave; jour (QC) : " + TimeZoneInfo.ConvertTime(DateTime.Now, EST).ToString());

                    HtmlContent = op.ToString();
                }
            }
            catch
            { }
        }

        #region IHttpHandler Members

        public bool CanHandle(HttpContext context, HttpVerb verb, string url)
        {
            return verb == HttpVerb.Get && url == "/status";
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Write(HtmlContent);
        }

        #endregion
    }
}
