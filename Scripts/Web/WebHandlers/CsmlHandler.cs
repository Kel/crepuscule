using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Spike.Network.Http;
using Server.Web.Csml;

namespace Server.Web
{
    public class CsmlHandler : IWebHandler
    {

        public bool CanHandle(WebSite site, HttpContext context, string resource)
        {
            if (!File.Exists(Path.Combine(site.LocalDirectory, resource)))
                return false;

            if (resource.EndsWith(".csml"))
                return true;

            return false;
        }


        public void ProcessRequest(WebSite site, HttpContext context, string resource, string query)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            var file = new FileInfo(Path.Combine(site.LocalDirectory, resource));
            if (file.Exists)
            {
                // create a context & render the page
                var rhtml = new CsmlContext(context, file.DirectoryName, resource, query);
                rhtml.RenderPage(file.Name);
            }
        }

        public void OnRegister(WebSite site)
        {
            site.DefaultPages.Add("index.csml");
        }

        public void OnUnregister(WebSite site)
        {
            site.DefaultPages.Remove("index.csml");
        }

    }
}
