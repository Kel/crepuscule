using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Threading;
using System.Globalization;
using Spike.Network.Http;
using System.Drawing;
using Server.Web.Rendering;

namespace Server.Web
{
    public class WebStatusImage : Spike.Timer, IHttpHandler
    {
        private static object Lock = new object();
        private static DateTime ModifiedTime = DateTime.Now;
        private static byte[] StatusImage = null;

        public WebStatusImage() : base(TimeSpan.FromSeconds(5.0), TimeSpan.FromMinutes(1.0))
        {
            Priority = Spike.TimerPriority.FiveSeconds;
        }

        protected override void OnTick()
        {
            lock (Lock)
            {
                try
                {
                    var store = PaperdollGenerator.GetCurrentStore();
                    var yext = (50 * (int)Math.Floor((double)store.Values.Count / 5));
                    var bmp = new Bitmap(550, 320 + yext);
                    using (Graphics g = Graphics.FromImage(bmp))
                    using (SolidBrush black = new SolidBrush(Color.Black))
                    {
                        Bitmap bck = ImageCache.GetImage("StatusBck.png");
                        g.FillRectangle(black, 0, 0, bmp.Width, bmp.Height);
                        g.DrawImage(bck, 0, 0, 550, 320);
                        int x = 0;
                        int y = 50;
                        int i = 0;
                        int players = 0;
                        bool even = false;
                        foreach (var doll in store.Values)
                        {
                            if (doll.AccessLevel > AccessLevel.Player)
                                continue;
                            players++;
                            try
                            {
                                i++;
                                Bitmap image = PaperdollRenderer.GeneratePaperdoll(doll);
                                if (((i % 6 == 0) && !even) || ((i % 5 == 0) && even))
                                {
                                    y += 50;
                                    even = !even;
                                    x = even ? 50 : 0;
                                    i = 1;
                                }

                                g.DrawImage(image, new Point(x, y));
                                x += 90;
                            }
                            catch (Exception ex) { Console.WriteLine(ex.Dump()); }
                        }

                        WebStatus.GenerateHtml(players);
                        g.Save();
                    }

                    ModifiedTime = DateTime.Now;
                    StatusImage = bmp.GetBytes();

                }
                catch { StatusImage = null; }
            }
        }
        
        public bool CanHandle(HttpContext context, HttpVerb verb, string url)
        {
            return verb == HttpVerb.Get && url == "/status/image";
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            if(StatusImage == null)
            {
                response.Status = "404";
                return;
            }

            /*var ifModifiedSince = request.Headers.Get("If-Modified-Since");
            if (!String.IsNullOrEmpty(ifModifiedSince))
            {
                DateTime cached;
                if (DateTime.TryParse(ifModifiedSince, out cached))
                {
                    if (cached >= ModifiedTime)
                    {
                        response.Status = "304";
                        return;
                    }
                }
            }*/

            // Write headers
            response.ContentType = "image/png";
            //response.Headers.Set("Date", DateTime.UtcNow.ToString("R"));
            //response.Headers.Set("Last-Modified", ModifiedTime.ToString("R"));
            response.Write(StatusImage, 0, StatusImage.Length);

            
        }

        public static void SpikeInitialize()
        {
            new WebStatusImage().Start();
        }

    }
}
