﻿using System;
using Server;
using Server.Misc;
using Server.Network;

namespace Server.Misc
{
    public class RestartSoon
    {
        public static void Initialize()
        {
            CommandSystem.Register("RestartSoon", AccessLevel.Administrator, new CommandEventHandler(RestartSoon_OnCommand));
        }

        [Usage("RestartSoon")]
        [Description("Restart dès qu'il n'y a plus personne")]
        public static void RestartSoon_OnCommand(CommandEventArgs e)
        {
            DoRestartSoon(e.Mobile);
        }

        public static void DoRestartSoon(Mobile mobile)
        {
            if (timer != null)
            {
                mobile.SendMessage("le Restart est déjà programmé.");
            }
            else
            {
                timer = new RestartSoonTimer(mobile);
                timer.Start();
                mobile.SendMessage("Restart programmé.");
            }
        }

        private static RestartSoonTimer timer;

        private class RestartSoonTimer : Timer
        {
            private Mobile mobile;

            public RestartSoonTimer(Mobile m)
                : base(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1))
            {
                mobile = m;
            }

            protected override void OnTick()
            {
                if (NetState.Instances.Count == 0)
                {
                    AutoRestart.DoAutoRestart(mobile);
                }
            }
        }
    }
}