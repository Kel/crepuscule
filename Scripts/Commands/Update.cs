using System;
using System.IO;
using Server;
using System.Text;
using System.Collections;
using System.Net;
using Server.Accounting;
using Server.Mobiles;
using Server.Items;
using Server.Menus;
using Server.Menus.Questions;
using Server.Menus.ItemLists;
using Server.Network;
using Server.Spells;
using Server.Targeting;
using Server.Targets;
using Server.Gumps;
using System.Diagnostics;
using SharpSvn;
using System.Collections.ObjectModel;

namespace Server.Scripts.Commands
{
	public class UpdateCommand
	{
		
		public static void Initialize()
		{
            Server.Commands.Register("Update", AccessLevel.Administrator, new CommandEventHandler(On_UpdateCommand));
		}
		
		[Usage( "Update" )]
		[Description( "Updates the server's scripts." )]
        private static void On_UpdateCommand(CommandEventArgs e)
		{
            UpdateScripts();
		
		}

        public static void UpdateScripts()
        {
            int MsgHue = 7;
            BroadcastMessage(AccessLevel.Player, MsgHue, "La mise � jour des scripts a �t� initialis�e, il est possible que le serveur va �tre red�marr� dans quelques instants. Merci de votre compr�hension.");
            //Misc.AutoSave.Save();

            try
            {
                string errorOutput = "";
                string output = CommandLineHelper.Run("update.bat", @"", out errorOutput);

                if (errorOutput.Trim().Length > 0)
                    BroadcastMessage(AccessLevel.Counselor, MsgHue, String.Format("Erreurs: {0}", errorOutput));
                BroadcastMessage(AccessLevel.Counselor, MsgHue, String.Format("Mise � Jour: {0}", output));

                BroadcastMessage(AccessLevel.Player, MsgHue, "Mise � jour des script termin�e, en attente de red�marrage...");

                string FileName = "data/revision.cfg";
                int LastRevision = 0;

                if (File.Exists(FileName))
                {
                    var reader = File.OpenText("data/revision.cfg");
                    LastRevision = int.Parse(reader.ReadLine());
                    reader.Close();
                }
                else
                {
                    var writer = File.CreateText(FileName);
                    writer.WriteLine(0);
                    writer.Close();
                }

                try
                {
                    using (SvnClient client = new SvnClient())
                    {
                        Collection<SvnLogEventArgs> list;
                        client.Authentication.DefaultCredentials = new NetworkCredential("scribe", "t1x6a7");
                        int NewLastRevision = LastRevision;

                        SvnLogArgs la = new SvnLogArgs(new SvnRevisionRange(new SvnRevision(LastRevision), new SvnRevision(DateTime.Now)));
                        client.GetLog(new Uri("svn://localhost/crepuscule1"), la, out list);

                        string Message = "";
                        foreach (SvnLogEventArgs a in list)
                        {
                            if (a.Revision == LastRevision)
                                continue;

                            var Text = String.Format("{0}<br />", String.Format("Revision {0} par {1} [{2}]", a.Revision, a.Author, a.Time.ToLongTimeString()));
                            Text += String.Format(" {0}<br />", a.LogMessage);

                            foreach (var path in a.ChangedPaths)
                            {
                                Text += String.Format(" - {0}<br />", path.Path.Remove(0, path.Path.LastIndexOf("/") + 1));
                            }

                            Text = Text.Replace("'", @"\'");
                            Message = String.Format("{0}<br /><br />{1}", Text, Message);
                            NewLastRevision = (int)a.Revision;
                        }

                        string TopicName = String.Format("Mise � Jour {0}", NewLastRevision).Replace("'", @"\'");
                        File.WriteAllText(FileName, NewLastRevision.ToString());

                        if (LastRevision < NewLastRevision)
                            CrepusculeWeb.WS.Service.PostWithScribe("PASSWORD", "141", TopicName, Message);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }



            }
            catch (Exception ex)
            {
                BroadcastMessage(AccessLevel.Counselor, MsgHue, "Une erreur est survenue: " + ex.Message);
            }
        }


        private static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;

                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }
	}
}
