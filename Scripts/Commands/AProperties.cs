/* --------------------------->
 * Advanced properties script by Kaon 
 *  & Lots of heavyweight modifications by Kel :)
 * Version 2.0
 * Creation : 28.03.2007
 * Revision : 01.05.2010
 * --------------------------->
 */
using System;
using System.Reflection;
using System.Collections;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Gumps;
using CPA = Server.CommandPropertyAttribute;

using Server.GenericCommands;
using Server.Scripts.Commands;

namespace Server
{
	public class AProperties
	{
        [CallPriority(10)]
        public static void Initialize()
		{
            Server.Commands.Register("Props", AccessLevel.Counselor, new CommandEventHandler( Props_OnCommand ) );
		}

		private class PropsTarget : Target
		{
			public PropsTarget() : base( -1, true, TargetFlags.None )
			{
			}

			protected override void OnTarget( Mobile from, object o )
			{
				if ( !Server.GenericCommands.BaseCommand.IsAccessible( from, o ) )
					from.SendMessage( "That is not accessible." );
				else
					from.SendGump( new APropertiesGump( from, o ) );
			}
		}

		[Usage( "Props [serial]" )]
		[Description( "Opens a menu where you can view and edit all properties of a targeted (or specified) object." )]
		private static void Props_OnCommand( CommandEventArgs e )
		{
			if ( e.Length == 1 )
			{
				IEntity ent = World.FindEntity( e.GetInt32( 0 ) );

				if ( ent == null )
					e.Mobile.SendMessage( "No object with that serial was found." );
				else if ( !Server.GenericCommands.BaseCommand.IsAccessible( e.Mobile, ent ) )
					e.Mobile.SendMessage( "That is not accessible." );
				else
					e.Mobile.SendGump( new APropertiesGump( e.Mobile, ent ) );
			}
			else
			{
				e.Mobile.Target = new PropsTarget();
			}
		}

        public static string SetDirect(Mobile from, object logObject, object obj, PropertyInfo prop, string givenName, object toSet, bool shouldLog, int index)
        {
            try
            {
                if (toSet is AccessLevel)
                {
                    AccessLevel newLevel = (AccessLevel)toSet;
                    AccessLevel reqLevel = AccessLevel.Administrator;

                    /*if (newLevel == AccessLevel.Administrator)
                        reqLevel = AccessLevel.Developer;
                    else if (newLevel >= AccessLevel.Developer)
                        reqLevel = AccessLevel.Owner;*/

                    if (from.AccessLevel < reqLevel)
                        return "You do not have access to that level.";
                }

                if (shouldLog)
                    CommandLogging.LogChangeProperty(from, logObject, givenName + (index >=0 ? "["+index+"]":""), toSet == null ? "(-null-)" : toSet.ToString());

                if (index >= 0)
                {
                    object[] indexes = new object[] { index };
                    prop.SetValue(obj, toSet, indexes);
                }
                else prop.SetValue(obj, toSet, null);
                return "Property has been set.";
            }
            catch
            {
                return "An exception was caught, the property may not be set.";
            }
        }
	}
}