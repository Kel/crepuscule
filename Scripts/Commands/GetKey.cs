using System;
using System.IO;
using Server;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Server.Accounting;
using Server.Mobiles;
using Server.Items;
using Server.Menus;
using Server.Menus.Questions;
using Server.Menus.ItemLists;
using Server.Network;
using Server.Spells;
using Server.Targeting;
using Server.Targets;
using Server.Gumps;
using System.Diagnostics;
using SharpSvn;
using System.Collections.ObjectModel;

namespace Server.Scripts.Commands
{
	public class GetKeyCommand
	{
		
		public static void Initialize()
		{
            Server.Commands.Register("GetKey", AccessLevel.Administrator, new CommandEventHandler(On_GetKeyCommand));
		}
		
		[Usage( "GetKey [KeyValue]" )]
		[Description( "Reveal position of all keys with this keyvalue" )]
        private static void On_GetKeyCommand(CommandEventArgs e)
		{
            // Get mobile
            Mobile from = e.Mobile;
            uint keyvalue;

            // Check argument
            if (e.Arguments.Length != 1)
            {
                // We must have exactly one argument
                from.SendMessage("Illegal usage. You should enter a keyvalue: .GetKey [KeyValue]");
                return;
            }
            else
            {
                try
                {
                    // This argument must be a valid keyvalue
                    keyvalue = uint.Parse(e.Arguments[0]);
                } catch (Exception exception)  {
                    from.SendMessage("Invalid key value, cannot parse it! Should be between 0 and 4294967295");
                    return;
                }
            }

            from.SendMessage("Looking for all key with keyvalue " + keyvalue);

            // Initialize collections
            List<Key> keys = new List<Key>();
            List<DwsKeyRing> keyrings = new List<DwsKeyRing>();
            
            foreach (Object item in World.Items.Values)
            {
                if (item is Key && ((Key) item).KeyValue == keyvalue && ((Key) item).Parent != null)
                {
                    keys.Add((Key) item);
                }

                if (item is DwsKeyRing && ((DwsKeyRing) item).SearchForKey(keyvalue))
                {
                    keyrings.Add((DwsKeyRing)item);
                }
            }

            // Key part
            from.SendMessage(String.Format("{0} key(s) found.", keys.Count));
            foreach (Key key in keys)
            {
                from.SendMessage(WhereIsBryan(key));
            }

            // Key ring part
            from.SendMessage(String.Format("{0} key ring(s) found.", keyrings.Count));
            foreach (DwsKeyRing keyring in keyrings)
            {
                from.SendMessage(WhereIsBryan(keyring));
            }
		}

        private static string WhereIsBryan(Item item)
        {
            // Bryan is in the kitchen !
            string output = "";

            while (item != null && item.Parent is Item)
            {
                item = ((Item) item.Parent);
            }

            if (item.Parent is RacePlayerMobile)
            {
                RacePlayerMobile player = (RacePlayerMobile)item.Parent;
                output = String.Format("On player (Account: {0}, Char: {1}, X: {2}, Y: {3}, Z: {4})", player.Account.ToString(), player.PlayerName, player.X, player.Y, player.Z); 
            }
            else if (item.Parent is Mobile)
            {
                Mobile parent = (Mobile)item.Parent;
                output = String.Format("On mobile (X: {0}, Y: {1}, Z: {2})", parent.X, parent.Y, parent.Z);
            }
            else if (item is Key)
            {
                output = String.Format("On map (X:{0}, Y:{1}, Z:{2})", item.X, item.Y, item.Z);
            }
            else
            {
                output = String.Format("In container (X:{0}, Y:{1}, Z:{2})", item.X, item.Y, item.Z);
            }

            return output;
        }

	}
}
