/////////////////////////////////
// Chuchotter                  //
// Scripteur:  Apoc            //
// Serveur: Cr�puscule         //
// 8 Mars 2008                 //
/////////////////////////////////

using System;
using System.Collections;
using Server;
using Server.Gumps;
using Server.Targeting;
using Server.Mobiles;
using Server.Network;
using Server.Scripts.Commands;

namespace Server.Misc
{
    public class ChuchotterCommand
    {
        private static string Msg;
        public static void Initialize()
        {
            Server.Commands.Register("chut", AccessLevel.Player, new CommandEventHandler(Chut_OnCommand));
        }

        [Usage("chut <text>")]
        [Description("Chuchotter tout en restant cach� si c'est le cas.")]
        private static void Chut_OnCommand(CommandEventArgs e)
        {
            PlayerMobile from = ((PlayerMobile)e.Mobile);

            AlertMessage(from, 1);
            double srcSkill = from.Skills[SkillName.Hiding].Value;
            int range = (int)(srcSkill / 20.0);
            Msg = e.ArgString;
            from.Target = new ChutTarget(Msg, range);
        }

        private static void AlertMessage(Mobile from, int Message)
        {
            string texte = null;

            switch (Message)
            {
                case 0: texte = "Vous ne pouvez chuchottez qu'aux �tres humains!"; break;
                case 1: texte = "� qui voulez vous chuchotter?"; break;
                case 3: texte = "Vous chuchottez."; break;
                case 4: texte = "Quelqu'un vous chuchotte tout pr�t:"; break;
                case 5: texte = "Vous entendez quelqu'un chuchotter tout pr�t:"; break;
                case 6: texte = "Vous parlez trop haut et �tes d�couvert!"; break;
                case 7: texte = "Vous �tes trop loin pour chuchotter!"; break;
            }

            if (from != null && texte != null)
                from.SendMessage(texte);
        }


        private class ChutTarget : Target
        {
            private static int range;
            public ChutTarget(string Msg, int range)
                : base(range, false, TargetFlags.None)
            {

            }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(targ is PlayerMobile)) AlertMessage(from, 0);
                else
                {
                    PlayerMobile target = ((PlayerMobile)targ);
                    if (from.Skills[SkillName.Hiding].Value < Utility.RandomMinMax(1, 100))
                    {
                        AlertMessage(from, 6);
                        from.RevealingAction();
                    }
                    else AlertMessage(from, 3);

                    AlertMessage(target, 4);
                    target.SendMessage(170, Msg);
                    SaySomething(from, Msg);
                }
            }

            protected override void OnTargetOutOfRange(Mobile from, object targ)
            {
                if (!(targ is PlayerMobile)) AlertMessage(from, 0);
                else AlertMessage(from, 7);
            }
        }

        private static void SaySomething(Mobile from, string texte)
        {
            IPooledEnumerable clientsInRange = from.GetClientsInRange(5);
            foreach (Server.Network.NetState state in clientsInRange)
            {
                Mobile mobile = state.Mobile;
                if (mobile is RacePlayerMobile)
                {
                    RacePlayerMobile mob = mobile as RacePlayerMobile;
                    if (mob != null && mob.AccessLevel >= AccessLevel.GameMaster && mob.AccessLevel > from.AccessLevel)
                    {
                        mob.Send (new UnicodeMessage(from.Serial, from.Body, MessageType.Regular, from.SpeechHue, 3, from.Language, ((RacePlayerMobile)from).PlayerName, String.Format("[Chuchotte]: {0}", texte)));
                    }
                    if (mob != null && mob.Skills[SkillName.DetectHidden].Value > from.Skills[SkillName.Hiding].Value)
                    {
                        AlertMessage(mob, 5);
                        mob.SendMessage(170, texte);
                    }
                }
            }

        }
    }
}
