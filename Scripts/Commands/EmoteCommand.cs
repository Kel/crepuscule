//*******************************//
// Fichier : Emote.cs            //
// Auteur  : Concepteur Kel      //
// Version : 1.0                 //
//*******************************//

using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Gumps;

namespace Server.Scripts.Commands
{
	public enum EmotePage 
	{ 
		P1,
		P2,
		P3,
		P4,
	}
	public class Emote
	{	
		public static void Initialize()
		{
			Server.Commands.Register( "emote", AccessLevel.Player, new CommandEventHandler( Emote_OnCommand ) );
		}

	  	[Usage( "<sound>" )] 
	    [Description( "Emotes avec les sons")] 
		public static void Emote_OnCommand( CommandEventArgs e )
		{
			Mobile pm = e.Mobile;
        	 	string em = e.ArgString.Trim();
			int SoundInt;
			switch( em )
			{
				case "ah":
					SoundInt = 1;
					break;
				case "ahha":
					SoundInt = 2;
					break;					
				case "clap":
					SoundInt = 3;
					break;				
				case "renifle":
					SoundInt = 4;
					break;					
				case "salue":
					SoundInt = 5;
					break;
				case "voix":
					SoundInt = 8;
					break;
				case "toux":
					SoundInt = 9;
					break;
				case "pleure":
					SoundInt = 10;
					break;
				case "tombe":					
					SoundInt = 11;
					break;
				case "prout":
					SoundInt = 12;
					break;
				case "rirenerveux":
					SoundInt = 14;
					break;
				case "hey":
					SoundInt = 17;
					break;
				case "hic":
					SoundInt = 18;
					break;
				case "huh":
					SoundInt = 19;
					break;
				case "bisou":
					SoundInt = 20;
					break;
				case "rit":
					SoundInt = 21;
					break;
				case "oh":
					SoundInt = 23;
					break;
				case "oooh":
					SoundInt = 24;
					break;
				case "oops":
					SoundInt = 25;
					break;
				case "vomit":
					SoundInt = 26;
					break;
				case "frappe": 					
					SoundInt = 27;
					break;
				case "crie":
					SoundInt = 28;
					break;
				case "shush":
					SoundInt = 29;
					break;
				case "soupir":
					SoundInt = 30;
					break;
				case "claque":
					SoundInt = 31;
					break;
				case "eternue":
					SoundInt = 32;
					break;
				case "ronfle":
					SoundInt = 34;
					break;
				case "crache":
					SoundInt = 35;
					break;
				case "langue":
					SoundInt = 36;
					break;
				case "pied":
					SoundInt = 37;
					break;
				case "siffle":
					SoundInt = 38;
					break;
				case "woohoo":
					SoundInt = 39;
					break;
				case "baille":
					SoundInt = 40;
					break;
				case "hurle":
					SoundInt = 42;
					break;				
				default:
					SoundInt = 0;
					e.Mobile.SendMessage( "Format: emote \"<text>\"" );
					e.Mobile.SendMessage( "Possibilites : ah, ahha, clap, renifle, salue, voix, toux, pleure, tombe, prout, rirenerveux, hey, hic, huh, bisou, rit, oh, oooh, oops, vomit, frappe, crie, shush, soupir, claque, eternue, ronfle, crache, langue, pied, siffle, woohoo, baille, hurle" );
					break;
			}
			if ( SoundInt > 0 )
				new ESound( pm, SoundInt );
		} 
	}
	public class ItemRemovalTimer : Timer 
	{ 
		private Item i_item; 
		public ItemRemovalTimer( Item item ) : base( TimeSpan.FromSeconds( 10.0 ) ) 
		{ 
			Priority = TimerPriority.OneSecond; 
			i_item = item; 
		} 

		protected override void OnTick() 
		{ 
		        if (( i_item != null ) && ( !i_item.Deleted )) 
			        i_item.Delete(); 
		} 
	} 

	public class Puke: CrepusculeItem 
	{ 
		[Constructable] 
		public Puke() : base( Utility.RandomList( 0xf3b, 0xf3c ) ) 
		{ 
			Name = "Tas de vomi"; 
			Hue = 0x557; 
			Movable = false; 

			ItemRemovalTimer thisTimer = new ItemRemovalTimer( this ); 
			thisTimer.Start(); 

		} 

		public override void OnSingleClick( Mobile from ) 
		{ 
			this.LabelTo( from, this.Name ); 
		} 
  
		public Puke( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize(GenericWriter writer) 
		{ 
			base.Serialize( writer ); 
			writer.Write( (int) 0 ); 
		} 

		public override void Deserialize(GenericReader reader) 
		{ 
			base.Deserialize( reader ); 
			int version = reader.ReadInt(); 

			this.Delete(); 
		} 
	}
	
	public class ESound
	{
		public ESound( Mobile pm, int SoundMade )
		{
			switch( SoundMade )
			{
				case 1:
					pm.PlaySound( pm.Female ? 778 : 1049 );
					pm.Say( "Ah!" );
					break;
				case 2:
					pm.PlaySound( pm.Female ? 779 : 1050 );
					pm.Say( "ah ha!" );
					break;
				case 3:
					pm.PlaySound( pm.Female ? 780 : 1051 );
					pm.Emote( "*clap* *clap*" );
					break;
				case 4:
					pm.PlaySound( pm.Female ? 781 : 1052 );
					pm.Emote( "*renifle*" );				
					if ( !pm.Mounted )
						pm.Animate( 34, 5, 1, true, false, 0 );
					break;
				case 5:
					pm.Emote( "*salue*" );
					if ( !pm.Mounted )
						pm.Animate( 32, 5, 1, true, false, 0 );
					break;
				case 8:
					pm.PlaySound( pm.Female ? 748 : 1055 );
					pm.Emote( "*s'�claircit la voix*" );
					if ( !pm.Mounted )
						pm.Animate( 33, 5, 1, true, false, 0 );
					break;
				case 9:
					pm.PlaySound( pm.Female ? 785 : 1056 );
					pm.Emote( "*tousse*" );				
					if ( !pm.Mounted )
						pm.Animate( 33, 5, 1, true, false, 0 );
					break;
				case 10:
					pm.PlaySound( pm.Female ? 787 : 1058 );
					pm.Emote( "*pleure*" );
					break;
				case 11:
					pm.PlaySound( pm.Female ? 791 : 1063 );
					pm.Emote( "*tombe*" );
					if ( !pm.Mounted )
						pm.Animate( 22, 5, 1, true, false, 0 );
					break;
				case 12:
					pm.PlaySound( pm.Female ? 792 : 1064 );
					pm.Emote( "*degage une odeur degeulasse, sortant de derriere*" );
					break;
				case 14:
					pm.PlaySound( pm.Female ? 794 : 1066 );
					pm.Emote( "*rit nerveusement*" );
					break;
				case 17:
					pm.PlaySound( pm.Female ? 797 : 1069 );
					pm.Say( "hey!" );
					break;
				case 18:
					pm.PlaySound( pm.Female ? 798 : 1070 );
					pm.Emote( "*hic*" );
					break;
				case 19:
					pm.PlaySound( pm.Female ? 799 : 1071 );
					pm.Say( "huh?" );
					break;
				case 20:
					pm.PlaySound( pm.Female ? 800 : 1072 );
					pm.Emote( "*bisou*" );
					break;
				case 21:
					pm.PlaySound( pm.Female ? 801 : 1073 );
					pm.Emote( "*rit*" );
					break;
				case 23:
					pm.PlaySound( pm.Female ? 803 : 1075 );
					pm.Say( "oh!" );
					break;
				case 24:
					pm.PlaySound( pm.Female ? 811 : 1085 );
					pm.Say( "oooh" );
					break;
				case 25:
					pm.PlaySound( pm.Female ? 812 : 1086 );
					pm.Say( "oops" );
					break;
				case 26:
					pm.PlaySound( pm.Female ? 813 : 1087 );
					pm.Emote( "*vomit*" );
		            		if ( !pm.Mounted ) 
              					pm.Animate( 32, 5, 1, true, false, 0 ); 
            				Point3D p = new Point3D( pm.Location ); 
		            		switch( pm.Direction ) 
            				{ 
               					case Direction.North: 
							p.Y--; break;							 
		               			case Direction.South: 
							p.Y++; break;							 
               					case Direction.East: 
                  					p.X++; break;							 
		               			case Direction.West: 
                		  			p.X--; break; 							
               					case Direction.Right: 
		                  			p.X++; p.Y--; break;
               					case Direction.Down: 
                  					p.X++; p.Y++; break;
		               			case Direction.Left: 
                		  			p.X--; p.Y++; break;
               					case Direction.Up: 
		                  			p.X--; p.Y--; break;
		               			default: 
                		  			break; 
					} 
       				 	p.Z = pm.Map.GetAverageZ( p.X, p.Y ); 
                    
		            		Puke puke = new Puke(); 
            				puke.Map = pm.Map; 
		            		puke.Location = p; 
					break;
				case 27:
					pm.PlaySound( 315 );
					pm.Emote( "*frappe*" );
					if ( !pm.Mounted )
						pm.Animate( 31, 5, 1, true, false, 0 );
					break;
				case 28:
					pm.PlaySound( pm.Female ? 814 : 1088 );
					pm.Say( "ahhhh!" );
					break;
				case 29:
					pm.PlaySound( pm.Female ? 815 : 1089 );
					pm.Say( "shhh!" );
					break;
				case 30:
					pm.PlaySound( pm.Female ? 816 : 1090 );
					pm.Emote( "*soupir*" );
					break;
				case 31:
					pm.PlaySound( 948 );
					pm.Emote( "*claque*" );
					if ( !pm.Mounted )
						pm.Animate( 11, 5, 1, true, false, 0 );
					break;
				case 32:
					pm.PlaySound( pm.Female ? 817 : 1091 );
					pm.Emote( "*atchoum*" );
					if ( !pm.Mounted )
						pm.Animate( 32, 5, 1, true, false, 0 );
					break;

				case 34:
					pm.PlaySound( pm.Female ? 819 : 1093 );
					pm.Emote( "*ronfle*" );
					break;
				case 35:
					pm.PlaySound( pm.Female ? 820 : 1094 );
					pm.Emote( "*crache*" );
					if ( !pm.Mounted )
						pm.Animate( 6, 5, 1, true, false, 0 );
					break;
				case 36:
					pm.PlaySound( 792 );
					pm.Emote( "*tire la langue*" );
					break;
				case 37:
					pm.PlaySound( 874 );
					pm.Emote( "*tape du pied*" );
					if ( !pm.Mounted )
						pm.Animate( 38, 5, 1, true, false, 0 );
					break;
				case 38:
					pm.PlaySound( pm.Female ? 821 : 1095 );	
					pm.Emote( "*siffle*" );
					if ( !pm.Mounted )
						pm.Animate( 5, 5, 1, true, false, 0 );
					break;
				case 39:
					pm.PlaySound( pm.Female ? 783 : 1054 );
					pm.Say( "woohoo!" );
					break;
				case 40:
					pm.PlaySound( pm.Female ? 822 : 1096 );
					pm.Emote( "*baille*" );
					if ( !pm.Mounted )
						pm.Animate( 17, 5, 1, true, false, 0 );
					break;
				case 42:
					pm.PlaySound( pm.Female ? 823 : 1098 );
					pm.Emote( "*hurle*" );
					break;
			}
		}
	}
}
