using System;
using Server;
using Server.Targeting;

namespace Server.Scripts.Commands
{
	public class CataHandlers
	{
		public static void Initialize()
		{
			Server.Commands.Register( "CataDis", AccessLevel.GameMaster, new CommandEventHandler( CataHide_OnCommand ) );
			Server.Commands.Register( "CataApp", AccessLevel.GameMaster, new CommandEventHandler( CataUnhide_OnCommand ) );
		}

		private class SetCataHiddenTarget : Target
		{
			public EffectTimer m_Timer;

			private bool m_Value;

			public SetCataHiddenTarget( bool value ) : base( 18, false, TargetFlags.None ) 
			{ 
				m_Value = value; 
			} 

			protected override void OnTarget( Mobile from, object targeted )
			{
				if( targeted is Mobile )
				{
					Mobile m = (Mobile)targeted;

					CommandLogging.WriteLine( from, "{0} {1} {2} {3}", from.AccessLevel, CommandLogging.Format( from ), m_Value ? "CataHiding" : "CataAppearance", CommandLogging.Format( m ) );
					m.PlaySound( 520 );
					m.PlaySound( 525 );
                    
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z ), m.Map, 0x3709, 17 );
					Effects.SendLocationEffect( new Point3D( m.X - 1, m.Y, m.Z ), m.Map, 0x3709, 17 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z ), m.Map, 0x3709, 17 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y - 1, m.Z ), m.Map, 0x3709, 17 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y, m.Z ), m.Map, 0x3709, 17 );
					
					DateTime now = DateTime.Now;

					m_Timer = new EffectTimer( now, m );
					m_Timer.Start();
					m.Hidden = m_Value;
				}
			}
		
		}

		public class EffectTimer : Timer
		{
			public Mobile m;
            public int inc;

			public EffectTimer( DateTime time, Mobile from ) : base( TimeSpan.FromSeconds( 0.1 ), TimeSpan.FromSeconds( 0.1 ) )
			{
				Priority = TimerPriority.FiftyMS;
				m = from;
				inc = 0;
			}
			
			protected override void OnTick()
			{
				inc++;
				
				Effects.SendLocationEffect( new Point3D( m.X + inc, m.Y, m.Z ), m.Map, 0x3709, 17 );
				Effects.SendLocationEffect( new Point3D( m.X - inc, m.Y, m.Z ), m.Map, 0x3709, 17 );
				Effects.SendLocationEffect( new Point3D( m.X, m.Y + inc, m.Z ), m.Map, 0x3709, 17 );
				Effects.SendLocationEffect( new Point3D( m.X, m.Y - inc, m.Z ), m.Map, 0x3709, 17 );
				Effects.SendLocationEffect( new Point3D( m.X + inc, m.Y - inc, m.Z ), m.Map, 0x3709, 17 );
				Effects.SendLocationEffect( new Point3D( m.X - inc, m.Y + inc, m.Z ), m.Map, 0x3709, 17 );
                
				if( inc >= 5 )
                    this.Stop();
			}
		}
 
		[Usage( "CataDis" )] 
		[Description( "Makes a targeted mobile disappear in a storm of effects." )] 

		public static void CataHide_OnCommand( CommandEventArgs e ) 
		{ 
			e.Mobile.Target = new SetCataHiddenTarget( true ); 
		}

		[Usage( "CataApp" )]
		[Description( "Makes a targeted mobile appear in a storm of fire." )]

		public static void CataUnhide_OnCommand( CommandEventArgs e )
		{
			e.Mobile.Target = new SetCataHiddenTarget( false );
		}
	}
}