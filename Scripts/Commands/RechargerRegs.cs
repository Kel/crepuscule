using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using Server.Items;

namespace Server.Misc
{
    public class RechargerRegs
	{

        

		public static void Initialize()
		{
            Commands.Register("RechargerRegs", AccessLevel.Administrator, new CommandEventHandler(RechargerRegs_OnCommand));

		}

        private static void RechargerRegs_OnCommand(CommandEventArgs args)
		{
            ArrayList mobiless = new ArrayList();
            World.Broadcast(15, true, "ATTENTION : MODIFICATION DES REACTIFS EN COURS...");
            foreach (Item m in World.Items)
            {
                mobiless.Add(m);
            }

            World.Save();

            foreach (object m in mobiless)
            {
                if (m is BloodmossPlant && m != null)
                {
                    BloodmossPlant pm = (BloodmossPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is BatWingPlant && m != null)
                {
                    BatWingPlant pm = (BatWingPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is HuitreNoire && m != null)
                {
                    HuitreNoire pm = (HuitreNoire)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is BloodPlant && m != null)
                {
                    BloodPlant pm = (BloodPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is DestroyingAngelPlant && m != null)
                {
                    DestroyingAngelPlant pm = (DestroyingAngelPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is GarlicPlant && m != null)
                {
                    GarlicPlant pm = (GarlicPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is GinsengPlant && m != null)
                {
                    GinsengPlant pm = (GinsengPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is GraveDustPlant && m != null)
                {
                    GraveDustPlant pm = (GraveDustPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is IronPlant && m != null)
                {
                    IronPlant pm = (IronPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is MandrakeRootPlant && m != null)
                {
                    MandrakeRootPlant pm = (MandrakeRootPlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is NightshadePlant && m != null)
                {
                    NightshadePlant pm = (NightshadePlant)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is NoxSmallCrystals && m != null)
                {
                    NoxSmallCrystals pm = (NoxSmallCrystals)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is SmallSpringWater && m != null)
                {
                    SmallSpringWater pm = (SmallSpringWater)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }

                if (m is SmallWood && m != null)
                {
                    SmallWood pm = (SmallWood)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is SpiderSilks && m != null)
                {
                    SpiderSilks pm = (SpiderSilks)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
                if (m is SulfurousRock && m != null)
                {
                    SulfurousRock pm = (SulfurousRock)m;
                    if (pm.Donne == false)
                        pm.Donne = true;

                }
            }

        }


    }
}

