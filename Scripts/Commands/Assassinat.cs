using System;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Mobiles;

namespace Server.Scripts.Commands
{
	public class AssassinationCommand
    {
        public static void Initialize()
        { 		
        	Server.Commands.Register("assassinat", AccessLevel.Player, new CommandEventHandler(assassinat_OnCommand));
        }

        [Usage( "assassinat" )]
        [Description("Assassiner la cible.")]
        private static void assassinat_OnCommand(CommandEventArgs e)
        {
        	PlayerMobile from = ((PlayerMobile)e.Mobile);
        	Item item = from.FindItemOnLayer(Layer.OneHanded);
        	if ( item is BaseKnife )
        		from.Target = new assassinatTarget();
        	else
        		from.SendMessage("Vous devez avoir le bon type de lame pour cette t�che.");
        }
           	
            private class AssassinatCheck : Item
            {
                private Timer m_Timer;

                [Constructable]
                public AssassinatCheck()
                    : base(3699)
                {
                    Movable = false;
                    Name = "Anti-abus de l'assassinat";
                    Visible = false;
                    Weight = 0;

                    m_Timer = new InternalTimer(this, TimeSpan.FromMinutes(Utility.Random(2, 5)));
                    m_Timer.Start();
                }

                private class InternalTimer : Timer
                {
                    private AssassinatCheck m_this;

                    public InternalTimer(AssassinatCheck ass, TimeSpan delay)
                        : base(delay)
                    {
                        Priority = TimerPriority.OneSecond;
                        m_this = ass;
                    }

                    protected override void OnTick()
                    {
                        if (m_this != null)
                            m_this.Delete();
                    }
                }

                public AssassinatCheck(Serial serial)
                    : base(serial)
                {
                }

                public override void Serialize(GenericWriter writer)
                {
                    base.Serialize(writer);
                }

                public override void Deserialize(GenericReader reader)
                {
                    base.Deserialize(reader);
                }
            }
   
//           	<summary>
//           	Syst�me d'Assassinat:
//           	
//           	D�finie le pourcentage d'assassinat et de r�sistance.
//           	Soustrait ces deux nombres et y ajoutes divers facteurs.
//           	</summary>
//           	<param name="from"></param>
//           	<param name="targeted"></param>

    	private class assassinatTarget : Target
    	{
    		private Item m_Item;
           	public double pcAss = 0;          //Pourcentage d'asassinat
           	public double pcResist = 0;       //Pourcentage de r�sistance
           	public double pcFinal = 0;        //Pourcentage final de r�ussite
           	public double nbAleatoire;    //1 � 100
           	
           	public assassinatTarget() : base(1, false, TargetFlags.None)
            {
				
            }
    		
           	protected override void OnTarget(Mobile from, object targeted)
           	{
                    Container pack = from.Backpack;

                    if (pack != null)
                    {
                        Item[] items = pack.FindItemsByType(typeof(AssassinatCheck));

                        if (items.Length > 0)
                        {
                            from.SendMessage("Vous devez attendre avant une nouvelle tentative.");
                            return;
                        }
                    }

                  	if ( targeted is RacePlayerMobile )
                   	{
                		RacePlayerMobile pm = (RacePlayerMobile)from;
                    		Mobile targ = (Mobile)targeted;   
                  
                      		//VALIDER SI L'ASSASSIN EST UN ASSASASSIN
                      		if (pm.Capacities[CapacityName.Assassination].Value >=7)
                      		{
                            		if (targ.Player && targ.Alive)
                            		{
                                        from.AddToBackpack(new AssassinatCheck());

                                 		//D�finit le pourcentage d'Assassinat
                                 		//Assassinat vaut 50% du calcul
                                 		//Fencing le quart
                                 		//Anatomy l'autre quart
                                        if (pm.Capacities[CapacityName.Assassination].Value > 99)
                                        		pcAss = 33;
                                 		else
                                            pcAss = pm.Capacities[CapacityName.Assassination].Value / 3;
                                 		//Si l'assassin est hid� il a 20% de bonus
                                 		if (pm.Hidden)
                                        		pcAss += 20;
   
                                 		pcAss += pm.Skills[SkillName.Fencing].Value / 3;
                                 		pcAss += pm.Skills[SkillName.Anatomy].Value / 3;
                                 		//---------------FinAssassinat----------------------
   
   
                                 		//D�finie le pourcentage de r�sistance
                                 		//M�me ratio que pour le % d'Assassinat
                                 		RacePlayerMobile victime = (RacePlayerMobile)targeted;
                                 		//Armes de Jet = Agilit�_Accrue
                                        if (victime.Capacities[CapacityName.BetterAgility].Value > 99)
                                 	       		pcResist = 33;
                                 		else
                                            pcResist = victime.Capacities[CapacityName.BetterAgility].Value / 3;
                                 		if (victime.Mounted)
                                        		pcResist += 10;
                                 		if (victime.Warmode)
                                        		pcResist += 10;
   
                                 		pcResist += victime.Skills[SkillName.Focus].Value / 3;
                                 		pcResist += victime.Skills[SkillName.Parry].Value / 3;
                                 		//-------------FinVictime--------------------------
         		
                                 		//Soustrait le % D'ass - % resist
                                 		//On ajoute le facteur de dex divis� par 2
                                 		pcFinal = pcAss - pcResist;
                                 		pcFinal += (pm.Dex - victime.Dex) / 2;
   
                                 		//Nombre al�atoire entre 0 et 100 pour le % d'assassinat
                                 		nbAleatoire = Utility.Random(0, 100);
      	
                                 		if (nbAleatoire <= pcFinal)
                                 		{
                                        		//Tue la victime
                                        		victime.Emote("*S'�croule, un filet de sang giclant de la gorge.*");
                                        		victime.PlaySound(315);
                                        		victime.SendMessage("Vous vous �tes fait assassin�!");
                                        		victime.Kill();
   
                                        		//S'il �tait hid� il a des chances de le rester
                                        		if (pm.Hidden)
                                        		{
                                            			if (Utility.Random(0, 100) > pm.Skills.Hiding.Value)
                                                   			pm.Hidden = false;
                                            			else
                                                   			pm.SendMessage("Vous assassinez votre victime avec succ�s.");
                                        		}
                                        		if (!pm.Hidden)
                                        		{
                                        	    		pm.Emote("*Egorge la victime d'un coup vicieux de dague.*");
                                            			pm.Animate(31, 5, 1, true, false, 0);
                                        		}
                                 		}
                                 		else if (nbAleatoire <= pcFinal + 10)
                                 		{
                                        		//Dommages de la moiti� du fencing
                                        		double dommages = pm.Skills[SkillName.Fencing].Value / 2;
                                        	
                                        		//victime.Hits -= int.Parse(dommages.ToString());
                                        		victime.Hits -= (int)dommages;
                                        
                                        		pm.Emote("*Vous ratez votre tentative d'assassinat mais vous blessez s�rieusement votre victime.*");
                                        		victime.SendMessage("Vous �tes s�rieusement bless� � la suite d'une tentative d'assassinat!");
                                        		pm.PlaySound(315);
                                        		pm.Animate(31, 5, 1, true, false, 0);
                                 		}	
                                 		else
                                 		{
                                        		//Echec de l'Assassinat
                                        		pm.Emote("*Vous �chouez compl�tement votre tentative d'assassinat.*");
                                        		pm.SendMessage("Il ne vous reste que peu de temps pour vous sauver!");
                                        		victime.SendMessage("Vous venez d'�tre vicime d'une tentative d'assassinat.");
                                        		pm.PlaySound(315);
                                        		pm.Animate(31, 5, 1, true, false, 0);
                                        		if (!victime.Warmode)
                                            			victime.Warmode = true;
                                 		}
                             		}
                             		else
                                 		from.SendMessage("Vous ne pouvez pas assassiner cela.");   
                      		}
                      		else
                             		from.SendMessage("Vous ne pouvez pas encore utiliser cette dague.");
               		}
                   	else
                      		from.SendMessage("Vous ne pouvez pas assassiner cela.");             
           		} 
         	}
	}
} 
