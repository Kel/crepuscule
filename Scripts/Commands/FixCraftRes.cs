﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Linq;

namespace Server.Scripts.Commands {
    public class FixCraftRes{
        public static void Initialize()
        {
            Server.Commands.Register("FixCraftRes", AccessLevel.Administrator, new CommandEventHandler(FixCraftRes_OnCommand));

        }

        [Usage("FixCraftRes")]
        [Description("Régle les listes de craft")]
        private static void FixCraftRes_OnCommand(CommandEventArgs e)
        {
            WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, "Fix de ressources de craft...");
            foreach (var item in ManagerConfig.Items.Values)
            {
                if (item.Craft != null && item.Craft.Ressources.Count > 0)
                {
                    var result = item
                        .Craft
                        .Ressources
                        .Select(r => r.Type)
                        .GroupBy(r => r.Name)
                        .ToList()
                        .TrueForAll(g => g.Count() == 1);

                    if (!result)
                    {
                        WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, item.Name + " est buggué");
                        var res = item.Craft.Ressources.ToList();
                        var first = item.Craft.Ressource1;

                        if (item.Craft.Ressource2 != null && item.Craft.Ressource2.Type == first.Type) { first.Amount += item.Craft.Ressource2.Amount; item.Craft.Ressource2 = new GameItemCraftRes();  }
                        else if (item.Craft.Ressource3 != null && item.Craft.Ressource3.Type == first.Type) { first.Amount += item.Craft.Ressource3.Amount; item.Craft.Ressource3 = new GameItemCraftRes(); }
                        else if (item.Craft.Ressource4 != null && item.Craft.Ressource4.Type == first.Type) { first.Amount += item.Craft.Ressource4.Amount; item.Craft.Ressource4 = new GameItemCraftRes(); }
                        else if (item.Craft.Ressource5 != null && item.Craft.Ressource5.Type == first.Type) { first.Amount += item.Craft.Ressource5.Amount; item.Craft.Ressource5 = new GameItemCraftRes(); }
                    }
                }
            }
        }



    }
}
