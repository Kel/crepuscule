using System;  
using Server;
using Server.Gumps;
using Server.Network;

namespace Server.Custom.Regions
{
	public class Animation
	{
		
		
		public static void Initialize()
		{
			Server.Commands.Register( "anim", AccessLevel.Player, new CommandEventHandler( Anim ) );
		}
		
		[Usage( "Anim" )]
		[Description( "Animates a certain number." )]
		private static void Anim( CommandEventArgs e )
		{
			if(e.Length == 1)
			{
				
				int m_Type;
				try
				{
					m_Type = Convert.ToInt32( e.GetString( 0 ) );
					e.Mobile.Animate( m_Type, 7, 1, true, false, 0 );

				}
				catch
				{
					e.Mobile.SendMessage( "Le num�ro que vous avez introduit est incorrect" );
					return;
				}
			}
			else
			{
				e.Mobile.SendMessage("Format: Anim <int>");	
			}
			
		}
	}
}
