/*****************************************************
 * ce script a �t� cr�e par Namorcen.
 * 
 * Merci de laisser ce copyright si vous l'utilisez.
 * 
 * pour tout renseignement:
 * commandercore@hotmail.com
 *****************************************************/

using System;
using Server.Gumps;
using Server.Items;
using Server.Network;
using Server.Targeting;

namespace Server.Scripts.Commands
{
	public class DecoMove
	{
		public static void Initialize()
		{
			Server.Commands.Register( "DecoMove", AccessLevel.Player, new CommandEventHandler( DecoMove_OnCommand ) );
		}

		[Usage( "DecoMove" )]
		[Description( "D�place un objet adjacent sous conditions." )]
		private static void DecoMove_OnCommand( CommandEventArgs e )
		{
			e.Mobile.Target = new DecoMoveTarget();
			e.Mobile.SendMessage( "Quel objet voulez-vous d�placer ?" );
		}

		private class DecoMoveTarget : Target
		{
			public DecoMoveTarget() : base( -1, false, TargetFlags.None )
			{
			}

			protected override void OnTarget( Mobile from, object target )
			{
				if ( !(target is Item) )
					from.SendMessage( "Vous ne pouvez pas d�placer ceci." );
				else
				{
					Item targ = (Item) target;

					if ( targ.TotalWeight + targ.PileWeight > 100 )
						from.SendMessage( "C'est trop lourd pour que vous puissiez le d�placer." );
               
					else if ( !from.InRange( targ.GetWorldLocation(), 1 ) )
						from.SendMessage( "C'est hors de port�e." );
	               
					else if ( !targ.Movable && !targ.IsLockedDown )
						from.SendMessage( "Vous ne pouvez pas bouger ceci." );
					else
					{
						from.CloseGump( typeof( DecoMoveGump ) );
						from.SendGump( new DecoMoveGump( targ ) );
					}
				}
			}

			private class DecoMoveGump : Gump
			{
				private Item m_targ;
	
				public DecoMoveGump( Item targ ) : base( 200, 150 )
				{
					m_targ = targ;
					this.Closable=true;
					this.Disposable=true;
					this.Dragable=true;
					this.Resizable=false;
					this.AddPage(0);
					this.AddImage(174, 125, 5010);
					this.AddButton(283, 167, 4501, 4501, 1, GumpButtonType.Reply, 0);
					this.AddButton(279, 233, 4503, 4503, 2, GumpButtonType.Reply, 0);
					this.AddButton(217, 233, 4505, 4505, 3, GumpButtonType.Reply, 0);
					this.AddButton(217, 167, 4507, 4507, 4, GumpButtonType.Reply, 0);
					this.AddButton(250, 154, 4500, 4500, 5, GumpButtonType.Reply, 0);
					this.AddButton(295, 199, 4502, 4502, 6, GumpButtonType.Reply, 0);
					this.AddButton(250, 245, 4504, 4504, 7, GumpButtonType.Reply, 0);
					this.AddButton(204, 199, 4506, 4506, 8, GumpButtonType.Reply, 0);
					this.AddButton(143, 168, 4500, 4500, 9, GumpButtonType.Reply, 0);
					this.AddButton(143, 228, 4504, 4504, 10, GumpButtonType.Reply, 0);
					this.AddButton(350, 227, 4504, 4504, 11, GumpButtonType.Reply, 0);
					this.AddButton(349, 168, 4500, 4500, 12, GumpButtonType.Reply, 0);
					this.AddButton(244, 194, 5582, 5582, 13, GumpButtonType.Reply, 0);
					this.AddLabel(156, 147, 132, @"1 Haut");
					this.AddLabel(147, 276, 132, @"1 Bas");
					this.AddLabel(358, 148, 132, @"5 Haut");
					this.AddLabel(354, 277, 132, @"5 Bas");
				}
	
				public void sendDecoMoveGump( Mobile from, Item targ )
				{
					from.CloseGump( typeof( DecoMoveGump ) );
					from.SendGump( new DecoMoveGump( targ ) );
				}

				public bool checkMoveAccess( Mobile from, Item targ )
				{
					if ( targ.TotalWeight + targ.PileWeight > 100 )
						from.SendMessage( "C'est trop lourd pour que vous puissiez le d�placer." );
		               
					else if ( !from.InRange( targ.GetWorldLocation(), 1 ) )
						from.SendMessage( "C'est hors de port�e." );
		               
					else if ( !targ.Movable && !targ.IsLockedDown )
						from.SendMessage( "Vous ne pouvez pas bouger ceci." );
					else
						return true;
	                  
					return false;
				}

				private int GetFloorZ( Item item )
				{
					Map map = item.Map;
		
					if ( map == null )
						return int.MinValue;
		
					Tile[] tiles = map.Tiles.GetStaticTiles( item.X, item.Y, true );

					int z = int.MinValue;
	
					for ( int i = 0; i < tiles.Length; ++i )
					{
						Tile tile = tiles[i];
						ItemData id = TileData.ItemTable[tile.ID & 0x3FFF];
				
						int top = tile.Z; // Confirmed : no height checks here
		
						if ( id.Surface && !id.Impassable && top > z && top <= item.Z )
							z = top;
					}
					return z;
				}
         
				private bool Up( Item item, Mobile from )
				{
					int floorZ = GetFloorZ( item );
		
					if ( floorZ > int.MinValue && item.Z < (floorZ + 15) )
					{
						item.Location = new Point3D( item.Location, item.Z + 1 );
						return true;
					}
					else
					{
						from.SendMessage( "Vous ne pouvez pas monter cet objet plus haut." );
						return false;
					}
				}
         
				private bool Down( Item item, Mobile from )
				{
					int floorZ = GetFloorZ( item );
		
					if ( floorZ > int.MinValue && item.Z > GetFloorZ( item ) )
					{
						item.Location = new Point3D( item.Location, item.Z - 1 );
						return true;
					}
					else
					{
						from.SendMessage( "Vous ne pouvez pas descendre cet objet plus bas." );
						return false;
					}
				}
         
				private static void Turn( Item item, Mobile from )
				{
					FlipableAttribute[] attributes = (FlipableAttribute[])item.GetType().GetCustomAttributes( typeof( FlipableAttribute ), false );
		
					if( attributes.Length > 0 )
						attributes[0].Flip( item );
					else
						from.SendMessage( "Vous ne pouvez pas tourner cet objet." );
				}

				public override void OnResponse( NetState state, RelayInfo info )
				{
					Mobile from = state.Mobile;
					
					switch ( info.ButtonID )
					{
						case 0:
						{
							break;
						}
						case 1:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X, m_targ.Y - 1, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 2:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X + 1, m_targ.Y, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 3:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X, m_targ.Y + 1, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 4:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X - 1, m_targ.Y, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 5:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X - 1, m_targ.Y - 1, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 6:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X + 1, m_targ.Y - 1, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 7:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X + 1, m_targ.Y + 1, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 8:
						{
							if ( checkMoveAccess( from, m_targ ) )
								m_targ.DropToWorld( from, new Point3D( m_targ.X - 1, m_targ.Y + 1, m_targ.Z ) );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 9:
						{
							if ( checkMoveAccess( from, m_targ ) )
								Up( m_targ, from );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 10:
						{
							if ( checkMoveAccess( from, m_targ ) )
								Down( m_targ, from );
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 11:
						{
							if ( checkMoveAccess( from, m_targ ) )
							{
								for ( int i = 0; i < 5; ++i )
								{
									if ( !Down( m_targ, from ) )
										break;
								}
							}
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 12:
						{
							if ( checkMoveAccess( from, m_targ ) )
							{
								for ( int i = 0; i < 5; ++i )
								{
									if ( !Up( m_targ, from ) )
										break;
								}
							}
							sendDecoMoveGump( from, m_targ );
							break;
						}
						case 13:
						{
							if ( checkMoveAccess( from, m_targ ) )
								Turn( m_targ, from );
							sendDecoMoveGump( from, m_targ );
							break;
						}
					}
				}
			}
		}
	}
}