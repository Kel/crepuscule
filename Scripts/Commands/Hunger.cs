// une petite commande .hungry parce que j'en ai marre de pas savoir si j'ai faim ou pas
// j'aurais pu faire plus simple pour la commande mais je voulais quelle soit integrable dans le Help
// elle affiche un pti gump mimi , qui vous donne numériquement la valeur de votre faim et soif

using System;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Gumps;

namespace Server.Scripts.Commands
{
	public class Hungry
	{
		public static void Initialize()
		{
			Server.Commands.Register( "faim", AccessLevel.Player, new CommandEventHandler( Hungry_OnCommand ) );
			Server.Commands.Register( "soif", AccessLevel.Player, new CommandEventHandler( Hungry_OnCommand ) );
		}

		[Usage( "Hungry" )]
		[Description( "Affiche votre niveau de nourriture et de Boisson" )]
		public static void Hungry_OnCommand( CommandEventArgs e )
		{
			Mobile from = e.Mobile;
			from.SendGump( new gumpfaim ( from ) );

		}

	}
}

namespace Server.Gumps
{
	public class gumpfaim : Gump
	{

		public gumpfaim(Mobile from) : base(0,0)
		{
			Closable = true;
			Dragable = true;

			AddPage(0);

			AddBackground( 0, 0, 295, 144, 5054);
			AddBackground( 14, 27, 261, 100, 3500);
			AddLabel( 60, 62, 0, string.Format( "Faim :{0} /20", from.Hunger));
			AddLabel( 60, 81, 0, string.Format( "Soif :{0} /20", from.Thirst));
			AddItem( 8, 78, 8093);
			AddItem( 19, 60, 4155);
		}

		public override void OnResponse( Server.Network.NetState sender, RelayInfo info )
		{

		}
	}
}
