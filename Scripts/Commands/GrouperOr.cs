using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using Server.Items;

namespace Server.Misc
{
    public class GrouperOr
    {
        public static void Initialize()
        {
            Commands.Register("GrouperOr", AccessLevel.Administrator, new CommandEventHandler(GrouperOr_OnCommand));
        }

        private static void GrouperOr_OnCommand(CommandEventArgs args)
        {
            var interestBags = new ArrayList();
            foreach (Item item in World.Items.Values)
            {
                if (item is InterestBag)
                {
                    InterestBag ibag = (InterestBag)item;
                    interestBags.Add(ibag);
                }
            }
            for (int i = 0; i < interestBags.Count; ++i)
            {
                var sac = (InterestBag)interestBags[i];
                var ItemsInBag = sac.Items;
                for (int z = 0; z < ItemsInBag.Count; ++z)
                {
                    Item or = (Item)ItemsInBag[z];
                    if (or is Gold)
                    {
                        Gold gib = (Gold)or;
                        sac.Interet += gib.Amount;
                        or.Delete();
                        z -= 1;
                    }
                }
            }
        }
    }
}

