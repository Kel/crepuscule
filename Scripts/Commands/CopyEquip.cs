using System;
using System.Collections;
using System.Reflection;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Regions;
using System.Collections.Generic;

namespace Server
{
    public class CopyEquip
    {
        public static void Initialize()
        {
            CommandSystem.Register("CopyEquip", AccessLevel.GameMaster, new CommandEventHandler(CopyEquip_OnCommand));
        }


        [Usage("Clone")]
        [Description("Fait une copie d'équipement d'un NPC vers un autre")]
        public static void CopyEquip_OnCommand(CommandEventArgs e)
        {
            e.Mobile.Target = new CloneTarget();
            e.Mobile.SendMessage("Equipment de qui voulez vous copier?");
        }

        private class CloneTarget : Target
        {
            public CloneTarget()
                : base(15, true, TargetFlags.None)
            {
            }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(targ is Mobile))
                {
                    from.SendMessage("Vous pouvez copier l'équipement les mobiles uniquement!");
                    return;
                }
                if (targ is RacePlayerMobile)
                {
                    from.SendMessage("Mais pas les joueurs, bon sang!");
                    return;
                }
                from.Target = new CopyEquipTarget((Mobile)targ);
                from.SendMessage("Vers qui?");

            }
        }

        private class CopyEquipTarget : Target
        {
            public Mobile CopyFrom = null;
            public CopyEquipTarget(Mobile copyFrom)
                : base(15, true, TargetFlags.None)
            {
                CopyFrom = copyFrom;
            }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(targ is Mobile))
                {
                    from.SendMessage("Vous devez selectionner les mobiles uniquement!");
                    return;
                }
                if (targ is RacePlayerMobile)
                {
                    from.SendMessage("Mais pas les joueurs, bon sang!");
                    return;
                }
                CopyEquipment(CopyFrom, (Mobile)targ);


            }
        }

        public static void DeleteAllItems(Mobile mobile)
        {
            List<Item> toDelete = new List<Item>();
            int itemsCount = mobile.Items.Count;
            for (int i = 0; i < itemsCount; i++)
            {
                Item item = (Item)mobile.Items[i];
                toDelete.Add(item);
            }

            int toDeleteCount = toDelete.Count;
            for (int i = 0; i < toDeleteCount; i++)
                toDelete[i].Delete();

            mobile.Items.Clear();
        }

        public static void CopyEquipment(Mobile copyFrom, Mobile copyTo)
        {
            CopyEquipment(copyFrom, copyTo, false);
        }

        public static void CopyEquipment(Mobile copyFrom, Mobile copyTo, bool clone)
        {
            CopyEquipment(copyFrom, copyTo, clone, true);
        }

        public static void CopyEquipment(Mobile copyFrom, Mobile copyTo, bool clone, bool copyHair)
        {
            if (copyTo == null || copyFrom == null) return;
            if (copyTo.Deleted || copyFrom.Deleted) return;
            try
            {
                DeleteAllItems(copyTo);
                if (clone)
                {
                    copyTo.Dex = copyFrom.Dex;
                    copyTo.Int = copyFrom.Int;
                    copyTo.Str = copyFrom.Str;
                    copyTo.Fame = copyFrom.Fame;
                    copyTo.Karma = copyFrom.Karma;
                    copyTo.NameHue = copyFrom.NameHue;
                    copyTo.SpeechHue = copyFrom.SpeechHue;
                    copyTo.Criminal = copyFrom.Criminal;
                    copyTo.Name = copyFrom.Name;
                    copyTo.Title = copyFrom.Title;
                    copyTo.Female = copyFrom.Female;
                    copyTo.Body = copyFrom.Body;
                    copyTo.Hue = copyFrom.Hue;
                    copyTo.Hits = copyFrom.HitsMax;
                    copyTo.Mana = copyFrom.ManaMax;
                    copyTo.Stam = copyFrom.StamMax;
                    copyTo.BodyMod = copyFrom.Body;
                }
                
                //copyTo.Map = copyFrom.Map;
                //copyTo.Location = copyFrom.Location;
                //copyTo.Direction = copyFrom.Direction;
                if (copyTo is BaseCreature && clone)
                {
                    BaseCreature bsClone = copyFrom as BaseCreature;
                    BaseCreature bsMob = copyTo as BaseCreature;

                    bsMob.Saying1 = bsClone.Saying1;
                    bsMob.Saying2 = bsClone.Saying2;
                    bsMob.Saying3 = bsClone.Saying3;
                    bsMob.Saying4 = bsClone.Saying4;
                    bsMob.Saying5 = bsClone.Saying5;
                    bsMob.SayingsEnabled = bsClone.SayingsEnabled;
                    bsMob.SayingsSecondsDelay = bsClone.SayingsSecondsDelay;
                }
                //copyTo.Frozen = copyFrom.Frozen;
                //copyTo.Location = copyFrom.Location;
                //copyTo.Map = copyFrom.Map;

                for (int i = 0; i < copyFrom.Skills.Length; i++)
                    copyTo.Skills[i].Base = copyFrom.Skills[i].Base;

                ArrayList CopiedItems = new ArrayList(copyFrom.Items);

                for (int i = 0; i < CopiedItems.Count; i++)
                {
                    Item CopiedItem = (Item)CopiedItems[i];
                    if (((CopiedItem != null) && (CopiedItem.Parent == copyFrom) && (CopiedItem != copyFrom.Backpack)))
                    {
                        Type type = CopiedItem.GetType();
                        Item newitem = Activator.CreateInstance(type) as Item;
                        if(newitem != null)
                        {
                            if (CopiedItem != null && newitem != null)
                            {
                                newitem.Layer = CopiedItem.Layer;
                                copyTo.AddItem(newitem);
                                newitem.Hue = CopiedItem.Hue;
                            }
                        }
                    }
                }


                Item etorso = copyFrom.FindItemOnLayer(Layer.OuterTorso);
                Item mtorso = copyTo.FindItemOnLayer(Layer.OuterTorso);
                if (etorso != null && mtorso != null)
                {
                    mtorso.Hue = etorso.Hue;
                    mtorso.LootType = LootType.Blessed;
                }

                Item eWaist = copyFrom.FindItemOnLayer(Layer.Waist);
                Item mWaist = copyTo.FindItemOnLayer(Layer.Waist);
                if (eWaist != null && mWaist != null)
                {
                    mWaist.Hue = eWaist.Hue;
                    mWaist.LootType = LootType.Blessed;
                }

                Item ehelm = copyFrom.FindItemOnLayer(Layer.Helm);
                Item mhelm = copyTo.FindItemOnLayer(Layer.Helm);
                if (ehelm != null && mhelm != null)
                {
                    mhelm.Hue = ehelm.Hue;
                    mhelm.LootType = LootType.Blessed;
                }

                Item emtorso = copyFrom.FindItemOnLayer(Layer.MiddleTorso);
                Item mmtorso = copyTo.FindItemOnLayer(Layer.MiddleTorso);
                if (emtorso != null && mmtorso != null)
                {
                    mmtorso.Hue = emtorso.Hue;
                    mmtorso.LootType = LootType.Blessed;
                }

                Item epants = copyFrom.FindItemOnLayer(Layer.Pants);
                Item mpants = copyTo.FindItemOnLayer(Layer.Pants);
                if (epants != null && mpants != null)
                {
                    mpants.Hue = epants.Hue;
                    mpants.LootType = LootType.Blessed;
                }

                Item earms = copyFrom.FindItemOnLayer(Layer.Arms);
                Item marms = copyTo.FindItemOnLayer(Layer.Arms);
                if (earms != null && marms != null)
                {
                    marms.Hue = earms.Hue;
                    marms.LootType = LootType.Blessed;
                }

                Item egloves = copyFrom.FindItemOnLayer(Layer.Gloves);
                Item mgloves = copyTo.FindItemOnLayer(Layer.Gloves);
                if (egloves != null && mgloves != null)
                {
                    mgloves.Hue = egloves.Hue;
                    mgloves.LootType = LootType.Blessed;
                }

                Item eneck = copyFrom.FindItemOnLayer(Layer.Neck);
                Item mneck = copyTo.FindItemOnLayer(Layer.Neck);
                if (eneck != null && mneck != null)
                {
                    mneck.Hue = eneck.Hue;
                    mneck.LootType = LootType.Blessed;
                }

                Item etwohand = copyFrom.FindItemOnLayer(Layer.TwoHanded);
                Item mtwohand = copyTo.FindItemOnLayer(Layer.TwoHanded);
                if (etwohand != null && mtwohand != null)
                {
                    mtwohand.Hue = etwohand.Hue;
                    mtwohand.LootType = LootType.Blessed;
                }

                Item eonehand = copyFrom.FindItemOnLayer(Layer.OneHanded);
                Item monehand = copyTo.FindItemOnLayer(Layer.OneHanded);
                if (eonehand != null && monehand != null)
                {
                    monehand.Hue = eonehand.Hue;
                    monehand.LootType = LootType.Blessed;
                }

                Item efirstvalid = copyFrom.FindItemOnLayer(Layer.FirstValid);
                Item mfirstvalid = copyTo.FindItemOnLayer(Layer.FirstValid);
                if (efirstvalid != null && mfirstvalid != null)
                {
                    mfirstvalid.Hue = efirstvalid.Hue;
                    mfirstvalid.LootType = LootType.Blessed;
                }

                Item eshirt = copyFrom.FindItemOnLayer(Layer.Shirt);
                Item mshirt = copyTo.FindItemOnLayer(Layer.Shirt);
                if (eshirt != null && mshirt != null)
                {
                    mshirt.Hue = eshirt.Hue;
                    mshirt.LootType = LootType.Blessed;
                }

                Item eshoes = copyFrom.FindItemOnLayer(Layer.Shoes);
                Item mshoes = copyTo.FindItemOnLayer(Layer.Shoes);
                if (eshoes != null && mshoes != null)
                {
                    mshoes.Hue = eshoes.Hue;
                    mshoes.LootType = LootType.Blessed;
                }

                if (!copyHair)
                {
                    var toDelete = copyTo.FindItemOnLayer(Layer.Hair);
                    if (toDelete != null) toDelete.Delete();

                    toDelete = copyTo.FindItemOnLayer(Layer.FacialHair);
                    if (toDelete != null) toDelete.Delete();
                }

            }
            catch (Exception ex)
            {
                Server.Scripts.Commands.CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 3, ex.Message);
                Server.Scripts.Commands.CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 7, ex.StackTrace);
            }
        }
    }
}

