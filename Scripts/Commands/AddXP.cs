﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Scripts.Commands
{
    public class AddXP
    {
        public static void Initialize()
        {
            Server.Commands.Register("addxp", AccessLevel.GameMaster, new CommandEventHandler(AddXP_OnCommand));

        }

        [Usage("addxp")]
        [Description("Ajoute une fiole d'expérience")]
        private static void AddXP_OnCommand(CommandEventArgs e)
        {
            e.Mobile.Target = new AddXPTarget();
            e.Mobile.SendMessage("A qui vous voulez ajouter une fiole?");
        }

        private class AddXPTarget : Target
        {
            public AddXPTarget()
                : base(15, true, TargetFlags.None)
            {
            }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(targ is RacePlayerMobile))
                {
                    from.SendMessage("Vous pouvez ajouter aux joueurs uniquement!");
                    return;
                }


                XPPotion potion = new XPPotion(from, targ as Mobile);
                try
                {
                    (targ as RacePlayerMobile).AddToBackpack(potion);
                }
                catch (SystemException ex)
                {
                    from.SendMessage("Erreur: " + ex.Message.ToString());
                    return;
                }


            }
        }


    }
}
