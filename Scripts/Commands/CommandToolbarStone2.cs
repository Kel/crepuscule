/**************************************
*       Command Toolbar Stone 2       *
*          Distro files: None         *
*                                     *
*     Made by Demortris AKA Joeku     *
*            and Rothhear             *
*             12/21/2005              *
*                                     *
* Anyone can modify/redistribute this *
*  DO NOT REMOVE/CHANGE THIS HEADER!  *
**************************************/

using System; 
using System.Net; 
using Server; 
using Server.Accounting; 
using Server.Gumps; 
using Server.Items; 
using Server.Mobiles; 
using Server.Network;
using Server.Targeting;
using Server.Targets;
using Server.Scripts.Commands;
using Server.ContextMenus;
using System.Collections;
using System.Collections.Generic;

namespace Server.Items
{
	public class CommandToolbarStone : Item
	{
		private bool m_HideToolbarEnabled;
		public bool HideToolbarEnabled{ get{ return m_HideToolbarEnabled; } set{ m_HideToolbarEnabled = value; InvalidateProperties(); }}
		private bool m_AdvToolbarEnabled;
		public bool AdvToolbarEnabled{ get{ return m_AdvToolbarEnabled; } set{ m_AdvToolbarEnabled = value; InvalidateProperties(); }}

		private string m_HideType;
		public string HideType{ get{ return m_HideType; } set{ m_HideType = value; InvalidateProperties(); }}

		#region CommandProperties
		private string m_Command1;
		private string m_Command2;
		private string m_Command3;
		private string m_Command4;
		private string m_Command5;
		private string m_Command6;
		private string m_Command7;

		public string Command1{ get{ return m_Command1; } set{ m_Command1 = value; InvalidateProperties(); }}
		public string Command2{ get{ return m_Command2; } set{ m_Command2 = value; InvalidateProperties(); }}
		public string Command3{ get{ return m_Command3; } set{ m_Command3 = value; InvalidateProperties(); }}
		public string Command4{ get{ return m_Command4; } set{ m_Command4 = value; InvalidateProperties(); }}
		public string Command5{ get{ return m_Command5; } set{ m_Command5 = value; InvalidateProperties(); }}
		public string Command6{ get{ return m_Command6; } set{ m_Command6 = value; InvalidateProperties(); }}
		public string Command7{ get{ return m_Command7; } set{ m_Command7 = value; InvalidateProperties(); }}
		#endregion

		[Constructable]
		public CommandToolbarStone() : base( 3699 )
		{
			Hue = 2101;
			Name = "Command Toolbar Stone";
			LootType = LootType.Newbied;

			HideToolbarEnabled = true;
			AdvToolbarEnabled = true;

			HideType = "Normal Hide";

			Command1 = "Props";
			Command2 = "M Tele";
			Command3 = "Move";
			Command4 = "M Delete";
			Command5 = "Wipe";
			Command6 = "Who";
			Command7 = "Add";
		}

		public CommandToolbarStone( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from ) 
		{
			if ( !IsChildOf( from.Backpack ) ) 
			{ 
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				return;
			}

			if ( from.AccessLevel < AccessLevel.GameMaster )
			{
				from.SendMessage( "Only Game Masters can use this." );
				return;
			}

			if ( from.HasGump( typeof( CommandToolbarEditMenu )))
				from.CloseGump( typeof( CommandToolbarEditMenu ));

			from.CloseGump( typeof( CommandToolbarGump ) );
			CommandToolbarGump gump = new CommandToolbarGump( this );
			from.SendGump( gump );
			from.SendMessage( "Command Toolbar initialized." );
		}

		public override void AddNameProperties( ObjectPropertyList list )
		{
			base.AddNameProperties( list );

			string prefix = Server.Commands.CommandPrefix;
			string entry = String.Format( "{0}{1}\nCommand 2: {0}{2}\nCommand 3: {0}{3}\nCommand 4: {0}{4}\nCommand 5: {0}{5}\nCommand 6: {0}{6}\nCommand 7: {0}{7}\n--------------------", prefix, Command1, Command2, Command3, Command4, Command5, Command6, Command7);
			string hideentry1 = String.Format( "\n Hide Type: {0}", this.HideType);
			string hideentry2 = String.Format( "\n Hide Type: {0}", this.HideType);
			list.Add( 1060658, "Command 1\t{0}", entry );
			if( this.HideToolbarEnabled )
				list.Add( 1060659, "Hide Toolbar\tEnabled{0}\n--------------------", hideentry1 );
			else
				list.Add( 1060659, "Hide Toolbar\tDisabled{0}\n--------------------", hideentry2 );
			if( this.AdvToolbarEnabled )
				list.Add( 1060660, "Advanced Toolbar\tEnabled" );
			else
				list.Add( 1060660, "Advanced Toolbar\tDisabled" );
	
		}

		private class EditEntry : ContextMenuEntry
		{
			private Mobile m_From;
			private CommandToolbarStone m_Item;

			public EditEntry( Mobile from, Item item ) : base( 5101 )
			{
				m_From = from;
				m_Item = item as CommandToolbarStone;
			}

			public override void OnClick()
			{
				if ( m_Item.IsChildOf( m_From.Backpack ) ) 
				{
					m_From.CloseGump( typeof( CommandToolbarGump ) );
					CommandToolbarEditMenu editmenu = new CommandToolbarEditMenu( m_Item );
						editmenu.AddTextEntry(153, 90, 445, 20, 2101, 1, editmenu.Command1 );
						editmenu.AddTextEntry(153, 130, 445, 20, 2101, 2, editmenu.Command2 );
						editmenu.AddTextEntry(153, 170, 445, 20, 2101, 3, editmenu.Command3 );
						editmenu.AddTextEntry(153, 210, 445, 20, 2101, 4, editmenu.Command4 );
						editmenu.AddTextEntry(153, 250, 445, 20, 2101, 5, editmenu.Command5 );
						editmenu.AddTextEntry(153, 290, 445, 20, 2101, 6, editmenu.Command6 );
						editmenu.AddTextEntry(153, 330, 445, 20, 2101, 7, editmenu.Command7 );
						if( !editmenu.HideToolbarEnabled )
							editmenu.AddButton(287, 369, 2151, 2153, 11, GumpButtonType.Reply, 1);
						else
							editmenu.AddButton(287, 369, 2153, 2151, 11, GumpButtonType.Reply, 1);
						if( !editmenu.AdvToolbarEnabled )
							editmenu.AddButton(287, 399, 2151, 2153, 12, GumpButtonType.Reply, 1);
						else
							editmenu.AddButton(287, 399, 2153, 2151, 12, GumpButtonType.Reply, 1);
						editmenu.AddLabel(374, 403, 0, m_Item.HideType);

						editmenu.AddPage(2);

						editmenu.AddBackground(0, 0, 273, 545, 9270);
						editmenu.AddLabel(75, 39, 2101, @"Hide Type Edit Menu");

						editmenu.AddBackground(64, 83, 131, 26, 9350);
						editmenu.AddLabel(72, 87, 0, @"Normal Hide");
						editmenu.AddButton(196, 87, 5601, 5605, 14, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 123, 131, 26, 9350);
						editmenu.AddLabel(72, 127, 0, @"Blood Oath Hide");
						editmenu.AddButton(196, 127, 5601, 5605, 15, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 163, 131, 26, 9350);
						editmenu.AddLabel(72, 167, 0, @"Divine Male Hide");
						editmenu.AddButton(196, 167, 5601, 5605, 16, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 203, 131, 26, 9350);
						editmenu.AddLabel(72, 207, 0, @"Divine Female Hide");
						editmenu.AddButton(196, 207, 5601, 5605, 17, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 243, 131, 26, 9350);
						editmenu.AddLabel(72, 247, 0, @"Explosion Hide");
						editmenu.AddButton(196, 247, 5601, 5605, 18, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 283, 131, 26, 9350);
						editmenu.AddLabel(72, 287, 0, @"Fire Hide");
						editmenu.AddButton(196, 287, 5601, 5605, 19, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 323, 131, 26, 9350);
						editmenu.AddLabel(72, 327, 0, @"Noble Hide");
						editmenu.AddButton(196, 327, 5601, 5605, 20, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 363, 131, 26, 9350);
						editmenu.AddLabel(72, 367, 0, @"Poison Hide");
						editmenu.AddButton(196, 367, 5601, 5605, 21, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 403, 131, 26, 9350);
						editmenu.AddLabel(72, 407, 0, @"Shiney Hide");
						editmenu.AddButton(196, 407, 5601, 5605, 22, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 443, 131, 26, 9350);
						editmenu.AddLabel(72, 447, 0, @"Thunder Hide");
						editmenu.AddButton(196, 447, 5601, 5605, 23, GumpButtonType.Reply, 2);
						editmenu.AddBackground(64, 483, 131, 26, 9350);
						editmenu.AddLabel(72, 487, 0, @"Wither Hide");
						editmenu.AddButton(196, 487, 5601, 5605, 24, GumpButtonType.Reply, 2);
					m_From.SendGump( editmenu );
					m_From.SendMessage( "Command Toolbar Stone Edit Menu initialized." );
				}
				else
				{
					m_From.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				}
			}
		}

		private class CloseEntry : ContextMenuEntry
		{
			private Mobile m_From;
			private CommandToolbarStone m_Item;

			public CloseEntry( Mobile from, Item item ) : base( 0363 )
			{
				m_From = from;
				m_Item = (CommandToolbarStone)item;
			}

			public override void OnClick()
			{
				if ( m_Item.IsChildOf( m_From.Backpack ) ) 
				{
					m_From.CloseGump( typeof( CommandToolbarGump ) );
					m_From.SendMessage( "Command Toolbar terminated." );
				}
				else
				{
					m_From.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				}
			}
		}

        public static void GetContextMenuEntries(Mobile from, Item item, List<ContextMenuEntry> list)
		{
			list.Add( new EditEntry( from, item ) );
			list.Add( new CloseEntry( from, item ) );
		}

		public override void GetContextMenuEntries( Mobile from, List<ContextMenuEntry> list )
		{
			if ( from.AccessLevel < AccessLevel.GameMaster )
			{
				from.SendMessage( "Only Game Masters can use this." );
				return;
			}
			else
			{
				base.GetContextMenuEntries( from, list );
				CommandToolbarStone.GetContextMenuEntries( from, this, list );
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
			
			writer.Write( (bool) m_HideToolbarEnabled );
			writer.Write( (bool) m_AdvToolbarEnabled );

			writer.Write( (string) m_HideType );

			#region CommandPropertySerialize
			writer.Write( (string) m_Command1 );
			writer.Write( (string) m_Command2 );
			writer.Write( (string) m_Command3 );
			writer.Write( (string) m_Command4 );
			writer.Write( (string) m_Command5 );
			writer.Write( (string) m_Command6 );
			writer.Write( (string) m_Command7 );
			#endregion
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			m_HideToolbarEnabled = reader.ReadBool();
			m_AdvToolbarEnabled = reader.ReadBool();

			m_HideType = reader.ReadString();
			
			#region CommandPropertyDeserialize
			m_Command1 = reader.ReadString();
			m_Command2 = reader.ReadString();
			m_Command3 = reader.ReadString();
			m_Command4 = reader.ReadString();
			m_Command5 = reader.ReadString();
			m_Command6 = reader.ReadString();
			m_Command7 = reader.ReadString();
			#endregion
		}
	}
}

namespace Server.Gumps
{
	public class CommandToolbarGump : Gump
	{
		public CommandToolbarStone stone;

		private string HideType;
		
		#region CommandProperties
		private string Command1;
		private string Command2;
		private string Command3;
		private string Command4;
		private string Command5;
		private string Command6;
		private string Command7;
		#endregion
		
		public CommandToolbarGump( Item item ) : base( 0, 28 )
		{
			CommandToolbarStone cmditem = item as CommandToolbarStone;
			this.stone = cmditem;
			string prefix = Server.Commands.CommandPrefix;

			this.HideType = cmditem.HideType;

			this.Command1 = cmditem.Command1;
			this.Command2 = cmditem.Command2;
			this.Command3 = cmditem.Command3;
			this.Command4 = cmditem.Command4;
			this.Command5 = cmditem.Command5;
			this.Command6 = cmditem.Command6;
			this.Command7 = cmditem.Command7;

			this.Closable=false;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			
			this.AddPage(0);
			this.AddPage(1);
			this.AddBackground(0, 0, 610, 75, 9270);
			this.AddButton(63, 35, 9726, 9728, 1, GumpButtonType.Reply, 1);
			this.AddButton(143, 35, 9726, 9728, 2, GumpButtonType.Reply, 1);
			this.AddButton(223, 35, 9726, 9728, 3, GumpButtonType.Reply, 1);
			this.AddButton(303, 35, 9726, 9728, 4, GumpButtonType.Reply, 1);
			this.AddButton(383, 35, 9726, 9728, 5, GumpButtonType.Reply, 1);
			this.AddButton(463, 35, 9726, 9728, 6, GumpButtonType.Reply, 1);
			this.AddButton(543, 35, 9726, 9728, 7, GumpButtonType.Reply, 1);
			this.AddLabelCropped(40, 15, 70, 20, 2101, prefix+Command1);
			this.AddLabelCropped(120, 15, 70, 20, 2101, prefix+Command2);
			this.AddLabelCropped(200, 15, 70, 20, 2101, prefix+Command3);
			this.AddLabelCropped(280, 15, 70, 20, 2101, prefix+Command4);
			this.AddLabelCropped(360, 15, 70, 20, 2101, prefix+Command5);
			this.AddLabelCropped(440, 15, 70, 20, 2101, prefix+Command6);
			this.AddLabelCropped(520, 15, 70, 20, 2101, prefix+Command7);
			if( cmditem.HideToolbarEnabled )
			{
				this.AddBackground(606, 0, 117, 75, 9270);
				this.AddButton(664, 19, 2642, 2643, 9, GumpButtonType.Reply, 1);
				this.AddLabel(618, 20, 2101, @"Hide &");
				this.AddLabel(618, 40, 2101, @"Unhide");
			}
			if( cmditem.AdvToolbarEnabled )
			{
				if( !cmditem.HideToolbarEnabled )
				{
					this.AddBackground(606, 0, 165, 75, 9270);
					this.AddButton(740, 17, 9762, 9763, 10, GumpButtonType.Reply, 1);
					this.AddLabel(620, 15, 2101, @"Item/Mobile Move");
					this.AddLabel(620, 40, 2101, @"Creature Control");
					this.AddButton(740, 43, 9762, 9763, 11, GumpButtonType.Reply, 1);
				}
				else
				{
					this.AddBackground(719, 0, 155, 75, 9270);
					this.AddButton(843, 17, 9762, 9763, 10, GumpButtonType.Reply, 1);
					this.AddLabel(731, 15, 2101, @"Item/Mobile Move");
					this.AddLabel(731, 40, 2101, @"Creature Control");
					this.AddButton(843, 43, 9762, 9763, 11, GumpButtonType.Reply, 1);
				}
			}
			this.AddButton(13, 13, 22053, 22055, 8, GumpButtonType.Page, 2);
			this.AddPage(2);
			this.AddBackground(0, 0, 45, 75, 9270);
			this.AddButton(13, 13, 22056, 22058, 9, GumpButtonType.Page, 1);

		}

		public override void OnResponse( NetState state, RelayInfo info )
        	{ 
			Mobile from = state.Mobile;
			CommandToolbarStone item = this.stone;
			string prefix = Server.Commands.CommandPrefix;
			IPoint3D p = from.Location as IPoint3D;

			if ( item == null )
			{
				from.SendMessage( "This command toolbar stone does not exist." );
				return;
			}

			if ( !item.IsChildOf( from.Backpack ))
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				return;
			}

			if ( from.AccessLevel < AccessLevel.GameMaster )
			{
				from.SendMessage( "Only Game Masters can use this." );
				return;
			}

			switch ( info.ButtonID ) 
           		{ 
				case 1:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command1 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command1 ) );
					from.SendGump( new CommandToolbarGump( item ) );
                			break;
				} 
				
				case 2:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command2 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command2 ) );
					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}

				case 3:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command3 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command3 ) );
					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}

				case 4:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command4 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command4 ) );
					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}

				case 5:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command5 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command5 ) );
					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}
				
				case 6:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command6 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command6 ) );
					from.SendGump( new CommandToolbarGump( item ) );
               				break;
				}

				case 7:
				{
					from.SendMessage( "{0}{1}", prefix, this.Command7 );
					Commands.Handle( from, String.Format( "{0}{1}", prefix, this.Command7 ) );
					from.SendGump( new CommandToolbarGump( item ) );
               				break;
				}
				
				case 9:
				{
					if ( HideType == "Normal Hide" )
					{
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y, from.Z + 4 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y, from.Z ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y, from.Z - 4 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X, from.Y + 1, from.Z + 4 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X, from.Y + 1, from.Z ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X, from.Y + 1, from.Z - 4 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y + 1, from.Z + 11 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y + 1, from.Z + 7 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y + 1, from.Z + 3 ), from.Map, 0x3728, 13 );
						Effects.SendLocationEffect( new Point3D( from.X + 1, from.Y + 1, from.Z - 1 ), from.Map, 0x3728, 13 );
						from.PlaySound( 0x228 );
						if ( !from.Hidden )
							from.Hidden = true;
						else
							from.Hidden = false;
					}
					else if ( HideType == "Blood Oath Hide" )
					{
						if ( !from.Hidden )
						{
							from.Hidden = true;
							from.FixedParticles( 0x375A, 1, 17, 9919, 33, 7, EffectLayer.Waist );
							from.FixedParticles( 0x3728, 1, 13, 9502, 33, 7, (EffectLayer)255 );
							from.PlaySound( 0x175 );
						}
						else
						{
							from.FixedParticles( 0x375A, 1, 17, 9919, 33, 7, EffectLayer.Waist );
							from.FixedParticles( 0x3728, 1, 13, 9502, 33, 7, (EffectLayer)255 );
							from.PlaySound( 0x175 );
							from.Hidden = false;
						}
					}
					else if ( HideType == "Divine Male Hide" )
					{
						if ( !from.Hidden )
						{ 
							from.FixedParticles(0x376A, 1, 31, 9961, 1160, 0, EffectLayer.Waist );
							from.FixedParticles( 0x37C4, 1, 31, 9502, 43, 2, EffectLayer.Waist );
							from.PlaySound( 0x20F );
							from.PlaySound( 0x44A );
							from.Hidden = true;
							
						} 
						else 
						{ 
							from.Hidden = false;
							from.FixedParticles(0x376A, 1, 31, 9961, 1160, 0, EffectLayer.Waist );
							from.FixedParticles( 0x37C4, 1, 31, 9502, 43, 2, EffectLayer.Waist );
							from.PlaySound( 0x20F );
							from.PlaySound(0x44A );
						} 
					}
					else if ( HideType == "Divine Female Hide" )
					{
						if ( !from.Hidden )
						{ 
							from.FixedParticles(0x376A, 1, 31, 9961, 1160, 0, EffectLayer.Waist );
							from.FixedParticles( 0x37C4, 1, 31, 9502, 43, 2, EffectLayer.Waist );
							from.PlaySound( 0x20F );
							from.PlaySound( 0x338 );
							from.Hidden = true;
						} 
						else 
						{ 
							from.Hidden = false;
							from.FixedParticles(0x376A, 1, 31, 9961, 1160, 0, EffectLayer.Waist );
							from.FixedParticles( 0x37C4, 1, 31, 9502, 43, 2, EffectLayer.Waist );
							from.PlaySound( 0x20F );
							from.PlaySound(0x338);
						}
					}
					else if ( HideType == "Explosion Hide" )
					{
						if ( !from.Hidden )
						{ 
							from.FixedParticles( 0x36BD, 20, 10, 5044, EffectLayer.Head );
							from.PlaySound( 0x307 );
							from.Hidden = true;
						} 
						else 
						{ 
							from.Hidden = false;
							from.FixedParticles( 0x36BD, 20, 10, 5044, EffectLayer.Head );
							from.PlaySound( 0x307 );
						} 
					}
					else if ( HideType == "Fire Hide" )
					{
						if ( !from.Hidden )
						{ 
							from.FixedParticles( 0x3709, 10, 30, 5052, EffectLayer.LeftFoot );
							from.PlaySound( 0x225 );
							from.Hidden = true;
						} 
						else 
						{ 
							from.Hidden = false;
							from.FixedParticles( 0x3709, 10, 30, 5052, EffectLayer.LeftFoot );
							from.PlaySound( 0x225 ); 
						} 
					}
					else if ( HideType == "Noble Hide" )
					{
						if ( !from.Hidden )
						{ 
							from.FixedParticles( 0x3709, 1, 30, 9965, 5, 7, EffectLayer.Waist );
							from.FixedParticles( 0x376A, 1, 30, 9502, 5, 3, EffectLayer.Waist );
							from.PlaySound( 0x244 );
							from.Hidden = true;
						} 
						else 
						{ 
							from.Hidden = false;
							from.FixedParticles( 0x3709, 1, 30, 9965, 5, 7, EffectLayer.Waist );
							from.FixedParticles( 0x376A, 1, 30, 9502, 5, 3, EffectLayer.Waist );
							from.PlaySound( 0x244 );
						} 
					}
					else if ( HideType == "Poison Hide" )
					{
						if ( !from.Hidden ) 
						{ 
							from.FixedParticles( 0x36CB, 1, 9, 9911, 67, 5, EffectLayer.Head ); 
							from.FixedParticles( 0x374A, 1, 17, 9502, 1108, 4, (EffectLayer)255 ); 
							from.PlaySound( 0x22F ); 
							from.Hidden = true; 
						} 
						else 
						{ 
							from.Hidden = false; 
							from.FixedParticles( 0x36CB, 1, 9, 9911, 67, 5, EffectLayer.Head ); 
							from.FixedParticles( 0x374A, 1, 17, 9502, 1108, 4, (EffectLayer)255 ); 
							from.PlaySound( 0x22F );
						} 
					}
					else if ( HideType == "Shiney Hide" )
					{
						if ( !from.Hidden ) 
						{ 
							from.FixedParticles( 0x375A, 1, 30, 9966, 33, 2, EffectLayer.Head ); 
							from.FixedParticles( 0x37B9, 1, 30, 9502, 43, 3, EffectLayer.Head ); 
							from.PlaySound( 0x0F5 ); 
							from.PlaySound( 0x1ED ); 
							from.Hidden = true; 
						} 
						else 
						{ 
							from.Hidden = false; 
							from.FixedParticles( 0x375A, 1, 30, 9966, 33, 2, EffectLayer.Head ); 
							from.FixedParticles( 0x37B9, 1, 30, 9502, 43, 3, EffectLayer.Head ); 
							from.PlaySound( 0x0F5 ); 
							from.PlaySound( 0x1ED ); 
						} 
					}
					else if ( HideType == "Thunder Hide" )
					{

						if ( !from.Hidden )
						{ 
							from.BoltEffect( 0 );
							from.Hidden = true;
						} 
						else 
						{ 
							from.Hidden = false;
							from.BoltEffect ( 0 );
						} 
					}
					else if ( HideType == "Wither Hide" )
					{
						if ( !from.Hidden )
						{ 
							from.FixedParticles( 0x37CC, 1, 40, 97, 3, 9917, EffectLayer.Waist );
							from.FixedParticles( 0x374A, 1, 15, 9502, 97, 3, (EffectLayer)255 );
							from.PlaySound( 0x1FB );
							from.Hidden = true;
						} 
						else 
						{ 
							from.Hidden = false;
							from.FixedParticles( 0x37CC, 1, 40, 97, 3, 9917,  EffectLayer.Waist );
							from.FixedParticles( 0x374A, 1, 15, 9502, 97, 3,  (EffectLayer)255 );
							from.PlaySound( 0x10B );
						} 
					}

					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}
				case 10:
				{
					if( (from.HasGump( typeof( ItemMove ))) || (from.HasGump( typeof( MobileMove))))
					{
						from.SendMessage("You must close all Item/Mobile movers before you do this!");
					}
					else
					{
						from.Target = new ItemMobileMoveTarget( this );
						from.SendMessage("Select an item/mobile.");
					}

					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}
				case 11:
				{
					if( from.HasGump( typeof( CreatureControl )))
					{
						from.SendMessage("You must close all Creature Controls before you do this!");
					}
					else
					{
						from.Target = new ControlTarget( this );
						from.SendMessage("Select a creature.");
					}

					from.SendGump( new CommandToolbarGump( item ) );
					break;
				}
			}
		}
	}

	public class CommandToolbarEditMenu : Gump
	{
		public CommandToolbarStone stone;
		public bool HideToolbarEnabled;
		public bool AdvToolbarEnabled;
		public string HideType;
		public string Command1;
		public string Command2;
		public string Command3;
		public string Command4;
		public string Command5;
		public string Command6;
		public string Command7;

		public CommandToolbarEditMenu( Item item ) : base( 150, 150 )
		{
			CommandToolbarStone cmditem = item as CommandToolbarStone;
			this.stone = cmditem;

			this.HideToolbarEnabled = cmditem.HideToolbarEnabled;
			this.AdvToolbarEnabled = cmditem.AdvToolbarEnabled;

			this.HideType = cmditem.HideType;

			this.Command1 = cmditem.Command1;
			this.Command2 = cmditem.Command2;
			this.Command3 = cmditem.Command3;
			this.Command4 = cmditem.Command4;
			this.Command5 = cmditem.Command5;
			this.Command6 = cmditem.Command6;
			this.Command7 = cmditem.Command7;

			this.Closable=false;
			this.Disposable=false;
			this.Dragable=true;
			this.Resizable=false;

			this.AddPage(0);

			this.AddPage(1);

			this.AddBackground(0, 0, 675, 535, 9270);
			this.AddLabel(226, 40, 2101, @"Command Toolbar Stone Edit Menu");

			this.AddLabel(60, 90, 2101, @"Command 1:");
			this.AddBackground(145, 83, 462, 33, 2620);
			this.AddLabel(60, 130, 2101, @"Command 2:");
			this.AddBackground(145, 123, 462, 33, 2620);
			this.AddLabel(60, 170, 2101, @"Command 3:");
			this.AddBackground(145, 163, 462, 33, 2620);
			this.AddLabel(60, 210, 2101, @"Command 4:");
			this.AddBackground(145, 203, 462, 33, 2620);
			this.AddLabel(60, 250, 2101, @"Command 5:");
			this.AddBackground(145, 243, 462, 33, 2620);
			this.AddLabel(60, 290, 2101, @"Command 6:");
			this.AddBackground(145, 283, 462, 33, 2620);
			this.AddLabel(60, 330, 2101, @"Command 7:");
			this.AddBackground(145, 323, 462, 33, 2620);

			this.AddLabel(148, 373, 2101, @"Hide Toolbar Enabled:");
			this.AddLabel(148, 403, 2101, @"Adv. Toolbar Enabled:");

			this.AddLabel(406, 373, 2101, @"Hide Type");
			this.AddBackground(367, 399, 143, 26, 9350);
			this.AddButton(511, 403, 5601, 5605, 13, GumpButtonType.Page, 2);

			this.AddLabel(188, 443, 2101, @"Created by Joeku AKA Demortris and Rothhear");

			this.AddButton(165, 480, 246, 244, 8, GumpButtonType.Reply, 1);
			this.AddButton(300, 480, 239, 240, 9, GumpButtonType.Reply, 1);
			this.AddButton(435, 480, 243, 241, 10, GumpButtonType.Reply, 1);
		}

		public override void OnResponse( NetState state, RelayInfo info )
        	{ 
			Mobile from = state.Mobile;
			CommandToolbarStone item = this.stone;

			if ( item == null )
				from.SendMessage( "This command toolbar stone does not exist." );
			else if ( !item.IsChildOf( from.Backpack ))
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
			if ( from.AccessLevel < AccessLevel.GameMaster )
				from.SendMessage( "Only Game Masters can use this." );
			else
			{
				switch ( info.ButtonID ) 
				{
					case 8:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
							gump.AddTextEntry(153, 90, 445, 20, 2101, 1, @"Props" );
							gump.AddTextEntry(153, 130, 445, 20, 2101, 2, @"M Tele" );
							gump.AddTextEntry(153, 170, 445, 20, 2101, 3, @"Move" );
							gump.AddTextEntry(153, 210, 445, 20, 2101, 4, @"M Delete" );
							gump.AddTextEntry(153, 250, 445, 20, 2101, 5, @"Wipe" );
							gump.AddTextEntry(153, 290, 445, 20, 2101, 6, @"Who" );
							gump.AddTextEntry(153, 330, 445, 20, 2101, 7, @"Add" );
							gump.HideToolbarEnabled = true;
							gump.AdvToolbarEnabled = true;
							gump.HideType = "Normal Hide";
							gump.AddButton(287, 369, 2153, 2151, 11, GumpButtonType.Reply, 1);
							gump.AddButton(287, 399, 2153, 2151, 12, GumpButtonType.Reply, 1);
							gump.AddLabel(374, 403, 0, gump.HideType);

							gump.AddPage(2);

							gump.AddBackground(0, 0, 273, 545, 9270);
							gump.AddLabel(75, 39, 2101, @"Hide Type Edit Menu");

							gump.AddBackground(64, 83, 131, 26, 9350);
							gump.AddLabel(72, 87, 0, @"Normal Hide");
							gump.AddButton(196, 87, 5601, 5605, 14, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 123, 131, 26, 9350);
							gump.AddLabel(72, 127, 0, @"Blood Oath Hide");
							gump.AddButton(196, 127, 5601, 5605, 15, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 163, 131, 26, 9350);
							gump.AddLabel(72, 167, 0, @"Divine Male Hide");
							gump.AddButton(196, 167, 5601, 5605, 16, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 203, 131, 26, 9350);
							gump.AddLabel(72, 207, 0, @"Divine Female Hide");
							gump.AddButton(196, 207, 5601, 5605, 17, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 243, 131, 26, 9350);
							gump.AddLabel(72, 247, 0, @"Explosion Hide");
							gump.AddButton(196, 247, 5601, 5605, 18, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 283, 131, 26, 9350);
							gump.AddLabel(72, 287, 0, @"Fire Hide");
							gump.AddButton(196, 287, 5601, 5605, 19, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 323, 131, 26, 9350);
							gump.AddLabel(72, 327, 0, @"Noble Hide");
							gump.AddButton(196, 327, 5601, 5605, 20, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 363, 131, 26, 9350);
							gump.AddLabel(72, 367, 0, @"Poison Hide");
							gump.AddButton(196, 367, 5601, 5605, 21, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 403, 131, 26, 9350);
							gump.AddLabel(72, 407, 0, @"Shiney Hide");
							gump.AddButton(196, 407, 5601, 5605, 22, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 443, 131, 26, 9350);
							gump.AddLabel(72, 447, 0, @"Thunder Hide");
							gump.AddButton(196, 447, 5601, 5605, 23, GumpButtonType.Reply, 2);
							gump.AddBackground(64, 483, 131, 26, 9350);
							gump.AddLabel(72, 487, 0, @"Wither Hide");
							gump.AddButton(196, 487, 5601, 5605, 24, GumpButtonType.Reply, 2);
						from.SendGump( gump );
						from.SendMessage( "Command entries have been changed to default." );

						break;
					}

					case 9:
					{
						TextRelay text1 = info.GetTextEntry( 1 );
						string NewCommand1 = text1.Text;
						TextRelay text2 = info.GetTextEntry( 2 );
						string NewCommand2 = text2.Text;
						TextRelay text3 = info.GetTextEntry( 3 );
						string NewCommand3 = text3.Text;
						TextRelay text4 = info.GetTextEntry( 4 );
						string NewCommand4 = text4.Text;
						TextRelay text5 = info.GetTextEntry( 5 );
						string NewCommand5 = text5.Text;
						TextRelay text6 = info.GetTextEntry( 6 );
						string NewCommand6 = text6.Text;
						TextRelay text7 = info.GetTextEntry( 7 );
						string NewCommand7 = text7.Text;

						if( NewCommand1 != null && NewCommand2 != null && NewCommand3 != null && NewCommand4 != null && NewCommand5 != null && NewCommand6 != null && NewCommand7 != null )
						{
							item.HideToolbarEnabled = HideToolbarEnabled;
							item.AdvToolbarEnabled = AdvToolbarEnabled;
							item.HideType = HideType;

							item.Command1 = NewCommand1;
							item.Command2 = NewCommand2;
							item.Command3 = NewCommand3;
							item.Command4 = NewCommand4;
							item.Command5 = NewCommand5;
							item.Command6 = NewCommand6;
							item.Command7 = NewCommand7;

							from.SendMessage( "Your new settings have been successfully applied." );
							from.CloseGump( typeof( CommandToolbarGump ) );
						}
						else
							from.SendMessage( "One of your text entries is invalid. Please try again." );

						break;
					}

					case 11:
					{
						if( HideToolbarEnabled )
						{
							CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
							gump.HideToolbarEnabled = false;
							gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
							gump.HideType = this.HideType;
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
							AddGumpProperties( gump );
							from.SendGump( gump );
						}
						else
						{
							CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
							gump.HideToolbarEnabled = true;
							gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
							gump.HideType = this.HideType;
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
							AddGumpProperties( gump );
							from.SendGump( gump );
						}
						
						break;
					}

					case 12:
					{
						if( AdvToolbarEnabled )
						{
							CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
							gump.AdvToolbarEnabled = false;
							gump.HideToolbarEnabled = this.HideToolbarEnabled;
							gump.HideType = this.HideType;
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
							AddGumpProperties( gump );
							from.SendGump( gump );
						}
						else
						{
							CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
							gump.AdvToolbarEnabled = true;
							gump.HideToolbarEnabled = this.HideToolbarEnabled;
							gump.HideType = this.HideType;
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
							AddGumpProperties( gump );
							from.SendGump( gump );
						}
						
						break;
					}

					case 14:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Normal Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 15:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Blood Oath Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 16:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Divine Male Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 17:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Divine Female Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 18:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Explosion Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 19:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Fire Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 20:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Noble Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 21:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Poison Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 22:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Shiney Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 23:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Thunder Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}

					case 24:
					{
						CommandToolbarEditMenu gump = new CommandToolbarEditMenu( item );
						gump.HideToolbarEnabled = this.HideToolbarEnabled;
						gump.AdvToolbarEnabled = this.AdvToolbarEnabled;
						gump.HideType = "Wither Hide";
							TextRelay text1 = info.GetTextEntry( 1 );
							string NewCommand1 = text1.Text;
							gump.Command1 = NewCommand1;
							TextRelay text2 = info.GetTextEntry( 2 );
							string NewCommand2 = text2.Text;
							gump.Command2 = NewCommand2;
							TextRelay text3 = info.GetTextEntry( 3 );
							string NewCommand3 = text3.Text;
							gump.Command3 = NewCommand3;
							TextRelay text4 = info.GetTextEntry( 4 );
							string NewCommand4 = text4.Text;
							gump.Command4 = NewCommand4;
							TextRelay text5 = info.GetTextEntry( 5 );
							string NewCommand5 = text5.Text;
							gump.Command5 = NewCommand5;
							TextRelay text6 = info.GetTextEntry( 6 );
							string NewCommand6 = text6.Text;
							gump.Command6 = NewCommand6;
							TextRelay text7 = info.GetTextEntry( 7 );
							string NewCommand7 = text7.Text;
							gump.Command7 = NewCommand7;
						AddGumpProperties( gump );
						from.SendGump( gump );
						
						break;
					}
				}
			}
		}

		public virtual void AddGumpProperties( CommandToolbarEditMenu gump )
		{
			gump.AddTextEntry(153, 90, 445, 20, 2101, 1, gump.Command1 );
			gump.AddTextEntry(153, 130, 445, 20, 2101, 2, gump.Command2 );
			gump.AddTextEntry(153, 170, 445, 20, 2101, 3, gump.Command3 );
			gump.AddTextEntry(153, 210, 445, 20, 2101, 4, gump.Command4 );
			gump.AddTextEntry(153, 250, 445, 20, 2101, 5, gump.Command5 );
			gump.AddTextEntry(153, 290, 445, 20, 2101, 6, gump.Command6 );
			gump.AddTextEntry(153, 330, 445, 20, 2101, 7, gump.Command7 );
			if( !gump.HideToolbarEnabled )
				gump.AddButton(287, 369, 2151, 2153, 11, GumpButtonType.Reply, 1);
			else
				gump.AddButton(287, 369, 2153, 2151, 11, GumpButtonType.Reply, 1);
			if( !gump.AdvToolbarEnabled )
				gump.AddButton(287, 399, 2151, 2153, 12, GumpButtonType.Reply, 1);
			else
				gump.AddButton(287, 399, 2153, 2151, 12, GumpButtonType.Reply, 1);
			gump.AddLabel(374, 403, 0, gump.HideType);

			gump.AddPage(2);

			gump.AddBackground(0, 0, 273, 545, 9270);
			gump.AddLabel(75, 39, 2101, @"Hide Type Edit Menu");

			gump.AddBackground(64, 83, 131, 26, 9350);
			gump.AddLabel(72, 87, 0, @"Normal Hide");
			gump.AddButton(196, 87, 5601, 5605, 14, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 123, 131, 26, 9350);
			gump.AddLabel(72, 127, 0, @"Blood Oath Hide");
			gump.AddButton(196, 127, 5601, 5605, 15, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 163, 131, 26, 9350);
			gump.AddLabel(72, 167, 0, @"Divine Male Hide");
			gump.AddButton(196, 167, 5601, 5605, 16, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 203, 131, 26, 9350);
			gump.AddLabel(72, 207, 0, @"Divine Female Hide");
			gump.AddButton(196, 207, 5601, 5605, 17, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 243, 131, 26, 9350);
			gump.AddLabel(72, 247, 0, @"Explosion Hide");
			gump.AddButton(196, 247, 5601, 5605, 18, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 283, 131, 26, 9350);
			gump.AddLabel(72, 287, 0, @"Fire Hide");
			gump.AddButton(196, 287, 5601, 5605, 19, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 323, 131, 26, 9350);
			gump.AddLabel(72, 327, 0, @"Noble Hide");
			gump.AddButton(196, 327, 5601, 5605, 20, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 363, 131, 26, 9350);
			gump.AddLabel(72, 367, 0, @"Poison Hide");
			gump.AddButton(196, 367, 5601, 5605, 21, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 403, 131, 26, 9350);
			gump.AddLabel(72, 407, 0, @"Shiney Hide");
			gump.AddButton(196, 407, 5601, 5605, 22, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 443, 131, 26, 9350);
			gump.AddLabel(72, 447, 0, @"Thunder Hide");
			gump.AddButton(196, 447, 5601, 5605, 23, GumpButtonType.Reply, 2);
			gump.AddBackground(64, 483, 131, 26, 9350);
			gump.AddLabel(72, 487, 0, @"Wither Hide");
			gump.AddButton(196, 487, 5601, 5605, 24, GumpButtonType.Reply, 2);
		}
	}

	public class ItemMobileMoveTarget : Target
	{
		private CommandToolbarGump m_Gump;

		public ItemMobileMoveTarget( CommandToolbarGump gump ) : base( 20, true, TargetFlags.None )
		{
			m_Gump = gump;
		}

		protected override void OnTarget( Mobile from, object to )
		{
			if ( to is Item )
			{
				Item Controlled = to as Item;

				if ( Controlled.IsChildOf( from.Backpack ) )
				{
					from.SendMessage("You cannot move an item that is in a container.");
					return;
				}

				from.SendMessage("Item/Mobile mover initiated.");
				from.SendGump ( new ItemMove( to, from ) );
            		}
			else if( to is Mobile )
			{
				Mobile controlled = to as Mobile;

				if( controlled.AccessLevel >= from.AccessLevel )
				{
					from.SendMessage( "You cannot do that.");
					return;
				}

				from.SendMessage("Item/Mobile mover initiated.");
				from.SendGump ( new MobileMove( to, from ) );
				controlled.Frozen = true;
			}
			else
				from.SendMessage("Invalid target. Only mobiles and items can be moved.");
		}
	}

	public class ItemMove : Gump
	{
		private object m_to;

		public ItemMove( object to, Mobile from ) : base( 200, 150 )
		{
			m_to = to;
			this.Closable=true;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddImage(174, 125, 5010);
			this.AddButton(283, 167, 4501, 4501, 1, GumpButtonType.Reply, 0);
			this.AddButton(279, 233, 4503, 4503, 2, GumpButtonType.Reply, 0);
			this.AddButton(217, 233, 4505, 4505, 3, GumpButtonType.Reply, 0);
			this.AddButton(217, 167, 4507, 4507, 4, GumpButtonType.Reply, 0);
			this.AddButton(250, 154, 4500, 4500, 5, GumpButtonType.Reply, 0);
			this.AddButton(295, 199, 4502, 4502, 6, GumpButtonType.Reply, 0);
			this.AddButton(250, 245, 4504, 4504, 7, GumpButtonType.Reply, 0);
			this.AddButton(204, 199, 4506, 4506, 8, GumpButtonType.Reply, 0);
			this.AddButton(143, 168, 4500, 4500, 9, GumpButtonType.Reply, 0);
			this.AddImage(244, 194, 5582);
			this.AddButton(143, 228, 4504, 4504, 10, GumpButtonType.Reply, 0);
			this.AddButton(350, 227, 4504, 4504, 11, GumpButtonType.Reply, 0);
			this.AddButton(349, 168, 4500, 4500, 12, GumpButtonType.Reply, 0);
			this.AddLabel(156, 147, 132, @"1 Up");
			this.AddLabel(147, 276, 132, @"1 Down");
			this.AddLabel(358, 148, 132, @"5 Up");
			this.AddLabel(354, 277, 132, @"5 Down");
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			Mobile from = state.Mobile;
			Item to = m_to as Item;

			switch ( info.ButtonID )
			{
				case 0:
				{
					from.SendMessage("Item/Mobile mover terminated.");
					break;
				}
				case 1:
				{
					to.Y = ( to.Y - 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 2:
				{
					to.X = ( to.X + 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 3:
				{
					to.Y = ( to.Y + 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 4:
				{
					to.X = ( to.X - 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 5:
				{
					to.X = ( to.X - 1);
					to.Y = ( to.Y - 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
				        break;
				}
				case 6:
				{
					to.X = ( to.X + 1);
					to.Y = ( to.Y - 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
				        break;
				}
				case 7:
				{
					to.Y = ( to.Y + 1);
					to.X = ( to.X + 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
				        break;
				}
				case 8:
				{
					to.X = ( to.X - 1);
					to.Y = ( to.Y + 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 9:
				{
					to.Direction = Direction.Up;
					to.Z = ( to.Z + 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 10:
				{
					to.Direction = Direction.Down;
					to.Z = ( to.Z - 1);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 11:
				{
					to.Direction = Direction.Up;
					to.Z = ( to.Z - 5);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 12:
				{
					to.Direction = Direction.Up;
					to.Z = ( to.Z + 5);
					from.SendGump ( new ItemMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
			}
		}
	}

	public class MobileMove : Gump
	{
		private object m_to;

		public MobileMove( object to, Mobile from ) : base( 200, 150 )
		{
			m_to = to;
			this.Closable=true;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddImage(174, 125, 5010);
			this.AddButton(283, 167, 4501, 4501, 1, GumpButtonType.Reply, 0);
			this.AddButton(279, 233, 4503, 4503, 2, GumpButtonType.Reply, 0);
			this.AddButton(217, 233, 4505, 4505, 3, GumpButtonType.Reply, 0);
			this.AddButton(217, 167, 4507, 4507, 4, GumpButtonType.Reply, 0);
			this.AddButton(250, 154, 4500, 4500, 5, GumpButtonType.Reply, 0);
			this.AddButton(295, 199, 4502, 4502, 6, GumpButtonType.Reply, 0);
			this.AddButton(250, 245, 4504, 4504, 7, GumpButtonType.Reply, 0);
			this.AddButton(204, 199, 4506, 4506, 8, GumpButtonType.Reply, 0);
			this.AddButton(143, 168, 4500, 4500, 9, GumpButtonType.Reply, 0);
			this.AddButton(143, 228, 4504, 4504, 10, GumpButtonType.Reply, 0);
			this.AddButton(350, 227, 4504, 4504, 11, GumpButtonType.Reply, 0);
			this.AddButton(349, 168, 4500, 4500, 12, GumpButtonType.Reply, 0);
			this.AddImage(244, 194, 5582);
			this.AddLabel(156, 147, 132, @"1 Up");
			this.AddLabel(147, 276, 132, @"1 Down");
			this.AddLabel(358, 148, 132, @"5 Up");
			this.AddLabel(354, 277, 132, @"5 Down");
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			Mobile from = state.Mobile;
			Mobile to = m_to as Mobile;

			switch ( info.ButtonID )
			{
				case 0:
				{
					from.SendMessage("Item/Mobile mover terminated.");
					to.Frozen = false;
					break;
				}
				case 1:
				{
					to.Y = ( to.Y - 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 2:
				{
					to.X = ( to.X + 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 3:
				{
					to.Y = ( to.Y + 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 4:
				{
					to.X = ( to.X - 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 5:
				{
					to.X = ( to.X - 1);
					to.Y = ( to.Y - 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
				        break;
				}
				case 6:
				{
					to.X = ( to.X + 1);
					to.Y = ( to.Y - 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
				        break;
				}
				case 7:
				{
					to.Y = ( to.Y + 1);
					to.X = ( to.X + 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
				        break;
				}
				case 8:
				{
					to.X = ( to.X - 1);
					to.Y = ( to.Y + 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 9:
				{
					to.Direction = Direction.Up;
					to.Z = ( to.Z + 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 10:
				{
					to.Direction = Direction.Down;
					to.Z = ( to.Z - 1);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 11:
				{
					to.Direction = Direction.Up;
					to.Z = ( to.Z - 5);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
				case 12:
				{
					to.Direction = Direction.Up;
					to.Z = ( to.Z + 5);
					from.SendGump ( new MobileMove( to, from ) );
					from.SendMessage( "New location: {0}", to.Location );
					break;
				}
			}
		}
	}

	public class ControlTarget : Target
	{
		private CommandToolbarGump m_Gump;

		public ControlTarget( CommandToolbarGump gump ) : base( 20, true, TargetFlags.None )
		{
			m_Gump = gump;
		}

		protected override void OnTarget( Mobile from, Object to )
		{
			if ( to is BaseCreature )
			{
				BaseCreature Controlled = to as BaseCreature;

				from.SendMessage("Now controlling " + Controlled.Name);
				Controlled.CantWalk = true;
				from.SendGump ( new CreatureControl( Controlled, from ) );
			}
			else
				from.SendMessage("Invalid target. Only creatures can be controlled.");
		}
	}

	public class CombatTarget : Target
	{
		private BaseAI m_AI;
		private BaseCreature creature;

		public CombatTarget( BaseAI ai, BaseCreature bc ) : base( 10, false, TargetFlags.None )
		{
			m_AI = ai;
			creature = bc;
		}

		protected override void OnTarget( Mobile from, object targeted )
		{
			if ( targeted is Mobile )
			{
				Mobile m = ((Mobile)targeted);

				if ( m == creature )
				{
					from.SendMessage("It cannot attack itself!");
					from.SendGump ( new CreatureControl( creature, from ) );
				}
				
				else
				{

					if ( creature.Alive )
					{
						m_AI.Action = ActionType.Combat;
						m_AI.NextMove = DateTime.Now;
						creature.Combatant = m;
						from.SendGump ( new CreatureControl( creature, from ) );	
					}
				}
			}

			else
			{
		    		from.SendMessage("You cannot attack that!");
				from.SendGump ( new CreatureControl( creature, from ) );
			}
		}

	}

	public class CreatureControl : Gump
	{
		private BaseCreature m_to;
		private BaseAI m_ai; 

		public void AddTextField( int x, int y, int width, int height, int index )
		{
			AddBackground( x - 2, y - 2, width + 4, height + 4, 0x2486 );
			AddTextEntry( x + 2, y + 2, width - 4, height - 4, 0, index, "" );
		}

		public CreatureControl( BaseCreature to, Mobile from ) : base( 200, 150 )
		{
			m_to = to;
			this.Closable=true;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddImage(167, 119, 9007);
			this.AddImage(227, 180, 1417);
			this.AddButton(236, 189, 5582, 5582, 9, GumpButtonType.Reply, 0);
			this.AddButton(276, 161, 4501, 4501, 1, GumpButtonType.Reply, 0);
			this.AddButton(272, 227, 4503, 4503, 2, GumpButtonType.Reply, 0);
			this.AddButton(210, 227, 4505, 4505, 3, GumpButtonType.Reply, 0);
			this.AddButton(210, 161, 4507, 4507, 4, GumpButtonType.Reply, 0);
			this.AddButton(243, 148, 4500, 4500, 5, GumpButtonType.Reply, 0);
			this.AddButton(288, 193, 4502, 4502, 6, GumpButtonType.Reply, 0);
			this.AddButton(243, 239, 4504, 4504, 7, GumpButtonType.Reply, 0);
			this.AddButton(197, 193, 4506, 4506, 8, GumpButtonType.Reply, 0);
			this.AddButton(236, 189, 5582, 5582, 9, GumpButtonType.Reply, 0);
			this.AddButton(377, 321, 2141, 2142, 10, GumpButtonType.Reply, 0);
			this.AddTextField( 185, 315, 190, 61, 0 );
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			TextRelay sayEntry = info.GetTextEntry( 0 );
			string text = ( sayEntry == null ? null : sayEntry.Text.Trim() );
			Mobile from = state.Mobile;
			BaseCreature toMobile = m_to;
			
			if( toMobile.Alive )
			{
				m_ai = toMobile.AIObject;

				toMobile.CantWalk = false;

				m_ai.NextMove = DateTime.Now + TimeSpan.FromSeconds( 60 );
				DateTime delay = DateTime.Now + TimeSpan.FromSeconds( 60 );

				if ( info.ButtonID == 0 )
				{
					from.SendMessage("You finish controlling " + toMobile.Name );
					m_ai.NextMove = DateTime.Now;	
				}

				if ( info.ButtonID == 1 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.North );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );	
				}

				if ( info.ButtonID == 2 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.East );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );
				}

				if ( info.ButtonID == 3 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.South );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );
				}	

				if ( info.ButtonID == 4 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.West );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );
				}

				if ( info.ButtonID == 5 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.Up );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );
				}

				if ( info.ButtonID == 6 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.Right );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );	
				}

				if ( info.ButtonID == 7 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.Down );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );	
				}

				if ( info.ButtonID == 8 )
				{
					m_ai.NextMove = DateTime.Now;
					m_ai.DoMove( Direction.Left );
					m_ai.NextMove = delay;
					from.SendGump ( new CreatureControl( toMobile, from ) );	
				}

				if ( info.ButtonID == 9 )
				{
					from.SendMessage("Who would you like this creature to attack?");
					from.Target = new CombatTarget( m_ai, toMobile );
				}

				if ( info.ButtonID == 10 )
				{
                    			toMobile.Say( text );
					from.SendGump ( new CreatureControl( toMobile, from ) );
				}
			}

		}
	
	}
}
