using System.Text.RegularExpressions;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.IO;
using System.Diagnostics;
using Crepuscule.Update.Engine;
using Crepuscule.Update;

namespace Server.Scripts.Commands
{
    /// <summary>
    /// Function .Patch, allowing a GM to notify something to a player
    /// </summary>
    public class Patch_Command
    {
        /// <summary>
        /// Handler initialization
        /// </summary>
        public static void Initialize()
        {
            Server.Commands.Register("Patch", AccessLevel.Administrator, new CommandEventHandler(Patch_OnCommand));
        }

        [Usage("Patch")]
        [Description("Cr�ation du patch du jeu, apr�s les freeze.")]
        private static void Patch_OnCommand(CommandEventArgs e)
        {
            UpdateStatics();
        }

        public static void UpdateStatics()
        {
            World.Broadcast(15, true, "Creation du patch en cours...");
            Packager.CreatePackage();
            World.Broadcast(15, true, "Nouvelle version sera bientot disponible!");
        }


    }
}
