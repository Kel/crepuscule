using Server.Mobiles;
using Server.Targeting;
using System;
using System.Collections;

namespace Server.Mobiles
{
	public class PossessCommand
	{
	
		public static ArrayList PossessList = new ArrayList();
		
		public static void Initialize()
		{
			Server.Commands.Register( "Possess", AccessLevel.GameMaster, new CommandEventHandler( Possess_OnCommand ) );
			Server.Commands.Register( "UnPossess", AccessLevel.GameMaster, new CommandEventHandler( UnPossess_OnCommand ) );
		}
		
		[Usage( "Possess" )]
		[Description( "S�lectionnez la cr�ature � poss�der." )]
		private static void Possess_OnCommand( CommandEventArgs e )
		{
			Mobile mobile = (Mobile) e.Mobile;

			mobile.SendMessage( "S�lectionnez la cr�ature � poss�der." );
			mobile.Target = new InternalTarget();
		}
		
		[Usage( "UnPossess" )]
		[Description( "Vous quittez la possession." )]
		private static void UnPossess_OnCommand( CommandEventArgs e )
		{
			Mobile from = (Mobile) e.Mobile;
			Unpossess( from );
		}
		
		private static void Unpossess( Mobile from )
		{
			UnpossessDeath( from, true );
		}
		
		public static bool UnpossessDeath( Mobile from )
		{
			return UnpossessDeath( from, false );
		}
		
		private static bool UnpossessDeath( Mobile from, bool alive )
		{
			bool allReadyPossess = false;
			PossessLink check = null;
			
			for ( int i = 0; i < PossessList.Count; i++ )
			{
				check = (PossessLink) PossessList[i];
				if ( check.getFrom() == from )
				{
					allReadyPossess = true;
					break;
				}
			}	
			
			if ( allReadyPossess && check != null )
			{
				from.SendMessage( "Vous perdez votre emprise sur la cr�ature." );
				check.UnPossess( alive );
				PossessList.Remove( check );
			}
			
			return !allReadyPossess;
		}
		
		private class InternalTarget : Target
		{
			public InternalTarget() : base( -1, false, TargetFlags.None )
			{
			}

			protected override void OnTarget( Mobile from, object targeted )
			{
				bool allReadyPossess = false;
				PossessLink check;
				
				for ( int i = 0; i < PossessList.Count; i++ )
				{
			 		check = (PossessLink) PossessList[i];
			 		if ( check.getFrom() == from )
			 		{
						allReadyPossess = true;
						break;
					}
				}
							
				if ( allReadyPossess )
					from.SendMessage( "Vous poss�dez d�j� une cr�ature." );
					
				else if ( targeted is BaseCreature )
				{
					BaseCreature to = (BaseCreature) targeted;
					
					if ( to.Summoned )
						from.SendMessage( "Vous ne pouvez pas poss�der une cr�ature invoqu�e." );
					else
					{
						PossessList.Add( new PossessLink( from, to ) );
						from.SendMessage( "Vous prennez possession de la cr�ature." );
					}
				}
				
				else
					from.SendMessage( "Vous ne pouvez pas poss�der ceci." );
			}
		}
		
		private class PossessLink
		{
			private BaseCreature m_to;
			private Mobile m_from;
			private BaseCreature m_save;
			private bool m_invul;
			
			private static Layer[] m_layer = new Layer[]
			{
				Layer.FirstValid,
				Layer.TwoHanded,
				Layer.Shoes,
				Layer.Pants,
				Layer.Shirt,
				Layer.Helm,
				Layer.Gloves,
				Layer.Ring,
				Layer.Talisman,
				Layer.Neck,
				Layer.Hair,
				Layer.Waist,
				Layer.InnerTorso,
				Layer.Bracelet,
				Layer.Unused_xF,
				Layer.FacialHair,
				Layer.MiddleTorso,
				Layer.Earrings,
				Layer.Arms,
				Layer.Cloak,
				Layer.OuterTorso,
				Layer.OuterLegs,
				Layer.LastUserValid
			};
			
			private static SkillName[] m_skillname = new SkillName[]
			{
				SkillName.Alchemy, 
				SkillName.Anatomy,
				SkillName.AnimalLore,
				SkillName.AnimalTaming,
				SkillName.Archery,
				SkillName.ArmsLore,
				SkillName.Begging,
				SkillName.Blacksmith,
				SkillName.Camping,
				SkillName.Carpentry,
				SkillName.Cartography,
				SkillName.Chivalry,
				SkillName.Cooking,
				SkillName.DetectHidden,
				SkillName.Discordance,
				SkillName.EvalInt,
				SkillName.Fencing,
				SkillName.Fishing,
				SkillName.Fletching,
				SkillName.Focus,
				SkillName.Forensics,
				SkillName.Healing,
				SkillName.Herding,
				SkillName.Hiding,
				SkillName.Inscribe,
				SkillName.ItemID,
				SkillName.Lockpicking, 
				SkillName.Lumberjacking,
				SkillName.Macing,
				SkillName.Magery,
				SkillName.MagicResist,
				SkillName.Meditation,
				SkillName.Mining,
				SkillName.Musicianship,
				SkillName.Necromancy,
				SkillName.Parry,
				SkillName.Peacemaking,
				SkillName.Poisoning,
				SkillName.Provocation,
				SkillName.RemoveTrap,
				SkillName.Snooping,
				SkillName.SpiritSpeak,
				SkillName.Stealing,
				SkillName.Stealth,
				SkillName.Swords,
				SkillName.Tactics,
				SkillName.Tailoring,
				SkillName.TasteID,
				SkillName.Tinkering,
				SkillName.Tracking,
				SkillName.Veterinary,			
				SkillName.Wrestling
			};
			
			public PossessLink( Mobile from, BaseCreature to )
			{
				m_to = to;
				m_from = from;
				m_invul = to.Blessed;
				this.Initialize();
			}
			
			private void Initialize()
			{
				m_save = new BaseCreature( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4 );
				m_save.Frozen = true;
				
				this.xChange( m_from, m_save );
				this.xChange( m_to, m_from );
				
				m_from.Map = m_to.Map;
				m_from.Location = m_to.Location;
								
				m_to.Blessed = true;
				m_to.Internalize();
			}
			
			public void UnPossess( bool alive )
			{				
				this.xChange( m_from, m_to );
				this.xChange( m_save, m_from );
				
				m_save.Delete();
				
				m_from.Hidden = true;
				
				m_to.Map = m_from.Map;
				m_to.Location = m_from.Location;
				
				m_to.Blessed = m_invul;
				
				if ( !alive )
					m_to.Kill();
			}
			
			private void xChange( Mobile from, Mobile to )
			{
				to.BaseSoundID = from.BaseSoundID;
				to.BodyValue = from.BodyValue;
				to.Criminal = from.Criminal;
				to.Dex = from.Dex;	
				to.Direction = from.Direction;
				to.Fame = from.Fame;
				to.Female = from.Female;
				to.Hidden = from.Hidden;
				to.Hits = from.Hits;
				to.Hue = from.Hue;
				to.Int = from.Int;
				to.Karma = from.Karma;
				to.Kills = from.Kills;
				to.Mana = from.Mana;
				to.Name = from.Name;
				to.RawDex = from.RawDex;
				to.RawInt = from.RawInt;
				to.RawStr = from.RawStr;
				to.SpeechHue = from.SpeechHue;
				to.Stam = from.Stam;
				to.Str = from.Str;
				to.Title = from.Title;
				
				
				for ( int i = 0; i < m_layer.Length; i++ )
					xChangeFromLayer( from, to, m_layer[i] );
				
				for ( int i = 0; i < m_skillname.Length; i++ )
					to.Skills[m_skillname[i]].BaseFixedPoint = from.Skills[m_skillname[i]].BaseFixedPoint; 
			}
			
			private void xChangeFromLayer( Mobile from, Mobile to, Layer layer ) 
			{
				if ( from.FindItemOnLayer( layer ) != null )
					to.AddItem( from.FindItemOnLayer( layer ) );
			}
			
			public BaseCreature getTo()
			{
				return m_to;
			}
			
			public Mobile getFrom()
			{
				return m_from;
			}
			
			public BaseCreature getSave()
			{
				return m_save;
			}
		}
	}
}