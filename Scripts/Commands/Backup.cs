using System.Text.RegularExpressions;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.IO;
using System.Diagnostics;
using Crepuscule.Update.Engine;
using Crepuscule.Update;
using Server.Engines.RemoteAdmin;

namespace Server.Scripts.Commands
{
    public class Backup_Command
    {
        /// <summary>
        /// Handler initialization
        /// </summary>
        public static void Initialize()
        {
            Server.Commands.Register("Backup", AccessLevel.Administrator, new CommandEventHandler(Backup_OnCommand));
        }

        [Usage("Backup")]
        [Description("Cr�ation du Backup du jeu.")]
        private static void Backup_OnCommand(CommandEventArgs e)
        {
            World.Broadcast(15, false, "Creation du Backup initialis�e...");
            BackupManager.BackupServer();
        }


    }
}
