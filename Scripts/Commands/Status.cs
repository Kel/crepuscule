using System.Text.RegularExpressions;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using Server.Mobiles;
using Server.Web;

namespace Server.Scripts.Commands
{
	/// <summary>
	/// Function .tip, allowing a GM to notify something to a player
	/// </summary>
	public class StatusCommand
	{
		/// <summary>
		/// Handler initialization
		/// </summary>
		public static void Initialize()
		{
			Server.Commands.Register( "status", AccessLevel.Player, new CommandEventHandler( Status_OnCommand ) );
		}

		[Usage( "status" )]
		[Description( "Permet de savoir le nombre de joueurs connect�s" )]
		private static void Status_OnCommand(CommandEventArgs e)
		{
            e.Mobile.SendMessage("Il y a actuellement " + WebStatus.PlayersCount + " joueurs connect�s");
		}
	}
}
