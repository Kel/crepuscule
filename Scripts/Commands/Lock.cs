using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Scripts.Commands
{

	public class Lock
	{
		public static void Initialize()
		{
            Server.Commands.Register("verrou", AccessLevel.Player, new CommandEventHandler(Lock_OnCommand));
			
		}

        [Usage("verrou")]
		[Description( "Verrouille un objet au sol." )]
		private static void Lock_OnCommand( CommandEventArgs e )
		{
            if (e.Mobile.Alive)
            {
                e.Mobile.Target = new LockTarget(false);
                e.Mobile.SendMessage("Que voulez-vous verrouiller?");
            }
		}


		private class LockTarget : Target
		{
			private bool m_InBag;

			public LockTarget( bool inbag ) : base( 15, false, TargetFlags.None )
			{
				m_InBag = inbag;
				
			}

			protected override void OnTarget( Mobile from, object targ )
			{
				bool done = false;
				if ( !(targ is Item) )
				{
					from.SendMessage( "Vous ne pouvez verrouiller que les objets." );
					return;
				}

				
				Item copy = (Item)targ;
				Container pack;
				
				if ( m_InBag )
				{
					if ( copy.Parent is Container )
						pack = (Container)copy.Parent;
					else if ( copy.Parent is Mobile )
						pack = ((Mobile)copy.Parent).Backpack;
					else
						pack = null;
				}
				else
					pack = from.Backpack;

				Type t = copy.GetType();

				ConstructorInfo[] info = t.GetConstructors();

				foreach ( ConstructorInfo c in info )
				{
					
					ParameterInfo[] paramInfo = c.GetParameters();

					if ( paramInfo.Length == 0 )
					{
						object[] objParams = new object[0];

						try 
						{
							
							object o = c.Invoke( objParams );

							if ( o != null && o is Item )
							{
								Item newItem = (Item)o;
								//CopyProperties( newItem, copy );//copy.Lock( item, copy.Amount );
								
                                //newItem.Parent = null;

                                RacePlayerMobile pm = (RacePlayerMobile)from;

                                if (pm.InRange(copy.GetWorldLocation(), 0))
                                {
                                    from.SendMessage("L'objet doit �tre au sol");
                                }
                                else
                                {
                                    int compte = 0;
                                    foreach (Mobile m in copy.GetMobilesInRange(0))
                                    {
                                        compte++;
                                    }
                                    if (pm.InRange(copy.GetWorldLocation(), 2) && compte==0)
                                    {
                                     
                                        if (copy.HeldBy == null)
                                        {
                                            if ( copy.IsLockedDown == false && copy.Movable == true && copy.Decays == true )
                                            {
                                            	copy.Movable = false;
                                                copy.IsLockedDown = true;
                                                copy.LootType = LootType.Newbied;
                                            }
                                            else if ( copy.IsLockedDown == true && copy.Movable == false && copy.Decays == false )
                                            {
                                                copy.Movable = true;
                                                copy.LootType = LootType.Regular;
                                                copy.IsLockedDown = false;
                                            }
                                             else if (copy.LootType == LootType.Newbied && copy.Movable == false && copy.Decays == false)
                                            {
                                                copy.Movable = true;
                                                copy.LootType = LootType.Regular;
                                                copy.IsLockedDown = false;
                                            }
                                            else
                                            {
                    							from.SendMessage("Cet objet ne peut �tre verrouiller");
											}
                               
                                        }
                                    }
                                    else
                                    {
                                        from.SendMessage("Veuillez vous rapprocher.");
                                    }
                                }
                                /*if (copy.HeldBy == null)
                                {
                                    if (copy.LastMoved != tempL && copy.Movable == true)
                                    {
                                        copy.Movable = false;
                                    }
                                    else if (copy.LastMoved == tempL && copy.Movable == false)
                                    {
                                        copy.Movable = true;
                                        copy.LastMoved = tempF;
                                    }
                                    else
                                    {
                                        from.SendMessage("Impossible de verrouiller.");
                                    }
                                }*/
								/*if ( pack != null )
									pack.DropItem( newItem );
								else
									newItem.MoveToWorld( from.Location, from.Map );*/
							}
							
							done = true;
						}
						catch( SystemException ex )
						{
                            from.SendMessage(".verrou Boom?" + ex.Message.ToString());
							return;
						}
					}
				}

				if ( !done )
				{
                    from.SendMessage(".verrou Erreur?");
				}
			}
		}

		
	}
}
