﻿using System;
using System.Collections.Generic;
using System.Text;
using Server.Network;

namespace Server
{
    public class CommandSystem
    {

        public static void Register(string command, AccessLevel access, CommandEventHandler handler)
        {
            Commands.Register(command, access, handler);
        }

  
    }

}
