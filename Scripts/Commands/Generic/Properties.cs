using System;
using System.Reflection;
using System.Collections;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Gumps;
using CPA = Server.CommandPropertyAttribute;

using Server.GenericCommands;

namespace Server
{
	public enum PropertyAccess
	{
		Read		= 0x01,
		Write		= 0x02,
		ReadWrite	= Read | Write
	}

}

namespace Server
{
	public abstract class PropertyException : ApplicationException
	{
		protected Property m_Property;

		public Property Property
		{
			get { return m_Property; }
		}

		public PropertyException( Property property, string message )
			: base( message )
		{
			m_Property = property;
		}
	}

	public abstract class BindingException : PropertyException
	{
		public BindingException( Property property, string message )
			: base( property, message )
		{
		}
	}

	public sealed class NotYetBoundException : BindingException
	{
		public NotYetBoundException( Property property )
			: base( property, String.Format( "Property has not yet been bound." ) )
		{
		}
	}

	public sealed class AlreadyBoundException : BindingException
	{
		public AlreadyBoundException( Property property )
			: base( property, String.Format( "Property has already been bound." ) )
		{
		}
	}

	public sealed class UnknownPropertyException : BindingException
	{
		public UnknownPropertyException( Property property, string current )
			: base( property, String.Format( "Property '{0}' not found.", current ) )
		{
		}
	}

	public sealed class ReadOnlyException : BindingException
	{
		public ReadOnlyException( Property property )
			: base( property, "Property is read-only." )
		{
		}
	}

	public sealed class WriteOnlyException : BindingException
	{
		public WriteOnlyException( Property property )
			: base( property, "Property is write-only." )
		{
		}
	}

	public abstract class AccessException : PropertyException
	{
		public AccessException( Property property, string message )
			: base( property, message )
		{
		}
	}

	public sealed class InternalAccessException : AccessException
	{
		public InternalAccessException( Property property )
			: base( property, "Property is internal." )
		{
		}
	}

	public abstract class ClearanceException : AccessException
	{
		protected AccessLevel m_PlayerAccess;
		protected AccessLevel m_NeededAccess;

		public AccessLevel PlayerAccess
		{
			get { return m_PlayerAccess; }
		}

		public AccessLevel NeededAccess
		{
			get { return m_NeededAccess; }
		}

		public ClearanceException( Property property, AccessLevel playerAccess, AccessLevel neededAccess, string accessType )
			: base( property, string.Format(
				"You must be at least {0} to {1} this property.",
				Mobile.GetAccessLevelName( neededAccess ),
				accessType
			) )
		{
		}
	}

	public sealed class ReadAccessException : ClearanceException
	{
		public ReadAccessException( Property property, AccessLevel playerAccess, AccessLevel neededAccess )
			: base( property, playerAccess, neededAccess, "read" )
		{
		}
	}

	public sealed class WriteAccessException : ClearanceException
	{
		public WriteAccessException( Property property, AccessLevel playerAccess, AccessLevel neededAccess )
			: base( property, playerAccess, neededAccess, "write" )
		{
		}
	}

	public sealed class Property
	{
		private string m_Binding;

		private PropertyInfo[] m_Chain;
		private PropertyAccess m_Access;

		public string Binding
		{
			get { return m_Binding; }
		}

		public bool IsBound
		{
			get { return ( m_Chain != null ); }
		}

		public PropertyAccess Access
		{
			get { return m_Access; }
		}

		public PropertyInfo[] Chain
		{
			get
			{
				if ( !IsBound )
					throw new NotYetBoundException( this );

				return m_Chain;
			}
		}

		public Type Type
		{
			get
			{
				if ( !IsBound )
					throw new NotYetBoundException( this );

				return m_Chain[m_Chain.Length - 1].PropertyType;
			}
		}

		public bool CheckAccess( Mobile from )
		{
			if ( !IsBound )
				throw new NotYetBoundException( this );

			for ( int i = 0; i < m_Chain.Length; ++i )
			{
				PropertyInfo prop = m_Chain[i];

				bool isFinal = ( i == ( m_Chain.Length - 1 ) );

				PropertyAccess access = m_Access;

				if ( !isFinal )
					access |= PropertyAccess.Read;

				CPA security = Server.Scripts.Commands.Properties.GetCPA( prop );

				if ( security == null )
					throw new InternalAccessException( this );

				if ( ( access & PropertyAccess.Read ) != 0 && from.AccessLevel < security.ReadLevel )
					throw new ReadAccessException( this, from.AccessLevel, security.ReadLevel );


			}

			return true;
		}



		public void BindTo( Type objectType, PropertyAccess desiredAccess )
		{
			if ( IsBound )
				throw new AlreadyBoundException( this );

			string[] split = m_Binding.Split( '.' );

			PropertyInfo[] chain = new PropertyInfo[split.Length];

			for ( int i = 0; i < split.Length; ++i )
			{
				bool isFinal = ( i == ( chain.Length - 1 ) );

				chain[i] = objectType.GetProperty( split[i], BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase );

				if ( chain[i] == null )
					throw new UnknownPropertyException( this, split[i] );

				objectType = chain[i].PropertyType;

				PropertyAccess access = desiredAccess;

				if ( !isFinal )
					access |= PropertyAccess.Read;

				if ( ( access & PropertyAccess.Read ) != 0 && !chain[i].CanRead )
					throw new WriteOnlyException( this );

				if ( ( access & PropertyAccess.Write ) != 0 && !chain[i].CanWrite )
					throw new ReadOnlyException( this );
			}

			m_Access = desiredAccess;
			m_Chain = chain;
		}

		public Property( string binding )
		{
			m_Binding = binding;
		}

		public Property( PropertyInfo[] chain )
		{
			m_Chain = chain;
		}

		public override string ToString()
		{
			if ( !IsBound )
				return m_Binding;

			string[] toJoin = new string[m_Chain.Length];

			for ( int i = 0; i < toJoin.Length; ++i )
				toJoin[i] = m_Chain[i].Name;

			return string.Join( ".", toJoin );
		}

		public static Property Parse( Type type, string binding, PropertyAccess access )
		{
			Property prop = new Property( binding );

			prop.BindTo( type, access );

			return prop;
		}


        public static string SetDirect(Mobile from, object logObject, object obj, PropertyInfo prop, string givenName, object toSet, bool shouldLog)
        {
            try
            {
                if (toSet is AccessLevel)
                {
                    AccessLevel newLevel = (AccessLevel)toSet;
                    AccessLevel reqLevel = AccessLevel.Administrator;



                    if (from.AccessLevel < reqLevel)
                        return "You do not have access to that level.";
                }

                if (shouldLog)
                    Server.Scripts.Commands.CommandLogging.LogChangeProperty(from, logObject, givenName, toSet == null ? "(-null-)" : toSet.ToString());

                prop.SetValue(obj, toSet, null);
                return "Property has been set.";
            }
            catch
            {
                return "An exception was caught, the property may not be set.";
            }
        }

	}
}