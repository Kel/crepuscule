using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Scripts.Commands
{
    public class Decrire
    {
        public static void Initialize()
        {
            Server.Commands.Register("decrire", AccessLevel.Player, new CommandEventHandler(Decrire_OnCommand));

        }

        [Usage("decrire")]
        [Description("Decrit un objet")]
        private static void Decrire_OnCommand(CommandEventArgs e)
        {
            if (e.Length != 1)
            {
                e.Mobile.SendMessage("Utilisation : .decrire <texte>");
            }
            else
            {
                e.Mobile.Target = new DecrireTarget(true,(string)e.GetString(0));
                e.Mobile.SendMessage("Que voulez vous d�crire?");
            }

            
        }


        private class DecrireTarget : Target
        {
            private bool m_InBag;
            private string m_Descr;

            public DecrireTarget(bool inbag,string descr)
                : base(15, false, TargetFlags.None)
            {
                m_InBag = inbag;
                m_Descr = descr;
            }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(targ is CrepusculeItem))
                {
                    from.SendMessage("Vous pouvez d�crire les objets uniquement");
                    return;
                }


                CrepusculeItem copy = (CrepusculeItem)targ;
                try
                {
                    if (m_InBag && (copy.Parent is Container || copy.Parent is Mobile))
                    {
                        copy.PlayerDescription = m_Descr;
                        copy.InvalidateProperties();
                        from.SendMessage("La d�scription a �t� modifi�e.");
                    }
                    else
                    {
                        from.SendMessage("L'objet doit se trouver dans votre sac.");
                        return;
                    }
                
                }
                catch (Exception)
                {
                    from.SendMessage("Imossible de d�crire.");
                    return;
                }
            
                
            }
        }


    }
}
