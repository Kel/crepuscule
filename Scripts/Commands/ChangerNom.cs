using System;  
using Server;
using Server.Gumps;
using Server.Network;
using Server.Mobiles;

namespace Server.Custom.Regions
{
    public class ChangerNomCommand
	{
		
		
		public static void Initialize()
		{
            Server.Commands.Register("ChangerNom", AccessLevel.GameMaster, new CommandEventHandler(ChangerNomCommand_OnCommand));
		}

        [Usage("ChangerNom")]
		[Description( "Animates a certain number." )]
        private static void ChangerNomCommand_OnCommand(CommandEventArgs e)
		{

            string nom = e.GetString(0);

            if (e.Length >= 1)
            {
                try
                {
                    if (e.Mobile is RacePlayerMobile)
                    {
                        RacePlayerMobile pm = (RacePlayerMobile)e.Mobile;
                        pm.RawName = nom;
                        pm.PlayerName = nom;
                        pm.Name = nom;
                        pm.NameMod = nom;
                    
                    }

                }
                catch
                {
                    e.Mobile.SendMessage("Format incorrect");
                    return;
                }
            }
            else
            {
                e.Mobile.SendMessage("Format incorrect");
            }
			
		}
	}
}
