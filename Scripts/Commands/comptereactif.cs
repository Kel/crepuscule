using System;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Items;

namespace Server.Scripts.Commands
{
public class CompteurReg
{
public static void Initialize()
{
 Server.Commands.Register( "Regs", AccessLevel.Player, new CommandEventHandler( Regs_OnCommand ) );
}

[Usage( "Regs" )]
[Description( "Compteur de Reactifs" )]
public static void Regs_OnCommand( CommandEventArgs e )
{
 Mobile somemobile = e.Mobile;
 somemobile.SendGump( new RegsGump(somemobile) );
}

}


public class RegsGump : Gump
{

public RegsGump ( Mobile from ) : base ( 20, 30 )
{
 string nb_blackpearl;
 string nb_bloodmoss;
 string nb_garlic;
 string nb_ginseng;
 string nb_mandrakeroot;
 string nb_nightshade;
 string nb_sulfurousash;
 string nb_spidersilk;
 string nb_batwing;
 string nb_pigiron;
 string nb_daemonblood;
 string nb_noxcrystal;
 string nb_gravedust;
 int nb;
 Item temp;

 Container ourpack = from.Backpack;
 temp = ourpack.FindItemByType( typeof(BlackPearl));
 if ( temp == null )
 {
  nb_blackpearl = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_blackpearl = "" + nb;
  }

  temp = ourpack.FindItemByType( typeof(Bloodmoss));
 if ( temp == null )
 {
  nb_bloodmoss = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_bloodmoss = "" + nb;
  }
  
 temp = ourpack.FindItemByType( typeof(Garlic));
 if ( temp == null )
 {
  nb_garlic = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_garlic = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(Ginseng));
 if ( temp == null )
 {
  nb_ginseng = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_ginseng = "" + nb;
  }
  
 temp = ourpack.FindItemByType( typeof(MandrakeRoot));
 if ( temp == null )
 {
  nb_mandrakeroot = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_mandrakeroot = "" + nb;
  }
  
 temp = ourpack.FindItemByType( typeof(Nightshade));
 if ( temp == null )
 {
  nb_nightshade = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_nightshade = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(SulfurousAsh));
 if ( temp == null )
 {
  nb_sulfurousash = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_sulfurousash = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(SpidersSilk));
 if ( temp == null )
 {
  nb_spidersilk = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_spidersilk = "" + nb;
  }


 temp = ourpack.FindItemByType( typeof(BatWing));
 if ( temp == null )
 {
  nb_batwing = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_batwing = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(DaemonBlood));
 if ( temp == null )
 {
  nb_daemonblood = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_daemonblood = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(PigIron));
 if ( temp == null )
 {
  nb_pigiron = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_pigiron = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(NoxCrystal));
 if ( temp == null )
 {
  nb_noxcrystal = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_noxcrystal = "" + nb;
  }

 temp = ourpack.FindItemByType( typeof(GraveDust));
 if ( temp == null )
 {
  nb_gravedust = "" + 0;
 }
 else
  {
   nb = temp.Amount;
   nb_gravedust = "" + nb;
  }

 AddPage ( 0 );
 AddBackground( 0, 0, 260, 351, 5054 );

 AddImageTiled( 10, 10, 240, 23, 0x52 );
 AddImageTiled( 11, 11, 238, 21, 0xBBC );

 AddLabel( 95, 11, 0, "Reactifs" );

   AddImageTiled( 10, 32 , 240, 23, 0x52 );
  AddImageTiled( 11, 33 , 238, 21, 0xBBC );
  AddItem(13, 33, 0xF7A);
  AddLabelCropped( 53, 33 , 150, 21, 0, "Black Pearl" );
  AddImageTiled( 180, 34 , 50, 19, 0x52 );
  AddImageTiled( 181, 35 , 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 , 234, 21, 0, nb_blackpearl );

  AddImageTiled( 10, 32 + 22, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 22, 238, 21, 0xBBC );
  AddItem(13, 55, 0xF7B);
  AddLabelCropped( 53, 33 + 22, 150, 21, 0, "Bloodmoss" );
  AddImageTiled( 180, 34 + 22, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 22, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 22, 234, 21, 0, nb_bloodmoss );

  AddImageTiled( 10, 32 + 44, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 44, 238, 21, 0xBBC );
  AddItem(13, 77, 0xF84);
  AddLabelCropped( 53, 33 + 44, 150, 21, 0, "Garlic" );
  AddImageTiled( 180, 34 + 44, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 44, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 44, 234, 21, 0, nb_garlic );

  AddImageTiled( 10, 32 + 66, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 66, 238, 21, 0xBBC );
  AddItem(13, 99, 0xF85);
  AddLabelCropped( 53, 33 + 66, 150, 21, 0, "Ginseng" );
  AddImageTiled( 180, 34 + 66, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 66, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 66, 234, 21, 0, nb_ginseng );

  AddImageTiled( 10, 32 + 88, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 88, 238, 21, 0xBBC );
  AddItem(13, 121, 0xF86);
  AddLabelCropped( 53, 33 + 88, 150, 21, 0, "Mandrake Root" );
  AddImageTiled( 180, 34 + 88, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 88, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 88, 234, 21, 0, nb_mandrakeroot );

  AddImageTiled( 10, 32 + 110, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 110, 238, 21, 0xBBC );
  AddItem(13, 143, 0xF88);
  AddLabelCropped( 53, 33 + 110, 150, 21, 0, "Night Shade" );
  AddImageTiled( 180, 34 + 110, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 110, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 110, 234, 21, 0, nb_nightshade );

  AddImageTiled( 10, 32 + 132, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 132, 238, 21, 0xBBC );
  AddItem(13, 165, 0xF8C);
  AddLabelCropped( 53, 33 + 132, 150, 21, 0, "Sulfurous Ash" );
  AddImageTiled( 180, 34 +  132, 50, 19, 0x52 );
  AddImageTiled( 181, 35 +  132, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 132, 234, 21, 0, nb_sulfurousash );

  AddImageTiled( 10, 32 + 154, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 154, 238, 21, 0xBBC );
  AddItem(13, 187, 0xF8D);
  AddLabelCropped( 53, 33 + 154, 150, 21, 0, "Spider's Silk" );
  AddImageTiled( 180, 34 + 154, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 154, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 154, 234, 21, 0, nb_spidersilk );

  AddImageTiled( 10, 32 + 176, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 176, 238, 21, 0xBBC );
  AddItem(13, 209, 0xF78);
  AddLabelCropped( 53, 33 + 176, 150, 21, 0, "Batwing" );
  AddImageTiled( 180, 34 + 176, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 176, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 176, 234, 21, 0, nb_batwing );


  AddImageTiled( 10, 32 + 198, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 198, 238, 21, 0xBBC );
  AddItem(13, 231, 0xF7D);
  AddLabelCropped( 53, 33 + 198, 150, 21, 0, "Daemon Blood" );
  AddImageTiled( 180, 34 + 198, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 198, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 198, 234, 21, 0, nb_daemonblood );

  AddImageTiled( 10, 32 + 220, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 220, 238, 21, 0xBBC );
  AddItem(13, 253, 0xF8A);
  AddLabelCropped( 53, 33 + 220, 150, 21, 0, "Pig Iron" );
  AddImageTiled( 180, 34 + 220, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 220, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 220, 234, 21, 0, nb_pigiron );

  AddImageTiled( 10, 32 + 242, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 242, 238, 21, 0xBBC );
  AddItem(13, 275, 0xF8E);
  AddLabelCropped( 53, 33 + 242, 150, 21, 0, "Nox Crystal" );
  AddImageTiled( 180, 34 + 242, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 242, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 242, 234, 21, 0, nb_noxcrystal );

  AddImageTiled( 10, 32 + 264, 240, 23, 0x52 );
  AddImageTiled( 11, 33 + 264, 238, 21, 0xBBC );
  AddItem(13, 297, 0xF8F);
  AddLabelCropped( 53, 33 + 264, 150, 21, 0, "Grave Dust" );
  AddImageTiled( 180, 34 + 264, 50, 19, 0x52 );
  AddImageTiled( 181, 35 + 264, 48, 17, 0xBBC );
  AddLabelCropped( 182, 35 + 264, 234, 21, 0, nb_gravedust );
}
}
}