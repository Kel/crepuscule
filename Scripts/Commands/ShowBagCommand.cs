using System;
using Server;
using Server.Misc;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Regions;
using Server.Targeting;

namespace Server.Scripts.Commands
{
    public class ShowBagCommand
	{
		
		
		public static void Initialize()
		{
            Server.Commands.Register("MontrerSac", AccessLevel.Player, new CommandEventHandler(ShowBagCommand_OnCommand));
		}

        [Usage("MontrerSac")]
		[Description( "Montre son sac � un individu" )]
        private static void ShowBagCommand_OnCommand(CommandEventArgs e)
		{
        	PlayerMobile from = (PlayerMobile)e.Mobile;
        	from.Target = new ShowBagTarget();
		}
    	private class ShowBagTarget : Target
    	{
    		public ShowBagTarget() : base(1, false, TargetFlags.None)
            {
				
            }
        	protected override void OnTarget(Mobile from, object targeted)
        	{
        		if (targeted is PlayerMobile)
        		{
        			Container bag = (Container)from.FindItemOnLayer(Layer.Backpack);
        			bag.DisplayTo( (PlayerMobile)targeted );
        		}
        	}   	
    	}
    }
}
