using System;  
using Server;
using Server.Gumps;
using Server.Network;
using Server.Mobiles;

namespace Server.Custom.Regions
{
    public class HardRestartCommand
	{
		
		
		public static void Initialize()
		{
            Server.Commands.Register("HardRestart", AccessLevel.Administrator, new CommandEventHandler(HardRestartCommand_OnCommand));
		}

        [Usage("HardRestart")]
		[Description( "Redemarre la machine (A NE PAS UTILISER)." )]
        private static void HardRestartCommand_OnCommand(CommandEventArgs e)
		{
            System.Diagnostics.Process.Start("ShutDown", "/r");
			
		}
	}
}
