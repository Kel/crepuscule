using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;

namespace Server.Scripts.Commands
{
	public class DupeCrep
	{
		public static void Initialize()
		{
			CommandSystem.Register( "DupeCrep", AccessLevel.GameMaster, new CommandEventHandler( DupeCrep_OnCommand ) );
		}

		[Usage( "DupeCrep [amount]" )]
		[Description( "Duplique un conteneur et son contenu ou un objet." )]
		private static void DupeCrep_OnCommand( CommandEventArgs e )
		{
			int amount = 1;
			if ( e.Length >= 1 )
				amount = e.GetInt32( 0 );
			e.Mobile.Target = new DupeCrepTarget( amount > 0 ? amount : 1 );
			e.Mobile.SendMessage( "Quel conteneur ou objet voulez-vous dupliquer ?" );
		}

		private class DupeCrepTarget : Target
		{
			private int m_Amount;

			public DupeCrepTarget( int amount ) : base( 15, false, TargetFlags.None )
			{
				m_Amount = amount;
			}

			protected override void OnTarget( Mobile from, object targ )
			{				
				Container pack = from.Backpack;
				
				if ( !(targ is Container) && !(targ is Item) )
				{
					from.SendMessage( "Vous pouvez uniquement dupliquer des conteneurs ou des objets." );
					return;
				}
								
				else if ( targ is Container )
				{
					for(int i = 0; i < m_Amount; ++i)					
						copyContainer( from, pack, (Container)targ );
				}
				else
				{
					for(int i = 0; i < m_Amount; ++i)
						copyItem( from, pack, (Item)targ );
				}
			}
			
			private static Item copyItem( Mobile from, Container pack, Item toCopy )
			{
				Type t = toCopy.GetType();
				ConstructorInfo[] info = t.GetConstructors();
							
				foreach ( ConstructorInfo c in info )
				{
					ParameterInfo[] paramInfo = c.GetParameters();
					if ( paramInfo.Length == 0 )
					{
						object[] objParams = new object[0];
						try
						{
							object o = c.Invoke( objParams );
							if ( o != null && o is Item )
							{
								Item copy = Mobile.LiftItemDupe(toCopy, toCopy.Amount);
								PropertyInfo[] props = toCopy.GetType().GetProperties();
								for ( int j = 0; j < props.Length; j++ ) 
								{
									if ( props[j].CanRead && props[j].CanWrite )
										props[j].SetValue( copy, props[j].GetValue( toCopy, null ), null );
								}
								if ( pack != null )
									pack.DropItem( copy );
								else
									copy.MoveToWorld( from.Location, from.Map );
								return copy;
							}
						}
						catch
						{
							from.SendMessage( "Erreur lors de la copie d'un objet !" );
							return null;
						}
					}
				}
				return null;
			}
			
			private static void copyContainer( Mobile from, Container pack, Container toCopy )
			{
				Container copy = (Container)copyItem( from, pack, toCopy );
				
				if ( copy != null )
				{
					Item[] Content = toCopy.FindItemsByType( typeof( Item ) );
					//copy.TotalGold = 0;					
					//copy.TotalItems = 0;
					//copy.TotalWeight = 0;
					for ( int i = 0; i < Content.Length; ++i)
					{
						if ( Content[i].Parent == toCopy )
						{
							if ( Content[i] is Container )
								copyContainer( from, copy, (Container)Content[i] );
							else
								copyItem( from, copy, Content[i] );
						}
					}
				}
			}
		}
	}
}
