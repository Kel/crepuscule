﻿using System.Text.RegularExpressions;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.IO;
using System.Diagnostics;
using Crepuscule.Update.Engine;
using Crepuscule.Update;
using Server.Engines.RemoteAdmin;
using Server.Items.Containers;

namespace Server.Scripts.Commands
{
    public class GenerateReagentsCommand
    {
        /// <summary>
        /// Handler initialization
        /// </summary>
        public static void Initialize()
        {
            Server.Commands.Register("GenerateReagents", AccessLevel.GameMaster, new CommandEventHandler(GenerateReagentsCommand_OnCommand));
        }

        [Usage("GenerateReagents")]
        [Description("Génére les réactifs dans les coffres de distribution")]
        private static void GenerateReagentsCommand_OnCommand(CommandEventArgs e)
        {
            WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, "Génération des réactifs dans les coffres...");
            AutoReagentChestTimer.GenerateReagents();
        }


    }
}
