using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;

namespace Server.Misc
{
    public class DeleteMummy
	{

		public static void Initialize()
		{
            Commands.Register("DeleteMummy", AccessLevel.Administrator, new CommandEventHandler(DeleteAllPersos_OnCommand));

		}

		private static void DeleteAllPersos_OnCommand( CommandEventArgs args )
		{
            List<Mummy2> mobiless = new List<Mummy2>();
            World.Broadcast(15, true, "ATTENTION: Kel est en train de buter les momies! ");
            foreach (Mobile m in World.Mobiles.Values)
            {
                if (m is Mummy2 && m != null)
                {
                    mobiless.Add((Mummy2)m);
                    World.Mobiles.Remove(m);
                }
            }
            World.Save();

            foreach (Mummy2 m in mobiless)
            {
                m.Delete();
            }

            World.Broadcast(15, true, "Momies lapidees avec succes! ");
     

        }


    }
}

