using System;
using System.Collections;
using System.Reflection;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Regions;
using Server.Engines;

namespace Server
{
    public class CloneMe
    {
        public static void Initialize()
        {
            CommandSystem.Register("Clone", AccessLevel.GameMaster, new CommandEventHandler(CloneMe_OnCommand));
        }


        [Usage("Clone")]
        [Description("Fait une copie d'un NPC")]
        public static void CloneMe_OnCommand(CommandEventArgs e)
        {
            e.Mobile.Target = new CloneTarget();
            e.Mobile.SendMessage("Qui voulez vous cloner?");
        }

        private class CloneTarget : Target
        {
            public CloneTarget()
                : base(15, true, TargetFlags.None)
            {
            }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(targ is Mobile))
                {
                    from.SendMessage("Vous pouvez cloner les mobiles uniquement!");
                    return;
                }
                if (targ is RacePlayerMobile)
                {
                    from.SendMessage("Mais pas les joueurs, bon sang!");
                    return;
                }
                Mobile clone = CloneMobile(from, (Mobile)targ);

                if (clone != null)
                {
                    clone.Map = from.Map;
                    clone.Location = from.Location;
                    clone.Direction = from.Direction;
                }
            }
        }

        public static Mobile CloneMobile(Mobile from, Mobile target)
        {
            if (target == null) return null;
            try
            {
                Type targetType = target.GetType();
                object Clone = Activator.CreateInstance(targetType);
                Mobile m = Clone as Mobile;
                var type = CreatureType.FindType(targetType);
                if (type != null)
                    type.SetToCreature(m as BaseCreature);

                CopyEquip.CopyEquipment(target, m,true);

                return m;
            }
            catch (Exception ex)
            {
                Server.Scripts.Commands.CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 3, ex.Message);
                return null;
            }
        }
    }
}

