using System;
using Server;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;
using Server.Items;

namespace Server.Scripts.Commands
{
	public class CustomCmdHandlers
	{
		public static void Initialize()
		{
			//Register( "Say", AccessLevel.GameMaster, new CommandEventHandler( Say_OnCommand ) );
			Register( "ItemSay", AccessLevel.GameMaster, new CommandEventHandler( ItemSay_OnCommand ) );

            TargetCommands.Register(new SayCommand());
		}

		public static void Register( string command, AccessLevel access, CommandEventHandler handler )
		{
			Server.Commands.Register( command, access, handler );
		}

        public class SayCommand : BaseCommand
        {
            public SayCommand()
            {
                AccessLevel = AccessLevel.GameMaster;
                Supports = CommandSupport.AllMobiles;
                Commands = new string[] { "Say" };
                ObjectTypes = ObjectTypes.Both;
                Usage = "Say <text>";
                Description = "Forces Targeted Mobile to Say <text>.";
            }

            public override void Execute(CommandEventArgs e, object obj)
            {
                string toSay = e.ArgString.Trim();

                if (toSay.Length > 0)
                {
                    bool targetItem = false;
                    if (obj is Item)
                        targetItem = true;
                    object targeted = obj;
                    Mobile from = e.Mobile;

                    if (!targetItem && targeted is Mobile)
                    {
                        Mobile targ = (Mobile)targeted;

                        if (from != targ && from.AccessLevel > targ.AccessLevel)
                        {
                            CommandLogging.WriteLine(from, "{0} {1} forcing speech on {2}", from.AccessLevel, CommandLogging.Format(from), CommandLogging.Format(targ));
                            targ.Say(toSay);
                        }
                    }
                    else if (targetItem && targeted is Item)
                    {
                        Item targ = (Item)targeted;
                        targ.PublicOverheadMessage(MessageType.Regular, Utility.RandomDyedHue(), false, toSay);
                    }
                    else
                        from.SendMessage("Invaild Target Type");

                }
                else
                    e.Mobile.SendMessage("Format: Say \"<text>\"");
            }
  
        }
		

		[Usage( "Say <text>" )]
		[Description( "Forces Targeted Mobile to Say <text>." )]
		public static void Say_OnCommand( CommandEventArgs e )
		{
			string toSay = e.ArgString.Trim();

			if ( toSay.Length > 0 )
				e.Mobile.Target = new SayTarget( toSay, false );
			else
				e.Mobile.SendMessage( "Format: Say \"<text>\"" );
		}

		[Usage( "ItemSay <text>" )]
		[Description( "Forces Targeted Item to Say <text>." )]
		public static void ItemSay_OnCommand( CommandEventArgs e )
		{
			string toSay = e.ArgString.Trim();

			if ( toSay.Length > 0 )
				e.Mobile.Target = new SayTarget( toSay, true );
			else
				e.Mobile.SendMessage( "Format: Say \"<text>\"" );
		}

		private class SayTarget : Target
		{
			private string m_toSay;
			private bool targetItem;

			public SayTarget( string say, bool item ) : base( -1, false, TargetFlags.None )
			{
				m_toSay = say;
				targetItem = item;
			}

			protected override void OnTarget( Mobile from, object targeted )
			{
				if ( !targetItem && targeted is Mobile )
				{
					Mobile targ = (Mobile)targeted;

					if ( from != targ && from.AccessLevel > targ.AccessLevel )
					{
						CommandLogging.WriteLine( from, "{0} {1} forcing speech on {2}", from.AccessLevel, CommandLogging.Format( from ), CommandLogging.Format( targ ) );
						targ.Say( m_toSay );
					}
				}
				else if ( targetItem && targeted is Item )
				{
					Item targ = (Item)targeted;
					targ.PublicOverheadMessage( MessageType.Regular, Utility.RandomDyedHue(), false, m_toSay);
				}
				else 
					from.SendMessage( "Invaild Target Type" );
			}
		}
	}
}