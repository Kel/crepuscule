/* --------------------------->
 * Advanced properties script by Kaon 
 * Version 1.0
 * Creation : 28.03.2007
 * Revision : N/A
 * --------------------------->
 */
using System;
using System.Reflection;
using System.Collections;
using Server;
using Server.Network;

using System.Globalization;
using System.Collections.Generic;
using Server.Targeting;
using CPA = Server.CommandPropertyAttribute;
using Server.Prompts;
using Server.Scripts.Commands;
using Server.Mobiles;
using Server.Guilds;
using Server.Engines.Craft;
using Server.Engines;

namespace Server.Gumps
{

	public class ASetListGump<T> : Gump
	{
		private PropertyInfo m_Property;
		private Mobile m_Mobile;
		private object m_Object;
		private Stack m_Stack;
		private int m_Page;
		private ArrayList m_List;

		public static readonly bool OldStyle = PropsConfig.OldStyle;
        private static bool PrevLabel = OldStyle, NextLabel = OldStyle;

		public static readonly int GumpOffsetX = PropsConfig.GumpOffsetX;
		public static readonly int GumpOffsetY = PropsConfig.GumpOffsetY;

		public static readonly int TextHue = PropsConfig.TextHue;
		public static readonly int TextOffsetX = PropsConfig.TextOffsetX;

		public static readonly int OffsetGumpID = PropsConfig.OffsetGumpID;
		public static readonly int HeaderGumpID = PropsConfig.HeaderGumpID;
		public static readonly int  EntryGumpID = PropsConfig.EntryGumpID;
		public static readonly int   BackGumpID = PropsConfig.BackGumpID;
		public static readonly int    SetGumpID = PropsConfig.SetGumpID;

		public static readonly int SetWidth = PropsConfig.SetWidth;
		public static readonly int SetOffsetX = PropsConfig.SetOffsetX, SetOffsetY = PropsConfig.SetOffsetY;
		public static readonly int SetButtonID1 = PropsConfig.SetButtonID1;
		public static readonly int SetButtonID2 = PropsConfig.SetButtonID2;

		public static readonly int PrevWidth = PropsConfig.PrevWidth;
		public static readonly int PrevOffsetX = PropsConfig.PrevOffsetX, PrevOffsetY = PropsConfig.PrevOffsetY;
		public static readonly int PrevButtonID1 = PropsConfig.PrevButtonID1;
		public static readonly int PrevButtonID2 = PropsConfig.PrevButtonID2;

        private static readonly int PrevLabelOffsetX = PrevWidth + 1;
        private static readonly int PrevLabelOffsetY = 0;

        private static readonly int NextLabelOffsetX = -29;
        private static readonly int NextLabelOffsetY = 0;

		public static readonly int NextWidth = PropsConfig.NextWidth;
		public static readonly int NextOffsetX = PropsConfig.NextOffsetX, NextOffsetY = PropsConfig.NextOffsetY;
		public static readonly int NextButtonID1 = PropsConfig.NextButtonID1;
		public static readonly int NextButtonID2 = PropsConfig.NextButtonID2;

		public static readonly int OffsetSize = PropsConfig.OffsetSize;

		public static readonly int EntryHeight = PropsConfig.EntryHeight;
		public static readonly int BorderSize = PropsConfig.BorderSize;

        private static readonly int EntryWidth = 300; //212;

		private static readonly int TotalWidth = OffsetSize + EntryWidth + OffsetSize + SetWidth + OffsetSize;
		private static readonly int TotalHeight = OffsetSize + (4 * (EntryHeight + OffsetSize));

		private static readonly int BackWidth = BorderSize + TotalWidth + BorderSize;
		private static readonly int BackHeight = BorderSize + TotalHeight + BorderSize;

        private List<T> CurrentList;

        public ASetListGump(PropertyInfo prop, Mobile mobile, object o, Stack stack, int page, ArrayList list)
            : base(GumpOffsetX, GumpOffsetY)
		{
			m_Property = prop;
			m_Mobile = mobile;
			m_Object = o;
			m_Stack = stack;
			m_Page = page;
			m_List = list;

			List<T> l = (List<T>)prop.GetValue( o, null );
            CurrentList = l;
            CPA attr = Properties.GetCPA(prop);
            bool canWrite = (attr != null && m_Mobile.AccessLevel >= attr.WriteLevel);

			AddPage( 0 );

            AddBackground(0, 0, BackWidth, BackHeight + (canWrite ? ((3 + Math.Min(20, l.Count)) * (EntryHeight + OffsetSize)) : (Math.Min(20, l.Count) * (EntryHeight + OffsetSize))), BackGumpID);

            int x = BorderSize + OffsetSize;
            int y = BorderSize + OffsetSize;
            int emptyWidth = TotalWidth - PrevWidth - NextWidth - (OffsetSize * 4) - (OldStyle ? SetWidth + OffsetSize : 0);

            AddImageTiled(BorderSize, BorderSize, TotalWidth - (OldStyle ? SetWidth + OffsetSize : 0), TotalHeight + (canWrite ? ((3 + Math.Min(20, l.Count)) * (EntryHeight + OffsetSize)) : (Math.Min(20, l.Count)) * (EntryHeight + OffsetSize)), OffsetGumpID);

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
            {
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);
                AddImageTiled(x + PrevWidth + OffsetSize - (OldStyle ? OffsetSize : 0), y, emptyWidth + (OldStyle ? OffsetSize * 2 : 0), EntryHeight, HeaderGumpID);
                AddImageTiled(x + PrevWidth + OffsetSize*2 + emptyWidth, y, NextWidth, EntryHeight, HeaderGumpID);
            }

            if (!OldStyle)

            AddRect(1, prop.Name, 0, -1);
            AddRect(2, "Liste de " + typeof(T).GetShortName() , 0, -1);
            AddRect(3, "Total : "+l.Count, 0, -1);
            if (canWrite)
            {
                AddRect(4, "Ajouter", 1, -1, true);
                AddRect(5, "Supprimer", 2, -1, true);
                AddRect(6, "" , 0, -1);
                //AddRect(5, "Supprimer", 2, -1, true);
                //AddRect(6, "Vider", 3, -1, true);
                
            }
            int nbpages = (int)Math.Ceiling(l.Count / 20.0);
            for (int i = 0, k=1; i < l.Count; ++i)
            {
                if (i % 20 == 0)
                {
                    AddPage(k);
                    if (k > 1)
                    {
                        AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 1, GumpButtonType.Page, k - 1);
                        if (PrevLabel)
                            AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Previous");
                    }
                    if (k < nbpages)
                    {
                        AddButton(x + PrevWidth + OffsetSize*2 + emptyWidth + NextOffsetX, y + NextOffsetY, NextButtonID1, NextButtonID2, 2, GumpButtonType.Page, k + 1);

                        if (NextLabel)
                            AddLabel(x + NextLabelOffsetX, y + NextLabelOffsetY, TextHue, "Next");
                    }

                    k++;
                }
                AddRect(7 - (canWrite ? 0 : 3)  + (i%20), l[i].ToString(), (canWrite ? (5 + i) : 0), -1);
            }
		}



        private void AddRect(int index, string str, int button, int text)
        {
            AddRect(index,str,button,text,false);
        }

        private void AddRect(int index, string str, int button, int text, bool action)
		{
			int x = BorderSize + OffsetSize;
			int y = BorderSize + OffsetSize + (index * (EntryHeight + OffsetSize));

			AddImageTiled( x, y, EntryWidth, EntryHeight, EntryGumpID );
			AddLabelCropped( x + TextOffsetX, y, EntryWidth - TextOffsetX, EntryHeight, action ? 47 : TextHue, str );

			if ( text != -1 )
				AddTextEntry( x + 16 + TextOffsetX, y, EntryWidth - TextOffsetX - 16, EntryHeight, TextHue, text, "" );

			x += EntryWidth + OffsetSize;

			if ( SetGumpID != 0 )
				AddImageTiled( x, y, SetWidth, EntryHeight, SetGumpID );

            if (button != 0)
            {
                if (action)
                {
                    AddButton(x + SetOffsetX - 3, y + SetOffsetY - 2, 5538, 5539, button, GumpButtonType.Reply, 0);
                }
                else
                {
                    bool canDelete = true;
                    if (CurrentList[button - 5] is ISelfDeletion)
                        canDelete = (CurrentList[button - 5] as ISelfDeletion).CanBeDeleted();
                    
                    if(canDelete)
                        AddButton(x + SetOffsetX - 57, y + SetOffsetY + 2, 2181, 2181, button + 1000, GumpButtonType.Reply, 0);
                    AddButton(x + SetOffsetX, y + SetOffsetY, SetButtonID1, SetButtonID2, button, GumpButtonType.Reply, 0);
                }
            }
		}

        private class InternalTarget : Target
        {
            private PropertyInfo m_Property;
            private Mobile m_Mobile;
            private object m_Object;
            private Stack m_Stack;
            private int m_Page;
            private ArrayList m_List;
            private bool bAdd;

            public InternalTarget(PropertyInfo prop, Mobile mobile, object o, Stack stack, int page, ArrayList list, bool add)
                : base(-1, true, TargetFlags.None)
            {
                m_Property = prop;
                m_Mobile = mobile;
                m_Object = o;
                m_Stack = stack;
                m_Page = page;
                m_List = list;
                bAdd = add;
            }

            protected override void OnTarget(Mobile from, object targeted)
            {
                T target = default(T);
                try
                {
                    if (typeof(T).Equals(typeof(Point3D)))
                        target = (T)((object)new Point3D(targeted as IPoint3D));
                    else if (typeof(T).Equals(typeof(Point2D)))
                        target = (T)((object)new Point2D(targeted as IPoint2D));
                    else target = (T)((object)targeted);
                }
                catch
                {
                    target = default(T);
                    from.SendMessage(targeted.ToString() + " can't be converted to type " + typeof(T) );
                }
                List<T> l = (List<T>)m_Property.GetValue(m_Object, null);

                if (target != null)
                {
                    try
                    {
                        if (bAdd)
                        {
                            if (m_Property.GetCustomAttributes(typeof(UniqueInListProperty), false).Length > 0 && l.Contains(target))
                            {
                                from.SendMessage(target.ToString() + " already in the list.");
                            }
                            else
                            {
                                CommandLogging.LogChangeProperty(m_Mobile, m_Object, m_Property.Name, "add " + target.ToString());
                                l.Add(target);
                                from.SendMessage(target.ToString() + " added to the list.");
                            }
                        }
                        else
                        {
                            CommandLogging.LogChangeProperty(m_Mobile, m_Object, m_Property.Name, "remove " + target.ToString());
                            if( l.Remove(target) )
                                from.SendMessage(target.ToString() + " removed from the list.");
                            else from.SendMessage(target.ToString() + " wasn't in the list.");
                        }
                    }
                    catch
                    {
                        m_Mobile.SendMessage("An exception was caught. The property may not have changed.");
                    }
                }
            }

            protected override void OnTargetFinish(Mobile from)
            {
                m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
            }
        }

        private class InternalPrompt : Prompt
        {
            private PropertyInfo m_Property;
            private Mobile m_Mobile;
            private object m_Object;
            private Stack m_Stack;
            private int m_Page;
            private ArrayList m_List;
            private bool bAdd;

            public InternalPrompt(PropertyInfo prop, Mobile mobile, object o, Stack stack, int page, ArrayList list, bool add) : base()
            {
                m_Property = prop;
                m_Mobile = mobile;
                m_Object = o;
                m_Stack = stack;
                m_Page = page;
                m_List = list;
                bAdd = add;
            }

            public override void OnResponse(Mobile from, string text)
            {
                T value = default(T);
                try 
                {
                    if (IsType(typeof(T), typeofBool)) value = (T)((object)bool.Parse(text));
                    else if (IsType(typeof(T), typeofDateTime)) value = (T)((object)DateTime.Parse(text));
                    else if (IsType(typeof(T), typeofMap)) value = (T)((object)Map.Parse(text));
                    else if (IsType(typeof(T), typeofPoison)) value = (T)((object)Poison.Parse(text));
                    else if (IsType(typeof(T), typeofGuild)) value = (T)((object)Enum.Parse(typeof(T), text));
                    else if (IsType(typeof(T), typeofEnum)) value = (T)((object)Enum.Parse(typeof(T), text));
                    else if (IsType(typeof(T), typeofNumeric)) value = (T)((object)Int64.Parse(text));
                    else if (IsType(typeof(T), typeofReal)) value = (T)((object)Double.Parse(text));
                    else if (IsType(typeof(T), typeofString)) value = (T)((object)text);
                    else if (IsType(typeof(T), typeofTimeSpan)) value = (T)((object)TimeSpan.Parse(text));
                    else value = (T)((object)Type.GetType(text));

                    List<T> l = (List<T>)m_Property.GetValue(m_Object, null);
                    if (bAdd)
                    {
                        if (m_Property.GetCustomAttributes(typeof(UniqueInListProperty), false).Length > 0 && l.Contains(value))
                        {
                            from.SendMessage(value.ToString() + " already in the list.");
                        }
                        else
                        {
                            CommandLogging.LogChangeProperty(m_Mobile, m_Object, m_Property.Name, "add " + value.ToString());
                            l.Add(value);
                            from.SendMessage(value.ToString() + " added to the list.");
                        }
                    }
                    else
                    {
                        CommandLogging.LogChangeProperty(m_Mobile, m_Object, m_Property.Name, "remove " + value.ToString());
                        if( l.Remove(value) )
                            from.SendMessage(value.ToString() + " removed from the list.");
                        else
                            from.SendMessage(value.ToString() + " wasn't in the list.");
                    }
                }
                catch
                {
                    from.SendMessage("Error trying to parse '" + text + "' as a(n) " + typeof(T).ToString());
                }
                m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
            }
        }

		public override void OnResponse( NetState sender, RelayInfo info )
		{
			bool shouldSend = false;
            Mobile from = sender.Mobile;
            List<T> l = (List<T>)m_Property.GetValue(m_Object, null);
            Type type = typeof(T);
			switch ( info.ButtonID )
			{
                case 0: // Exit
                    shouldSend = !(m_Stack == null && m_List == null);
                    break;   
				case 1: // Add
                    if (IsType(type, typeof(GameCraftCategory)))
                    {
                        var list = l as List<GameCraftCategory>;
                        list.Add(new GameCraftCategory());
                        m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
                    }
                    else if (IsType(type, typeof(GameLootPack)))
                    {
                        var list = l as List<GameLootPack>;
                        list.Add(new GameLootPack());
                        m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
                    }
                    else if (IsType(type, typeof(GameLootItem)))
                    {
                        var list = l as List<GameLootItem>;
                        list.Add(new GameLootItem());
                        m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
                    }
                    else if (IsType(type, typeof(GameCraftSystem)))
                    {
                        var list = l as List<GameCraftSystem>;
                        list.Add(new GameCraftSystem());
                        m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
                    }
                    else if (IsType(type, typeof(TradeInfo)))
                    {
                        var list = l as List<TradeInfo>;
                        list.Add(new TradeInfo());
                        m_Mobile.SendGump(new ASetListGump<T>(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List));
                    }
                    else if (IsType(type, typeofMobile) || IsType(type, typeofItem) || IsType(type, typeofPoint3D) || IsType(type, typeofPoint2D))
                    {
                        if (from.Target != null) from.Target.Cancel(m_Mobile, TargetCancelType.Overriden);
                        from.SendMessage("Target the " + typeof(T).ToString() + " you want to add :");
                        from.Target = new InternalTarget(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List, true);
                    }
                    else
                    {
                        if (from.Prompt != null) from.Prompt.OnCancel(from);
                        from.SendMessage("Enter the " + typeof(T).ToString() + " you want to add :");
                        from.Prompt = new InternalPrompt(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List, true);
                    }
                    shouldSend = false;
					break;
                case 2: // Remove
                    if (IsType(type, typeofMobile) || IsType(type, typeofItem) || IsType(type, typeofPoint3D) || IsType(type, typeofPoint2D))
                    {
                        if (from.Target != null) from.Target.Cancel(m_Mobile, TargetCancelType.Overriden);
                        from.SendMessage("Target the " + typeof(T).ToString() + " you want to remove :");
                        from.Target = new InternalTarget(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List, false);
                    }
                    else
                    {
                        if (from.Prompt != null) from.Prompt.OnCancel(from);
                        from.SendMessage("Enter the " + typeof(T).ToString() + " you want to remove :");
                        from.Prompt = new InternalPrompt(m_Property, m_Mobile, m_Object, m_Stack, m_Page, m_List, false);
                    }
                    shouldSend = false;
                    break;
                case 3: // Clear
                    m_Mobile.SendGump(new AListClearConfirmationGump<T>(l, m_Property, m_Mobile as RacePlayerMobile, m_Object, m_Stack, m_Page, m_List, -1));
                    break;
				default:
				{
                    if (info.ButtonID > 1000) // Remove
                    {
                        int i = info.ButtonID - 5 - 1000;
                        m_Mobile.SendGump(new AListRemoveConfirmationGump<T>(l, m_Property, m_Mobile as RacePlayerMobile, m_Object, m_Stack, m_Page, m_List, i));
                    }
                    else
                    {

                        int i = info.ButtonID - 5;

                        if (IsType(type, typeofMobile) || IsType(type, typeofItem))
                            from.SendGump(new ASetObjectGump(m_Property, from, m_Object, m_Stack, type, m_Page, m_List, i));
                        else if (IsType(type, typeofType))
                            from.Target = new ASetObjectTarget(m_Property, from, m_Object, m_Stack, type, m_Page, m_List, i);
                        else if (IsType(type, typeofPoint3D))
                            from.SendGump(new ASetPoint3DGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, i));
                        else if (IsType(type, typeofPoint2D))
                            from.SendGump(new ASetPoint2DGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, i));
                        else if (IsType(type, typeofTimeSpan))
                            from.SendGump(new ASetTimeSpanGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, i));
                        else if (IsType(type, typeofDateTime))
                            from.SendGump(new ASetDateTimeGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, i));
                        else if (IsType(type, typeofGuild))
                            from.SendGump(new ASetListOptionGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, Enum.GetNames(type), APropertiesGump.GetObjects(Enum.GetValues(type)), i));
                        else if (IsType(type, typeofEnum))
                            from.SendGump(new ASetListOptionGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, Enum.GetNames(type), APropertiesGump.GetObjects(Enum.GetValues(type)), i));
                        else if (IsType(type, typeofBool))
                            from.SendGump(new ASetListOptionGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, APropertiesGump.m_BoolNames, APropertiesGump.m_BoolValues, i));
                        else if (IsType(type, typeofString) || IsType(type, typeofReal) || IsType(type, typeofNumeric))
                            from.SendGump(new ASetGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, i));
                        else if (IsType(type, typeofPoison))
                            from.SendGump(new ASetListOptionGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, APropertiesGump.m_PoisonNames, APropertiesGump.m_PoisonValues, i));
                        else if (IsType(type, typeofMap))
                            from.SendGump(new ASetListOptionGump(m_Property, from, m_Object, m_Stack, m_Page, m_List, Map.GetMapNames(), Map.GetMapValues(), i));
                        else if (APropertiesGump.HasAttribute(type, typeofPropertyObject, true))
                        {
                            from.SendGump(new ASetListGump<T>(m_Property, from, m_Object, m_Stack, m_Page, m_List));
                            from.SendGump(new APropertiesGump(from, l[i], m_Stack, new APropertiesGump.StackEntry(m_Object, m_Property)));
                        }

                        shouldSend = false;
                    }
                    break;
				}
			}

			if ( shouldSend )
				m_Mobile.SendGump( new APropertiesGump( m_Mobile, m_Object, m_Stack, m_List, m_Page ) );
		}

        private static bool IsType(Type type, Type check)
        {
            return type == check || type.IsSubclassOf(check);
        }

        private static bool IsType(Type type, Type[] check)
        {
            for (int i = 0; i < check.Length; ++i)
                if (IsType(type, check[i]))
                    return true;

            return false;
        }

        private static Type typeofMobile = typeof(Mobile);
        private static Type typeofItem = typeof(Item);
        private static Type typeofType = typeof(Type);
        private static Type typeofPoint3D = typeof(Point3D);
        private static Type typeofPoint2D = typeof(Point2D);
        private static Type typeofTimeSpan = typeof(TimeSpan);
        private static Type typeofDateTime = typeof(DateTime);
        private static Type typeofCustomEnum = typeof(CustomEnumAttribute);
        private static Type typeofEnum = typeof(Enum);
        private static Type typeofGuild = typeof(GuildType);
        private static Type typeofBool = typeof(Boolean);
        private static Type typeofString = typeof(String);
        private static Type typeofPoison = typeof(Poison);
        private static Type typeofMap = typeof(Map);
        private static Type typeofSkills = typeof(Skills);
        private static Type typeofPropertyObject = typeof(PropertyObjectAttribute);
        private static Type typeofNoSort = typeof(NoSortAttribute);

        private static Type[] typeofReal = new Type[]
			{
				typeof( Single ),
				typeof( Double )
			};

        private static Type[] typeofNumeric = new Type[]
			{
				typeof( Byte ),
				typeof( Int16 ),
				typeof( Int32 ),
				typeof( Int64 ),
				typeof( SByte ),
				typeof( UInt16 ),
				typeof( UInt32 ),
				typeof( UInt64 )
			};
	}

    public class AListRemoveConfirmationGump<T> : MessageBoxGump
    {
        private const string Description = "Etes-vous s�r de vouloir effacer cet objet de la liste?";
        private PropertyInfo prop;
        private object o;
        private Stack stack;
        private int page;
        private ArrayList list ;
        private int index;
        private List<T> mainList;
        public AListRemoveConfirmationGump(List<T> mainList, PropertyInfo prop, RacePlayerMobile mobile, object o, Stack stack, int page, ArrayList list, int index)
            : base(mobile, "Confirmation", Description)
        {
             this.prop = prop;
             this.o = o;
             this.stack = stack;
             this.page = page;
             this.list = list;
             this.index = index;
             this.mainList = mainList;
        }

        public override void OnYesClicked(RacePlayerMobile owner, string title, string text)
        {
            mainList.RemoveAt(index);
            m_Owner.SendGump(new ASetListGump<T>(prop, owner, o, stack, page, list));
        }

        public override void OnNoClicked(RacePlayerMobile owner, string title, string text)
        {
            m_Owner.SendGump(new ASetListGump<T>(prop, owner, o, stack, page, list));
        }
    }

    public class AListClearConfirmationGump<T> : MessageBoxGump
    {
        private const string Description = "Etes-vous s�r de vouloir vider la liste compl�tement?";
        private PropertyInfo prop;
        private object o;
        private Stack stack;
        private int page;
        private ArrayList list ;
        private int index;
        private List<T> mainList;
        public AListClearConfirmationGump(List<T> mainList, PropertyInfo prop, RacePlayerMobile mobile, object o, Stack stack, int page, ArrayList list, int index)
            : base(mobile, "Confirmation", Description)
        {
             this.prop = prop;
             this.o = o;
             this.stack = stack;
             this.page = page;
             this.list = list;
             this.index = index;
             this.mainList = mainList;
        }

        public override void OnYesClicked(RacePlayerMobile owner, string title, string text)
        {
            mainList.Clear();
            m_Owner.SendGump(new ASetListGump<T>(prop, owner, o, stack, page, list));
        }

        public override void OnNoClicked(RacePlayerMobile owner, string title, string text)
        {
            m_Owner.SendGump(new ASetListGump<T>(prop, owner, o, stack, page, list));
        }
    }
}