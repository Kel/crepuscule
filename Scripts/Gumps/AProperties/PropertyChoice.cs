﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Scripts.Commands;

namespace Server
{
    public interface IWeakChoice
    {
        void SetValueByKey(object Object, object KeyPropertyValue);
        void SetValueDirectly(object Object, object Value);
        IEnumerable<Pair<object, string>> GetChoiceList(object Object);
    }

    public class WeakChoice<T> : IWeakChoice where T :class
    {
        // List to chose from, Title to show in the gump and Key to link with Value property
        public string ObjectKeyProperty { get; set; }
        public string ListProperty { get; set; }
        public string ListKeyProperty { get; set; }

        private T fValue;


        public T Value { get; private set; }
        public bool HasValue { get { return Value != default(T); } }

        public WeakChoice(string ObjectKeyProperty, string ListProperty, string ListKeyProperty)
        {
            this.ListProperty = ListProperty;
            this.ListKeyProperty = ListKeyProperty;
            this.ObjectKeyProperty = ObjectKeyProperty;
        }

        /// <summary>
        /// This one sets directly the value 
        /// </summary>
        public void SetValueDirectly(object Object, object value)
        {
            Value = value as T;

            // Set the linked key
            if (ObjectKeyProperty != null && ListKeyProperty != null)
            {
                var KeyPropertyValue = Properties.GetPropertyValue(Value, ListKeyProperty);
                Properties.SetPropertyValue(Object, ObjectKeyProperty, KeyPropertyValue);
            }
        }

        /// <summary>
        /// Use this for setting by an internal (indirect) key, as internal name or something
        /// </summary>
        public void SetValueByKey(object Object, object KeyPropertyValue)
        {
            if (Object == null || ListProperty == null || ListKeyProperty == null)
            {
                //WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, "Some parameters are wrong!");
            }
            else
            {
                if (KeyPropertyValue == null)
                {
                    Value = default(T);
                    return;
                }

                
                var list = Properties.GetPropertyValue(Object, ListProperty) as IList;
                if (list != null)
                {
                    foreach (var item in list)
                    {
                        var itemValue = Properties.GetPropertyValue(item, ListKeyProperty);
                        if (itemValue != null && itemValue.Equals(KeyPropertyValue))
                        {
                            Value = item as T;
                            break;
                        }
                    }
                }

            }
        }

        public IEnumerable<Pair<object, string>> GetChoiceList(object Object)
        {
            var result = new List<Pair<object, string>>();
            var list = Properties.GetPropertyValue(Object, ListProperty) as IEnumerable;
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (item == null) continue;
                    var value = new Pair<object, string>();
                    value.First = item;
                    value.Second = item.ToString();

                    result.Add(value);
                }
            }
            return result;
        }



        public override string ToString()
        {
            if (!HasValue)
                return "(Non-Défini)";
            else
            {
                return Value.ToString();
            }
        }

    }

}
