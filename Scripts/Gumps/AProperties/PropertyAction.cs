﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    public class PropertyAction
    {
        public Mobile Owner;
        public string Name = "Bouton";
        public PropertyAction(Mobile owner)
        {
            Owner = owner;
        }
        public PropertyAction(string name)
        {
            Name = name;
        }
        public override string ToString()
        {
            return Name;
        }
    }

}
