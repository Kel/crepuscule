/* --------------------------->
 * Advanced properties script by Kaon 
 * Version 1.0
 * Creation : 28.03.2007
 * Revision : N/A
 * --------------------------->
 */
using System;
using System.Reflection;
using System.Collections;
using Server;
using Server.Network;


namespace Server.Gumps
{
	public class ASetCustomEnumGump : ASetListOptionGump
	{
		private string[] m_Names;

		public ASetCustomEnumGump( PropertyInfo prop, Mobile mobile, object o, Stack stack, int propspage, ArrayList list, string[] names, int index ) : base( prop, mobile, o, stack, propspage, list, names, null, index )
		{
			m_Names = names;
		}

		public override void OnResponse( NetState sender, RelayInfo relayInfo )
		{
			int index = relayInfo.ButtonID - 1;

			if ( index >= 0 && index < m_Names.Length )
			{
				try
				{
					MethodInfo info = m_Property.PropertyType.GetMethod( "Parse", new Type[]{ typeof( string ) } );

					string result = "";

					if ( info != null )
                        result = AProperties.SetDirect(m_Mobile, m_Object, m_Object, m_Property, m_Property.Name, info.Invoke(null, new object[] { m_Names[index] }), true, m_Index);
					else if ( m_Property.PropertyType == typeof( Enum ) || m_Property.PropertyType.IsSubclassOf( typeof( Enum ) ) )
                        result = AProperties.SetDirect(m_Mobile, m_Object, m_Object, m_Property, m_Property.Name, Enum.Parse(m_Property.PropertyType, m_Names[index], false), true, m_Index);

					m_Mobile.SendMessage( result );

					if ( result == "Property has been set." )
						APropertiesGump.OnValueChanged( m_Object, m_Property, m_Stack, m_Index );
				}
				catch
				{
					m_Mobile.SendMessage( "An exception was caught. The property may not have changed." );
				}
			}

			m_Mobile.SendGump( new APropertiesGump( m_Mobile, m_Object, m_Stack, m_List, m_Page ) );
		}
	}
}