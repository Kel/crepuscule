/* --------------------------->
 * Advanced properties script by Kaon 
 * Version 1.0
 * Creation : 28.03.2007
 * Revision : N/A
 * --------------------------->
 */
using System;
namespace Server
{
    [AttributeUsage(AttributeTargets.Property)]
    public class UniqueInListProperty : Attribute
    {
    }
}
