﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class TemplateAttribute : PropertyObjectAttribute
    {
        internal Type m_Type;

        // This is a positional argument
        public TemplateAttribute(Type type)
        {
            m_Type = type;
        }
    }

    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class InheritedFromAttribute : Attribute
    {
        public Type m_Type;

        // This is a positional argument
        public InheritedFromAttribute(Type type)
        {
            m_Type = type;
        }
    }


    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class StartGroupAttribute : Attribute
    {
        public string GroupName;
        public int TextHue = 0;

        public StartGroupAttribute(string GroupName)
        {
            this.GroupName = GroupName;
        }

        public StartGroupAttribute(string GroupName, int TextHue)
        {
            this.GroupName = GroupName;
            this.TextHue = TextHue;
        }
    }

    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class NoSetIfNullAttribute : Attribute
    {
        public NoSetIfNullAttribute()
        {
        }
    }


    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class ChoiceParamAttribute : Attribute
    {
        internal string m_Name;

        // This is a positional argument
        public ChoiceParamAttribute(string propertyName)
        {
            m_Name = propertyName;
        }
    }

    
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class PropertyObjectsListAttribute : CommandPropertyAttribute
    {
        // This is a positional argument
        public PropertyObjectsListAttribute() : base(AccessLevel.GameMaster)
        {
        }

        public PropertyObjectsListAttribute(AccessLevel ac)
            : base(ac)
        {
        }
    }
}
