/* --------------------------->
 * Advanced properties script by Kaon 
 * Version 1.0
 * Creation : 28.03.2007
 * Revision : N/A
 * --------------------------->
 */
using System;
using System.Reflection;
using System.Collections;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Scripts.Commands;


namespace Server.Gumps
{
	public class ASetObjectTarget : Target
	{
		private PropertyInfo m_Property;
		private Mobile m_Mobile;
		private object m_Object;
		private Stack m_Stack;
		private Type m_Type;
		private int m_Page;
		private ArrayList m_List;
        private int m_Index;

		public ASetObjectTarget( PropertyInfo prop, Mobile mobile, object o, Stack stack, Type type, int page, ArrayList list, int index ) : base( -1, false, TargetFlags.None )
		{
			m_Property = prop;
			m_Mobile = mobile;
			m_Object = o;
			m_Stack = stack;
			m_Type = type;
			m_Page = page;
			m_List = list;
            m_Index = index;
		}

		protected override void OnTarget( Mobile from, object targeted )
		{
			try
			{
				if ( m_Type == typeof( Type ) )
					targeted = targeted.GetType();
				else if ( (m_Type == typeof( BaseAddon ) || m_Type.IsAssignableFrom( typeof( BaseAddon ) )) && targeted is AddonComponent )
					targeted = ((AddonComponent)targeted).Addon;

				if ( m_Type.IsAssignableFrom( targeted.GetType() ) )
				{
					CommandLogging.LogChangeProperty( m_Mobile, m_Object, m_Property.Name, targeted.ToString() );
                    if (m_Index >= 0)
                    {
                        object[] indexes = new object[] { m_Index };
                        m_Property.SetValue(m_Object, targeted, indexes);
                    }
                    else m_Property.SetValue(m_Object, targeted, null);
					APropertiesGump.OnValueChanged( m_Object, m_Property, m_Stack, m_Index );
				}
				else
				{
					m_Mobile.SendMessage( "That cannot be assigned to a property of type : {0}", m_Type.Name );
				}
			}
			catch
			{
				m_Mobile.SendMessage( "An exception was caught. The property may not have changed." );
			}
		}

		protected override void OnTargetFinish( Mobile from )
		{
			if ( m_Type == typeof( Type ) )
				from.SendGump( new APropertiesGump( m_Mobile, m_Object, m_Stack, m_List, m_Page ) );
			else
				from.SendGump( new ASetObjectGump( m_Property, m_Mobile, m_Object, m_Stack, m_Type, m_Page, m_List, m_Index ) );
		}
	}
}