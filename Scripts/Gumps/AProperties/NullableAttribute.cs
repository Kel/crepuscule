﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    sealed class NullableAttribute : Attribute
    {
        // This is a positional argument
        public NullableAttribute()
        {

        }
    }

}
