using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;

namespace Server.Gumps
{

    public class SecondIdentityGump : Gump
    {
        RacePlayerMobile m_pour;
        //protected TresorerieBook m_Book;
        public SecondIdentityGump(RacePlayerMobile pour)
            : base(0, 0)
        {
            // m_Book = Livre;
            m_pour = pour;
            this.Closable = false;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(74, 74, 254, 102, 9300);
            this.AddBackground(73, 47, 257, 31, 9200);
            this.AddLabel(95, 54, 0, @"Choisissez votre deuxi�me identit�");
            this.AddTextEntry(99, 102, 203, 20, 0, (int)Buttons.AgeTxt, @" ");
            this.AddButton(167, 140, 247, 248, (int)Buttons.Button2, GumpButtonType.Reply, 0);

        }

        public enum Buttons
        {
            AgeTxt,
            Button2,
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            RacePlayerMobile from = state.Mobile as RacePlayerMobile;
            RacePlayerMobile mobile = m_pour;

            int button = info.ButtonID;


            switch (button)
            {
                case 0:

                    break;

                case 1:
                    TextRelay relay = info.GetTextEntry(0);

                    string nom_introduit = (relay == null ? null : relay.Text.Trim());

                    if (nom_introduit.Length > 25 || nom_introduit.Length == 0)
                    {
                        from.SendMessage("Veuillez entrer un nom de moins de 25 lettres.");
                        from.CloseGump(typeof(SecondIdentityGump));
                        from.SendGump(new SecondIdentityGump(mobile));
                    }
                    else
                    {
                        from.SecondIdentity = nom_introduit;
                        from.CloseGump(typeof(SecondIdentityGump));
                        from.CloseGump(typeof(ChoseAlignmentGump));
                        from.SendGump(new ChoseAlignmentGump());
                    }
                    break;

            }



        }


    }
}
