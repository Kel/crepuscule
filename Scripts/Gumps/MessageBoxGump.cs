﻿using System;
using Server;
using Server.Network;
using Server.Items;
using Server.Engines.PartySystem;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Server.Mobiles;

namespace Server.Gumps
{

    public abstract class MessageBoxGump : Gump
    {

        protected RacePlayerMobile m_Owner;
        protected string m_Title;
        protected string m_Text;

        public MessageBoxGump(RacePlayerMobile owner, string title, string text)
            : this(owner, title, text, true, true)
        {

        }
        public MessageBoxGump(RacePlayerMobile owner, string title, string text, bool yesButton, bool noButton): base(0, 0)
        {
            m_Owner = owner;
            m_Title = title;
            m_Text = text;

            Closable = true;
            Disposable = true;
            Dragable = true;
            Resizable = false;

            AddPage(0);
            
            AddBackground(111, 93, 280, 226, 2600);
            //AddTextEntry(180, 110, 150, 20, 0, 0, m_Title);
            AddLabel(180, 110, 0, m_Title);
            AddHtml(140, 137, 220, 130, m_Text, false, true);

            if (yesButton)  AddButton(280, 280, 4023, 4024, 1, GumpButtonType.Reply, 0);
            if (noButton)  AddButton(310, 280, 4017, 4018, 2, GumpButtonType.Reply, 0);

        }

        public abstract void OnYesClicked(RacePlayerMobile owner, string title, string text);
        public abstract void OnNoClicked(RacePlayerMobile owner, string title, string text);

        public override void OnResponse(NetState state, RelayInfo info)
        {

            switch (info.ButtonID)
            {
                case 1:
                {
                    OnYesClicked(m_Owner, m_Title, m_Text);
                    break;
                }
                case 2:
                {
                    OnNoClicked(m_Owner, m_Title, m_Text);
                    break;
                }
            }
        }
    }
}