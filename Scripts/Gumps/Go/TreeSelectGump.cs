using System;
using System.IO;
using System.Xml;
using System.Collections;
using Server.Network;
using Server.Engines;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Server.Scripts.Commands;
using Server.Misc;

namespace Server.Gumps
{

    public class TreeSelectGump : Gump
    {
        #region Constants
        public static bool OldStyle = PropsConfig.OldStyle;

        public const int GumpOffsetX = PropsConfig.GumpOffsetX;
        public const int GumpOffsetY = PropsConfig.GumpOffsetY;

        public const int TextHue = PropsConfig.TextHue;
        public const int TextOffsetX = PropsConfig.TextOffsetX;

        public const int OffsetGumpID = PropsConfig.OffsetGumpID;
        public const int HeaderGumpID = PropsConfig.HeaderGumpID;
        public const int EntryGumpID = PropsConfig.EntryGumpID;
        public const int BackGumpID = PropsConfig.BackGumpID;
        public const int SetGumpID = PropsConfig.SetGumpID;

        public const int SetWidth = PropsConfig.SetWidth;
        public const int SetOffsetX = PropsConfig.SetOffsetX, SetOffsetY = PropsConfig.SetOffsetY + (((EntryHeight - 20) / 2) / 2);
        public const int SetButtonID1 = PropsConfig.SetButtonID1;
        public const int SetButtonID2 = PropsConfig.SetButtonID2;

        public const int PrevWidth = PropsConfig.PrevWidth;
        public const int PrevOffsetX = PropsConfig.PrevOffsetX, PrevOffsetY = PropsConfig.PrevOffsetY + (((EntryHeight - 20) / 2) / 2);
        public const int PrevButtonID1 = PropsConfig.PrevButtonID1;
        public const int PrevButtonID2 = PropsConfig.PrevButtonID2;

        public const int NextWidth = PropsConfig.NextWidth;
        public const int NextOffsetX = PropsConfig.NextOffsetX, NextOffsetY = PropsConfig.NextOffsetY + (((EntryHeight - 20) / 2) / 2);
        public const int NextButtonID1 = PropsConfig.NextButtonID1;
        public const int NextButtonID2 = PropsConfig.NextButtonID2;

        public const int OffsetSize = PropsConfig.OffsetSize;

        public const int EntryHeight = 26; //24;//PropsConfig.EntryHeight;
        public const int BorderSize = PropsConfig.BorderSize;

        private static bool PrevLabel = false, NextLabel = false;

        private const int PrevLabelOffsetX = PrevWidth + 1;
        private const int PrevLabelOffsetY = 0;

        private const int NextLabelOffsetX = -29;
        private const int NextLabelOffsetY = 0;

        private const int PropWidth = 75;
        private const int PropOffset = 37;
        private const int EntryWidth = 180; //180;
        private const int EntryCount = 23; //15;

        private const int TotalWidth = OffsetSize + EntryWidth + OffsetSize + PropWidth + SetWidth + OffsetSize;
        private const int TotalHeight = OffsetSize + EntryHeight + ((EntryHeight + OffsetSize) * (EntryCount + 1));

        private const int BackWidth = BorderSize + TotalWidth + BorderSize;
        private const int BackHeight = BorderSize + TotalHeight + BorderSize;
        #endregion

        private Mobile m_Owner;
        private int m_Page;
        private CAGCategory m_Category;

        public TreeSelectGump(Mobile owner, CAGCategory category, int page)
            : base(GumpOffsetX, GumpOffsetY)
        {
            owner.CloseGump(typeof(WhoGump));
            m_Owner = owner;
            m_Category = category;
            Initialize(page);
        }


        public void Initialize(int page)
        {
            m_Page = page;

            CAGNode[] nodes = m_Category.Nodes;

            int count = nodes.Length - (page * EntryCount);

            if (count < 0)
                count = 0;
            else if (count > EntryCount)
                count = EntryCount;

            int totalHeight = OffsetSize  + ((EntryHeight + OffsetSize) * (count + 1));

            AddPage(0);

            AddBackground(0, 0, BackWidth, BorderSize + totalHeight + BorderSize, BackGumpID);
            AddImageTiled(BorderSize, BorderSize, TotalWidth - (OldStyle ? SetWidth + OffsetSize : 0), totalHeight, OffsetGumpID);

            int x = BorderSize + OffsetSize;
            int y = BorderSize + OffsetSize;

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);

            if (m_Category.Parent != null)
            {
                AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 1, GumpButtonType.Reply, 0);

                if (PrevLabel)
                    AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Previous");
            }

            x += PrevWidth + OffsetSize;

            int emptyWidth = TotalWidth - (PrevWidth * 2) - NextWidth - (OffsetSize * 5) - (OldStyle ? SetWidth + OffsetSize : 0);

            if (!OldStyle)
                AddImageTiled(x - (OldStyle ? OffsetSize : 0), y, emptyWidth + (OldStyle ? OffsetSize * 2 : 0), EntryHeight, EntryGumpID);

            AddHtml(x + TextOffsetX, y + ((EntryHeight - 20) / 2), emptyWidth - TextOffsetX, EntryHeight, String.Format("<center>{0}</center>", m_Category.Caption), false, false);

            x += emptyWidth + OffsetSize;

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);

            if (page > 0)
            {
                AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 2, GumpButtonType.Reply, 0);

                if (PrevLabel)
                    AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Previous");
            }

            x += PrevWidth + OffsetSize;

            if (!OldStyle)
                AddImageTiled(x, y, NextWidth, EntryHeight, HeaderGumpID);

            if ((page + 1) * EntryCount < nodes.Length)
            {
                AddButton(x + NextOffsetX, y + NextOffsetY, NextButtonID1, NextButtonID2, 3, GumpButtonType.Reply, 1);

                if (NextLabel)
                    AddLabel(x + NextLabelOffsetX, y + NextLabelOffsetY, TextHue, "Next");
            }


            x = BorderSize + OffsetSize;
            /*y += EntryHeight;

            AddImageTiled(x, y, TotalWidth - OffsetSize, EntryHeight, HeaderGumpID);


            // Searchbar
            AddImageTiled(x + 34, y + 4, 180, 20, 2624);
            
            AddImageTiled(x+31, y + 4, 184, 18, 0xBBC);
            AddImageTiled(x+32, y+5, 182, 16, 2624);
            AddAlphaRegion(x+32, y+5, 182, 16);

            AddButton(x, y + 2, 0x15A1, 0x15A3, 999, GumpButtonType.Reply, 999); // Search button
            AddTextEntry(x + 34, y + 4, 180, 20, 0x480, 998, m_SearchString);

            AddHtmlLocalized(x+220, y+4, 100, 20, 3010005, 0x7FFF, false, false);*/




            // Resultlist
            for (int i = 0, index = page * EntryCount; i < EntryCount && index < nodes.Length; ++i, ++index)
            {
                x = BorderSize + OffsetSize;
                y += EntryHeight + OffsetSize;

                CAGNode node = nodes[index];

                AddImageTiled(x, y, EntryWidth + PropWidth, EntryHeight, EntryGumpID);
                AddLabelCropped(x + TextOffsetX, y + ((EntryHeight - 20) / 2), EntryWidth - TextOffsetX + PropWidth, EntryHeight, TextHue, node.Caption);

                x += EntryWidth + OffsetSize;


                if (SetGumpID != 0)
                    AddImageTiled(x + PropWidth, y, SetWidth, EntryHeight, SetGumpID);


                // Add or Next button
                if (node is CAGAction)
                {
                    if(m_Owner.AccessLevel >= (node as CAGAction).AccessLevel)
                        AddButton(x + SetOffsetX + PropWidth - 2, y + SetOffsetY, 5538, 5539, i + 4, GumpButtonType.Reply, 0);
                }
                else if (!(node is CAGText))
                    AddButton(x + SetOffsetX + PropWidth, y + SetOffsetY, SetButtonID1, SetButtonID2, i + 4, GumpButtonType.Reply, 0);

                if (node is CAGCraftResource)
                {
                    var obj = (CAGCraftResource)node;
                    int itemID = obj.ItemID;

                    try
                    {
                        Rectangle2D bounds = ItemBounds.Table[itemID];

                        if (itemID != 1 && bounds.Height < (EntryHeight * 2))
                        {
                            if (bounds.Height < EntryHeight)
                                AddItem(x - OffsetSize - ((i % 2) * 44) - (bounds.Width / 2) - bounds.X, y + (EntryHeight / 2) - (bounds.Height / 2) - bounds.Y, itemID, obj.Hue);
                            else
                                AddItem(x - OffsetSize - ((i % 2) * 44) - (bounds.Width / 2) - bounds.X, y + EntryHeight - 1 - bounds.Height - bounds.Y, itemID, obj.Hue);
                        }
                    }
                    catch { }
                }
                else if (node is CAGObject)
                {
                    var obj = (CAGObject)node;
                    int itemID = obj.ItemID;

                    try
                    {
                        Rectangle2D bounds = ItemBounds.Table[itemID];

                        if (itemID != 1 && bounds.Height < (EntryHeight * 2))
                        {
                            if (bounds.Height < EntryHeight)
                                AddItem(x - OffsetSize - 22 - ((i % 2) * 44) - (bounds.Width / 2) - bounds.X, y + (EntryHeight / 2) - (bounds.Height / 2) - bounds.Y, itemID, obj.Hue);
                            else
                                AddItem(x - OffsetSize - 22 - ((i % 2) * 44) - (bounds.Width / 2) - bounds.X, y + EntryHeight - 1 - bounds.Height - bounds.Y, itemID, obj.Hue);
                        }
                    }
                    catch { }
                }

            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            Mobile from = m_Owner;

            switch (info.ButtonID)
            {
                case 0: // Closed
                    {
                        return;
                    }
                case 1: // Up
                    {
                        if (m_Category.Parent != null)
                        {
                            int index = Array.IndexOf(m_Category.Parent.Nodes, m_Category) / EntryCount;

                            if (index < 0)
                                index = 0;

                            from.SendGump(new TreeSelectGump(from, m_Category.Parent, index));
                        }

                        break;
                    }
                case 2: // Previous
                    {
                        if (m_Page > 0)
                        {
                            from.SendGump(new TreeSelectGump(from, m_Category, m_Page - 1));
                        }
                        break;
                    }
                case 3: // Next
                    {
                        if ((m_Page + 1) * EntryCount < m_Category.Nodes.Length)
                        {
                            from.SendGump(new TreeSelectGump(from, m_Category, m_Page + 1));
                        }
                        break;
                    }
                default:
                    {

                        int index = (m_Page * EntryCount) + (info.ButtonID - 4);
                        if (index >= 0 && index < m_Category.Nodes.Length)
                        {
                            if (m_Category.Nodes[index] is CAGAction)
                            {
                                from.SendGump(new TreeSelectGump(from, m_Category, m_Page));
                                m_Category.Nodes[index].OnClick(from, m_Page);
                            }
                            else if (m_Category.Nodes[index] is CAGCategory)
                            {
                                from.SendGump(new TreeSelectGump(from, m_Category.Nodes[index] as CAGCategory, 0));
                            }
                            else
                            {
                                m_Category.Nodes[index].OnClick(from, m_Page);
                            }
                        }
                    
                        break;
                    }
            }
        }

        internal static Type typeofItem = typeof(Item), typeofMobile = typeof(Mobile);
        internal static void Match(string match, Type[] types, ArrayList results)
        {
            if (match.Length == 0)
                return;

            match = match.ToLower();

            for (int i = 0; i < types.Length; ++i)
            {
                Type t = types[i];

                if ((typeofMobile.IsAssignableFrom(t) || typeofItem.IsAssignableFrom(t)) && t.Name.ToLower().IndexOf(match) >= 0 && !results.Contains(t))
                {
                    ConstructorInfo[] ctors = t.GetConstructors();

                    for (int j = 0; j < ctors.Length; ++j)
                    {
                        if (ctors[j].GetParameters().Length == 0 && ctors[j].IsDefined(typeof(ConstructableAttribute), false))
                        {
                            results.Add(t);
                            break;
                        }
                    }
                }
            }
        }


        public static ArrayList Match(string match)
        {
            ArrayList results = new ArrayList();
            Type[] types;

            Assembly[] asms = ScriptCompiler.Assemblies;

            for (int i = 0; i < asms.Length; ++i)
            {
                types = ScriptCompiler.GetTypeCache(asms[i]).Types;
                Match(match, types, results);
            }

            types = ScriptCompiler.GetTypeCache(Core.Assembly).Types;
            Match(match, types, results);

            results.Sort(new TypeNameComparer());

            return results;
        }

        private class TypeNameComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Type a = x as Type;
                Type b = y as Type;

                return a.Name.CompareTo(b.Name);
            }
        }


    }
}