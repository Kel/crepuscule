using System;
using System.IO;
using System.Xml;
using System.Collections;
using Server.Network;
using Server.Engines;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Server.Scripts.Commands;
using Server.Misc;

namespace Server.Gumps
{
    #region Categories
    public abstract class CAGNode
    {
        public abstract string Caption { get; }
        public abstract void OnClick(Mobile from, int page);
    }

    public class CAGText : CAGNode
    {
        private string m_Caption = "";
        public override string Caption { get { return m_Caption; } }
        public override void OnClick(Mobile from, int page)
        {
        }

        public CAGText(string title)
        {
            m_Caption = title;
        }
    }

    public class CAGAction : CAGNode
    {
        public AccessLevel AccessLevel { get; set; }
        public Action<Mobile, int> ClickAction {get;set;}
        private string m_Caption="";
        public override string Caption { get{return m_Caption;} }
        public override void OnClick(Mobile from, int page)
        {
            ClickAction(from, page);
        }

        public CAGAction(string title, Action<Mobile, int> action) : this(title, action, AccessLevel.GameMaster){}
        public CAGAction(string title, Action<Mobile, int> action, AccessLevel level)
        {
            m_Caption = title;
            ClickAction = action;
            AccessLevel = level;
        }
    }

    public class CAGObject : CAGNode
    {
        private Type m_Type;
        private int m_ItemID;
        private int m_Hue;
        private CAGCategory m_Parent;

        public Type Type { get { return m_Type; } }
        public int ItemID { get { return m_ItemID; } }
        public int Hue { get { return m_Hue; } }
        public CAGCategory Parent { get { return m_Parent; } }

        public override string Caption { get { return (m_Type == null ? "bad type" : m_Type.Name); } }

        public override void OnClick(Mobile from, int page)
        {
            if (m_Type == null)
            {
                from.SendMessage("That is an invalid type name.");
            }
            else
            {
                Commands.Handle(from, String.Format("{0}Add {1}", Commands.CommandPrefix, m_Type.Name));

                from.SendGump(new CategorizedAddGump(from, m_Parent, page));
            }
        }

        public CAGObject(CAGCategory parent, Type type, int itemID, int hue)
        {
            m_Parent = parent;

            m_Type = type;
            m_ItemID = itemID;
            m_Hue = hue;
        }

        public CAGObject(CAGCategory parent, XmlTextReader xml)
        {
            m_Parent = parent;

            if (xml.MoveToAttribute("type"))
                m_Type = ScriptCompiler.FindTypeByFullName(xml.Value, false);

            if (xml.MoveToAttribute("gfx"))
                m_ItemID = XmlConvert.ToInt32(xml.Value);

            if (xml.MoveToAttribute("hue"))
                m_Hue = XmlConvert.ToInt32(xml.Value);
        }
    }

    public class CAGCategory : CAGNode
    {
        private string m_Title;
        private CAGNode[] m_Nodes;
        private CAGCategory m_Parent;

        public string Title { get { return m_Title; } }
        public CAGNode[] Nodes { get { return m_Nodes; } set { m_Nodes = value; } }
        public CAGCategory Parent { get { return m_Parent; } }

        public override string Caption { get { return m_Title; } }


        public override void OnClick(Mobile from, int page)
        {
            from.SendGump(new CategorizedAddGump(from, this, 0));
        }

        private CAGCategory()
        {
            m_Title = "no data";
            m_Nodes = new CAGNode[0];
        }

        public CAGCategory(string title)
        {
            m_Title = title;
            m_Parent = CAGCategory.Root;
            m_Nodes = new CAGNode[0];
        }

        public CAGCategory(string title, CAGCategory parent)
        {
            m_Title = title;
            m_Parent = parent;
            m_Nodes = new CAGNode[0];
        }

        public CAGCategory(CAGCategory parent, XmlTextReader xml)
        {
            m_Parent = parent;

            if (xml.MoveToAttribute("title"))
                m_Title = xml.Value;
            else
                m_Title = "empty";

            if (m_Title == "Docked")
                m_Title = "Docked 2";

            if (xml.IsEmptyElement)
            {
                m_Nodes = new CAGNode[0];
            }
            else
            {
                ArrayList nodes = new ArrayList();

                while (xml.Read() && xml.NodeType != XmlNodeType.EndElement)
                {
                    if (xml.Name == "category" && xml.IsEmptyElement)
                        continue;

                    if (xml.NodeType == XmlNodeType.Element && xml.Name == "object")
                        nodes.Add(new CAGObject(this, xml));
                    else if (xml.NodeType == XmlNodeType.Element && xml.Name == "category" && !xml.IsEmptyElement)
                        nodes.Add(new CAGCategory(this, xml));
                    else
                        xml.Skip();
                }

                m_Nodes = (CAGNode[])nodes.ToArray(typeof(CAGNode));
            }
        }

        public void AddNode(CAGNode newNode)
        {
            var arr = new List<CAGNode>();
            foreach (var node in m_Nodes)
                arr.Add(node);
            arr.Add(newNode);
            m_Nodes = arr.ToArray();
        }

        private static CAGCategory m_Root;

        public static CAGCategory Root
        {
            get
            {
                if (m_Root == null)
                {
                    m_Root = Load("Data/objects.xml");

                    if (m_Root != null)
                    {
                        var managers = new CAGCategory(" - Commandes de Gestion");
                        managers.AddNode(new CAGText("Interfaces"));
                        managers.AddNode(new CAGAction("  Gestion: Utilisateurs en Ligne", (mobile, page) => WhoGump.ShowWhoGump(mobile), AccessLevel.Counselor));
                        managers.AddNode(new CAGAction("  Gestion: Achat et Vente", (mobile, page) => CraftManagerCommand.ShowTradeGump(mobile), AccessLevel.GameMaster));
                        managers.AddNode(new CAGAction("  Gestion: Cat�gories de Craft", (mobile, page) => CraftManagerCommand.ShowCraftGump(mobile), AccessLevel.GameMaster));
                        managers.AddNode(new CAGAction("  Gestion: Loot", (mobile, page) => CraftManagerCommand.ShowLootPacksGump(mobile), AccessLevel.GameMaster));
                        managers.AddNode(new CAGAction("  Gestion: Classes", (mobile, page) => ClassesCommand.ShowClassesGump(mobile), AccessLevel.Seer));
                        managers.AddNode(new CAGAction("  Gestion: Coter les Guildes", (mobile, page) => MarkGuildCommand.ShowMarkGuildGump(mobile), AccessLevel.GameMaster));

                        
                        managers.AddNode(new CAGText("Commandes"));
                        managers.AddNode(new CAGAction("  Update: Objets", (mobile, page) => CraftManagerUpdateCommand.UpdateCrafts(), AccessLevel.GameMaster));
                        managers.AddNode(new CAGAction("  Update: Cr�atures", (mobile, page) => CreaturesUpdateCommand.UpdateCreatures(), AccessLevel.GameMaster));
                        managers.AddNode(new CAGAction("  Update: Classes", (mobile, page) => ClassesUpdateCommand.UpdateClasses(), AccessLevel.Seer));

                        var admin = new CAGCategory(" - Commandes Administratives");
                        admin.AddNode(new CAGText("Interfaces"));
                        admin.AddNode(new CAGAction("  Gestion: Administration", (mobile, page) => AdminGump.ShowAdminGump(mobile), AccessLevel.Administrator));
                        admin.AddNode(new CAGText("Commandes"));
                        admin.AddNode(new CAGAction("  Update: Scripts", (mobile, page) => UpdateCommand.UpdateScripts(), AccessLevel.Administrator));
                        admin.AddNode(new CAGAction("  Update: Statiques", (mobile, page) => Patch_Command.UpdateStatics(), AccessLevel.Administrator));
                        admin.AddNode(new CAGAction("  Sauvegarde", (mobile, page) => World.Save(), AccessLevel.Administrator));
                        admin.AddNode(new CAGAction("  Red�marrage", (mobile, page) => AutoRestart.DoAutoRestart(mobile) , AccessLevel.Administrator));
                        admin.AddNode(new CAGAction("  Red�marrage Planifi�", (mobile, page) => RestartSoon.DoRestartSoon(mobile), AccessLevel.Administrator));

                        m_Root.AddNode(new CAGText("Ressources"));
                        m_Root.AddNode(managers);
                        m_Root.AddNode(admin);
                        
                        //CraftManagerUpdateCommand
                    }
                }

                return m_Root;
            }
        }

        public static void RebuildRessourceNode()
        {
            var root = new CAGCategory("Ressources");

            var SubRessources = ManagerConfig.Items.Values
                .Where(item => item.Craft.ParentRessource != null);
            SubRessources
                .Select(item => ManagerConfig.Items[item.Craft.ParentRessource])
                .Where(item => item.Craft.ParentRessource == null)
                .Distinct()
                .ToList()
                .ForEach(subroot =>
                {
                    subroot.Craft.RebuildMutableResourcesList();
                    BuildRecursive(SubRessources, root, subroot);
                });


            Root.Nodes[2] = root;
        }

        private static void BuildRecursive(IEnumerable<GameItemType> allRes, CAGCategory category, GameItemType type)
        {
            var subres = allRes
                .Where(item => item.Craft.ParentRessource == type.Type)
                .ToList();

            if (subres.Count > 0)
            {
                var node = new CAGCategory(type.Type.GetShortName(), category);
                node.AddNode(new CAGObject(node, type.Type, type.ItemID, type.Hue));
                type.HasSubResources = true;

                foreach (var sub in subres)
                {
                    CAGCategory.BuildRecursive(allRes, node, sub);
                }

                category.AddNode(node);
            }
            else
            {
                type.HasSubResources = false;
                category.AddNode(new CAGObject(category, type.Type, type.ItemID, type.Hue));
            }
        }

        public static CAGCategory Load(string path)
        {
            if (File.Exists(path))
            {
                XmlTextReader xml = new XmlTextReader(path);

                xml.WhitespaceHandling = WhitespaceHandling.None;

                while (xml.Read())
                {
                    if (xml.Name == "category" && xml.NodeType == XmlNodeType.Element && !xml.IsEmptyElement)
                    {
                        CAGCategory cat = new CAGCategory(null, xml);

                        xml.Close();

                        return cat;
                    }
                }
            }

            return new CAGCategory();
        }
    }

    #endregion

    public enum CategorizedAddGumpEnum
    {
        Add,
        Set
    }

    public class CategorizedAddGump : Gump
    {
        public static bool OldStyle = PropsConfig.OldStyle;

        public const int GumpOffsetX = PropsConfig.GumpOffsetX;
        public const int GumpOffsetY = PropsConfig.GumpOffsetY;

        public const int TextHue = PropsConfig.TextHue;
        public const int TextOffsetX = PropsConfig.TextOffsetX;

        public const int OffsetGumpID = PropsConfig.OffsetGumpID;
        public const int HeaderGumpID = PropsConfig.HeaderGumpID;
        public const int EntryGumpID = PropsConfig.EntryGumpID;
        public const int BackGumpID = PropsConfig.BackGumpID;
        public const int SetGumpID = PropsConfig.SetGumpID;

        public const int SetWidth = PropsConfig.SetWidth;
        public const int SetOffsetX = PropsConfig.SetOffsetX, SetOffsetY = PropsConfig.SetOffsetY + (((EntryHeight - 20) / 2) / 2);
        public const int SetButtonID1 = PropsConfig.SetButtonID1;
        public const int SetButtonID2 = PropsConfig.SetButtonID2;

        public const int PrevWidth = PropsConfig.PrevWidth;
        public const int PrevOffsetX = PropsConfig.PrevOffsetX, PrevOffsetY = PropsConfig.PrevOffsetY + (((EntryHeight - 20) / 2) / 2);
        public const int PrevButtonID1 = PropsConfig.PrevButtonID1;
        public const int PrevButtonID2 = PropsConfig.PrevButtonID2;

        public const int NextWidth = PropsConfig.NextWidth;
        public const int NextOffsetX = PropsConfig.NextOffsetX, NextOffsetY = PropsConfig.NextOffsetY + (((EntryHeight - 20) / 2) / 2);
        public const int NextButtonID1 = PropsConfig.NextButtonID1;
        public const int NextButtonID2 = PropsConfig.NextButtonID2;

        public const int OffsetSize = PropsConfig.OffsetSize;

        public const int EntryHeight = 26; //24;//PropsConfig.EntryHeight;
        public const int BorderSize = PropsConfig.BorderSize;

        private static bool PrevLabel = false, NextLabel = false;

        private const int PrevLabelOffsetX = PrevWidth + 1;
        private const int PrevLabelOffsetY = 0;

        private const int NextLabelOffsetX = -29;
        private const int NextLabelOffsetY = 0;

        private const int PropWidth = 75;
        private const int PropOffset = 37;
        private const int EntryWidth = 230; //180; //180;
        private const int EntryCount = 23; //15;

        private const int TotalWidth = OffsetSize + EntryWidth + OffsetSize + PropWidth + SetWidth + OffsetSize;
        private const int TotalHeight = OffsetSize + EntryHeight + ((EntryHeight + OffsetSize) * (EntryCount + 1));

        private const int BackWidth = BorderSize + TotalWidth + BorderSize;
        private const int BackHeight = BorderSize + TotalHeight + BorderSize;

        private CategorizedAddGumpEnum m_Method = CategorizedAddGumpEnum.Add;
        private Mobile m_Owner;
        private bool m_InSearchMode = false;
        private string m_SearchString = "";
        private Type[] m_SearchResults;
        private Stack m_Stack;
        private int m_Index;
        private ArrayList m_List;
        private CAGCategory m_Category;
        private PropertyInfo m_Property;
        private object m_Object;
        private int m_Page;

        #region AddMenu Command
        public static void Initialize()
        {
            Commands.Register("AddMenu", AccessLevel.GameMaster, new CommandEventHandler(AddMenu_OnCommand));
        }

        [Usage("AddMenu [searchString]")]
        [Description("Opens an add menu, with an optional initial search string. This menu allows you to search for Items or Mobiles and add them interactively.")]
        private static void AddMenu_OnCommand(CommandEventArgs e)
        {
            string val = e.ArgString.Trim();
            Type[] types;
            bool explicitSearch = false;

            if (val.Length == 0)
            {
                types = Type.EmptyTypes;
            }
            else if (val.Length < 3)
            {
                e.Mobile.SendMessage("Invalid search string.");
                types = Type.EmptyTypes;
            }
            else
            {
                types = (Type[])CategorizedAddGump.Match(val).ToArray(typeof(Type));
                explicitSearch = true;
            }

            e.Mobile.SendGump(new CategorizedAddGump(e.Mobile, val, 0, types, explicitSearch));
        }
        #endregion

        public CategorizedAddGump(Mobile owner)
            : this(owner, CAGCategory.Root, 0, CategorizedAddGumpEnum.Add, null, null, null,null, 0)
        {
        }

        public CategorizedAddGump(Mobile owner, PropertyInfo prop, object obj, Stack stack, ArrayList list, int index)
            : this(owner, CAGCategory.Root, 0, CategorizedAddGumpEnum.Set, prop, obj, stack, list, index)
        {
        }

        public CategorizedAddGump(Mobile owner, string searchString, int page, Type[] searchResults, bool explicitSearch)
            : this(owner, searchString, page, searchResults, explicitSearch, CategorizedAddGumpEnum.Add, null, null, null, null, 0)
        {
        }


        public CategorizedAddGump(Mobile owner, CAGCategory category, int page, CategorizedAddGumpEnum method)
            : this(owner, category, page, method, null, null, null, null, 0)
        {
        }

        public CategorizedAddGump(Mobile owner, CAGCategory category, int page)
            : this(owner, category, page, CategorizedAddGumpEnum.Add, null, null, null, null, 0)
        {
        }

        public CategorizedAddGump(Mobile owner, CAGCategory category, int page, CategorizedAddGumpEnum method, PropertyInfo prop, object obj, Stack stack, ArrayList list, int index)
            : base(GumpOffsetX, GumpOffsetY)
        {
            owner.CloseGump(typeof(WhoGump));

            m_List = list;
            m_Stack = stack;
            m_Index = index;
            m_Property = prop;
            m_Object = obj;
            m_Method = method;
            m_Owner = owner;
            m_Category = category;

            Initialize(page);
        }

        public CategorizedAddGump(Mobile owner, string searchString, int page, Type[] searchResults, bool explicitSearch, CategorizedAddGumpEnum method, PropertyInfo prop, object obj, Stack stack, ArrayList list, int index)
            : base(GumpOffsetX, GumpOffsetY)
        {
            m_Owner = owner;
        	m_SearchString = searchString;
        	m_SearchResults = searchResults;
        	m_Page = page;
            m_Method = method;
            m_Property = prop;
            m_Object = obj;
            m_Stack = stack;
            m_Index = index;
            m_List = list;

            m_InSearchMode = true;

            Initialize(page);
        }

        public void Initialize(int page)
        {
            m_Page = page;

            if (m_InSearchMode)
            {
                m_Category = new CAGCategory("Recherche:");
                var snodes = new List<CAGNode>();

                if (m_SearchResults != null && m_SearchResults.Length > 0)
                {
                    foreach (var type in m_SearchResults)
                    {
                        int itemID = 1;
                        int hue = 0;
                        if (ManagerConfig.Items.ContainsKey(type))
                        {
                            itemID = ManagerConfig.Items[type].ItemID;
                        }

                        var obj = new CAGObject(m_Category, type, itemID, hue);
                        snodes.Add(obj as CAGNode);
                    }
                }

                if (snodes.Count == 0)
                    snodes.Add(new CAGText("Aucun r�sultat trouv�"));

                m_Category.Nodes = snodes.ToArray();
            }

            // dont take menus
            CAGNode[] nodes = m_Category.Nodes;
            if (m_Method == CategorizedAddGumpEnum.Set && m_Category == CAGCategory.Root)
                nodes = nodes.Take(3).ToArray();
            
            
            int count = nodes.Length - (page * EntryCount);

            if (count < 0)
                count = 0;
            else if (count > EntryCount)
                count = EntryCount;

            int totalHeight = OffsetSize + EntryHeight + ((EntryHeight + OffsetSize) * (count + 1));

            AddPage(0);

            AddBackground(0, 0, BackWidth, BorderSize + totalHeight + BorderSize, BackGumpID);
            AddImageTiled(BorderSize, BorderSize, TotalWidth - (OldStyle ? SetWidth + OffsetSize : 0), totalHeight, OffsetGumpID);

            int x = BorderSize + OffsetSize;
            int y = BorderSize + OffsetSize;

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);

            if (m_Category.Parent != null)
            {
                AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 1, GumpButtonType.Reply, 0);

                if (PrevLabel)
                    AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Previous");
            }

            x += PrevWidth + OffsetSize;

            int emptyWidth = TotalWidth - (PrevWidth * 2) - NextWidth - (OffsetSize * 5) - (OldStyle ? SetWidth + OffsetSize : 0);

            if (!OldStyle)
                AddImageTiled(x - (OldStyle ? OffsetSize : 0), y, emptyWidth + (OldStyle ? OffsetSize * 2 : 0), EntryHeight, EntryGumpID);

            AddHtml(x + TextOffsetX, y + ((EntryHeight - 20) / 2), emptyWidth - TextOffsetX, EntryHeight, String.Format("<center>{0}</center>", m_Category.Caption), false, false);

            x += emptyWidth + OffsetSize;

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);

            if (page > 0)
            {
                AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 2, GumpButtonType.Reply, 0);

                if (PrevLabel)
                    AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Previous");
            }

            x += PrevWidth + OffsetSize;

            if (!OldStyle)
                AddImageTiled(x, y, NextWidth, EntryHeight, HeaderGumpID);

            if ((page + 1) * EntryCount < nodes.Length)
            {
                AddButton(x + NextOffsetX, y + NextOffsetY, NextButtonID1, NextButtonID2, 3, GumpButtonType.Reply, 1);

                if (NextLabel)
                    AddLabel(x + NextLabelOffsetX, y + NextLabelOffsetY, TextHue, "Next");
            }


            x = BorderSize + OffsetSize;
            y += EntryHeight;

            AddImageTiled(x, y, TotalWidth - OffsetSize, EntryHeight, HeaderGumpID);


            // Searchbar
            AddImageTiled(x + 34, y + 4, 210, 20, 2624);
            
            AddImageTiled(x+31, y + 4, 214, 18, 0xBBC);
            AddImageTiled(x+32, y+5, 212, 16, 2624);
            AddAlphaRegion(x+32, y+5, 212, 16);

            AddButton(x, y + 2, 0x15A1, 0x15A3, 999, GumpButtonType.Reply, 999); // Search button
            AddTextEntry(x + 34, y + 4, 210, 20, 0x480, 998, m_SearchString);

            AddHtmlLocalized(x+250, y+4, 100, 20, 3010005, 0x7FFF, false, false);




            // Resultlist
            for (int i = 0, index = page * EntryCount; i < EntryCount && index < nodes.Length; ++i, ++index)
            {
                x = BorderSize + OffsetSize;
                y += EntryHeight + OffsetSize;

                CAGNode node = nodes[index];

                AddImageTiled(x, y, EntryWidth + PropWidth, EntryHeight, EntryGumpID);
                AddLabelCropped(x + TextOffsetX, y + ((EntryHeight - 20) / 2), EntryWidth - TextOffsetX + PropWidth, EntryHeight, TextHue, node.Caption);

                x += EntryWidth + OffsetSize;


                if (SetGumpID != 0)
                    AddImageTiled(x + PropWidth, y, SetWidth, EntryHeight, SetGumpID);


                // Add or Next button
                if (node is CAGAction)
                {
                    if(m_Owner.AccessLevel >= (node as CAGAction).AccessLevel)
                        AddButton(x + SetOffsetX + PropWidth - 2, y + SetOffsetY, 5538, 5539, i + 4, GumpButtonType.Reply, 0);
                }
                else if (!(node is CAGText))
                    AddButton(x + SetOffsetX + PropWidth, y + SetOffsetY, SetButtonID1, SetButtonID2, i + 4, GumpButtonType.Reply, 0);

                if (node is CAGObject)
                {
                    CAGObject obj = (CAGObject)node;
                    int itemID = obj.ItemID;

                    // Inherit / Edit Type buttons
                    if (obj != null && obj.Type != null && ManagerConfig.Items.ContainsKey(obj.Type))
                    {
                        AddButton(x + PropOffset - 35, y + SetOffsetY, 0xFBA, 0xFBB, 10000 + i + 4, GumpButtonType.Reply, 0);
                        AddButton(x + PropOffset, y + SetOffsetY, 0xFBD, 0xFBE, 1000 + i + 4, GumpButtonType.Reply, 0);
                    }
                    if (obj != null && obj.Type != null && ManagerConfig.Creatures.ContainsKey(obj.Type))
                    {
                        AddButton(x + PropOffset - 35, y + SetOffsetY, 0xFBA, 0xFBB, 10000 + i + 4, GumpButtonType.Reply, 0);
                        AddButton(x + PropOffset, y + SetOffsetY, 0xFA8, 0xFA9, 1000 + i + 4, GumpButtonType.Reply, 0);
                    }

                    try
                    {
                        Rectangle2D bounds = ItemBounds.Table[itemID];

                        if (itemID != 1 && bounds.Height < (EntryHeight * 2))
                        {
                            if (bounds.Height < EntryHeight)
                                AddItem(x - OffsetSize -40 - ((i % 2) * 44) - (bounds.Width / 2) - bounds.X, y + (EntryHeight / 2) - (bounds.Height / 2) - bounds.Y, itemID, obj.Hue);
                            else
                                AddItem(x - OffsetSize - 40 - ((i % 2) * 44) - (bounds.Width / 2) - bounds.X, y + EntryHeight - 1 - bounds.Height - bounds.Y, itemID, obj.Hue);
                        }
                    }
                    catch { }
                }
            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            Mobile from = m_Owner;

            switch (info.ButtonID)
            {
                case 0: // Closed
                    {
                        return;
                    }
                case 1: // Up
                    {
                        if (m_Category.Parent != null)
                        {
                            int index = Array.IndexOf(m_Category.Parent.Nodes, m_Category) / EntryCount;

                            if (index < 0)
                                index = 0;

                            from.SendGump(new CategorizedAddGump(from, m_Category.Parent, index, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                        }

                        break;
                    }
                case 2: // Previous
                    {
                        if (m_Page > 0)
                        {
                            if (m_InSearchMode)
                            {
                                from.SendGump(new CategorizedAddGump(from, m_SearchString, m_Page - 1, m_SearchResults, true, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                            }
                            else
                            {
                                from.SendGump(new CategorizedAddGump(from, m_Category, m_Page - 1, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                            }
                        }
                        break;
                    }
                case 3: // Next
                    {
                        if ((m_Page + 1) * EntryCount < m_Category.Nodes.Length)
                        {
                            if (m_InSearchMode)
                            {
                                from.SendGump(new CategorizedAddGump(from, m_SearchString, m_Page + 1, m_SearchResults, true, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                            }
                            else
                            {
                                from.SendGump(new CategorizedAddGump(from, m_Category, m_Page + 1, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                            }
                        }
                        break;
                    }
                case 999: // Search
                    {
                        TextRelay te = info.GetTextEntry(998);
                        string match = (te == null ? "" : te.Text.Trim());

                        if (match.Length < 3)
                        {
                            from.SendMessage("Invalid search string.");
                            from.SendGump(new CategorizedAddGump(from, match, m_Page, m_SearchResults, false, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                        }
                        else
                        {
                            from.SendGump(new CategorizedAddGump(from, match, 0, (Type[])Match(match).ToArray(typeof(Type)), true, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                        }

                        break;
                    }
                default:
                    {
                        if (info.ButtonID >= 10000) //Inherit
                        {
                            int index = (m_Page * EntryCount) + (info.ButtonID - 10000 - 4);
                            if (index >= 0 && index < m_Category.Nodes.Length)
                            {
                                var ObjectNode = m_Category.Nodes[index] as CAGObject;
                                if (ObjectNode != null && ManagerConfig.Items.ContainsKey(ObjectNode.Type))
                                {
                                    from.SendGump(new CategorizedAddGump(from, m_Category, m_Page, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                                    from.SendGump(new APropertiesGump(from, new InheritFromScriptInfo( 
                                        ManagerConfig.Items[ObjectNode.Type].Type,
                                        ManagerConfig.Items[ObjectNode.Type].ScriptName
                                        )));
                                }
                                else if (ObjectNode != null && ManagerConfig.Creatures.ContainsKey(ObjectNode.Type))
                                {
                                    from.SendGump(new CategorizedAddGump(from, m_Category, m_Page, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                                    from.SendGump(new APropertiesGump(from, new InheritFromScriptInfo(
                                          ManagerConfig.Creatures[ObjectNode.Type].Type,
                                          ManagerConfig.Creatures[ObjectNode.Type].ScriptName
                                          )));
                                }
                            }

                        }
                        else if (info.ButtonID >= 1000) // Props
                        {
                            int index = (m_Page * EntryCount) + (info.ButtonID - 1000 - 4);
                            if (index >= 0 && index < m_Category.Nodes.Length)
                            {
                                var ObjectNode = m_Category.Nodes[index] as CAGObject;
                                if (ObjectNode != null && ManagerConfig.Items.ContainsKey(ObjectNode.Type))
                                {
                                    from.SendGump(new CategorizedAddGump(from, m_Category, m_Page, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                                    from.SendGump(new APropertiesGump(from, ManagerConfig.Items[ObjectNode.Type] as GameItemType));
                                }
                                else if (ObjectNode != null && ManagerConfig.Creatures.ContainsKey(ObjectNode.Type))
                                {
                                    from.SendGump(new CategorizedAddGump(from, m_Category, m_Page, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                                    from.SendGump(new APropertiesGump(from, ManagerConfig.Creatures[ObjectNode.Type] as CreatureType));
                                }
                            }

                        }
                        else // Add or Next or Set
                        {
                            int index = (m_Page * EntryCount) + (info.ButtonID - 4);
                            if (index >= 0 && index < m_Category.Nodes.Length)
                            {
                                if (m_Method == CategorizedAddGumpEnum.Set && m_Category.Nodes[index] is CAGObject)
                                {
                                    try
                                    {
                                        var node = m_Category.Nodes[index] as CAGObject;
                                        CommandLogging.LogChangeProperty(m_Owner, m_Object, m_Property.Name, node.Type.ToString());
                                        m_Property.SetValue(m_Object, node.Type, null);
                                        m_Owner.SendMessage("Le type a �t� chang�");

                                        APropertiesGump.OnValueChanged(m_Object, m_Property, m_Stack, -1);
                                        from.SendGump(new APropertiesGump(from, m_Object, m_Stack, m_List, m_Index));
                                    }
                                    catch
                                    {
                                        m_Owner.SendMessage("An exception was caught. The property may not have changed.");
                                    }

                                }
                                else if (m_Category.Nodes[index] is CAGAction)
                                {
                                    from.SendGump(new CategorizedAddGump(from, m_Category, m_Page, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                                    m_Category.Nodes[index].OnClick(from, m_Page);
                                }
                                else if (m_Category.Nodes[index] is CAGCategory)
                                {
                                    from.SendGump(new CategorizedAddGump(from, m_Category.Nodes[index] as CAGCategory,
                                        0, m_Method, m_Property, m_Object, m_Stack, m_List, m_Index));
                                }
                                else
                                {
                                    m_Category.Nodes[index].OnClick(from, m_Page);
                                }
                            }
                        }
                        break;
                    }
            }
        }

        internal static Type typeofItem = typeof(Item), typeofMobile = typeof(Mobile);
        internal static void Match(string match, Type[] types, ArrayList results)
        {
            if (match.Length == 0)
                return;

            match = match.ToLower();

            for (int i = 0; i < types.Length; ++i)
            {
                Type t = types[i];

                if ((typeofMobile.IsAssignableFrom(t) || typeofItem.IsAssignableFrom(t)) && t.Name.ToLower().IndexOf(match) >= 0 && !results.Contains(t))
                {
                    ConstructorInfo[] ctors = t.GetConstructors();

                    for (int j = 0; j < ctors.Length; ++j)
                    {
                        if (ctors[j].GetParameters().Length == 0 && ctors[j].IsDefined(typeof(ConstructableAttribute), false))
                        {
                            results.Add(t);
                            break;
                        }
                    }
                }
            }
        }


        public static ArrayList Match(string match)
        {
            ArrayList results = new ArrayList();
            Type[] types;

            Assembly[] asms = ScriptCompiler.Assemblies;

            for (int i = 0; i < asms.Length; ++i)
            {
                types = ScriptCompiler.GetTypeCache(asms[i]).Types;
                Match(match, types, results);
            }

            types = ScriptCompiler.GetTypeCache(Core.Assembly).Types;
            Match(match, types, results);

            results.Sort(new TypeNameComparer());

            return results;
        }

        private class TypeNameComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Type a = x as Type;
                Type b = y as Type;

                return a.Name.CompareTo(b.Name);
            }
        }


    }
}