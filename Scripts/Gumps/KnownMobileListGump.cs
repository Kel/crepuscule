﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Network;
using Server.Mobiles;

namespace Server.Gumps
{
    public abstract class KnownMobileListGump : Gump
    {
        public static void Initialize(){}

        #region Constants
        public static bool OldStyle = PropsConfig.OldStyle;

        public const int GumpOffsetX = PropsConfig.GumpOffsetX;
        public const int GumpOffsetY = PropsConfig.GumpOffsetY;

        public const int TextHue = PropsConfig.TextHue;
        public const int TextOffsetX = PropsConfig.TextOffsetX;

        public const int OffsetGumpID = PropsConfig.OffsetGumpID;
        public const int HeaderGumpID = PropsConfig.HeaderGumpID;
        public const int EntryGumpID = PropsConfig.EntryGumpID;
        public const int BackGumpID = PropsConfig.BackGumpID;
        public const int SetGumpID = PropsConfig.SetGumpID;

        public const int SetWidth = PropsConfig.SetWidth;
        public const int SetOffsetX = PropsConfig.SetOffsetX, SetOffsetY = PropsConfig.SetOffsetY;
        public const int SetButtonID1 = PropsConfig.SetButtonID1;
        public const int SetButtonID2 = PropsConfig.SetButtonID2;

        public const int PrevWidth = PropsConfig.PrevWidth;
        public const int PrevOffsetX = PropsConfig.PrevOffsetX, PrevOffsetY = PropsConfig.PrevOffsetY;
        public const int PrevButtonID1 = PropsConfig.PrevButtonID1;
        public const int PrevButtonID2 = PropsConfig.PrevButtonID2;

        public const int NextWidth = PropsConfig.NextWidth;
        public const int NextOffsetX = PropsConfig.NextOffsetX, NextOffsetY = PropsConfig.NextOffsetY;
        public const int NextButtonID1 = PropsConfig.NextButtonID1;
        public const int NextButtonID2 = PropsConfig.NextButtonID2;

        public const int OffsetSize = PropsConfig.OffsetSize;

        public const int EntryHeight = PropsConfig.EntryHeight;
        public const int BorderSize = PropsConfig.BorderSize;

        private static bool PrevLabel = false, NextLabel = false;

        private const int PrevLabelOffsetX = PrevWidth + 1;
        private const int PrevLabelOffsetY = 0;

        private const int NextLabelOffsetX = -29;
        private const int NextLabelOffsetY = 0;

        private const int EntryWidth = 300;
        private const int EntryCount = 25;

        private const int TotalWidth = OffsetSize + EntryWidth + OffsetSize + SetWidth + OffsetSize;
        private const int TotalHeight = OffsetSize + ((EntryHeight + OffsetSize) * (EntryCount + 1));

        private const int BackWidth = BorderSize + TotalWidth + BorderSize;
        private const int BackHeight = BorderSize + TotalHeight + BorderSize;
        #endregion

        #region Fields
        protected Mobile m_Owner;
        protected PlayerKnownDictionary m_Mobiles;
        protected List<PlayerKnown> m_FirstList;
        protected List<PlayerKnown> m_SecondList;
        protected int m_Page;
        #endregion


        public KnownMobileListGump(Mobile owner, PlayerKnownDictionary list, int page) : base(GumpOffsetX, GumpOffsetY)
        {
            owner.CloseGump(typeof(WhoGump));

            m_Owner = owner;
            m_Mobiles = list;
            m_FirstList = list.GetFirstList();
            m_SecondList = list.GetSecondList();

            Filter(ref m_Mobiles,ref m_FirstList,ref m_SecondList);

            Initialize(page);
        }

        public virtual void Filter(ref PlayerKnownDictionary mobiles, ref List<PlayerKnown> firstList, ref List<PlayerKnown> secondList)
        { 
            
        }

        public void Initialize(int page)
        {
            m_Page = page;

            int count = m_Mobiles.Count - (page * EntryCount);

            if (count < 0)
                count = 0;
            else if (count > EntryCount)
                count = EntryCount;

            int totalHeight = OffsetSize + ((EntryHeight + OffsetSize) * (count + 1));

            AddPage(0);

            AddBackground(0, 0, BackWidth, BorderSize + totalHeight + BorderSize, BackGumpID);
            AddImageTiled(BorderSize, BorderSize, TotalWidth - (OldStyle ? SetWidth + OffsetSize : 0), totalHeight, OffsetGumpID);

            int x = BorderSize + OffsetSize;
            int y = BorderSize + OffsetSize;

            int emptyWidth = TotalWidth - PrevWidth - NextWidth - (OffsetSize * 4) - (OldStyle ? SetWidth + OffsetSize : 0);

            if (!OldStyle)
                AddImageTiled(x - (OldStyle ? OffsetSize : 0), y, emptyWidth + (OldStyle ? OffsetSize * 2 : 0), EntryHeight, EntryGumpID);

            AddLabel(x + TextOffsetX, y, TextHue, String.Format("Page {0} de {1} ({2})", page + 1, (m_Mobiles.Count + EntryCount - 1) / EntryCount, m_Mobiles.Count));

            x += emptyWidth + OffsetSize;

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);

            if (page > 0)
            {
                AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 1, GumpButtonType.Reply, 0);

                if (PrevLabel)
                    AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Prec.");
            }

            x += PrevWidth + OffsetSize;

            if (!OldStyle)
                AddImageTiled(x, y, NextWidth, EntryHeight, HeaderGumpID);

            if ((page + 1) * EntryCount < m_Mobiles.Count)
            {
                AddButton(x + NextOffsetX, y + NextOffsetY, NextButtonID1, NextButtonID2, 2, GumpButtonType.Reply, 1);

                if (NextLabel)
                    AddLabel(x + NextLabelOffsetX, y + NextLabelOffsetY, TextHue, "Suiv.");
            }


            int TotalMobilesCount = m_FirstList.Count + m_SecondList.Count;
            int MobileIndex;

            for (int i = 0, index = page * EntryCount; i < EntryCount && index < TotalMobilesCount; ++i, ++index)
            {
                x = BorderSize + OffsetSize;
                y += EntryHeight + OffsetSize;
                try
                {
                    MobileIndex = index;
                    RacePlayerMobile rpm;
                    string MobileName = "";

                    //Get correct RPM
                    if (index >= m_FirstList.Count)
                    { // Second list
                        MobileIndex -= m_FirstList.Count;
                        rpm = m_SecondList[MobileIndex].Known;
                        MobileName = rpm.GetKnownName2(m_Owner as RacePlayerMobile);
                    }
                    else
                    { // First list
                        rpm = m_FirstList[MobileIndex].Known;
                        MobileName = rpm.GetKnownName1(m_Owner as RacePlayerMobile);
                    }
                        
                    // Mobile m = (Mobile)m_Mobiles[index];


                    if (rpm.AccessLevel == AccessLevel.Player && rpm != null)
                    {
                        int HueCotation = 0;
                        AddImageTiled(x, y, EntryWidth, EntryHeight, EntryGumpID);

                        string texte = SpaceFill(MobileName, 17);
                        AddLabelCropped(x + TextOffsetX, y, EntryWidth - TextOffsetX, EntryHeight, HueCotation, rpm.Deleted ? "(effacé)" : texte);
                    }
                    else
                    {
                        AddImageTiled(x, y, EntryWidth, EntryHeight, EntryGumpID);
                        AddLabelCropped(x + TextOffsetX, y, EntryWidth - TextOffsetX, EntryHeight, GetHueFor(rpm), rpm.Deleted ? "(effacé)" : MobileName);
                    }

                    x += EntryWidth + OffsetSize;

                    if (SetGumpID != 0)
                        AddImageTiled(x, y, SetWidth, EntryHeight, SetGumpID);

                    if (!rpm.Deleted)
                        AddButton(x + SetOffsetX, y + SetOffsetY, SetButtonID1, SetButtonID2, i + 3, GumpButtonType.Reply, 0);
                }
                catch { }
            }
        }

        private static string SpaceFill(string mot, int taille)
        {
            string tempmot;
            if (taille > mot.Length)
            {
                tempmot = mot;
                for (int i = mot.Length; i < taille; i++)
                {
                    tempmot += " ";
                }
                return tempmot;
            }
            return mot;
        }


        private static int GetHueFor(Mobile m)
        {
            switch (m.AccessLevel)
            {
                case AccessLevel.Administrator: return 0x516;
                case AccessLevel.Seer: return 0x144;
                case AccessLevel.GameMaster: return 0x21;
                case AccessLevel.Counselor: return 0x2;
                case AccessLevel.Player:
                default:
                    {
                        if (m.Kills >= 5)
                            return 0x21;
                        else if (m.Criminal)
                            return 0x3B1;

                        return 0x58;
                    }
            }
        }

        protected abstract void OnPageTurn(Mobile from, Mobile owner, PlayerKnownDictionary mobils, int page);
        protected abstract void OnMobileClicked(Mobile from, RacePlayerMobile clicked, IdentityType identity);

        public override void OnResponse(NetState state, RelayInfo info)
        {
            Mobile from = state.Mobile;

            switch (info.ButtonID)
            {
                case 0: // Closed
                    {
                        return;
                    }
                case 1: // Previous
                    {
                        if (m_Page > 0)
                            OnPageTurn(from, from, m_Mobiles, m_Page - 1);

                        break;
                    }
                case 2: // Next
                    {
                        if ((m_Page + 1) * EntryCount < m_Mobiles.Count)
                            OnPageTurn(from, from, m_Mobiles, m_Page + 1);

                        break;
                    }
                default:
                    {
                        int index = (m_Page * EntryCount) + (info.ButtonID - 3);
                        int TotalMobilesCount = m_FirstList.Count + m_SecondList.Count;
                        int MobileIndex = index;
                        IdentityType Identity;

                        if (index >= 0 && index < TotalMobilesCount)
                        {
                            RacePlayerMobile rpm;

                            //Get correct RPM
                            if (index >= m_FirstList.Count)
                            { // Second list
                                MobileIndex -= m_FirstList.Count;
                                rpm = m_SecondList[MobileIndex].Known;
                                Identity = IdentityType.Second;
                            }
                            else
                            { // First list
                                rpm = m_FirstList[MobileIndex].Known;
                                Identity = IdentityType.First;
                            }

                            if (rpm.Deleted)
                            {
                                from.SendMessage("That player has deleted their character.");
                                OnPageTurn(from, from, m_Mobiles, m_Page);
                            }
                            else
                            {
                                OnMobileClicked(from, rpm, Identity);
                                //(from as RacePlayerMobile).DoAction((ActionFinishedCallback)delegate(RacePlayerMobile player)
                                //{
                                //    OnMobileClicked(from, rpm, Identity);
                                //},1,1);
                            }
                     
                        }
                        break;
                    }
            }
        }


    }
}
