using System;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Accounting;
using Server.Guilds;

namespace Server.Misc
{
	public class CharacterCreation
	{
		public static void Initialize()
		{
			// Register our event handler
			EventSink.CharacterCreated += new CharacterCreatedEventHandler( EventSink_CharacterCreated );
		}

		private static void AddBackpack( Mobile m )
		{
			Container pack = m.Backpack;

			if ( pack == null )
			{
				pack = new Backpack();
				pack.Movable = false;

				m.AddItem( pack );
			}
			
			PackItem( new Gold( 500 ) ); // Starting gold can be customized here
			PackItem( new Dagger() );
			PackItem( new HairRestylingDeed() );
			EquipItem( new Shirt() );
			EquipItem( new LongPants() );
			EquipItem( new vetement_a2() );
			EquipItem( new Boots() );
			EquipItem( new Robe() );


		}

		private static void PlaceItemIn( Container parent, int x, int y, Item item )
		{
			parent.AddItem( item );
			item.Location = new Point3D( x, y, 0 );
		}

		private static void AddHair( Mobile m, int itemID, int hue )
		{
			Item item;

			switch ( itemID & 0x3FFF )
			{

				case 0x368C: item =  new TresLongues( hue ); break;	
				case 0x368D: item =  new CalvLong( hue ); break;	
				case 0x368E: item =  new Albator( hue ); break;
				case 0x368F: item =  new Lulu( hue ); break;
				case 0x3690: item =  new Raide( hue ); break;
				case 0x369C: item =  new PlatChev( hue ); break;
				case 0x369D: item =  new PiquesChev( hue ); break;
				case 0x203C: item =  new LongHair( hue ); break;
				case 0x203D: item =  new PonyTail( hue ); break;
				case 0x2044: item =  new Mohawk( hue ); break;
				case 0x2045: item =  new PageboyHair( hue ); break;
				case 0x2046: item =  new BunsHair( hue ); break;
				case 0x2047: item =  new Afro( hue ); break;
				case 0x2048: item =  new ReceedingHair( hue ); break;
				case 0x2049: item =  new TwoPigTails( hue ); break;
				case 0x204A: item =  new KrisnaHair( hue ); break;
				case 0x35B7: item =  new ChevLisse( hue ); break;
				case 0x35B8: item =  new ChevCouette( hue ); break;
				case 0x35B6: item =  new ChevToupet( hue ); break;

				default: return;
			}

			m.AddItem( item );
		}

		private static void AddBeard( Mobile m, int itemID, int hue )
		{
			if ( m.Female )
				return;

			Item item;

			switch ( itemID & 0x3FFF )
			{

				case 0x203E: item = new LongBeard( hue ); break;
				case 0x203F: item = new ShortBeard( hue ); break;
				case 0x2040: item = new Goatee( hue ); break;
				case 0x2041: item = new Mustache( hue ); break;
				case 0x204B: item = new MediumShortBeard( hue ); break;
				case 0x204C: item = new MediumLongBeard( hue ); break;
				case 0x204D: item = new Vandyke( hue ); break;
				default: return;
			}

			m.AddItem( item );
		}



        private static RacePlayerMobile CreateMobile(Account a)
		{
			for ( int i = 0; i < 5; ++i )
				if ( a[i] == null )
                    return (a[i] = new RacePlayerMobile()) as RacePlayerMobile;

			return null;
		}

		private static void EventSink_CharacterCreated( CharacterCreatedEventArgs args )
		{
			var newChar = CreateMobile( args.Account as Account );

			if ( newChar == null )
			{
				Console.WriteLine( "Login: {0}: Character creation failed, account full", args.State );
				return;
			}

			args.Mobile = newChar;
			m_Mobile = newChar;

			newChar.Player = true;
			newChar.AccessLevel = ((Account)args.Account).AccessLevel;
			newChar.Female = args.Female;
			newChar.Body = newChar.Female ? 0x191 : 0x190;
			newChar.Hue = Utility.ClipSkinHue( args.Hue & 0x3FFF ) | 0x8000;
			newChar.Hunger = 20;

			SetName( newChar, args.Name );

            newChar.Profession = args.Profession;
            newChar.PlayerName = newChar.Name;
            newChar.Name = newChar.Female ? newChar.Name = "Inconnue" : newChar.Name = "Inconnu";
            newChar.SetName(newChar.Serial.Value, newChar.PlayerName, newChar);
        

			AddBackpack( newChar );
			AddHair( newChar, args.HairID, Utility.ClipHairHue( args.HairHue & 0x3FFF ) );
			AddBeard( newChar, args.BeardID, Utility.ClipHairHue( args.BeardHue & 0x3FFF ) );


			SetStats( newChar, args.Str, args.Dex, args.Int );
			
			//CityInfo city = args.City;
            //5487 399 25
            CityInfo city = new CityInfo("Cr�ation", "Cr�ation", 5511, 399, 68);

			newChar.MoveToWorld( city.Location, Map.Felucca );

			Console.WriteLine( "Login: {0}: New character being created (account={1})", args.State, ((Account)args.Account).Username );
			Console.WriteLine( " - Character: {0} (serial={1})", newChar.Name, newChar.Serial );
			Console.WriteLine( " - Started: {0} {1}", city.City, city.Location );
		}

		private static void SetStats( Mobile m, int str, int dex, int intel )
		{
            var from = m as RacePlayerMobile;
			int i=0;
			str = 15;
			dex = 15;
			intel = 15;

            from.InitStats(str, dex, intel);
            from.GuildInfo.PlayerGuild = GuildType.None;
			from.EvolutionInfo.DestinyPoints = 1;
            from.EvolutionInfo.LastGrade = 1;
            from.EvolutionInfo.Grade1 = 4;
            from.EvolutionInfo.Grade2 = 4;
            from.EvolutionInfo.Grade3 = 4;
            from.EvolutionInfo.Grade4 = 5;
			from.EvolutionInfo.XPTicks = 60;
            from.EvolutionInfo.Experience = 0;
            from.EvolutionInfo.GradeTime = DateTime.Now;
            from.Timers_Started = true;
            from.FenetreOuverte = false;

            from.BankBox.AddItem(new InterestBag());
            if (from.AccessLevel > AccessLevel.Player)
            {
                GMHidingStone hs = new GMHidingStone();
                from.AddItem(hs);
            }

			for(i=0;i<52;i++)
				from.Skills[i].Cap = 0.0;
			
            for (i = 0; i < 52; i++)
                from.Skills[i].Base = 0;
            
			m.SkillsCap = 14000;
			
		}

		private static void SetName( Mobile m, string name )
		{
			name = name.Trim();

			if ( !NameVerification.Validate( name, 2, 16, true, true, true, 1, NameVerification.SpaceDashPeriodQuote ) )
				name = "Generic Player";

			m.Name = name;
		}

        private static Mobile m_Mobile;

		private static void EquipItem( Item item )
		{
			EquipItem( item, false );
		}

		private static void EquipItem( Item item, bool mustEquip )
		{
			if ( m_Mobile != null && m_Mobile.EquipItem( item ) )
				return;

			Container pack = m_Mobile.Backpack;

			if ( !mustEquip && pack != null )
				pack.DropItem( item );
			else
				item.Delete();
		}

		private static void PackItem( Item item )
		{
			Container pack = m_Mobile.Backpack;

			if ( pack != null )
				pack.DropItem( item );
			else
				item.Delete();
		}

		private static Item NecroHue( Item item )
		{
			item.Hue = 0x2C3;
			return item;
		}

	}
}
