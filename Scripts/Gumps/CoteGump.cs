using System;
using System.Collections;
using Server;
using Server.Misc;
using Server.Gumps;
using Server.Network;
using Server.Mobiles;
using Server.Accounting;
using System.Globalization;


namespace Server.Gumps
{
    public class CoteGump : Gump
    {
        private RacePlayerMobile JoueurACoter;
        public static void Initialize()
        {
        }

        public CoteGump(RacePlayerMobile T_JoueurACoter)
            : base(PropsConfig.GumpOffsetX, PropsConfig.GumpOffsetY)
        {
            JoueurACoter = T_JoueurACoter;

            if (JoueurACoter.Account is Account)
            {
                Closable = true;
                AddPage(0);
                int width = 300;
                int height = 150;

                Account PlayerAccount = (Account)JoueurACoter.Account;

                AddBackground(0, 0, width, height, 9200);

                // Le Nom du perso et account
                //AddImageTiled(10, 10, width - 20, 20, 2624);
                //AddAlphaRegion(10, 10, width - 20, 20);
                AddHtml(10, 10, width - 20, 20, String.Format("<BASEFONT COLOR=#FFFFFF><CENTER>[{0}] {1}</CENTER></BASEFONT>", PlayerAccount.Username, JoueurACoter.PlayerName), false, false);

                //
                //AddImageTiled(10, 40, width - 20, 20, 2624);
                //AddAlphaRegion(10, 40, width - 20, 20);

                // Profil public
                //AddButton(10, 40, 4005, 4007, 8, GumpButtonType.Reply, 0);
                //AddHtml(40, 40, 100, 20, "<BASEFONT COLOR=#FFFFFF>Dossier</BASEFONT>", false, false);

                // Profil priv�
                //if (JoueurACoter.ProfilPrive != null && JoueurACoter.ProfilPrive.Length > 0)
                //{
                //	AddButton(5 + (width - 20)/3, 40, 4005, 4007, 9, GumpButtonType.Reply, 0);
                //	AddHtml(35 + (width - 20)/3, 40, 100, 20, "<BASEFONT COLOR=#FF0000>Profil priv�</BASEFONT>", false, false);
                //}

                //AddButton(((width - 20)/3 * 2), 40, 4005, 4007, 0, GumpButtonType.Reply, 0);
                //AddHtml(30 + ((width - 20)/3*2), 40, 200, 20, "<BASEFONT COLOR=#FFFFFF>Passer Joueur</BASEFONT>", false, false);


                //AddImageTiled(10, 170, width - 20, 20, 2624);
                //AddAlphaRegion(10, 170, width - 20, 20);

                AddHtml(10, 40, width, 20, "<BASEFONT COLOR=#FFFFFF>Cote " + JoueurACoter.EvolutionInfo.Grade1.ToString() + " par " + JoueurACoter.EvolutionInfo.Grade1By + "</BASEFONT>", false, false);
                AddHtml(10, 60, width, 20, "<BASEFONT COLOR=#FFFFFF>Cote " + JoueurACoter.EvolutionInfo.Grade2.ToString() + " par " + JoueurACoter.EvolutionInfo.Grade2By + "</BASEFONT>", false, false);
                AddHtml(10, 80, width, 20, "<BASEFONT COLOR=#FFFFFF>Cote " + JoueurACoter.EvolutionInfo.Grade3.ToString() + " par " + JoueurACoter.EvolutionInfo.Grade3By + "</BASEFONT>", false, false);
                AddHtml(10, 100, width, 20, "<BASEFONT COLOR=#FFFFFF>Cote " + JoueurACoter.EvolutionInfo.Grade4.ToString() + " par " + JoueurACoter.EvolutionInfo.Grade4By + "</BASEFONT>", false, false);


                //AddButton(10, 70, 4005, 4007, 0, GumpButtonType.Reply, 0);
                //AddHtml(40, 70, 20, 20, "<BASEFONT COLOR=#FC0000>0</BASEFONT>", false, false);
                AddButton(((width - 10) / 7), height - 30, 4005, 4007, 1, GumpButtonType.Reply, 0);
                AddHtml(30 + ((width - 10) / 7), height - 30, 20, 20, "<BASEFONT COLOR=#D22A00>1</BASEFONT>", false, false);
                AddButton(((width - 10) / 7 * 2), height - 30, 4005, 4007, 2, GumpButtonType.Reply, 0);
                AddHtml(30 + ((width - 10) / 7 * 2), height - 30, 20, 20, "<BASEFONT COLOR=#A85400>2</BASEFONT>", false, false);
                AddButton(((width - 10) / 7 * 3), height - 30, 4005, 4007, 3, GumpButtonType.Reply, 0);
                AddHtml(30 + ((width - 10) / 7 * 3), height - 30, 20, 20, "<BASEFONT COLOR=#7E7E00>3</BASEFONT>", false, false);
                AddButton(((width - 10) / 7 * 4), height - 30, 4005, 4007, 4, GumpButtonType.Reply, 0);
                AddHtml(30 + ((width - 10) / 7 * 4), height - 30, 20, 20, "<BASEFONT COLOR=#54A800>4</BASEFONT>", false, false);
                AddButton(((width - 10) / 7 * 5), height - 30, 4005, 4007, 5, GumpButtonType.Reply, 0);
                AddHtml(30 + ((width - 10) / 7 * 5), height - 30, 20, 20, "<BASEFONT COLOR=#2AD200>5</BASEFONT>", false, false);
                AddButton(((width - 10) / 7 * 6), height - 30, 4005, 4007, 6, GumpButtonType.Reply, 0);
                AddHtml(30 + ((width - 10) / 7 * 6), height - 30, 20, 20, "<BASEFONT COLOR=#00FC00>6</BASEFONT>", false, false);



            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            if (state.Mobile is PlayerMobile)
            {
                RacePlayerMobile from = (RacePlayerMobile)state.Mobile;
                if (info.ButtonID > 0 && info.ButtonID < 8)
                {
                    int m_Value = info.ButtonID;

                    RacePlayerMobile targ = (RacePlayerMobile)JoueurACoter;

                    TimeSpan tempDate = DateTime.Now.Subtract(((RacePlayerMobile)targ).EvolutionInfo.GradeTime);

                    if (tempDate.Days < 3)
                    {
                        from.SendMessage("Encore trop t�t pour la cotation.");
                    }
                    else
                    {
                        Account fromAccount = (Account)from.Account;
			if(targ.EvolutionInfo.LastGrade < 1 || targ.EvolutionInfo.LastGrade >4)
				targ.EvolutionInfo.LastGrade = 1;

                        switch (targ.EvolutionInfo.LastGrade)
                        {
                            case 1:
                            targ.EvolutionInfo.Grade1 = (short)m_Value;
                            targ.EvolutionInfo.LastGrade = (short)2;
                            targ.EvolutionInfo.Grade1By = "[" + fromAccount.Username + "], " + from.PlayerName;
                            break;
                            case 2:
                            targ.EvolutionInfo.Grade2 = (short)m_Value;
                            targ.EvolutionInfo.Grade2By = "[" + fromAccount.Username + "], " + from.PlayerName;
                            targ.EvolutionInfo.LastGrade = (short)3;
                            break;
                            case 3:
                            targ.EvolutionInfo.Grade3 = (short)m_Value;
                            targ.EvolutionInfo.Grade3By = "[" + fromAccount.Username + "], " + from.PlayerName;
                            targ.EvolutionInfo.LastGrade = (short)4;
                            break;
                            case 4:
                            targ.EvolutionInfo.Grade4 = (short)m_Value;
                            targ.EvolutionInfo.Grade4By = "[" + fromAccount.Username + "], " + from.PlayerName;
                            targ.EvolutionInfo.LastGrade = (short)1;
                            break;
                        }
                        targ.EvolutionInfo.GradeTime = DateTime.Now;
                        if ((short)m_Value == 1 || (short)m_Value == 5 || (short)m_Value == 6)
                            BroadcastMessage(AccessLevel.Counselor, 34, from.Name.ToString() + " a cot� le personnage " + targ.PlayerName + " (" + info.ButtonID.ToString() + ")");
                    }


                }
                else if (info.ButtonID == 8)
                {
                    //from.CloseGump(typeof(DossierGump));
                    //from.SendGump(this);
                    //from.SendGump(new DossierGump(JoueurACoter));
                }
                else if (info.ButtonID == 9)
                {
                    //from.CloseGump(typeof(ProfilGump));
                    //from.SendGump(this);
                    //from.SendGump(new ProfilGump(JoueurACoter, true));
                }
                else from.SendMessage("Cotation Annule�");
            }
        }

        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;

                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }


    }
}
