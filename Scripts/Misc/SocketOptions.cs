/*using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Server;
using Server.Misc;
using Server.Network;

namespace Server
{
	public class SocketOptions
	{
		private const int CoalesceBufferSize = 512; // MSS that the core will use when buffering packets, a value of 0 will turn this buffering off and Nagle on

		private static int[] m_AdditionalPorts = new int[0];
		//private static int[] m_AdditionalPorts = new int[]{ 2594 };

		public static void Initialize()
		{
			NetState.CreatedCallback = new NetStateCreatedCallback( NetState_Created );
			SendQueue.CoalesceBufferSize = CoalesceBufferSize;

		}


		public static void NetState_Created( NetState ns )
		{
			if ( IPLimiter.SocketBlock && !IPLimiter.Verify( ns ) )
			{
				Console.WriteLine( "Login: {0}: Past IP limit threshold", ns );

				using ( StreamWriter op = new StreamWriter( "ipLimits.log", true ) )
					op.WriteLine( "{0}\tPast IP limit threshold\t{1}", ns, DateTime.Now );

				ns.Dispose();
				return;
			}

			Socket s = ns.Socket;

			s.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 15000 );
			s.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 15000 );

			if ( CoalesceBufferSize > 0 )
				s.SetSocketOption( SocketOptionLevel.Tcp, SocketOptionName.NoDelay, 1 ); // RunUO uses its own algorithm
		}
	}
}*/

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Server;
using Server.Misc;
using Server.Network;

namespace Server
{
    public class SocketOptions
    {
        private const bool NagleEnabled = false; // Should the Nagle algorithm be enabled? This may reduce performance
        private const int CoalesceBufferSize = 512; // MSS that the core will use when buffering packets

        private static IPEndPoint[] m_ListenerEndPoints = new IPEndPoint[] {
			new IPEndPoint( IPAddress.Any, 2593 ), // Default: Listen on port 2593 on all IP addresses
			
			// Examples:
			// new IPEndPoint( IPAddress.Any, 80 ), // Listen on port 80 on all IP addresses
			// new IPEndPoint( IPAddress.Parse( "1.2.3.4" ), 2593 ), // Listen on port 2593 on IP address 1.2.3.4
		};

        public static void Initialize()
        {
            SendQueue.CoalesceBufferSize = CoalesceBufferSize;
            EventSink.SocketConnect += new SocketConnectEventHandler(EventSink_SocketConnect);
            Listener.EndPoints = m_ListenerEndPoints;
        }

        private static void EventSink_SocketConnect(SocketConnectEventArgs e)
        {
            if (!e.AllowConnection)
                return;

            if (!NagleEnabled)
                e.Socket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, 1); // RunUO uses its own algorithm
        }
    }
}