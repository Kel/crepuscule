using System;
using Server;

namespace Server.Items
{
	public class Cadeau2 : Item
	{
		

		[Constructable]
		public Cadeau2() : this( 1 ){
                Name = "Cadeau";
		
		{
		}

}		[Constructable]
		public Cadeau2( int amount ) : base( 0x2ced)
		{
			Stackable = false;
			Amount = amount;
		}

		public Cadeau2( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}