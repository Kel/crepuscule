 //////custom container engraving tool/////////////
 ////////scripted by Soldierfortune///////////////
 /////////Edited by Evil Lord Kirby //////////////
using System;
using Server;
using Server.Multis;
using Server.Targeting;
using Server.Items;
using Server.Prompts;

namespace Server.Items
{
	[FlipableAttribute( 0x0FBF, 0x0FC0 )]
	public class ItemRenameTool: CrepusculeItem
	{

		[Constructable]
		public ItemRenameTool() : base( 0x0FBF )
		{
            Name = "Outil de marquage des objets";
			Weight = 1.0;
		}
        public ItemRenameTool( Serial serial ) : base( serial )
		{
		}
		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 
			writer.Write( (int) 0 );
		} 
		
		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 
			int version = reader.ReadInt(); 
		}

        public override void OnDoubleClick( Mobile from )
		{
			if ( from.InRange( this.GetWorldLocation(), 1 ) )
			{
				from.SendMessage( "S�l�ctionnez l'objet que vous voulez r�nommer" );
				from.Target = new InternalTarget( this );
			}
			else
			{
				from.SendLocalizedMessage( 500446 ); // That is too far away.
			}
		}
        private class InternalTarget : Target
		{
			private ItemRenameTool m_ItemRenameTool;
			private Item m_engtarg;

			public InternalTarget( ItemRenameTool engrave ) : base( 1, false, TargetFlags.None )
			{
				m_ItemRenameTool = engrave;
			}
            protected override void OnTarget( Mobile from, object targeted )
			{
				if ( targeted is Item )
				{
					m_engtarg = (Item)targeted;

					if ( !from.InRange( m_ItemRenameTool.GetWorldLocation(), 2 ) || !from.InRange( m_engtarg.GetWorldLocation(), 2 ) )
							from.SendLocalizedMessage( 500446 ); // That is too far away.
					else if ( m_engtarg.Parent == null )
								{
									BaseHouse house = BaseHouse.FindHouseAt( m_engtarg );

									if ( house == null || !house.IsLockedDown( m_engtarg ) )
										from.SendMessage( "Les objets doivent �tre fix�s pour les renommer." );

									else if ( !house.IsCoOwner( from ) )
										from.SendLocalizedMessage( 501023 ); // You must be the owner to use this item.

									else
										{
											from.Prompt = new RenameItemPrompt( m_engtarg );
											from.SendMessage( "En quoi voulez vous renommer cela?" );
										}
								}
				}
				else
				{
					from.SendMessage( "Vous ne pouvez pas le renommer." );
				}

			}
		}
		
	}
}
namespace Server.Prompts
{
	public class RenameItemPrompt : Prompt
	{
		private Item m_engtarg;

		public RenameItemPrompt( Item rcont )
		{
			m_engtarg = rcont;
		}
		public override void OnResponse( Mobile from, string text )
		{
			m_engtarg.Name = text;
            from.SendMessage( "Vous l'avez renomm�." );

		}
	}
}