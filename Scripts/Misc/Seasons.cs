using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;

namespace Server.Custom.Regions
{
	public class SeasonsTimer : Timer
	{
		private short m_Season;

		public static void Initialize()
		{
			new SeasonsTimer();
		}
		public SeasonsTimer() : base(TimeSpan.FromSeconds(10),TimeSpan.FromDays(30) )
		{
			this.Start();
		}
		protected override void OnTick()
		{
			switch(m_Season)
			{
				case 0:
				{
					Map.Felucca.Season = 1;
					Map.Trammel.Season = 1;
					Map.Ilshenar.Season = 1;
					Map.Malas.Season = 1;
					//Console.WriteLine( "Season: Summer" );
					m_Season = 4;

				} break;
				case 2:
				{
                    Map.Felucca.Season = 1;
                    Map.Trammel.Season = 1;
                    Map.Ilshenar.Season = 1;
                    Map.Malas.Season = 1;
					//Console.WriteLine( "Season: Spring" );
					m_Season = 3;

				} break;
				case 3:
				{
					Map.Felucca.Season = 1;
					Map.Trammel.Season = 1;
					Map.Ilshenar.Season = 1;
					Map.Malas.Season = 1;
					//Console.WriteLine( "Season: Summer" );
					m_Season = 4;
				} break;
				case 4:
				{
					Map.Felucca.Season = 1;
					Map.Trammel.Season = 1;
					Map.Ilshenar.Season = 1;
					Map.Malas.Season = 1;
					//Console.WriteLine( "Season: Summer" );
					m_Season = 4;

					foreach( Mobile m in World.Mobiles.Values )
					{
						if ( m is RacePlayerMobile )
						{
							RacePlayerMobile pm = m as RacePlayerMobile;
							pm.EvolutionInfo.Age++;
							//Console.WriteLine("\t{0}", pm.Name);
							
							
						}
					} 

				} break;
				/*case 4:
				{
					Map.Felucca.Season = 2;
					Map.Trammel.Season = 2;
					Map.Ilshenar.Season = 2;
					Map.Malas.Season = 2;
					Console.WriteLine( "Season: Fall" );
					m_Season = 5;
				} break;*/
				case 5:
				{
                    Map.Felucca.Season = 1;
                    Map.Trammel.Season = 1;
                    Map.Ilshenar.Season = 1;
                    Map.Malas.Season = 1;
					//Console.WriteLine( "Season: Winter" );
					m_Season = 2;
				} break;
				default:
				{
					Map.Felucca.Season = 1;
					Map.Trammel.Season = 1;
					Map.Ilshenar.Season = 1;
					Map.Malas.Season = 1;
					//Console.WriteLine( "Season: Summer" );
					m_Season = 4;
				}break;
			}
		}
		public static void SetGlobalWeather( int weather )
		{

            foreach (NetState ns in NetState.Instances)
			{
				Mobile m = ns.Mobile;
				Point3D originalLocation = m.Location;
				m.Location = new Point3D( originalLocation.X, originalLocation.Y, originalLocation.Z + 1 );
				m.Location = originalLocation;
				m.Send( new Weather( weather, weather == 0 ? 30 : 60, 1 ) );
			}
		}

	}
}