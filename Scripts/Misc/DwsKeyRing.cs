// created on 01/06/2004 at 18:40
// by DorkiWan Scriptobi
// for UO Shard Hyel
// KeyRing v1.0

using System;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.ContextMenus;
using Server.Mobiles;
using Server.Prompts;
using Server.Targeting;

namespace Server.Items
{
	public class DwsKeyRing: CrepusculeItem
	{
		private string m_Description;
		private int m_MaxKeyNb;
		private string m_KeyRingName;
 
		protected List<KeyRingEntry> m_Entries;
 
		[CommandProperty ( AccessLevel.GameMaster ) ]
		public string Description
		{
			get{ return m_Description; }
			set{ m_Description = value; InvalidateProperties(); }
		}
 
		[CommandProperty ( AccessLevel.GameMaster ) ]
		public int MaxKeyNb
		{
			get{ return m_MaxKeyNb; }
			set{ m_MaxKeyNb = value; }
		}
 
		[CommandProperty ( AccessLevel.GameMaster ) ]
		public int CurrentKeyNb
		{
			get { return m_Entries.Count; }
		}
 
		[CommandProperty( AccessLevel.GameMaster ) ]
		public string KeyRingName
		{
			get{ return m_KeyRingName; }
			set{ m_KeyRingName = value; InvalidateProperties(); }
		}
 
		[Constructable]
		public DwsKeyRing() : base ( 0x1011 )
		{
			Weight = 0.1;
			LootType = LootType.Blessed;
			m_MaxKeyNb = 20;
			m_Entries = new List<KeyRingEntry>();
		}
 
		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );
 
			string desc;
 
			if ( m_Entries.Count == 0 )
			{
				desc = "Pas de clefs.";
			}
			else if ( m_Entries.Count > 1 )
			{
				desc = m_Entries.Count.ToString()+" clefs";
			}
			else
			{
				desc = "1 clef";
			}
			list.Add( desc );
 
			if ( m_KeyRingName != null && m_KeyRingName.Length > 0 )
				list.Add( m_KeyRingName );
		}
 
		public DwsKeyRing( Serial serial ) : base( serial )
		{
		}
 
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
 
			writer.Write( (int) 1 ); // version
 
			writer.WriteEncodedInt( (int) m_Entries.Count );
 
			for ( int i = 0; i < m_Entries.Count; ++i )
			{
				((KeyRingEntry)m_Entries[i]).Serialize( writer );
			}
 
			writer.Write( m_Description );
			writer.Write( m_MaxKeyNb );
			writer.Write( m_KeyRingName );
		}
 
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
 
			int version = reader.ReadInt();
 
			switch ( version )
			{
				case 1:
				{
					int keycount = reader.ReadEncodedInt();
					m_Entries = new List<KeyRingEntry>();
					for ( int i = 0; i < keycount; ++i )
					{
						m_Entries.Add( new KeyRingEntry( reader ) );
					}
					goto case 0;
				}
				case 0:
				{
					m_Description = reader.ReadString();
					m_MaxKeyNb = reader.ReadInt();
					m_KeyRingName = reader.ReadString();
					break;
				}
			}
		}
 
		public override bool OnDragDrop( Mobile from, Item dropped )
		{
			if ( dropped is Key )
			{
				Key k = (Key)dropped;
                if (k.GetType() == typeof(DestructableKey))
                {
                    from.SendMessage(" Les cl�s magiques ne sont compatibles qu'avec les anneaux de guilde.");
                    return false;
                }
                if (k.GetType() == typeof(GuildKey))
                {
                    from.SendMessage(" Les cl�s de guilde ne sont compatibles qu'avec les anneaux de guilde.");
                    return false;
                }
				if( !IsChildOf( from.Backpack ) )
				{
					from.SendMessage( "Le porte-clef doit �tre dans votre sac � dos." );
					return false;
				}
				else if ( SearchForKey( k.KeyValue ) )
				{
					from.SendMessage( "Cette clef est d�j� sur votre porte-clef." );
					return false;
				}
				else if ( m_Entries.Count < m_MaxKeyNb )
				{
					if ( k.KeyValue != 0 )
					{
						from.SendMessage( "Clef ajout�e au porte-clef." );
						m_Entries.Add( new KeyRingEntry( k.Description, k.KeyValue, k.Link, k.MaxRange, k.ItemID, k.Hue ) );
						InvalidateProperties();
						this.ChangeKeyRingSkin( m_Entries.Count );
						return true;
					}
					from.SendMessage( "Vous ne pouvez ajouter une clef vierge." );
					return false;
				}
				else
				{
					from.SendMessage( "Le porte-clef est plein." );
					return false;
				}
			}
			from.SendMessage( "Ce n'est pas une clef." );
			return false;
		}
 
		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendMessage( "Le porte-clef doit �tre dans votre sac � dos pour �tre utilis�." );
			}
			else
			{
				from.SendMessage( "Target an Item..." );
				from.BeginTarget( 3, false, TargetFlags.None, new TargetCallback( OnTarget ) );
			}
		}
 
		public void OnTarget ( Mobile from, object obj )
		{
			if ( obj is Key )
			{
				Key k = (Key)obj;
				if ( !IsChildOf( from.Backpack ) )
				{
					from.SendMessage( "Le porte-clef doit �tre dans votre sac � dos pour �tre utilis�." );
				}
				else if ( k.KeyValue == 0 )
				{
					from.SendMessage( "Cette clef est vierge." );
				}
				else
				{
					if ( SearchForKey ( k.KeyValue ) )
					{
						from.SendMessage( "Cette clef est d�j� sur votre porte-clef." );
					}
					else if ( m_Entries.Count < m_MaxKeyNb )
					{
						from.SendMessage( "Vous rajoutez cette clef � votre porte-clef." );
						m_Entries.Add( new KeyRingEntry( k.Description, k.KeyValue, k.Link, k.MaxRange, k.ItemID, k.Hue ) );
						InvalidateProperties();
						k.Delete();
     
						this.ChangeKeyRingSkin( m_Entries.Count );
					}
					else
					{
						from.SendMessage( "Votre porte-clef est plein." );
					}
				}
			}
			else if ( obj is ILockable )
			{
				ILockable ilock = (ILockable)obj;
				if ( m_Entries.Count == 0 )
				{
					from.SendMessage( "Votre porte-clef est vide !" );
				}
				else if ( ilock is BaseDoor && !((BaseDoor)ilock).UseLocks() )
				{
					from.SendMessage( "Cette porte n'a pas de serrures." );
				}
				else if ( ilock.KeyValue == 0 )
				{
					from.SendMessage( "Il n'y a pas de serrures ici." );
				}
				else
				{
					if ( SearchForKey ( ilock.KeyValue ) )
					{
						from.SendMessage( "Vous utilisez une clef de votre porte-clef." );
						if ( ilock.Locked )
						{
							from.SendMessage( "Vous d�v�rouillez la serrure." );
						}
						else
						{
							from.SendMessage( "Vous v�rouillez la serrure." );
						}
						ilock.Locked = !ilock.Locked;
					}
					else
					{
						from.SendMessage( "Aucune clef de votre porte-clef n'ouvre cette serrure." );
					}
				}
			}
			else if ( obj == this )
			{
				if ( m_Entries.Count == 0 )
				{
					from.SendMessage( "Votre porte-clef est d�j� vide !" );
				}
				else
				{
                    KeyRingEntry kre;

                    for (int i = 0; i < m_Entries.Count; ++i)
                    {

                        #region Copy key props

                        kre = m_Entries[i];
                        Key k = new Key();
                        k.ItemID = kre.KeyType;
                        k.KeyValue = kre.KeyVal;
                        k.Link = kre.KeyLink;
                        k.Description = kre.KeyDesc;
                        k.MaxRange = kre.KeyMaxRange;
                        k.Hue = kre.KeyHue;
                        k.ShouldAssignProperties = false;

                        #endregion

                        from.AddToBackpack(k);
                    }

                    m_Entries.Clear();

					this.ChangeKeyRingSkin( m_Entries.Count );
				}
			}
			else
			{
				from.SendMessage( "Cet objet ne poss�de pas de serrure  !" );
			}
		}
 
		public bool SearchForKey(uint keyval)
		{
			for ( int i = 0; i < m_Entries.Count; ++i )
			{
				KeyRingEntry k = (KeyRingEntry)m_Entries[i];
				if ( k.KeyVal == keyval )
				{
					return true;
				}
			}
			return false;
		}
 
		public void ChangeKeyRingSkin ( int currentkeynb )
		{
			int maxk = this.MaxKeyNb;
			if ( currentkeynb == 0 )
			{
				this.ItemID = 0x1011;
			}
			else if ( currentkeynb < ( maxk / 2 ) )
			{
				this.ItemID = 0x1769;
			}
			else if ( currentkeynb < maxk )
			{
				this.ItemID = 0x176a;
			}
			else
			{
				this.ItemID = 0x176b;
			}
		}
 
		public override void GetContextMenuEntries( Mobile from, List<ContextMenuEntry> list )
		{
			base.GetContextMenuEntries( from, list );
			if ( from.CheckAlive() && IsChildOf( from.Backpack ) )
				list.Add( new KeyRingNameEntry( from, this ) );
		}
 
		private class KeyRingNameEntry : ContextMenuEntry
		{
			private Mobile m_From;
			private DwsKeyRing m_KeyRing;
 
			public KeyRingNameEntry( Mobile from, DwsKeyRing keyring ) : base ( 6216 )
			{
				m_From = from;
				m_KeyRing = keyring;
			}
 
			public override void OnClick()
			{
				if ( m_From.CheckAlive() && m_KeyRing.IsChildOf( m_From.Backpack ) )
				{
					m_From.SendMessage( "Entrez un nouveau nom." );
					m_From.Prompt = new KeyRingNamePrompt( m_KeyRing );
				}
			}
		}
 
		private class KeyRingNamePrompt : Prompt
		{
			private DwsKeyRing m_KeyRing;
 
			public KeyRingNamePrompt( DwsKeyRing keyring )
			{
				m_KeyRing = keyring;
			}
 
			public override void OnResponse( Mobile from, string text )
			{
				if ( text.Length > 40 )
					text = text.Substring( 0, 40 );
				if ( from.CheckAlive() && m_KeyRing.IsChildOf( from.Backpack ) )
				{
					m_KeyRing.KeyRingName = text.Trim();
					from.SendMessage( "Le nom du porte-clef a �t� chang�." );
				}
			}
 
			public override void OnCancel( Mobile from )
			{
			}
		}
	}

	public class KeyRingEntry
	{
		private string m_KeyDesc;
		private uint m_KeyVal;
		private Item m_KeyLink;
		private int m_KeyMaxRange;
 
		private int m_KeyType;
		private int m_KeyHue;
 
		public string KeyDesc{ get{ return m_KeyDesc; } }
		public uint KeyVal{ get{ return m_KeyVal; } }
		public Item KeyLink{ get{ return m_KeyLink; } }
		public int KeyMaxRange{ get{ return m_KeyMaxRange; } }
 
		public int KeyType{ get{ return m_KeyType; } }
		public int KeyHue{ get{ return m_KeyHue;} }
 
		public KeyRingEntry( string desc, uint keyval, Item link, int maxrange, int type, int hue)
		{
			m_KeyDesc = desc;
			m_KeyVal = keyval;
			m_KeyLink = link;
			m_KeyMaxRange = maxrange;
 
			m_KeyType = type;
			m_KeyHue = hue;
		}
 
		public void Serialize( GenericWriter writer )
		{
			writer.WriteEncodedInt( 1 ); // version
 
			writer.Write( (int) m_KeyType );
			writer.Write( (int) m_KeyHue );
 
			writer.Write( (string) m_KeyDesc );
			writer.Write( (uint) m_KeyVal );
			writer.Write( (Item) m_KeyLink );
			writer.Write( (int) m_KeyMaxRange );
		}
 
		public KeyRingEntry( GenericReader reader )
		{
			int version = reader.ReadEncodedInt();
 
			switch ( version )
			{
				case 1:
				{
					m_KeyType = reader.ReadInt();
					m_KeyHue = reader.ReadInt();
					goto case 0;
				}
				case 0:
				{
					m_KeyDesc = reader.ReadString();
					m_KeyVal = reader.ReadUInt();
					m_KeyLink = reader.ReadItem();
					m_KeyMaxRange = reader.ReadInt();
					break;
				}
			}
		}
	}
}