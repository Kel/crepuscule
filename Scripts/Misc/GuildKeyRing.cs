﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Server.Items
{
    public class GuildKeyRing : DwsKeyRing
    {
        [Constructable]
        public GuildKeyRing(): base()
		{
			Weight = 0.1;
			MaxKeyNb = 20;
			m_Entries = new List<KeyRingEntry>();
            Hue = 632;
            LootType = LootType.Regular;
		}

        public GuildKeyRing(Serial serial)
            : base(serial)
		{
		}
        
        public override bool OnDragDrop(Mobile from, Item dropped)
        {
            if (dropped is Key)
            {
                Key k = (Key)dropped;
                if (k.GetType() != typeof(GuildKey))
                {
                    from.SendMessage("Ce porte-clef ne peut accepter que les clés de guilde.");
                    return false;
                }
                if (!IsChildOf(from.Backpack))
                {
                    from.SendMessage("Le porte-clef doit être dans votre sac à dos.");
                    return false;
                }
                else if (SearchForKey(k.KeyValue))
                {
                    from.SendMessage("Cette clef est déjà sur votre porte-clef.");
                    return false;
                }
                else if (m_Entries.Count < MaxKeyNb)
                {
                    if (k.KeyValue != 0)
                    {
                        from.SendMessage("Clef ajoutée au porte-clef.");
                        m_Entries.Add(new KeyRingEntry(k.Description, k.KeyValue, k.Link, k.MaxRange, k.ItemID, k.Hue));
                        InvalidateProperties();
                        this.ChangeKeyRingSkin(m_Entries.Count);
                        return true;
                    }
                    from.SendMessage("Vous ne pouvez ajouter une clef vierge.");
                    return false;
                }
                else
                {
                    from.SendMessage("Le porte-clef est plein.");
                    return false;
                }
            }
            from.SendMessage("Ce n'est pas une clef.");
            return false;
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)1); // version
            writer.WriteEncodedInt((int)m_Entries.Count);

            for (int i = 0; i < m_Entries.Count; ++i)
            {
                ((KeyRingEntry)m_Entries[i]).Serialize(writer);
            }

            writer.Write(Description);
            writer.Write(MaxKeyNb);
            writer.Write(KeyRingName);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            switch (version)
            {
                case 1:
                    {
                        int keycount = reader.ReadEncodedInt();
                        m_Entries = new List<KeyRingEntry>();
                        for (int i = 0; i < keycount; ++i)
                        {
                            m_Entries.Add(new KeyRingEntry(reader));
                        }
                        goto case 0;
                    }
                case 0:
                    {
                        Description = reader.ReadString();
                        MaxKeyNb = reader.ReadInt();
                        KeyRingName = reader.ReadString();
                        break;
                    }
            }
        }
    }
}
