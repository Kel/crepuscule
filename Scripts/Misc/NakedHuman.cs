﻿using System;
using System.Collections;
using Server.Items;
using Server.ContextMenus;
using Server.Misc;
using Server.Network;

namespace Server.Mobiles 
{
    public class NakedHumanNoAI : BaseCreature 
    {
        public override bool ClickTitle { get { return false; } }

        public NakedHumanNoAI(AIType ai)
            : base(ai, FightMode.Closest, 10, 1, 0.2, 0.4)
        {
            SpeechHue = Utility.RandomDyedHue();
            Hue = Utility.RandomSkinHue();


            SetStr(86, 100);    
            SetDex(81, 95);
            SetInt(61, 75);

            Fame = 1500;
            Karma = -1500;

            SetDamage(10, 23);

            SetSkill(SkillName.Fencing, 66.0, 97.5);
            SetSkill(SkillName.Macing, 65.0, 87.5);
            SetSkill(SkillName.MagicResist, 25.0, 47.5);
            SetSkill(SkillName.Swords, 65.0, 87.5);
            SetSkill(SkillName.Tactics, 65.0, 87.5);
            SetSkill(SkillName.Wrestling, 15.0, 37.5);
        }

        public NakedHumanNoAI(Serial serial)
            : base(serial)
        {
        }

        public override void GenerateLoot()
        {
            
        }

        public override bool AlwaysMurderer { get { return true; } }



        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class NakedHumanNoAIWithHair : BaseCreature
    {
        public override bool ClickTitle { get { return false; } }

        public NakedHumanNoAIWithHair(AIType ai)
            : base(ai, FightMode.Closest, 10, 1, 0.2, 0.4)
        {

            Item hair = new Item(Utility.RandomList(0x203B, 0x2049, 0x2048, 0x204A));
            hair.Hue = Utility.RandomNondyedHue();
            hair.Layer = Layer.Hair;
            hair.Movable = false;
            AddItem(hair);
        }

        public override void GenerateLoot()
        {

        }

        public override bool AlwaysMurderer { get { return true; } }

        public NakedHumanNoAIWithHair(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class NakedHumanWarrior : NakedHumanNoAI
    {
        [Constructable]
        public NakedHumanWarrior(): base(AIType.AI_Melee ){}
        public NakedHumanWarrior(Serial serial): base(serial){}

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    public class NakedHumanWarriorWithHair : NakedHumanNoAI
    {
        [Constructable]
        public NakedHumanWarriorWithHair() : base(AIType.AI_Melee) { }
        public NakedHumanWarriorWithHair(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    public class NakedHumanMage : NakedHumanNoAI
    {
        [Constructable]
        public NakedHumanMage() : base(AIType.AI_Melee) { }
        public NakedHumanMage(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    public class NakedHumanMageWithHair : NakedHumanNoAI
    {
        [Constructable]
        public NakedHumanMageWithHair() : base(AIType.AI_Mage) { }
        public NakedHumanMageWithHair(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    public class NakedHumanArcher : NakedHumanNoAI
    {
        [Constructable]
        public NakedHumanArcher() : base(AIType.AI_Archer) { }
        public NakedHumanArcher(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    public class NakedHumanArcherWithHair : NakedHumanNoAI
    {
        [Constructable]
        public NakedHumanArcherWithHair() : base(AIType.AI_Archer) { }
        public NakedHumanArcherWithHair(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}