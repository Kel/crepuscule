using System;
using Server;
using Server.Mobiles;

namespace Server.Misc
{
    public class SkillCheck
    {
        private const bool AntiMacroCode = false; //Change this to false to disable anti-macro code

        public static TimeSpan AntiMacroExpire = TimeSpan.FromMinutes(5.0); //How long do we remember targets/locations?
        public const int Allowance = 3; //How many times may we use the same location/target for gain
        private const int LocationSize = 5; //The size of eeach location, make this smaller so players dont have to move as far
        private static bool[] UseAntiMacro = new bool[]
{
// true if this skill uses the anti-macro code, false if it does not
false,// Alchemy = 0,
false,// Anatomy = 1,
false,// AnimalLore = 2,
false,// ItemID = 3,
false,// ArmsLore = 4,
false,// Parry = 5,
false,// Begging = 6,
false,// Blacksmith = 7,
false,// Fletching = 8,
false,// Peacemaking = 9,
false,// Camping = 10,
false,// Carpentry = 11,
false,// Cartography = 12,
false,// Cooking = 13,
false,// DetectHidden = 14,
false,// Discordance = 15,
false,// EvalInt = 16,
false,// Healing = 17,
false,// Fishing = 18,
false,// Forensics = 19,
false,// Herding = 20,
false,// Hiding = 21,
false,// Provocation = 22,
false,// Inscribe = 23,
false,// Lockpicking = 24,
false,// Magery = 25,
false,// MagicResist = 26,
false,// Tactics = 27,
false,// Snooping = 28,
false,// Musicianship = 29,
false,// Poisoning = 30,
false,// Archery = 31,
false,// SpiritSpeak = 32,
false,// Stealing = 33,
false,// Tailoring = 34,
false,// AnimalTaming = 35,
false,// TasteID = 36,
false,// Tinkering = 37,
false,// Tracking = 38,
false,// Veterinary = 39,
false,// Swords = 40,
false,// Macing = 41,
false,// Fencing = 42,
false,// Wrestling = 43,
false,// Lumberjacking = 44,
false,// Mining = 45,
false,// Meditation = 46,
false,// Stealth = 47,
false,// RemoveTrap = 48,
false,// Necromancy = 49,
false,// Focus = 50,
false,// Chivalry = 51
};

        public static void Initialize()
        {
            Mobile.SkillCheckLocationHandler = new SkillCheckLocationHandler(Mobile_SkillCheckLocation);
            Mobile.SkillCheckDirectLocationHandler = new SkillCheckDirectLocationHandler(Mobile_SkillCheckDirectLocation);

            Mobile.SkillCheckTargetHandler = new SkillCheckTargetHandler(Mobile_SkillCheckTarget);
            Mobile.SkillCheckDirectTargetHandler = new SkillCheckDirectTargetHandler(Mobile_SkillCheckDirectTarget);

            //*********EDIT THESE LINES TO SET THE BASE PERCENT CHANCE TO GAIN FOR EACH INDIVIDUAL SKILL***********************//
            //This Table used to set the base chance to gain skill between 40 and 70
            //for instance .15 would be a 15% chance to gain in the particular skill
            SkillInfo.Table[0].GainFactor = 1.75;// Alchemy = 0, 
            SkillInfo.Table[1].GainFactor = 1.75;// Anatomy = 1, 
            SkillInfo.Table[2].GainFactor = 1.75;// AnimalLore = 2, 
            SkillInfo.Table[3].GainFactor = 1.75;// ItemID = 3, 
            SkillInfo.Table[4].GainFactor = 1.75;// ArmsLore = 4, 
            SkillInfo.Table[5].GainFactor = 1.75;// Parry = 5, 
            SkillInfo.Table[6].GainFactor = 1.75;// Begging = 6, 
            SkillInfo.Table[7].GainFactor = 1.75;// Blacksmith = 7, 
            SkillInfo.Table[8].GainFactor = 1.75;// Fletching = 8, 
            SkillInfo.Table[9].GainFactor = 1.75;// Peacemaking = 9, 
            SkillInfo.Table[10].GainFactor = 1.75;// Camping = 10, 
            SkillInfo.Table[11].GainFactor = 1.75;// Carpentry = 11, 
            SkillInfo.Table[12].GainFactor = 1.75;// Cartography = 12, 
            SkillInfo.Table[13].GainFactor = 1.75;// Cooking = 13, 
            SkillInfo.Table[14].GainFactor = 1.75;// DetectHidden = 14, 
            SkillInfo.Table[15].GainFactor = 1.75;// Discordance = 15, 
            SkillInfo.Table[16].GainFactor = 1.75;// EvalInt = 16, 
            SkillInfo.Table[17].GainFactor = 1.75;// Healing = 17, 
            SkillInfo.Table[18].GainFactor = 1.75;// Fishing = 18, 
            SkillInfo.Table[19].GainFactor = 1.75;// Forensics = 19, 
            SkillInfo.Table[20].GainFactor = 1.75;// Herding = 20, 
            SkillInfo.Table[21].GainFactor = 1.75;// Hiding = 21, 
            SkillInfo.Table[22].GainFactor = 1.75;// Provocation = 22, 
            SkillInfo.Table[23].GainFactor = 1.75;// Inscribe = 23, 
            SkillInfo.Table[24].GainFactor = 1.75;// Lockpicking = 24, 
            SkillInfo.Table[25].GainFactor = 1.75;// Magery = 25, 
            SkillInfo.Table[26].GainFactor = 1.75;// MagicResist = 26, 
            SkillInfo.Table[27].GainFactor = 1.75;// Tactics = 27, 
            SkillInfo.Table[28].GainFactor = 1.75;// Snooping = 28, 
            SkillInfo.Table[29].GainFactor = 1.75;// Musicianship = 29, 
            SkillInfo.Table[30].GainFactor = 1.75;// Poisoning = 30 
            SkillInfo.Table[31].GainFactor = 1.75;// Archery = 31 
            SkillInfo.Table[32].GainFactor = 1.75;// SpiritSpeak = 32 
            SkillInfo.Table[33].GainFactor = 1.75;// Stealing = 33 
            SkillInfo.Table[34].GainFactor = 1.75;// Tailoring = 34 
            SkillInfo.Table[35].GainFactor = 1.75;// AnimalTaming = 35 
            SkillInfo.Table[36].GainFactor = 1.75;// TasteID = 36 
            SkillInfo.Table[37].GainFactor = 1.75;// Tinkering = 37 
            SkillInfo.Table[38].GainFactor = 1.75;// Tracking = 38 
            SkillInfo.Table[39].GainFactor = 1.75;// Veterinary = 39 
            SkillInfo.Table[40].GainFactor = 1.75;// Swords = 40 
            SkillInfo.Table[41].GainFactor = 1.75;// Macing = 41 
            SkillInfo.Table[42].GainFactor = 1.75;// Fencing = 42 
            SkillInfo.Table[43].GainFactor = 1.75;// Wrestling = 43 
            SkillInfo.Table[44].GainFactor = 1.75;// Lumberjacking = 44 
            SkillInfo.Table[45].GainFactor = 1.75;// Mining = 45 
            SkillInfo.Table[46].GainFactor = 1.75;// Meditation = 46 
            SkillInfo.Table[47].GainFactor = 1.75;// Stealth = 47 
            SkillInfo.Table[48].GainFactor = 1.75;// RemoveTrap = 48 
            SkillInfo.Table[49].GainFactor = 1.75;// Necromancy = 49 
            SkillInfo.Table[50].GainFactor = 1.75;// Focus = 50 
            SkillInfo.Table[51].GainFactor = 1.75;// Chivalry = 51
            /*for (int i = 0 ;i <51 ; i++)
            {
                SkillInfo.Table[i].GainFactor = 0;
            }*/
        }

        public static bool Mobile_SkillCheckLocation(Mobile from, SkillName skillName, double minSkill, double maxSkill)
        {
            Skill skill = from.Skills[skillName];

            if (skill == null)
                return false;

            double value = skill.Value;

            if (value < minSkill)
                return false; // Too difficult
            else if (value >= maxSkill)
                return true; // No challenge

            double chance = (value - minSkill) / (maxSkill - minSkill);

            Point2D loc = new Point2D(from.Location.X / LocationSize, from.Location.Y / LocationSize);
            return CheckSkill(from, skill, loc, chance);
        }

        public static bool Mobile_SkillCheckDirectLocation(Mobile from, SkillName skillName, double chance)
        {
            Skill skill = from.Skills[skillName];

            if (skill == null)
                return false;

            if (chance < 0.0)
                return false; // Too difficult
            else if (chance >= 1.0)
                return true; // No challenge

            Point2D loc = new Point2D(from.Location.X / LocationSize, from.Location.Y / LocationSize);
            return CheckSkill(from, skill, loc, chance);
        }

        public static bool CheckSkill(Mobile from, Skill skill, object amObj, double chance)
        {
            if (from.Skills.Cap == 0)
                return false;

            bool success = (chance >= Utility.RandomDouble());

            //**********EDIT THESE LINES FOR MODIFICATIONS TO BASE PERCENTAGE AT DIFFERENT SKILL LEVELS************************//
            double LowSkillMod = .35; // percent added to skill gain for skill between 0 - 40
            double BaseSkillGain = skill.Info.GainFactor; // base percent to gain at skill between 40 - 70 set in table above.
            double HighSkillMod = .5; // percent subtracted from base gain for skill between 70 - 100
            double AboveGmSkillMod = .35; // percent subtracted from base gain for skill over 100
            //Example: Player Skill is 85.3 - We would take the 15% base chance and subtract 8% - Player now has a 7% chance to gain.
            //************************************************** ************************************************** *************//

            double ComputedSkillMod = 100; // holds the percent chance to gain after checking players skill

            if (skill.Base < 40.1)
                ComputedSkillMod = BaseSkillGain + LowSkillMod;
            else if (skill.Base < 70.1)
                ComputedSkillMod = BaseSkillGain;
            else if (skill.Base < 100.1)
                ComputedSkillMod = BaseSkillGain - HighSkillMod;
            else if (skill.Base > 100.0)
                ComputedSkillMod = BaseSkillGain - AboveGmSkillMod;

            if (ComputedSkillMod < 0.01)
                ComputedSkillMod = 0.01;

            //following line used to see chance to gain ingame
            //from.SendMessage( "Your chance to gain {0} is {1}",skill.Name, ComputedSkillMod );

            if (from is BaseCreature && ((BaseCreature)from).Controled)
                ComputedSkillMod *= 2;

            if (from.Alive && ((ComputedSkillMod >= Utility.RandomDouble() && AllowGain(from, skill, amObj)) || skill.Base < 10.0))
                Gain(from, skill);

            return success;
        }

        public static bool Mobile_SkillCheckTarget(Mobile from, SkillName skillName, object target, double minSkill, double maxSkill)
        {
            Skill skill = from.Skills[skillName];

            if (skill == null)
                return false;

            double value = skill.Value;

            if (value < minSkill)
                return false; // Too difficult
            else if (value >= maxSkill)
                return true; // No challenge

            double chance = (value - minSkill) / (maxSkill - minSkill);

            return CheckSkill(from, skill, target, chance);
        }

        public static bool Mobile_SkillCheckDirectTarget(Mobile from, SkillName skillName, object target, double chance)
        {
            Skill skill = from.Skills[skillName];

            if (skill == null)
                return false;

            if (chance < 0.0)
                return false; // Too difficult
            else if (chance >= 1.0)
                return true; // No challenge

            return CheckSkill(from, skill, target, chance);
        }

        private static bool AllowGain(Mobile from, Skill skill, object obj)
        {
            return true;

            /*if (from is PlayerMobile && AntiMacroCode && UseAntiMacro[skill.Info.SkillID])
            {
                return ((PlayerMobile)from).AntiMacroCheck(skill, obj);
            }
            else
            {
                return true;
            }*/
        }

        public enum Stat { Str, Dex, Int }

        public static void Gain(Mobile from, Skill skill)
        {
            if (from.Region is Regions.Jail)
                return;

            if (from is BaseCreature && ((BaseCreature)from).IsDeadPet)
                return;

            if (skill.SkillName == SkillName.Focus && from is BaseCreature)
                return;

            if (skill.Base < skill.Cap && skill.Lock == SkillLock.Up)
            {
                int toGain = 1;

                if (skill.Base <= 10.0)
                    toGain = Utility.Random(4) + 1;

                Skills skills = from.Skills;

                if ((skills.Total / skills.Cap) >= Utility.RandomDouble())//( skills.Total >= skills.Cap )
                {
                    for (int i = 0; i < skills.Length; ++i)
                    {
                        Skill toLower = skills[i];

                        if (toLower != skill && toLower.Lock == SkillLock.Down && toLower.BaseFixedPoint >= toGain)
                        {
                            toLower.BaseFixedPoint -= toGain;
                            break;
                        }
                    }
                }

                if ((skills.Total + toGain) <= skills.Cap)
                {
                    skill.BaseFixedPoint += toGain;
                }
            }

            if (skill.Lock == SkillLock.Up)
            {
                SkillInfo info = skill.Info;

                //*****EDIT THIS LINE TO MAKE STATS GAIN FASTER, NUMBER MUST BE BETWEEN 0.00 and 1.00, THIS IS ADDED TO THE CHANCE TO GAIN IN A STAT********//
                double StatGainBonus = .05; //Extra chance to gain in stats. Left at 0 would be default runuo gains.
                //************************************************** ************************************************** **************************************//

                if (from.StrLock == StatLockType.Up && ((info.StrGain / 33.3) + StatGainBonus) > Utility.RandomDouble())
                {
                    if (info.StrGain != 0)
                        GainStat(from, Stat.Str);
                }
                else if (from.DexLock == StatLockType.Up && ((info.DexGain / 33.3) + StatGainBonus) > Utility.RandomDouble())
                {
                    if (info.DexGain != 0)
                        GainStat(from, Stat.Dex);
                }
                else if (from.IntLock == StatLockType.Up && ((info.IntGain / 33.3) + StatGainBonus) > Utility.RandomDouble())
                {
                    if (info.IntGain != 0)
                        GainStat(from, Stat.Int);
                }
                //following line used to show chance to gain stats ingame
                //from.SendMessage( "Str: {0} Dex: {1} Int: {2}",((info.StrGain / 33.3) + StatGainBonus),((info.DexGain / 33.3) + StatGainBonus),((info.IntGain / 33.3) + StatGainBonus) );
            }
        }

        public static bool CanLower(Mobile from, Stat stat)
        {
            switch (stat)
            {
                case Stat.Str: return (from.StrLock == StatLockType.Down && from.RawStr > 10);
                case Stat.Dex: return (from.DexLock == StatLockType.Down && from.RawDex > 10);
                case Stat.Int: return (from.IntLock == StatLockType.Down && from.RawInt > 10);
            }

            return false;
        }

        public static bool CanRaise(Mobile from, Stat stat)
        {
            if (!(from is BaseCreature && ((BaseCreature)from).Controled))
            {
                if (from.RawStatTotal >= from.StatCap)
                    return false;
            }

            //TEST
            
            if (from.Player && (from is RacePlayerMobile))
            {
                RacePlayerMobile m = from as RacePlayerMobile; 

                switch (stat)
                {
                    case Stat.Str: return (m.StrLock == StatLockType.Up && m.RawStr < m.m_MaxStr);
                    case Stat.Dex: return (m.DexLock == StatLockType.Up && m.RawDex < m.m_MaxDex);
                    case Stat.Int: return (m.IntLock == StatLockType.Up && m.RawInt < m.m_MaxInt);
                }
            }
            else
            {
                switch (stat)
                {
                    case Stat.Str: return (from.StrLock == StatLockType.Up && from.RawStr < 305);
                    case Stat.Dex: return (from.DexLock == StatLockType.Up && from.RawDex < 305);
                    case Stat.Int: return (from.IntLock == StatLockType.Up && from.RawInt < 305);
                }
            }
          
            return false;
        }

        public static void IncreaseStat(Mobile from, Stat stat, bool atrophy)
        {
            atrophy = atrophy || (from.RawStatTotal >= from.StatCap);

            switch (stat)
            {
                case Stat.Str:
                    {
                        if (atrophy)
                        {
                            if (CanLower(from, Stat.Dex) && (from.RawDex < from.RawInt || !CanLower(from, Stat.Int)))
                                --from.RawDex;
                            else if (CanLower(from, Stat.Int))
                                --from.RawInt;
                        }

                        if (CanRaise(from, Stat.Str))
                            ++from.RawStr;

                        break;
                    }
                case Stat.Dex:
                    {
                        if (atrophy)
                        {
                            if (CanLower(from, Stat.Str) && (from.RawStr < from.RawInt || !CanLower(from, Stat.Int)))
                                --from.RawStr;
                            else if (CanLower(from, Stat.Int))
                                --from.RawInt;
                        }

                        if (CanRaise(from, Stat.Dex))
                            ++from.RawDex;

                        break;
                    }
                case Stat.Int:
                    {
                        if (atrophy)
                        {
                            if (CanLower(from, Stat.Str) && (from.RawStr < from.RawDex || !CanLower(from, Stat.Dex)))
                                --from.RawStr;
                            else if (CanLower(from, Stat.Dex))
                                --from.RawDex;
                        }

                        if (CanRaise(from, Stat.Int))
                            ++from.RawInt;

                        break;
                    }
            }
        }

        private static TimeSpan m_StatGainDelay = TimeSpan.FromMinutes(0.0);

        public static void GainStat(Mobile from, Stat stat)
        {
            if ((from.LastStatGain + m_StatGainDelay) >= DateTime.Now)
                return;

            from.LastStatGain = DateTime.Now;

            bool atrophy = ((from.RawStatTotal / (double)from.StatCap) >= Utility.RandomDouble());

            IncreaseStat(from, stat, atrophy);
        }
    }
}