using System;
using System.IO;
using System.Diagnostics;
using Server;
using Server.Engines;

namespace Server.Misc
{
	public class AutoRestart : Timer
	{
		private static bool Enabled = false; // is the script enabled?

		private static TimeSpan RestartTime = TimeSpan.FromHours( 2.0 ); // time of day at which to restart
		private static TimeSpan RestartDelay = TimeSpan.Zero; // how long the server should remain active before restart (period of 'server wars')

		private static TimeSpan WarningDelay = TimeSpan.FromMinutes( 1.0 ); // at what interval should the shutdown message be displayed?

		private static bool m_Restarting;
		private static DateTime m_RestartTime;

		public static bool Restarting
		{
			get{ return m_Restarting; }
		}

		public static void Initialize()
		{
			Commands.Register( "Restart", AccessLevel.Administrator, new CommandEventHandler( Restart_OnCommand ) );
			new AutoRestart().Start();
		}

		public static void Restart_OnCommand( CommandEventArgs e )
		{
            DoAutoRestart(e.Mobile);
		}

        public static void DoAutoRestart(Mobile mobile)
        {
            if (m_Restarting)
            {
                mobile.SendMessage("Le serveur est d�j� en train de red�marrer.");
            }
            else
            {
                mobile.SendMessage("Vous avez initialis� le red�marrage du serveur.");
                Enabled = true;
                m_RestartTime = DateTime.Now;
            }
        }

		public AutoRestart() : base( TimeSpan.FromSeconds( 1.0 ), TimeSpan.FromSeconds( 1.0 ) )
		{
			Priority = TimerPriority.FiveSeconds;

			m_RestartTime = DateTime.Now.Date + RestartTime;

			if ( m_RestartTime < DateTime.Now )
				m_RestartTime += TimeSpan.FromDays( 1.0 );
		}

		private void Warning_Callback()
		{
			World.Broadcast( 0x22, false, "Le serveur est en train de red�marrer..." );
		}

		private void Restart_Callback()
		{
			Process.Start( Core.ExePath );
			Core.Process.Kill();
		}

		protected override void OnTick()
		{
			if ( m_Restarting || !Enabled )
				return;

			if ( DateTime.Now < m_RestartTime )
				return;

			if ( WarningDelay > TimeSpan.Zero )
			{
				Warning_Callback();
				Timer.DelayCall( WarningDelay, WarningDelay, new TimerCallback( Warning_Callback ) );
			}

            // Flag the serializers
            ItemsSerializer.SaveNeeded = true;
            CreaturesSerializer.SaveNeeded = true;

            // Save (everything)
			AutoSave.Save();

			m_Restarting = true;

			Timer.DelayCall( RestartDelay, new TimerCallback( Restart_Callback ) );
		}
	}
}