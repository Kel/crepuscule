using System;
using System.Collections;
using Server.Network;
using Server.Targeting;

namespace Server.Items
{
	public abstract class BaseWallMov: CrepusculeItem
	{
		private bool m_FirstMov;
		private int m_FirstMovedID;
		private int m_SecondMovID;
		private Point3D m_Offset;
		private bool m_Running;
		private Timer m_Timer;
        private TimeSpan m_MinDelay;
        private TimeSpan m_MaxDelay;
        private DateTime m_End;
        

		private static Point3D[] m_Offsets = new Point3D[]
			{
				new Point3D(-1, 1, 0 ),
				new Point3D( 1, 1, 0 ),
				new Point3D(-1, 0, 0 ),
				new Point3D( 1,-1, 0 ),
				new Point3D( 1, 1, 0 ),
				new Point3D( 1,-1, 0 ),
				new Point3D( 0, 0, 0 ),
				new Point3D( 0,-1, 0 )             
				
			};

		// Called by RunUO
		public static void Initialize()
		{
			EventSink.OpenDoorMacroUsed += new OpenDoorMacroEventHandler( EventSink_OpenDoorMacroUsed );
			
		} 
	

		private static void EventSink_OpenDoorMacroUsed( OpenDoorMacroEventArgs args )
		{
			Mobile m = args.Mobile;

			if ( m.Map != null )
			{
				int x = m.X, y = m.Y;

				switch ( m.Direction & Direction.Mask )
				{
					case Direction.North:      --y; break;
					case Direction.Right: ++x; --y; break;
					case Direction.East:  ++x;      break;
					case Direction.Down:  ++x; ++y; break;
					case Direction.South:      ++y; break;
					case Direction.Left:  --x; ++y; break;
					case Direction.West:  --x;      break;
					case Direction.Up:    --x; --y; break;
				}

				Sector sector = m.Map.GetSector( x, y );

				foreach ( Item item in sector.Items )
				{
					if ( item.Location.X == x && item.Location.Y == y && (item.Z + item.ItemData.Height) > m.Z && (m.Z + 16) > item.Z && item is BaseWallMov && m.CanSee( item ) && m.InLOS( item ) )
					{
						if ( m.CheckAlive() && m.AccessLevel == AccessLevel.GameMaster )
						{
							m.SendMessage( "Vous d�placez le mur" ); 
							item.OnDoubleClick( m );
						}

						break;
					}
				}
			}
		}

		public static Point3D GetOffset( WallFacing facing )
		{
			return m_Offsets[(int)facing];
		}
        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan MinDelay
        {
            get { return m_MinDelay; }
            set { m_MinDelay = value; InvalidateProperties(); }
        }


        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan MaxDelay
        {
            get { return m_MaxDelay; }
            set { m_MaxDelay = value; InvalidateProperties(); }
        }
        public void InitWall(TimeSpan minDelay, TimeSpan maxDelay)
        {
            Visible = true;
            Movable = false;
            m_Running = false;
            Name = "Spawner";
            m_MinDelay = minDelay;
            m_MaxDelay = maxDelay;
                             
            DoTimer(TimeSpan.FromSeconds(1));
        }

		private class InternalTimer : Timer
		{
            private BaseWallMov m_WallMov;

			public InternalTimer( BaseWallMov wall, TimeSpan delay ) : base( delay )
			{
				Priority = TimerPriority.OneSecond;
				m_WallMov = wall;
			}

			protected override void OnTick()
			{
                //if (m_WallMov != null)
                  //  if (!m_WallMov.Deleted)
                    //   m_WallMov.OnTick();
                if (m_WallMov.FirstMov == true )
                {
                    m_WallMov.FirstMov = false;
                    m_WallMov.OnTick();
                }
                else if (m_WallMov.FirstMov == false)
                {
                    m_WallMov.FirstMov = true;
                    m_WallMov.OnTick();
                }
            }

		}
        public void OnTick()
        {
            DoTimer();
                
			
		}
        
        [CommandProperty(AccessLevel.GameMaster)]
        public bool Running
        {
            get { return m_Running; }
            set
            {
                if (value)
                    Start();
                else
                    Stop();

                InvalidateProperties();
            }
        }

        public void Start()
        {
            if (!m_Running)
            {
                    m_Running = true;
                    DoTimer();
                
            }
        }

        public void Stop()
        {
            if (m_Running)
            {
                m_Timer.Stop();
                m_Running = false;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public TimeSpan Mouvementdans
        {
            get
            {
                if (m_Running)
                    return m_End - DateTime.Now;
                else
                    return TimeSpan.FromSeconds(0);
            }
            set
            {
                Start();
                DoTimer(value);
            }
        }

        public void DoTimer()
        {
           
            if (!m_Running)
                return;

            int minSeconds = (int)m_MinDelay.TotalSeconds;
            int maxSeconds = (int)m_MaxDelay.TotalSeconds;

            if (m_FirstMov == true)
            {
                TimeSpan delay = TimeSpan.FromSeconds(maxSeconds);
                DoTimer(delay);
            }
            if (m_FirstMov == false)
            {
                TimeSpan delay = TimeSpan.FromSeconds(minSeconds);
                DoTimer(delay);
            }
                
        
         
        }

        public void DoTimer(TimeSpan delay)
        {
            if (!m_Running)
                return;

            m_End = DateTime.Now + delay;

            if (m_Timer != null)
                m_Timer.Stop();

            m_Timer = new InternalTimer(this, delay);
            m_Timer.Start();
        }

       

		

		

		[CommandProperty( AccessLevel.GameMaster )]
		public bool FirstMov
		{
			get
			{
				return m_FirstMov;
			}
			set
			{
				if ( m_FirstMov != value )
				{
					m_FirstMov = value;

					ItemID = m_FirstMov ? m_FirstMovedID : m_SecondMovID;

					if ( m_FirstMov )
						Location = new Point3D( X + m_Offset.X, Y + m_Offset.Y, Z + m_Offset.Z );
					else
						Location = new Point3D( X - m_Offset.X, Y - m_Offset.Y, Z - m_Offset.Z );

					

					
				}
			}
		}

		public bool CanClose()
		{
			if ( !m_FirstMov )
				return true;

			Map map = Map;

			if ( map == null )
				return false;

			Point3D p = new Point3D( X - m_Offset.X, Y - m_Offset.Y, Z - m_Offset.Z );

			return CheckFit( map, p, 16 );
		}

		private bool CheckFit( Map map, Point3D p, int height )
		{
			if ( map == Map.Internal )
				return false;

			int x = p.X;
			int y = p.Y;
			int z = p.Z;

			Sector sector = map.GetSector( x, y );
			ArrayList items = sector.Items, mobs = sector.Mobiles;

			for ( int i = 0; i < items.Count; ++i )
			{
				Item item = (Item)items[i];

				if ( item.ItemID < 0x4000 && item.AtWorldPoint( x, y ) && !(item is BaseWallMov) )
				{
					ItemData id = item.ItemData;
					bool surface = id.Surface;
					bool impassable = id.Impassable;

					if ( (surface || impassable) && (item.Z + id.CalcHeight) > z && (z + height) > item.Z )
						return false;
				}
			}

			for ( int i = 0; i < mobs.Count; ++i )
			{
				Mobile m = (Mobile)mobs[i];

				if ( m.Location.X == x && m.Location.Y == y )
				{
					if ( m.Hidden && m.AccessLevel > AccessLevel.Player )
						continue;

					if ( !m.Alive )
						continue;

					if ( (m.Z + 16) > z && (z + height) > m.Z )
						return false;
				}
			}

			return true;
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int FirstMovedID
		{
			get
			{
				return m_FirstMovedID;
			}
			set
			{
				m_FirstMovedID = value;
			}
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int SecondMovID
		{
			get
			{
				return m_SecondMovID;
			}
			set
			{
				m_SecondMovID = value;
			}
		}

		
	

		[CommandProperty( AccessLevel.GameMaster )]
		public Point3D Offset
		{
			get
			{
				return m_Offset;
			}
			set
			{
				m_Offset = value;
			}
		}
		public bool IsFreeToClose()
		{			
						

			bool freeToClose = true;		

			return freeToClose;
		}
        
		public virtual void OnFirstMoved( Mobile from )
		{
		}

		public virtual void OnSecondMov( Mobile from )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( from.AccessLevel == AccessLevel.Player && (/*!from.InLOS( this ) || */!from.InRange( GetWorldLocation(), 2 )) )
				from.LocalOverheadMessage( MessageType.Regular, 0x3B2, 1019045 ); // I can't reach that.
			
		}



        public BaseWallMov(int closedID, int openedID, Point3D offset)
            : base(closedID)
		{
			m_FirstMovedID = openedID;
           
			m_SecondMovID = closedID;
			
			m_Offset = offset;

          

            

			Movable = false;
		}

		public BaseWallMov( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
			

			writer.Write( m_FirstMov );		
			writer.Write( m_FirstMovedID );
			writer.Write( m_SecondMovID );			
			writer.Write( m_Offset );
			writer.Write( m_MinDelay );
			writer.Write( m_MaxDelay );			
			writer.Write( m_Running );			
			if ( m_Running )
				writer.WriteDeltaTime( m_End );
        }		
        

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
                
				case 0:
				{
					m_FirstMov = reader.ReadBool();
					m_FirstMovedID = reader.ReadInt();
					m_SecondMovID = reader.ReadInt();					
					m_Offset = reader.ReadPoint3D();    
					if ( m_FirstMov )
						m_Timer.Start();
                    m_MinDelay = reader.ReadTimeSpan();
                    m_MaxDelay = reader.ReadTimeSpan();
                    m_Running = reader.ReadBool();
                    TimeSpan ts = TimeSpan.Zero;
                    if (m_Running)
                        ts = reader.ReadDeltaTime() - DateTime.Now;
                    if (m_Running)
                        DoTimer(ts);

					break;
				}
			}
		}
	}
}
    namespace Server.Items
    {
        public enum WallFacing
        {
            WestCW,
            EastCCW,
            WestCCW,
            EastCW,
            SouthCW,
            NorthCCW,
            SouthCCW,
            NorthCW          
          
        }
        public class MovingWall : BaseWallMov
        {
            [Constructable]
            public MovingWall(WallFacing facing)
                : base(0x340 + (2 * (int)facing), 0x340 + (2 * (int)facing), BaseWallMov.GetOffset(facing))
            {
                
            }

           


            public MovingWall(Serial serial)
                : base(serial)
            {
            }

            public override void Serialize(GenericWriter writer) // Default Serialize method
            {
                base.Serialize(writer);

                writer.Write((int)0); // version
            }

            public override void Deserialize(GenericReader reader) // Default Deserialize method
            {
                base.Deserialize(reader);

                int version = reader.ReadInt();
            }
        }
    
}