﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Server
{
    public static class UtilityExtensions
    {
        public static string GetAttribute(XmlElement node, string attributeName)
        {
            return GetAttribute(node, attributeName, null);
        }

        public static string GetAttribute(XmlElement node, string attributeName, string defaultValue)
        {
            if (node == null)
                return defaultValue;

            XmlAttribute attr = node.Attributes[attributeName];

            if (attr == null)
                return defaultValue;

            return attr.Value;
        }

    }
}
