﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using System.Collections;
using Server.Network;

namespace Server
{
    public static class WorldContext
    {
        private static List<RacePlayerMobile> m_RacePlayerMobiles = new List<RacePlayerMobile>();

        /// <summary>
        /// Retourne tous les RacePlayerMobile
        /// </summary>
        public static List<RacePlayerMobile> RacePlayerMobiles
        {
            get { return m_RacePlayerMobiles; }
        }

        /// <summary>
        /// Retourne tous les mobiles dont le joueur connait (.renom)
        /// Kel: Linq powa! (ha ma premiere requete complexe en linq)
        /// </summary>
        public static PlayerKnownDictionary GetKnownMobiles(RacePlayerMobile mobile)
        {
            Serial mSerial = mobile.Serial; //perf optimisation
            PlayerKnownDictionary result = new PlayerKnownDictionary();

            var Ident1  = from rpm in RacePlayerMobiles
                          where (
                            from NAS in rpm.NomListe.Cast<NameAndSerial>()
                            where NAS.Serial == mSerial
                            select NAS
                          ).Count() > 0
                          select rpm;

            var Ident2 = from rpm in RacePlayerMobiles
                         where (
                           from NAS in rpm.NomListe2.Cast<NameAndSerial>()
                           where NAS.Serial == mSerial
                           select NAS
                         ).Count() > 0
                         select rpm;

            foreach(RacePlayerMobile rpm in Ident1)
                result.TryAdd(rpm, IdentityType.First);

            foreach (RacePlayerMobile rpm in Ident2)
                result.TryAdd(rpm, IdentityType.Second);

            return result;
        }

        public static ArrayList GetKnownMobilesArray(RacePlayerMobile mobile)
        {
            ArrayList array = new ArrayList();
            PlayerKnownDictionary list = GetKnownMobiles(mobile);
            foreach (PlayerKnown pk in list.Values)
                array.Add(pk.Known);
            return array;
        }

        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;

                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }
    }

}
