using System;
using Server;

namespace Server.Items
{
	public class Ceramique : Item
	{
		

		[Constructable]
		public Ceramique() : this( 1 ){
                Name = "Ceramique";
		
		{
		}

}		[Constructable]
		public Ceramique( int amount ) : base( 0x2cdd)
		{
			Stackable = false;
			Amount = amount;
		}

		public Ceramique( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}