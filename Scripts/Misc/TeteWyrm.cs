using System;
using Server;

namespace Server.Items
{
	public class TeteWyrm : Item
	{
		

		[Constructable]
		public TeteWyrm() : this( 1 ){
                Name = "T�te de Wyrm";
		Weight = 10.0;
		
		{
		}

}		[Constructable]
		public TeteWyrm( int amount ) : base( 0x2ef9)
		{
			Stackable = false;
			Amount = amount;
		}

		public TeteWyrm( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}