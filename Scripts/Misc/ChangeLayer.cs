/*****************************************************
 * ce script a �t� cr�e par Namorcen.
 * 
 * Merci de laisser ce copyright si vous l'utilisez.
 * 
 * pour tout renseignement:
 * commandercore@hotmail.com
 *****************************************************/

using System;
using System.Reflection;
using Server.Gumps;
using Server.Items;
using Server.Network;
using Server.Targeting;

namespace Server.Scripts.Commands
{
	public class ChangeLayer
	{
		public static void Initialize()
		{
			Server.Commands.Register( "ChangeLayer", AccessLevel.Player, new CommandEventHandler( ChangeLayer_OnCommand ) );
		}

		[Usage( "ChangeLayer" )]
		[Description( "Change le layer d'un objet sous conditions." )]
		private static void ChangeLayer_OnCommand( CommandEventArgs e )
		{
			e.Mobile.Target = new ChangeLayerTarget();
			e.Mobile.SendMessage( "De quel v�tement voulez-vous changer le layer ?" );
		}
		
		private class ChangeLayerTarget : Target
		{
			private static LayerTable m_Layer = 
			new LayerTable( new LayerGroup[] {
				new LayerGroup( new Layer[] {
					Layer.Neck,
					Layer.Helm,
					Layer.Earrings }
				),
				new LayerGroup( new Layer[] {
					Layer.Pants,
					Layer.OuterLegs }
				),
				new LayerGroup( new Layer[] {
					Layer.Shirt,
					Layer.OuterTorso,
					Layer.MiddleTorso,
					Layer.InnerTorso }
				) }
			);
			
			private class LayerTable
			{
				private LayerGroup[] m_Layers;
				
				public LayerTable( LayerGroup[] layers )
				{
					m_Layers = layers;
				}
				
				public bool contains( Layer layer )
				{		
					for ( int i = 0; i < m_Layers.Length; i++ )
					{
						if ( m_Layers[i].contains( layer ) )
							return true;
					}
					return false;
				}
				
				public Layer[] getGroup( Layer layer )
				{
					for ( int i = 0; i < m_Layers.Length; i++ )
					{
						if ( m_Layers[i].contains( layer ) )
							return m_Layers[i].getTable();
					}
					return null;
				}
			}
			
			private class LayerGroup
			{
				private Layer[] m_Layers;
			
				public LayerGroup( Layer[] layers )
				{
					m_Layers = layers;
				}
				
				public bool contains( Layer layer )
				{
					for( int i = 0; i < m_Layers.Length; i++ )
					{
						if ( layer == m_Layers[i] )
							return true;
					}
					return false;
				}
				
				public Layer[] getTable()
				{
					return m_Layers;
				}
			}
		
			public ChangeLayerTarget() : base( -1, false, TargetFlags.None )
			{
			}
			
			protected override void OnTarget( Mobile from, object target )
			{				
				if ( !(target is BaseClothing) || target is BaseHat )
				{
					from.SendMessage( "Vous ne pouvez pas changer le layer de cela." );
					return;
				}
				
				BaseClothing targ = (BaseClothing) target;
				
				if ( !targ.IsChildOf( from.Backpack ) )
				{
					from.SendMessage( "Cela doit �tre dans votre sac pour changer le layer." );
					return;
				}
				
				else if ( m_Layer.contains( targ.Layer ) )
				{
					from.CloseGump( typeof( ChangeLayerGump ) );
					from.SendGump( new ChangeLayerGump( targ ) );
				}
				
				else
					from.SendMessage( "Vous ne pouvez pas changer le layer de ce v�tement." );
			}
			
			private class ChangeLayerGump : Gump
			{
				private BaseClothing m_Cloth;
				private Layer[] m_Entry;

				public ChangeLayerGump( BaseClothing cloth ) : base( 50, 50 )
				{
					m_Cloth = cloth;					
					
					m_Entry = m_Layer.getGroup( cloth.Layer );
					
					AddPage( 0 );

					AddBackground( 100, 10, 350, 96 + m_Entry.Length * 22, 2600 );
					AddBackground( 120, 54, 110, 6 + m_Entry.Length * 22, 5100 );

					AddLabel( 185, 25, 0x00, "Menu de changement de Layer.");
					
					AddLabel( 260, 59 - 22 + m_Entry.Length * 11, 0x00, "Le Layer actuel est :" );
					AddLabel( 260, 59 + m_Entry.Length * 11, 0x00, "" + m_Cloth.Layer );

					AddButton( 149, 70 + m_Entry.Length * 22, 4005, 4007, 1, GumpButtonType.Reply, 0 );
					AddLabel( 185, 71 + m_Entry.Length * 22, 0x00, "Changer le layer ! " );
					
					for ( int i = 0; i < m_Entry.Length; ++i )
					{
						AddLabel( 130, 59 + i * 22, 0x00, "" + m_Entry[i] );
						AddRadio( 207, 60 + i * 22, 210, 211, false, i );
					}
				}

				public override void OnResponse( NetState from, RelayInfo info )
				{
					if ( m_Cloth.Deleted )
						return;

					Mobile m = from.Mobile;
					int[] switches = info.Switches;

					if ( !m_Cloth.IsChildOf( m.Backpack ) )
					{
						m.SendMessage( "Le v�tement doit rester dans votre sac !" );
						return;
					}

					if ( info.ButtonID != 0 && switches.Length > 0 )
					{
						if ( switches[0] < m_Entry.Length )
						{
							Layer e = m_Entry[switches[0]];
							if ( m_Cloth.Layer != e )
							{
								m_Cloth.Layer = e;
								m.SendMessage( "Vous changez le Layer en : " + e );
							}
							else
								m.SendMessage( "Vous gardez le m�me Layer." );
						}
					}
					else
					{
						m.SendMessage( "Vous d�cidez de ne pas changer le Layer." );
					}
				}
			}
		}
	}
}
