using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "un corps" )]
	public class MeleeTrainerB : BaseCreature
	{
		[Constructable]
		public MeleeTrainerB() : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.2, 0.4 )
		{
			Name = "Entraineur";
			Body = 0x191;

			SetStr( 467, 645 );
			SetDex( 770, 950 );
			SetInt( 0, 00 );

			SetHits( 296000, 372000 );
			SetMana( 0, 0 );

			SetDamage( 1, 5 );

			SetDamageType( ResistanceType.Physical, 75 );

			SetSkill( SkillName.Wrestling, 110.1, 120.0 );

			Fame = 25000;
			Karma = -25000;

			VirtualArmor = 20;
		}

		public override void GenerateLoot()
		{
			//AddLoot( LootPack.UltraRich, 2 );
		}


		public override OppositionGroup OppositionGroup
		{
			get{ return OppositionGroup.MeleeTrainerAAndMeleeTrainerB; }
		}

		public MeleeTrainerB( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

			if ( BaseSoundID == 263 )
				BaseSoundID = 0x24D;
		}
	}
}
