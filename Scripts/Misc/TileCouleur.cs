using System;
using Server;

namespace Server.Items
{
	public class TileCouleur : Item
	{
		

		[Constructable]
		public TileCouleur() : this( 1 ){
                Name = "Plancher De Couleur";
		
		{
		}

}		[Constructable]
		public TileCouleur( int amount ) : base( 0x2c98)
		{
			Stackable = false;
			Amount = amount;
		}

		public TileCouleur( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}