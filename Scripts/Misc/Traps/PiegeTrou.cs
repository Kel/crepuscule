using System;

namespace Server.Items
{
	public class PiegeTrou : BaseTrap
	{
		private int i_DAMMIN = 20;
		private int i_DAMMAX = 30;
		[CommandProperty( AccessLevel.GameMaster )]
		public int DAMMIN
		{
			get { return i_DAMMIN; }
			set { i_DAMMIN = value; }
		}
		[CommandProperty( AccessLevel.GameMaster )]
		public int DAMMAX
		{
			get { return i_DAMMAX; }
			set { i_DAMMAX = value; }
		}
		[Constructable]
		public PiegeTrou() : base( 0x11C1 )
		{
		}

		public override bool PassivelyTriggered{ get{ return true; } }
		public override TimeSpan PassiveTriggerDelay{ get{ return TimeSpan.FromSeconds( 10.0 ); } }
		public override int PassiveTriggerRange{ get{ return 0; } }
		public override TimeSpan ResetDelay{ get{ return TimeSpan.FromSeconds( 1.0 ); } }

		public override void OnTrigger( Mobile from )
		{
			Effects.SendLocationParticles( EffectItem.Create( Location, Map, EffectItem.DefaultDuration ), 0x11C0, 10, 30, 5052 );
			Effects.PlaySound( Location, Map, 0x236 );

			if ( from.Alive && from.Location == this.Location )
				Spells.SpellHelper.Damage( TimeSpan.FromSeconds( 0.5 ), from, Utility.RandomMinMax( this.DAMMIN, this.DAMMAX ), 0, 100, 0, 0, 0 );
		}

		public PiegeTrou( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
			writer.Write( (int) i_DAMMIN );
			writer.Write( (int) i_DAMMAX );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
 
			i_DAMMIN = reader.ReadInt();
			i_DAMMAX = reader.ReadInt();
		}
	}
} 