using System;
using Server;

namespace Server.Items
{
	public class ChampignonPiege: Item
	{
		private bool m_Activated;

		[CommandProperty( AccessLevel.GameMaster )]
		public bool Activated
		{
			get { return m_Activated; }
			set { m_Activated = value; }
		}

		[Constructable]
		public ChampignonPiege() : this( 1 )
		{
			Movable = false;
			m_Activated = true;
		}

		[Constructable]
		public ChampignonPiege( int amount ) : base( 4389 )
		{
		}

		public ChampignonPiege( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
			writer.Write( m_Activated );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
			m_Activated = reader.ReadBool();

		}

		public override void OnDoubleClick( Mobile from )
		{
			if (from.AccessLevel >= AccessLevel.GameMaster)
			{
				m_Activated = !m_Activated;
				if (m_Activated)
					from.SendMessage("Be Careful! Activated!");
				else
					from.SendMessage("Unactivated.");
			}
		}


		public override bool OnMoveOver( Mobile m )
		{
			if ( m_Activated && m.Alive && (m.Hidden == false))
			{
				new DelayTimer(this).Start();
				m.PlaySound( Utility.RandomMinMax( 553 , 554 ) );
				m.SendLocalizedMessage( 1010523 ); // A toxic vapor envelops thee.
				AOS.Damage( m, m, Utility.RandomMinMax( 1, 8 ), 0, 100, 0, 0, 0 );
			}
			return true;
		}

		private class DelayTimer : Timer
		{
			private ChampignonPiege m_Champignon;

			public DelayTimer( ChampignonPiege champignon ) : base( TimeSpan.FromSeconds( 0 ), TimeSpan.FromSeconds( 0.3 ) )
			{
				m_Champignon = champignon;
			}

			protected override void OnTick()
			{
				if (m_Champignon.ItemID >= 4394)
				{
					Stop();
					m_Champignon.Delete();
				}
				else
					m_Champignon.ItemID++;
			}
		}
	}
}