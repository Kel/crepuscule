﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.IO;
using System.Text;
using System.IO.Compression;

namespace Server.Engines.RemoteAdmin
{
    public class GZippedFile
    {
        public int Index = 0;
        public string RelativePath = null;
        public DateTime ModifiedDate;
        public int Length = 0;
        public bool AddedToTempFile = false;
        public bool Restored = false;
        public string LocalPath = null;
        public string Folder = null;

        public static GZippedFile GetGZippedFile(string fileInfo)
        {
            GZippedFile gzf = null;

            if (!string.IsNullOrEmpty(fileInfo))
            {
                // get the file information
                string[] info = fileInfo.Split(',');
                if (info != null && info.Length == 4)
                {
                    gzf = new GZippedFile();
                    gzf.Index = Convert.ToInt32(info[0]);
                    gzf.RelativePath = info[1].Replace("/", "\\");
                    gzf.ModifiedDate = Convert.ToDateTime(info[2]);
                    gzf.Length = Convert.ToInt32(info[3]);
                }
            }
            return gzf;
        }
    }
}
