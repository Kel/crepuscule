﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.IO.Compression;
using Server.Scripts.Commands;
using Crepuscule.Update;
using Crepuscule.Update.Engine;
using Server.Network;

namespace Server.Engines.RemoteAdmin
{
    public static class BackupManager
    {
        public static string SavesFolder = "Saves/";
        public static string SavesTemp = "SavesTemp/";
        public static string BackupZip = "LastBackup.gzip";

        public static Thread BackupThread = null;

        public static void BackupServer()
        {
            try
            {
                if (BackupThread != null) BackupThread.Abort();
                //Copy data folder
                var saves = new DirectoryInfo(SavesFolder);
                if (saves.Exists)
                {
                    var savesTemp = new DirectoryInfo(SavesTemp);
                    if (savesTemp.Exists) savesTemp.Delete(true);
                    CopyAll(saves, savesTemp);

                    BackupThread = new Thread(new ThreadStart(BackupAndUpload));
                    BackupThread.Start();
                }
            }
            catch (Exception ex) { BroadcastMessage(AccessLevel.Administrator, 7, ex.Message); }
        }

        private static void BackupAndUpload()
        {
            try
            {
                var Gzip = new FileInfo(BackupZip);
                if (Gzip.Exists) Gzip.Delete();

                var SavesTempPath = new DirectoryInfo(SavesTemp);

                var result = GZip.Compress(SavesTempPath.FullName, SavesTempPath.FullName, BackupZip);
                Gzip = new FileInfo(result.ZipFile);
                if (Gzip.Exists)
                {
                    var Encrypted = new FileInfo("LastBackup.bak");
                    var Password = "";
                    var BackupConfig = new FileInfo("Data/backup.cfg");
                    if (BackupConfig.Exists)
                    {
                        Password = File.ReadAllText(BackupConfig.FullName);
                    }

                    if (!String.IsNullOrEmpty(Password))
                    {
                        if (Encrypted.Exists) Encrypted.Delete();
                        BroadcastMessage(AccessLevel.Administrator, 7, "Compression terminée, cryptage en cours...");
                        CryptoHelp.EncryptFile(Gzip.FullName, Encrypted.FullName, Password, null);
                        BroadcastMessage(AccessLevel.Administrator, 7, "Cryptage terminée, upload en cours...");

                        //Prepare FTP
                        var ftp = new FTP();
                        ftp.Connect(Packager.FtpHost, Packager.FtpPort, Packager.FtpUsername, Packager.FtpPassword);
                        ftp.ChangeDirectory("backups");

                        var now = DateTime.Now;
                        string PackageName = String.Format("{0}_{1}_{2}.bak",
                            now.Year, now.Month, now.Day);

                        //Upload file to ftp
                        ftp.Files.Upload(PackageName, Encrypted.FullName);
                        while (!ftp.Files.UploadComplete)
                        {
                            Thread.Sleep(100);
                        }

                        try
                        {
                            ftp.Disconnect();
                        }
                        catch { }
                        BroadcastMessage(AccessLevel.Administrator, 7, "Backup terminé!");
                    }
                    else
                    {
                        BroadcastMessage(AccessLevel.Administrator, 7, "Mot de passe non trouvé, annulation du backup...");
                    }
                }
                else
                {
                    BroadcastMessage(AccessLevel.Administrator, 7, "Annulation du backup...");
                }
            }
            catch (Exception ex) { BroadcastMessage(AccessLevel.Administrator, 7, ex.Message); }
            BackupThread = null;
        }

        
        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it’s new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }


        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;

                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }

    }
}
