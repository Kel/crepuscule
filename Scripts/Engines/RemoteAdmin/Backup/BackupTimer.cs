﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Scripts.Commands;
using Server.Mobiles;
using System.Threading;
using Server.Engines.RemoteAdmin;
using Server.Guilds;

namespace Server.Engines
{

    public class BackupTimer : Timer
    {

        public static TimeSpan BackupDelay = new TimeSpan(3, 0, 0, 0);
        public static TimeSpan TimerDelay = new TimeSpan(0,1, 0, 0);

        public static void Initialize()
        {
            new BackupTimer();
        }

        public BackupTimer()
            : base(TimerDelay, TimerDelay)
        {
            this.Start();
        }

        protected override void OnTick()
        {
            //if (GuildsSystem.LastBackupDay.Add(BackupDelay) <= DateTime.Now)
            //{ // it's backup day!!!
            //    GuildsSystem.LastBackupDay = DateTime.Now;
            //    BackupManager.BackupServer();
            //}


        }


    }
}
