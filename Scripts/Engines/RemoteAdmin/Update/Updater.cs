﻿using System;
using System.Collections.Generic;
using System.Text;
using Crepuscule.Update.Engine;
using System.Net;
using System.IO;
using System.Security.Cryptography;

namespace Crepuscule.Update
{
    public static class Updater
    {
        public static PatchInfo PatchInfo = null;
        public static string UltimaOnlinePath = @"c:\";
        public static string PatchInfoFilename = "";
        public static List<PatchFileInfo> UpdateList = new List<PatchFileInfo>();

        static MD5 md5 = new MD5CryptoServiceProvider();

        public static void GetPatchInfo()
        {
            WebClient Client = new WebClient();
            string PatchInfoFile = Path.Combine(PatchInfoFilename, "Patch.xml");
            Client.DownloadFile("http://www.au-crepuscule.com/download/update/Patch.xml", PatchInfoFile);
            PatchInfo = PatchInfo.Load(PatchInfoFile);
        }


        public static bool CheckUpdates()
        {
            UpdateList = new List<PatchFileInfo>();
            GetPatchInfo();
            foreach (PatchFileInfo file in PatchInfo.Files)
            {
                FileInfo hddFile = new FileInfo(Path.Combine(UltimaOnlinePath, file.Filename));
                bool NeedUpdate=true;
                if (hddFile.Exists)
                {
                    byte[] Hash;
                    //Compute the hash
                    using (FileStream inStream = new FileStream(hddFile.FullName, FileMode.Open))
                    {
                        Hash = md5.ComputeHash(inStream);
                    }

                    //Check by hash
                    if (CheckHash(Hash, file.FileMD5))
                        NeedUpdate = false;
                }
                if (NeedUpdate) UpdateList.Add(file);
            }

            return UpdateList.Count > 0;
        }


        public static bool CheckHash(byte[] ba1, byte[] ba2)
        {
            if (ba1 == null)
                throw new ArgumentNullException("ba1");
            if (ba2 == null)
                throw new ArgumentNullException("ba2");

            if (ba1.Length != ba2.Length)
                return false;

            for (int i = 0; i < ba1.Length; i++)
            {
                if (ba1[i] != ba2[i])
                    return false;
            }
            return true;
        }



    }
}
