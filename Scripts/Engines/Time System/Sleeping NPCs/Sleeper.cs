using System;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.Gumps;

namespace Server.Items
{
	// version 1.1.1 Bed coordinates of 0,0,0 will cause npc to sleep and wake at it's current location.
	// version 1.0 initial release.
	public class Sleeper : CrepusculeItem
	{
		private Timer m_Timer;
		private SleepingNPCBody m_SleepingNPCBody;
		private bool m_Active = false;
		private int m_SleepHour = 22;
		private int m_SleepMin = 0;
		private int m_WakeHour = 6;
		private int m_WakeMin = 0;
		private Mobile m_Vendor;
		private Point3D m_Location;
		private Direction m_Direction = Direction.South;
		private bool m_Sleeping = false;
        private bool m_Debug = false;
        private BaseDoor m_Door;

		[CommandProperty( AccessLevel.GameMaster )]
		public Point3D Bed
		{
			get{ return m_Location; }
			set{ m_Location = value; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public bool Debug
		{
			get{ return m_Debug; }
			set{ m_Debug = value; }
		}

        [CommandProperty(AccessLevel.GameMaster)]
        public BaseDoor DoorToLock
        {
            get { return m_Door; }
            set { m_Door = value; }
        }

		[CommandProperty( AccessLevel.GameMaster )]
		public bool Active
		{
			get{ return m_Active; }
			set{ m_Active = value; InvalidateProperties(); }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public bool Asleep
		{
			get{ return m_Sleeping; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public Mobile Mobile
		{
			get{ return m_Vendor; }
			set
			{ 
				if( value != null && value.Player )
					value = null;

				if( value == null )
					m_Active = false;
				else
					m_Active = true;

				m_Vendor = value; 

				InvalidateProperties();
			}
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int Sleep_Hour
		{
			get{ return m_SleepHour; }
			set{ m_SleepHour = value % 24; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int Sleep_Minute
		{
			get{ return m_SleepMin; }
			set{ m_SleepMin = value % 60; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int Wake_Hour
		{
			get{ return m_WakeHour; }
			set{ m_WakeHour = value % 24; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int Wake_Minute
		{
			get{ return m_WakeMin; }
			set{ m_WakeMin = value % 60; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public Direction Facing
		{
			get { return m_Direction; }
			set { m_Direction = value; }
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public string Time_Now
		{
			get
			{ 
				Map map = this.Map;
				int x = this.X;
				int y = this.Y;
				int hours;
				int minutes;

				Clock.GetTime( map, x, y, out hours, out minutes );
				return String.Format( "{0}:{1}", hours, minutes.ToString("d2") ); // minutes display with leading zero
			}
		}

		[Constructable]
		public Sleeper() : base( 0xFAA )
		{
			Visible = false;
			Name = "Sleeper";

			m_Timer = new InternalTimer( this );
			m_Timer.Start();
		}

		public void CheckTime()
		{
			if( m_Vendor == null || !m_Vendor.Alive )
			{
				m_Active = false;
				InvalidateProperties(); 
			}
			if( !m_Active ) return;

			Map map = this.Map;
			int x = this.X;
			int y = this.Y;
			int hours;
			int minutes;

			Clock.GetTime( map, x, y, out hours, out minutes );

			double curTime = hours + ( minutes == 0 ? 0.0 : (1/(60.0/minutes)) ); // use .0 to force double precision
			double sleepTime = m_SleepHour + ( m_SleepMin == 0 ? 0.0 : (1/(60.0/m_SleepMin)) ); // 2 hours 30 minutes = 2.5
			double wakeTime = m_WakeHour + ( m_WakeMin == 0 ? 0.0 : (1/(60.0/m_WakeMin)) );

			if ( m_Debug )
			{
				string msg = String.Format("current: {0}\nsleep: {1}\nwake: {2}", curTime.ToString("f2"), sleepTime.ToString("f2"), wakeTime.ToString("f2") ); 
				PublicOverheadMessage( Network.MessageType.Regular, 0x3B2, false, msg ); 
			}
 
			if( sleepTime == wakeTime ) return;
			if( sleepTime > wakeTime ) // Diurnal
			{
				if( curTime >= sleepTime ) Sleep();
				else if( curTime >= wakeTime ) Wakeup();
			}
			else // Nocturnal
			{
				if( curTime >= wakeTime ) Wakeup();
				else if( curTime >= sleepTime ) Sleep();
			}
		}

		private void Sleep()
		{
			if( m_Sleeping ) return;
			m_Sleeping = true;

			m_Vendor.Direction = m_Direction;

			Point3D p = ( m_Location == Point3D.Zero ? m_Vendor.Location : m_Location );

			m_SleepingNPCBody = new SleepingNPCBody( m_Vendor, false, false );
			m_SleepingNPCBody.MoveToWorld( p, this.Map );

			m_Vendor.Map = Map.Internal;

            if (m_Door != null)
                m_Door.Locked = true;
		}

		private void Wakeup()
		{
			if( !m_Sleeping ) return;
			m_Sleeping = false;

			Point3D p = ( m_Location == Point3D.Zero ? m_SleepingNPCBody.Location : this.Location );

			m_Vendor.MoveToWorld( p, this.Map );

			if( m_SleepingNPCBody != null )
				m_SleepingNPCBody.Delete();

			m_SleepingNPCBody = null;

            if (m_Door != null)
                m_Door.Locked = false;
		}
		
		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

			string tmp = String.Format( "{0}: {1}", this.Name, ( m_Vendor != null ? m_Vendor.Name : "unassigned" ) );
			list.Add( tmp );

			if ( m_Active )
				list.Add( 1060742 ); // active
			else
				list.Add( 1060743 ); // inactive
		}

		public override void OnDoubleClick( Mobile from ) 
		{ 
			if ( m_Vendor != null && from.AccessLevel >= AccessLevel.GameMaster )
				from.SendGump( new APropertiesGump( from, m_Vendor ) );
		} 

		public Sleeper( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );

            writer.Write( (Item)m_Door);
			writer.Write( (Item)m_SleepingNPCBody );
			writer.Write( m_SleepHour );
			writer.Write( m_SleepMin );
			writer.Write( m_WakeHour );
			writer.Write( m_WakeMin );
			writer.Write( (Mobile)m_Vendor );
			writer.Write( m_Active );
			writer.Write( m_Location );
			writer.Write( (int)m_Direction );
			writer.Write( m_Sleeping );
            
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

            m_Door = (BaseDoor)reader.ReadItem();
			m_SleepingNPCBody = (SleepingNPCBody)reader.ReadItem();
			m_SleepHour = reader.ReadInt();
			m_SleepMin = reader.ReadInt();
			m_WakeHour = reader.ReadInt();
			m_WakeMin = reader.ReadInt();
			m_Vendor = reader.ReadMobile();
			m_Active = reader.ReadBool();
			m_Location = reader.ReadPoint3D();
			m_Direction = (Direction)reader.ReadInt();
			m_Sleeping = reader.ReadBool();

			m_Debug = false;
			m_Timer = new InternalTimer( this );
			m_Timer.Start();
		}


		private class InternalTimer : Timer
		{
			private Sleeper m_Sleeper;

			public InternalTimer( Sleeper s ) : base( TimeSpan.FromMinutes( 1 ), TimeSpan.FromMinutes( 1 ) )
			{
				Priority = TimerPriority.OneMinute;
				m_Sleeper = s;
			}

			protected override void OnTick()
			{
				m_Sleeper.CheckTime();
			}
		}
	}
}
