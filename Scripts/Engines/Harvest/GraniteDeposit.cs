using System;
using Server.Mobiles;
using Server.Engines.Craft;
using Server.Engines;
using Server.Scripts.Commands;

namespace Server.Items
{
	public class GraniteDeposit: CrepusculeItem
	{
        private Type m_Resource;
		private int m_Quantity = 0;
		private int m_Hours = 1;
		private int m_Max = 10;
		private int m_Skill = 0;
		private InternalTimer Regen;
		
		[CommandProperty( AccessLevel.GameMaster )]
		public Type Resource
		{
			get
			{
				return m_Resource;
			}
			set
			{ 
				if ( m_Resource != value )
				{
					m_Resource = value;
                    var type = GameItemType.FindType(m_Resource);
                    if (type != null)
                        Hue = type.Hue;

					InvalidateProperties();
				}
			}
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public int Quantity
		{
			get
			{
				return m_Quantity;
			}
			set
			{ 
				m_Quantity = value;
				InvalidateProperties();
			}
		}
		[CommandProperty( AccessLevel.GameMaster )]
		public int Hours
		{
			get
			{
				return m_Hours;
			}
			set
			{ 
				m_Hours = value;
				InvalidateProperties();
			}
		}
		[CommandProperty( AccessLevel.GameMaster )]
		public int Max
		{
			get
			{
				return m_Max;
			}
			set
			{ 
				m_Max = value;
				InvalidateProperties();
			}
		}
		[CommandProperty( AccessLevel.GameMaster )]
		public int Skill
		{
			get
			{
				return m_Skill;
			}
			set
			{ 
				m_Skill = value;
				InvalidateProperties();
			}
		}
		
		[Constructable]
		public GraniteDeposit() : base( 0x177A )
		{
			Name = "D�pot de Granite";
			Movable = false;
            m_Resource = typeof(Granite);
            Hue = GameItemType.GetHue(m_Resource);
			
			Regen = new InternalTimer(this, TimeSpan.FromHours(m_Hours));
			Regen.Start();
		}

		public GraniteDeposit( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 1 ); // version
			writer.Write(m_Quantity);
			writer.Write(m_Hours);
			writer.Write(m_Max);
			writer.Write(m_Skill);
			writer.Write(m_Resource);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

            m_Quantity = reader.ReadInt();
            m_Hours = reader.ReadInt();
            m_Max = reader.ReadInt();
            m_Skill = reader.ReadInt();
            m_Resource = reader.ReadType();

            Regen = new InternalTimer(this, TimeSpan.FromHours(m_Hours));
			Regen.Start();
		}
		private class InternalTimer : Timer
        {
			private GraniteDeposit m_from;
		
			public InternalTimer( GraniteDeposit from, TimeSpan m_Delay) : base( TimeSpan.Zero, m_Delay )
            {
				m_from = from;
            }
			protected override void OnTick() 
         	{
				if (m_from.m_Quantity < m_from.m_Max )
				m_from.m_Quantity += 1;
			}
		}
		public void Gather(Mobile from, Item tool, GraniteDeposit target)
		{
			double skillValue = from.Skills[SkillName.Mining].Value;
			
			if (from.InRange( target.GetWorldLocation(), 1 ))
				if (from.CanBeginAction(typeof(GraniteDeposit)))
					if (skillValue >= target.m_Skill && from is PlayerMobile && ((PlayerMobile)from).StoneMining )
						if (target.m_Quantity >= 1)
						{
							from.BeginAction(typeof(GraniteDeposit));
          					(new HarvestTimer(from, TimeSpan.FromSeconds(2.0), tool, target)).Start();
          					from.Direction = from.GetDirectionTo( target.GetWorldLocation() );
							from.PlaySound(Utility.RandomList(0x125, 0x126));
          					from.Animate(11, 1, 1, true, false, 0);
						}
						else from.SendMessage("Il n'y a pas assez de granite.");
					else from.SendMessage("Vous n'avez pas les connaissances requises.");
				else from.SendMessage("Concentrez-vous sur votre premi�re t�che!");
			else from.SendMessage("Vous �tes trop loin.");
		}
		private class HarvestTimer : Timer
		{
			private Mobile m_from;
			private Pickaxe m_tool;
			private GraniteDeposit m_target;
			private BaseGranite granite;
			private int i;

			public HarvestTimer(Mobile from, TimeSpan duration, Item tool, GraniteDeposit target) : base(TimeSpan.Zero, duration)
			{
				m_from = from;
				m_tool = tool as Pickaxe;
				m_target = target;
				i = 0;

                if (m_target != null && m_target.m_Resource != null)
                {
                    granite = GameItemType.CreateItem(m_target.m_Resource) as BaseGranite;
                    if (granite == null) granite = new Granite();
                }

			}
			protected override void OnTick() 
         	{
                if (m_target == null || m_tool == null || granite == null)
                {
                    OnStop();
                    return;
                }
				if (m_from.InRange( m_target.GetWorldLocation(), 1 ))
				{
					m_from.Direction = m_from.GetDirectionTo( m_target.GetWorldLocation() );
					m_from.PlaySound(Utility.RandomList(0x125, 0x126));
          			m_from.Animate(11, 1, 1, true, false, 0);
          			i++ ;
          			if (i >= 3)
          			{
						m_target.m_Quantity -- ;
						m_tool.UsesRemaining -- ;
						m_from.AddToBackpack(granite);
						m_from.SendMessage("Vous trouvez un granite utilisable.");
						OnStop();
          			}
				}
          		else 
				{
					m_from.SendMessage("Vous �tes trop loin.");
                    OnStop();
				}
			}
			private void OnStop()
			{
				m_from.EndAction(typeof(GraniteDeposit));
				Stop();
			}
		}
	}
}
