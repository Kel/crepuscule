///////////////////////////////////////////////////////////////////////////////////////
// Concept and Ideas by WhoopeR. Coding and Debugging by Kwwres10 (Farmer Noxstyx).  //
// Use freely but please don't claim the script.                                     //
// Contact Kwwres10 at Kwwres10@hotmail.com through MSN For more Details.            //
///////////////////////////////////////////////////////////////////////////////////////
using System; 
using Server.Items;

namespace Server.Items
{ 
   public class GraveDustPlant: CrepusculeItem 
   { 
      [Constructable]
       public GraveDustPlant()
           : base(0xF8F) 
      { 
         Movable = false; 
         Name = "Cendres";
         timer_pousse = new Timer_Pousse(this);
         timer_pousse.Debuter();
        
      }

       private bool m_Donne;
       private double m_Ticks;
       private Timer_Pousse timer_pousse;

       [CommandProperty(AccessLevel.GameMaster)]
       public bool Donne
       {
           get { return m_Donne; }
           set { m_Donne = value; }
       }


       [CommandProperty(AccessLevel.GameMaster)]
       public double Ticks
       {
           get { return m_Ticks; }
           set { m_Ticks = value; }
       }

      public override void OnDoubleClick( Mobile from ) 
      {
			if( from.InRange(this.Location,2) )
			{
				if (this.Donne == true)
				{
					GraveDust itemadd = new GraveDust();
					from.AddToBackpack(itemadd);
					this.ItemID = 1;
					this.Donne = false;
					from.SendMessage("Vous les r�coltez."); this.timer_pousse.Debuter();
				}
			}
         
      } 

      public GraveDustPlant( Serial serial ) : base( serial ) 
      { 
      } 

      public override void Serialize( GenericWriter writer ) 
      { 
         base.Serialize( writer );
         writer.Write(m_Donne);
         writer.Write(m_Ticks);
         writer.Write( (int) 0 ); // version 
      } 

      public override void Deserialize( GenericReader reader ) 
      { 
         base.Deserialize( reader );
         m_Donne = reader.ReadBool();
         m_Ticks = reader.ReadDouble();
         int version = reader.ReadInt(); 

timer_pousse = new Timer_Pousse(this);
         timer_pousse.Debuter();
      }




       public class Timer_Pousse : Timer
       {
           private GraveDustPlant who;
           private GraveDustPlant m_item;

           public void Arreter()
           {
               //m_Timer_Pousse.Remove(who.Serial.Value);
               this.Stop();
           }
           public void Debuter()
           {
               m_item = (GraveDustPlant)who;
               
               this.Start();
           }

           // Apres 2.5 secondes, le timer sera declanch� tout les secondes. apres 5 ticks, �a s'arretera
           public Timer_Pousse(GraveDustPlant from)
               : base(TimeSpan.Zero, TimeSpan.FromHours(1.0))
           {
               m_item = (GraveDustPlant)from;
               who = (GraveDustPlant)from;
               m_item.Ticks = Utility.Random(2,7);
               this.Start();
           }

          

 

           protected override void OnTick()
           {
               if (m_item.Ticks <= 0)
               {
                   m_item.Ticks = Utility.Random(2,7);
                   if (m_item.Donne == false)
                   {
							 this.Stop();
                       m_item.ItemID = 0xF8F;
                       m_item.Donne = true;
                   }  
               }
               else
               {
                   m_item.Ticks -= 1;
               }

           }
       }

   } 
} 