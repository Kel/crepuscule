using System;
using Server;

namespace Server.Items
{
	public class ReptilePeau : Item
	{
		

		[Constructable]
		public ReptilePeau() : this( 1 ){
                Name = "Peau Reptilienne";
		Hue = 0X888;
		{
		}

}		[Constructable]
		public ReptilePeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public ReptilePeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}