using System;
using Server;

namespace Server.Items
{
	public class ReptileCuir : Item
	{
		

		[Constructable]
		public ReptileCuir() : this( 1 ){
                Name = "Cuir Reptilien";
		Hue = 0X888;
		{
		}

}		[Constructable]
		public ReptileCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public ReptileCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}