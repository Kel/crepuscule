using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0x107a, 0x107a )]
	public class tanins : BaseTool
	{

		[Constructable]
		public tanins() : base( 0x107a )
		{
			Weight = 2.0;
			Name= "Tanin";
		}

		[Constructable]
		public tanins( int uses ) : base( uses, 0x107a )
		{
			Weight = 2.0;
			Name= "Tanin";
		}

		public tanins( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}