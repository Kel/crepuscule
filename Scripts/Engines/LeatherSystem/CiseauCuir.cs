using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	[FlipableAttribute( 0xdfd, 0xdfc )]
	public class CiseauCuir : BaseTool
	{

		[Constructable]
		public CiseauCuir() : base( 0xdfd )
		{
			Weight = 2.0;
			Name= "Ciseau a cuir";
		}

		[Constructable]
		public CiseauCuir( int uses ) : base( uses, 0xdfd )
		{
			Weight = 2.0;
			Name= "Ciseau a Cuir";
		}

		public CiseauCuir( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}