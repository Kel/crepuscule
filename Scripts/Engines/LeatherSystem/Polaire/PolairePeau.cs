using System;
using Server;

namespace Server.Items
{
	public class PolairePeau : Item
	{
		

		[Constructable]
		public PolairePeau() : this( 1 ){
                Name = "Peau Polaire";
		Hue = 0X80B;
		{
		}

}		[Constructable]
		public PolairePeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public PolairePeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}