using System;
using Server;

namespace Server.Items
{
	public class PolaireCuir : Item
	{
		

		[Constructable]
		public PolaireCuir() : this( 1 ){
                Name = "Cuir Polaire";
		Hue = 0X80B;
		{
		}

}		[Constructable]
		public PolaireCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public PolaireCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}