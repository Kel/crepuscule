using System;
using Server;

namespace Server.Items
{
	public class EpaisCuir : Item
	{
		

		[Constructable]
		public EpaisCuir() : this( 1 ){
                Name = "Cuir �pais";
		Hue = 0X7f4;
		{
		}

}		[Constructable]
		public EpaisCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public EpaisCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}