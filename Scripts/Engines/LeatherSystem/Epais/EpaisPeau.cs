using System;
using Server;

namespace Server.Items
{
	public class EpaisPeau : Item
	{
		

		[Constructable]
		public EpaisPeau() : this( 1 ){
                Name = "Peau �paisse";
		Hue = 0X7f4;
		{
		}

}		[Constructable]
		public EpaisPeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public EpaisPeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}