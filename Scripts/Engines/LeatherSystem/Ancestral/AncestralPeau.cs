using System;
using Server;

namespace Server.Items
{
	public class AncestralPeau : Item
	{
		

		[Constructable]
		public AncestralPeau() : this( 1 ){
                Name = "Peau Ancestrale";
		Hue = 0X7f5;
		{
		}

}		[Constructable]
		public AncestralPeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public AncestralPeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}