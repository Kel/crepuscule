using System;
using Server;

namespace Server.Items
{
	public class AncestralCuir : Item
	{
		

		[Constructable]
		public AncestralCuir() : this( 1 ){
                Name = "Cuir ancestral";
		Hue = 0X7f5;
		{
		}

}		[Constructable]
		public AncestralCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public AncestralCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}