using System;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0x1db9, 0x1dba )]
	public class LeatherCapAncestral : BaseArmor
	{
		public override int BasePhysicalResistance{ get{ return 9; } }
		public override int BaseFireResistance{ get{ return 8; } }
		public override int BaseColdResistance{ get{ return 8; } }
		public override int BasePoisonResistance{ get{ return 8; } }
		public override int BaseEnergyResistance{ get{ return 8; } }

		public override int InitMinHits{ get{ return 30; } }
		public override int InitMaxHits{ get{ return 40; } }

		public override int AosStrReq{ get{ return 20; } }
		public override int OldStrReq{ get{ return 15; } }

		public override int ArmorBase{ get{ return 13; } }
		public override int AosNivReq { get { return 21; } }

        public override ArmorMaterialType MaterialType { get { return ArmorMaterialType.Ancestral; } }
		

		public override ArmorMeditationAllowance DefMedAllowance{ get{ return ArmorMeditationAllowance.All; } }

		[Constructable]
		public LeatherCapAncestral() : base( 0x1DB9 )
		{
			Weight = 2.0;
			Name = "Cuir Ancestral";
		Hue = 0X7f5;
		}

		public LeatherCapAncestral( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

			if ( Weight == 1.0 )
				Weight = 2.0;
		}
	}
}