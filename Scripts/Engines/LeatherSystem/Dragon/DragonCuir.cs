using System;
using Server;

namespace Server.Items
{
	public class DragonCuir : Item
	{
		

		[Constructable]
		public DragonCuir() : this( 1 ){
                Name = "Cuir de Dragon";
		Hue = 0X457;
		{
		}

}		[Constructable]
		public DragonCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public DragonCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}