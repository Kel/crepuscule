using System;
using Server;

namespace Server.Items
{
	public class DragonPeau : Item
	{
		

		[Constructable]
		public DragonPeau() : this( 1 ){
                Name = "Peau De Dragon";
		Hue = 0X457;
		{
		}

}		[Constructable]
		public DragonPeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public DragonPeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}