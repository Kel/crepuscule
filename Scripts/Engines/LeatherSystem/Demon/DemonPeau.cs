using System;
using Server;

namespace Server.Items
{
	public class DemonPeau : Item
	{
		

		[Constructable]
		public DemonPeau() : this( 1 ){
                Name = "Peau De D�mon";
		Hue = 0X7f7;
		{
		}

}		[Constructable]
		public DemonPeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public DemonPeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}