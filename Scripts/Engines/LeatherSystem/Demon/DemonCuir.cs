using System;
using Server;

namespace Server.Items
{
	public class DemonCuir : Item
	{
		

		[Constructable]
		public DemonCuir() : this( 1 ){
                Name = "Cuir De D�mon";
		Hue = 0X7f7;
		{
		}

}		[Constructable]
		public DemonCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public DemonCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}