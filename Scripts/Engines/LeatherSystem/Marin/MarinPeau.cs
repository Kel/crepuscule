using System;
using Server;

namespace Server.Items
{
	public class MarinPeau : Item
	{
		

		[Constructable]
		public MarinPeau() : this( 1 ){
                Name = "Peau Marine";
		Hue = 0X7fe;
		{
		}

}		[Constructable]
		public MarinPeau( int amount ) : base( 0x1079 )
		{
			Stackable = false;
			Amount = amount;
		}

		public MarinPeau( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}