using System;
using Server;

namespace Server.Items
{
	public class MarinCuir : Item
	{
		

		[Constructable]
		public MarinCuir() : this( 1 ){
                Name = "Cuir Marin";
		Hue = 0X7FE;
		{
		}

}		[Constructable]
		public MarinCuir( int amount ) : base( 0x1081 )
		{
			Stackable = false;
			Amount = amount;
		}

		public MarinCuir( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}