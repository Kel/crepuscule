//////////////// GLOBAL INTERFACE SYSTEM ////////////////
///////////////////////// Gumps /////////////////////////
/////////////// GlobalInterfaceOptionsGump //////////////
///////////////////					  ///////////////////
//////////////////////			   //////////////////////
///////////////////////// V1.0 //////////////////////////       
/////////////////// Written By tobyjug //////////////////

using System;
using System.Collections;
using System.Reflection;
using System.IO;
using Server;
using Server.Prompts;
using Server.Gumps;
using Server.Network;
using Server.Items;
using Server.Multis;
using Server.Scripts.Commands;
using Server.Targeting;
using Server.Targets;

namespace Server.Gumps
{
	public class GlobalInterfaceOptionsGump : Gump
	{
		private Mobile m_from;
		private string m_SearchString;
		private int m_TotalPages;
		private int m_CurrentPage;
		private static Type m_SearchType;
		private string m_SearchPropertyName;
		private ArrayList m_SearchResults = new ArrayList();
		private string m_Option;
		private int m_ThisTotalPages;
		private int m_ThisCurrentPage;
		private ArrayList m_List = new ArrayList();
		private static PropertyInfo m_SearchProperty;
			
		public GlobalInterfaceOptionsGump( Mobile from, String SearchString, int TotalPages, int CurrentPage, Type SearchType, string SearchPropertyName, ArrayList SearchResults, string Option, int ThisTotalPages, int ThisCurrentPage, ArrayList List, PropertyInfo SearchProperty )
			: base( 0, 0 )
		{
			m_from = from;
			m_SearchString = SearchString;
			m_TotalPages = TotalPages;
			m_CurrentPage = CurrentPage;
			m_SearchType = SearchType;
			m_SearchResults = SearchResults;
			m_SearchPropertyName = SearchPropertyName;
			m_Option = Option;
			m_ThisTotalPages = ThisTotalPages;
			m_ThisCurrentPage = ThisCurrentPage;
			m_List = List;
			m_SearchProperty = SearchProperty;

			m_from.ClearTarget();			
			m_from.CloseGump( typeof( GlobalInterfaceItemGump ) );
			m_from.CloseGump( typeof( APropertiesGump ) );		
			
			this.Closable=true;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(3, 3, 477, 477, 2620);

			this.AddImage(449, 55, 2622);
			this.AddImage(14, 195, 2626);
			this.AddImage(449, 195, 2628);
			this.AddImage(14, 55, 2620);
			this.AddLabel(174, 13, 1152, @"GLOBAL INTERFACE");
			this.AddImageTiled(14, 58, 6, 141, 2623);
			this.AddImageTiled(34, 55, 429, 8, 2621);
			this.AddImageTiled(19, 86, 445, 8, 2621);
			this.AddImageTiled(464, 69, 5, 140, 2623);
			this.AddImageTiled(26, 206, 429, 7, 2621);
			this.AddAlphaRegion(18, 92, 446, 114);
			this.AddAlphaRegion(19, 62, 444, 23);

			this.AddImageTiled(14, 228, 6, 220, 2623);
			this.AddImageTiled(34, 225, 429, 8, 2621);
			this.AddImageTiled(464, 239, 5, 209, 2623);
			this.AddImageTiled(26, 441, 429, 7, 2621);
			this.AddImage(449, 225, 2622);
			this.AddImage(449, 430, 2628);
			this.AddImage(14, 225, 2620);
			this.AddImage(14, 430, 2626);
			this.AddLabel(178, 451, 1152, @"V1.0 By Tobyjug");
			this.AddAlphaRegion(18, 231, 446, 211);

			this.AddLabel(150, 63, 1152, @"Return to Main:");
			this.AddButton(270, 63, 4014, 4016, 1, GumpButtonType.Reply, 0);

			// Search Information
			if ( m_Option == "Type" )
			{
				this.AddLabel(165, 93, 1152, @"Search Type Information");
				this.AddLabel(130, 133, 196, @"Search Type Currently:");				
				this.AddLabel(280, 133, 61, @"" + m_SearchType.Name);
			}
			else if ( m_Option == "Property" )
			{			
				this.AddLabel(150, 93, 1152, @"Search Property Information");
				this.AddLabel(110, 133, 196, @"Search Property Currently:");				
				this.AddLabel(280, 133, 61, @"" + m_SearchPropertyName);
			}

			// Change Options
			if ( m_Option == "Type" )
			{
				this.AddLabel(170, 233, 1152, @"Search Type Options");
				
				m_ThisTotalPages = DisplayTypeOptions( this, m_SearchType, m_List, m_ThisTotalPages, m_ThisCurrentPage );
			}
			else if ( m_Option == "Property" )
			{			
				this.AddLabel(160, 233, 1152, @"Search Property Options");
				
				m_ThisTotalPages = DisplayPropertyOptions( this, m_SearchType, m_List, m_ThisTotalPages, m_ThisCurrentPage );
			}
	
			// 1 Or No Pages, No need for buttons
			if ( m_ThisTotalPages <= 1 )
			{
				this.AddImage(443, 236, 9702);
				this.AddImage(425, 236, 9706);
			}
			else
			{
				if ( m_ThisCurrentPage == m_ThisTotalPages )
				{
					this.AddImage(443, 236, 9702);
					this.AddButton(425, 236, 9706, 9707, 5, GumpButtonType.Reply, 0);								
				}
				else if ( m_ThisCurrentPage == 1 )
				{
					this.AddImage(425, 236, 9706);
					this.AddButton(443, 236, 9702, 9703, 6, GumpButtonType.Reply, 0);								
				}
				else
				{				
					this.AddButton(443, 236, 9702, 9703, 6, GumpButtonType.Reply, 0);
					this.AddButton(425, 236, 9706, 9707, 5, GumpButtonType.Reply, 0);				
				}
			}
			
			this.AddLabel(335, 233, 196, String.Format(@"Page {0} of {1}", m_ThisCurrentPage, m_ThisTotalPages) );						
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			Mobile from = state.Mobile;
			int buttonID = info.ButtonID;		

			switch ( buttonID )
			{
				// Return to Menu Button
				case 1:
				{
					from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName , m_SearchResults, m_SearchProperty ) );					
					break;
				}
				
				// Back Button
				case 5:
				{

					--m_ThisCurrentPage;
					
					from.SendGump( new GlobalInterfaceOptionsGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_Option, m_ThisTotalPages, m_ThisCurrentPage, m_List, m_SearchProperty ) );
					break;
				}				

				// Forward Button
				case 6:
				{
					++m_ThisCurrentPage;
					
					from.SendGump( new GlobalInterfaceOptionsGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_Option, m_ThisTotalPages, m_ThisCurrentPage, m_List, m_SearchProperty ) );
					break;
				}
				
				// Select 'Type Name' or select 'Item' Button
				case 49:
				{
					if( m_Option == "Property" )
					{
						m_SearchPropertyName = "Type Name";
						m_SearchProperty = null;
						from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
					}
					else if( m_Option == "Type" )
					{
						m_SearchType = typeof(Item);
						m_SearchPropertyName = "Type Name";
						m_SearchProperty = null;
						from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );					
					}
					
					break;					
				}
								
				// 'Select Property' or 'Select Type' Buttons
				default:
				{
					if( buttonID >= 50 )
					{
						int index = (buttonID - 50);
						if ( m_List[index] != null )
						{
							if( m_Option == "Property" )
							{
								m_SearchPropertyName = ((PropertyInfo)m_List[index]).Name;
								m_SearchProperty = ((PropertyInfo)m_List[index]);							
								from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
							}
							else if( m_Option == "Type" )
							{
								m_SearchType = ((Type)m_List[index]);
								m_SearchPropertyName = "Type Name";
								m_SearchProperty = null;							
								from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );							
							}
						}
						else
						{
							from.SendMessage( "That was not accessible" );
						}
					}
					break;
				}

			}	
		}
		
		public static int DisplayTypeOptions( Gump gump, Type SearchType, ArrayList List, int ThisTotalPages, int ThisCurrentPage )
		{
			// There are Types in the List
			if ( List.Count > 0 )
			{				
				// Works Out Number of Pages Needed.
				ThisTotalPages = (int)( (List.Count)/6 );
				if ( (ThisTotalPages * 6) < (List.Count) )
				{
					++ThisTotalPages;
				}
				//
				
				// SearchResults Can Fit on One Page
				if (ThisTotalPages == 0)
					ThisTotalPages = 1;
				
				// This calculates the start and end of the SearchResults for the Current Page.
				int start = ((ThisCurrentPage * 6) - 6);
				int end = ((ThisCurrentPage * 6));
				if ( end > List.Count )
				{
					end = List.Count;
				}												
				// Add Search Results to Columns
				int ypos = 280;
				
				if ( ThisCurrentPage == 1 )
				{
					gump.AddLabel(155, ypos, 196, "Item");				
					gump.AddButton(130, (ypos + 3), 5601, 5605, ( 49 ), GumpButtonType.Reply, 0);
					ypos = (ypos + 20);

				}
				
				// This loops through the SearchResults and adds the info to the Current Page.
				for ( int i = start; i < end; ++i )
				{
					string ThisType = ((Type)List[i]).Name;
					gump.AddLabel(155, ypos, 196, @"" + ThisType);
					gump.AddButton(130, (ypos + 3), 5601, 5605, ( 50 + i ), GumpButtonType.Reply, 0);						
								
					// Increment the y position.
					ypos = (ypos + 20);																				
				}
							
			}

			return ThisTotalPages;		
		}
		
		public static int DisplayPropertyOptions( Gump gump, Type SearchType, ArrayList List, int ThisTotalPages, int ThisCurrentPage )
		{			
			// There are Properties in the List
			if ( List.Count > 0 )
			{				
				// Works Out Number of Pages Needed.
				ThisTotalPages = (int)( (List.Count)/6 );
				if ( (ThisTotalPages * 6) < (List.Count) )
				{
					++ThisTotalPages;
				}
				//
				
				// SearchResults Can Fit on One Page
				if (ThisTotalPages == 0)
					ThisTotalPages = 1;
				
				// This calculates the start and end of the SearchResults for the Current Page.
				int start = ((ThisCurrentPage * 6) - 6);
				int end = ((ThisCurrentPage * 6));
				if ( end > List.Count )
				{
					end = List.Count;
				}												
				// Add Search Results to Columns
				int ypos = 280;
				
				if ( ThisCurrentPage == 1 )
				{
					gump.AddLabel(155, ypos, 196, "(String) Type Name");				
					gump.AddButton(130, (ypos + 3), 5601, 5605, ( 49 ), GumpButtonType.Reply, 0);
					ypos = (ypos + 20);

				}
				
				// This loops through the SearchResults and adds the info to the Current Page.
				for ( int i = start; i < end; ++i )
				{
					string PropertyType = ((PropertyInfo)List[i]).PropertyType.Name.ToString();
					string PropertyName = ((PropertyInfo)List[i]).Name as string;
					gump.AddLabel(155, ypos, 196, @"(" + PropertyType + ") " + PropertyName);
					gump.AddButton(130, (ypos + 3), 5601, 5605, ( 50 + i ), GumpButtonType.Reply, 0);						
								
					// Increment the y position.
					ypos = (ypos + 20);																				
				}
							
			}

			return ThisTotalPages;
				
		}					
	}
}