//////////////// GLOBAL INTERFACE SYSTEM ////////////////
///////////////////////// Gumps /////////////////////////
////////////////// GlobalInterfaceGump //////////////////
///////////////////					  ///////////////////
//////////////////////			   //////////////////////
///////////////////////// V1.0 //////////////////////////       
/////////////////// Written By tobyjug //////////////////

using System;
using System.Collections;
using System.Reflection;
using System.IO;
using Server;
using Server.Prompts;
using Server.Gumps;
using Server.Network;
using Server.Items;
using Server.Multis;
using Server.Engines;

namespace Server.Gumps
{
	public class GlobalInterfaceGump : Gump
	{
		private Mobile m_from;
		private string m_SearchString;
		private int m_TotalPages;
		private int m_CurrentPage;
		private static Type m_SearchType;
		private string m_SearchPropertyName;
		private ArrayList m_SearchResults = new ArrayList();
		private PropertyInfo m_SearchProperty;
			
		public GlobalInterfaceGump( Mobile from, String SearchString, int TotalPages, int CurrentPage, Type SearchType, string SearchPropertyName, ArrayList SearchResults, PropertyInfo SearchProperty )
			: base( 0, 0 )
		{
			m_from = from;
			m_SearchString = SearchString;
			m_TotalPages = TotalPages;
			m_CurrentPage = CurrentPage;
			m_SearchType = SearchType;
			m_SearchResults = SearchResults;
			m_SearchPropertyName = SearchPropertyName;
			m_SearchProperty = SearchProperty;
			
			m_from.ClearTarget();
			m_from.CloseGump( typeof( GlobalInterfaceGump ) );
			m_from.CloseGump( typeof( APropertiesGump ) );		
			
			this.Closable=true;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(3, 3, 477, 477, 2620);

			this.AddImage(449, 55, 2622);
			this.AddImage(14, 195, 2626);
			this.AddImage(449, 195, 2628);
			this.AddImage(14, 55, 2620);
			this.AddLabel(174, 13, 1152, @"GLOBAL INTERFACE");
			this.AddImageTiled(14, 58, 6, 141, 2623);
			this.AddImageTiled(34, 55, 429, 8, 2621);
			this.AddImageTiled(19, 86, 445, 8, 2621);
			this.AddImageTiled(464, 69, 5, 140, 2623);
			this.AddImageTiled(26, 206, 429, 7, 2621);
			this.AddAlphaRegion(18, 92, 446, 114);
			this.AddAlphaRegion(19, 62, 444, 23);
			this.AddImageTiled(134, 60, 5, 26, 2623);
			this.AddImageTiled(344, 59, 5, 26, 2623);
			this.AddImageTiled(14, 228, 6, 220, 2623);
			this.AddImageTiled(34, 225, 429, 8, 2621);
			this.AddImageTiled(464, 239, 5, 209, 2623);
			this.AddImageTiled(26, 441, 429, 7, 2621);
			this.AddImage(449, 225, 2622);
			this.AddImage(449, 430, 2628);
			this.AddImage(14, 225, 2620);
			this.AddImage(14, 430, 2626);
			this.AddLabel(178, 451, 1152, @"V1.0 By Tobyjug");
			this.AddAlphaRegion(18, 231, 446, 211);
			
			// Find out how many Pages to Display our Search Results.
			m_TotalPages = DisplayResults( m_from, this, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults );
			
			
			
			this.AddLabel(82, 63, 1152, @"Search:");
			this.AddTextEntry(142, 65, 200, 18, 37, 1, m_SearchString);
			this.AddButton(357, 63, 4023, 4024, 2, GumpButtonType.Reply, 0);

			this.AddLabel(190, 93, 1152, @"Search Options");
			this.AddLabel(37, 124, 196, @"Type:");
			this.AddLabel(77, 124, 61, @"" + m_SearchType.Name);
			this.AddButton(37, 151, 9702, 9703, 3, GumpButtonType.Reply, 0);
			this.AddLabel(61, 149, 61, @"Change Type");
			this.AddLabel(287, 124, 196, @"Property:");
			this.AddLabel(357, 124, 61, @"" + m_SearchPropertyName);			
			this.AddButton(287, 152, 9702, 9703, 4, GumpButtonType.Reply, 0);
			this.AddLabel(311, 150, 61, @"Change Property");			
			
			this.AddLabel(190, 233, 1152, @"Search Results");

			// 1 Or No Pages, No need for buttons
			if ( m_TotalPages <= 1 )
			{
				this.AddImage(443, 236, 9702);
				this.AddImage(425, 236, 9706);
			}
			else
			{
				if ( m_CurrentPage == m_TotalPages )
				{
					this.AddImage(443, 236, 9702);
					this.AddButton(425, 236, 9706, 9707, 5, GumpButtonType.Reply, 0);								
				}
				else if ( m_CurrentPage == 1 )
				{
					this.AddImage(425, 236, 9706);
					this.AddButton(443, 236, 9702, 9703, 6, GumpButtonType.Reply, 0);								
				}
				else
				{				
					this.AddButton(443, 236, 9702, 9703, 6, GumpButtonType.Reply, 0);
					this.AddButton(425, 236, 9706, 9707, 5, GumpButtonType.Reply, 0);				
				}
			}
							
			this.AddLabel(320, 233, 196, String.Format(@"Page {0} of {1}", m_CurrentPage, m_TotalPages) );

		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			Mobile from = state.Mobile;
			int buttonID = info.ButtonID;
			
			switch ( buttonID )
			{
				// Ok Button - Start Search
				case 2:
				{
					TextRelay relay = info.GetTextEntry( 1 );
					m_SearchString = relay.Text;
					m_CurrentPage = 1;					
					
					if ( m_SearchString.Length >= 3 )
					{
						m_SearchResults = DoSearch( m_SearchString, m_SearchResults, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchProperty );
					}

					
					from.SendGump( new GlobalInterfaceGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
					break;
				}

				// Change Type Button
				case 3:
				{
					ArrayList m_TypeList = new ArrayList();
					m_TypeList = GetTypeList( m_TypeList );				
					from.SendGump( new GlobalInterfaceOptionsGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, "Type", 1, 1, m_TypeList, m_SearchProperty ) );				
				
					break;
				}
				
				// Change Property Button
				case 4:
				{
					ArrayList m_PropertyList = new ArrayList();
					m_PropertyList = GetPropertyList( m_SearchType, m_PropertyList );
					from.SendGump( new GlobalInterfaceOptionsGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, "Property", 1, 1, m_PropertyList, m_SearchProperty ) );
					
					break;				
				}
				
				// Back Button
				case 5:
				{

					--m_CurrentPage;
					
					from.SendGump( new GlobalInterfaceGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
					break;
				}				

				// Forward Button
				case 6:
				{
					++m_CurrentPage;
					
					from.SendGump( new GlobalInterfaceGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
					break;
				}

				// 'More' Buttons
				default:
				{
					if( buttonID >= 50 )
					{
						int index = (buttonID - 50);
						if ( m_SearchResults[index] != null )
						{
							from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, index, m_SearchProperty ) );
						}
						else
						{
							from.SendMessage( "That Item is not accessible" );
						}
					}
					break;
				}
				
			}		
		}

		public static ArrayList DoSearch( string SearchString, ArrayList SearchResults, int TotalPages, int CurrentPage, Type SearchType, string SearchPropertyName, PropertyInfo SearchProperty )
		{			
			// Put our Input into lowercase.	
			SearchString = SearchString.ToLower();
			

			SearchResults = GetSearchResults( SearchString, SearchResults, TotalPages, CurrentPage, SearchType, SearchPropertyName, SearchProperty );
				
			return SearchResults;			
		}

		public static ArrayList GetSearchResults( string SearchString, ArrayList SearchResults, int TotalPages, int CurrentPage, Type SearchType, string SearchPropertyName, PropertyInfo SearchProperty )
		{
		
			SearchResults = new ArrayList();
			TotalPages = 1;
			
			// Loop through all the Items in the World.
			foreach ( Item item in World.Items.Values )
			{
				
				Type t = item.GetType();
				// Check If the Item Type is derived from our Search Type or is our Search Type.
				if ( (t.IsSubclassOf( SearchType )) || (t == SearchType) )
				{
					
					if ( SearchPropertyName == "Type Name" )
					{
						// Check If the Type Name contains our Input.
						if( t.Name.ToLower().IndexOf( SearchString ) >= 0 )					
						{
							// The Item matches our criteria, add it to the results.
							SearchResults.Add( item );
						}
					}
					
					else
					{
						try
						{
							// Get the value of our chosen Search Property
							string value = (SearchProperty.GetValue( item, null )).ToString();
							// Check If the Property value contains our Input.							
							if( value.ToLower().IndexOf( SearchString ) >= 0 )
							{
								// The Item matches our criteria, add it to the results.
								SearchResults.Add( item );
							}
						}
						catch
						{
							//Console.WriteLine( "failed" );
						}

					}

				}
				
			}
			
			return SearchResults;			
			
		}

		public static int DisplayResults( Mobile from, Gump gump, string SearchString, int TotalPages, int CurrentPage, Type SearchType, string SearchPropertyName, ArrayList SearchResults )
		{
			TotalPages = 1;
			
			// There is no input.
			if ( SearchString == "" )
			{
				gump.AddLabel(50, 290, 1152, @"No results to display");			
			}
			
			// Input is too short
			else if ( SearchString.Length < 3 )
			{				
				gump.AddLabel(50, 290, 1152, @"Invalid search string ( Must be 3 Chars or more )");
			}
			
			// Nothing matched the Input
			else if ( SearchResults.Count <= 0 )
			{
				gump.AddLabel(50, 290, 1152, @"Nothing matched your search terms");				
				gump.AddLabel(154, 253, 196, @"Objects Found: 0");
			}
			
			// Matches were found for the Input. Display Them.
			else if ( SearchResults.Count > 0 )
			{				
				// Works Out Number of Pages Needed.
				TotalPages = (int)( (SearchResults.Count)/6 );
				if ( (TotalPages * 6) < (SearchResults.Count) )
				{
					++TotalPages;
				}
				//
				
				// Prevents crashing if Items are deleted whilst using the Gump.
				// CurrentPage is reset back to 1.
				if ( CurrentPage > TotalPages )
				{
					CurrentPage = 1;
				}
				
				// SearchResults Can Fit on One Page
				if (TotalPages == 0)
					TotalPages = 1;
				
				// Put Number of Results at Top
				gump.AddLabel(154, 253, 196, @"Objects Found: " + SearchResults.Count);				
					

				// Add the Column Headers
				gump.AddLabel(155, 280, 42, @"Serial");
				gump.AddLabel(255, 280, 42, @"Location");
				gump.AddLabel(430, 280, 42, @"More");
				// This calculates the start and end of the SearchResults for the Current Page.
				int start = ((CurrentPage * 6) - 6);
				int end = ((CurrentPage * 6));
				if ( end > SearchResults.Count )
				{
					end = SearchResults.Count;
				}												
				// Add Search Results to Columns
				int ypos = 300;
				// This loops through the SearchResults and adds the info to the Current Page.
				for ( int i = start; i < end; ++i )
				{
					Serial Serial = ((Item)SearchResults[i]).Serial;
					int x = ((Item)SearchResults[i]).X;
					int y = ((Item)SearchResults[i]).Y;
					int z = ((Item)SearchResults[i]).Z;
					string Location = ((Item)SearchResults[i]).Map + " " + String.Format("({0}, {1}, {2})", x, y, z);

					gump.AddLabel(155, ypos, 1152, @"" + Serial);
					gump.AddLabel(255, ypos, 1152, @"" + Location);
					gump.AddButton(444, (ypos + 3), 5603, 5607, ( 50 + i ), GumpButtonType.Reply, 0);						
								
					// Increment the y position.
					ypos = (ypos + 20);																				
				}
				ypos = 300;							

				DisplayResultsByTypeName( gump, CurrentPage, SearchResults, ypos, start, end );
							
			}
			
			return TotalPages;		
		}

		public static void DisplayResultsByTypeName( Gump gump, int CurrentPage, ArrayList SearchResults, int ypos, int start, int end )
		{

			// Add the Column Header
			gump.AddLabel(35, 280, 42, @"Type Name");
					
			// This loops through the SearchResults and adds the info to the Current Page.
			for ( int i = start; i < end; ++i )
			{
				string TypeName = SearchResults[i].GetType().Name;
				// If the Name is greater than 16 chars long it is shortened to the first 16.
				if ( TypeName.Length > 16 )
				{
					TypeName = TypeName.Substring(0, 16);
					gump.AddLabel(35, ypos, 1152, @"" + TypeName + "...");
				}
				else
				{
					gump.AddLabel(35, ypos, 1152, @"" + TypeName);
				}						
						
				// Increment the y position.
				ypos = (ypos + 20);																				
			}
		
		}

		public static ArrayList GetPropertyList( Type SearchType, ArrayList PropertyList )
		{				
			
			PropertyInfo[] props = SearchType.GetProperties();
			Item item = null;
			
			try
			{
                item = GameItemType.CreateItem(SearchType);
			}
			catch
			{

			}		
			
			for ( int i = 0; i < props.Length; i++ ) 
			{ 
				string test;
				try
				{
					test = (props[i].GetValue( item, null )) as string;
					// Success
					PropertyList.Add( props[i] );
					
				}
				catch
				{
					// Failure									
				}
								
			}
			if (item != null)
			{
				item.Delete();
			}

			PropertyList.Sort( new PropertyNameComparer() );			
			
			return PropertyList;		
		
		}

		public static ArrayList GetTypeList( ArrayList TypeList )
		{				
			Type[] types;
			Type typeofItem = typeof( Item );			
			
			/////
			Assembly[] asms = ScriptCompiler.Assemblies;
			for ( int i = 0; i < asms.Length; ++i )
			{
				types = ScriptCompiler.GetTypeCache( asms[i] ).Types;
				for ( int j = 0; j < types.Length; ++j )
				{
					Type t = types[j];
	
					if ( typeofItem.IsAssignableFrom( t ) && !TypeList.Contains( t ) )
					{
						ConstructorInfo[] ctors = t.GetConstructors();
	
						for ( int k = 0; k < ctors.Length; ++k )
						{
							if ( ctors[k].GetParameters().Length == 0 && ctors[k].IsDefined( typeof( ConstructableAttribute ), false ) )
							{
								TypeList.Add( t );
								break;
							}
						}
					}
				}
			}
			/////
			
			/////
			types = ScriptCompiler.GetTypeCache( Core.Assembly ).Types;
			for ( int i = 0; i < types.Length; ++i )
			{
				Type t = types[i];
	
				if ( typeofItem.IsAssignableFrom( t ) && !TypeList.Contains( t ) )
				{
					ConstructorInfo[] ctors = t.GetConstructors();
	
					for ( int j = 0; j < ctors.Length; ++j )
					{
						if ( ctors[j].GetParameters().Length == 0 && ctors[j].IsDefined( typeof( ConstructableAttribute ), false ) )
						{
							TypeList.Add( t );
							break;
						}
					}
				}
			}
			/////
			
			TypeList.Sort( new TypeNameComparer() );
									
			return TypeList;			
								
		}

		private class TypeNameComparer : IComparer
		{
			public int Compare( object x, object y )
			{
				Type a = x as Type;
				Type b = y as Type;

				return a.Name.CompareTo( b.Name );
			}
		}

		private class PropertyNameComparer : IComparer
		{
			public int Compare( object x, object y )
			{
				PropertyInfo a = x as PropertyInfo;
				PropertyInfo b = y as PropertyInfo;

				return a.Name.CompareTo( b.Name );
			}
		}

	}
}