//////////////// GLOBAL INTERFACE SYSTEM ////////////////
///////////////////////// Gumps /////////////////////////
//////////////// GlobalInterfaceItemGump ////////////////
///////////////////					  ///////////////////
//////////////////////			   //////////////////////
///////////////////////// V1.0 //////////////////////////       
/////////////////// Written By tobyjug //////////////////

using System;
using System.Collections;
using System.Reflection;
using System.IO;
using Server;
using Server.Prompts;
using Server.Gumps;
using Server.Network;
using Server.Items;
using Server.Multis;
using Server.Scripts.Commands;
using Server.Targeting;
using Server.Targets;

namespace Server.Gumps
{
	public class GlobalInterfaceItemGump : Gump
	{
		private Mobile m_from;
		private string m_SearchString;
		private int m_TotalPages;
		private int m_CurrentPage;
		private static Type m_SearchType;
		private string m_SearchPropertyName;
		private ArrayList m_SearchResults = new ArrayList();
		private int m_IndexNumber;
		private PropertyInfo m_SearchProperty;
			
		public GlobalInterfaceItemGump( Mobile from, String SearchString, int TotalPages, int CurrentPage, Type SearchType, string SearchPropertyName, ArrayList SearchResults, int IndexNumber, PropertyInfo SearchProperty )
			: base( 0, 0 )
		{
			m_from = from;
			m_SearchString = SearchString;
			m_TotalPages = TotalPages;
			m_CurrentPage = CurrentPage;
			m_SearchType = SearchType;
			m_SearchResults = SearchResults;
			m_SearchPropertyName = SearchPropertyName;
			m_IndexNumber = IndexNumber;

			m_from.ClearTarget();			
			m_from.CloseGump( typeof( GlobalInterfaceItemGump ) );
			m_from.CloseGump( typeof( APropertiesGump ) );		
			
			this.Closable=true;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(3, 3, 477, 477, 2620);

			this.AddImage(449, 55, 2622);
			this.AddImage(14, 195, 2626);
			this.AddImage(449, 195, 2628);
			this.AddImage(14, 55, 2620);
			this.AddLabel(174, 13, 1152, @"GLOBAL INTERFACE");
			this.AddImageTiled(14, 58, 6, 141, 2623);
			this.AddImageTiled(34, 55, 429, 8, 2621);
			this.AddImageTiled(19, 86, 445, 8, 2621);
			this.AddImageTiled(464, 69, 5, 140, 2623);
			this.AddImageTiled(26, 206, 429, 7, 2621);
			this.AddAlphaRegion(18, 92, 446, 114);
			this.AddAlphaRegion(19, 62, 444, 23);

			this.AddImageTiled(14, 228, 6, 220, 2623);
			this.AddImageTiled(34, 225, 429, 8, 2621);
			this.AddImageTiled(464, 239, 5, 209, 2623);
			this.AddImageTiled(26, 441, 429, 7, 2621);
			this.AddImage(449, 225, 2622);
			this.AddImage(449, 430, 2628);
			this.AddImage(14, 225, 2620);
			this.AddImage(14, 430, 2626);
			this.AddLabel(178, 451, 1152, @"V1.0 By Tobyjug");
			this.AddAlphaRegion(18, 231, 446, 211);

			this.AddLabel(150, 63, 1152, @"Return to Main:");
			this.AddButton(270, 63, 4014, 4016, 1, GumpButtonType.Reply, 0);

			// Item Information
			this.AddLabel(180, 93, 1152, @"Item Information");			
			DisplayItemInformation( this, m_SearchResults, m_IndexNumber, m_SearchPropertyName );

			// Item Options
			this.AddLabel(192, 233, 1152, @"Item Options");			
			DisplayItemOptions( this );			
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			Mobile from = state.Mobile;
			int buttonID = info.ButtonID;
			
			// Return to Menu Button
			if ( buttonID == 1 )
			{
				from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
					return;
			}

			Item m_Item = ((Item)m_SearchResults[m_IndexNumber]);
			
			if ( buttonID > 1 )
			{
				if ( !BaseCommand.IsAccessible( m_from, m_Item ) )
				{
					m_from.SendMessage( "That Item is not accessible" );
					return;				
				}
			}		

			switch ( buttonID )
			{
				
				// Properties
				case 2:
				{
					from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_IndexNumber, m_SearchProperty ) );
					from.SendGump( new APropertiesGump( from, m_Item ) );				
					break;
				}

				// Delete
				case 3:
				{
					CommandLogging.WriteLine( from, "{0} {1} deleting {2}", from.AccessLevel, CommandLogging.Format( from ), CommandLogging.Format( m_Item ) );
					m_Item.Delete();
					from.SendMessage( "Item Deleted" );
					
					// Redo our Search Results
					m_SearchResults = GlobalInterfaceGump.DoSearch( m_SearchString, m_SearchResults, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchProperty );
					from.SendGump( new GlobalInterfaceGump( m_from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_SearchProperty ) );
									
					break;
				}

				// Go to Item
				case 4:
				{
					from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_IndexNumber, m_SearchProperty ) );
					from.Map = m_Item.Map;
					InvokeCommand( String.Format( "Go {0}", m_Item.Serial.Value ) );				
					break;
				}

				// Move Item to target				
				case 5:
				{
					from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_IndexNumber, m_SearchProperty ) );					
					from.Target = new MoveTarget( m_Item );				
					break;
				}

				// Bring Item to pack				
				case 6:
				{
					Mobile owner = m_Item.RootParent as Mobile;

					if ( owner != null && (owner.Map != null && owner.Map != Map.Internal) && !from.CanSee( owner ) )
					{
						from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_IndexNumber, m_SearchProperty ) );
						from.SendMessage( "You can not get what you can not see." );
					}
					else if ( owner != null && (owner.Map == null || owner.Map == Map.Internal) && owner.Hidden && owner.AccessLevel >= from.AccessLevel )
					{
						from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_IndexNumber, m_SearchProperty ) );
						from.SendMessage( "You can not get what you can not see." );
					}
					else
					{
						from.SendGump( new GlobalInterfaceItemGump( from, m_SearchString, m_TotalPages, m_CurrentPage, m_SearchType, m_SearchPropertyName, m_SearchResults, m_IndexNumber, m_SearchProperty ) );
						from.AddToBackpack( m_Item );
						from.SendMessage( "Item has been moved to your pack" );
					}				
					break;
				}				
			}		
		}

		public static void DisplayItemOptions( Gump gump )
		{
			// Properties Button
			gump.AddButton(155, 280, 9702, 9703, 2, GumpButtonType.Reply, 0);
			gump.AddLabel(180, 278, 196, @"Properties");
			
			// Delete Button
			gump.AddButton(155, 305, 9702, 9703, 3, GumpButtonType.Reply, 0);
			gump.AddLabel(180, 303, 196, @"Delete");						

			// Go to the Item Button
			gump.AddButton(155, 330, 9702, 9703, 4, GumpButtonType.Reply, 0);
			gump.AddLabel(180, 328, 196, @"Go to the Item");			

			// Move Item to target Button
			gump.AddButton(155, 355, 9702, 9703, 5, GumpButtonType.Reply, 0);
			gump.AddLabel(180, 353, 196, @"Move Item to target");

			// Bring Item to pack Button
			gump.AddButton(155, 380, 9702, 9703, 6, GumpButtonType.Reply, 0);
			gump.AddLabel(180, 378, 196, @"Bring Item to pack");
		}

		public static void DisplayItemInformation( Gump gump, ArrayList SearchResults, int IndexNumber, string SearchPropertyName )
		{
			// Add the Column Headers
			gump.AddLabel(155, 120, 42, @"Serial");
			gump.AddLabel(255, 120, 42, @"Location");

			Serial Serial = ((Item)SearchResults[IndexNumber]).Serial;
			int x = ((Item)SearchResults[IndexNumber]).X;
			int y = ((Item)SearchResults[IndexNumber]).Y;
			int z = ((Item)SearchResults[IndexNumber]).Z;
			string Location = ((Item)SearchResults[IndexNumber]).Map + " " + String.Format("({0}, {1}, {2})", x, y, z);
			gump.AddLabel(155, 140, 1152, @"" + Serial);
			gump.AddLabel(255, 140, 1152, @"" + Location);

			switch( SearchPropertyName )
			{
				case "Type Name":
				{
					DisplayResultsByTypeName( gump, SearchResults, IndexNumber );
					break;						
				}
			}																								
		}

		public static void DisplayResultsByTypeName( Gump gump, ArrayList SearchResults, int IndexNumber )
		{
			// Add the Column Header
			gump.AddLabel(35, 120, 42, @"Type Name");

			string TypeName = SearchResults[IndexNumber].GetType().Name;
			// If the Name is greater than 16 chars long it is shortened to the first 16.
			if ( TypeName.Length > 16 )
			{
				TypeName = TypeName.Substring(0, 16);
				gump.AddLabel(35, 140, 1152, @"" + TypeName + "...");
			}
			else
			{
				gump.AddLabel(35, 140, 1152, @"" + TypeName);
			}																								
		}

		private void InvokeCommand( string ip )
		{
			Server.Commands.Handle( m_from, String.Format( "{0}{1}", Server.Commands.CommandPrefix, ip ) );
		}
		
	}
}