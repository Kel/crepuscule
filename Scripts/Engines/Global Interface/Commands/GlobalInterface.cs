//////////////// GLOBAL INTERFACE SYSTEM ////////////////
/////////////////////// Commands ////////////////////////
//////////////////// GlobalInterface ////////////////////
///////////////////					  ///////////////////
//////////////////////			   //////////////////////
///////////////////////// V1.0 //////////////////////////       
/////////////////// Written By tobyjug //////////////////

using System;
using System.Collections;
using System.Reflection;
using System.IO;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Multis;
using Server.Gumps;
using Server.Network;

namespace Server.Scripts.Commands
{
	public class GlobalInterface
	{
		public static void Initialize()
		{
			Server.Commands.Register( "GlobalInterface", AccessLevel.GameMaster, new CommandEventHandler( GlobalInterface_OnCommand ) );
		}

		[Usage( "GlobalInterface" )]
		[Description( "Displays the Global Interface" )]
		private static void GlobalInterface_OnCommand( CommandEventArgs e )
		{
			e.Mobile.CloseGump( typeof( GlobalInterfaceGump ) );
			e.Mobile.CloseGump( typeof( GlobalInterfaceItemGump ) );
			e.Mobile.CloseGump( typeof( GlobalInterfaceOptionsGump ) );
			e.Mobile.CloseGump( typeof( APropertiesGump ) );
			e.Mobile.SendGump( new GlobalInterfaceGump( e.Mobile, "", 1, 1, typeof( Item ), "Type Name", null, null) );
			
		}
	}
}