////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "a flesh golem's corpse" )]
	public class LesserFleshGolem : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.BleedAttack;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public LesserFleshGolem() : this( false, 1.0 )
		{
		}

		[Constructable]
		public LesserFleshGolem( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "a lesser flesh golem";
			Body = 304;
			BaseSoundID = 684;

			SetStr( (int)(88*scalar), (int)(100*scalar) );
			SetDex( (int)(25*scalar), (int)(37*scalar) );
			SetInt( (int)(23*scalar), (int)(35*scalar) );

			SetHits( (int)(53*scalar), (int)(60*scalar) );

			SetDamage( (int)(9*scalar), (int)(11*scalar) );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, (int)(50*scalar), (int)(60*scalar) );
			SetResistance( ResistanceType.Fire, (int)(0*scalar) );
			SetResistance( ResistanceType.Cold, (int)(15*scalar), (int)(25*scalar) );
			SetResistance( ResistanceType.Poison, (int)(60*scalar), (int)(70*scalar) );
			SetResistance( ResistanceType.Energy, (int)(30*scalar), (int)(40*scalar) );

			SetSkill( SkillName.MagicResist, (25.0*scalar), (37.0*scalar) );
			SetSkill( SkillName.Tactics, (27.0*scalar), (40.0*scalar) );
			SetSkill( SkillName.Wrestling, (30.0*scalar), (35.0*scalar) );

			if ( summoned )
			{
				Fame = 10;
				Karma = 10;
			}
			else
			{
				Fame = 600;
				Karma = -600;
			}

			VirtualArmor = 17;

			ControlSlots = 1;

			switch ( Utility.Random( 10 ))
			{
				case 0: PackItem( new Head() ); break;
				case 1: PackItem( new Torso() ); break;
				case 2: PackItem( new LeftArm() ); break;
				case 3: PackItem( new RightArm() ); break;
				case 4: PackItem( new LeftLeg() ); break;
				case 5: PackItem( new RightLeg() ); break;
				case 6: PackItem( new Brain() ); break;
				case 7: PackItem( new Heart() ); break;
				case 8: PackItem( new Liver() ); break;
				case 9: PackItem( new Entrails() ); break;
			}
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Greater; } }

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public LesserFleshGolem( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}