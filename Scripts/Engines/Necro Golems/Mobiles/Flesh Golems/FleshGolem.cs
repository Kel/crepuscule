////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Golem de Chair" )]
	public class FleshGolem : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.BleedAttack;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public FleshGolem() : this( false, 1.0 )
		{
		}

		[Constructable]
		public FleshGolem( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Petit Golem de Chair";
			Body = 304;
			BaseSoundID = 684;

			SetStr( (int)(176*scalar), (int)(200*scalar) );
			SetDex( (int)(51*scalar), (int)(75*scalar) );
			SetInt( (int)(46*scalar), (int)(70*scalar) );

			SetHits( (int)(106*scalar), (int)(120*scalar) );

			SetDamage( (int)(18*scalar), (int)(22*scalar) );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, (int)(50*scalar), (int)(60*scalar) );
			SetResistance( ResistanceType.Fire, (int)(0*scalar) );
			SetResistance( ResistanceType.Cold, (int)(15*scalar), (int)(25*scalar) );
			SetResistance( ResistanceType.Poison, (int)(60*scalar), (int)(70*scalar) );
			SetResistance( ResistanceType.Energy, (int)(30*scalar), (int)(40*scalar) );

			SetSkill( SkillName.MagicResist, (50.1*scalar), (75.0*scalar) );
			SetSkill( SkillName.Tactics, (55.1*scalar), (80.0*scalar) );
			SetSkill( SkillName.Wrestling, (60.1*scalar), (70.0*scalar) );

			if ( summoned )
			{
				Fame = 10;
				Karma = 10;
			}
			else
			{
				Fame = 1000;
				Karma = -1800;
			}

			VirtualArmor = 34;

			ControlSlots = 4;

			switch ( Utility.Random( 10 ))
			{
				case 0: PackItem( new Head() ); break;
				case 1: PackItem( new Torso() ); break;
				case 2: PackItem( new LeftArm() ); break;
				case 3: PackItem( new RightArm() ); break;
				case 4: PackItem( new LeftLeg() ); break;
				case 5: PackItem( new RightLeg() ); break;
				case 6: PackItem( new Brain() ); break;
				case 7: PackItem( new Heart() ); break;
				case 8: PackItem( new Liver() ); break;
				case 9: PackItem( new Entrails() ); break;
			}
		
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lesser; } }

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public FleshGolem( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}