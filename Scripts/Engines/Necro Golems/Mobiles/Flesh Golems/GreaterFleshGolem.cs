////////////////////
//   Created by   //
//     Willy     //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps De Golem de chair" )]
	public class GreaterFleshGolem : BaseCreature
	{

		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.BleedAttack;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public GreaterFleshGolem() : this( false, 1.0 )
		{
		}

		[Constructable]
		public GreaterFleshGolem( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Agressor, 10, 1, 0.4, 0.8 )
		{
			Name = "Golem de Chair";
			Body = 0x9B;
			BaseSoundID = 684;

			SetStr( (int)(250*scalar), (int)(400*scalar) );
			SetDex( (int)(90*scalar), (int)(150*scalar) );
			SetInt( (int)(92*scalar), (int)(140*scalar) );

			SetHits( (int)(212*scalar), (int)(240*scalar) );

			SetDamage( (int)(23*scalar), (int)(27*scalar) );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, (int)(55*scalar), (int)(70*scalar) );
			SetResistance( ResistanceType.Fire, (int)(0*scalar) );
			SetResistance( ResistanceType.Cold, (int)(30*scalar), (int)(50*scalar) );
			SetResistance( ResistanceType.Poison, (int)(60*scalar), (int)(70*scalar) );
			SetResistance( ResistanceType.Energy, (int)(35*scalar), (int)(45*scalar) );

			SetSkill( SkillName.MagicResist, (100.2*scalar), (150.0*scalar) );
			SetSkill( SkillName.Tactics, (90.2*scalar), (160.0*scalar) );
			SetSkill( SkillName.Wrestling, (90.2*scalar), (140.0*scalar) );
			SetSkill( SkillName.Anatomy, (80.1*scalar), (100.0*scalar) );

			if ( summoned )
			{
				Fame = 100;
				Karma = 100;
			}
			else
			{
				Fame = 2000;
				Karma = -2600;
			}


			ControlSlots = 4;

			VirtualArmor = 50;

			switch ( Utility.Random( 10 ))
			{
				case 0: PackItem( new Head() ); break;
				case 1: PackItem( new Torso() ); break;
				case 2: PackItem( new LeftArm() ); break;
				case 3: PackItem( new RightArm() ); break;
				case 4: PackItem( new LeftLeg() ); break;
				case 5: PackItem( new RightLeg() ); break;
				case 6: PackItem( new Brain() ); break;
				case 7: PackItem( new Heart() ); break;
				case 8: PackItem( new Liver() ); break;
				case 9: PackItem( new Entrails() ); break;
			}
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool CanRummageCorpses{ get{ return true; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lethal; } }
		public override Poison HitPoison{ get{ return Poison.Lesser; } }

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public GreaterFleshGolem( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}