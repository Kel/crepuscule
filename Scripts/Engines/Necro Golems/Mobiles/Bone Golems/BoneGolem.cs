////////////////////
//   Created by   //
//     Willy      //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Golem D'os" )]
	public class BoneGolem : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.ArmorIgnore;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public BoneGolem() : this( false, 1.0 )
		{
		}

		[Constructable]
		public BoneGolem( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Golem d'os";
			Body = 308;
			BaseSoundID = 0x48D;

			SetStr( (int)(196*scalar), (int)(250*scalar) );
			SetDex( (int)(76*scalar), (int)(95*scalar) );
			SetInt( (int)(36*scalar), (int)(60*scalar) );

			SetHits( (int)(118*scalar), (int)(150*scalar) );

			SetDamage( (int)(8*scalar), (int)(18*scalar) );

			SetDamageType( ResistanceType.Physical, 100 );

			SetResistance( ResistanceType.Physical, (int)(35*scalar), (int)(45*scalar) );
			SetResistance( ResistanceType.Fire, (int)(20*scalar), (int)(30*scalar) );
			SetResistance( ResistanceType.Cold, (int)(50*scalar), (int)(60*scalar) );
			SetResistance( ResistanceType.Poison, (int)(20*scalar), (int)(30*scalar) );
			SetResistance( ResistanceType.Energy, (int)(30*scalar), (int)(40*scalar) );

			SetSkill( SkillName.MagicResist, (65.1*scalar), (80.0*scalar) );
			SetSkill( SkillName.Tactics, (85.1*scalar), (100.0*scalar) );
			SetSkill( SkillName.Wrestling, (85.1*scalar), (95.0*scalar) );
			SetSkill( SkillName.Anatomy, (55.1*scalar), (80.0*scalar) );

			if ( summoned )
			{
				Fame = 100;
				Karma = 100;
			}
			else
			{
				Fame = 100;
				Karma = 100;
			}

			VirtualArmor = 40;
			ControlSlots = 3;

			switch ( Utility.Random( 5 ))
			{
				case 0: PackItem( new BoneGolemHead() ); break;
				case 1: PackItem( new BoneGolemArm() ); break;
				case 2: PackItem( new BoneGolemLeg() ); break;
				case 3: PackItem( new BoneGolemHands() ); break;
				case 4: PackItem( new BoneGolemPelvis() ); break;
			}
		
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lethal; } }
		public override bool CanRummageCorpses{ get{ return true; } }


		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public BoneGolem( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}