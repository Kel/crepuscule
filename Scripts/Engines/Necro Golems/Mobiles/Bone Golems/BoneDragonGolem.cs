using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Dragon d'os" )]
	public class BoneDragonGolem : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.ArmorIgnore;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public BoneDragonGolem() : this( false, 1.0 )
		{
		}

		[Constructable]
		public BoneDragonGolem( bool summoned, double scalar ) : base( AIType.AI_Mage, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Dragon D'os";
			Body = 104;
			BaseSoundID = 0x488;

			SetStr( 449, 515 );
			SetDex( 34, 100 );
			SetInt( 244, 310 );

			SetHits( 279, 299 );

			SetDamage( 29, 35 );

			SetDamageType( ResistanceType.Physical, 75 );
			SetDamageType( ResistanceType.Fire, 25 );

			SetResistance( ResistanceType.Physical, 75, 80 );
			SetResistance( ResistanceType.Fire, 40, 60 );
			SetResistance( ResistanceType.Cold, 40, 60 );
			SetResistance( ResistanceType.Poison, 70, 80 );
			SetResistance( ResistanceType.Energy, 40, 60 );

			SetSkill( SkillName.EvalInt, 80.1, 100.0 );
			SetSkill( SkillName.Magery, 80.1, 100.0 );
			SetSkill( SkillName.MagicResist, 100.3, 130.0 );
			SetSkill( SkillName.Tactics, 97.6, 100.0 );
			SetSkill( SkillName.Wrestling, 97.6, 100.0 );

			Fame = 11000;
			Karma = -11000;
			VirtualArmor = 80;
			ControlSlots = 5;

			switch ( Utility.Random( 10 ))
			{
				case 0: PackItem( new Head() ); break;
				case 1: PackItem( new Torso() ); break;
				case 2: PackItem( new LeftArm() ); break;
				case 3: PackItem( new RightArm() ); break;
				case 4: PackItem( new LeftLeg() ); break;
				case 5: PackItem( new RightLeg() ); break;
				case 6: PackItem( new Brain() ); break;
				case 7: PackItem( new Heart() ); break;
				case 8: PackItem( new Liver() ); break;
				case 9: PackItem( new Entrails() ); break;
			}
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool HasBreath{ get{ return true; } } // breath attack enabled
		public override int BreathFireDamage{ get{ return 0; } }
		public override int BreathColdDamage{ get{ return 100; } }
		public override int BreathEffectHue{ get{ return 0x480; } }
		public override bool AutoDispel{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lethal; } }

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public BoneDragonGolem( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}