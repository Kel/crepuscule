﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;

namespace Server.Mobiles
{
    class FilletteMaudite : Zombie
    {
        [Constructable]
        public FilletteMaudite()
        {
            Name = "Fillette maudite";
            Body = 401;
            Hue = RandomBodyHue();

            AddItem(new Sandals(RandomSandalHue()));
            AddItem(new PlainDress(RandomDressHue()));

            Shirt s = new Shirt();
            s.ItemID = 12401;

            AddItem(new Shirt(0));

            switch (Utility.Random(3))
            {
                case 0: AddItem(new LongHair(RandomHairHue())); break;
                case 1: AddItem(new PonyTail(RandomHairHue())); break;
                case 2: AddItem(new TwoPigTails(RandomHairHue())); break;
            }
        }

        public FilletteMaudite(Serial serial)
            : base(serial)
		{
		}

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }

        private int RandomHairHue()
        {
            switch (Utility.Random(4))
            {
                case 0: return 1148;
                case 1: return 1119;
                case 2: return 1133;
                case 3: return 1108;
            }
            return 0;
        }

        private int RandomBodyHue()
        {
            return 33814 + Utility.Random(2);
        }

        private int RandomDressHue()
        {
            switch (Utility.Random(6))
            {
                case 0: return 1712;
                case 1: return 1454;
                case 2: return 1806;
                case 3: return 1744;
                case 4: return 1106;
                case 5: return 1723;
            }
            return 0;
        }

        private int RandomSandalHue()
        {
            return 0;
        }
    }
}
