////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
	public class NecroGolemBook: CrepusculeItem
	{
		[Constructable]
		public NecroGolemBook() : base( 0x2253 )
		{
			Name = "Th�ories sur les gol�ms noirs.";
			Weight = 1.0;
		}

		public NecroGolemBook( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override void OnDoubleClick( Mobile from )
		{
			PlayerMobile pm = from as PlayerMobile;

			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
			}
			else if ( pm == null || from.Skills[SkillName.Necromancy].Base < 70.0 )
			{
				pm.SendMessage( "Uniquement un n�cromancien peux le lire." );
			}
			else if ( pm.NecroGolem )
			{
				pm.SendMessage( "Cela n'apportera rien de nouveau." );
			}
			else
			{
				pm.NecroGolem = true;
				pm.SendMessage( "Vous avez appris une science noire" );
				Delete();
			}
		}
	}
}