using System;
using Server;

namespace Server.Items
{
	public class GemofPower: CrepusculeItem
	{
		[Constructable]
		public GemofPower() : base( 0x1869 )
		{
			Name = "Gemme de pouvoir";
			Weight = 0.5;
		}

        public GemofPower(int count)
            : base(){}

		public GemofPower( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}