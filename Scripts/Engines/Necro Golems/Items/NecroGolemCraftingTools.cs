using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{	
	public class NecroGolemCraftingTools : BaseTool
	{

		[Constructable]
		public NecroGolemCraftingTools() : base( 0xF9D )
		{
			Weight = 2.0;
			Name = "Outils de cr�ation des n�crogolems.";
		}

		[Constructable]
		public NecroGolemCraftingTools( int uses ) : base( uses, 0xF9D )
		{
			Weight = 2.0;
			Name = "Outils de cr�ation des n�crogolems.";
		}

		public NecroGolemCraftingTools( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}