////////////////////
//   Created by   //
//     Willy      //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Mage" )]
	public class MageSQ : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.Disarm;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public MageSQ() : this( false, 1.0 )
		{
		}

		[Constructable]
		public MageSQ( bool summoned, double scalar ) : base( AIType.AI_Mage, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Mage Squelettique";
			Body = 148;
			BaseSoundID = 0x48D;

			SetStr( (int)(145*scalar), (int)(160*scalar) );
			SetDex( (int)(68*scalar), (int)(80*scalar) );
			SetInt( (int)(250*scalar), (int)(600*scalar) );

			SetHits( (int)(65*scalar), (int)(145*scalar) );

			SetDamage( (int)(22*scalar), (int)(39*scalar) );

			SetDamageType( ResistanceType.Physical, 70 );

			SetResistance( ResistanceType.Physical, (int)(40*scalar), (int)(40*scalar) );
			SetResistance( ResistanceType.Fire, (int)(30*scalar), (int)(62*scalar) );
			SetResistance( ResistanceType.Cold, (int)(30*scalar), (int)(93*scalar) );
			SetResistance( ResistanceType.Poison, (int)(30*scalar), (int)(100*scalar) );
			SetResistance( ResistanceType.Energy, (int)(30*scalar), (int)(64*scalar) );

			SetSkill( SkillName.MagicResist, (65.1*scalar), (80.0*scalar) );
			SetSkill( SkillName.EvalInt, (25.0*scalar), (75.0*scalar) );
			SetSkill( SkillName.Magery, (60.0*scalar), (80.0*scalar) );
			SetSkill( SkillName.Anatomy, (18.0*scalar), (60.5*scalar) );

			if ( summoned )
			{
				Fame = 50;
				Karma = 50;
			}
			else
			{
				Fame = 50;
				Karma = 50;
			}

			VirtualArmor = 12;
			ControlSlots = 3;

			switch ( Utility.Random( 5 ))
			{
				case 0: PackItem( new MageSQChest() ); break;
				case 1: PackItem( new Heart() ); break;
				case 2: PackItem( new Brain() ); break;
				case 3: PackItem( new Celectrik() ); break;
				case 4: PackItem( new Ossature() ); break;
			}
		
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Regular; } }
		public override bool CanRummageCorpses{ get{ return false; } }


		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public MageSQ( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}