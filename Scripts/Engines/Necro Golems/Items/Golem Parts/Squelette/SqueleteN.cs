////////////////////
//   Created by   //
//     Willy      //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Squelette" )]
	public class SqueleteN : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.BleedAttack;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public SqueleteN() : this( false, 1.0 )
		{
		}

		[Constructable]
		public SqueleteN( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Squelette";
			Body = 56;
			BaseSoundID = 0x48D;

			SetStr( (int)(110*scalar), (int)(130*scalar) );
			SetDex( (int)(45*scalar), (int)(50*scalar) );
			SetInt( (int)(9*scalar), (int)(35*scalar) );

			SetHits( (int)(50*scalar), (int)(75*scalar) );

			SetDamage( (int)(10*scalar), (int)(22*scalar) );

			SetDamageType( ResistanceType.Physical, 35 );

			SetResistance( ResistanceType.Physical, (int)(23*scalar), (int)(29*scalar) );
			SetResistance( ResistanceType.Fire, (int)(25*scalar), (int)(52*scalar) );
			SetResistance( ResistanceType.Cold, (int)(82*scalar), (int)(93*scalar) );
			SetResistance( ResistanceType.Poison, (int)(80*scalar), (int)(170*scalar) );
			SetResistance( ResistanceType.Energy, (int)(12*scalar), (int)(34*scalar) );

			SetSkill( SkillName.MagicResist, (65.1*scalar), (80.0*scalar) );
			SetSkill( SkillName.Tactics, (45.0*scalar), (70.0*scalar) );
			SetSkill( SkillName.Wrestling, (43.0*scalar), (60.0*scalar) );
			SetSkill( SkillName.Anatomy, (13*scalar), (50.5*scalar) );

			if ( summoned )
			{
				Fame = 50;
				Karma = 50;
			}
			else
			{
				Fame = 50;
				Karma = 50;
			}

			VirtualArmor = 12;
			ControlSlots = 2;

			switch ( Utility.Random( 5 ))
			{
				case 0: PackItem( new SqueleteNChest() ); break;
				case 1: PackItem( new Heart() ); break;
				case 2: PackItem( new Brain() ); break;
				case 3: PackItem( new Celectrik() ); break;
				case 4: PackItem( new Ossature() ); break;
			}
		
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lesser; } }
		public override bool CanRummageCorpses{ get{ return false; } }


		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public SqueleteN( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}