////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;

namespace Server.Items
{
	public class Heart: CrepusculeItem
	{
		[Constructable]
		public Heart() : base( 7405 )
		{
			Weight = 0.5;
			Name = "un coeur";
		}

		public Heart( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}