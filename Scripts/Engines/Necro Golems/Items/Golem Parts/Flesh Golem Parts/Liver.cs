////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;

namespace Server.Items
{
	public class Liver: CrepusculeItem
	{
		[Constructable]
		public Liver() : base( 7406 )
		{
			Weight = 0.5;
			Name = "foie";
		}

		public Liver( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}