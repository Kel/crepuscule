////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;

namespace Server.Items
{
	public class Brain: CrepusculeItem
	{
		[Constructable]
		public Brain() : base( 7408 )
		{
			Weight = 0.5;
			Name = "cerveau";
		}

		public Brain( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}