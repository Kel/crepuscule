////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;
using Server.Mobiles;
using Server.Spells;

namespace Server.Items
{
	public class FleshGolemTorso: CrepusculeItem
	{
		[Constructable]
		public FleshGolemTorso() : base( 0x1D9F )
		{
			Weight = 15.0;
			Name = "Torse de golem de chair";
		}

		public FleshGolemTorso( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				return;
			}

			double necromancySkill = from.Skills[SkillName.Necromancy].Value;

			if ( necromancySkill < 80.0 )
			{
				from.SendMessage( "You do not have the knowledge required to attempt this." );
				return;
			}

			else if ( (from.Followers + 4) > from.FollowersMax )
			{
				from.SendLocalizedMessage( 1049607 ); // You have too many followers to control that creature.
				return;
			}

			double scalar;

			if ( necromancySkill >= 100.0 )
				scalar = 1.0;
			else if ( necromancySkill >= 95.0 )
				scalar = 0.9;
			else if ( necromancySkill >= 85.0 )
				scalar = 0.8;
			else
				scalar = 0.5;

			Container pack = from.Backpack;

			if ( pack == null )
				return;

			int res = pack.ConsumeTotal(
				new Type[]
				{
					typeof( LeftArm ),
					typeof( RightArm ),
					typeof( LeftLeg ),
					typeof( RightLeg ),
					typeof( Head ),
					typeof( Heart ),
					typeof( Brain ),
					typeof( Entrails ),
					typeof( Liver ),
					typeof( DaemonBlood ),
				},
				new int[]
				{
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1

				} );

			switch ( res )
			{
				case 0:
				{
					from.SendMessage( "Vous avez besoin d'un bras." );
					break;
				}
				case 1:
				{
					from.SendMessage( "Vous avez besoin d'un bras." );
					break;
				}
				case 2:
				{
					from.SendMessage( "Vous avez besoin d'une jambe." );
					break;
				}
				case 3:
				{
					from.SendMessage( "Vous avez besoin d'une jambe" );
					break;
				}
				case 4:
				{
					from.SendMessage( "Vous avez besoin d'une t�te." );
					break;
				}
				case 5:
				{
					from.SendMessage( "Vous avez besoin d'un coeur." );
					break;
				}
				case 6:
				{
					from.SendMessage( "Vous avez besoin d'un cerveau." );
					break;
				}
				case 7:
				{
					from.SendMessage( "Vous avez besoin des entailles cerveau." );
					break;
				}
				case 8:
				{
					from.SendMessage( "Vous avez besoin d'un foie cerveau." );
					break;
				}
				case 9:
				{
					from.SendMessage( "Vous avez besoin de 1 fiole du sang de d�mon." );
					break;
				}

				default:
				{
					FleshGolem m = new FleshGolem( true, scalar );

					if ( m.SetControlMaster( from ) )
					{
						Delete();

						m.MoveToWorld( from.Location, from.Map );
						from.PlaySound( 0x244 );
					}

					break;
				}
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}