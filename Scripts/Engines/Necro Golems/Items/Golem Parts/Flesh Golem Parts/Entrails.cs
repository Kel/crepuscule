////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;

namespace Server.Items
{
	public class Entrails: CrepusculeItem
	{
		[Constructable]
		public Entrails() : base( 7407 )
		{
			Weight = 0.5;
			Name = "entrailles";
		}

		public Entrails( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}