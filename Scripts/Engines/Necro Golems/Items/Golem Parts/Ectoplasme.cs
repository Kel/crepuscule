using System;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Items
{
	public class Ectoplasme: CrepusculeItem
	{
		[Constructable]
		public Ectoplasme() : this( 1 )
		{
		}
		
		[Constructable]
		public Ectoplasme( int amount ) : base( 0x1869 )
		{
			Name = "Ectoplasme";
                        Weight = 5.0;
			
			
		}

		public Ectoplasme( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Ectoplasme( amount ), amount );
		}
	}
}