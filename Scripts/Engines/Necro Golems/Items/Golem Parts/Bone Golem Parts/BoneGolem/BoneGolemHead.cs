////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneGolemHead: CrepusculeItem
	{
		[Constructable]
		public BoneGolemHead() : base( 0x1AE3 )
		{
			Name = "Gol�m d'os, t�te";
			Weight = 5.0;
			Hue = 2220;
		}

		public BoneGolemHead( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}