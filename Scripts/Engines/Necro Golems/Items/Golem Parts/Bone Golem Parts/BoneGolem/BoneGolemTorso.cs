////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;
using Server.Mobiles;
using Server.Spells;

namespace Server.Items
{
	public class BoneGolemTorso: CrepusculeItem
	{
		[Constructable]
		public BoneGolemTorso() : base( 0x1B17 )
		{
			Weight = 8.0;
			Name = "Golem d'os, torse";
			Hue = 2220;
		}

		public BoneGolemTorso( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				return;
			}

			double necromancySkill = from.Skills[SkillName.Necromancy].Value;

			if ( necromancySkill < 70.0 )
			{
				from.SendMessage( "You do not have the knowledge required to attempt this." );
				return;
			}
			else if ( (from.Followers + 2) > from.FollowersMax )
			{
				from.SendLocalizedMessage( 1049607 ); // You have too many followers to control that creature.
				return;
			}

			double scalar;

			if ( necromancySkill >= 80.0 )
				scalar = 1.0;
			else if ( necromancySkill >= 70.0 )
				scalar = 0.9;
			else if ( necromancySkill >= 60.0 )
				scalar = 0.8;
			else
				scalar = 0.5;

			Container pack = from.Backpack;

			if ( pack == null )
				return;

			int res = pack.ConsumeTotal(
				new Type[]
				{
					typeof( BoneArms ),
					typeof( BoneLegs ),
					typeof( BoneGloves ),
					typeof( BoneHelm ),
					typeof( BoneGolemPelvis )
				},
				new int[]
				{
					1,
					1,
					1,
					1,
					1

				} );

			switch ( res )
			{
				case 0:
				{
					from.SendMessage( "Vous avez besoin des bras." );
					break;
				}
				case 1:
				{
					from.SendMessage( "Vous avez besoin de jambes." );
					break;
				}
				case 2:
				{
					from.SendMessage( "Vous avez besoin des mains." );
					break;
				}
				case 3:
				{
					from.SendMessage( "Vous avez besoin d'une t�te." );
					break;
				}
				case 4:
				{
					from.SendMessage( "Vous avez besoin d'un bassin." );
					break;
				}

				default:
				{
					BoneGolem m = new BoneGolem( true, scalar );

					if ( m.SetControlMaster( from ) )
					{
						Delete();

						m.MoveToWorld( from.Location, from.Map );
						from.PlaySound( 0x244 );
					}

					break;
				}
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}