////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneGolemHands: CrepusculeItem
	{
		[Constructable]
		public BoneGolemHands() : base( 0x1450 )
		{
			Name = "Gol�m d'os, mains";
			Weight = 4.0;
			Hue = 2220;
		}

		public BoneGolemHands( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}