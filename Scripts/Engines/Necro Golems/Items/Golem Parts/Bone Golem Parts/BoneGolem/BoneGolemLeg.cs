////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneGolemLeg: CrepusculeItem
	{
		[Constructable]
		public BoneGolemLeg() : base( 0x1B12 )
		{
			Name = "Gol�m d'os, jambe";
			Weight = 12.0;
			Hue = 2220;
		}

		public BoneGolemLeg( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}