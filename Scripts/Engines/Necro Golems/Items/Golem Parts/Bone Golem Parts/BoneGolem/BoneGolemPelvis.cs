////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneGolemPelvis: CrepusculeItem
	{
		[Constructable]
		public BoneGolemPelvis() : base( 0x1B15 )
		{
			Name = "Bassin D'os";
			Weight = 12.0;
			Hue = 2220;
		}

		public BoneGolemPelvis( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}