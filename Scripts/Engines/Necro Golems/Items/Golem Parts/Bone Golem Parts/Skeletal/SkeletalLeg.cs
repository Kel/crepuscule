////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class SkeletalLeg: CrepusculeItem
	{
		[Constructable]
		public SkeletalLeg() : base( 0x1B12 )
		{
			Name = "jambe d'un squelette";
			Weight = 12.0;
		}

		public SkeletalLeg( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}