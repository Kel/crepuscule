////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class Ribcage: CrepusculeItem
	{
		[Constructable]
		public Ribcage() : base( 0x1B17 )
		{
			Name = "Cage Thoracique";
			Weight = 12.0;
		}

		public Ribcage( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}