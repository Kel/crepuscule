////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class SkeletalHands: CrepusculeItem
	{
		[Constructable]
		public SkeletalHands() : base( 0x1450 )
		{
			Name = "mains d'un squelette";
			Weight = 4.0;
		}

		public SkeletalHands( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}