////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class SkeletalArm: CrepusculeItem
	{
		[Constructable]
		public SkeletalArm() : base( 0x1B12 )
		{
			Name = "bras d'un squelette";
			Weight = 5.0;
		}

		public SkeletalArm( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}