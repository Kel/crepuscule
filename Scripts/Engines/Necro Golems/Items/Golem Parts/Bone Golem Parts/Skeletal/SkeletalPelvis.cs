////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class SkeletalPelvis: CrepusculeItem
	{
		[Constructable]
		public SkeletalPelvis() : base( 0x1B15 )
		{
			Name = "bassin d'un squelette";
			Weight = 12.0;
		}

		public SkeletalPelvis( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}