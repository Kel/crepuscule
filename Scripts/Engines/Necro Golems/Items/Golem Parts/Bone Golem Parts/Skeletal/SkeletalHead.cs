////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class SkeletalHead: CrepusculeItem
	{
		[Constructable]
		public SkeletalHead() : base( 0x1AE2 )
		{
			Name = "t�te d'un squelette";
			Weight = 5.0;
		}

		public SkeletalHead( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}