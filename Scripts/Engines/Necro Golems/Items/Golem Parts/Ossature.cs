using System;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Items
{
	public class Ossature: CrepusculeItem
	{
		[Constructable]
		public Ossature() : this( 1 )
		{
		}
		
		[Constructable]
		public Ossature( int amount ) : base( 0xed2 )
		{
			Name = "Ossature";
                        Weight = 5.0;
			
			
		}

		public Ossature( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Ossature( amount ), amount );
		}
	}
}