////////////////////
//   Created by   //
//     Willy      //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Wraith" )]
	public class WraithN : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.ArmorIgnore;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public WraithN() : this( false, 1.0 )
		{
		}

		[Constructable]
		public WraithN( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Wraith";
			Body = 26;
			BaseSoundID = 0x48D;

			SetStr( (int)(125*scalar), (int)(145*scalar) );
			SetDex( (int)(55*scalar), (int)(70*scalar) );
			SetInt( (int)(90*scalar), (int)(110*scalar) );

			SetHits( (int)(90*scalar), (int)(110*scalar) );

			SetDamage( (int)(15*scalar), (int)(30*scalar) );

			SetDamageType( ResistanceType.Physical, 40 );

			SetResistance( ResistanceType.Physical, (int)(10*scalar), (int)(40*scalar) );
			SetResistance( ResistanceType.Fire, (int)(10*scalar), (int)(52*scalar) );
			SetResistance( ResistanceType.Cold, (int)(10*scalar), (int)(93*scalar) );
			SetResistance( ResistanceType.Poison, (int)(10*scalar), (int)(170*scalar) );
			SetResistance( ResistanceType.Energy, (int)(2*scalar), (int)(34*scalar) );

			SetSkill( SkillName.MagicResist, (65.1*scalar), (80.0*scalar) );
			SetSkill( SkillName.Tactics, (55.0*scalar), (75.0*scalar) );
			SetSkill( SkillName.Wrestling, (49.0*scalar), (65.0*scalar) );
			SetSkill( SkillName.Anatomy, (18.0*scalar), (60.5*scalar) );

			if ( summoned )
			{
				Fame = 50;
				Karma = 50;
			}
			else
			{
				Fame = 50;
				Karma = 50;
			}

			VirtualArmor = 12;
			ControlSlots = 2;

			switch ( Utility.Random( 5 ))
			{
				case 0: PackItem( new SqueleteNChest() ); break;
				case 1: PackItem( new Heart() ); break;
				case 2: PackItem( new Brain() ); break;
				case 3: PackItem( new Celectrik() ); break;
				case 4: PackItem( new Ossature() ); break;
			}
		
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lesser; } }
		public override bool CanRummageCorpses{ get{ return false; } }


		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public WraithN( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}