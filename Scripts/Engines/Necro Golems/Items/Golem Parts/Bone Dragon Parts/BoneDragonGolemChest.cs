////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;
using Server.Mobiles;
using Server.Spells;

namespace Server.Items
{
	public class BoneDragonGolemChest: CrepusculeItem
	{
		[Constructable]
		public BoneDragonGolemChest() : base( 0x144F )
		{
			Weight = 95.0;
			Name = "Gol�m de dragon d'os, corps";
		}

		public BoneDragonGolemChest( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
				return;
			}

			double necromancySkill = from.Skills[SkillName.Necromancy].Value;

			if ( necromancySkill < 90.0 )
			{
				from.SendMessage( "You do not have the knowledge required to attempt this." );
				return;
			}
			else if ( (from.Followers + 5) > from.FollowersMax )
			{
				from.SendLocalizedMessage( 1049607 ); // You have too many followers to control that creature.
				return;
			}

			double scalar;

			if ( necromancySkill >= 100.0 )
				scalar = 1.0;
			else if ( necromancySkill >= 97.0 )
				scalar = 0.9;
			else if ( necromancySkill >= 95.0 )
				scalar = 0.8;
			else
				scalar = 0.5;

			Container pack = from.Backpack;

			if ( pack == null )
				return;

			int res = pack.ConsumeTotal(
				new Type[]
				{
					typeof( BoneDragonGolemClaw ),
					typeof( BoneDragonGolemHead ),
					typeof( BoneDragonGolemLeftWing ),
					typeof( BoneDragonGolemLeg ),
					typeof( BoneDragonGolemPelvis ),
					typeof( BoneDragonGolemRightWing ),
					typeof( BoneDragonGolemTail )
				},
				new int[]
				{
					1,
					1,
					1,
					1,
					1,
					1,
					1

				} );

			switch ( res )
			{
				case 0:
				{
					from.SendMessage( "Vous avez besoin de griffes." );
					break;
				}
				case 1:
				{
					from.SendMessage( "Vous avez besoin d'une t�te." );
					break;
				}
				case 2:
				{
					from.SendMessage( "Vous avez besoin d'aile gauche." );
					break;
				}
				case 3:
				{
					from.SendMessage( "Vous avez besoin des jambes." );
					break;
				}
				case 4:
				{
					from.SendMessage( "Vous avez besoin d'un bassin." );
					break;
				}
				case 5:
				{
					from.SendMessage( "Vous avez besoin d'aile droite." );
					break;
				}
				case 6:
				{
					from.SendMessage( "Vous avez besoin d'une queue." );
					break;
				}

				default:
				{
					BoneDragonGolem m = new BoneDragonGolem( true, scalar );

					if ( m.SetControlMaster( from ) )
					{
						Delete();

						m.MoveToWorld( from.Location, from.Map );
						from.PlaySound( 0x244 );
					}

					break;
				}
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}