////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneDragonGolemTail: CrepusculeItem
	{
		[Constructable]
		public BoneDragonGolemTail() : base( 0x14EF )
		{
			Name = "Gol�m de dragon d'os, queue";
			Weight = 25.0;
		}

		public BoneDragonGolemTail( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}