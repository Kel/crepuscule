////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneDragonGolemLeftWing: CrepusculeItem
	{
		[Constructable]
		public BoneDragonGolemLeftWing() : base( 0x14EF )
		{
			Name = "Gol�m de dragon d'os, aile gauche";
			Weight = 35.0;
		}

		public BoneDragonGolemLeftWing( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}