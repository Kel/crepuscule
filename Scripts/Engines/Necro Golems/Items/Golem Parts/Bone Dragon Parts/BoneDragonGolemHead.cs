////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneDragonGolemHead: CrepusculeItem
	{
		[Constructable]
		public BoneDragonGolemHead() : base( 0x2250 )
		{
			Name = "Gol�m de dragon d'os, t�te";
			Weight = 35.0;
		}

		public BoneDragonGolemHead( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}