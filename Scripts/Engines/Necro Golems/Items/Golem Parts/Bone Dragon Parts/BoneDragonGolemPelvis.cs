////////////////////
//   Created by   //
//     Sabot      //
////////////////////
using System;
using Server;

namespace Server.Items
{
	public class BoneDragonGolemPelvis: CrepusculeItem
	{
		[Constructable]
		public BoneDragonGolemPelvis() : base( 0x14EF )
		{
			Name = "Gol�m de dragon d'os, bassin";
			Weight = 45.0;
		}

		public BoneDragonGolemPelvis( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}