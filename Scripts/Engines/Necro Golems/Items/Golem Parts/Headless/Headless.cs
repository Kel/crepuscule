////////////////////
//   Created by   //
//     Willy      //
////////////////////
using System;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "Corps de Headless" )]
	public class Headless : BaseCreature
	{
		public override WeaponAbility GetWeaponAbility()
		{
			return WeaponAbility.WhirlwindAttack;
		}

		public override bool IsScaredOfScaryThings{ get{ return false; } }
		public override bool IsScaryToPets{ get{ return true; } }

		public override bool IsBondable{ get{ return true; } }

		[Constructable]
		public Headless() : this( false, 1.0 )
		{
		}

		[Constructable]
		public Headless( bool summoned, double scalar ) : base( AIType.AI_Melee, FightMode.Closest, 10, 1, 0.4, 0.8 )
		{
			Name = "Headless";
			Body = 31;
			BaseSoundID = 0x48D;

			SetStr( (int)(75*scalar), (int)(125*scalar) );
			SetDex( (int)(19*scalar), (int)(14*scalar) );
			SetInt( (int)(2*scalar), (int)(5*scalar) );

			SetHits( (int)(25*scalar), (int)(38*scalar) );

			SetDamage( (int)(3*scalar), (int)(12*scalar) );

			SetDamageType( ResistanceType.Physical, 13 );

			SetResistance( ResistanceType.Physical, (int)(10*scalar), (int)(15*scalar) );
			SetResistance( ResistanceType.Fire, (int)(9*scalar), (int)(11*scalar) );
			SetResistance( ResistanceType.Cold, (int)(15*scalar), (int)(20*scalar) );
			SetResistance( ResistanceType.Poison, (int)(1*scalar), (int)(3*scalar) );
			SetResistance( ResistanceType.Energy, (int)(5*scalar), (int)(8*scalar) );

			SetSkill( SkillName.MagicResist, (65.1*scalar), (80.0*scalar) );
			SetSkill( SkillName.Tactics, (30.1*scalar), (38.0*scalar) );
			SetSkill( SkillName.Wrestling, (30.1*scalar), (38.0*scalar) );
			SetSkill( SkillName.Anatomy, (4.1*scalar), (50.0*scalar) );

			if ( summoned )
			{
				Fame = 50;
				Karma = 50;
			}
			else
			{
				Fame = 50;
				Karma = 50;
			}

			VirtualArmor = 12;
			ControlSlots = 1;

			switch ( Utility.Random( 5 ))
			{
				case 0: PackItem( new HeadlessChest() ); break;
				case 1: PackItem( new Heart() ); break;
				case 2: PackItem( new Brain() ); break;
				case 3: PackItem( new Celectrik() ); break;
				case 4: PackItem( new Ossature() ); break;
			}
		
		}

		public override bool DeleteOnRelease{ get{ return false; } }
		public override bool AutoDispel{ get{ return !Controled; } }
		public override bool BardImmune{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Lethal; } }
		public override bool CanRummageCorpses{ get{ return false; } }


		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.2 > Utility.RandomDouble() )
				defender.Combatant = null;
		}

		public override void OnDamage( int amount, Mobile from, bool willKill )
		{
			if ( Controled || Summoned )
			{
				Mobile master = ( this.ControlMaster );

				if ( master == null )
					master = this.SummonMaster;

				if ( master != null && master.Player && master.Map == this.Map && master.InRange( Location, 20 ) )
				{
					if ( master.Mana >= amount )
					{
						master.Mana -= amount;
					}
					else
					{
						amount -= master.Mana;
						master.Mana = 0;
						master.Damage( amount );
					}
				}
			}

			base.OnDamage( amount, from, willKill );
		}

		public Headless( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}