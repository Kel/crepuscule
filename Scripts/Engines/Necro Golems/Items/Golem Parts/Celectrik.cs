using System;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Items
{
	public class Celectrik: CrepusculeItem
	{
		[Constructable]
		public Celectrik() : this( 1 )
		{
		}
		
		[Constructable]
		public Celectrik( int amount ) : base( 0x1f1c )
		{
			Name = "Concentrateur électrique";
                        Weight = 1.0;
			Hue = 0x58;
			
		}

		public Celectrik( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Celectrik( amount ), amount );
		}
	}
}