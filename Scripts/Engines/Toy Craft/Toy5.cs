using System;
using Server.Items;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Items
{
	public class Toy5: CrepusculeItem
	{
		[Constructable]
		public Toy5() : this( 1 )
		{
		}
		
		[Constructable]
		public Toy5( int amount ) : base( 0x212F )
		{
			Name = "Crapaud en jouet";
                        Weight = 1.0;
			
		}

		public Toy5( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Toy5( amount ), amount );
		}
	}
}