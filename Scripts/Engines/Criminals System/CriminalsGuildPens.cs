﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Guilds;

namespace Server.Items
{
    public class EmpirePen : CriminalsPen
    {
        [Constructable]
        public EmpirePen() : base(GuildType.Empire) { Name = "La plume Impériale"; }
        public EmpirePen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class CerclePen : CriminalsPen
    {
        [Constructable]
        public CerclePen() : base(GuildType.Cercle) { Name = "La plume de Cercle"; }
        public CerclePen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class CommercantsPen : CriminalsPen
    {
        [Constructable]
        public CommercantsPen() : base(GuildType.Commercants) { Name = "La plume des Commerçants"; }
        public CommercantsPen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class ConfreriePen : CriminalsPen
    {
        [Constructable]
        public ConfreriePen() : base(GuildType.Confrerie) { Name = "La plume de la Confrèrie"; }
        public ConfreriePen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class GitansPen : CriminalsPen
    {
        [Constructable]
        public GitansPen() : base(GuildType.Gitans) { Name = "La plume des Gitans"; }
        public GitansPen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class MercenairesPen : CriminalsPen
    {
        [Constructable]
        public MercenairesPen() : base(GuildType.Mercenaires) { Name = "La plume des Mercenaires"; }
        public MercenairesPen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class SoleilPen : CriminalsPen
    {
        [Constructable]
        public SoleilPen() : base(GuildType.Soleil) { Name = "La plume de l'Ordre du Soleil"; }
        public SoleilPen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class OmbresPen : CriminalsPen
    {
        [Constructable]
        public OmbresPen() : base(GuildType.Ombres) { Name = "La plume Mystérieuse"; }
        public OmbresPen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }

    public class ChenePen : CriminalsPen
    {
        [Constructable]
        public ChenePen() : base(GuildType.Chene) { Name = "La plume de la Chêne"; }
        public ChenePen(Serial serial) : base(serial) { }

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
        #endregion
    }


}
