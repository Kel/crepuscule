﻿using System;
using Server;
using Server.Engines.Craft;
using Server.Mobiles;
using Server.Targeting;
using Server.Gumps;
using Server.Guilds;

namespace Server.Items
{
    [FlipableAttribute(0x0FBF, 0x0FC0)]
    public class CriminalsPen : CrepusculeItem
    {
        #region Properties
        private GuildType m_Guild;
        public GuildType Guild
        {
            get { return m_Guild; }
            set { m_Guild = value; }
        }
        #endregion

        #region Constructors
        [Constructable]
        public CriminalsPen(GuildType Guild)
            : base(0x0FBF)
        {
            m_Guild = Guild;
            Weight = 1.0;
        }

        public CriminalsPen(Serial serial)
            : base(serial)
        {
        }
        #endregion

        #region Serialization

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version

            writer.Write((int)m_Guild);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            m_Guild = (GuildType)reader.ReadInt();
        }

        #endregion

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);
            from.Target = new CriminalsPenTarget(this);
        }


        private class CriminalsPenTarget : Target
        {
            private const int ConstRange = 2;
            private CriminalsPen Pen;
            public CriminalsPenTarget(CriminalsPen item) : base(ConstRange, false, TargetFlags.None) { Pen = item; }

            protected override void OnTarget(Mobile from, object targ)
            {
                if (!(from is RacePlayerMobile)) return;
                else if (targ is CriminalsBook)
                {
                    CriminalsBook target = ((CriminalsBook)targ);
                    if (target.Guild == (from as RacePlayerMobile).GuildInfo.PlayerGuild
                        || from.AccessLevel >= AccessLevel.GameMaster)
                    {
                        if (Pen.Guild == target.Guild)
                        { // Ok, invoke gump
                            from.SendGump(new SelectAsCriminalGump(from, WorldContext.GetKnownMobiles(from as RacePlayerMobile), 0, target));
                        }
                        else AlertMessage(from, 2);
                    } else AlertMessage(from, 1);
                }
                else AlertMessage(from, 0);
            }

            protected override void OnTargetOutOfRange(Mobile from, object targ)
            {
                if (!(targ is CriminalsBook)) AlertMessage(from, 0);
                else AlertMessage(from, 3);
            }
        }

        private static void AlertMessage(Mobile from, int msgNumber)
        {
            string Message = "";

            switch (msgNumber)
            {
                case 0: Message = "Vous ne pouvez écrire que dans le registres de la guilde!"; break;
                case 1: Message = "Vous ne pouvez écrire que dans le registre de votre guilde!"; break;
                case 2: Message = "Vous remarquez que la plume dont vous avez ne convient pas."; break;
                case 3: Message = "Vous êtes trop loin pour écrire!"; break;
            }

            if (from != null && Message != null)
                from.SendMessage(Message);
        }
			
    }
}