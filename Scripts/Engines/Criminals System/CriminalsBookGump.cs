﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Mobiles;
using Server.Items;
using Server.Network;

namespace Server.Gumps
{

    public class CriminalsBookGump : KnownMobileListGump
    {
        #region Properties
        private CriminalsBook m_Book;
        public CriminalsBook Book
        {
            get { return m_Book; }
            set { m_Book = value; }
        }
        #endregion

        public CriminalsBookGump(Mobile owner, PlayerKnownDictionary list, int page, CriminalsBook book)
            : base(owner, list, page)
        {
            m_Book = book;
        }

        protected override void OnPageTurn(Mobile from, Mobile owner, PlayerKnownDictionary mobiles, int page)
        {
            from.SendGump(new CriminalsBookGump(owner, mobiles, page, m_Book));
        }

        protected override void OnMobileClicked(Mobile from, RacePlayerMobile clicked, IdentityType identity)
        {
            m_Book.RecorderCriminalsList.TryRemove(clicked, identity);
            from.SendMessage("Vous avez effacé un criminel!");
        }
    }

}
