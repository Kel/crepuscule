﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Mobiles;
using Server.Gumps;
using Server.Guilds;

namespace Server.Items
{
    public class CriminalsBook : CrepusculeItem
    {
        public static List<CriminalsBook> CriminalsBooks = new List<CriminalsBook>();

        private PlayerKnownDictionary m_RecordedCriminalsList = new PlayerKnownDictionary();
        private GuildType m_GuildOwnership = GuildType.None;

        #region Properties
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType Guild
        {
            get { return m_GuildOwnership; }
            set { m_GuildOwnership = value;
                this.Name = String.Format("{0} - Registre des hors la loi", FormatHelper.GetGuildDescription(value));
            }
        }

        public PlayerKnownDictionary RecorderCriminalsList
        {
            get { return m_RecordedCriminalsList; }
            set { m_RecordedCriminalsList = value; }
        }
        #endregion

        public CriminalsBook(Serial serial) : base(serial) { }

        [Constructable]
        public CriminalsBook()
            : base(0xFEF)
        {
            this.Name = "Registre des hors la loi";
            CriminalsBooks.Add(this);
        }

        public override void OnDoubleClick(Mobile from)
        {
            try
            {
                if (from.InRange(this.Location, 2))
                {
                    base.OnDoubleClick(from);
                    from.CloseGump(typeof(CriminalsBookGump));
                    from.SendGump(new CriminalsBookGump(from, this.RecorderCriminalsList, 0, this));
                }
                else
                {
                    from.SendMessage("Vous êtes trop loin");
                }
            }
            catch (Exception ex)
            {
                from.SendMessage("Erreur : " + ex.Message);
            }
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            RecorderCriminalsList = new PlayerKnownDictionary();
            if (CriminalsBooks == null) CriminalsBooks = new List<CriminalsBook>();
            CriminalsBooks.Add(this);

            int version = reader.ReadInt();
            if (version == 1)
                m_GuildOwnership = (GuildType)(reader.ReadInt());

            int Count = (int)reader.ReadInt();
            if (Count > 0)
            {
                for (int i = 0; i < Count; i++)
                {
                    RacePlayerMobile rpm = (RacePlayerMobile)reader.ReadMobile();
                    IdentityType ident = (IdentityType)(reader.ReadInt());
                    RecorderCriminalsList.TryAdd(rpm, ident);
                }
            }

        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)1); // version

            writer.Write((int)m_GuildOwnership);

            int Count = 0;
            if (RecorderCriminalsList != null)
                Count = RecorderCriminalsList.Count;

            writer.Write((int)Count);
            if (RecorderCriminalsList != null)
            {
                foreach (PlayerKnown known in RecorderCriminalsList.Values)
                {
                    writer.Write((Mobile)known.Known);
                    writer.Write((int)known.Type);
                }
            }
        }

    }
}
