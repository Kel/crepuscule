﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Mobiles;
using Server.Items;
using Server.Network;

namespace Server.Gumps
{

    public class SelectAsCriminalGump : KnownMobileListGump
    {
        #region Properties
        private CriminalsBook m_Book;
        public CriminalsBook Book
        {
            get { return m_Book; }
            set { m_Book = value; }
        }
        #endregion

        public SelectAsCriminalGump(Mobile owner, PlayerKnownDictionary list, int page, CriminalsBook book): base(owner, list,page)
        {
            m_Book = book;
        }

        protected override void OnPageTurn(Mobile from, Mobile owner, PlayerKnownDictionary mobiles, int page)
        {
            from.SendGump(new SelectAsCriminalGump(owner, mobiles, page, m_Book));
        }

        protected override void OnMobileClicked(Mobile from, RacePlayerMobile clicked, IdentityType identity)
        {
            if(from is RacePlayerMobile)
            {
                m_Book.RecorderCriminalsList.TryAdd(clicked, identity);
                from.SendMessage("Vous avez enregistré un criminel!");
                World.Broadcast(38,false,String.Format("{0}: Oye Oye, {1} est maintenant un hors la loi!", 
                    FormatHelper.GetGuildDescription(m_Book.Guild),
                    clicked.GetKnownName1(from as RacePlayerMobile, identity)));
            }
        }


    }

}
