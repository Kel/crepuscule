﻿using System;
using Server;
using Server.Misc;
using Server.Items;
using Server.Guilds;

namespace Server.Mobiles
{
	public class CriminalsGuard : BaseShieldGuard
	{
		public override int Keyword{ get{ return 0x22; } } // *chaos shield*
		public override BaseShield Shield{ get{ return new ChaosShield(); } }
		public override int SignupNumber{ get{ return 1007140; } } // Sign up with a guild of chaos if thou art interested.
		public override GuildType Type{ get{ return GuildType.Empire; } }

        #region Properties
        private GuildType m_Guild;

        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType GuardGuild
        {
            get { return m_Guild; }
            set { m_Guild = value; }
        }
        #endregion

		[Constructable]
		public CriminalsGuard() : base(AIType.AI_CriminalsGuard)
		{
		}

		public CriminalsGuard( Serial serial ) : base( serial )
		{
		}

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
            writer.Write((int)m_Guild);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        m_Guild = (GuildType)reader.ReadInt();
                        break;
                    }
            }
        }
        #endregion
	}
}