﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;
using Server.Items;

namespace Server.Engines
{
    public class CureAddiction : HealingSkill
    {
        public override int NeededAptitude { get { return 15; } }
        public override int NeededMana { get { return 10; } }
        protected BasePotion Potion = null;

        public CureAddiction(RacePlayerMobile caster, BasePotion potion) : base(caster) { Potion = potion; }

        public override void OnCast()
        {
            if (CanCast())
                Caster.Target = new InternalTarget(this);
        }

        public void Target(Mobile m)
        {
            if (!Caster.CanSee(m))
            {
                Caster.SendLocalizedMessage(500237); // Target can not be seen.
            }
            else if (!Caster.CanBeginAction(typeof(CureAddiction)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else
            {
                if (m is RacePlayerMobile && CheckSequence())
                {
                    //Get values
                    RacePlayerMobile target = m as RacePlayerMobile;
                    HealingHelper.Turn(Caster, target);
            
                    if (target != null && Caster != null)
                    {
                        Caster.BeginAction(typeof(CureAddiction));
                        AddictionsHelper.TryCure(Caster, target);
                        if (Potion != null)
                        {
                            BasePotion.PlayDrinkEffect(target);
                            Potion.Delete();
                            Caster.AddToBackpack(new Bottle());
                        }
                        Timer.DelayCall(TimeSpan.FromSeconds(60.0), new TimerStateCallback(EndAction), Caster);
                    }
                }
            }
        }

        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(CureAddiction));
        }


        #region InternalTarget
        private class InternalTarget : Target
        {
            private CureAddiction m_Owner;

            public InternalTarget(CureAddiction owner): base(1, false, TargetFlags.None)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is RacePlayerMobile)
                    m_Owner.Target((RacePlayerMobile)o);
            }

            protected override void OnTargetFinish(Mobile from)
            {

            }
        }
        #endregion
    }
}