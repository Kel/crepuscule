﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;
using Server.Engines;

namespace Server.Guilds
{
    #region Enums
    public enum GuildDomain
    {
        None,
        Defense,
        Wealth,
        Religion,
        Knowledge,
        Magic
    }

    public enum GuildMobileType
    {
        Townfolk,
        Diplomat,
        Guard
    }
    #endregion

    public static class GuildsSystem
    {
        public const int MaxPointsPerDomain = 100;
        public const int SubsidyDaysDelay = 7;

        public static DateTime LastPayDay = DateTime.MinValue;
        public static DateTime LastBackupDay = DateTime.MinValue;
        public static GuildsControllerStone MainController = null;
        public static Dictionary<GuildType, GuildsDomainStone> GuildsDomainStones = new Dictionary<GuildType, GuildsDomainStone>();
        public static Dictionary<GuildType, List<GuildPoints>> GuildsPoints = new Dictionary<GuildType, List<GuildPoints>>();
        public static Dictionary<GuildType, List<GuildGoldPerPoint>> GuildsGuildGoldPerPoint = new Dictionary<GuildType, List<GuildGoldPerPoint>>();
        public static Dictionary<GuildType, List<GuildGoal>> GuildsGoals = new Dictionary<GuildType, List<GuildGoal>>();
        public static Dictionary<GuildDomain, int> AvailablePoints = new Dictionary<GuildDomain, int>();
        public static Dictionary<GuildDomain, int> DomainGuildsCount = new Dictionary<GuildDomain,int>();

        public static Dictionary<GuildMobileType, List<BaseGuildMobile>> GuildMobiles = new Dictionary<GuildMobileType, List<BaseGuildMobile>>();
        public static Dictionary<GuildType, List<BaseGuildMobile>> GuildMobilesPerGuild = new Dictionary<GuildType, List<BaseGuildMobile>>();
        

        public static List<GuildPointsReward> RewardBatch = new List<GuildPointsReward>();

        static GuildsSystem()
        {
            // Initialize
            PlayerMobile.GuildClickMessage = false;
            string[] domains = Enum.GetNames(typeof(GuildDomain));
            foreach (string domainStr in domains)
            {
                GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domainStr));
                if (!DomainGuildsCount.ContainsKey(domain)) DomainGuildsCount.Add(domain, 0);
            }
        }

        public static void Update()
        {
            string[] domains = Enum.GetNames(typeof(GuildDomain));
            foreach (string domainStr in domains)
            {
                GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domainStr));
                DomainGuildsCount[domain] = 0;

                List<GuildType> guilds = GetActiveGuilds(domain);
                int Available = MaxPointsPerDomain;
                foreach (GuildType guild in guilds)
                {
                    GuildPoints points = GetPoints(guild, domain);
                    Available -= points.Points;
                }
                AvailablePoints[domain] = Available;
            }

            var lists = GuildsGoals.Values;
            foreach(var goals in lists)
            {
                foreach (var goal in goals)
                {
                    if (goal.CanParticipate) DomainGuildsCount[goal.Domain]++;
                }
            }

            //DistrubuteMobiles();
        }

        #region Distribute Mobiles
        public static void DistrubuteMobiles()
        {
            Dictionary<KeyValuePair<GuildDomain, GuildType>, int> ShouldBeNPCs    = new Dictionary<KeyValuePair<GuildDomain, GuildType>, int>();
            Dictionary<KeyValuePair<GuildDomain, GuildType>, int> AlreadyHaveNPCs = new Dictionary<KeyValuePair<GuildDomain, GuildType>, int>();

            string[] domains = Enum.GetNames(typeof(GuildDomain));
            foreach (string domainStr in domains)
            { // For Each domain

                GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domainStr));
                GuildMobileType type = GetMobileTypeByDomain(domain);
                int divisor = 1;
                if(type == GuildMobileType.Townfolk)
                    divisor = 4; // sharing 4 domains

                //how many npc's for this domain?
                if (!GuildMobiles.ContainsKey(type)) GuildMobiles.Add(type, new List<BaseGuildMobile>());
                int totalNPCs = (int)((double)GuildMobiles[type].Count / divisor);
                if(totalNPCs < 0) {totalNPCs = 0; return;}

                //World.Broadcast(3, false, "Total is " + GuildMobiles[type].Count.ToString());
               // World.Broadcast(3, false, "Total for the domain is " + totalNPCs.ToString());

                List<GuildType> guilds = GetActiveGuilds(domain);
                foreach (GuildType guild in guilds)
                {
                    //calculate how many npc of each type there should be
                    double percentage = GetPercentage(guild, domain);
                    int population = ((int)(totalNPCs * percentage) / 100);

                    ShouldBeNPCs.Add(new KeyValuePair<GuildDomain, GuildType>(domain, guild), population);

                    //get how many npcs there are for each type
                    if (!GuildMobilesPerGuild.ContainsKey(guild)) GuildMobilesPerGuild.Add(guild, new List<BaseGuildMobile>());

                    List<BaseGuildMobile> mobiles = GuildMobilesPerGuild[guild];
                    int GuildNPCsCount = mobiles.Count;
                    int thisType = 0;
                    for (int i = 0; i < GuildNPCsCount; i++)
                    {
                        if (mobiles[i].MobileType == type && mobiles[i].Domain == domain) thisType++;
                    }

                    AlreadyHaveNPCs.Add(new KeyValuePair<GuildDomain, GuildType>(domain, guild), thisType);

                    // CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{3}: Should be: {0}, Are: {1}, Max: {2}", population, thisType, totalNPCs, guild.ToString()));

                    //get the difference
                    int diff = population - thisType;

                    //adjust npc's guild
                    if (diff != 0)
                    {
                        if (diff < 0)
                        { //smaller, loose of npcs

                           // CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0} perd {1} NPCs", guild.ToString(), diff * -1));

                            List<BaseGuildMobile> list = new List<BaseGuildMobile>();
                            for (int i = 0; i < GuildNPCsCount; i++)
                            {
                                if (mobiles[i].MobileType == type && mobiles[i].Domain == domain) list.Add(mobiles[i]);
                            }

                            for (int i = 0; i < diff * -1; i++)
                            {
                                if (list[i] != null)
                                {
                                    list[i].GuildOfMobile = GuildType.None;
                                    list[i].Domain = GuildDomain.None;
                                }
                            }
                        }
                        else
                        { //bigger, assign new

                            List<BaseGuildMobile> indMobiles = GuildMobilesPerGuild[GuildType.None];
                            List<BaseGuildMobile> list = new List<BaseGuildMobile>();

                           // CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0} obtient {1} NPCs", guild.ToString(), diff ));

                            for (int i = 0; i < indMobiles.Count; i++)
                            {
                                if (indMobiles[i].MobileType == type) list.Add(indMobiles[i]);
                            }

                            for (int i = 0; i < diff; i++)
                            {
                                if (list[i] != null)
                                {
                                    //CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0} devient {1}",list[i].Name, guild.ToString()));
                                    list[i].GuildOfMobile = guild;
                                    list[i].Domain = domain;
                                }
                            }

                        }
                    }
                }


            }




        }
        #endregion

        #region Distribute Diplomats
        public static void DistrubuteDiplomats()
        {
            Dictionary<GuildType, int> ShouldBeNPCs = new Dictionary<GuildType, int>();
            Dictionary<GuildType, int> AlreadyHaveNPCs = new Dictionary<GuildType, int>();

            if (!GuildMobiles.ContainsKey(GuildMobileType.Diplomat)) GuildMobiles.Add(GuildMobileType.Diplomat, new List<BaseGuildMobile>());
            int totalNPCs = GuildMobiles[GuildMobileType.Diplomat].Count;
            if (totalNPCs <= 0) { return; }


            List<GuildType> guilds = GetAllActiveGuilds();
            foreach (GuildType guild in guilds)
            {
                //calculate how many npc of each type there should be
                double percentage = GetGlobalPercentage(guild);
                int population = ((int)(totalNPCs * percentage) / 100);

                ShouldBeNPCs.Add(guild, population);

                //get how many npcs there are for each type
                if (!GuildMobilesPerGuild.ContainsKey(guild)) GuildMobilesPerGuild.Add(guild, new List<BaseGuildMobile>());

                List<BaseGuildMobile> mobiles = GuildMobilesPerGuild[guild];
                int GuildNPCsCount = mobiles.Count;
                int thisType = 0;
                for (int i = 0; i < GuildNPCsCount; i++)
                {
                    if (mobiles[i].MobileType == GuildMobileType.Diplomat) thisType++;
                }

                AlreadyHaveNPCs.Add(guild, thisType);

    
                //get the difference
                int diff = population - thisType;

                //adjust npc's guild
                if (diff != 0)
                {
                    if (diff < 0)
                    { //smaller, loose of npcs

                       // CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0} perd {1} Diplomate", guild.ToString(), diff * -1));

                        List<BaseGuildMobile> list = new List<BaseGuildMobile>();
                        for (int i = 0; i < GuildNPCsCount; i++)
                        {
                            if (mobiles[i].MobileType == GuildMobileType.Diplomat) list.Add(mobiles[i]);
                        }

                        for (int i = 0; i < diff * -1; i++)
                        {
                            if (list[i] != null)
                            {
                                list[i].GuildOfMobile = GuildType.None;
                                list[i].Domain = GuildDomain.None;
                            }
                        }
                    }
                    else
                    { //bigger, assign new

                        List<BaseGuildMobile> indMobiles = GuildMobilesPerGuild[GuildType.None];
                        List<BaseGuildMobile> list = new List<BaseGuildMobile>();

                       // CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0} obtient {1} Diplomate", guild.ToString(), diff));

                        for (int i = 0; i < indMobiles.Count; i++)
                        {
                            if (indMobiles[i].MobileType == GuildMobileType.Diplomat) list.Add(indMobiles[i]);
                        }

                        for (int i = 0; i < diff; i++)
                        {
                            if (list[i] != null)
                            {
                                list[i].GuildOfMobile = guild;
                                list[i].Domain = GuildDomain.None;
                            }
                        }

                    }
                }
            }
        }

       #endregion

        #region Points Management

        public static void AddReward(GuildPointsReward reward)
        {
            RewardBatch.Add(reward);

            List<GuildType> guilds = GetActiveGuilds(reward.Domain);
            foreach (GuildType guild in guilds)
            {
                if (guild == reward.Guild)
                {
                    AddPoints(guild, reward.Domain, reward.RewardSelf);
                }
                else
                {
                    AddPoints(guild, reward.Domain, reward.RewardOthers);
                }
            }

        }


        public static GuildPoints GetPoints(GuildType guild, GuildDomain domain)
        {
            if (GuildsPoints.ContainsKey(guild))
            {
                foreach (GuildPoints points in GuildsPoints[guild])
                {
                    if (points.Domain == domain) return points;
                }
            }
            return null;
        }

        public static void AddPoints(GuildType guild, GuildDomain domain, int pointsToAdd)
        {
            GuildPoints points = GetPoints(guild, domain);
            if (points != null && points.Points < MaxPointsPerDomain)
            {
                if (points.Points + pointsToAdd > MaxPointsPerDomain)
                { //will be at max
                    pointsToAdd = MaxPointsPerDomain - points.Points;
                }
                if (points.Points + pointsToAdd < 0)
                { //will be at 0
                    pointsToAdd = points.Points * -1;
                }

                int newValue = pointsToAdd;

                if (AvailablePoints[domain] - pointsToAdd < 0)
                { // too low
                    int diff = (AvailablePoints[domain] - pointsToAdd) * -1; // ex: (2 - +7)  => 5
                    newValue = pointsToAdd - diff; // ex: 7 - 5 => 2

                    int gathered = 0;
                    //Apply life :)
                    List<GuildType> guilds = GetActiveGuilds(domain);
                    foreach (GuildType victim in guilds)
                    {
                        if (victim != guild)
                        {
                            
                            if (GetPoints(victim, domain).Points > 0)
                            {
                                AddPoints(victim, domain, -1);
                                gathered++;
                            }
                            
                            WorldContext.BroadcastMessage(AccessLevel.Counselor, 3,
                                String.Format("{0} perd de l'influence.",
                                GuildDescriptions.GetGuildDescription(victim)));
                        }
                    }
                    if (gathered > 0)
                    {
                        if (gathered > diff) gathered = diff;
                        newValue += gathered;
                    }
                }else if (AvailablePoints[domain] - pointsToAdd > MaxPointsPerDomain)
                { // too high
                    newValue = MaxPointsPerDomain - (AvailablePoints[domain] - pointsToAdd); 
                }

                AvailablePoints[domain] -= newValue;
                SetGuildDomainPoints(guild, domain,points, points.Points + newValue);

                DistrubuteMobiles();
                DistrubuteDiplomats();
            }
        }


        public static void SetGuildDomainPoints(GuildType guild, GuildDomain domain, GuildPoints Points, int points)
        {
            if (domain == GuildDomain.Defense) GuildsDomainStones[guild].m_PointsDefense = points;
            if (domain == GuildDomain.Knowledge) GuildsDomainStones[guild].m_PointsKnowledge = points;
            if (domain == GuildDomain.Magic) GuildsDomainStones[guild].m_PointsMagic = points;
            if (domain == GuildDomain.Religion) GuildsDomainStones[guild].m_PointsReligion = points;
            if (domain == GuildDomain.Wealth) GuildsDomainStones[guild].m_PointsWealth = points;
            Points.Points = points;
        }

        #endregion

        #region Static Helpers

        public static int GetIncome(GuildType guild)
        {
            int result = 0;
            if (guild != GuildType.None)
            {
                if (GetGlobalPercentage(guild) > 10.0)
                {
                    string[] domains = Enum.GetNames(typeof(GuildDomain));
                    foreach (string domainStr in domains)
                    { // For Each domain
                        GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domainStr));

                        GuildGoldPerPoint income = GetGoldPerPoint(guild, domain);
                        GuildPoints points = GetPoints(guild, domain);
                        if (income != null && points != null)
                        {
                            //World.Broadcast(20, false, "{2}: {0}po x {1}pt", income.GoldPerPoint, points.Points, domain.ToString());
                            result += income.GoldPerPoint * points.Points;
                        }
                    }
                }
            }
            return result;
        }

        public static int GetSubsidy(GuildType guild)
        {
            int result = 0;
            if (GuildsDomainStones.ContainsKey(guild))
            {
                if (GetGlobalPercentage(guild) > 40.0) // > 40% global influence
                {
                    if (GuildsDomainStones[guild].LastSubsidyAttribution.AddDays(SubsidyDaysDelay) <= DateTime.Now)
                    {
                        result = GuildsDomainStones[guild].GoldForSubsidy;
                        GuildsDomainStones[guild].m_LastSubsidyAttribution = DateTime.Now;
                    }
                }
            }
            return result;
        }

        public static GuildMobileType GetMobileTypeByDomain(GuildDomain domain)
        {
            if (domain == GuildDomain.Defense) return GuildMobileType.Guard;
            return GuildMobileType.Townfolk;
        }

        public static List<GuildType> GetActiveGuilds(GuildDomain domain)
        {
            List<GuildType> list = new List<GuildType>();
            foreach (GuildType guild in GuildsGoals.Keys)
            {
                foreach (GuildGoal goal in GuildsGoals[guild])
                {
                    if(goal.Domain == domain && goal.CanParticipate)
                        list.Add(guild);
                }
            }

            return list;
        }

        public static List<GuildType> GetAllActiveGuilds()
        {
            List<GuildType> list = new List<GuildType>();
            foreach (GuildType guild in GuildsGoals.Keys)
            {
                if (!list.Contains(guild))
                {
                     list.Add(guild);
                }
            }

            return list;
        }

        public static GuildGoal GetGoal(GuildType guild, GuildDomain domain)
        {
            if (GuildsGoals.ContainsKey(guild))
            {
                foreach (GuildGoal goal in GuildsGoals[guild])
                {
                    if (goal.Domain == domain) return goal;
                }
            }
            return null;
        }

        public static double GetPercentage(GuildType guild, GuildDomain domain)
        {
            GuildPoints points = GetPoints(guild,domain);
            if(points == null) return 0.0;

            return (points.Points * ((double)MaxPointsPerDomain / 100.0));
        }

        public static double GetGlobalPercentage(GuildType guild)
        {
            string[] domains = Enum.GetNames(typeof(GuildDomain));
            double result = 0;
            int domainsLen = domains.Length -1;
            foreach (string domainStr in domains)
            { // For Each domain
                GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domainStr));
                if(domain != GuildDomain.None)
                {
                    result += GetPercentage(guild, domain);
                }
            }
            return result / domainsLen;
        }

        public static GuildGoldPerPoint GetGoldPerPoint(GuildType guild, GuildDomain domain)
        {
            if (GuildsGuildGoldPerPoint.ContainsKey(guild))
            {
                foreach (GuildGoldPerPoint points in GuildsGuildGoldPerPoint[guild])
                {
                    if (points.Domain == domain) return points;
                }
            }
            return null;
        }

        #endregion

    }
}
