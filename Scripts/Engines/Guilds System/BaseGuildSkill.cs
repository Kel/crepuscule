﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Guilds
{
    public abstract class BaseGuildSkill
    {
        protected string m_Label = "";

        public BaseGuildSkill()
        {

        }

        public abstract GuildType Guild { get; }
        public abstract void Action(PlayerMobile from, params object[] args);

        public virtual string Label
        {
            get { return m_Label; }
            set { m_Label = value; }
        }
        
    }
}
