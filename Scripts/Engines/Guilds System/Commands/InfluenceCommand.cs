﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;
using Server.Guilds;

namespace Server.Scripts.Commands
{
    public class InfluenceCommand
    {
        public static void Initialize()
        {
            Server.Commands.Register("influence", AccessLevel.Player, new CommandEventHandler(InfluenceCommand_OnCommand));
        }

        [Usage("influence")]
        [Description("Permet de voir le rapport de l'influence globale des guildes")]
        private static void InfluenceCommand_OnCommand(CommandEventArgs e)
        {
            List<GuildType> guilds = GuildsSystem.GetAllActiveGuilds();
            for (int i = 0; i < guilds.Count; i++)
            {
                GuildType guild = guilds[i];
                if (guild != GuildType.None)
                {
                    e.Mobile.SendMessage(2 + i * 12, "{0}: Niveau d'influence est {1}", FormatHelper.GetGuildDescription(guild), FormatHelper.GetGuildGlobalInfluenceDescription(guild));
                }
            }
        }

    }
}
