using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using Server.Gumps;

namespace Server.Misc
{
    public class ShowGuildInfoCommand
	{

        

		public static void Initialize()
		{
            Commands.Register("guilde", AccessLevel.Player, new CommandEventHandler(ShowGuildInfo_OnCommand));

		}

        private static void ShowGuildInfo_OnCommand(CommandEventArgs args)
		{
            Mobile m = args.Mobile;

            if (m is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)m;
                pm.CloseAllGumps();
                pm.SendGump(new GuildInfoGump(pm));
            }

        }


    }
}

