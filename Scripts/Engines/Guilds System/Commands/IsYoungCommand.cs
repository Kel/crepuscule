using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Misc;
using Server.Gumps;
using Server.Multis;
using Server.Engines.Help;

namespace Server.Scripts.Commands
{

	public class IsYoungCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("Nouveau", AccessLevel.Player, new CommandEventHandler(IsYoung_OnCommand));
		}

        [Usage("Nouveau")] 
	    [Description( "Active/D�sactive le mode nouveau joueur")]
        public static void IsYoung_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile is RacePlayerMobile)
            {
                RacePlayerMobile from = e.Mobile as RacePlayerMobile;
                from.GuildInfo.IsYoung = !from.GuildInfo.IsYoung;
                if (from.GuildInfo.IsYoung)
                    from.SendMessage(60, "Le mode 'nouveau joueur' est activ�.");
                else
                    from.SendMessage(40, "Le mode 'nouveau joueur' est d�sactiv�.");

            }

		} 
	}


}
