﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;
using Server.Guilds;

namespace Server.Scripts.Commands
{
    public class RefreshNPCsCommand
    {
        public static void Initialize()
        {
            Server.Commands.Register("RefreshNPCs", AccessLevel.Administrator, new CommandEventHandler(RefreshNPCsCommand_OnCommand));
        }

        [Usage("resetguilds")]
        [Description("rafraichit les items des npc de guilde")]
        private static void RefreshNPCsCommand_OnCommand(CommandEventArgs e)
        {
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Townfolk])
            {
                ClearMobile(mobile);
            }
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Guard])
            {
                ClearMobile(mobile);
            }
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Diplomat])
            {
                ClearMobile(mobile);
            }
        }

        private static void ClearMobile(BaseGuildMobile mobile)
        {
            GuildType guild = mobile.GuildOfMobile;
            CopyEquip.DeleteAllItems(mobile);
            mobile.SetGuild(guild);
        }
    }
}
