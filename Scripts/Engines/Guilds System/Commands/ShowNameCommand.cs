using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Misc;
using Server.Gumps;
using Server.Multis;
using Server.Engines.Help;

namespace Server.Scripts.Commands
{

	public class ShowNameCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("MontrerNom", AccessLevel.Player, new CommandEventHandler(ShowName_OnCommand));
		}

        [Usage("MontrerNom")] 
	    [Description( "Montre ou pas le nom (tag) aux joueurs arrivants")]
        public static void ShowName_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile is RacePlayerMobile)
            {
                RacePlayerMobile from = e.Mobile as RacePlayerMobile;
                from.GuildInfo.ShowName = !from.GuildInfo.ShowName;
                if (from.GuildInfo.ShowName)
                    from.SendMessage(60, "Votre tag de nom sera affich� aux joueurs qui s'approchent de vous.");
                else
                    from.SendMessage(40, "Votre tag de nom ne sera pas affich� aux joueurs qui s'approchent de vous.");

            }

		} 
	}


}
