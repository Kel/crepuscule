﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;
using Server.Guilds;
using Server.Gumps;

namespace Server.Scripts.Commands
{
    public class Gossip
    {
        public static void Initialize()
        {
            CommandSystem.Register("rumeurs", AccessLevel.Player, new CommandEventHandler(Gossip_OnCommand));
        }

        [Usage("rumeurs")]
        [Description("Permet de voir le rapport de l'influence des guildes dans les différents points")]
        private static void Gossip_OnCommand(CommandEventArgs e)
        {
            List<int> huesList;
            List<string> text = GetGossip(e.Mobile, out huesList);

            if (text.Count > 0)
            {
                e.Mobile.CloseGump(typeof(StringListGump));
                e.Mobile.SendGump(new StringListGump("RUMEURS", 150, text, huesList));
            }
        }

        public static List<string> GetGossip(Mobile mob, out List<int> huesList)
        {
            List<string> result = new List<string>();
            huesList = new List<int>();
            if (mob != null && mob is PlayerMobile &&
                  ((mob as PlayerMobile).GuildInfo.PlayerGuild == GuildType.Empire || (mob as PlayerMobile).AccessLevel >= AccessLevel.GameMaster))
            {
                string[] domains = Enum.GetNames(typeof(GuildDomain));
                for (int i = 0; i < domains.Length; i++)
                {
                    GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domains[i]));
                    if (domain != GuildDomain.None)
                    {
                        //mob.SendMessage(2 + i * 12, " {0}:", GuildDescriptions.GetGuildDomainDescription(domain));
                        result.Add(String.Format(" {0}:", GuildDescriptions.GetGuildDomainDescription(domain)));
                        huesList.Add(2 + i);

                        List<GuildType> guilds = GuildsSystem.GetActiveGuilds(domain);
                        foreach (GuildType guild in guilds)
                        {
                            if (mob.AccessLevel >= AccessLevel.GameMaster)
                            {
                                result.Add(String.Format("   {0}: {1} pt.", GuildDescriptions.GetGuildDescription(guild), GuildsSystem.GetPoints(guild, domain).Points));
                                huesList.Add(2 + i * 12);
                                //mob.SendMessage(2 + i * 12, "   {0}: {1} pt.", GuildDescriptions.GetGuildDescription(guild), GuildsSystem.GetPoints(guild, domain).Points);
                            }
                            else
                            {
                                result.Add(String.Format("   {0}: {1}", GuildDescriptions.GetGuildDescription(guild), GuildDescriptions.GetGuildInfluenceDescription(guild, domain)));
                                huesList.Add(2 + i * 12);
                                //mob.SendMessage(2 + i * 12, "   {0}: {1}", GuildDescriptions.GetGuildDescription(guild), GuildDescriptions.GetGuildInfluenceDescription(guild, domain));
                            }
                        }

                        result.Add("");
                        huesList.Add(2 + i);
                    }
                }
            }
            return result;
        }

    }
}
