using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;

namespace Server.Misc
{
    public class MaskCommand
	{

        

		public static void Initialize()
		{
            Commands.Register("masque", AccessLevel.Player, new CommandEventHandler(Mask_OnCommand));
		}

        private static void Mask_OnCommand(CommandEventArgs args)
		{
            Mobile m = args.Mobile;

            if (m is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)m;
                if (pm.UseIdentity == 0)
                    pm.UseIdentity = 1;
                else
                    pm.UseIdentity = 0;

                // Show message
                if (pm.UseIdentity == 0)
                    pm.SendMessage(60, "La premiere identit� est utilis�e");
                else
                    pm.SendMessage(40, "La seconde identit� est utilis�e");

                pm.SendPropertiesTo(pm);
                pm.InvalidateProperties();
            }
        }


    }
}

