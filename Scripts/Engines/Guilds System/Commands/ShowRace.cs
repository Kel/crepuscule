using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;

namespace Server.Misc
{
    public class ShowRaceCommand
	{

        

		public static void Initialize()
		{
            Commands.Register("montrerrace", AccessLevel.Player, new CommandEventHandler(ShowRaceCommand_OnCommand));
		}

        private static void ShowRaceCommand_OnCommand(CommandEventArgs args)
		{
            Mobile m = args.Mobile;

            if (m is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)m;
                pm.GuildInfo.ShowRace = !pm.GuildInfo.ShowRace;

                // Show message
                if (pm.GuildInfo.ShowRace == false)
                    pm.SendMessage(60, "Votre race sera masqu�e");
                else
                    pm.SendMessage(40, "Votre race sera affich�e");

                pm.SendPropertiesTo(pm);
                pm.InvalidateProperties();
            }



        }


    }
}

