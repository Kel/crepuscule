using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;

namespace Server.Misc
{
    public class ShowGuildCommand
	{

        

		public static void Initialize()
		{
            Commands.Register("montrerguilde", AccessLevel.Player, new CommandEventHandler(ShowGuild_OnCommand));

		}

        private static void ShowGuild_OnCommand(CommandEventArgs args)
		{
            Mobile m = args.Mobile;

            if (m is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)m;
                pm.GuildInfo.ShowGuild = !pm.GuildInfo.ShowGuild;
                
                // show the message
                if (pm.GuildInfo.ShowGuild)
                    pm.SendMessage(60, "La guilde et le titre de votre personnage seront montr�s.");
                else
                    pm.SendMessage(40, "La guilde et le titre de votre personnage ne seront pas montr�s.");

                pm.SendPropertiesTo(pm);
                pm.InvalidateProperties();
            }



        }


    }
}

