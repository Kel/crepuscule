﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;
using Server.Gumps;

namespace Server.Scripts.Commands
{
    public class MarkGuildCommand
    {
        public static void Initialize()
        {
            Server.Commands.Register("CoteGuilde", AccessLevel.GameMaster, new CommandEventHandler(MarkGuild_OnCommand));
            Server.Commands.Register("CoteGuild", AccessLevel.GameMaster, new CommandEventHandler(MarkGuild_OnCommand));
        }

        [Usage("CoteGuilde")]
        [Description("Permet de côter les guildes en leur attribuant des points")]
        private static void MarkGuild_OnCommand(CommandEventArgs e)
        {
            ShowMarkGuildGump(e.Mobile);
        }

        public static void ShowMarkGuildGump(Mobile from)
        {
            from.CloseAllGumps();
            from.SendGump(new GuildsPointsGump());
        }

    }
}
