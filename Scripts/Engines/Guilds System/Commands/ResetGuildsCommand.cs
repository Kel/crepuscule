﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;
using Server.Guilds;

namespace Server.Scripts.Commands
{
    public class ResetGuildsCommand
    {
        public static void Initialize()
        {
            Server.Commands.Register("ResetGuilds", AccessLevel.Administrator, new CommandEventHandler(ResetGuildsCommand_OnCommand));
        }

        [Usage("resetguilds")]
        [Description("Remet à 0 tous les points des guildes")]
        private static void ResetGuildsCommand_OnCommand(CommandEventArgs e)
        {
            List<GuildType> guilds = GuildsSystem.GetAllActiveGuilds();
            foreach (GuildType guild in guilds)
            {
                string[] domains = Enum.GetNames(typeof(GuildDomain));
                foreach (string domainStr in domains)
                { // For Each domain
                    GuildDomain domain = (GuildDomain)(Enum.Parse(typeof(GuildDomain), domainStr));
                    if (domain != GuildDomain.None)
                    {
                        //CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0}/{1}",guild.ToString(),domain.ToString()));
                        GuildPoints points = GuildsSystem.GetPoints(guild, domain);
                        if (points != null)
                        {
                            int toZero = points.Points * -1;
                            GuildsSystem.AddPoints(guild, domain, toZero);
                        }
                    }
                }
            }

            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Townfolk])
            {
                mobile.GuildOfMobile = GuildType.None;
            }
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Guard])
            {
                mobile.GuildOfMobile = GuildType.None;
            }
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Diplomat])
            {
                mobile.GuildOfMobile = GuildType.None;
            }
        }

    }
}
