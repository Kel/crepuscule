using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Misc;
using Server.Gumps;
using Server.Multis;
using Server.Engines.Help;

namespace Server.Scripts.Commands
{

	public class ShowMonstersCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("MontrerMonstres", AccessLevel.Player, new CommandEventHandler(ShowMonsters_OnCommand));
		}

        [Usage("MontrerMonstres")] 
	    [Description( "Montre ou pas le nom (tag) des monstres arrivants")]
        public static void ShowMonsters_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile is RacePlayerMobile)
            {
                RacePlayerMobile from = e.Mobile as RacePlayerMobile;
                from.GuildInfo.ShowMonsters = !from.GuildInfo.ShowMonsters;
                if (from.GuildInfo.ShowMonsters)
                    from.SendMessage(60, "Le nom des monstres s'approcheant de votre personnage seront montr�s.");
                else
                    from.SendMessage(40, "Le nom des monstres s'approcheant de votre personnage ne seront pas montr�s.");

            }

		} 
	}


}
