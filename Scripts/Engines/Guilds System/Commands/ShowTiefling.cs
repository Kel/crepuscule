using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;

namespace Server.Misc
{
    public class TieflingCommand
	{

        

		public static void Initialize()
		{
            Commands.Register("tiefling", AccessLevel.Player, new CommandEventHandler(TieflingCommand_OnCommand));

		}

        private static void TieflingCommand_OnCommand(CommandEventArgs args)
		{
            if (args.Mobile is RacePlayerMobile)
            {
                var pm = args.Mobile as RacePlayerMobile;
                pm.GuildInfo.ShowTiefling = !pm.GuildInfo.ShowTiefling;

                if (pm.EvolutionInfo.Race == RaceType.Tiefling)
                {
                	if (pm.PenaliteMort == true) 	
                	{
                		pm.SendMessage("Vous �tes trop faible pour faire ressortir votre sang d�moniaque.");
                        pm.GuildInfo.ShowTiefling = false;
                	}
                	else 
                	{
                        if (pm.GuildInfo.ShowTiefling == true)
                    	{	
                    		pm.SendMessage("Votre sang d�moniaque retourne au fin fond de votre �tre suivit de vos sens accrus.");
                            pm.GuildInfo.ShowTiefling = false;
                    		//pm.StatCap -= 90;
                    		//pm.MaxStr -= 30;
                    		//pm.MaxDex -= 30;
                    		//pm.MaxInt -= 30;
                    		//pm.Str -= 30;
                    		//pm.Dex -= 30;
                    		//pm.Int -= 30;
                    	}
                    	else
                    	{	
                    		pm.SendMessage("Votre sang d�moniaque bouillonne vous donnant des sens accrus.");
                            pm.GuildInfo.ShowTiefling = true;
                    		//pm.StatCap += 90;
                    		//pm.MaxStr += 30;
                    		//pm.MaxDex += 30;
                    		//pm.MaxInt += 30;
                    		//pm.Str += 30;
                    		//pm.Dex += 30;
                    		//pm.Int += 30;
                    	}
                	}
                }
                else
                {
                    pm.SendMessage("Cette commande est accessible uniquement aux tieflings");
                    pm.GuildInfo.ShowTiefling = false;
                }

                pm.SendPropertiesTo(pm);
                pm.InvalidateProperties();
            }



        }


    }
}

