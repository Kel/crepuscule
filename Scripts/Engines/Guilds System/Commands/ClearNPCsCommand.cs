﻿using System;
using System.Reflection;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;
using Server.Guilds;

namespace Server.Scripts.Commands
{
    public class ClearNPCsCommand
    {
        public static void Initialize()
        {
            Server.Commands.Register("clearNPCs", AccessLevel.Administrator, new CommandEventHandler(ClearNPCsCommand_OnCommand));
        }

        [Usage("resetguilds")]
        [Description("efface les items des items des npc de guilde")]
        private static void ClearNPCsCommand_OnCommand(CommandEventArgs e)
        {
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Townfolk])
            {
                ClearMobile(mobile);
            }
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Guard])
            {
                ClearMobile(mobile);
            }
            foreach (BaseGuildMobile mobile in GuildsSystem.GuildMobiles[GuildMobileType.Diplomat])
            {
                ClearMobile(mobile);
            }
        }

        private static void ClearMobile(BaseGuildMobile mobile)
        {
            CopyEquip.DeleteAllItems(mobile);
            
        }
    }
}
