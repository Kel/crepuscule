using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Misc;
using Server.Gumps;
using Server.Multis;
using Server.Engines.Help;

namespace Server.Scripts.Commands
{

	public class IsInvisibleCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("Invisible", AccessLevel.Player, new CommandEventHandler(IsInvisible_OnCommand));
		}

        [Usage("Invisible")] 
	    [Description( "Active/D�sactive la visibilit� sur le statut du serveur")]
        public static void IsInvisible_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile is RacePlayerMobile)
            {
                RacePlayerMobile from = e.Mobile as RacePlayerMobile;
                from.GuildInfo.m_IsInvisible = !from.GuildInfo.m_IsInvisible;
                if (from.GuildInfo.m_IsInvisible)
                    from.SendMessage(60, "Votre personnage n'est plus visible sur le statut.");
                else
                    from.SendMessage(40, "Votre personnage est visible sur le statut.");

            }

		} 
	}


}
