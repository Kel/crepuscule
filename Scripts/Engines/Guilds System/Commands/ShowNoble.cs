using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;

namespace Server.Misc
{
    public class ShowNobleCommand
	{

        

		public static void Initialize()
		{
            Commands.Register("noblesse", AccessLevel.Player, new CommandEventHandler(ShowNoble_OnCommand));
		}

        private static void ShowNoble_OnCommand(CommandEventArgs args)
		{
            Mobile m = args.Mobile;

            if (m is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)m;
                pm.GuildInfo.ShowNoble = !pm.GuildInfo.ShowNoble;

                // Show message
                if (pm.GuildInfo.ShowNoble)
                    pm.SendMessage(60, "Le titre de noblesse de votre personnage seront montr�s.");
                else
                    pm.SendMessage(40, "Le titre de noblesse de votre personnage ne seront pas montr�s.");

                pm.SendPropertiesTo(pm);
                pm.InvalidateProperties();
            }



        }


    }
}

