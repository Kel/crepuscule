﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Network;

namespace Server.Guilds
{
    [PropertyObject]
    public class SalaryInfo
    {
        public SalaryInfo() 
        {
            
        }

        public SalaryInfo(GenericReader reader)
        {
            Deserialize(reader);
        }

        internal int m_Salary = 0;
        internal int m_SalarySum = 0;
        internal string m_EmployeeName = "";


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(m_Salary);
            writer.Write(m_SalarySum);
            writer.Write(m_EmployeeName);
        }

        public void Deserialize(GenericReader reader)
        {
            var version = reader.ReadInt();
            m_Salary = reader.ReadInt();
            m_SalarySum = reader.ReadInt();
            m_EmployeeName = reader.ReadString();
        }


        [CommandProperty(AccessLevel.GameMaster)]
        public int Salary
        {
            get { return m_Salary; }
            set { m_Salary = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int SalarySum
        {
            get { return m_SalarySum; }
            set { m_SalarySum = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string EmployeeName
        {
            get { return m_EmployeeName; }
            set { m_EmployeeName = value; }
        }


        public override string ToString()
        {
            return "(Infos de Salaire)";
        }
    }

}
