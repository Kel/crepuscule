﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Network;
using Server.Items;
using Server.Engines;

namespace Server.Guilds
{
    public class Guild : BaseGuild
    {
        private static bool IsInitialized = false;
        public Guild(GuildType type) : base(type){}

        #region Properties
        private List<RacePlayerMobile> m_Members;
        private string m_Abbreviation;
        private string m_Name;
        private GuildType m_Type;
        private bool m_Disbanded = false;
        private bool m_Secret = false;
        private bool m_Special = false;

        public List<RacePlayerMobile> Members
        {
            get { return m_Members; }
        }
        public override string Abbreviation
        {
            get { return m_Abbreviation; }
            set { m_Abbreviation = value; }
        }
        public override string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public override GuildType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public override bool Disbanded
        {
            get { return m_Disbanded; }
            set { m_Disbanded = value; }
        }
        public override bool Secret
        {
            get { return m_Secret; }
            set { m_Secret = value; }
        }
        public override bool Special
        {
            get { return m_Special; }
            set { m_Special = value; }
        }
        #endregion

        public override void OnMemberJoin(Mobile member)
        {
            if (member is RacePlayerMobile)
            {
                if (m_Members == null) m_Members = new List<RacePlayerMobile>();
                m_Members.Add(member as RacePlayerMobile);
            }
        }

        public override void OnMemberLeave(Mobile member)
        {
            if (member is RacePlayerMobile)
            {
                if (m_Members.Contains(member as RacePlayerMobile))
                    m_Members.Remove(member as RacePlayerMobile);
            }
        }

        public override void OnMobileDelete(Mobile mob)
        {
            if (mob is PlayerMobile)
            {
                var member = mob as PlayerMobile;
                if (m_Members.Contains(member as RacePlayerMobile))
                    m_Members.Remove(member as RacePlayerMobile);
            }
        }

        #region Guild[Text]Message(...)
        public void GuildMessage(int num, bool append, string format, params object[] args)
        {
            GuildMessage(num, append, String.Format(format, args));
        }
        public void GuildMessage(int number)
        {
            for (int i = 0; i < m_Members.Count; ++i)
                m_Members[i].SendLocalizedMessage(number);
        }
        public void GuildMessage(int number, string args)
        {
            GuildMessage(number, args, 0x3B2);
        }
        public void GuildMessage(int number, string args, int hue)
        {
            for (int i = 0; i < m_Members.Count; ++i)
                m_Members[i].SendLocalizedMessage(number, args, hue);
        }
        public void GuildMessage(int number, bool append, string affix)
        {
            GuildMessage(number, append, affix, "", 0x3B2);
        }
        public void GuildMessage(int number, bool append, string affix, string args)
        {
            GuildMessage(number, append, affix, args, 0x3B2);
        }
        public void GuildMessage(int number, bool append, string affix, string args, int hue)
        {
            for (int i = 0; i < m_Members.Count; ++i)
                m_Members[i].SendLocalizedMessage(number, append, affix, args, hue);
        }

        public void GuildTextMessage(string text)
        {
            GuildTextMessage(0x3B2, text);
        }
        public void GuildTextMessage(string format, params object[] args)
        {
            GuildTextMessage(0x3B2, String.Format(format, args));
        }
        public void GuildTextMessage(int hue, string text)
        {
            for (int i = 0; i < m_Members.Count; ++i)
                m_Members[i].SendMessage(hue, text);
        }
        public void GuildTextMessage(int hue, string format, params object[] args)
        {
            GuildTextMessage(hue, String.Format(format, args));
        }

        /*public void GuildChat(Mobile from, int hue, string text)
        {
            Packet p = null;
            for (int i = 0; i < m_Members.Count; i++)
            {
                Mobile m = m_Members[i];

                NetState state = m.NetState;

                if (state != null)
                {
                    if (p == null)
                        p = Packet.Acquire(new UnicodeMessage(from.Serial, from.Body, MessageType.Guild, hue, 3, from.Language, from.Name, text));

                    state.Send(p);
                }
            }

            Packet.Release(p);
        }

        public void GuildChat(Mobile from, string text)
        {
            PlayerMobile pm = from as PlayerMobile;

            GuildChat(from, (pm == null) ? 0x3B2 : pm.GuildMessageHue, text);
        }*/
        #endregion


        public override void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();

            switch (version)
            {

                case 0:
                {
                    m_Type = (GuildType) reader.ReadInt();
                    m_Members = reader.ReadStrongMobileList<RacePlayerMobile>();

                    m_Abbreviation = GuildDescriptions.GetGuildAbbreviation(m_Type);
                    m_Name = GuildDescriptions.GetGuildDescription(m_Type);
                    m_Secret = GuildDescriptions.GetGuildIsSecret(m_Type);
                    m_Special = GuildDescriptions.GetGuildIsSpecial(m_Type);
                    m_Disbanded = GuildDescriptions.GetGuildIsDisbanded(m_Type);


                } break;
            }
        }

        public override void Serialize(GenericWriter writer)
        {
            writer.Write((int)0);	//Version


            // Version 0 
            writer.Write((int)m_Type);
            if (m_Members == null) m_Members = new List<RacePlayerMobile>();
            writer.WriteMobileList<RacePlayerMobile>(m_Members);
        }


        #region Static Methods

        public static void HandleDeath(Mobile victim)
        {
            HandleDeath(victim, null);
        }

        public static void HandleDeath(Mobile victim, Mobile killer)
        {
            // uses for handling death, can be used later
        }
        #endregion



    }
}
