﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;
using Server.Guilds;

namespace Server.Engines
{

    public class GuildPoints
    {
        public GuildPoints()
        {
            Guild = GuildType.None;
            Domain = GuildDomain.None;
            Points = 0;
        }

        public GuildPoints(GuildType guild, GuildDomain domain, int points)
        {
            Guild = guild;
            Domain = domain;
            Points = points;
        }

        public GuildType Guild;
        public GuildDomain Domain;
        public int Points;
    }

}
