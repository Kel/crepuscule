﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;
using Server.Guilds;

namespace Server.Engines
{

    public class GuildGoldPerPoint
    {
        public GuildGoldPerPoint()
        {
            Guild = GuildType.None;
            Domain = GuildDomain.None;
            GoldPerPoint = 0;
        }

        public GuildGoldPerPoint(GuildType guild, GuildDomain domain, int goldPerPoint)
        {
            Guild = guild;
            Domain = domain;
            GoldPerPoint = goldPerPoint;
        }

        public GuildType Guild;
        public GuildDomain Domain;
        public int GoldPerPoint;
    }
   
}
