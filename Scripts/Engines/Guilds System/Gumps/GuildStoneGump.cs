﻿using System;
using Server;
using Server.Mobiles;
using Server.Gumps;
using Server.Network;
using Server.Guilds;

namespace Server.Gumps
{
    public class GuildStoneGump : Gump
    {
        public GuildStoneGump(PlayerMobile mobile)
            : base(0, 0)
        {
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(35, 40, 273, 305, 3000);
            this.AddBackground(34, 15, 275, 31, 9200);
            this.AddLabel(112, 21, 880, @"CHOIX DE GUILDE");
            this.AddImage(264, 272, 9004);
            FillList();
        }

        private void FillList()
        {
            var list = GuildDescriptions.GetNormalGuilds();
            int position = 0;
            foreach (var guild in list)
            {
                this.AddButton(49, 93 + (position * 30), 4023, 4024, (int)guild, GumpButtonType.Reply, 0);
                this.AddLabel(86, 93 + (position * 30), 774, GuildDescriptions.GetGuildDescription(guild));
                position++;
            }
        }


        public override void OnResponse(NetState state, RelayInfo info)
        {
            PlayerMobile mobile = state.Mobile as PlayerMobile;
            if (mobile != null)
            {
                mobile.GuildInfo.PlayerGuild = (GuildType)info.ButtonID;
            }
            
            mobile.CloseGump(typeof(GuildStoneGump));
        }

    }
}
