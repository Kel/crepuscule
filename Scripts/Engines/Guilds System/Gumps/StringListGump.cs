﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Gumps
{
    public class StringListGump : GenericListGump<string>
    {
        public StringListGump(string title,  List<string> items)
            : base(title, 0, 0, items, null)
        {
        }

        public StringListGump(string title, int stretch, List<string> items, List<int> hues)
            : base(title, 0, stretch, items, hues)
        {
        }

        public StringListGump(string title, List<string> items, List<int> hues)
            : base(title, 0, 0, items, hues)
        {
        }

        public StringListGump(string title, string[] items, List<int> hues)
            : base(title, 0, 0, items, hues)
        {
        }

        public StringListGump(string title, int xShift, string[] items, List<int> hues)
            : base(title, xShift, 0, items, hues)
        {
        }
    }
}
