using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Scripts.Commands;

namespace Server.Gumps
{
    public class TreasuryGumpConfig
    {
        public const bool OldStyle = false;

        public const int GumpOffsetX = 30;
        public const int GumpOffsetY = 30;

        public const int TextHue = 0;
        public const int TextOffsetX = 2;

        public const int OffsetGumpID = 0x0A40; // Pure black
        public const int HeaderGumpID = OldStyle ? 0x0BBC : 0x0E14; // Light offwhite, textured : Dark navy blue, textured
        public const int EntryGumpID = 0x0BBC; // Light offwhite, textured
        public const int BackGumpID = 0x13BE; // Gray slate/stoney
        public const int SetGumpID = OldStyle ? 0x0000 : 0x0E14; // Empty : Dark navy blue, textured

        public const int SetWidth = 20;
        public const int SetOffsetX = OldStyle ? 4 : 2, SetOffsetY = 2;
        public const int SetButtonID1 = 0x15E1; // Arrow pointing right
        public const int SetButtonID2 = 0x15E5; // " pressed

        public const int PrevWidth = 20;
        public const int PrevOffsetX = 2, PrevOffsetY = 2;
        public const int PrevButtonID1 = 0x15E3; // Arrow pointing left
        public const int PrevButtonID2 = 0x15E7; // " pressed

        public const int NextWidth = 20;
        public const int NextOffsetX = 2, NextOffsetY = 2;
        public const int NextButtonID1 = 0x15E1; // Arrow pointing right
        public const int NextButtonID2 = 0x15E5; // " pressed

        public const int OffsetSize = 1;

        public const int EntryHeight = 20;
        public const int BorderSize = 10;
    }

    [TypeAlias("Server.Gumps.TresorerieGump")]
    public class TreasuryGump : Gump
    {


        public static bool OldStyle = TreasuryGumpConfig.OldStyle;

        public const int GumpOffsetX = TreasuryGumpConfig.GumpOffsetX;
        public const int GumpOffsetY = TreasuryGumpConfig.GumpOffsetY;

        public const int TextHue = TreasuryGumpConfig.TextHue;
        public const int TextOffsetX = TreasuryGumpConfig.TextOffsetX;

        public const int OffsetGumpID = TreasuryGumpConfig.OffsetGumpID;
        public const int HeaderGumpID = TreasuryGumpConfig.HeaderGumpID;
        public const int EntryGumpID = TreasuryGumpConfig.EntryGumpID;
        public const int BackGumpID = TreasuryGumpConfig.BackGumpID;
        public const int SetGumpID = TreasuryGumpConfig.SetGumpID;

        public const int SetWidth = TreasuryGumpConfig.SetWidth;
        public const int SetOffsetX = TreasuryGumpConfig.SetOffsetX, SetOffsetY = TreasuryGumpConfig.SetOffsetY;
        public const int SetButtonID1 = TreasuryGumpConfig.SetButtonID1;
        public const int SetButtonID2 = TreasuryGumpConfig.SetButtonID2;

        public const int PrevWidth = TreasuryGumpConfig.PrevWidth;
        public const int PrevOffsetX = TreasuryGumpConfig.PrevOffsetX, PrevOffsetY = TreasuryGumpConfig.PrevOffsetY;
        public const int PrevButtonID1 = TreasuryGumpConfig.PrevButtonID1;
        public const int PrevButtonID2 = TreasuryGumpConfig.PrevButtonID2;

        public const int NextWidth = TreasuryGumpConfig.NextWidth;
        public const int NextOffsetX = TreasuryGumpConfig.NextOffsetX, NextOffsetY = TreasuryGumpConfig.NextOffsetY;
        public const int NextButtonID1 = TreasuryGumpConfig.NextButtonID1;
        public const int NextButtonID2 = TreasuryGumpConfig.NextButtonID2;

        public const int OffsetSize = TreasuryGumpConfig.OffsetSize;

        public const int EntryHeight = TreasuryGumpConfig.EntryHeight;
        public const int BorderSize = TreasuryGumpConfig.BorderSize;

        private static bool PrevLabel = false, NextLabel = false;

        private const int PrevLabelOffsetX = PrevWidth + 1;
        private const int PrevLabelOffsetY = 0;

        private const int NextLabelOffsetX = -29;
        private const int NextLabelOffsetY = 0;

        private const int EntryWidth = 300;
        private const int EntryCount = 25;

        private const int TotalWidth = OffsetSize + EntryWidth + OffsetSize + SetWidth + OffsetSize;
        private const int TotalHeight = OffsetSize + ((EntryHeight + OffsetSize) * (EntryCount + 1));

        private const int BackWidth = BorderSize + TotalWidth + BorderSize;
        private const int BackHeight = BorderSize + TotalHeight + BorderSize;

        private Mobile m_Owner;
        private ArrayList m_Mobiles;
        private int m_Page;

        private class InternalComparer : IComparer
        {
            public static readonly IComparer Instance = new InternalComparer();


            public InternalComparer()
            {
            }

            public int Compare(object x, object y)
            {
                if (x == null && y == null)
                    return 0;
                else if (x == null)
                    return -1;
                else if (y == null)
                    return 1;

                Mobile a = x as Mobile;
                Mobile b = y as Mobile;

                if (a == null || b == null)
                    throw new ArgumentException();

                if (a.AccessLevel > b.AccessLevel)
                    return -1;
                else if (a.AccessLevel < b.AccessLevel)
                    return 1;
                else
                    return Insensitive.Compare(((RacePlayerMobile)a).PlayerName, ((RacePlayerMobile)b).PlayerName);
            }
        }


        public TreasuryBook m_Book;

        public TreasuryGump(Mobile owner, ArrayList list, int page, TreasuryBook book)
            : base(GumpOffsetX, GumpOffsetY)
        {
            owner.CloseGump(typeof(WhoGump));

            m_Book = book;
            m_Owner = owner;
            m_Mobiles = list;
            m_Mobiles.Sort(InternalComparer.Instance);

            Initialize(page);
        }

        public static ArrayList BuildList(Mobile owner)
        {
            ArrayList list = new ArrayList();
            /*ArrayList states = NetState.Instances;

            for ( int i = 0; i < states.Count; ++i )
            {
                Mobile m = ((NetState)states[i]).Mobile;

                if ( m != null && (m == owner || !m.Hidden || owner.AccessLevel > m.AccessLevel) )
                    list.Add( m );
            }*/

            list.Sort(InternalComparer.Instance);

            return list;
        }

        public void Initialize(int page)
        {
            m_Page = page;

            //int count = m_Mobiles.Count - (page * EntryCount);
            int count = m_Mobiles.Count - (page * EntryCount);

            if (count < 0)
                count = 0;
            else if (count > EntryCount)
                count = EntryCount;



            int totalHeight = OffsetSize + ((EntryHeight + OffsetSize) * (count + 1));

            AddPage(0);

            AddBackground(0, 0, BackWidth, BorderSize + totalHeight + BorderSize, BackGumpID);
            AddImageTiled(BorderSize, BorderSize, TotalWidth - (OldStyle ? SetWidth + OffsetSize : 0), totalHeight, OffsetGumpID);

            int x = BorderSize + OffsetSize;
            int y = BorderSize + OffsetSize;


            int emptyWidth = TotalWidth - PrevWidth - NextWidth - (OffsetSize * 4) - (OldStyle ? SetWidth + OffsetSize : 0);

            if (!OldStyle)
                AddImageTiled(x - (OldStyle ? OffsetSize : 0), y, emptyWidth + (OldStyle ? OffsetSize * 2 : 0), EntryHeight, EntryGumpID);

            AddLabel(x + TextOffsetX, y, TextHue, String.Format("Page {0} de {1} ({2})", page + 1, (m_Mobiles.Count + EntryCount - 1) / EntryCount, m_Mobiles.Count));

            x += emptyWidth + OffsetSize;

            if (OldStyle)
                AddImageTiled(x, y, TotalWidth - (OffsetSize * 3) - SetWidth, EntryHeight, HeaderGumpID);
            else
                AddImageTiled(x, y, PrevWidth, EntryHeight, HeaderGumpID);

            if (page > 0)
            {
                AddButton(x + PrevOffsetX, y + PrevOffsetY, PrevButtonID1, PrevButtonID2, 1, GumpButtonType.Reply, 0);

                if (PrevLabel)
                    AddLabel(x + PrevLabelOffsetX, y + PrevLabelOffsetY, TextHue, "Prec.");
            }

            x += PrevWidth + OffsetSize;

            if (!OldStyle)
                AddImageTiled(x, y, NextWidth, EntryHeight, HeaderGumpID);

            if ((page + 1) * EntryCount < m_Mobiles.Count)
            {
                AddButton(x + NextOffsetX, y + NextOffsetY, NextButtonID1, NextButtonID2, 2, GumpButtonType.Reply, 1);

                if (NextLabel)
                    AddLabel(x + NextLabelOffsetX, y + NextLabelOffsetY, TextHue, "Suiv.");
            }

            for (int i = 0, index = page * EntryCount; i < EntryCount && index < m_Mobiles.Count; ++i, ++index)
            {

                x = BorderSize + OffsetSize;
                y += EntryHeight + OffsetSize;

                //World.Broadcast(3, false, String.Format("{0}, {1}", x, y));
                try
                {
                    var m = m_Mobiles[index] as RacePlayerMobile;
                    if (m == null)
                        continue;
                        //CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, "TreasuryBook: Mobile is null!");

                    if (m.AccessLevel == AccessLevel.Player)
                    {
                        int HueCotation = 0;
                        AddImageTiled(x, y, EntryWidth, EntryHeight, EntryGumpID);

                        string texte = String.Format("{0}, {1}", ((RacePlayerMobile)m).SalaryInfo.EmployeeName, FormatHelper.GetFormatedGoldAmount((double)m.SalaryInfo.Salary));
                        AddLabelCropped(x + TextOffsetX, y, EntryWidth - TextOffsetX, EntryHeight, HueCotation, m.Deleted ? "(effac�)" : texte);
                    }
                    else
                    {
                        AddImageTiled(x, y, EntryWidth, EntryHeight, EntryGumpID);
                        AddLabelCropped(x + TextOffsetX, y, EntryWidth - TextOffsetX, EntryHeight, GetHueFor(m), m.Deleted ? "(effac�)" : m.SalaryInfo.EmployeeName);
                    }

                    x += EntryWidth + OffsetSize;

                    if (SetGumpID != 0)
                        AddImageTiled(x, y, SetWidth, EntryHeight, SetGumpID);

                    if (!m.Deleted)
                        AddButton(x + SetOffsetX, y + SetOffsetY, SetButtonID1, SetButtonID2, i + 3, GumpButtonType.Reply, 0);
                }
                catch (Exception ex) 
                {
                    CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, String.Format("{0}, {1}, {2}", ex.Message, ex.StackTrace, ex.TargetSite));
                }
            }
        }

        private static string SpaceFill(string mot, int taille)
        {
            string tempmot;
            if (taille > mot.Length)
            {
                tempmot = mot;
                for (int i = mot.Length; i < taille; i++)
                {
                    tempmot += " ";
                }
                return tempmot;
            }
            return mot;
        }


        private static int GetHueFor(Mobile m)
        {
            switch (m.AccessLevel)
            {
                case AccessLevel.Administrator: return 0x516;
                case AccessLevel.Seer: return 0x144;
                case AccessLevel.GameMaster: return 0x21;
                case AccessLevel.Counselor: return 0x2;
                case AccessLevel.Player:
                default:
                    {
                        if (m.Kills >= 5)
                            return 0x21;
                        else if (m.Criminal)
                            return 0x3B1;

                        return 0x58;
                    }
            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            Mobile from = state.Mobile;

            switch (info.ButtonID)
            {
                case 0: // Closed
                    {
                        return;
                    }
                case 1: // Previous
                    {
                        if (m_Page > 0)
                            from.SendGump(new TreasuryGump(from, m_Mobiles, m_Page - 1, m_Book));

                        break;
                    }
                case 2: // Next
                    {
                        if ((m_Page + 1) * EntryCount < m_Mobiles.Count)
                            from.SendGump(new TreasuryGump(from, m_Mobiles, m_Page + 1, m_Book));

                        break;
                    }
                default:
                    {
                        int index = (m_Page * EntryCount) + (info.ButtonID - 3);

                        if (index >= 0 && index < m_Mobiles.Count)
                        {
                            Mobile m = (Mobile)m_Mobiles[index];

                            if (m.Deleted)
                            {
                                from.SendMessage("[HRP] Ce joueur a effac� son personnage.");
                                from.SendGump(new TreasuryGump(from, m_Mobiles, m_Page, m_Book));
                            }
                            else// if ( m == m_Owner || m_Owner.AccessLevel > m.AccessLevel )
                            {
                                from.SendGump(new SalaireChoixGump((RacePlayerMobile)m));
                            }
                            /*else
                            {
                                from.SendMessage("[HRP] Vous ne pouvez pas les voir.");
                                from.SendGump(new TreasuryGump(from, m_Mobiles, m_Page, m_Book));
                            }*/
                        }

                        break;
                    }
            }
        }





        public class SalaireChoixGump : Gump
        {
            RacePlayerMobile m_pour;
            //protected TresorerieBook m_Book;
            public SalaireChoixGump(RacePlayerMobile pour)
                : base(0, 0)
            {
                // m_Book = Livre;
                m_pour = pour;
                this.Closable = false;
                this.Disposable = true;
                this.Dragable = true;
                this.Resizable = false;
                this.AddPage(0);
                this.AddBackground(74, 74, 254, 102, 9300);
                this.AddBackground(73, 47, 257, 31, 9200);
                this.AddLabel(145, 54, 0, @"Choisissez le salaire");
                this.AddTextEntry(99, 102, 203, 20, 0, (int)Buttons.AgeTxt, @"" + m_pour.SalaryInfo.Salary.ToString());
                this.AddButton(167, 140, 247, 248, (int)Buttons.Button2, GumpButtonType.Reply, 0);

            }

            public enum Buttons
            {
                AgeTxt,
                Button2,
            }

            public override void OnResponse(NetState state, RelayInfo info)
            {
                RacePlayerMobile from = state.Mobile as RacePlayerMobile;
                RacePlayerMobile mobile = m_pour;

                int button = info.ButtonID;


                switch (button)
                {
                    case 0:

                        break;

                    case 1:
                        TextRelay relay = info.GetTextEntry(0);
                        int newText = -1;

                        try
                        {
                            newText = Utility.ToInt32(relay.Text);
                        }
                        catch
                        {
                            from.SendMessage("Le salaire est incorrect.");
                            from.CloseGump(typeof(SalaireChoixGump));
                            from.SendGump(new SalaireChoixGump(mobile));
                        }

                        string age_introduit = (relay == null ? null : relay.Text.Trim());

                        if (age_introduit.Length > 5 || age_introduit.Length == 0)
                        {
                            from.SendMessage("Le salaire est incorrect.");
                            from.CloseGump(typeof(SalaireChoixGump));
                            from.SendGump(new SalaireChoixGump(mobile));
                        }
                        else
                        {
                            if (newText >= 0 && newText <= 1000)
                            {
                                mobile.SalaryInfo.Salary = (int)newText;
                                from.SendMessage("Le salaire est chang�.");
                                from.CloseGump(typeof(SalaireChoixGump));
                            }
                            else
                            {
                                from.SendMessage("Le salaire est incorrect.");
                                from.CloseGump(typeof(SalaireChoixGump));
                                from.SendGump(new SalaireChoixGump(mobile));
                            }
                        }

                        break;

                }



            }


        }



    }

}