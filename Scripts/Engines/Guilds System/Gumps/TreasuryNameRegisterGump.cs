﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;

namespace Server.Gumps
{
    [TypeAlias("Server.Gumps.TresorerieNameRegisterGump")]
    public class TreasuryNameRegisterGump : Gump
    {
        RacePlayerMobile m_pour;
        //protected TresorerieBook m_Book;
        public TreasuryNameRegisterGump(RacePlayerMobile pour)
            : base(0, 0)
        {
            // m_Book = Livre;
            m_pour = pour;
            this.Closable = false;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(74, 74, 254, 102, 9300);
            this.AddBackground(73, 47, 257, 31, 9200);
            this.AddLabel(145, 54, 0, @"Choisissez le nom");
            this.AddTextEntry(99, 102, 203, 20, 0, (int)Buttons.AgeTxt, @" ");
            this.AddButton(167, 140, 247, 248, (int)Buttons.Button2, GumpButtonType.Reply, 0);

        }

        public enum Buttons
        {
            AgeTxt,
            Button2,
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            RacePlayerMobile from = state.Mobile as RacePlayerMobile;
            RacePlayerMobile mobile = m_pour;

            int button = info.ButtonID;


            switch (button)
            {
                case 0:

                    break;

                case 1:
                    TextRelay relay = info.GetTextEntry(0);



                    string nom_introduit = (relay == null ? null : relay.Text.Trim());

                    if (nom_introduit.Length > 25 || nom_introduit.Length == 0)
                    {
                        from.SendMessage("Le nom est incorrect.");
                        from.CloseGump(typeof(TreasuryNameRegisterGump));
                        from.SendGump(new TreasuryNameRegisterGump(mobile));
                    }
                    else
                    {

                        from.SalaryInfo.EmployeeName = nom_introduit;
                        from.SendMessage("Vous êtes inscrit dans le livre.");
                        from.CloseGump(typeof(TreasuryNameRegisterGump));

                    }

                    break;

            }



        }


    }

}