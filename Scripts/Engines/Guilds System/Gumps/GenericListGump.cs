﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Gumps
{
    public class GenericListGump<T> : Server.Gumps.Gump
    {
        internal string Title = "";
        internal int TitleXShift = 0;
        internal IEnumerable<T> Items;
        internal List<int> HuesList = null;
        internal int Page;
        internal int XStretch;
        internal int StartPosition = 0;
        internal int EndPosition = 0;

        private const int EntryCount = 14;

        public GenericListGump(string title, IEnumerable<T> items)
            : this(title,0,0,items, null)
        {
        }

        public GenericListGump(string title, int xStretch, IEnumerable<T> items)
            : this(title, 0, xStretch, items, null)
        {
        }

        public GenericListGump(string title, int titleXShift, int xStretch, IEnumerable<T> items, List<int> huesList)
            : this(title,  titleXShift, xStretch, items,  huesList, 0)
        {

        }

        public GenericListGump(string title, int titleXShift, int xStretch, IEnumerable<T> items, List<int> huesList, int page)
            : base(0, 0)
        {
            Items = items;
            Title = title;
            TitleXShift -= (int)(xStretch / 2);
            TitleXShift -= titleXShift;
            if(huesList != null)
                HuesList = huesList;
            XStretch = xStretch;
            Page = page;

            this.Closable = true;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(154, 70, 465 - xStretch, 443, 9200);
            this.AddBackground(162, 77, 451 - xStretch, 27, 9350);
            this.AddBackground(161, 107, 451 - xStretch, 389, 9350);
            
            this.AddImage(103, 43, 10400);
            this.AddImage(587 - xStretch, 262, 10410);
            this.AddImage(104, 204, 10401);
            this.AddImage(587 - xStretch, 439, 10412);
            SetInfo();
            SetPages();
            FillList();
        }

        private enum Buttons
        {
            bClose,
            bNextPage,
            bPreviousPage,
        }

        public override void OnResponse(Server.Network.NetState sender, RelayInfo info)
        {
            base.OnResponse(sender, info);
            if (info.ButtonID == (int)Buttons.bPreviousPage)
            {
                sender.Mobile.CloseGump(this.GetType()) ; 
                sender.Mobile.SendGump(new GenericListGump<T>(Title, TitleXShift, XStretch, Items, HuesList, Page - 1));
            }
            else if (info.ButtonID == (int)Buttons.bNextPage)
            {
                sender.Mobile.CloseGump(this.GetType());
                sender.Mobile.SendGump(new GenericListGump<T>(Title, TitleXShift, XStretch, Items, HuesList, Page + 1));
            }
            else
            {
                sender.Mobile.CloseGump(this.GetType());
            }
        }

        private void SetPages()
        {
            int ItemsCount = Items.Count();
            if (ItemsCount > EntryCount)
            { //Do Pages
                if (Page > 0)
                {
                    this.AddButton(169, 468, 4014, 4015, (int)Buttons.bPreviousPage, GumpButtonType.Reply, 0);
                    StartPosition = Page * EntryCount;
                }
                else
                {
                    StartPosition = 0;
                }

                if (((Page + 1) * EntryCount) < ItemsCount)
                { //There's more...
                    this.AddButton(573 - XStretch, 468, 4005, 4006, (int)Buttons.bNextPage, GumpButtonType.Reply, 0);
                }
                EndPosition = (Page + 1) * EntryCount;
            }
            else
            {
                StartPosition = 0;
                EndPosition = ItemsCount;
            }
        }

        private void SetInfo()
        {
            //this.AddLabel(349 + TitleXShift, 81, 100, Title);
            this.AddLabel(172, 81, 100, Title);
        }

        private void FillList()
        {

            int position = 0;
            int entry = 0;
            int huesCount = HuesList != null ? HuesList.Count : 0;
            foreach (T item in Items)
            {
                if (position >= StartPosition && position < EndPosition)
                {
                    int hueNumber = 250; // default
                    if (HuesList != null && huesCount > position)
                    {
                        hueNumber = HuesList[position];
                    }
                    this.AddLabel(172, 116 + (entry * 23), hueNumber, item.ToString());
                    entry++;
                }
                position++;
            }

        }

    }
}

