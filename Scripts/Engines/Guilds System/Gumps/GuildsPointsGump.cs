using System;
using Server;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Mobiles;
using Server.Network;
using Server.Guilds;

namespace Server.Gumps
{
	public class GuildsPointsGump : Gump
	{
		public GuildsPointsGump()
			: base( 0, 0 )
		{
			this.Closable=false;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(16, 10, 593, 433, 9200);
			this.AddBackground(26, 220, 192, 213, 9350);
			this.AddButton(529, 410, 4023, 4024, (int)Buttons.ButtonConfirm, GumpButtonType.Reply, 0);
			this.AddButton(568, 410, 4017, 4018, (int)Buttons.ButtonCancel, GumpButtonType.Reply, 0);
			this.AddBackground(21, 17, 581, 25, 9350);
			this.AddLabel(260, 19, 874, @"Cotation des guildes");
			this.AddLabel(38, 228, 874, @"Guilde à coter:");
			this.AddBackground(224, 51, 146, 300, 9350);
			this.AddBackground(371, 49, 231, 301, 9350);
			this.AddLabel(384, 56, 874, @"Commentaire:");
			this.AddTextEntry(381, 83, 209, 255, 0, (int)Buttons.Comment, @"");
			this.AddBackground(25, 50, 194, 165, 9350);
            this.AddLabel(448, 412, 0, @"Confirmer?");
			this.AddLabel(230, 362, 0, @"Rappel: Afin de côter équitablement et de façon impartiale,");
			this.AddLabel(276, 382, 0, @" consultez la charte de cotation!");
            this.AddLabel(234, 57, 874, @"Pts. Guilde/Autres:");
            
            //Add Radios
            this.AddGroup(1);

            this.AddLabel(37, 58, 874, @"Domaine:");
            this.AddRadio(35, 84, 210, 211, true, (int)Buttons.DefenseRadio);
            this.AddLabel(58, 84, 195, @"Protéction de l'Empire");
            this.AddRadio(34, 107, 210, 211, false, (int)Buttons.WealthRadio);
            this.AddLabel(58, 108, 195, @"Contrôle de la Richesse ");
            this.AddRadio(35, 133, 210, 211, false, (int)Buttons.MagicRadio);
            this.AddLabel(58, 133, 195, @"Contrôle de la Magie ");
            this.AddRadio(35, 159, 210, 211, false, (int)Buttons.ReligionRadio);
            this.AddLabel(58, 159, 195, @"Promotion de la Religion ");
            this.AddRadio(35, 183, 210, 211, false, (int)Buttons.KnowledgeRadio);
            this.AddLabel(58, 183, 195, @"Contrôle du Savoir ");

            this.AddGroup(2);

            FillRewards();
            //this.AddRadio(235, 84, 210, 211, false, (int)Buttons.PointsRadio);
            //this.AddLabel(259, 84, 1518, @"+1");


            this.AddGroup(3);

            FillGuilds();
            //this.AddRadio(36, 254, 210, 211, false, (int)Buttons.GuildRadio);
            //this.AddLabel(62, 255, 339, @"New Label");

		}

        private void FillGuilds()
        {
            List<GuildType> guilds = GuildsSystem.GetAllActiveGuilds();
            for (int i = 0; i < guilds.Count; i++)
            {
                this.AddRadio(36, 255 + (24 * i), 210, 211, (i == 0), (int)100 + (int)guilds[i]); // IMPORTANT: id = 100 + guilde type
                this.AddLabel(62, 255 + (24 * i), 339, FormatHelper.GetGuildDescription(guilds[i]));
            }
        }


        private void FillRewards()
        {
            List<GuildPointsReward> rewards = GuildPointsReward.StandardList;
            for (int i = 0; i < rewards.Count; i++)
            {
                this.AddRadio(235, 84 + (24 * i), 210, 211, (i == 0), (int)200 + i); // IMPORTANT: id = 200 + reward index
                this.AddLabel(259, 84 + (24 * i), 1518, rewards[i].ToString());
            }
        }

        public override void OnResponse(Server.Network.NetState sender, RelayInfo info)
        {
            base.OnResponse(sender, info);
            if (info.ButtonID == (int)Buttons.ButtonConfirm)
            {
                GuildDomain DomainSelected = GuildDomain.None;
                GuildType GuildSelected = GuildType.None;
                GuildPointsReward RewardSelected = null;

                foreach (int swt in info.Switches)
                {
                    if (swt >= (int)Buttons.DefenseRadio && swt <= (int)Buttons.MagicRadio)
                    {
                        DomainSelected = (GuildDomain)(int)swt;
                    }
                    else if (swt >= 100 && swt < 200)
                    {
                        GuildSelected = (GuildType)(swt - 100);
                    }
                    else if (swt >= 200 )
                    {
                        RewardSelected = GuildPointsReward.StandardList[swt - 200];
                    }
                }

                if (RewardSelected != null && GuildSelected != GuildType.None && DomainSelected != GuildDomain.None)
                {
                    Mobile from = sender.Mobile;

                    BroadcastMessage(AccessLevel.Counselor, 34, String.Format("{0} [{1}] a coté la guilde {2}, domaine {3} de {4}",
                        from.Name.ToString(), sender.Account.ToString(), FormatHelper.GetGuildDescription(GuildSelected), FormatHelper.GetGuildDomainDescription(DomainSelected),
                        RewardSelected.ToString()));

                    GuildPointsReward newReward = new GuildPointsReward(GuildSelected,
                        DomainSelected, RewardSelected.RewardSelf,
                        RewardSelected.RewardOthers, sender.Account.ToString(), DateTime.Now, info.TextEntries[0].Text);

                    GuildsSystem.AddReward(newReward);
                }
            }
        }


        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;

                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }
		
		public enum Buttons
		{
            Comment,
			DefenseRadio,
			WealthRadio,
			ReligionRadio,
			KnowledgeRadio,
            MagicRadio,
            GuildRadio,
            ButtonConfirm,
            ButtonCancel,
            PointsRadio
		}

	}
}