﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Guilds;
using Server.Scripts.Commands;

namespace Server.Gumps
{
    public class GuildInfoGump : Server.Gumps.Gump
    {
        private PlayerMobile player;
        public GuildInfoGump(PlayerMobile player)
            : base(0, 0)
        {
            this.player = player;
            this.Closable = true;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(61, 93, 636, 425, 9200);
            this.AddBackground(69, 100, 621, 34, 9350);
            this.AddBackground(70, 177, 308, 333, 9350);
            this.AddImage(665, 229, 10412);
            this.AddBackground(382, 139, 308, 32, 9350);
            this.AddLabel(308, 107, 250, @"GUILDES DE SYSTÉRIA");
            this.AddBackground(70, 139, 308, 32, 9350);
            this.AddBackground(382, 177, 308, 121, 9350);
            this.AddLabel(156, 144, 244, @"Influence des guildes");
            this.AddLabel(462, 146, 244, @"Votre état dans la guilde");
            this.AddLabel(428, 188, 250, @"Votre guilde:");
            this.AddLabel(428, 212, 250, @"Nom salarié:");
            this.AddLabel(428, 235, 250, @"Salaire:");
            this.AddLabel(428, 259, 250, @"Salaire cumulé:");
            this.AddImage(384, 255, 3823);
            this.AddImage(384, 238, 3822);
            this.AddImage(384, 215, 4081);
            this.AddImage(385, 184, 7971);
            this.AddBackground(382, 304, 308, 32, 9350);
            this.AddLabel(426, 310, 244, @"Capacités spéciales de votre guilde");
            this.AddBackground(382, 343, 308, 167, 9350);
            this.AddImage(665, 57, 10410);

            this.AddItem(385, 184, 7971);
            this.AddItem(384, 215, 4081);
            this.AddItem(384, 238, 3822);
            this.AddItem(384, 255, 3823);

            FillGuilds();
            SetInfo();
            SetEmpireInfo();
            FillSkills();
        }

        private void SetInfo()
        {
            this.AddLabel(516, 188, 390, GuildDescriptions.GetGuildDescription(player.GuildInfo.PlayerGuild));
            this.AddLabel(516, 212, 390, player.SalaryInfo.m_EmployeeName);
            this.AddLabel(488, 235, 390, FormatHelper.GetFormatedGoldAmount(player.SalaryInfo.m_Salary));
            this.AddLabel(532, 258, 390, FormatHelper.GetFormatedGoldAmount(player.SalaryInfo.m_SalarySum));
        }

        private void SetEmpireInfo()
        {
            if (player.GuildInfo.PlayerGuild == GuildType.Empire)
            {
                this.AddLabel(112, 482, 244, @"Voir l'état par domaine");
                this.AddButton(76, 481, 4011, 4012, (int)Buttons.bDomainState, GumpButtonType.Reply, 0);
            }
        }

        private void FillSkills()
        {
            int position = 0;

            if (player.GuildInfo.PlayerGuild != GuildType.None)
            {
                var skills = GuildDescriptions.Descriptions[player.GuildInfo.PlayerGuild].Skills;
                foreach (var skill in skills)
                {
                    this.AddLabel(393, 353 + (position * 23), 250, skill.Label);
                    position++;
                }
            }
            if (player.GuildInfo.SecretGuild != GuildType.None)
            {
                var skills = GuildDescriptions.Descriptions[player.GuildInfo.PlayerGuild].Skills;
                foreach (var skill in skills)
                {
                    this.AddLabel(393, 353 + (position * 23), 250, skill.Label);
                    position++;
                }
            }
        }


        private void FillGuilds()
        {
            List<GuildType> guilds = GuildsSystem.GetAllActiveGuilds();
            for (int i = 0; i < guilds.Count; i++)
            {
                var info = String.Format("{0}: Influence est {1}", GuildDescriptions.GetGuildDescription(guilds[i]), GuildDescriptions.GetGuildGlobalInfluenceDescription(guilds[i]));
                this.AddLabel(82, 189 + (24 * i), 50, info);
            }
        }

        private enum Buttons
        {
            bDomainState,
        }

        public override void OnResponse(Server.Network.NetState sender, RelayInfo info)
        {
            base.OnResponse(sender, info);
            if (info.ButtonID == (int)Buttons.bDomainState && player.GuildInfo.PlayerGuild == GuildType.Empire)
            {
                List<int> huesList;
                List<string> text = Gossip.GetGossip(sender.Mobile, out huesList);

                if (text.Count > 0)
                {
                    sender.Mobile.CloseGump(typeof(StringListGump));
                    sender.Mobile.SendGump(new StringListGump("RUMEURS", 150, text, huesList));
                }
                
            }
        }
    }
}

