﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Scripts.Commands;
using Server.Mobiles;
using System.Threading;
using Server.Guilds;

namespace Server.Engines
{

    public class EconomicsTimer : Timer
    {
        //public static TimeSpan PayDelay = new TimeSpan(0, 0, 2, 0);
        //public static TimeSpan TimerDelay = new TimeSpan(0,0, 0, 30);
        public static TimeSpan PayDelay = new TimeSpan(1, 0, 0, 0);
        public static TimeSpan TimerDelay = new TimeSpan(0,0, 5, 0);
        public static EconomicsTimer Timer = null;

        public static void Initialize()
        {
            Timer = new EconomicsTimer();
        }

        public EconomicsTimer()
            : base(TimerDelay, TimerDelay)
        {
            this.Start();
        }

        public void ForcePayment()
        {
            GuildsSystem.LastPayDay = DateTime.Now.Subtract(PayDelay.Add(TimeSpan.FromSeconds(2)));
            OnTick();
        }

        protected override void OnTick()
        {
            if (GuildsSystem.LastPayDay.Add(PayDelay) <= DateTime.Now)
            { // it's payday!!! 
                GuildsSystem.LastPayDay = DateTime.Now;
                CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 13, "Economie: Jour de paye!");

                Dictionary<GuildType, int> Incomes = new Dictionary<GuildType, int>();

                // Generate gold

                var generators = BaseGoldGenContainer.Generators;
                foreach (var generator in generators)
                {
                    if (generator.DailyIncome > 0)
                    {
                        generator.AddItem(new Gold(generator.DailyIncome));
                    }
                }
                

                // fill containers
                if (BaseTreasuryContainer.Tresoreries != null)
                {
                    for (int i = 0; i < BaseTreasuryContainer.Tresoreries.Count; i++)
                    {
                        if (BaseTreasuryContainer.Tresoreries[i] is BaseTreasuryContainer)
                        {
                            BaseTreasuryContainer B = (BaseTreasuryContainer)BaseTreasuryContainer.Tresoreries[i];
                            if (B.Guild == GuildType.None) continue;

                            int income = B.DailyIncome;
                            if (GuildsSystem.GetAllActiveGuilds().Contains(B.Guild))
                            {
                                if (B.Guild != GuildType.Empire && B.Guild != GuildType.Ombres && B.Guild != GuildType.Planaires)
                                {
                                    income = B.m_DailyIncome = GuildsSystem.GetIncome(B.Guild);
                                    int subsidy = GuildsSystem.GetSubsidy(B.Guild);
                                    income += subsidy;
                                }
                            }

                            if (income > 0)
                            {
                                Gold iG = new Gold(income);
                                B.AddItem(iG);
                            }
                            if (!Incomes.ContainsKey(B.Guild))
                            {
                                var Payment = String.Format(" {0} reçoit {1} po",FormatHelper.GetGuildDescription(B.Guild),income);
                                CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 13, Payment );
                                Incomes.Add(B.Guild, income);
                            }
                        }


                    }
                }


                // share salaries
                if (TreasuryBook.TresoreriesBook != null)
                {
                    for (int j = 0; j < TreasuryBook.TresoreriesBook.Count; j++)
                    {
                        if (TreasuryBook.TresoreriesBook[j] is TreasuryBook)
                        {
                            TreasuryBook Book = (TreasuryBook)TreasuryBook.TresoreriesBook[j];
                            if (Incomes.ContainsKey(Book.Guilde))
                            {
                                Book.AddSalaries(Incomes[Book.Guilde]);
                            }
                        }

                    }
                }

                Thread sendThread = new Thread(new ThreadStart(SendRewardsToWeb));
                sendThread.Start();
            }


        }


        private static void SendRewardsToWeb()
        {
            if (GuildsSystem.RewardBatch.Count > 0)
            {
                string Text = "";
                foreach (GuildPointsReward reward in GuildsSystem.RewardBatch)
                {
                    Text += String.Format("[{0}] Guilde: {1} a été recompensée par {2}, dans le domaine {3} ({4} / {5}). Raison: {6} <br />",
                        reward.When.ToLongTimeString(),
                        FormatHelper.GetGuildDescription(reward.Guild),
                        reward.AccountName,
                        FormatHelper.GetGuildDomainDescription(reward.Domain),
                        reward.RewardSelf,
                        reward.RewardOthers,
                        reward.Comment
                        );


                    //Text += Environment.NewLine;
                }

                try
                {
                    string TopicName = DateTime.Now.ToShortDateString() + " - Rapport de cotation";

                    GuildsSystem.RewardBatch.Clear();
                    TopicName = TopicName.Replace("'", @"\'");
                    Text = Text.Replace("'", @"\'");


                    CrepusculeWeb.WS.Service.PostWithScribe("PASSWORD", "138", TopicName, Text);
                    GuildsSystem.RewardBatch.Clear();
                }
                catch{
                    
                }

                
            }
        }
    }
}
