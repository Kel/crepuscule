﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Engines;
using Server.Guilds;

namespace Server.Mobiles
{
    public class GuildTownfolk : BaseGuildMobile
    {
        public GuildTownfolk(Serial serial) : base(serial) { }

        [Constructable]
        public GuildTownfolk() : base(AIType.AI_Animal, FightMode.None, 10, 1, 0.2, 0.4)
        {
            GenerateAppearance();
            InitStats(80, 60, 90);
        }

        public override GuildMobileType MobileType { get { return GuildMobileType.Townfolk; } }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }


    }


}
