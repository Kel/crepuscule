﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Engines;
using Server.Guilds;

namespace Server.Mobiles
{
    public class GuildGuard : BaseGuildMobile
    {
        public GuildGuard(Serial serial) : base(serial) { }

        [Constructable]
        public GuildGuard() : base(AIType.AI_Melee, FightMode.None, 10, 1, 0.2, 0.4)
        {
            GenerateAppearance();
            InitStats(150, 130, 50);

        }

        public override GuildMobileType MobileType{ get{ return GuildMobileType.Guard; } }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }


    }


}
