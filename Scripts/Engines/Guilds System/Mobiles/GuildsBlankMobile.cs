﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;

namespace Server.Mobiles
{

    public class GuildsBlankMobile : BaseCreature
    {
        public GuildsBlankMobile() : base(AIType.AI_Animal, FightMode.None, 10, 1, 0.2, 0.4)
        {

        }
        public GuildsBlankMobile(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }


    }


}
