﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Engines;
using Server.Guilds;

namespace Server.Mobiles
{

    public class BaseGuildMobile : BaseCreature
    {
        public BaseGuildMobile(Serial serial) : base(serial) { RegisterSelf(); NameHue = 0x35; }
        public BaseGuildMobile(AIType ai, FightMode mode, int iRangePerception, int iRangeFight, double dActiveSpeed, double dPassiveSpeed)
            : base(ai, mode, iRangePerception, iRangeFight, dActiveSpeed, dPassiveSpeed)
        {
            RegisterSelf();
            GenerateFreeAppearance();
            GenerateAppearance();
            NameHue = 0x35;
        }

        #region Properties
        private GuildType m_Guild = GuildType.None;
        private GuildDomain m_Domain = GuildDomain.None;

        public virtual bool IsInvulnerable { get { return true; } }
        public virtual GuildMobileType MobileType { get { return GuildMobileType.Townfolk; } }

        #region Disable Local Names
        //public override void OnAosSingleClick(Mobile from)
        //{
        //    //base.OnAosSingleClick(from);
        //}

        //public override void OnSingleClick(Mobile from)
        //{
        //    //base.OnSingleClick(from);
        //}
        #endregion

        public GuildDomain Domain
        {
            get { return m_Domain; }
            set { m_Domain = value; }
        }


        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType GuildOfMobile
        {
            get { return m_Guild; }
            set 
            { 
                if(m_Guild != value)
                {
                    SetGuild(value);
                }
            }
        }

        public void SetGuild(GuildType value)
        {
            //Unregister & Register
            if (GuildsSystem.GuildMobilesPerGuild.ContainsKey(m_Guild))
            {
                //Server.Scripts.Commands.CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, "Changing guild from " + m_Guild.ToString() + " to " + value.ToString());
                GuildsSystem.GuildMobilesPerGuild[m_Guild].Remove(this);
            }
            if (!GuildsSystem.GuildMobilesPerGuild.ContainsKey(value))
                GuildsSystem.GuildMobilesPerGuild.Add(value, new List<BaseGuildMobile>());
            GuildsSystem.GuildMobilesPerGuild[value].Add(this);

            //Server.Scripts.Commands.CommandHandlers.BroadcastMessage(AccessLevel.Administrator, 3, "Changing guild from " + m_Guild.ToString() + " to " + value.ToString());
            m_Guild = value;


            if (m_Guild == GuildType.None)
            {
                switch (Utility.Random(2))
                {
                    case 0: this.Female = false; break;
                    case 1: this.Female = true; break;
                }

                GenerateFreeAppearance();
                GenerateAppearance();
                GenerateSayings(m_Guild);
            }
            else
            {
                Mobile sample = null;
                if (GuildsSystem.GuildsDomainStones.ContainsKey(m_Guild))
                {
                    switch (Utility.Random(2))
                    {
                        case 0: this.Female = false; break;
                        case 1: this.Female = true; break;
                    }

                    if (MobileType == GuildMobileType.Diplomat)
                    {
                        if (this.Female) sample = GuildsSystem.GuildsDomainStones[m_Guild].m_MobileDiplomatFemale;
                        else sample = GuildsSystem.GuildsDomainStones[m_Guild].m_MobileDiplomat;
                    }
                    else if (MobileType == GuildMobileType.Guard)
                    {
                        if (this.Female) sample = GuildsSystem.GuildsDomainStones[m_Guild].m_MobileGuardFemale;
                        else sample = GuildsSystem.GuildsDomainStones[m_Guild].m_MobileGuard;
                    }
                    else if (MobileType == GuildMobileType.Townfolk)
                    {
                        if (this.Female) sample = GuildsSystem.GuildsDomainStones[m_Guild].m_MobileTownfolkFemale;
                        else sample = GuildsSystem.GuildsDomainStones[m_Guild].m_MobileTownfolk;
                    }

                    if (sample != null)
                    {
                        CopyEquip.CopyEquipment(sample, this, false, false);
                        GenerateAppearance();
                    }
                }
                GenerateSayings(m_Guild);
            }
        }
        #endregion

        #region Methods

        #region Overrides
        public override void OnDelete()
        {
            UnRegisterSelf();
            base.OnDelete();
        }

        public override bool CanBeDamaged()
        {
            return !IsInvulnerable;
        }
        #endregion

        #region Get Hues
        public virtual int GetHairHue()
        {
            return Utility.RandomHairHue();
        }
        public virtual int GetRandomHue()
        {
            switch (Utility.Random(10))
            {
                default:
                case 0: return Utility.RandomBlueHue();
                case 1: return Utility.RandomGreenHue();
                case 2: return Utility.RandomRedHue();
                case 3: return Utility.RandomYellowHue();
                case 4: return Utility.RandomNeutralHue();
                case 5: return Utility.RandomNeutralHue();
                case 6: return Utility.RandomNeutralHue();
                case 7: return Utility.RandomNeutralHue();
                case 8: return Utility.RandomNeutralHue();
                case 9: return Utility.RandomNeutralHue();
            }
        }

        public virtual int GetShoeHue()
        {
            if (0.1 > Utility.RandomDouble())
                return 0;

            return Utility.RandomNeutralHue();
        }


        #endregion

        #region Appearance
        public virtual void InitOutfit()
        {
            switch (Utility.Random(20))
            {
                case 0: AddItem(new Chapeau_tricorne1(GetRandomHue())); break;
                case 1: AddItem(new Chapeau_tricorne2(GetRandomHue())); break;
                case 2: AddItem(new Chapeau_turban1(GetRandomHue())); break;
                case 3: AddItem(new FlowerGarland(GetRandomHue())); break;
                case 4: AddItem(new WideBrimHat(GetRandomHue())); break;
                case 5: AddItem(new FloppyHat(GetRandomHue())); break;
                case 6: AddItem(new Cap(GetRandomHue())); break;
                case 7: AddItem(new Bandana(GetRandomHue())); break;
                case 8: AddItem(new TallStrawHat(GetRandomHue())); break;
                case 9: AddItem(new StrawHat(GetRandomHue())); break;
                case 10: AddItem(new WizardsHat(GetRandomHue())); break;
            }

            switch (Utility.Random(7))
            {
                case 0: AddItem(new CapePoil(GetShoeHue())); break;
                case 1: AddItem(new Cloak(GetShoeHue())); break;
                case 2: AddItem(new CapeNoble(GetShoeHue())); break;
            }

            if (MobileType == GuildMobileType.Townfolk)
            {
                switch (Utility.Random(2))
                {
                    case 0: AddItem(new Lantern()); break;
                }

            }
            else if (MobileType == GuildMobileType.Guard)
            {
                switch (Utility.Random(17))
                {
                    case 0: AddItem(new Axe()); break;
                    case 1: AddItem(new Pickaxe()); break;
                    case 2: AddItem(new LargeBattleAxe()); break;
                    case 3: AddItem(new TwoHandedAxe()); break;
                    case 4: AddItem(new WarAxe()); break;
                    case 5: AddItem(new DoubleAxe()); break;
                    case 6: AddItem(new ButcherKnife()); break;
                    case 7: AddItem(new SkinningKnife()); break;
                    case 8: AddItem(new Dagger()); break;
                    case 9: AddItem(new Club()); break;
                    case 10: AddItem(new Mace()); break;
                    case 11: AddItem(new Maul()); break;
                    case 12: AddItem(new Mace()); break;
                    case 13: AddItem(new WarHammer()); break;
                    case 14: AddItem(new Scythe()); break;
                    case 15: AddItem(new Shovel()); break;
                    case 16: AddItem(new Halberd()); break;
                }
            }



            switch (Utility.Random(7))
            {
                case 0: AddItem(new Shoes(GetShoeHue())); break;
                case 1: AddItem(new Boots(GetShoeHue())); break;
                case 2: AddItem(new Sandals(GetShoeHue())); break;
                case 3: AddItem(new ThighBoots(GetShoeHue())); break;
                case 4: AddItem(new bottes_cuir(GetShoeHue())); break;
                case 5: AddItem(new bottes_poil(GetShoeHue())); break;
                case 6: AddItem(new bottes_musket(GetShoeHue())); break;
            }

            
            if (Female)
            {

                switch (Utility.Random(5))
                {
                    case 1: AddItem(new robe_seduisante(GetRandomHue())); break;
                    case 2: AddItem(new robe_elegante(GetRandomHue())); break;
                    case 3: AddItem(new robe_amusante(GetRandomHue())); break;
                    case 4: AddItem(new robe_gitane(GetRandomHue())); break;
                }
            }
            else
            {
                switch (Utility.Random(7))
                {
                    case 0: AddItem(new FancyShirt(GetRandomHue())); break;
                    case 1: AddItem(new Doublet(GetRandomHue())); break;
                    case 2: AddItem(new Shirt(GetRandomHue())); break;
                    case 3: AddItem(new chemise_large(GetRandomHue())); break;
                    case 4: AddItem(new chemise1(GetRandomHue())); break;
                    case 5: AddItem(new chemise2(GetRandomHue())); break;
                    case 6: AddItem(new chemise3(GetRandomHue())); break;

                }

                switch (Utility.Random(5))
                {
                    case 0: AddItem(new LongPants(GetRandomHue())); break;
                    case 1: AddItem(new ShortPants(GetRandomHue())); break;
                    case 2: AddItem(new Pantalon1(GetRandomHue())); break;
                    case 3: AddItem(new Pantalon2(GetRandomHue())); break;
                    case 4: AddItem(new Pantalon3(GetRandomHue())); break;
                }

            }

        }

        public void GenerateSayings(GuildType guild)
        {
            if (TownfolkSayings.SayingsList.ContainsKey(guild))
            {
                string[] list = TownfolkSayings.SayingsList[guild];
                int listCount = list.Length;
                this.Saying1 = list[Utility.Random(listCount)];
                this.Saying2 = list[Utility.Random(listCount)];
                this.Saying3 = list[Utility.Random(listCount)];
                this.Saying4 = list[Utility.Random(listCount)];
                this.Saying5 = list[Utility.Random(listCount)];

                this.SayingsEnabled = true;
                this.SayingsSecondsDelay = 1800;
            }
            else
            {
                this.SayingsEnabled = false;
            }
        }

        public virtual void GenerateAppearance()
        {
            SpeechHue = Utility.RandomDyedHue(); // speach colour
            int hairHue = GetHairHue();

            if (this.Female)
            {
                this.Name = NameList.RandomName("female");
                this.Body = 0x191;
                
                switch (Utility.Random(15))
                {
                    case 0: AddItem(new Afro(hairHue)); break;
                    case 1: AddItem(new KrisnaHair(hairHue)); break;
                    case 2: AddItem(new PageboyHair(hairHue)); break;
                    case 3: AddItem(new PonyTail(hairHue)); break;
                    case 4: AddItem(new ReceedingHair(hairHue)); break;
                    case 5: AddItem(new TwoPigTails(hairHue)); break;
                    case 6: AddItem(new ShortHair(hairHue)); break;
                    case 7: AddItem(new LongHair(hairHue)); break;
                    case 8: AddItem(new BunsHair(hairHue)); break;
                    case 9: AddItem(new TresLongues(hairHue)); break;
                    case 10: AddItem(new CalvLong(hairHue)); break;
                    case 11: AddItem(new ChevCouette(hairHue)); break;
                    case 12: AddItem(new ChevLisse(hairHue)); break;
                    case 13: AddItem(new Lulu(hairHue)); break;
                    case 14: AddItem(new ChevToupet(hairHue)); break;
                }
            }
            else
            {
                this.Name = NameList.RandomName("male");
                this.Body = 0x190;

                switch (Utility.Random(12))
                {
                    case 0: AddItem(new Afro(hairHue)); break;
                    case 1: AddItem(new KrisnaHair(hairHue)); break;
                    case 2: AddItem(new PageboyHair(hairHue)); break;
                    case 3: AddItem(new PonyTail(hairHue)); break;
                    case 4: AddItem(new ReceedingHair(hairHue)); break;
                    case 5: AddItem(new TwoPigTails(hairHue)); break;
                    case 6: AddItem(new ShortHair(hairHue)); break;
                    case 7: AddItem(new LongHair(hairHue)); break;
                    case 8: AddItem(new PiquesChev(hairHue)); break;
                    case 9: AddItem(new PlatChev(hairHue)); break;
                    case 10: AddItem(new Albator(hairHue)); break;
                    case 11: AddItem(new Raide(hairHue)); break;

                }

                switch (Utility.Random(5))
                {
                    case 0: AddItem(new LongBeard(hairHue)); break;
                    case 1: AddItem(new MediumLongBeard(hairHue)); break;
                    case 2: AddItem(new Vandyke(hairHue)); break;
                    case 3: AddItem(new Mustache(hairHue)); break;
                    case 4: AddItem(new Goatee(hairHue)); break;
                }
            }

            Hue = Utility.RandomSkinHue();    // Random Skin on NPC
        }

        public virtual void GenerateFreeAppearance()
        {
            InitOutfit();
        }
        #endregion

        #region Registration
        private void RegisterSelf()
        {
            if (!GuildsSystem.GuildMobiles.ContainsKey(MobileType))
                GuildsSystem.GuildMobiles.Add(MobileType, new List<BaseGuildMobile>());
            GuildsSystem.GuildMobiles[MobileType].Add(this);

            if (!GuildsSystem.GuildMobilesPerGuild.ContainsKey(GuildOfMobile))
                GuildsSystem.GuildMobilesPerGuild.Add(GuildOfMobile, new List<BaseGuildMobile>());
            GuildsSystem.GuildMobilesPerGuild[GuildOfMobile].Add(this);
        }

        private void UnRegisterSelf()
        {
            if (!GuildsSystem.GuildMobiles.ContainsKey(MobileType))
                GuildsSystem.GuildMobiles.Add(MobileType, new List<BaseGuildMobile>());
            GuildsSystem.GuildMobiles[MobileType].Remove(this);

            if (!GuildsSystem.GuildMobilesPerGuild.ContainsKey(GuildOfMobile))
                GuildsSystem.GuildMobilesPerGuild.Add(GuildOfMobile, new List<BaseGuildMobile>());
            GuildsSystem.GuildMobilesPerGuild[GuildOfMobile].Remove(this);
        }
        #endregion

        #endregion

        #region Serialization
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 

            writer.Write((int)m_Guild);
            writer.Write((int)m_Domain);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            m_Guild = (GuildType)reader.ReadInt();
            m_Domain = (GuildDomain)reader.ReadInt();

            GuildOfMobile = GuildType.None;
            GuildOfMobile = m_Guild;
        }
        #endregion

    }

}