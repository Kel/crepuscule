﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;
using Server.Guilds;

namespace Server.Engines
{

    public class GuildPointsReward
    {
        public static List<GuildPointsReward> StandardList = new List<GuildPointsReward>();
        static GuildPointsReward()
        {
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 1, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 2, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 3, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 4, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, -1, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, -2, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, -3, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, -4, 0, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 1, -4, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 4, -1, "", DateTime.Now, ""));
            StandardList.Add(new GuildPointsReward(GuildType.None, GuildDomain.None, 10, 0, "", DateTime.Now, ""));
        }

        public GuildPointsReward()
        {
            Guild = GuildType.None;
            Domain = GuildDomain.None;
            RewardSelf = 0;
            RewardOthers = 0;
        }

        public GuildPointsReward(GuildType guild, GuildDomain domain, int rewardSelf, int rewardOthers, string accountName, DateTime when, string comment)
        {
            Guild = guild;
            Domain = domain;
            RewardSelf = rewardSelf;
            RewardOthers = rewardOthers;
            AccountName = accountName;
            When = when;
            Comment = comment;
        }

        public GuildType Guild;
        public GuildDomain Domain;
        public int RewardSelf;
        public int RewardOthers;
        public string AccountName;
        public DateTime When;
        public string Comment;

        public override string ToString()
        {
            string self = RewardSelf.ToString();
            if(RewardSelf > 0) self = "+"+RewardSelf;
            string others = RewardOthers.ToString();
            if(RewardOthers > 0) others = "+"+RewardOthers;

            return String.Format("{0} / {1}", self,others );
        }
    }



}
