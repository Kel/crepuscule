﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;
using Server.Guilds;

namespace Server.Engines
{

    public static class TownfolkSayings
    {
        
        public static Dictionary<GuildType,string[]> SayingsList = new Dictionary<GuildType,string[]>();

        static TownfolkSayings()
        {
            SayingsList.Add(GuildType.None, new string[]{
                    "Que Thaar veille sur vous!", "Ma tête...", "Les gitans existent toujours?", "Je m'ennuie ici..", "J'ai soif", "*Gromelle*", "Systéria... travail assuré qu'ils ont dit..",
                    "On dit que ces Berguenois font plus de 2 mètres... Vous y croyez?", "Cybelle est bonne pour nous.", "La Sanglante a fait exécuter mon pauvre fils...",
                    "Le port de Posdrenia est vraiment énorme, je vous le dis!", "Le ciel est rouge... Il ne fera pas beau demain!", "Il ne faudrait pas que je rate la messe!",
                    "Vous n'auriez pas vu un chien?", "Je ne suis jamais sorti de l'archipel, et vous?", "Les jardins impériaux sont très jolis au printemps.",
                    "*S'affaire à ses travaux quotidiens*", "Pfffu... Encore des taxes!", "On raconte que les terres sont gelées en permanence au Nord... J'y crois pas!",
                    "Mon voisin me doit toujours quelques écus...", "Avez-vous déjà entendu parler des sirènes?", "Le prix de la farine a vraiment augmenté!",
                    "J'adore les galettes à la cannelle.", "J'espère qu'il ne pleuvra pas...", "Je me demande à quoi ressemble le prince-consort...",
                    "Les thermes sont toujours bondés, je n'y vais plus.", "Le désert sert d'habitat aux 'Dracos', à ce qu'on dit.", "Comme j'ai hâte que finisse la journée!",
                    "Ces sauvages des marais sont vraiment barbares.", "Cette caverne à l'Est, elle rugit! Je vous le jure!", "Il y a de jolies ruines à l'ouest, vous devriez les visiter.",
                    "Les Landes-Unies sont vraiment un pays étrange, avec cette 'démocratie'...", "J'adore le théâtre, et vous?", "Mon aïeul disait que Feuil n'apporte jamais rien de bon.",
                    "Maemor II était un drôle de bougre!", "Mon cousin connaît un garde impérial! Vous imaginez?", "Naguère, les jeunes étaient moins frivoles.",
                    "On ne valorise pas suffisamment les artistes.", "Y a-t-il un conservatoire par ici?", "Le musée des Thermes est vraiment intéressant.",
                    "J'adore la senteur du lilas.", "La basse-ville a brûlé par deux fois... Je ne m'y sens plus en sécurité.", "Nous aurions dû lyncher Mala!",
                    "Les gens de la Confrérie sont bizarres...", "Ces Mercenaires sont corrompus jusqu'à la moelle!", "On dirait que l'Ordre ne réussit pas à freiner les dissidents...",
                    "La Fraternité est pleine d'elfes, à ce qu'on dit!", "L'Association a saisi ma maison! Ça va chauffer!", "Les ogres sont une plaie dans le Nord.",
                    "Vaerdon, Shaelim et tout ça... Je ne crois qu'en Thaar!", "Les elfes noirs me font peur...", "Les gens de Tsen ont des yeux étirés.",
                    "Mon fils n'ira pas dans ces 'écoles' où on les endoctrine!", "J'ai lu un livre sur le Fol... Un drôle d'oiseau, n'est-ce pas?", "J'espère que Cybelle n'a pas d'autres soeurs!",
                    "Tous ces immigrants qui arpentent les rues, ça me fiche la trouille.", "Bourse Pleine, c'est un drôle de nom! C'est pas qu'économique, ha! ha!",
                    "Je me plais à croire que Thaar nous réserve un monde meilleur après cette vie.", "Les Glaces Éternelles portent bien leur nom...! C'est vraiment frisquet, par là-bas, je vous le dis.", 
                    "*S'arrête pour saluer les passants*", "*Sifflotte en déambulant*", "J'aime quand le soleil brille: c'est bon signe.", "Le dernier festival agricole était fabuleux.", 
                    "Vous vous souvenez de Slang? Ce démon qui avait failli ravager Systéria?", "Ah, il y a quelques années, d'étranges phénomènes climatiques ont fait rage ici...", 
                    "Un permis pour bûcher! Ces gens de la Fraternité sont des hurluberlus!", "Ma pauvre mère a dû louer les services d'un mercenaire pour se débarasser des chauve-souris chez elle.",
                    "Je fais confiance au clergé. Ses membres sont très compétents.", "Systéria est bien agréable, malgré la pluie fréquente, vous ne trouvez pas?",
                    "Au mois de Résu, un fort vent de l'ouest balaie habituellement les landes.", "Les moissons ont été excellentes cette année!", 
                    "L'Hôpital Ste-Élisa me fait vraiment peur, avec toutes les rumeurs l'entourant...", "Avez-vous lu la dernière Gazette du Citoyen? Enfin, un journal intéressant!", 
                    "Mon cousin n'est jamais ressorti de Ste-Élisa... Il y a bien 10 ans qu'il y est.", "Il se passe des drôles de choses à la Rose Cendrée...",
                    "Les quais sont désagréables... L'odeur qui y règne, pouah!", "Ha! Ha! Ce Melgonder, on en avait bien jasé. Émasculé par l'Ordre! C'est ça que de forniquer.",
                    "On ne peut pas faire confiance aux mages... Ils se jouent de vous à cause de leur intelligence!", "L'impie qui a incendié notre chapelle en basse-ville a mis longtemps à brûler elle-même...",
                    "J'ai déjà vu un haut-placé de la Fraternité avec une cape en plumes...!", "On dit que le vieux Majere mangeait des cervelles! Je le tiens de la Gazette!",
                    "C'est vrai, dites-moi, que les mercenaires sont pas très thaariens dans leurs dortoirs?", "Excusez-moi, pourriez-vous m'indiquer l'auberge la plus proche?",
                    "L'intendant est un vrai dépravé. Il paraît qu'il n'engage que les damoiselles bien roulées...", "Ce violeur de porcs, Jack Lenomson, n'avait-il pas été enfermé par un caporal mercenaire?", 
                    "Si seulement l'attentat contre la Sanglante avait fonctionné, son règne n'aurait jamais eu lieu...", "Il paraît que l'Archevêque a tué tous ses prédécesseurs...",
                    "Avez-vous déjà entendu parler du Commandant des mercenaires? Un vieux fou, à ce qu'on dit!", 
                    "L'Ancienne de la Confrérie est bizarroïde. Il paraît qu'elle s'enferme dans sa tour des jours durant.", "Y a-t-il seulement un dirigeant à la Fraternité?"
                    });

            SayingsList.Add(GuildType.Mercenaires, new string[]{
               "Ça, c'est un coup pour finir sur l'île-prison!", "Encore un coup de l'Ordre, hein? Pas possible, ça!", "Je dirais pas non pour une petite bière.", 
               "Hanzo Hattori c'était du consul ça! Dommage qu'il soit mort empoisonné par sa maîtresse.", "Psst, belle demoiselle, pas besoin de protection?", 
               "Moi je dis qu'il faut toujours protéger sa maison!", "Germund Novitch, il est mort comme un brave lui! Dommage que c'était pour sauver un jaune...", 
               "Les poussins, ça m'épuise, toujours a traîner partout.", "Gloire à l'armée!", "* siffle l'hymne mercenaire*", "Non mais c'est pas possible ça! Il y a que nous qui travaillons ici.", 
               "Tu te souviens de l'attaque sur le camp du Cercle de Pierre? Un bon moment que celui-là!", "Faudrait pas abuser du rhum: la Rose Cendrée, c'est pas donné leurs prix.", 
               "Tu fais quoi ce soir belle poulette?", "J'suis sûr que l'oeil du nain, il est valide sous son bandeau.", "Pourquoi appelle-t-on coupe-jarret un gars qui coupe les gorges?", 
               "Tu te souviens de Morgathys? Celui qui habitait la basse ville! Un gars bizarre...", "Clavius Décimus, un bon gars.. parti trop tôt de la ville.", 
               "Ça fait du bien que l'Ange Noir n'est plus dans le coin, on ramasse moins de monde.", "T'as vu ce qu'il se passe du côté de Ste-Élisa? Moi, ça m'inspire pas.", 
               "Le règne de Mala était l'un des plus meurtriers, on s'en souviendra!", "Ah les barbares! Le plaisir de croiser le fer contre de vrais guerriers, c'était le bon temps de la guerre du nord.", 
               "Quand t'es mercenaire, tu as du sang, de l'amour et de la bière, de quoi rendre un homme heureux.", "Le vert, c'est la couleur qui me va le mieux!", "Garde à vous!", "*salut militaire*", 
               "Alors, bande de veaux? On se motive! Hop hop hop!", "On fait une petite ronde et puis on revient à la caserne, compris?", "Ouais, que ça de vrai, comme tu dis: pas vu pas pris, vu et pris pendu!",
               "Il a pris 40 ans à la prison mercenaire de l'île! Heureusement, il vivra pas jusque-là.", "Pas possible une solde pareille! Je vais me mettre un peu d'argent de côté parce que là...", 
               "Si jamais ça tourne mal, faut pas hésiter à nous appeler, hein?", "Ça fait plaisir de voir des gens qui nous admirent. L'ordre et la droiture, c'est un peu nous... ou presque.", 
               "Tu penses quoi toi de cette nouvelle? Ouais, moi aussi j'y pense.", "Et là, le marin lui a dit: 'Espèce de mécréant!' et Tom lui a donné un coup dans le ventre avec son gant de plaque!",
               "Hé ben, ça nous rendra pas riche, tout ça, va falloir encore bosser...", "À quel âge encore, la retraite sur Systéria?", "Tu crois pas qu'utiliser des chiens serait une bonne chose?", 
               "Paf! un grand coup de targe dans le nez! Je savais pas que c'était un noble...", "*regarde autour d'un air sérieux*", "*fronce les sourcils*", "*vérifie le tranchant de sa lame*", 
               "Les combats, moi, ça me manque. J'aime bien ça, ce mélange de sueur et de sang qu'on éponge dans la bière.", 
               "Moi je te dis que d'ici un an ou deux, on contrôle toute cette ville! Ouais, parole de mercenaire!", "Heureusement qu'on est là, nous. C'est pas l'Ordre du Sommeil qui attrapera les criminels.", 
               "Tu connais Stornaar? Apparament, si tu réplique à ses ordres il t'enferme dans une trappe sous sa chaise pendant trois jours!"
            });

            SayingsList.Add(GuildType.Commercants, new string[]{
                "Du temps de la grosse Lucrèce, nous payions notre uniforme!", "Le duc d'Orbrillant avait l'habitude de se goinfrer pendant les réunions...", 
                "Les marchands illégaux me font frémir! Qu'ils prennent garde!", "*Prend des notes dans un calepin en regardant une demeure*", 
                "Systéria n'est pas le meilleur marché pour un sculpteur comme moi...", "On m'a un jour demandé de coudre trois bottes!", 
                "Il arrive que mes clients se plaignent de nos prix... Ne comprennent-ils pas que c'est pour le bien de l'Économie?", "Hep, vous avez un permis de vente?", "*Scrute les alentours*", 
                "Un jour, je serai Trésorier!", "L'Association se préoccupe des gens... Nous faisons des dons à la cathédrale.", "Oyez, oyez! Le meilleur cordonnier en ville, c'est par ici!", 
                "Les meilleurs prix de Systéria, approchez, approchez!", "J'ai visité Azgal'Ankor, une fois... Quelle ville splendide pour le commerce!", 
                "Les étoffes de Briganne sont vraiment idéales pour la couture.", "Je pars prochainement pour Nguelundi... Vous savez, là où il y a des animaux sauvages à profusion.", 
                "Il y a du profit à faire à Zanther! C'est ma devise!", "Le prix des maisons est encore sujet à des augmentations...!", "J'ai un bon salaire dans l'Association.", 
                "Avez-vous déjà croisé le... la nouvelle dirigeante? Nathalia?", "On raconte que la nouvelle juge de l'Économie a un faciès horrible...", 
                "Avec le baron Blaise, nous ne manquions de rien! Dommage qu'il soit parti.", "Je cuisine à mes heures perdues... Peut-être ouvrirai-je bientôt une taverne.",
                "Mon compagnon m'a tissé un superbe hakama que je mettrai en allant à Tsen.", "Rappelez-vous, pas plus de deux maisons par personne!", 
                "N'oubliez pas de vous déclarer à l'Association lors de l'achat d'une demeure.", "Vous êtes sûr que vous n'avez rien à cacher?"
            });

            SayingsList.Add(GuildType.Soleil, new string[]{
                "Oh, de vrai croyants, il y en a de moins en moins...", "Récitez-moi un peu les vertus?", "*Se signe*", "Lielos.", "Oh, il est loin, le temps d'un bûcher par jour!",
                "Oh, les êtres de sang impur ne sont pas acceptés dans notre guilde, c'est simple.", "Tous ces mercenaires corrompus... Où va le monde d'Enrya?", 
                "Regardez la lumière de Thaar comme le pêcheur regarde son filet dans l'attente et l'envie d'y voir quelque chose.", "Paladin un jour, paladin toujours!",
                " Tu as déja vu le perroquet Coco?", "Moi, ce que j'aime à cette vie, c'est sa simplicité et sa pureté", "N'oubliez pas la messe d'ici quelques minutes!", 
                "Gloire au seigneur, qu'il m'évite les zones d'ombres et les failles de ma volonté.", "Priez, messire, priez pour le salut de votre âme", "Il y a une messe à la cathédrale ce soir?", 
                "Simple: après le lever, il y a la prière!"
            });
        }

    }



}
