﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Scripts.Commands;
using System.Linq;
using System.IO;
using Server.Guilds;
using Server.Accounting;

namespace Server.Items
{

    public class GuildTitleBook : CrepusculeItem
    {
        private TreasuryBook m_TreasuryBook = null;

        [CommandProperty(AccessLevel.GameMaster)]
        public TreasuryBook LinkedTreasuryBook
        {
            get { return m_TreasuryBook; }
            set { m_TreasuryBook = value; }
        }

        [Constructable]
        public GuildTitleBook() : base(0xFEF)
        {
            this.Name = "Livre de titres de guilde";
        }


        public GuildTitleBook(Serial serial)
            : base(serial)
        {
        }

        public override void OnDoubleClick(Mobile from)
        {
            try
            {
                if (m_TreasuryBook == null)
                {
                    from.SendMessage("Ce livre n'est pas lié, appelez un GM!");
                    return;
                }

                if (from.InRange(this.Location, 2))
                {
                    base.OnDoubleClick(from);
                    var EmployeesCopy = new ArrayList();
                    m_TreasuryBook.PurgeList();

                    // Copy to purged
                    foreach (var item in m_TreasuryBook.Employees)
                        EmployeesCopy.Add(item);

                    from.CloseGump(typeof(GuildTitleGump));
                    from.SendGump(new GuildTitleGump(from, EmployeesCopy, 0, m_TreasuryBook));
                }
                else
                {
                    from.SendMessage("Vous êtes trop loin");
                }
            }
            catch (Exception ex)
            {
                from.SendMessage("Erreur : " + ex.Message);
            }
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            m_TreasuryBook = reader.ReadItem<TreasuryBook>();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
            writer.WriteItem<TreasuryBook>(m_TreasuryBook);
       
        }



    }
    
}
