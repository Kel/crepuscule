﻿using System;
using System.Collections;
using Server.Network;
using Server.Targeting;
using Server.Prompts;

namespace Server.Items
{
    public class GuildKey : Key
    {
		[Constructable]
		public GuildKey() : base( KeyType.Copper, 1 )
		{
		}

		[Constructable]
		public GuildKey( KeyType type ) : this( type, 1 )
		{
		}

		[Constructable]
		public GuildKey( uint val ) : this ( KeyType.Copper, val )
		{
		}

		[Constructable]
		public GuildKey( KeyType type, uint LockVal ) : this( type, LockVal, null )
		{
			KeyValue = LockVal;
		}

		public GuildKey( KeyType type, uint LockVal, Item link ) : base( (int)type )
		{
			Weight = 1.0;
			MaxRange = 3;
			KeyValue = LockVal;
			Link = link;
            LootType = LootType.Regular;
		}

        public GuildKey(Serial serial)
            : base(serial)
		{
		}

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)2); // version
            writer.Write((int)MaxRange);
            writer.Write((Item)Link);
            writer.Write((string)Description);
            writer.Write((uint)KeyValue);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            switch (version)
            {
                case 2:
                    {
                        MaxRange = reader.ReadInt();
                        goto case 1;
                    }
                case 1:
                    {
                        Link = reader.ReadItem();
                        goto case 0;
                    }
                case 0:
                    {
                        if (version < 2 || MaxRange == 0)
                            MaxRange = 3;

                        Description = reader.ReadString();

                        KeyValue = reader.ReadUInt();
                        if (KeyValue == 0) KeyValue = 1;

                        break;
                    }
            }
        }
    }
}
