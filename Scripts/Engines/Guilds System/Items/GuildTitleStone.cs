using System;
using System.IO;
using Server.Gumps;
using Server.Guilds;
using Server.Network;

using Server.Multis;
using Server.Mobiles;
using System.Collections.Generic;

namespace Server.Items
{
	public class GuildTitleStone : Item
	{
        private GuildType m_Guild = GuildType.None;

        private GuildTitleRankInfo m_Rank1;
        private GuildTitleRankInfo m_Rank2;
        private GuildTitleRankInfo m_Rank3;
        private GuildTitleRankInfo m_Rank4;
        private GuildTitleRankInfo m_Rank5;
        private GuildTitleRankInfo m_Rank6;
        private GuildTitleRankInfo m_Rank7;
        private GuildTitleRankInfo m_Rank8;
        private GuildTitleRankInfo m_Rank9;
        private GuildTitleRankInfo m_Rank10;
        private List<GuildTitleRankInfo> m_List = new List<GuildTitleRankInfo>();

        [Constructable]
		public GuildTitleStone() : base( 0xED6  )
		{
            Movable = false;
            Name = "Pierre de grades";
            m_Rank1 = new GuildTitleRankInfo();
            m_Rank2 = new GuildTitleRankInfo();
            m_Rank3 = new GuildTitleRankInfo();
            m_Rank4 = new GuildTitleRankInfo();
            m_Rank5 = new GuildTitleRankInfo();
            m_Rank6 = new GuildTitleRankInfo();
            m_Rank7 = new GuildTitleRankInfo();
            m_Rank8 = new GuildTitleRankInfo();
            m_Rank9 = new GuildTitleRankInfo();
            m_Rank10 = new GuildTitleRankInfo();

            m_List = GetRanks();
            
		}

		public GuildTitleStone( Serial serial ) : base( serial )
		{
		}

        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank1
        {
            get { return m_Rank1; }
            set { m_Rank1 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank2
        {
            get { return m_Rank2; }
            set { m_Rank2 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank3
        {
            get { return m_Rank3; }
            set { m_Rank3 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank4
        {
            get { return m_Rank4; }
            set { m_Rank4 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank5
        {
            get { return m_Rank5; }
            set { m_Rank5 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank6
        {
            get { return m_Rank6; }
            set { m_Rank6 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank7
        {
            get { return m_Rank7; }
            set { m_Rank7 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank8
        {
            get { return m_Rank8; }
            set { m_Rank8 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank9
        {
            get { return m_Rank9; }
            set { m_Rank9 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildTitleRankInfo Rank10
        {
            get { return m_Rank10; }
            set { m_Rank10 = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType Guild
        {
            get { return m_Guild; }
            set 
            { 
                m_Guild = value;
                Name = GuildDescriptions.GetGuildDescription(value); 
            }
        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int)0 ); // version
            writer.Write((int)m_Guild);
            
            m_Rank1.Serialize(writer);
            m_Rank2.Serialize(writer);
            m_Rank3.Serialize(writer);
            m_Rank4.Serialize(writer);
            m_Rank5.Serialize(writer);
            m_Rank6.Serialize(writer);
            m_Rank7.Serialize(writer);
            m_Rank8.Serialize(writer);
            m_Rank9.Serialize(writer);
            m_Rank10.Serialize(writer);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

			switch( version )
			{
				case 0:
				{
                    m_Guild = (GuildType)reader.ReadInt();

                    m_Rank1 = new GuildTitleRankInfo(reader);
                    m_Rank2 = new GuildTitleRankInfo(reader);
                    m_Rank3 = new GuildTitleRankInfo(reader);
                    m_Rank4 = new GuildTitleRankInfo(reader);
                    m_Rank5 = new GuildTitleRankInfo(reader);
                    m_Rank6 = new GuildTitleRankInfo(reader);
                    m_Rank7 = new GuildTitleRankInfo(reader);
                    m_Rank8 = new GuildTitleRankInfo(reader);
                    m_Rank9 = new GuildTitleRankInfo(reader);
                    m_Rank10 = new GuildTitleRankInfo(reader);

                    m_List = GetRanks();
					break;
				}
			}
		}

        private List<GuildTitleRankInfo> GetRanks()
        {
            var list = new List<GuildTitleRankInfo>();
            list.Add(m_Rank1);
            list.Add(m_Rank2);
            list.Add(m_Rank3);
            list.Add(m_Rank4);
            list.Add(m_Rank5);
            list.Add(m_Rank6);
            list.Add(m_Rank7);
            list.Add(m_Rank8);
            list.Add(m_Rank9);
            list.Add(m_Rank10);
            return list;
        }

        public override bool HandlesOnSpeech { get { return true; } }


        public override void OnSpeech(SpeechEventArgs e)
        {
            base.OnSpeech(e);
            if (e.Mobile.InRange(this.Location, 2))
            {
                if (e.Mobile is RacePlayerMobile && e.Speech != null)
                {
                    var low = e.Speech.ToLower();
                    foreach (var rank in m_List)
                    {
                        if (rank.Rank != null && rank.Enabled && rank.Keyword != null && low.Contains(rank.Keyword.ToLower()))
                        {
                            e.Mobile.GuildTitle = rank.Rank;
                            e.Mobile.PublicOverheadMessage(MessageType.Emote, 50, false, "*Prend titre de " + rank.Rank + "*");
                        
                            break;
                        }
                    }
                }
            }
        }


	}

    [PropertyObject]
    public class GuildTitleRankInfo
    {
        private string m_Rank;
        private string m_Keyword;
        private bool m_Enabled;

        public GuildTitleRankInfo() 
        {
            
        }

        public GuildTitleRankInfo(GenericReader reader)
        {
            Deserialize(reader);
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(m_Rank);
            writer.Write(m_Keyword);
            writer.Write(m_Enabled);
        }

        public void Deserialize(GenericReader reader)
        {
            var version = reader.ReadInt();
            m_Rank = reader.ReadString();
            m_Keyword = reader.ReadString();
            m_Enabled = reader.ReadBool();
        }


        [CommandProperty(AccessLevel.GameMaster)]
        public string Rank
        {
            get { return m_Rank; }
            set { m_Rank = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public string Keyword
        {
            get { return m_Keyword; }
            set { m_Keyword = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public bool Enabled
        {
            get { return m_Enabled; }
            set { m_Enabled = value; }
        }

        public override string ToString()
        {
            if (m_Enabled)
                return m_Rank;
            return "(libre)";
        }
    }

}
