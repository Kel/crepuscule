using Server;
using Server.Items;
using Server.Network;
using System;
using System.Collections;
using Server.Mobiles;
using Server.Engines;
using Server.Guilds;

namespace Server.Items
{
    [TypeAlias("Server.Items.BaseTresorerie")]
	public abstract class BaseTreasuryContainer : LockableContainer
	{
        internal int m_DailyIncome = 0;
        private GuildType m_Guild = GuildType.None;
        public static ArrayList Tresoreries = new ArrayList();

        [CommandProperty(AccessLevel.GameMaster)]
        public int DailyIncome
        {
            get { return m_DailyIncome; }
            set { m_DailyIncome = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType Guild
        {
            get { return m_Guild; }
            set { m_Guild = value; }
        }
		
		public BaseTreasuryContainer( int itemID ) : base ( itemID )
		{
			
			Locked = true;
			Movable = false;
			Name = "Coffre de Tresorerie";
            Tresoreries.Add(this);
		}

        public BaseTreasuryContainer(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 1 );
            writer.Write((int)DailyIncome);
            writer.Write((int)m_Guild);
		}

        public override void OnDoubleClick(Mobile from)
        {
            if (from.InRange(this.Location, 2))
            {
                base.OnDoubleClick(from);
            }
            else
            {
                from.SendMessage("Vous �tes trop loin");
            }
        }

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

            if (Tresoreries == null) Tresoreries = new ArrayList();
            Tresoreries.Add(this);

            if (version >= 0)
            {
                m_DailyIncome = reader.ReadInt();
            }
            if (version >= 1)
            {
                m_Guild = (GuildType)reader.ReadInt();
            }


		}


		
	}
}









			









