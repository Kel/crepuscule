using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;
using System.Collections.Generic;
using Server.Engines;
using Server.Guilds;

namespace Server.Items
{

    public class GuildsControllerStone: CrepusculeItem
    {
        #region Constructors
        

        [Constructable]
        public GuildsControllerStone()
            : base(0xED4)
        {
            if (GuildsSystem.MainController == null)
            {
                Movable = false;
                Hue = 7;
                Name = "Pierre de controle du syst�me des Guildes";
                GuildsSystem.MainController = this;
            }
            else
            {
                BroadcastMessage(AccessLevel.Counselor, 3, "Impossible de rajouter, le controlleur existe d�j�");
                this.Delete();
            }
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (from is RacePlayerMobile)
            {
                RacePlayerMobile mobile = from as RacePlayerMobile;
                mobile.SendMessage("Mais non, il faut passer par .props pour modifier les param�tres du syst�me!");
            }
        }

        public override void OnDelete()
        {
            //Can't delete
            //base.OnDelete();
        }

        public GuildsControllerStone(Serial serial)
            : base(serial)
        {
        }
        #endregion



        #region Serialize
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)1); // version 

            writer.Write((int)GuildsSystem.RewardBatch.Count);
            foreach (GuildPointsReward reward in GuildsSystem.RewardBatch)
            {
                writer.Write((int)reward.Domain);
                writer.Write((int)reward.Guild);
                writer.Write(reward.RewardSelf);
                writer.Write(reward.RewardOthers);
                writer.Write(reward.When);
                writer.Write(reward.AccountName);
                writer.Write(reward.Comment);
            }

            writer.Write(GuildsSystem.LastPayDay);
            writer.Write(GuildsSystem.LastBackupDay);
        }
        #endregion

        #region Deserialize
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();

            // Console.WriteLine("StoneID: " + this.Serial.Value);
            int batchCount = reader.ReadInt();
            for (int i = 0; i < batchCount; i++)
            {
                GuildPointsReward reward = new GuildPointsReward();
                reward.Domain = (GuildDomain)reader.ReadInt();
                reward.Guild  = (GuildType)reader.ReadInt();
                reward.RewardSelf = reader.ReadInt();
                reward.RewardOthers = reader.ReadInt();
                reward.When = reader.ReadDateTime();
                reward.AccountName = reader.ReadString();
                reward.Comment = reader.ReadString();
                GuildsSystem.RewardBatch.Add(reward);
            }
            GuildsSystem.LastPayDay = reader.ReadDateTime();

            if(version >= 1)
                GuildsSystem.LastBackupDay = reader.ReadDateTime();

            if (GuildsSystem.MainController == null) GuildsSystem.MainController = this;
        }
        #endregion
    }




}
    
