using Server;
using Server.Items;
using Server.Multis;
using Server.Network;
using System;

namespace Server.Items
{
	[FlipableAttribute( 0xe43, 0xe42 )]
    [TypeAlias("Server.Items.TresorerieWooden")]
    public class TreasuryContainerWooden : BaseTreasuryContainer 
	{ 
		public override int DefaultGumpID{ get{ return 0x49; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 20, 105, 150, 180 ); }
		}

		[Constructable] 
		public TreasuryContainerWooden() : base( 0xE43 ) 
		{ 
		}

        public TreasuryContainerWooden(Serial serial)
            : base(serial) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0xe41, 0xe40 )]
    [TypeAlias("Server.Items.TresorerieMetalGolden")]
    public class TreasuryContainerMetalGolden : BaseTreasuryContainer 
	{ 
		public override int DefaultGumpID{ get{ return 0x42; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 20, 105, 150, 180 ); }
		}

		[Constructable] 
		public TreasuryContainerMetalGolden() : base( 0xE41 ) 
		{ 
		}

        public TreasuryContainerMetalGolden(Serial serial)
            : base(serial) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 

	[FlipableAttribute( 0x9ab, 0xe7c )]
    [TypeAlias("Server.Items.TresorerieMetal")]
    public class TreasuryContainerMetal : BaseTreasuryContainer 
	{ 
		public override int DefaultGumpID{ get{ return 0x4A; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 20, 105, 150, 180 ); }
		}

		[Constructable] 
		public TreasuryContainerMetal() : base( 0x9AB ) 
		{ 
		}

        public TreasuryContainerMetal(Serial serial)
            : base(serial) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 
}