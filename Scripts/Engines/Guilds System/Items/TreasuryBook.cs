﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Scripts.Commands;
using System.Linq;
using System.IO;
using Server.Guilds;
using Server.Accounting;

namespace Server.Items
{

    [TypeAlias("Server.Gumps.TresorerieBook")]
    public class TreasuryBook : CrepusculeItem
    {
        public static ArrayList TresoreriesBook = new ArrayList();

        internal List<RacePlayerMobile> Employees = new List<RacePlayerMobile>();
        private GuildType guilde = GuildType.None;

        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType Guilde
        {
            get { return guilde; }
            set { guilde = value; }
        }


        [CommandProperty(AccessLevel.GameMaster)]
        public int EmployeesCount
        {
            get
            {
                return Employees.Count;
            }
        }


        [Constructable]
        public TreasuryBook()
            : base(0xFEF)
        {
            this.Name = "Livre de comptabilité";
            TresoreriesBook.Add(this);
        }


        public TreasuryBook(Serial serial)
            : base(serial)
        {
        }

        public void PurgeList()
        {
            var PurgedList = new List<RacePlayerMobile>();

            for (int i = 0; i < this.Employees.Count; i++)
            {
                // purge the list
                if (Employees[i] != null)
                {
                    var item = Employees[i] as RacePlayerMobile;

                    if (item != null &&
                        item is RacePlayerMobile &&
                        item.GuildInfo != null &&
                        !item.Deleted &&
                        item.GuildInfo.PlayerGuild == this.Guilde &&
                        !String.IsNullOrEmpty(item.SalaryInfo.EmployeeName.Trim()) &&
                        (DateTime.Now - (item.Account as Account).LastLogin).TotalDays <= 60)
                    {
                        PurgedList.Add(item);
                    }
                }
            }

            // Replace the list
            Employees = PurgedList;

        }

        public override void OnDoubleClick(Mobile from)
        {
            try
            {
                if (from.InRange(this.Location, 2))
                {
                    base.OnDoubleClick(from);
                    var EmployeesCopy = new ArrayList();
                    PurgeList();

                    // Copy to purged
                    foreach(var item in Employees)
                        EmployeesCopy.Add(item);

                    from.CloseGump(typeof(TreasuryGump));
                    from.SendGump(new TreasuryGump(from, EmployeesCopy, 0, this));
                }
                else
                {
                    from.SendMessage("Vous êtes trop loin");
                }
            }
            catch (Exception ex)
            {
                from.SendMessage("Erreur : " + ex.Message);
            }
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            Employees = new List<RacePlayerMobile>();
            if (TresoreriesBook == null) TresoreriesBook = new ArrayList();
            TresoreriesBook.Add(this);

            int version = reader.ReadInt();

            if (version == 1)
                guilde = (GuildType)(reader.ReadInt());

            int employeesCount = (int)reader.ReadInt();
            for (int i = 0; i < employeesCount; i++)
            {
                Employees.Add((RacePlayerMobile)reader.ReadMobile());
            }
        
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)1); // version

            writer.Write((int)guilde);
            writer.Write((int)Employees.Count);
            for (int i = 0; i < Employees.Count; i++)
            {
                writer.Write((Mobile)Employees[i]);
            }
       
        }

        public void Inscription(RacePlayerMobile from)
        {
            if (Employees.Contains(from))
            {
                from.SendMessage("Vous êtes déjà dans la liste");
                if (String.IsNullOrEmpty(from.SalaryInfo.EmployeeName.Trim()))
                {
                    // fix to reregister
                    Employees.Remove(from);
                    ProceedRegister(from);
                }
            }
            else
            {
                ProceedRegister(from);
            }
        }

        private void ProceedRegister(RacePlayerMobile from)
        {
            if (from.GuildInfo.PlayerGuild == this.Guilde)
            {
                from.CloseGump(typeof(TreasuryNameRegisterGump));
                from.SendGump(new TreasuryNameRegisterGump(from));
                Employees.Add(from);
            }
            else
            {
                from.SendMessage("Ce n'est pas votre guilde...");
            }
        }
        

        public void AddSalaries(double AmountToShare)
        {
            if (Employees != null)
            {
                //Get debts
                List<SalaryToShare> Debts = new List<SalaryToShare>();

                for (int i = 0; i < Employees.Count; i++)
                {
                    if (Employees[i] != null && Employees[i] is RacePlayerMobile)
                    {
                        RacePlayerMobile rpm = (RacePlayerMobile)Employees[i];
                        Debts.Add(new SalaryToShare(rpm, rpm.SalaryInfo.Salary));
                    }
                }

                Debts.Sort();
                var Shares = new List<double>();
                foreach(SalaryToShare debt in Debts)
                    Shares.Add(debt.Salary);

                double[] salaries = TreasurySplit.Share(AmountToShare, Shares.ToArray());

                //Share
                for (int i = 0; i < Shares.Count; i++)
                {
                    if (salaries[i] < 0)
                    {
                        CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 20, Debts[i].Mobile.PlayerName + " a reçu un salaire négatif! (bug?)");
                    }
                    else if (salaries[i] > 0)
                    {
                        Debts[i].Mobile.SalaryInfo.SalarySum += (int)(salaries[i]);
                    }
                }
               

            }
        }

        public struct SalaryToShare : IComparable<SalaryToShare>
        {
            public RacePlayerMobile Mobile;
            public double Salary;
            public SalaryToShare(RacePlayerMobile mobile, double salary)
            {
                Salary = salary;
                Mobile = mobile;
            }

            #region IComparable<SalaryToShare> Members

            public int CompareTo(SalaryToShare other)
            {
                return (int)(this.Salary - other.Salary);
            }

            #endregion
        }

    }
    
}
