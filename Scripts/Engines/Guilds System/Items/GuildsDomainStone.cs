using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;
using System.Collections.Generic;
using Server.Engines;
using Server.Guilds;

namespace Server.Items
{

    public class GuildsDomainStone: CrepusculeItem
    {
        #region Constructors
        [Constructable]
        public GuildsDomainStone()
            : base(0xED4)
        {
            Movable = false;
            Hue = 3;
            Name = "Pierre de controle d'une guilde";
            RegisterSayings();
        }


        public GuildsDomainStone(Serial serial)
            : base(serial)
        {
            
        }

        private void RegisterSayings()
        {
            if (TownfolkSayings.SayingsList.ContainsKey(m_Guild) && m_SayingsList.Count == 0)
            {
                m_SayingsList.AddRange(TownfolkSayings.SayingsList[m_Guild]);
            }
        }
        #endregion

        #region Properties
        private GuildType m_Guild = GuildType.None;
        private bool m_GoalIsDefense = true;
        private bool m_GoalIsWealth = true;
        private bool m_GoalIsReligion = true;
        private bool m_GoalIsMagic = true;
        private bool m_GoalIsKnowledge = true;

        private int m_GoldPerDefense = 75;
        private int m_GoldPerWealth = 100;
        private int m_GoldPerReligion = 50;
        private int m_GoldPerMagic = 40;
        private int m_GoldPerKnowledge = 20;

        private int m_GoldForSubsidy = 30000;
        internal DateTime m_LastSubsidyAttribution = DateTime.MinValue;

        internal int m_PointsDefense = 0;
        internal int m_PointsWealth = 0;
        internal int m_PointsReligion = 0;
        internal int m_PointsMagic = 0;
        internal int m_PointsKnowledge = 0;
        internal List<string> m_SayingsList = new List<string>();

        internal Mobile m_MobileTownfolk = null;
        internal Mobile m_MobileDiplomat = null;
        internal Mobile m_MobileGuard = null;

        internal Mobile m_MobileTownfolkFemale = null;
        internal Mobile m_MobileDiplomatFemale = null;
        internal Mobile m_MobileGuardFemale = null;

        [CommandProperty(AccessLevel.GameMaster)]
        public List<string> SayingsList
        {
          get { return m_SayingsList; }
          set { m_SayingsList = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public GuildType Guild
        {
            get { return m_Guild; }
            set {
                if (m_Guild != value)
                {
                    if (value != GuildType.None && GuildsSystem.GuildsDomainStones.ContainsKey(value))
                    { // already exists
                        BroadcastMessage(AccessLevel.Counselor, 3, "Pierre de controle pour cette guilde existe d�j�!");
                        return;
                    }

                    if (m_Guild != GuildType.None)
                    {
                        // Remove all data
                        if (GuildsSystem.GuildsGoals.ContainsKey(m_Guild)) GuildsSystem.GuildsGoals.Remove(m_Guild);
                        if (GuildsSystem.GuildsGuildGoldPerPoint.ContainsKey(m_Guild)) GuildsSystem.GuildsGuildGoldPerPoint.Remove(m_Guild);
                        if (GuildsSystem.GuildsPoints.ContainsKey(m_Guild)) GuildsSystem.GuildsPoints.Remove(m_Guild);

                        if (GuildsSystem.GuildsDomainStones.ContainsKey(m_Guild)) GuildsSystem.GuildsDomainStones.Remove(m_Guild);

                    }
                    if (value != GuildType.None)
                    {
                        // Add all data
                        if (!GuildsSystem.GuildsGoals.ContainsKey(value))
                        {
                            GuildsSystem.GuildsGoals.Add(value, new List<GuildGoal>()
                                {
                                    new GuildGoal(value,GuildDomain.Defense,m_GoalIsDefense),
                                    new GuildGoal(value,GuildDomain.Knowledge,m_GoalIsKnowledge),
                                    new GuildGoal(value,GuildDomain.Magic,m_GoalIsMagic),
                                    new GuildGoal(value,GuildDomain.Religion,m_GoalIsReligion),
                                    new GuildGoal(value,GuildDomain.Wealth,m_GoalIsWealth)
                                });
                        }

                        if (!GuildsSystem.GuildsGuildGoldPerPoint.ContainsKey(value))
                        {
                            GuildsSystem.GuildsGuildGoldPerPoint.Add(value, new List<GuildGoldPerPoint>()
                                {
                                    new GuildGoldPerPoint(value,GuildDomain.Defense,m_GoldPerDefense),
                                    new GuildGoldPerPoint(value,GuildDomain.Knowledge,m_GoldPerKnowledge),
                                    new GuildGoldPerPoint(value,GuildDomain.Magic,m_GoldPerMagic),
                                    new GuildGoldPerPoint(value,GuildDomain.Religion,m_GoldPerReligion),
                                    new GuildGoldPerPoint(value,GuildDomain.Wealth,m_GoldPerWealth)
                                });
                        }

                        if (!GuildsSystem.GuildsPoints.ContainsKey(value))
                        {
                            GuildsSystem.GuildsPoints.Add(value, new List<GuildPoints>()
                                {
                                    new GuildPoints(value,GuildDomain.Defense,m_PointsDefense),
                                    new GuildPoints(value,GuildDomain.Knowledge,m_PointsKnowledge),
                                    new GuildPoints(value,GuildDomain.Magic,m_PointsMagic),
                                    new GuildPoints(value,GuildDomain.Religion,m_PointsReligion),
                                    new GuildPoints(value,GuildDomain.Wealth,m_PointsWealth)
                                });
                        }

                        if (!GuildsSystem.GuildsDomainStones.ContainsKey(value)) GuildsSystem.GuildsDomainStones.Add(value, this);
                    }
                    m_Guild = value;

                    Name = "Pierre de controle de " + FormatHelper.GetGuildDescription(value);

                    GuildsSystem.Update();
                }
            }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public DateTime LastSubsidyAttribution
        {
            get { return m_LastSubsidyAttribution; }
            //set { m_LastSubsidyAttribution = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int GoldForSubsidy
        {
            get { return m_GoldForSubsidy; }
            set { m_GoldForSubsidy = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool GoalIsDefense
        {
            get { return m_GoalIsDefense; }
            set { m_GoalIsDefense = value; SetGoal(GuildDomain.Defense, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool GoalIsWealth
        {
            get { return m_GoalIsWealth; }
            set { m_GoalIsWealth = value; SetGoal(GuildDomain.Wealth, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool GoalIsReligion
        {
            get { return m_GoalIsReligion; }
            set { m_GoalIsReligion = value; SetGoal(GuildDomain.Religion, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool GoalIsMagic
        {
            get { return m_GoalIsMagic; }
            set { m_GoalIsMagic = value; SetGoal(GuildDomain.Magic, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public bool GoalIsKnowledge
        {
            get { return m_GoalIsKnowledge; }
            set { m_GoalIsKnowledge = value; SetGoal(GuildDomain.Knowledge, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int GoldPerDefense
        {
            get { return m_GoldPerDefense; }
            set { m_GoldPerDefense = value; SetGold(GuildDomain.Defense, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int GoldPerWealth
        {
            get { return m_GoldPerWealth; }
            set { m_GoldPerWealth = value; SetGold(GuildDomain.Defense, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int GoldPerReligion
        {
            get { return m_GoldPerReligion; }
            set { m_GoldPerReligion = value; SetGold(GuildDomain.Defense, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int GoldPerMagic
        {
            get { return m_GoldPerMagic; }
            set { m_GoldPerMagic = value; SetGold(GuildDomain.Defense, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int GoldPerKnowledge
        {
            get { return m_GoldPerKnowledge; }
            set { m_GoldPerKnowledge = value; SetGold(GuildDomain.Defense, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int PointsDefense
        {
            get { return m_PointsDefense; }
            //set { m_PointsDefense = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int PointsWealth
        {
            get { return m_PointsWealth; }
            //set { m_PointsWealth = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int PointsReligion
        {
            get { return m_PointsReligion; }
            //set { m_PointsReligion = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int PointsMagic
        {
            get { return m_PointsMagic; }
            //set { m_PointsMagic = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public int PointsKnowledge
        {
            get { return m_PointsKnowledge; }
            //set { m_PointsKnowledge = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public Mobile MobileTownfolk
        {
            get { return m_MobileTownfolk; }
            set { m_MobileTownfolk = value; }
            //set { m_MobileTownfolk = SetMobile(m_MobileTownfolk, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public Mobile MobileDiplomat
        {
            get { return m_MobileDiplomat; }
            set { m_MobileDiplomat = value;}
            //set { m_MobileDiplomat = SetMobile(m_MobileDiplomat, value);}
        }

        [CommandProperty(AccessLevel.Administrator)]
        public Mobile MobileGuard
        {
            get { return m_MobileGuard; }
            set { m_MobileGuard = value; }
            //set { m_MobileGuard = SetMobile(m_MobileGuard, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public Mobile MobileTownfolkFemale
        {
            get { return m_MobileTownfolkFemale; }
            set { m_MobileTownfolkFemale = value; }
            //set { m_MobileTownfolkFemale = SetMobile(m_MobileTownfolkFemale, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public Mobile MobileDiplomatFemale
        {
            get { return m_MobileDiplomatFemale; }
            set { m_MobileDiplomatFemale = value; }
            //set { m_MobileDiplomatFemale = SetMobile(m_MobileDiplomatFemale, value); }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public Mobile MobileGuardFemale
        {
            get { return m_MobileGuardFemale; }
            set { m_MobileGuardFemale = value; }
            //set { m_MobileGuardFemale = SetMobile(m_MobileGuardFemale, value); }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Sets and updates a domain of a guild 
        /// </summary>
        private void SetGoal(GuildDomain domain, bool value)
        {
            if (m_Guild == GuildType.None)
                return;
                //CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 3, "Veuillez d'abord choisir la guilde pour cette pierre");

            GuildsSystem.GetGoal(m_Guild, domain).CanParticipate = value;
        }

        /// <summary>
        /// Sets and updates a gold ratio
        /// </summary>
        private void SetGold(GuildDomain domain, int value)
        {
            if (m_Guild == GuildType.None)
                return;
                //CommandHandlers.BroadcastMessage(AccessLevel.Counselor, 3, "Veuillez d'abord choisir la guilde pour cette pierre");

            GuildsSystem.GetGoldPerPoint(m_Guild, domain).GoldPerPoint = value;
        }

        /// <summary>
        /// Clones a mobile 
        /// </summary>
        /// <param name="original"></param>
        /// <param name="newMobile"></param>
        private Mobile SetMobile(Mobile original, Mobile newMobile)
        {
            if (original != null && !original.Deleted)
                original.Delete();

            Mobile internalMobile = new GuildsBlankMobile();
            if (internalMobile != null)
            {
                CopyEquip.CopyEquipment(newMobile, internalMobile);

                internalMobile.CantWalk = true;
                internalMobile.Location = new Point3D(0, 0, 0);
                internalMobile.Map = Map.Internal;

                internalMobile.Name = "Pour systeme de guildes, pas toucher!";
                return internalMobile;
            }
            return null;
        }


        #endregion

        #region Overrides
        public override void OnDoubleClick(Mobile from)
        {
            if (from is RacePlayerMobile)
            {
                RacePlayerMobile mobile = from as RacePlayerMobile;
                mobile.SendMessage("Mais non, il faut passer par .props pour modifier les param�tres du syst�me!");
            }
        }

        public override void OnDelete()
        {
            if (Guild != GuildType.None)
            {
                Guild = GuildType.None;
                BroadcastMessage(AccessLevel.Counselor, 3, "Vous ne pouvez pas effacer cet objet sans avoir remis la guilde � None");
                return;
            }

            base.OnDelete();
        }
        #endregion

        #region Serialize
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)3); // version 

            writer.Write((int)m_Guild);

            writer.Write(m_PointsDefense);
            writer.Write(m_PointsWealth);
            writer.Write(m_PointsReligion);
            writer.Write(m_PointsMagic);
            writer.Write(m_PointsKnowledge);

            writer.Write(m_GoalIsDefense);
            writer.Write(m_GoalIsWealth);
            writer.Write(m_GoalIsReligion);
            writer.Write(m_GoalIsMagic);
            writer.Write(m_GoalIsKnowledge);

            writer.Write( m_GoldPerDefense);
            writer.Write( m_GoldPerWealth);
            writer.Write( m_GoldPerReligion);
            writer.Write( m_GoldPerMagic);
            writer.Write( m_GoldPerKnowledge);

            writer.Write(m_MobileDiplomat);
            writer.Write(m_MobileGuard);
            writer.Write(m_MobileTownfolk);

            writer.Write(m_GoldForSubsidy);
            writer.Write(m_LastSubsidyAttribution);

            writer.Write(m_SayingsList.Count);
            for(int i=0;i<m_SayingsList.Count;i++)
                writer.Write(m_SayingsList[i]);

            writer.Write(m_MobileDiplomatFemale);
            writer.Write(m_MobileGuardFemale);
            writer.Write(m_MobileTownfolkFemale);

        }
        #endregion

        #region Deserialize
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();


            try
            {
                if (version >= 0)
                {
                    Guild = (GuildType)reader.ReadInt();

                    m_PointsDefense = reader.ReadInt();
                    m_PointsWealth = reader.ReadInt();
                    m_PointsReligion = reader.ReadInt();
                    m_PointsMagic = reader.ReadInt();
                    m_PointsKnowledge = reader.ReadInt();

                    GoalIsDefense = reader.ReadBool();
                    GoalIsWealth = reader.ReadBool();
                    GoalIsReligion = reader.ReadBool();
                    GoalIsMagic = reader.ReadBool();
                    GoalIsKnowledge = reader.ReadBool();

                    GoldPerDefense = reader.ReadInt();
                    GoldPerWealth = reader.ReadInt();
                    GoldPerReligion = reader.ReadInt();
                    GoldPerMagic = reader.ReadInt();
                    GoldPerKnowledge = reader.ReadInt();

                    if (m_Guild != GuildType.None)
                    {
                        if (!GuildsSystem.GuildsPoints.ContainsKey(m_Guild))
                        {
                            GuildsSystem.GuildsPoints.Add(m_Guild, new List<GuildPoints>()
                        {
                            new GuildPoints(m_Guild,GuildDomain.Defense,m_PointsDefense),
                            new GuildPoints(m_Guild,GuildDomain.Knowledge,m_PointsKnowledge),
                            new GuildPoints(m_Guild,GuildDomain.Magic,m_PointsMagic),
                            new GuildPoints(m_Guild,GuildDomain.Religion,m_PointsReligion),
                            new GuildPoints(m_Guild,GuildDomain.Wealth,m_PointsWealth)
                        });
                        }
                        else
                        {
                            GuildsSystem.GuildsPoints[m_Guild] = new List<GuildPoints>()
                        {
                            new GuildPoints(m_Guild,GuildDomain.Defense,m_PointsDefense),
                            new GuildPoints(m_Guild,GuildDomain.Knowledge,m_PointsKnowledge),
                            new GuildPoints(m_Guild,GuildDomain.Magic,m_PointsMagic),
                            new GuildPoints(m_Guild,GuildDomain.Religion,m_PointsReligion),
                            new GuildPoints(m_Guild,GuildDomain.Wealth,m_PointsWealth)
                        };
                        }
                    }

                    m_MobileDiplomat = reader.ReadMobile();
                    m_MobileGuard = reader.ReadMobile();
                    m_MobileTownfolk = reader.ReadMobile();
                }

                if (version >= 1)
                {
                    m_GoldForSubsidy = reader.ReadInt();
                    m_LastSubsidyAttribution = reader.ReadDateTime();
                }

                if (version >= 2)
                {
                    int sayingsCount = reader.ReadInt();
                    for (int i = 0; i < sayingsCount;i++ )
                        m_SayingsList.Add(reader.ReadString());
                }

                if (version >= 3)
                {
                    m_MobileDiplomatFemale = reader.ReadMobile();
                    m_MobileGuardFemale = reader.ReadMobile();
                    m_MobileTownfolkFemale = reader.ReadMobile();
                }



                RegisterSayings();
                GuildsSystem.Update();
            }
            catch (Exception ex) { Console.WriteLine(ex.StackTrace); throw ex; }

            
        }
        #endregion
    }
}
    
