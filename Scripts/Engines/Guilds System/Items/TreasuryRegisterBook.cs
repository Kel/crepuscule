﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;

namespace Server.Items
{
    [TypeAlias("Server.Gumps.TresorerieBookInscription")]
    public class TreasuryRegisterBook : CrepusculeItem
    {
        private TreasuryBook m_Book;

        [CommandProperty(AccessLevel.GameMaster)]
        public TreasuryBook Book
        {
            get { return m_Book; }
            set { m_Book = value; }
        }


        [Constructable]
        public TreasuryRegisterBook()
            : base(0xFEF)
        {
            this.Name = "Livre de salaires";
        }


        public TreasuryRegisterBook(Serial serial)
            : base(serial)
        {
        }

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);
            if (m_Book == null)
            {
                from.SendMessage("Ce livre n\'est pas lié, appelez un GM!");
                return;
            }
            else
            {
                if (from is RacePlayerMobile)
                {
                    if (from.InRange(this.Location, 2))
                    {
                        RacePlayerMobile rpm = (RacePlayerMobile)from;
                        m_Book.Inscription(rpm);
                    }
                    else
                    {
                        from.SendMessage("Vous êtes trop loin");
                    }
                }
            }
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();

            Book = (TreasuryBook)reader.ReadItem();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version

            writer.Write((TreasuryBook)Book);


        }
    }
 
}
