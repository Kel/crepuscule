﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;

namespace Server.Items
{

    [FlipableAttribute(0x9ab, 0xe7c)]
    [TypeAlias("Server.Gumps.TresorerieDistributor")]
    public class TreasurySalaryDistributor : CrepusculeItem
    {
        private TreasuryBook m_Book;

        private BaseTreasuryContainer m_CoffreTresorerie;

        [CommandProperty(AccessLevel.GameMaster)]
        public BaseTreasuryContainer CoffreTresorerie
        {
            get { return m_CoffreTresorerie; }
            set { m_CoffreTresorerie = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public TreasuryBook Book
        {
            get { return m_Book; }
            set { m_Book = value; }
        }

        /* public override int DefaultGumpID { get { return 0x4A; } }
         public override int DefaultDropSound { get { return 0x42; } }

         public override Rectangle2D Bounds
         {
             get { return new Rectangle2D(20, 105, 150, 180); }
         }*/

        [Constructable]
        public TreasurySalaryDistributor()
            : base(0x9AB)
        {
            Name = "Distributeur de salaires";
        }

        public TreasurySalaryDistributor(Serial serial)
            : base(serial)
        {
            Name = "Distributeur de salaires";
        }

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);
            if (m_Book == null)
            {
                from.SendMessage("Ce coffre n\'est pas lié, appelez un GM! (Manque un Livre)");
                return;
            }
            else if (CoffreTresorerie == null)
            {
                from.SendMessage("Ce coffre n\'est pas lié, appelez un GM! (Manque un Coffre de Guilde)");
                return;
            }
            else
            {
                if (from is RacePlayerMobile)
                {
                    if (from.InRange(this.Location, 2))
                    {
                        if (Book.Employees.Contains(from as RacePlayerMobile))
                        {
                            RacePlayerMobile rpm = (RacePlayerMobile)from;
                            int Go = CoffreTresorerie.GetAmount(typeof(Gold));

                            int TotalAmount = Go;
                            ArrayList ToDelete = new ArrayList();

                            if (rpm.SalaryInfo.SalarySum <= TotalAmount)
                            {
                                int AmountNeed = rpm.SalaryInfo.SalarySum;
                                for (int i = 0; i < CoffreTresorerie.Items.Count; i++)
                                {

                                    if (CoffreTresorerie.Items[i] is Gold)
                                    {
                                        Gold G = (Gold)CoffreTresorerie.Items[i];
                                        if (G.Amount > AmountNeed)
                                        {
                                            G.Consume(AmountNeed);
                                            AmountNeed = 0;
                                            break;
                                        }
                                        else if (G.Amount == AmountNeed)
                                        {
                                            ToDelete.Add(G);
                                            AmountNeed = 0;
                                            break;
                                        }
                                        else if (G.Amount < AmountNeed)
                                        {
                                            int A = G.Amount;
                                            AmountNeed -= A;
                                            ToDelete.Add(G);

                                        }
                                    }
                                }
                                for (int i = 0; i < ToDelete.Count; i++)
                                {
                                    ((Gold)ToDelete[i]).Delete();
                                }
                                Banker.Deposit(rpm, rpm.SalaryInfo.SalarySum);
                                rpm.SendMessage("Vous recuperez " + rpm.SalaryInfo.SalarySum.ToString() + " piéces d'or");
                                rpm.SalaryInfo.SalarySum = 0;
                            }
                            else
                            {
                                rpm.SendMessage("Il n'y a pas assez d'or pour vous payer.");
                            }
                        }
                        else
                        {
                            from.SendMessage("Vous n'êtes pas inscrit.");
                        }
                    }
                    else
                    {
                        from.SendMessage("Vous êtes trop loin");
                    }
                }
            }
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version 

            writer.Write((BaseTreasuryContainer)CoffreTresorerie);
            writer.Write((TreasuryBook)Book);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();

            CoffreTresorerie = (BaseTreasuryContainer)reader.ReadItem();
            Book = (TreasuryBook)reader.ReadItem();
        }
    } 

}
