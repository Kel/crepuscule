using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;

namespace Server.Items
{
    [TypeAlias("Server.Items.cGuildeStone")]
    public class GuildStone: CrepusculeItem
    {
        [Constructable]
        public GuildStone()
            : base(0xED4)
        {
            Movable = false;
            Hue = 33;
            Name = "Pierre de Guilde";
        }

        public override void OnDoubleClick(Mobile from)
        {
            if (from is RacePlayerMobile)
            {
                RacePlayerMobile mobile = from as RacePlayerMobile;
                mobile.SendGump(new GuildStoneGump(mobile));
            }
            else from.SendMessage(33, "D'abord faut virer tous les perso.");
        }

        public GuildStone(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version 
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}
    
