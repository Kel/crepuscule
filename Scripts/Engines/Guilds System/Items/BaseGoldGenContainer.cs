using Server;
using Server.Items;
using Server.Network;
using System;
using System.Collections;
using Server.Mobiles;
using Server.Engines;
using System.Collections.Generic;

namespace Server.Items
{
    [TypeAlias("Server.Items.BaseTresorerie")]
	public abstract class BaseGoldGenContainer : LockableContainer
	{
        internal int m_DailyIncome = 0;
        public static List<BaseGoldGenContainer> Generators = new List<BaseGoldGenContainer>();

        [CommandProperty(AccessLevel.GameMaster)]
        public int DailyIncome
        {
            get { return m_DailyIncome; }
            set { m_DailyIncome = value; }
        }

		public BaseGoldGenContainer( int itemID ) : base ( itemID )
		{
			Locked = true;
			Movable = false;
			Name = "Generateur d'or";
            Generators.Add(this);
		}

        public BaseGoldGenContainer(Serial serial)
            : base(serial)
		{
		}

        public override void OnDelete()
        {
            Generators.Remove(this);
            base.OnDelete();
        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int)0 );
            writer.Write((int)DailyIncome);
		}

        public override void OnDoubleClick(Mobile from)
        {
            if (from.InRange(this.Location, 2))
            {
                base.OnDoubleClick(from);
            }
            else
            {
                from.SendMessage("Vous �tes trop loin");
            }
        }

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

            if (Generators == null) Generators = new List<BaseGoldGenContainer>();
            Generators.Add(this);

            if (version >= 0)
            {
                m_DailyIncome = reader.ReadInt();
            }
		}


		
	}
}









			









