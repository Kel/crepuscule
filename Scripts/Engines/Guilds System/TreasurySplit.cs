﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    public class TreasurySplit
    {
        public static double[] Share(double ToShare, double[] Debts)
        {
            int WindowNum = Debts.Length;
            double[] Result = new double[WindowNum];
            int i, j;
            if (Debts.Sum() <= ToShare)
            {
                for (i = 0; i < WindowNum; i++)
                {
                    Result[i] = Debts[i];
                }
                return Result;
            }

            double[] Share = new double[WindowNum];
            double Debt;

            Share[WindowNum - 1] = Debts[WindowNum - 1];

            for (i = WindowNum - 2; i >= 0; i--)
            {
                Share[i] = Share[i + 1] + Debts[i];
            }

            i = 0;
            while (i < WindowNum - 1)
            {
                Debt = Share[i];
                if (ToShare <= ((WindowNum - i) * Debts[i] / 2))
                { // VC 
                    ToShare = ToShare / (WindowNum - i);
                    for (j = i; j < WindowNum; j++)
                        Result[j] = ToShare;
                    i = j;
                }
                else
                {
                    if (ToShare >= (Debt - (WindowNum - i) * Debts[i] / 2))
                    {
                        for (j = i; j < WindowNum; j++)
                            Result[j] = Debts[j] - (Debt - ToShare) / (WindowNum - i);
                        ToShare = Result[WindowNum - 1];
                        i = j;
                    }
                    else
                    {
                        Result[i] = (ToShare - Plus(ToShare - Debts[i]) + Plus(ToShare - Share[i + 1])) / 2;
                        ToShare = (ToShare + Plus(ToShare - Debts[i]) - Plus(ToShare - Share[i + 1])) / 2;
                    }
                }
                i++;
            }
            Result[WindowNum - 1] = ToShare;
            return Result;
        }

        private static double Plus(double a)
        {
            if (a > 0) return (a);
            return (0);
        }

    }
}
