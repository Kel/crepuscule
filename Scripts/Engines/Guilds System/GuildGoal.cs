﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;
using Server.Guilds;

namespace Server.Engines
{

    public class GuildGoal
    {
        public GuildGoal()
        {
            Guild = GuildType.None;
            Domain = GuildDomain.None;
            CanParticipate = true;
        }

        public GuildGoal(GuildType guild, GuildDomain domain, bool canParticipate)
        {
            Guild = guild;
            Domain = domain;
            CanParticipate = canParticipate;
        }

        public GuildType Guild;
        public GuildDomain Domain;
        public bool CanParticipate;
    }



}
