﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Network;


namespace Server.Guilds
{
    [PropertyObject]
    public class GuildInfo
    {
        public GuildInfo(PlayerMobile player) 
        {
            Player = player as RacePlayerMobile;
        }

        public GuildInfo(GenericReader reader, PlayerMobile player)
        {
            Player = player as RacePlayerMobile;
            Deserialize(reader);
        }

        internal RacePlayerMobile Player;
        internal bool m_ShowGuild = true;
        internal bool m_ShowSecretGuild = false;
        internal bool m_ShowAge = true;
        internal bool m_ShowTiefling = false;
        internal bool m_ShowNoble = true;
        internal NobleType m_NobleType = NobleType.Citoyen;
        internal string m_NobleCustomTitle;
        internal bool m_ShowMonsters = true;
        internal bool m_IsYoung = true;
        internal bool m_IsInvisible = false;
        internal bool m_ShowName = true;
        internal bool m_ShowRace = true;
        internal BaseGuild m_SecretGuild;
        internal string m_SecretGuildTitle;

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)2); //version

            // 2
            writer.Write(m_IsInvisible);

            // 1
            writer.Write(m_ShowTiefling);

            // 0
            writer.Write(m_ShowGuild);
            writer.Write(m_ShowAge);
            writer.Write(m_ShowNoble);
            writer.Write((int)m_NobleType);

            writer.Write(m_NobleCustomTitle);
            writer.Write(m_ShowMonsters);
            writer.Write(m_IsYoung);
            writer.Write(m_SecretGuild);

            writer.Write(m_SecretGuildTitle);
            writer.Write(m_ShowSecretGuild);
            writer.Write(m_ShowName);
            writer.Write(m_ShowRace);
        }

        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();

            switch (version)
            {
                case 2:
                {
                    m_IsInvisible = reader.ReadBool();
                    goto case 1;
                }
                case 1:
                {
                    m_ShowTiefling = reader.ReadBool();
                    goto case 0;
                }
                case 0:
                {
                    m_ShowGuild = reader.ReadBool();
                    m_ShowAge = reader.ReadBool();
                    m_ShowNoble = reader.ReadBool();
                    m_NobleType = (NobleType)reader.ReadInt();

                    m_NobleCustomTitle = reader.ReadString();
                    m_ShowMonsters = reader.ReadBool();
                    m_IsYoung = reader.ReadBool();
                    m_SecretGuild = reader.ReadGuild();

                    m_SecretGuildTitle = reader.ReadString();
                    m_ShowSecretGuild = reader.ReadBool();
                    m_ShowName = reader.ReadBool();
                    m_ShowRace = reader.ReadBool();
                    break;
                }
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowGuild
        {
            get { return m_ShowGuild; }
            set { m_ShowGuild = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowName
        {
            get { return m_ShowName; }
            set { m_ShowName = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowRace
        {
            get { return m_ShowRace; }
            set { m_ShowRace = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowSecretGuild
        {
            get { return m_ShowSecretGuild; }
            set { m_ShowSecretGuild = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowAge
        {
            get { return m_ShowAge; }
            set { m_ShowAge = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowTiefling
        {
            get { return m_ShowTiefling; }
            set { m_ShowTiefling = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowNoble
        {
            get { return m_ShowNoble; }
            set { m_ShowNoble = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public NobleType NobleType
        {
            get { return m_NobleType; }
            set { m_NobleType = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string NobleCustomTitle
        {
            get { return m_NobleCustomTitle; }
            set { m_NobleCustomTitle = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowMonsters
        {
            get { return m_ShowMonsters; }
            set { m_ShowMonsters = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool IsYoung
        {
            get { return m_IsYoung; }
            set { m_IsYoung = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool IsInvisible
        {
            get 
            {
                if (m_IsInvisible == false)
                    return Player.Incognito != 0;
                return m_IsInvisible; 
            }
            set { m_IsInvisible = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public string SecretGuildTitle
        {
            get { return m_SecretGuildTitle; }
            set { m_SecretGuildTitle = value; }
        }

        //Kel: for changes handling
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType PlayerGuild
        {
            get
            {
                if (Player.Guild == null) return GuildType.None;
                return Player.Guild.Type;
            }
            set
            {
                if (!GuildDescriptions.GetGuildIsSecret(value) && !GuildDescriptions.GetGuildIsDisbanded(value)) //not secret one and alive
                {
                    if (Player.Guild == null)
                    {
                        var guild = BaseGuild.Find(value);
                        if (guild != null)
                        {
                            Player.Guild = guild;
                            Player.Guild.OnMemberJoin(Player);
                        }
                    }
                    else if (Player.Guild.Type != value)
                    {
                        Player.Guild.OnMemberLeave(Player);
                        Player.Guild = null;
                        if (value != GuildType.None)
                        {
                            var guild = BaseGuild.Find(value);
                            if (guild != null)
                            {
                                Player.Guild = guild;
                                guild.OnMemberJoin(Player);
                            }
                        }
                    }
                }
            }
        }


        //Kel: for changes handling
        [CommandProperty(AccessLevel.GameMaster)]
        public GuildType SecretGuild
        {
            get
            {
                if (m_SecretGuild == null) return GuildType.None;
                return m_SecretGuild.Type;
            }
            set
            {
                if (GuildDescriptions.GetGuildIsSecret(value) || value == GuildType.None)
                {
                    if (m_SecretGuild == null)
                    {
                        var guild = BaseGuild.Find(value);
                        if (guild != null)
                        {
                            m_SecretGuild = guild;
                            m_SecretGuild.OnMemberJoin(Player);
                        }
                    }
                    else if (m_SecretGuild.Type != value)
                    {
                        m_SecretGuild.OnMemberLeave(Player);
                        this.m_SecretGuild = null;
                        if (value != GuildType.None)
                        {
                            var guild = BaseGuild.Find(value);
                            if (guild != null)
                            {
                                m_SecretGuild = guild;
                                guild.OnMemberJoin(Player);
                            }
                        }
                    }
                }
            }
        }


        public override string ToString()
        {
            return "(Infos de Guilde)";
        }
    }

}
