﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Guilds
{
    public enum NobleType
    {
        Citoyen = 0,
        Ecuyer = 1,
        Chevalier = 2,
        Duc = 3,
        Marquis = 4,
        Baron = 5,
        Comte = 6,
        Custom = 7
    }

    public class GuildDescriptions
    {
        internal static Dictionary<GuildType, GuildDescription> Descriptions = new Dictionary<GuildType, GuildDescription>();
        internal static Dictionary<NobleType, NobleDescription> NobleDescriptions = new Dictionary<NobleType, NobleDescription>();
        internal static List<GuildType> NormalGuilds = new List<GuildType>();
        internal static List<GuildType> SpecialGuilds = new List<GuildType>();
        internal static List<GuildType> SecretGuilds = new List<GuildType>();

        public static void Configure()
        {
            //Special guilds
            Descriptions.Add(GuildType.None, new GuildDescription("Indépendant", "Ind.", false, false, true, new List<BaseGuildSkill>(){}));
            Descriptions.Add(GuildType.Empire, new GuildDescription("Empire", "Emp.", false, false, true, new List<BaseGuildSkill>() { }));



            //Standard guilds
            Descriptions.Add(GuildType.Confrerie, new GuildDescription("Confrerie Pourpre", "CP", false, false, false, new List<BaseGuildSkill>()
            {

            }));
            Descriptions.Add(GuildType.Mercenaires, new GuildDescription("Armée des Mercenaires", "AdM", false, false, false, new List<BaseGuildSkill>()
            {

            }));
            Descriptions.Add(GuildType.Soleil, new GuildDescription("Ordre du Soleil", "OdS", false, false, false, new List<BaseGuildSkill>()
            {

            }));
            Descriptions.Add(GuildType.Assemblee, new GuildDescription("Assemblée Druidique", "AD", false, false, false, new List<BaseGuildSkill>()
            {

            }));
            Descriptions.Add(GuildType.Commercants, new GuildDescription("Ass. des Commerçants", "AdC", false, false, false, new List<BaseGuildSkill>()
            {

            }));



            //Secret guilds
            Descriptions.Add(GuildType.Ombres, new GuildDescription("Guilde des Ombres", "GdO", false, true, false, new List<BaseGuildSkill>()
            {

            }));
            Descriptions.Add(GuildType.Planaires, new GuildDescription("Société Planaire", "SP", false, true, false, new List<BaseGuildSkill>()
            {

            }));



            //Dead guilds
            Descriptions.Add(GuildType.Gitans, new GuildDescription("Union des Gitans", "UdG", true, false, false, new List<BaseGuildSkill>() { }));
            Descriptions.Add(GuildType.Cercle, new GuildDescription("Cercle de Pierre", "CdP", true, false, false, new List<BaseGuildSkill>() { }));
            Descriptions.Add(GuildType.Chene, new GuildDescription("Fraternité du Chêne", "FdC", true, false, false, new List<BaseGuildSkill>() { }));
            Descriptions.Add(GuildType.Concervatoire, new GuildDescription("Conservatoire des Arts", "CdA", true, false, false, new List<BaseGuildSkill>() { }));



            //Noble
            IntitializeNobleDescriptions();

            //Initialize lists
            foreach (var type in Descriptions.Keys)
            {
                if (!Descriptions[type].IsDisbanded &&
                    !Descriptions[type].IsSecret &&
                    !Descriptions[type].IsSpecial)
                {
                    NormalGuilds.Add(type);
                }
                if (Descriptions[type].IsSecret)
                {
                    SecretGuilds.Add(type);
                }
                if (Descriptions[type].IsSpecial)
                {
                    SpecialGuilds.Add(type);
                }
            }


            EventSink.CreateGuild += new CreateGuildHandler(EventSink_CreateGuild);
        }

        #region Initialize Noble Descriptions
        internal static void IntitializeNobleDescriptions()
        {
            NobleDescriptions.Add(NobleType.Citoyen, new NobleDescription("Citoyen", "Citoyenne"));
            NobleDescriptions.Add(NobleType.Ecuyer, new NobleDescription("Écuyer", "Écuyère"));
            NobleDescriptions.Add(NobleType.Chevalier, new NobleDescription("Chevalier", "Chevalière"));
            NobleDescriptions.Add(NobleType.Duc, new NobleDescription("Duc", "Duchesse"));
            NobleDescriptions.Add(NobleType.Marquis, new NobleDescription("Marquis", "Marquise"));
            NobleDescriptions.Add(NobleType.Baron, new NobleDescription("Baron", "Baronne"));
            NobleDescriptions.Add(NobleType.Comte, new NobleDescription("Comte", "Comtesse"));
        }
        #endregion



        #region Initialize Guilds
        public static void Initialize()
        {
            //Create classes if needed
            var guilds = Enum.GetNames(typeof(GuildType));
            foreach (var guildName in guilds)
            {
                var guild = (GuildType)Enum.Parse(typeof(GuildType), guildName);
                if (guild != GuildType.None)
                { // skip not guilded
                    InitializeGuild(guild);
                }
            }
        }

        public static BaseGuild EventSink_CreateGuild(CreateGuildEventArgs e)
        {
            return InitializeGuild((GuildType)e.Id);
        }

        public static BaseGuild InitializeGuild(GuildType guild)
        {
            var result = Guild.Find(guild);
            if (result == null)
            {
               result = new Guild(guild)
                    {
                        Name = GetGuildDescription(guild),
                        Abbreviation = GetGuildAbbreviation(guild),
                        Disbanded = GetGuildIsDisbanded(guild),
                        Secret = GetGuildIsSecret(guild),
                        Special = GetGuildIsSpecial(guild),
                        Type = guild
                    };
            }
            return result;
        }
        #endregion

        #region Helper Methods
        static public List<GuildType> GetNormalGuilds()
        {
            return NormalGuilds;   
        }

        static public List<GuildType> GetSpecialGuilds()
        {
            return SpecialGuilds;
        }

        static public List<GuildType> GetSecretGuilds()
        {
            return SecretGuilds;
        }

        static public string GetNobleDescription(bool female, NobleType type)
        {
            if (female) return NobleDescriptions[type].FemaleName;
            return NobleDescriptions[type].MaleName;
        }

        static public string GetGuildDescription(GuildType _Guild)
        {
            return Descriptions[_Guild].Name;
        }

        static public string GetGuildAbbreviation(GuildType _Guild)
        {
            return Descriptions[_Guild].Abbreviation;
        }

        static public bool GetGuildIsDisbanded(GuildType _Guild)
        {
            return Descriptions[_Guild].IsDisbanded;
        }

        static public bool GetGuildIsSecret(GuildType _Guild)
        {
            return Descriptions[_Guild].IsSecret;
        }

        static public bool GetGuildIsSpecial(GuildType _Guild)
        {
            return Descriptions[_Guild].IsSpecial;
        }

        static public string GetGuildDomainDescription(GuildDomain domain)
        {
            string Description = "";
            switch (domain)
            {
                case GuildDomain.None:
                    Description = "(Non initialisé)";
                    break;
                case GuildDomain.Defense:
                    Description = "Protéction de l'Empire";
                    break;
                case GuildDomain.Knowledge:
                    Description = "Contrôle du Savoir";
                    break;
                case GuildDomain.Magic:
                    Description = "Contrôle de la Magie";
                    break;
                case GuildDomain.Religion:
                    Description = "Promotion de la Religion";
                    break;
                case GuildDomain.Wealth:
                    Description = "Contrôle de la Richesse";
                    break;

            }
            return Description;
        }

        static public string GetGuildInfluenceDescription(GuildType guild, GuildDomain domain)
        {
            double percentage = GuildsSystem.GetPercentage(guild, domain);

            if (percentage >= 50.0) return "Dominant";
            else if (percentage >= 25.0) return "Important";
            else if (percentage >= 10.0) return "Considérable";
            else if (percentage >= 5.0) return "Négligeable";
            else return "Nul";
        }

        static public string GetGuildGlobalInfluenceDescription(GuildType guild)
        {
            double percentage = GuildsSystem.GetGlobalPercentage(guild);

            if (percentage >= 50.0) return "Dominant";
            else if (percentage >= 25.0) return "Important";
            else if (percentage >= 10.0) return "Considérable";
            else if (percentage >= 5.0) return "Négligeable";
            else return "Nul";
        }
        #endregion
    }
}
