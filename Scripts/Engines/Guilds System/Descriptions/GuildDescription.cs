﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Guilds
{
    public class GuildDescription
    {
        public GuildDescription(string name, string abbr, bool isDisbanded, bool isSecret, bool isSpecial, List<BaseGuildSkill> skills)
        {
            Name = name;
            Abbreviation = abbr;
            IsDisbanded = isDisbanded;
            IsSecret = isSecret;
            IsSpecial = isSpecial;
            Skills = skills;
        }

        internal string Name;
        internal string Abbreviation;
        internal bool IsDisbanded;
        internal bool IsSecret;
        internal bool IsSpecial;
        internal List<BaseGuildSkill> Skills = new List<BaseGuildSkill>();
    }
}
