﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Guilds
{
    public class NobleDescription
    {
        public NobleDescription(string maleName, string femaleName)
        {
            MaleName = maleName;
            FemaleName = femaleName;
        }

        internal string MaleName;
        internal string FemaleName;
    }
}
