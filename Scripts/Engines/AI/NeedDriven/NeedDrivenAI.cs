using System;
using System.Collections;
using Server.Targeting;
using Server.Network;
using System.Linq;
using Server.Engines.AI.NeedDriven;

//
// This is a first simple AI
//
//

namespace Server.Mobiles
{
	public class NeedDrivenAI : BaseAI
	{
        /// <summary>
        /// Needs of the NPC
        /// </summary>
        NeedStack Needs = new NeedStack();

		public NeedDrivenAI(BaseCreature m) : base (m)
		{
        }

        #region DoActionWander
        public override bool DoActionWander()
        {
            if (m_Mobile.Combatant != null)
            {
                m_Mobile.Say(Utility.RandomList(1005305, 501603));
                Action = ActionType.Flee;
            }
            else
            {
                if (AquireFocusMob(m_Mobile.RangePerception, FightMode.Closest, false, true, true))
                {
                    m_Mobile.Say("Oh... Bonjour...");
                }

                if (m_Mobile.FocusMob != null)
                {
                    Mobile focused = m_Mobile.FocusMob;
                    if (m_Mobile.FearLove.Contains(focused))
                    {
                        m_Mobile.Say("Ah oui, je me souviens de vous!");
                    }
                    else
                    {
                        m_Mobile.Say("Laissez moi vous mémoriser...");
                        //Memorize
                        if (m_Mobile is BaseCreature) (m_Mobile as BaseCreature).MemorizeMobile(focused);
                    }

                    //Vendor behaviour
                    Action = ActionType.Interact;
                }
                else
                {
                    m_Mobile.Warmode = false;
                    base.DoActionWander();
                }
            }

            return true;
        }
        #endregion

        #region Need-Driven Action

        /// <summary>
        /// Before the AI decides what to do it needs to evaluate its environment. The AI
        /// need to look around and memorize all objects and creatures that it can perceive.
        /// This can be done using normal LOS, or any other suitable way. Each object or
        /// creature (from now on entity) perceived is assigned a Fear/Love value (1.2) and
        /// a Hate value (1.3). The most Feared, Loved and Hated entity is memorized (1.4)
        /// and the Fear/Love and Hate center is calculated (1.5). These values will be used
        /// later on to determine where to move or whom to attack.
        /// </summary>
        public virtual void OnObserveEnvironment()
        {

        }
        #endregion


        #region Basic Actions
 

		public override bool DoActionInteract()
		{
			Mobile customer = m_Mobile.FocusMob;

			if ( m_Mobile.Combatant != null )
			{
				if ( m_Mobile.Debug )
					m_Mobile.DebugSay( "{0} is attacking me", m_Mobile.Combatant.Name );

				m_Mobile.Say( Utility.RandomList( 1005305, 501603 ) );

				Action = ActionType.Flee;
				
				return true;
			}

			if ( customer == null || customer.Deleted || customer.Map != m_Mobile.Map )
			{
				m_Mobile.DebugSay( "My customer have disapeared" );
				m_Mobile.FocusMob = null;

				Action = ActionType.Wander;
			}
			else
			{
				if ( customer.InRange( m_Mobile, m_Mobile.RangeFight ) )
				{
					if ( m_Mobile.Debug )
						m_Mobile.DebugSay( "I am with {0}", customer.Name );

					m_Mobile.Direction = m_Mobile.GetDirectionTo( customer );
				}
				else
				{
					if ( m_Mobile.Debug )
						m_Mobile.DebugSay( "{0} is gone", customer.Name );

					m_Mobile.FocusMob = null;

					Action = ActionType.Wander;	
				}
			}

			return true;
		}

		public override bool DoActionGuard()
		{
			m_Mobile.FocusMob = m_Mobile.Combatant;
			return base.DoActionGuard();
		}

		public override bool HandlesOnSpeech( Mobile from )
		{
			if ( from.InRange( m_Mobile, 4 ) )
				return true;

			return base.HandlesOnSpeech( from );
		}

		// Temporary 
		public override void OnSpeech( SpeechEventArgs e )
		{
			base.OnSpeech( e );
 
			Mobile from = e.Mobile;
 
			if ( m_Mobile is BaseVendor && from.InRange( m_Mobile, 4 ) && !e.Handled )
			{
				if ( e.HasKeyword( 0x14D ) ) // *vendor sell*
				{
					e.Handled = true;

					((BaseVendor)m_Mobile).VendorSell( from );
					m_Mobile.FocusMob = from;
				}
				else if ( e.HasKeyword( 0x3C ) )
				{
					e.Handled = true;

					((BaseVendor)m_Mobile).VendorBuy( from );
					m_Mobile.FocusMob = from;
				}
				else if ( WasNamed( e.Speech ) )
				{
					e.Handled = true;

					if ( e.HasKeyword( 0x177 ) ) // *sell*
						((BaseVendor)m_Mobile).VendorSell( from );
					else if ( e.HasKeyword( 0x171 ) ) // *buy*
						((BaseVendor)m_Mobile).VendorBuy( from );

					m_Mobile.FocusMob = from;
				}
			}
        }
        #endregion
    }
}