﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Mobiles;

namespace Server.Engines.AI.NeedDriven
{
    public static class AIMobileExtensions
    {
        /// <summary>
        /// This method returns the entity's current hitpoints divided by its maximum hitpoints.
        /// </summary>
        public static double GetPhysicalCondition(this Mobile source)
        {
            return (double)(source.Hits / source.HitsMax);
        }

        /// <summary>
        /// This method returns the entity's current hitpoints divided by its maximum hitpoints.
        /// </summary>
        public static PhysicalCondition GetPhysicalConditionState(this Mobile source)
        {
            double Condition= source.GetPhysicalCondition();
            if (Condition > 1) return PhysicalCondition.Boosted;
            else if (Condition == 1) return PhysicalCondition.Unharmed;
            else if (Condition > 0) return PhysicalCondition.Damaged;
            else return PhysicalCondition.Dead;
        }

        /// <summary>
        /// This method should return the entity's perceived strength, ie how strong it
        /// looks if we were to see it. This value should be based on the entity's physique,
        /// not powerful equipment. Returned value is based on the entity's strength multiplied by
        /// it's PhysicalCondition.
        /// </summary>
        public static double GetPerceivedStrength(this Mobile source)
        {
            return (double)(source.Str * source.GetPhysicalCondition());
        }

        /// <summary>
        /// This method should return the entity's perceived danger, ie how dangerous it
        /// looks. This value should be based on the entity's equipment. If the entity has
        /// sharp weapons and heavy armour the returned value should be high.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static double GetPerceivedDanger(this Mobile source)
        {
//#warning    // Later should be optimised (cashed) for the monsters

            double Danger = 0;
            IEnumerable<Item> MobileItems = source.Items.Cast<Item>();
            var Weapons = (from item in MobileItems
                          where item is BaseWeapon
                          select (item as BaseWeapon).WeaponRating()).Sum();

            var Armors =  (from item in MobileItems
                          where item is BaseArmor
                          select (item as BaseArmor).ArmorRating).Sum();

            Danger = Weapons + Armors;
            return Danger;
        }

        /// <summary>
        /// Memorizes the mobile generating the first hate/love and fear values
        /// </summary>
        public static void MemorizeMobile(this BaseCreature source, Mobile mobile)
        {
            if (!source.FearLove.Contains(mobile))
            {
                double FearLoveValue = 0;
                source.FearLove.Add(mobile, FearLoveValue);
            }
            if (!source.Hate.Contains(mobile))
            {
                double HateValue = 0;
                source.Hate.Add(mobile, HateValue);
            }
        }
    }

    public enum PhysicalCondition
    {
        /// <summary>
        /// The creature is dead
        /// </summary>
        Dead,
        /// <summary>
        /// The creature is damaged
        /// </summary>
        Damaged,
        /// <summary>
        /// The creature is unharmed, healthy
        /// </summary>
        Unharmed,
        /// <summary>
        /// The creature is healthy and boosted (spells, potions...)
        /// </summary>
        Boosted
    }

}
