﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;

namespace Server.Engines.AI.NeedDriven
{
    public static class AIItemExtensions
    {
        public static double WeaponRating(this BaseWeapon source)
        {
            return (double)(source.MaxDamage - source.MinDamage);
        }

    }
}
