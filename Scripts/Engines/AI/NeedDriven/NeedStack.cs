﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines.AI.NeedDriven
{

    /// <summary>
    /// This list helps to determine which need is our top priority
    /// </summary>
    public class NeedStack : SortableList
    {
        public NeedStack() : base()
        {
            this.KeepSorted = true;

            this.Add(new Need(NeedType.Drink,0.2f));
            this.Add(new Need(NeedType.Eat, 0.1f));
        }

        public Need PriorityNeed { get { return this[this.IndexOfMin()] as Need; } }

        /// <summary>
        /// Degrades all needs
        /// </summary>
        public void DegradeNeeds()
        {
            foreach (Need need in this)
                need.Degrade();
            this.Sort();
        }
    }

    public class Need : IComparable
    {
        public NeedType Type;
        public float Value;
        public float DegradationFactor;

        public Need(NeedType type, float degradationFactor)
        {
            Type = type;
            DegradationFactor = degradationFactor;
            Value = 100f;
        }

        public void Degrade() { Value -= DegradationFactor; if (Value < -100) Value = -100; }

        #region IComparable Members
        public int CompareTo(object other)
        {
            return Convert.ToInt32(Value - ((Need)other).Value);
        }
        #endregion
    }

    public enum NeedType
    {
        Eat,
        Drink       
    }
}
