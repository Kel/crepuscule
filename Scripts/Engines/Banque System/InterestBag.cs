using System;
using Server;
using Server.Items;
using System.Collections;
using Server.Multis;
using Server.Mobiles;
using Server.Network;
using Server.Misc;

namespace Server.Items
{
	public class InterestBag : Bag
	{
		public override int MaxWeight{ get{ return 0; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

        private int m_SommeMax = 30;
        private int m_Interet = 0;

        [CommandProperty(AccessLevel.Administrator)]
        public int SommeMax
        {
            get { return m_SommeMax; }
            set { m_SommeMax = value; }
        }
        
        [CommandProperty(AccessLevel.GameMaster)]
        public int Interet
        {
            get { return m_Interet; }
            set { m_Interet = value; }
        }

		[Constructable]
		public InterestBag() : base()
		{
            Name = "Compte d'�pargne";
			Movable = false;
            Hue = 876;
			LootType = LootType.Blessed;
            SommeMax = 30;
            Interet = 0;
		}

		public InterestBag( Serial serial ) : base( serial )
		{
		}
		
		public override void OnDoubleClick(Mobile from)
		{
			if (Interet != 0)
			{
				from.AddToBackpack ( new BankCheck ( Interet ) );
				Interet = 0;
			}
			base.OnDoubleClick(from);
		}
		                              

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 2 ); // version

            writer.Write(SommeMax);
            writer.Write(Interet);
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

            if (version == 2)
            {
            	SommeMax = reader.ReadInt();
            	Interet = reader.ReadInt();
            }
            if (version == 1)
            {
            	Interet = 0;
                SommeMax = reader.ReadInt();
            }
            if (version == 0)
            {
            	Interet = 0;
                SommeMax = 30;
            }
            
		}
	}
}
