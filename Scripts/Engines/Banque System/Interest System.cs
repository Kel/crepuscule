using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Misc
{
	public class interestTimer : Timer
	{
		public static void Initialize()
		{
			new interestTimer();
		}
		public interestTimer() : base(TimeSpan.FromSeconds(1),TimeSpan.FromHours(1))
		{
			this.Start();
		}
		protected override void OnTick()
		{
			switch(DateTime.Now.Hour)
			{
				case 23: //Change what hour it gives interest here
				{
					ArrayList bankBoxes = new ArrayList();
					ArrayList interestBags = new ArrayList();
					ArrayList intToGive = new ArrayList();

					foreach( Item item in World.Items.Values )
					{
						if( item is BankBox )
						{
							BankBox box = (BankBox)item;
							bankBoxes.Add( box );
						}
					}
					for( int i = 0; i < bankBoxes.Count; ++i )
					{
						bool notFound = true;
						BankBox bb = (BankBox)bankBoxes[i];
						int totalGold = 0;
						foreach( Item inBank in bb.Items )
						{
							if( inBank is BankCheck )
							{
								BankCheck bc = (BankCheck)inBank;
								totalGold += bc.Worth;
							}
							else if( inBank is Gold )
							{
								Gold g = (Gold)inBank;
								totalGold += g.Amount;
							}


							else if( inBank is Container )
							{
								if( inBank is InterestBag && notFound )
								{
									InterestBag ibag = (InterestBag)inBank;
									interestBags.Add( ibag );
									notFound = false;
								}
								var ItemsInBag = inBank.Items;
								for( int z = 0; z < ItemsInBag.Count; z++ )
								{
									Item inBag = (Item)ItemsInBag[z];
									if( inBag is Gold )
									{
										Gold gib = (Gold)inBag;
										totalGold += gib.Amount;
									}
									else if( inBag is BankCheck )
									{
										BankCheck bcib = (BankCheck)inBag;
										totalGold += bcib.Worth;
                                    }

								}
							}
						}
						if( notFound )
						{
							InterestBag ib = new InterestBag();
							bb.AddItem( ib );
							ib.Location = new Point3D( 0, 0, 0 );
							interestBags.Add( ib );
							notFound = false;
						}
						intToGive.Add( totalGold );
					}
					for( int b = 0; b < interestBags.Count; ++b )
					{
						InterestBag toPutIn = (InterestBag)interestBags[b];
						int totalGold = (int)intToGive[b];
						totalGold += toPutIn.Interet;
                        int pourcentage = 1; // 1%
                        int interestToGive = ((int)totalGold / 100) * pourcentage;  //Change amount to give here

                        
                        if (interestToGive > toPutIn.SommeMax)
                            interestToGive = toPutIn.SommeMax;
						
                        toPutIn.Interet += interestToGive;
					}
				}break;
			}
		}
	}
}
