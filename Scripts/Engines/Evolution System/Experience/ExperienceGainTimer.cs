﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Server.Mobiles
{
    public class ExperienceGainTimer : Timer
    {
        private RacePlayerMobile m_Mobile;
        private static Hashtable m_ExperienceGainTimers = new Hashtable();
        private Mobile m_Who;
        public string m_What = "";

        public static Hashtable ExperienceGainTimers { get { return m_ExperienceGainTimers; } }

        public static void Initialize()
        {
            EventSink.Logout += new LogoutEventHandler(OnLogout);
            EventSink.Login += new LoginEventHandler(OnLogin);
            EventSink.Disconnected += new DisconnectedEventHandler(RacePlayerMobile.EventSink_Disconnected);
        }

        public void StopAll()
        {
            m_ExperienceGainTimers.Remove(m_Who.Serial.Value);
            //((RacePlayerMobile)e.Mobile).Timers_Started = (bool)true;
            this.Stop();
        }

        // Apres 2.5 secondes, le timer sera declanché tout les secondes. apres 5 ticks, ça s'arretera
        public ExperienceGainTimer(RacePlayerMobile from)
            : base(TimeSpan.Zero, TimeSpan.FromSeconds(60.0))
        {
            m_Mobile = (RacePlayerMobile)from;
            m_Who = (RacePlayerMobile)from;
            this.Start();
        }

        public static void Disconnect(object state)
        {
            if (m_ExperienceGainTimers.Contains(((RacePlayerMobile)state).Serial.Value))
            {
                ExperienceGainTimer timer_exp = (ExperienceGainTimer)m_ExperienceGainTimers[((RacePlayerMobile)state).Serial.Value];
                if (timer_exp == null)
                {
                    ((RacePlayerMobile)state).SendMessage("Erreur, objet timer inexistant ! ");
                    return;
                }
                timer_exp.StopAll();
            }
        }

        public static void OnLogin(LoginEventArgs e)
        {
            if (m_ExperienceGainTimers.Contains(e.Mobile.Serial.Value))
            {
                ExperienceGainTimer timer_exp = (ExperienceGainTimer)m_ExperienceGainTimers[e.Mobile.Serial.Value];
                if (timer_exp == null)
                {
                    e.Mobile.SendMessage("Objet Timer inexistant!");
                    return;
                }
                timer_exp.Start();
            }
            else
            {
                m_ExperienceGainTimers.Add(e.Mobile.Serial.Value, new ExperienceGainTimer((RacePlayerMobile)(e.Mobile)));
                ((RacePlayerMobile)e.Mobile).Timers_Started = true;
                //Timer timer_exp = new Timer_EXP((RacePlayerMobile)e.Mobile);
                //timer_exp.Start();
                //this.Start();
                //((RacePlayerMobile)from).Timers_Started = (bool)true;
            }
        }

        public static void OnLogout(LogoutEventArgs e)
        {
            if (m_ExperienceGainTimers.Contains(e.Mobile.Serial.Value))
            {
                ExperienceGainTimer timer_exp = (ExperienceGainTimer)m_ExperienceGainTimers[e.Mobile.Serial.Value];
                if (timer_exp == null)
                {
                    e.Mobile.SendMessage("Erreur, objet timer inexistant ! ");
                    return;
                }
                timer_exp.StopAll();
            }
        }

        public void DoTick()
        {
            OnTick();
        }

        protected override void OnTick()
        {
            var Evo = m_Mobile.EvolutionInfo;
            if (Evo.XPTicks <= 0)
            {
                if (Evo.GradesLimit == 0)
                {
                    if (m_Mobile.Timers_Started == true)
                        m_Mobile.EvolutionInfo.XPTicks = 1;
                }
                else
                {
                    Evo.GradesLimit--;

                    int PreviousLevel = Evo.Level;
                    m_Mobile.SendMessage("Vous avez acquis de l'experience.");
                    Evo.Experience += 1;

                    // New algorithm
                    var mean = ((float)(Evo.Grade1 + Evo.Grade2 + Evo.Grade3 + Evo.Grade4) / 4);
                    Evo.XPTicks = (int)Math.Round((1 / mean) * 120, 0);


                    if (PreviousLevel < Evo.Level)
                    {
                        m_Mobile.SendMessage("Félicitations! Vous avez atteint le niveau " + Evo.Level);
                        m_Mobile.SkillsCap += 1400;
                    }
                }
            }
            else
            {
                if (m_Mobile.Timers_Started == true)
                {
                    Evo.XPTicks -= 1;
                }

            }

        }
    }
}
