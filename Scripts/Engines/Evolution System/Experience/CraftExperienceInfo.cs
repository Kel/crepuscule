﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject]
    public class CraftExperienceInfo
    {
        /// <summary>
        /// 10K XP per item is the limit for all calculation
        /// </summary>
        private const int XPLimit = 5000;
        private RacePlayerMobile Player;
        public Dictionary<Type, int> Table = new Dictionary<Type, int>();

        public void AddXP(Type type, int xp)
        {
            if (Table.ContainsKey(type))
            {
                if (Table[type] <= XPLimit)
                {
                    Table[type] += xp;
                    if(Table[type] > XPLimit) Table[type] = XPLimit;
                    if (Table[type] % 100 == 0)
                        Player.SendMessage(String.Format("Vous progressez dans la fabrication"));
                }
            }
            else
                Table.Add(type, xp);
        }

        public int GetXP(Type type)
        {
            if (Table.ContainsKey(type))
                return Table[type];
            else
                return 0;
        }

        /// <summary>
        /// Expertise level, goes from 0 to 100%
        /// </summary>
        public short GetPercetage(Type type)
        {
            var xp = GetXP(type);
            return (short)Math.Round( (xp / (float)XPLimit) * 100 );
        }

        /// <summary>
        /// Rolls a dice
        /// </summary>
        public bool GetChance(Type type)
        {
            var rand = Utility.Random(1, 110);
            if (GetPercetage(type) >= rand)
                return true;
            return false;
        }
        
        #region Constructors & Serialization
        public CraftExperienceInfo(RacePlayerMobile player)
        {
            Player = player;
        }

        public CraftExperienceInfo(GenericReader reader, RacePlayerMobile player)
        {
            Player = player;
            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        var entries = reader.ReadInt();
                        for (int i = 0; i < entries; i++)
                        {
                            var type = reader.ReadType();
                            var xp = reader.ReadInt();
                            if (type != null)
                            {
                                if (!Table.ContainsKey(type))
                                {
                                    Table.Add(type, xp);
                                }
                                else
                                {
                                    Table[type] += xp;
                                }
                            }
                        }
                        break;
                    }
            }
        }


        public virtual void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Table.Count);
            foreach(var key in Table.Keys)
            {
                writer.Write(key);
                writer.Write(Table[key]);
            }

        }

        public override string ToString()
        {
            return "(Craft XP)";
        }


        #endregion
    }


    public static class CraftExperiencePMExtensions
    {
        public static void AddCraftXP(this RacePlayerMobile player, Type itemType, int xp)
        {
            player.EvolutionInfo.CraftExperience.AddXP(itemType, xp);
        }

        public static int GetCraftXP(this RacePlayerMobile player, Type itemType)
        {
            return player.EvolutionInfo.CraftExperience.GetXP(itemType);
        }

        public static bool GetCraftXPChance(this RacePlayerMobile player, Type itemType)
        {
            return player.EvolutionInfo.CraftExperience.GetChance(itemType);
        }

        public static int GetCraftXPPercentage(this RacePlayerMobile player, Type itemType)
        {
            return player.EvolutionInfo.CraftExperience.GetPercetage(itemType);
        }
    }
}
