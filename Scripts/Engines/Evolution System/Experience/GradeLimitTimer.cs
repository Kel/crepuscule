using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server.Misc
{
	public class GradeLimitTimer : Timer
	{
		public static void Initialize()
		{
			new GradeLimitTimer();
		}
		public GradeLimitTimer() : base(TimeSpan.FromSeconds(1),TimeSpan.FromHours(1))
		{
			this.Start();
		}
		protected override void OnTick()
		{
			switch(DateTime.Now.Hour)
			{
				case 20: //Change what hour it gives cotelimit here
				{
                    SetLimit();
                    //World.Save();
				}break;
			}
		}

        public static void SetLimit()
        {
            foreach (Mobile m in World.Mobiles.Values)
            {
                if ( m != null && m is RacePlayerMobile)
                {
                    var pm = m as RacePlayerMobile;
                    if (pm.AccessLevel == AccessLevel.Player && m != null)
                    {
                        switch (pm.EvolutionInfo.GradeMean)
                        {
                            case 1:
                            pm.EvolutionInfo.GradesLimit = 0;
                            break;
                            case 2:
                            pm.EvolutionInfo.GradesLimit = 1;
                            break;
                            case 3:
                            pm.EvolutionInfo.GradesLimit = 2;
                            break;
                            case 4:
                            pm.EvolutionInfo.GradesLimit = 4;
                            break;
                            case 5:
                            pm.EvolutionInfo.GradesLimit = 5;
                            break;
                            case 6:
                            pm.EvolutionInfo.GradesLimit = 6;
                            break;
                            default:
                            pm.EvolutionInfo.GradesLimit = 2;
                            break;
                        }
                    }
                }
            }
        }
	}
}