using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;

namespace Server.Engines
{
    public class Races
    {
        public static void SelectRace(RacePlayerMobile mobile, RaceType race)
        {
            if (race == RaceType.None)
            {

            }
            else
            {
                GetStats(race).SetToMobile(mobile);
                mobile.Name = GetRaceName(mobile);

                // Reset the skills
                for (int i = 0; i < 52; i++)
                {
                    mobile.Skills[i].Cap = 0;
                    mobile.Skills[i].Base = 0;
                }

                // Set skins
                if (race == RaceType.Elfe)
                    mobile.EquipItem(new SkinElf());
                else if (race == RaceType.Gnome)
                    mobile.EquipItem(new SkinGnome());
                else if (race == RaceType.Petite_Personne)
                    mobile.EquipItem(new SkinHalfling());
                else if (race == RaceType.Nain)
                    mobile.EquipItem(new SkinDwarf());
                else if (race == RaceType.Demi_Orque)
                    mobile.EquipItem(new SkinHalfOrc());
                else if (race == RaceType.Elfe_Noir)
                    mobile.EquipItem(new SkinDrow());


                //#warning Choose a branch here?
                mobile.SendGump(new ChoseBranchGump());
                //mobile.SendGump ( new ClasseChoixGump ( mobile ) );
            }
        }

        #region Static Helpers

        /// <summary>
        /// Gets the maximum stats for a particular race
        /// </summary>
        public static RaceStats GetStats(RaceType race)
        {
            var result = new RaceStats();
            if (race == RaceType.Elfe)
            {
                result.StatCap = 300;
                result.MaxStr = 110;
                result.MaxDex = 140;
                result.MaxInt = 150;
            }
            else if (race == RaceType.Gnome)
            {
                result.StatCap = 300;
                result.MaxStr = 110;
                result.MaxDex = 145;
                result.MaxInt = 145;
            }
            else if (race == RaceType.Petite_Personne)
            {
                result.StatCap = 300;
                result.MaxStr = 115;
                result.MaxDex = 150;
                result.MaxInt = 135;
            }
            else if (race == RaceType.Demi_Elfe)
            {
                result.StatCap = 300;
                result.MaxStr = 120;
                result.MaxDex = 140;
                result.MaxInt = 140;
            }
            else if (race == RaceType.Humain)
            {
                result.StatCap = 300;
                result.MaxStr = 130;
                result.MaxDex = 135;
                result.MaxInt = 135;
            }
            else if (race == RaceType.Nain)
            {
                result.StatCap = 300;
                result.MaxStr = 145;
                result.MaxDex = 120;
                result.MaxInt = 135;
            }
            else if (race == RaceType.Demi_Orque)
            {
                result.StatCap = 300;
                result.MaxStr = 150;
                result.MaxDex = 135;
                result.MaxInt = 115;
            }
            else if (race == RaceType.Elfe_Noir)
            {
                result.StatCap = 300;
                result.MaxStr = 120;
                result.MaxDex = 140;
                result.MaxInt = 140;
            }
            else if (race == RaceType.Tiefling)
            {
                result.StatCap = 300;
                result.MaxStr = 130;
                result.MaxDex = 135;
                result.MaxInt = 135;
            }

            return result;
        }

        public static string GetAlignementName(AlignementType alignment)
        {
            if (alignment == AlignementType.Chaotique_Bon) return "Chaotique-Bon";
            if (alignment == AlignementType.Chaotique_Mauvais) return "Chaotique-Bon";
            if (alignment == AlignementType.Chaotique_Neutre) return "Chaotique-Bon";
            if (alignment == AlignementType.Loyal_Bon) return "Loyal-Bon";
            if (alignment == AlignementType.Loyal_Mauvais) return "Loyal-Mauvais";
            if (alignment == AlignementType.Loyal_Neutre) return "Loyal-Neutre";
            if (alignment == AlignementType.Neutre_Bon) return "Neutre-Bon";
            if (alignment == AlignementType.Neutre_Mauvais) return "Neutre-Mauvais";
            if (alignment == AlignementType.Neutre_Strict) return "Neutre Strict";

            return "/";
        }

        public static string GetRaceName(RacePlayerMobile mobile) 
        {
            return GetRaceName(mobile, false);
        }

        /// <summary>
        /// Gets the race name
        /// </summary>
        public static string GetRaceName(RacePlayerMobile mobile, bool ignoreShowRace)
        {
            var result = "";
            var race = mobile.EvolutionInfo.Race;
            if (!mobile.GuildInfo.ShowRace && !ignoreShowRace)
            {
                if (race == RaceType.Petite_Personne || race == RaceType.Nain || race == RaceType.Gnome)
                    result = "Humanoide de petite taille";
                else
                    result = "Humanoide de taille moyenne";
            }
            else
            {
                if (mobile.Female == false)
                {
                    if (race == RaceType.Elfe)
                        result = "Elfe";
                    else if (race == RaceType.Gnome)
                        result = "Gnome";
                    else if (race == RaceType.Petite_Personne)
                        result = "Petite Personne";
                    else if (race == RaceType.Humain)
                        result = "Humain";
                    else if (race == RaceType.Nain)
                        result = "Nain";
                    else if (race == RaceType.Demi_Orque)
                        result = "Demi Orque";
                    else if (race == RaceType.Demi_Elfe)
                        result = "Demi Elfe";
                    else if (race == RaceType.Elfe_Noir)
                        result = "Elfe Noir";
                    else if (race == RaceType.Tiefling && mobile.GuildInfo.ShowTiefling == true)
                        result = "Tiefling";
                    else if (race == RaceType.Tiefling && mobile.GuildInfo.ShowTiefling == false)
                        result = "Humain";
                }
                else
                {
                    if (race == RaceType.Elfe)
                        result = "Elfe";
                    else if (race == RaceType.Gnome)
                        result = "Gnome";
                    else if (race == RaceType.Petite_Personne)
                        result = "Petite Personne";
                    else if (race == RaceType.Demi_Orque)
                        result = "Demi Orque";
                    else if (race == RaceType.Demi_Elfe)
                        result = "Demi Elfe";
                    else if (race == RaceType.Tiefling && mobile.GuildInfo.ShowTiefling == true)
                        result = "Tiefling";
                    else if (race == RaceType.Tiefling && mobile.GuildInfo.ShowTiefling == false)
                        result = "Humaine";
                    else if (race == RaceType.Humain)
                        result = "Humaine";
                    else if (race == RaceType.Nain)
                        result = "Naine";
                    else if (race == RaceType.Elfe_Noir)
                        result = "Elfe Noire";

                }
            }
            return result;
        }

        #endregion

    }

    public class RaceStats
    {
        public int StatCap;
        public int MaxDex;
        public int MaxStr;
        public int MaxInt;

        public void SetToMobile(RacePlayerMobile mobile)
        {
            mobile.StatCap = StatCap;
            mobile.MaxDex = MaxDex;
            mobile.MaxInt = MaxInt;
            mobile.MaxStr = MaxStr;
        }
    }
}
