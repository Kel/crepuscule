﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject]
    public class EvolutionInfo
    {
        #region Fields
        private int m_Level;
        private int m_Experience;
        private AlignementType m_Alignement;
        internal RaceType m_Race;
        private short m_DestinyPoints;
        private int m_Age;
        internal bool m_IsResetting = false;
        internal bool m_MindShieldOff = false;

        private int m_XPTicks;
        private short m_LastGrade;
        private short m_Grade1;
        private short m_Grade2;
        private short m_Grade3;
        private short m_Grade4;
        private string m_Grade1By;
        private string m_Grade2By;
        private string m_Grade3By;
        private string m_Grade4By;
        private DateTime m_GradeTime;
        private short m_GradesLimit;
        private CraftExperienceInfo m_CraftExperience;
        #endregion

        #region Properties
        [CommandProperty(AccessLevel.GameMaster)]
        public CraftExperienceInfo CraftExperience
        {
            get { return m_CraftExperience; }
            set { m_CraftExperience = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Age
        {
            get { return m_Age; }
            set { m_Age = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Level
        {
            get { return m_Level; }
            set { m_Level = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int PointsSpent
        {
            get 
            {
                var result = 0;
                for(int i=0;i<CapacityInfo.CapacitiesCount;++i)
                {
                    var capacity = Player.Capacities[i];
                    result += CapacitiesConfig.GetCostValue(capacity.Cost) * capacity.Value;
                }
                return result;
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int PointsAvailable
        {
            get { return PointsTotal - PointsSpent; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int PointsTotal
        {
            get { return EvolutionConfig.GetPoints(Level); }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Experience
        {
            get { return m_Experience; }
            set 
            {
                m_Experience = value;
                Level = EvolutionConfig.GetLevel(value);
                if (Player != null /*&& Player.AccessLevel == AccessLevel.Player*/)
                {
                    EvolutionConfig.SetStats(Player);
                    EvolutionConfig.SetSkillCaps(Player);
                }
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int NextLevelExperience
        {
            get { return EvolutionConfig.GetExperience(Level + 1); }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxDestinyPoints
        {
            get { return EvolutionConfig.GetDestinyPoints(Player); }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int ExperienceNeeded
        {
            get { return EvolutionConfig.GetExperience(Level + 1) - m_Experience; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public AlignementType Alignement
        {
            get { return m_Alignement; }
            set { m_Alignement = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public RaceType Race
        {
            get { return m_Race; }
            set 
            {
                m_Race = value;
                Races.SelectRace(Player, value);
                Player.InvalidateProperties();
            }
        }
        [CommandProperty(AccessLevel.Administrator)]
        public int XPTicks
        {
            get { return m_XPTicks; }
            set { m_XPTicks = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public short DestinyPoints
        {
            get { return m_DestinyPoints; }
            set { m_DestinyPoints = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public short LastGrade
        {
            get { return m_LastGrade; }
            set { m_LastGrade = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public short Grade1
        {
            get { return m_Grade1; }
            set { m_Grade1 = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public short Grade2
        {
            get { return m_Grade2; }
            set { m_Grade2 = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public short Grade3
        {
            get { return m_Grade3; }
            set { m_Grade3 = value; }
        }

        [CommandProperty(AccessLevel.Administrator)]
        public short Grade4
        {
            get { return m_Grade4; }
            set { m_Grade4 = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public bool ResetPA
        {
            get { return false; }
            set 
            {
                if(!m_IsResetting)
                    ClassConfig.ShowResetGump(Player);
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public bool ResetStatCaps
        {
            get { return false; }
            set
            {
                EvolutionConfig.SetStats(Player);
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public bool ResetStats
        {
            get { return false; }
            set
            {
                Player.Str = 30;
                Player.Dex = 30;
                Player.Int = 30;
                EvolutionConfig.SetStats(Player);
            }
        }
        public string Grade1By
        {
            get { return m_Grade1By; }
            set { m_Grade1By = value; }
        }
        public string Grade2By
        {
            get { return m_Grade2By; }
            set { m_Grade2By = value; }
        }
        public string Grade3By
        {
            get { return m_Grade3By; }
            set { m_Grade3By = value; }
        }
        public string Grade4By
        {
            get { return m_Grade4By; }
            set { m_Grade4By = value; }
        }
        public DateTime GradeTime
        {
            get { return m_GradeTime; }
            set { m_GradeTime = value; }
        }
        public short GradeMean
        {
            get { return (short) Math.Round(((float)Grade1 + Grade2 + Grade3 + Grade4) / 4.0f); }
        }
        public short GradesLimit
        {
            get { return m_GradesLimit; }
            set { m_GradesLimit = value; }
        }
        public bool IsResetting
        {
            get { return m_IsResetting; }
            set { m_IsResetting = value; }
        }
        #endregion

        #region Constructors & Serialization
        internal RacePlayerMobile Player;
        public EvolutionInfo(PlayerMobile player)
        {
            Player = player as RacePlayerMobile;
            CraftExperience = new CraftExperienceInfo(Player);
        }

        public EvolutionInfo(PlayerMobile player, GenericReader reader)
        {
            Player = player as RacePlayerMobile;
            var version = reader.ReadInt();

            switch (version)
            {

                case 1:
                    {
                        CraftExperience = new CraftExperienceInfo(reader, Player);
                        goto case 0;
                    }
                case 0:
                    {
                        if (CraftExperience == null)
                            CraftExperience = new CraftExperienceInfo(Player);

                        m_Level = reader.ReadInt();
                        m_Experience = reader.ReadInt();
                        m_Alignement = (AlignementType)reader.ReadInt();
                        m_Race = (RaceType)reader.ReadInt();
                        m_DestinyPoints = reader.ReadShort();
                        m_XPTicks = reader.ReadInt();
                        m_LastGrade = reader.ReadShort();
                        m_Grade1 = reader.ReadShort();
                        m_Grade2 = reader.ReadShort();
                        m_Grade3 = reader.ReadShort();
                        m_Grade4 = reader.ReadShort();
                        m_Grade1By = reader.ReadString();
                        m_Grade2By = reader.ReadString();
                        m_Grade3By = reader.ReadString();
                        m_Grade4By = reader.ReadString();
                        m_GradeTime = reader.ReadDateTime();
                        m_GradesLimit = reader.ReadShort();
                        m_Age = reader.ReadInt();
                        m_IsResetting = reader.ReadBool();
                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)1); //version
            m_CraftExperience.Serialize(writer);
            writer.Write(m_Level);
            writer.Write(m_Experience);
            writer.Write((int)m_Alignement);
            writer.Write((int)m_Race);
            writer.Write(m_DestinyPoints);
            writer.Write(m_XPTicks);
            writer.Write(m_LastGrade);
            writer.Write(m_Grade1);
            writer.Write(m_Grade2);
            writer.Write(m_Grade3);
            writer.Write(m_Grade4);
            writer.Write(m_Grade1By);
            writer.Write(m_Grade2By);
            writer.Write(m_Grade3By);
            writer.Write(m_Grade4By);
            writer.Write(m_GradeTime);
            writer.Write(m_GradesLimit);
            writer.Write(m_Age);
            writer.Write(m_IsResetting);
        }

        public override string ToString()
        {
            return "(Infos d'Evolution)";
        }
        #endregion
    }

}
