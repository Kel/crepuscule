﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Engines;

namespace Server
{

    #region Enums
    /// <summary>
    /// The type of the capacity
    /// </summary>
    public enum CapacityCost {
        Primary,
        Normal,
        Penalized,
        Unavailable
    }

    /// <summary>
    /// The type of the capacity
    /// </summary>
    public enum CapacityName {
        Acrobatics,
        Alchemy,
        Assassination,
        Bardic,
        Tinkering,
        Bows,
        CloseCombat,
        DistanceCombat,
        BetterAgility,
        Wrestling,
        Tailoring,
        Hunting,
        Enchanting,
        BetterEndurance,
        Lore,
        AnimalEmpathy,
        Blacksmithing,
        Stealth,
        Herblore,
        Wizardry,
        Sorcery,
        Necromancy,
        Theology,
        Nature,
        Healing,
        LockPicking,
        Rage,
        Sabotage,
        Telepathy,
        PickPocket,
        MartialArts,
        BloodMagic
    }
    #endregion

    public static class EvolutionConfig
    {
        #region Config Constants
        static EvolutionConfig()
        {
            InitialCap.Add(CapacityCost.Primary, 30.0);
            InitialCap.Add(CapacityCost.Normal, 10.0);
            InitialCap.Add(CapacityCost.Penalized, 0.0);
            InitialCap.Add(CapacityCost.Unavailable, 0);

            MaxCap.Add(CapacityCost.Primary, 120.0);
            MaxCap.Add(CapacityCost.Normal, 100.0);
            MaxCap.Add(CapacityCost.Penalized, 80.0);
            MaxCap.Add(CapacityCost.Unavailable, 0);

            MaxCapLow.Add(CapacityCost.Primary, 100.0);
            MaxCapLow.Add(CapacityCost.Normal, 90.0);
            MaxCapLow.Add(CapacityCost.Penalized, 80.0);
            MaxCapLow.Add(CapacityCost.Unavailable, 0);

            CapsPerLevel.Add(CapacityCost.Primary, 2.0);
            CapsPerLevel.Add(CapacityCost.Normal, 2.0);
            CapsPerLevel.Add(CapacityCost.Penalized, 2.0);
            CapsPerLevel.Add(CapacityCost.Unavailable, 0.0);

            CapsPerHighLevel.Add(CapacityCost.Primary, 1.0);
            CapsPerHighLevel.Add(CapacityCost.Normal, 1.0);
            CapsPerHighLevel.Add(CapacityCost.Penalized, 1.0);
            CapsPerHighLevel.Add(CapacityCost.Unavailable, 0.0);
        }

        #region Experience Tables
        /// <summary>
        /// Experience needed in order to gain a level
        /// </summary>
        public static int[] ExperienceNeeded = new int[]{ 
            0, // Level 1
            1, // Level 2
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            11,
            13,
            15,
            17,
            19,
            21,
            23,
            25,
            27,
            29,
            31,
            33,
            35,
            37,
            39,
            41,
            43,
            45,
            47,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,
            57,
            58,
            58,
            60, // Level 41
        };

        /// <summary>
        /// Experience for each level
        /// </summary>
        public static int[] ExperienceLevel = new int[]{ 
            0, // Level 1
            1, // Level 2
            3,
            6,
            10,
            15,
            21,
            28,
            36,
            45,
            56,
            69,
            84,
            101,
            120,
            141,
            164,
            189,
            216,
            245,
            276,
            309,
            344,
            381,
            420,
            461,
            504,
            549,
            596,
            645,
            695,
            746,
            798,
            851,
            905,
            960,
            1016,
            1073,
            1131,
            1189,
            1249 // Level 41
        };
        #endregion

        /// <summary>
        /// The linear function, defining the experience needed for high levels
        /// </summary>
        public static int MaxCapacityCap = 100;

        /// <summary>
        /// Maximum 'normal' level
        /// </summary>
        public static int MaxLevel = ExperienceNeeded.Length;

        /// <summary>
        /// The number of points one gains when gaining a new level
        /// </summary>
        public static int PointsPerLevel = 9;

        /// <summary>
        /// The linear function, defining the experience needed for high levels
        /// </summary>
        public static int HighLevelExperienceNeeded = 85;

        /// <summary>
        /// The number of points one gets when gaining a new high level
        /// </summary>
        public static int PointsPerHighLevel = 4;

        /// <summary>
        /// The initial caps per cost
        /// </summary>
        public static Dictionary<CapacityCost, double> InitialCap = new Dictionary<CapacityCost, double>();

        /// <summary>
        /// The initial caps per cost
        /// </summary>
        public static Dictionary<CapacityCost, double> MaxCap = new Dictionary<CapacityCost, double>();

        /// <summary>
        /// The initial caps per cost
        /// </summary>
        public static Dictionary<CapacityCost, double> MaxCapLow = new Dictionary<CapacityCost, double>();

        /// <summary>
        /// The number of skill caps one gets when gaining a new level
        /// </summary>
        public static Dictionary<CapacityCost, double> CapsPerLevel = new Dictionary<CapacityCost, double>();

        /// <summary>
        /// The number of skill caps one gets when gaining a new level (mode high level)
        /// </summary>
        public static Dictionary<CapacityCost, double> CapsPerHighLevel = new Dictionary<CapacityCost, double>();

        #endregion

        /// <summary>
        /// Checks wether the level is in high level zone or not
        /// </summary>
        public static bool IsHighLevel(int level) 
        { 
            return level >= MaxLevel ? true : false; 
        }

        /// <summary>
        /// Gets the amount of experience needed to reach a particular level
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static int GetExperience(int level)
        {
            if (level < 1) return 0;
            if (level <= MaxLevel)
            {
                // From table
                return ExperienceLevel[level - 1];
            }
            else
            {
                // High level calculation
                return ExperienceLevel[MaxLevel - 1] + (HighLevelExperienceNeeded * (level - MaxLevel));
            }
        }

        /// <summary>
        /// Gets the level, given the experience amount
        /// </summary>
        public static int GetLevel(int experience)
        {
            //TODO: Optimize, lol
            if (experience == 0) return 1;
            for (int i = 1; i < 999; ++i)
            {
                if (GetExperience(i) > experience)
                    return i - 1;
            }
            return 1;
        }

        /// <summary>
        /// Gets the points available at a particular level
        /// </summary>
        public static int GetPoints(int level)
        {
            if (level <= MaxLevel)
            {
                return PointsPerLevel * (level - 1);
            }
            else
            {
                return (PointsPerLevel * (MaxLevel - 1)) + (PointsPerHighLevel * (level - MaxLevel));
            }
        }

        /// <summary>
        /// Gets maximum number of destiny points
        /// </summary>
        public static int GetDestinyPoints(int level)
        {
            return 1 + (int)Math.Floor((double)level / 10);
        }

        /// <summary>
        /// Sets correctly the stats to the player
        /// </summary>
        public static void SetStats(RacePlayerMobile player)
        {
            var Capacities = player.ClassInfo.Capacities;
            var RaceMod = Races.GetStats(player.EvolutionInfo.Race);

            // Initialize on race stats
            int StatCap = RaceMod.StatCap;
            int MaxDex = RaceMod.MaxDex;
            int MaxInt = RaceMod.MaxInt;
            int MaxStr = RaceMod.MaxStr;

            for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
            {
                var mod = Capacities[i].Stats;
                if (mod != null)
                {
                    if (mod.ModDex) MaxDex += Capacities[i].Value / mod.ModDexLevelInterval;
                    if (mod.ModInt) MaxInt += Capacities[i].Value / mod.ModIntLevelInterval;
                    if (mod.ModStr) MaxStr += Capacities[i].Value / mod.ModStrLevelInterval;
                }
            }

            StatCap = (MaxDex - RaceMod.MaxDex) + (MaxInt - RaceMod.MaxInt) + (MaxStr - RaceMod.MaxStr) + RaceMod.StatCap;

            // Set finals
            player.StatCap = StatCap;
            player.MaxDex = MaxDex;
            player.MaxStr = MaxStr;
            player.MaxInt = MaxInt;
        }

        /// <summary>
        /// Sets correctly all skillcaps to the player
        /// </summary>
        public static void SetSkillCaps(RacePlayerMobile player)
        {
            for (int i = 0; i < 52; ++i)
            {
                player.Skills[i].Cap = player.ClassInfo.GetSkillCapForLevel((SkillName)i, player.EvolutionInfo.Level);
            }
        }

        /// <summary>
        /// Sets correctly  to the player all skills to not be more than the caps
        /// </summary>
        public static void TrimStats(RacePlayerMobile player)
        {
            if (player.RawStr > player.MaxStr) player.RawStr = player.MaxStr;
            if (player.RawDex > player.MaxDex) player.RawDex = player.MaxDex;
            if (player.RawInt > player.MaxInt) player.RawInt = player.MaxInt;
        }

        /// <summary>
        /// Sets correctly  to the player all skills to not be more than the caps
        /// </summary>
        public static void TrimSkills(RacePlayerMobile player)
        {
            for (int i = 0; i < 52; ++i)
            {
                var cap = player.ClassInfo.GetSkillCapForLevel((SkillName)i, player.EvolutionInfo.Level);
                if (player.Skills[i].Base > cap)
                    player.Skills[i].Base = cap;
            }
        }

        /// <summary>
        /// Adds 1 Capacity point
        /// </summary>
        public static void AddCapacity(RacePlayerMobile Player, CapacityInfo Info)
        {
            if (Player.EvolutionInfo.PointsAvailable >= CapacitiesConfig.GetCostValue(Info.Cost))
            {
                Info.Value++;
                if (!Player.EvolutionInfo.IsResetting)
                {
                    EvolutionConfig.SetStats(Player);
                    EvolutionConfig.SetSkillCaps(Player);
                    EvolutionConfig.TrimSkills(Player);
                    EvolutionConfig.TrimStats(Player);
                }
            }
        }

        /// <summary>
        /// Gets the maximum number of destiny points for a player
        /// </summary>
        public static int GetDestinyPoints(RacePlayerMobile Player)
        {
            return GetDestinyPoints(Player.EvolutionInfo.Level);
        }
    }

}
