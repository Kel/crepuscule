﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Engines;

namespace Server.Gumps
{
    public class ChangeClassGump : MessageBoxGump
    {
        private const string ChangeClassDescription =
            @"Attention! En continuant, vous allez procéder à un changement de classe de votre personnage. "+
            "Ce changement de classe va ensuite réinitialiser vos points d'aptitude, les remettant à 0. Vous "+
            "allez bien entendu garder votre expérience et votre niveau, et de ce fait, vous pourriez "+
            "reinvestir tous vos points disponibles dans les aptitudes de cette nouvelle classe.";

        private ClassInfo Class;
        public ChangeClassGump(RacePlayerMobile owner, ClassInfo classInfo) : base(owner, "Changement de classe", ChangeClassDescription) 
        {
            Class = classInfo;
        }

        public override void OnYesClicked(RacePlayerMobile owner, string title, string text)
        {
            ClassConfig.ChangeClass(owner, Class);
        }

        public override void OnNoClicked(RacePlayerMobile owner, string title, string text)
        {
            
        }
    }

}
