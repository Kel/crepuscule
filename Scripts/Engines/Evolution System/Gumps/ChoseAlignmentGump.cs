using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;

namespace Server.Gumps
{
	public class ChoseAlignmentGump : Gump
	{
        public ChoseAlignmentGump()
			: base( 0, 0 )
		{
			this.Closable=false;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(61, 94, 225, 308, 9350);
			this.AddBackground(61, 71, 224, 28, 9200);
			this.AddLabel(110, 116, 669, @"Loyal Bon");
			this.AddButton(72, 114, 4014, 4015, (int)Buttons.btn1, GumpButtonType.Reply, 0);
			this.AddLabel(109, 145, 669, @"Neutre Bon");
			this.AddButton(71, 143, 4014, 4015, (int)Buttons.btn2, GumpButtonType.Reply, 0);
			this.AddLabel(109, 175, 669, @"Chaotique Bon");
			this.AddButton(71, 173, 4014, 4015, (int)Buttons.btn3, GumpButtonType.Reply, 0);
			this.AddLabel(109, 205, 5, @"Loyal Neutre");
			this.AddButton(71, 203, 4014, 4015, (int)Buttons.btn4, GumpButtonType.Reply, 0);
			this.AddLabel(109, 236, 5, @"Neutre Strict");
			this.AddButton(71, 234, 4014, 4015, (int)Buttons.btn5, GumpButtonType.Reply, 0);
			this.AddLabel(110, 267, 5, @"Chaotique Neutre");
			this.AddButton(72, 265, 4014, 4015, (int)Buttons.btn6, GumpButtonType.Reply, 0);
			this.AddLabel(110, 298, 140, @"Loyal Mauvais");
			this.AddButton(72, 296, 4014, 4015, (int)Buttons.btn7, GumpButtonType.Reply, 0);
			this.AddLabel(110, 330, 140, @"Neutre Mauvais");
			this.AddButton(72, 328, 4014, 4015, (int)Buttons.btn8, GumpButtonType.Reply, 0);
			this.AddLabel(110, 361, 140, @"Chaotique Mauvais");
			this.AddButton(72, 359, 4014, 4015, (int)Buttons.btn9, GumpButtonType.Reply, 0);
			this.AddLabel(103, 77, 0, @"CHOIX D'ALIGNEMENT");
			this.AddImage(251, 29, 10441);

		}
		
		public enum Buttons
		{
			btn1,
			btn2,
			btn3,
			btn4,
			btn5,
			btn6,
			btn7,
			btn8,
			btn9,
		}

        public override void OnResponse(NetState state, RelayInfo info)
        {
            RacePlayerMobile mobile = state.Mobile as RacePlayerMobile;

            if (info.ButtonID == 0)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Loyal_Bon;
            }
            else if (info.ButtonID == 1)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Neutre_Bon;
            }
            else if (info.ButtonID == 2)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Chaotique_Bon;
            }
            else if (info.ButtonID == 3)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Loyal_Neutre;
            }
            else if (info.ButtonID == 4)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Neutre_Strict;
            }
            else if (info.ButtonID == 5)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Chaotique_Neutre;
            }
            else if (info.ButtonID == 6)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Loyal_Mauvais;
            }
            else if (info.ButtonID == 7)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Neutre_Mauvais;
            }
            else if (info.ButtonID == 8)
            {
                mobile.EvolutionInfo.Alignement = AlignementType.Chaotique_Mauvais;
            }

            mobile.CloseGump(typeof(ChoseAlignmentGump));
            mobile.SendGump(new ChoseRaceGump(mobile));
        }

	}
}
