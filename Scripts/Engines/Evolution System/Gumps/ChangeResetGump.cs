﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Engines;

namespace Server.Gumps
{
    public class ChangeResetGump : MessageBoxGump
    {
        private const string Description = "Attention! Etes-vous sûr de vouloir confirmer? "+
            "La fiche sera changée et vos maximums des capacités (skill caps) seront ajustés en fonction de la fiche choisie. Cette opération est irréversible.";
        public ChangeResetGump(RacePlayerMobile owner)
            : base(owner, "Confirmation", Description) 
        {
            
        }

        public override void OnYesClicked(RacePlayerMobile owner, string title, string text)
        {
            EvolutionConfig.SetStats(owner);
            EvolutionConfig.SetSkillCaps(owner);
            EvolutionConfig.TrimSkills(owner);
            EvolutionConfig.TrimStats(owner);
            owner.EvolutionInfo.IsResetting = false;
            owner.Squelched = false;
            owner.Frozen = false;
            owner.CloseAllGumps();
        }

        public override void OnNoClicked(RacePlayerMobile owner, string title, string text)
        {
            
        }
    }

}
