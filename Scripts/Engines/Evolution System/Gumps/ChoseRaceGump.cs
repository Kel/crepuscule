using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;
using Server.Guilds;

namespace Server.Gumps
{
	public class ChoseRaceGump : Gump
	{
		private int gumpY = 100;
		private int bID = 1;
		private int pID = 1;
		private int pRef = 1;

		private int e = 0;

		public ChoseRaceGump( PlayerMobile mobile )
			: base( 0, 0 )
		{
            RacePlayerMobile pm = (RacePlayerMobile)mobile;

			this.Closable=false;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(144, 36, 478, 455, 9380);
			this.AddLabel(228, 80, 0, @"Elfes");
			this.AddLabel(228, 115, 0, @"Gnome");
			this.AddLabel(228, 150, 0, @"Petite Personne");
			this.AddLabel(228, 185, 0, @"Demi Elfe");
			this.AddLabel(228, 220, 0, @"Humain");
			this.AddLabel(228, 255, 0, @"Nain");
			this.AddLabel(228, 290, 0, @"Demi Orque");
			this.AddLabel(228, 325, 0, @"Elfe Noir");
			this.AddLabel(325, 41, 0, @"CHOIX DE LA RACE");

            var alignment = pm.EvolutionInfo.Alignement;
            if (alignment != AlignementType.Chaotique_Mauvais && alignment != AlignementType.Loyal_Mauvais && alignment != AlignementType.Neutre_Mauvais)
            {
                this.AddButton(184, 82, 4005, 4006, (int)Buttons.radio1, GumpButtonType.Page, 1); //elfes
            }
            if (mobile.GuildInfo.PlayerGuild != GuildType.Cercle)
            {
                if (alignment != AlignementType.Chaotique_Mauvais && alignment != AlignementType.Loyal_Mauvais && alignment != AlignementType.Neutre_Mauvais)
                {
                    this.AddButton(184, 117, 4005, 4006, (int)Buttons.radio2, GumpButtonType.Page, 2); //gnome
                }
                if (alignment != AlignementType.Loyal_Bon && alignment != AlignementType.Loyal_Mauvais && alignment != AlignementType.Loyal_Neutre)
                {
                    this.AddButton(184, 152, 4005, 4006, (int)Buttons.radio3, GumpButtonType.Page, 3); // pp
                }
            }
            this.AddButton(184, 187, 4005, 4006, (int)Buttons.radio4, GumpButtonType.Page, 4); //de
			this.AddButton(184, 222, 4005, 4006, (int)Buttons.radio5, GumpButtonType.Page, 5); // humain
			this.AddButton(184, 257, 4005, 4006, (int)Buttons.radio6, GumpButtonType.Page, 6); // nain
            if (alignment != AlignementType.Loyal_Bon && alignment != AlignementType.Loyal_Mauvais && alignment != AlignementType.Loyal_Neutre)
            {
                this.AddButton(184, 292, 4005, 4006, (int)Buttons.radio7, GumpButtonType.Page, 7); // d orque
            }
            if (alignment != AlignementType.Chaotique_Bon && alignment != AlignementType.Loyal_Bon && alignment != AlignementType.Neutre_Bon)
            {
                this.AddButton(184, 327, 4005, 4006, (int)Buttons.radio8, GumpButtonType.Page, 8); // drow
            }
			this.AddImage(576, 49, 10410);

			CreatePage( RaceType.Elfe );
			CreatePage( RaceType.Gnome );
			CreatePage( RaceType.Petite_Personne );
			CreatePage( RaceType.Demi_Elfe );
			CreatePage( RaceType.Humain );
			CreatePage( RaceType.Nain );
			CreatePage( RaceType.Demi_Orque );
			CreatePage( RaceType.Elfe_Noir );

		}
		
		public enum Buttons
		{
			bID,
			radio1,
			radio2,
			radio3,
			radio4,
			radio5,
			radio6,
			radio7,
			radio8,
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			RacePlayerMobile mobile = state.Mobile as RacePlayerMobile;
			
			if (info.ButtonID == 1) 
			{
				mobile.EvolutionInfo.Race = RaceType.Elfe;
				mobile.Hue = 2425;

			}
			else if (info.ButtonID == 2)
			{
                mobile.EvolutionInfo.Race = RaceType.Gnome;
				mobile.Hue = 1812;

			}
			else if (info.ButtonID == 3) 
			{
                mobile.EvolutionInfo.Race = RaceType.Petite_Personne;
				mobile.Hue = 33770 ; 
			}
			else if (info.ButtonID == 4)
			{
                mobile.EvolutionInfo.Race = RaceType.Demi_Elfe;
				mobile.Hue = 33770 ;

			}
			else if (info.ButtonID == 5)
			{
                mobile.EvolutionInfo.Race = RaceType.Humain;
				mobile.Hue = 33770;
			}
			else if (info.ButtonID == 6) 
			{
                mobile.EvolutionInfo.Race = RaceType.Nain;
				mobile.Hue = 1812;
			}
			else if (info.ButtonID == 7) 
			{
                mobile.EvolutionInfo.Race = RaceType.Demi_Orque;
				mobile.Hue = 2207 ;
			}
			else if (info.ButtonID == 8) 
			{
                mobile.EvolutionInfo.Race = RaceType.Elfe_Noir;
				mobile.Hue = 1102;
			}

			else World.Broadcast ( 33, true, "Error code: RaceChoixGump_OnResponse, Race not denifed! ButtonID = " + info.ButtonID );
		}

		public void CreatePage( RaceType race )
		{
			AddPage( pID );

			if ( race == RaceType.Elfe )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Elfes'</center><left><br>�galement appel�s les �tres parfaits, les elfes incarnent l'art, la beaut�, la gr�ce et la sagesse au sein des races de cette terre. Ils sont plus sveltes que les humains et de taille plus petite, ils ont les oreilles pointues et des traits fins et d�marqu�s. Les Elfes sont les �tres civilis�s les plus beaux et les plus agiles qui soient connus, aux d�triments de leur force physique qui est au-dessous de la moyenne des humains. Des elfes ont �t� aper�us de tout les m�tiers, mis � part les m�tiers exigeant une �me mal tourn�e.<br></left></font>"), false, true);
			else if ( race == RaceType.Gnome )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Gnomes'</center><left><br>Le peuple de la joie, les gnomes sont tr�s certainement les �tres les plus �tranges, ils sont plus petits encore que les petites-personnes et les nains, portant des nez rondelets et un visage semblant �ternellement joyeux. Leurs coiffures excentriques aux couleurs vari�es rappellent leur chaleur int�rieure et leur compassion. Ils sont naturellement port�s vers la magie, les pierres pr�cieuses, mais plusieurs sont aussi des filous aux bonnes intentions, s'ils sont plus intelligents et plus dext�res que la moyenne des races civilis�s, ils sont de loin dans les plus faibles en terme de force physique, �gals aux elfes.<br></left></font>"), false, true);
			else if ( race == RaceType.Petite_Personne )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Petites-Personnes'</center><left><br>Un peu plus grands que leurs deux cousins, les gnomes et les nains, les petites-personnes sont des �tres agiles et discrets, sur qui ont os� rarement se fiez, � cause de leur comportement plut�t chaotique et d�sorganis�. Port�s d�s leur plus jeune �ge soient vers la ferme, ou le vol, ils ont toujours �t� reconnus comme le peuple le plus al�atoire des terres. Ils ont tous un exc�s de poids, qui est tr�s clairement expliqu� � leur manie de manger cinq fois par jours. Lorsqu'ils ne s'assument pas comme voleurs, l� plupart des petites-personnes finissent kleptomanes. <br></left></font>"), false, true);
			else if ( race == RaceType.Demi_Elfe )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Demi-Elfes'</center><left><br>Le fruit de la passion entre un elfe et un humain, souvent l�g�rement discrimin� des deux camps, le demi-elfe finit l� plupart du temps par trouv� place au sein d'une des deux communaut�, la plus souvent humaine. Ils ont l'apparence d'un humain de petite taille et mince. Combinant la sagesse et la beaut� des elfes ainsi que la d�termination et le g�nie des humains, le demi-elfe est une caract�ristique forte, et ses habilet�s physiques et mentales sont tr�s �quilibr�es, plusieurs demi-elfes se font pass� pour des humains simplement en cachant leurs oreilles et en laissant pousser un peu leur barbe.<br></left></font>"), false, true);
			else if ( race == RaceType.Humain )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Humains'</center><left><br>Le peuple le plus commun � travers tout Syst�ria et au-del�, les Humains. Leur courage et leur d�termination sont une de leur principale caract�ristique. Leur soci�t� est �galement tr�s chaotique, certains sont bienveillants, d'autres malveillants, et d'autres ne prennent simplement pas parti. C'est le peuple qui � travers le monde est le plus libre de ses actes, et qui poss�de un physique et un mental des plus �quilibr�s. Un humain peu exercer n'importe quelle profession, l�gale ou ill�gale, bonne ou mauvaise.<br></left></font>"), false, true);
			else if ( race == RaceType.Nain )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Nains'</center><left><br>Le peuple le plus travaillant, les nains sont des artisans de sang, et de fiers guerriers. On dit que partout ou il y � un commerce, il y � un nain, et ce proverbe n'est pas qu'une simple rumeur. Plus grand que les gnomes mais plus petits que les petites-personnes, les nains portent de longues barbes qu'ils consid�rent comme la fiert� de leur �tre et de leur clan. Ils ont, comme les petite-personnes, une tendance � l'embonpoint, mais semble l� contr�l�e beaucoup plus. Toujours volontaires � une beuverie en trop, les nains savent �trangement �quilibrer plaisir et travail.<br></left></font>"), false, true);
			else if ( race == RaceType.Demi_Orque )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Demi-Orques'</center><left><br>Le plus souvent r�sultat d'un viol atroce, un demi-orque est le croisement d'un orque et d'une humaine. Ces �tres, consid�r�s comme civilis�s, sont brutaux et impulsifs. Ils sont bien moins intelligents que la moyenne des humains, mais r�ussissent � s'exprimer et � r�fl�chir de mani�re propre compar�e � leurs anc�tres orques. Moins soign�s que les humains, ils sont plus grands, plus muscl�s, ont de petites canines avanc�es et un syst�me pileux �galement plus d�velopp�. Gr�ce � leur tendance � �tre discrimin� un peu partout sur Syst�ria, ils sont devenus des �l�ments changeants, qui s'adaptent l� ou l�on veut bien d'eux. Ces derniers ne reste donc g�n�ralement gu�re longtemps au m�me endroit.<br></left></font>"), false, true);
			else if ( race == RaceType.Elfe_Noir )
                AddHtml(325, 79, 266, 371, String.Format("<basefont color=54><center>'Elfes Noirs'</center><left><br>Compl�tement l'antipode de leurs cousins de la surface, les elfes noirs sont des �tres sadiques et cruels, qui n'h�siteraient pas � tuer leur meilleur ami pour un pot de vin. Ils ont une peau noire d'�b�ne et des cheveux blancs comme neiges tournant parfois vers le mauve fonc�. Bien qu'ils soient, comme leurs cousins des for�ts, d'une beaut� sup�rieure, d'une taille et d'un poids �quivalent ainsi que d'une gr�ce f�line, ce sont principalement leurs m�ninges qui fonctionnent � une vitesse ahurissante. Tous les elfes noirs ont un affinit� avec la magie, peu importe le m�tier ou l��ge.<br></left></font>"), false, true);
			else if ( race == RaceType.Tiefling )
				AddHtml(  325, 79, 266, 371, String.Format( "<basefont color=54><center>'Tieflings'</center><left><br><br></left></font>" ), false, true );
			
			AddButton( 211, 427, 247, 248, bID, GumpButtonType.Reply, 0 );
			//this.AddButton(211, 427, 247, 248, (int)Buttons.Button1, GumpButtonType.Reply, 0);
			++bID;
			++pID;
		}

	}
}
