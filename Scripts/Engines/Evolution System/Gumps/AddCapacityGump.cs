﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Engines;

namespace Server.Gumps
{
    public class AddCapacityGump : MessageBoxGump
    {
        RacePlayerMobile Player;
        private CapacityInfo Info;

        public AddCapacityGump(RacePlayerMobile owner, string title, string description, CapacityInfo info)
            : base(owner, title, description) 
        {
            Player = owner;
            Info = info;
        }

        public override void OnYesClicked(RacePlayerMobile owner, string title, string text)
        {
            EvolutionConfig.AddCapacity(Player, Info);

            owner.CloseGump(typeof(CharacterSheetGump));
            owner.SendGump(new CharacterSheetGump(owner, Player.EvolutionInfo.IsResetting));
        }

        public override void OnNoClicked(RacePlayerMobile owner, string title, string text)
        {
            
        }
    }

}
