﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Engines;

namespace Server.Gumps
{
    public class NotifyResetGump : MessageBoxGump
    {
        private const string Description = "Attention! La fiche des points d'aptitude de votre personnage a été réinitialisée." +
            "Afin de continuer le jeu normalement, vous devez réinvestir vos points en point d'aptitudes via l'interface de fiche de " +
            "personnage.";
        public NotifyResetGump(RacePlayerMobile owner)
            : base(owner, "Réinitialisation", Description, false, true) 
        {

        }

        public override void OnYesClicked(RacePlayerMobile owner, string title, string text)
        {
        }

        public override void OnNoClicked(RacePlayerMobile owner, string title, string text)
        {
            
        }
    }

}
