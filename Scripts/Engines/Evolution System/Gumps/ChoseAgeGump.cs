using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;

namespace Server.Gumps
{
	public class ChoseAgeGump : Gump
	{
		public ChoseAgeGump()
			: base( 0, 0 )
		{
			this.Closable=false;
			this.Disposable=true;
			this.Dragable=true;
			this.Resizable=false;
			this.AddPage(0);
			this.AddBackground(74, 74, 254, 102, 9300);
			this.AddBackground(73, 47, 257, 31, 9200);
			this.AddLabel(90, 54, 0, @"Choisissez l'�ge de votre personnage");
			this.AddTextEntry(99, 102, 203, 20, 0, (int)Buttons.AgeTxt, @"20");
			this.AddButton(167, 140, 247, 248, (int)Buttons.Button2, GumpButtonType.Reply, 0);

		}
		
		public enum Buttons
		{
			AgeTxt,
			Button2,
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			RacePlayerMobile mobile = state.Mobile as RacePlayerMobile;
			int button = info.ButtonID;


			switch(button) 
			{
				case 0:
	
				break;

				case 1:
					TextRelay relay = info.GetTextEntry( 0 );
					int newText= -1;

					try
					{
						newText = Utility.ToInt32( relay.Text );
					} 
					catch{ 
						mobile.SendMessage("Veuillez entrer un �ge entre 1 et 300");
						mobile.CloseGump( typeof( ChoseAgeGump ) );
						mobile.SendGump ( new ChoseAgeGump () );
					}

					string age_introduit = ( relay == null ? null : relay.Text.Trim() );

					if ( age_introduit.Length > 3 || age_introduit.Length == 0 )
					{
						mobile.SendMessage("Veuillez entrer un �ge entre 1 et 300");
						mobile.CloseGump( typeof( ChoseAgeGump ) );
						mobile.SendGump ( new ChoseAgeGump () );
					}
					else
					{
						if(newText > 0 && newText <= 300)
						{
							mobile.EvolutionInfo.Age = (short)newText;
                            CityInfo city = new CityInfo( "Cr�ation", "Cr�ation", 2381, 2172, 0 );
                            
                            mobile.MoveToWorld(city.Location, Map.Felucca);
						}
						else
						{
							mobile.SendMessage("Veuillez entrer un �ge entre 1 et 300");
							mobile.CloseGump( typeof( ChoseAgeGump ) );
							mobile.SendGump ( new ChoseAgeGump () );
						}
					}
				
					break;
			
			}



		}


	}
}
