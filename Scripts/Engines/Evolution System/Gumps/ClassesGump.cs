﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines;

namespace Server.Gumps
{
    public class ClassesGump : ActionListGump
    {
        public ClassesGump(Mobile owner, List<GumpAction> list, int page) 
            : base(owner,  list, page){}

        protected override void OnPageTurn(Mobile from, Mobile owner, List<GumpAction> list, int page)
        {
            from.CloseGump(typeof(ClassesGump));
            from.SendGump(new ClassesGump(owner, list, page));
        }

        protected override void OnActionClicked(Mobile from, GumpAction clicked)
        {
            from.CloseGump(typeof(APropertiesGump));
            from.SendGump(new APropertiesGump(from, clicked.Tag as ClassInfo));
            
        }
    }
}
