using System;
using Server;
using Server.Mobiles;
using Server.Gumps;
using Server.Network;
using Server.Engines;
using System.Collections.Generic;
using Server.Guilds;

namespace Server.Gumps
{

    public class CharacterSheetGump : Gump
    {
        RacePlayerMobile Player;
        List<CapacityInfo> Capacities;
        bool MigrationMode = false;

        public CharacterSheetGump(RacePlayerMobile mobile, bool migrationMode)
            : base(PropsConfig.GumpOffsetX, PropsConfig.GumpOffsetY)
        {
            Player = mobile;
            MigrationMode = migrationMode;


            this.Closable = !MigrationMode;
            this.Disposable = !MigrationMode;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(240, 4, 391, 437, 3500);
            //this.AddBackground(3, 299, 245, 142, 3500);
            this.AddBackground(3, 4, 245, 437, 3500);

            // Titles
            this.AddLabel(106, 11, 1047, @"Fiche");
            //this.AddLabel(106, 311, 1047, @"Informations");
            this.AddLabel(398, 11, 1047, @"Capacités");

            var labels = new List<string>()
            {
                "Nom:",
                "Niveau:",
                "Expérience:",
                "Classe:",
                "Race:",
                "Guilde:",
                "Alignement:",
                "Genre:",
                "Pts. Destin:",
                "Age:",
                "Age Perçu:",
                "Faim:",
                "Soif:",
                "Dépendance:" ,
                "",
                "Points:",
                "Total Pts.:",
            };
            var infos = new List<string>()
            {
                mobile.UseIdentity == 0 ? mobile.PlayerName : mobile.SecondIdentity,
                mobile.EvolutionInfo.Level.ToString(),
                String.Format("{0}/{1}", mobile.EvolutionInfo.Experience, mobile.EvolutionInfo.NextLevelExperience),
                mobile.ClassInfo.ClassInfoLink == 0 ? "Sans-Classe" : mobile.ClassInfo.Name,
                Races.GetRaceName(mobile, true),
                mobile.Guild == null ? "Indépendant" : mobile.Guild.Name,
                Races.GetAlignementName(mobile.EvolutionInfo.Alignement),
                mobile.Female ? "Féminin" : "Masculin",
                mobile.EvolutionInfo.DestinyPoints.ToString(),
                String.Format("{0} Ans", mobile.EvolutionInfo.Age.ToString()),
                FormatHelper.GetAgeDescription(mobile.EvolutionInfo.Race, mobile.Female,  mobile.EvolutionInfo.Age),
                String.Format("{0}/20",mobile.Hunger),
                String.Format("{0}/20",mobile.Thirst),
                mobile.IsAddicted == true ? "Existante" : "Aucune",
                "",
                mobile.EvolutionInfo.PointsAvailable.ToString(),
                mobile.EvolutionInfo.PointsTotal.ToString()
            };

            var step = 23 - (MigrationMode ? 2 : 0);
            if (labels.Count == infos.Count)
            {
                for (int i = 0; i < labels.Count; ++i)
                {
                    var label = labels[i];
                    var text = infos[i];

                    this.AddLabel(25, 35 + (i * step), 0, label);
                    this.AddLabel(100, 35 + (i * step), 0, text);
                }
            }

            if (MigrationMode) AddButton(25, 35 + (labels.Count * step), 4023, 4024, 999, GumpButtonType.Reply, 999);

            RenderCapacities();
        }

        private void RenderCapacities()
        {
            Capacities = Player.ClassInfo.Capacities.ToOrderedList();
            var lines = Capacities.Count / 2; if (lines * 2 < Capacities.Count) lines++;
            var step = 24;


            // 1st Column
            for (int i = 0; i < lines; ++i)
            {
                if (Capacities.Count > i)
                    RenderCapacity(292, 36 + (step * i), Capacities[i], i);
            }
            // 2nd Column
            for (int i = lines; i < lines * 2; ++i)
            {
                if (Capacities.Count > i)
                    RenderCapacity(480, 36 + (step * (i - lines)), Capacities[i], i);
            }
        }

        private void RenderCapacity(int x, int y, CapacityInfo capacity, int id)
        {
            this.AddLabel(x, y, capacity.Hue, String.Format("{0}", capacity.Value));
            this.AddLabel(x + 30, y, capacity.Hue, String.Format("{0}", capacity.Name));

            if (Player.EvolutionInfo.PointsAvailable >= CapacitiesConfig.GetCostValue(capacity.Cost) &&
                capacity.Value < EvolutionConfig.MaxCapacityCap)
            {
                //this.AddButton(x - 30, y, 30008, 30009, id + 1, GumpButtonType.Reply, id + 1);
                this.AddButton(x - 30, y, 55, 55, id + 1, GumpButtonType.Reply, id + 1);
            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            RacePlayerMobile mobile = state.Mobile as RacePlayerMobile;

            if (info.ButtonID >= 1 && info.ButtonID <= Capacities.Count)
            {
                var Capacity = Capacities[info.ButtonID - 1];
                var Modifiers = new List<SkillCapModifier>();

                if (Capacity.Skill1 != null) Modifiers.Add(Capacity.Skill1);
                if (Capacity.Skill2 != null) Modifiers.Add(Capacity.Skill2);
                if (Capacity.Skill3 != null) Modifiers.Add(Capacity.Skill3);
                if (Capacity.Skill4 != null) Modifiers.Add(Capacity.Skill4);
                if (Capacity.Skill5 != null) Modifiers.Add(Capacity.Skill5);

                var Description = "<br />Description de la capacité:";
                foreach (var modifier in Modifiers)
                    Description += String.Format("<br /> +{0} {1}", modifier.CapPerLevel,  CapacitiesConfig.GetName(modifier.Skill));
                Description += "<br />" + Capacity.Description;

                if (mobile.GuildInfo.IsYoung)
                {
                    mobile.CloseGump(typeof(AddCapacityGump));
                    mobile.SendGump(new CharacterSheetGump(mobile, MigrationMode));
                    mobile.SendGump(new AddCapacityGump(mobile, Capacity.Name,
                        String.Format("Voulez-vous augmenter votre capacité {0} d'un point? Cette opération est irréversible (mis à part le changement de classe) et vous coutera {1} points.{2}",
                        Capacity.Name, CapacitiesConfig.GetCostValue(Capacity.Cost), Description),
                        Capacity));
                }
                else
                {
                    EvolutionConfig.AddCapacity(Player, Capacity);
                    mobile.SendGump(new CharacterSheetGump(mobile, MigrationMode));
                }
            }
            else if (info.ButtonID == 999)
            {
                // Confirm
                mobile.SendGump(new CharacterSheetGump(mobile, MigrationMode));
                mobile.SendGump(new ChangeResetGump(mobile));
            }


        }
    }
}