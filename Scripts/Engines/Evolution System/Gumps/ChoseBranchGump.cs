﻿using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;
using Server.Engines;

namespace Server.Gumps
{
    public class ChoseBranchGump : Gump
    {
        public ChoseBranchGump()
            : base(0, 0)
        {
            this.Closable = false;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(61, 94, 225, 308, 9350);
            this.AddBackground(61, 71, 224, 28, 9200);
            
            this.AddButton(71, 114, 4014, 4015, (int)ClassBranch.Warrior, GumpButtonType.Reply, 0);
            this.AddLabel(109, 114, 669, @"Branche Guerriere");

            this.AddButton(71, 143, 4014, 4015, (int)ClassBranch.Mage, GumpButtonType.Reply, 0);
            this.AddLabel(109, 143, 669, @"Branche Mage");

            this.AddButton(71, 173, 4014, 4015, (int)ClassBranch.Rogue, GumpButtonType.Reply, 0);
            this.AddLabel(109, 173, 669, @"Branche Rogue");

            this.AddButton(71, 203, 4014, 4015, (int)ClassBranch.Spiritual, GumpButtonType.Reply, 0);
            this.AddLabel(109, 203, 669, @"Branche Spirituelle");

            this.AddButton(71, 234, 4014, 4015, (int)ClassBranch.Craftsman, GumpButtonType.Reply, 0);
            this.AddLabel(110, 234, 669, @"Branche Artisanale");

            this.AddLabel(103, 77, 0, @"CHOIX D'ALIGNEMENT");
            this.AddImage(251, 29, 10441);

        }


        public override void OnResponse(NetState state, RelayInfo info)
        {
            RacePlayerMobile mobile = state.Mobile as RacePlayerMobile;
            mobile.ClassInfo.Branch = (ClassBranch)info.ButtonID;
            mobile.GuildInfo.PlayerGuild = Guilds.GuildType.None;

            mobile.CloseGump(typeof(ChoseBranchGump));
            mobile.SendGump(new ChoseAgeGump());
        }

    }
}
