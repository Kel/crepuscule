using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server.Scripts.Commands
{

	public class XPCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("xp", AccessLevel.Player, new CommandEventHandler(XPCommand_OnCommand));
		}

		[Description( "Affiche l'exp�rience de votre personnage")]
        public static void XPCommand_OnCommand(CommandEventArgs e)
		{
            var from = e.Mobile as RacePlayerMobile;
            from.SendMessage("Vous avez atteint le niveau " + from.EvolutionInfo.Level);
            from.SendMessage("Vous poss�dez " + from.EvolutionInfo.Experience + "/" + from.EvolutionInfo.NextLevelExperience + " points d'exp�rience");
            from.SendMessage("Il vous faut " + from.EvolutionInfo.ExperienceNeeded + " points d'exp�rience pour le niveau suivant!");
		}
	}
}
