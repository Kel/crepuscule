using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;

namespace Server.Scripts.Commands
{

	public class CharacterSheetCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("fiche", AccessLevel.Player, new CommandEventHandler(CharacterSheetCommand_OnCommand));
		}

		[Description( "Affiche la fiche de joueur")]
        public static void CharacterSheetCommand_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                var from = e.Mobile as RacePlayerMobile;
                if (!from.EvolutionInfo.IsResetting)
                {
                    // Show gump
                    from.CloseGump(typeof(CharacterSheetGump));
                    from.SendGump(new CharacterSheetGump(from, false));
                }
            }
		}
	}
}
