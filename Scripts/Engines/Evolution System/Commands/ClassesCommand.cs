using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;

namespace Server.Scripts.Commands
{

	public class ClassesCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("classes", AccessLevel.Seer, new CommandEventHandler(ClassesCommand_OnCommand));
		}

		[Description( "Permet de voir les classes disponibles en jeu et les �diter")]
        public static void ClassesCommand_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                var from = e.Mobile as RacePlayerMobile;
                ShowClassesGump(from);
            }
		}

        public static void ShowClassesGump(Mobile mobile)
        {
            var from = mobile as RacePlayerMobile;
            var actions = new List<GumpAction>();

            int i = 0;
            foreach (var classInfo in ClassInfo.Classes)
            {
                actions.Add(new GumpAction(String.Format("{0} = {1}", classInfo.Number, classInfo.Name), i, classInfo));
                ++i;
            }

            // Show gump
            from.CloseGump(typeof(ClassesGump));
            from.SendGump(new ClassesGump(from, actions, 0));
        }
	}
}
