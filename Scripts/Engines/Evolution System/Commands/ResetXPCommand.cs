using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using Server.Misc;

namespace Server.Scripts.Commands
{

    public class ResetXPCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("ResetXP", AccessLevel.Administrator, new CommandEventHandler(ResetXPCommand_OnCommand));
		}

		[Description( "Reinitialise la limite des cotes xp")]
        public static void ResetXPCommand_OnCommand(CommandEventArgs e)
		{
            GradeLimitTimer.SetLimit();
		}
	}
}
