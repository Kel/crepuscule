//*******************************//
// Fichier : Cotecommand.cs      //
// Auteur  : Concepteur Kel      //
// Version : 1.0                 //
//*******************************//

using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Gumps;
using Server.Targeting;


namespace Server.Scripts.Commands
{

	public class GradeCommand
	{	

		public static void Initialize()
		{
            Server.Commands.Register("cote", AccessLevel.GameMaster, new CommandEventHandler(GradeCommand_OnCommand));
		}

	  	[Usage( "<nombre>" )] 
	    [Description( "Met une cote � un joueur")]
        public static void GradeCommand_OnCommand(CommandEventArgs e)
		{
            e.Mobile.Target = new GradeCommandTarget((int)e.GetDouble(0));
		} 

	}

	public class GradeCommandTarget : Target
	{
		private double m_Value;
        public GradeCommandTarget(double value): base(-1, false, TargetFlags.None)
		{
			m_Value = value;
		}

		protected override void OnTarget( Mobile from, object targeted )
		{
			if ( targeted is RacePlayerMobile )
			{
				RacePlayerMobile targ = (RacePlayerMobile)targeted;
                from.CloseGump(typeof(CoteGump));
                from.SendGump(new CoteGump(targ));
			}
		}

        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;
                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }
	}



}
