using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;
using Server.Accounting;
using System.Linq;

namespace Server
{
    public enum ClasseType
    {
        None = 0,
        Guerrier = 1,
        Mage,
        Bricoleur,
        Sorcier,
        Paladin,
        Necromancien,
        Druide,
        Alchimiste,
        Archer,
        Barde,
        Moine,
        Voleur,
        Sage,
        Rodeur,
        Enchanteur,
        Forgeron,
        Clerc,
        Couturier,
        Assasin,
        Barbare,
        Vampire
    }

	public class MigrateEvoCommand
	{	
		public static void Initialize()
		{
            //Server.Commands.Register("MigrateEvo", AccessLevel.Administrator, new CommandEventHandler(MigrateEvoCommand_OnCommand));
		}

		[Description( "Effectue une migration de syst�me d'�volution vers Cr�p2")]
        public static void MigrateEvoCommand_OnCommand(CommandEventArgs e)
		{
            World.Broadcast(15, true, "ATTENTION : CHANGEMENT DE SYSTEME D'EVOLUTION ... ");
            CreateClasses();

            // First, get all players
            var players = new List<RacePlayerMobile>();
            foreach (Mobile m in World.Mobiles.Values)
            {
                if (m is RacePlayerMobile && m != null)
                    players.Add(m as RacePlayerMobile);
            }

            // Now, do the migration
            var CharsNegative = 0;
            var ActiveNegative = 0;
            var TotalChars = 0;
            var TotalActive = 0;
            var PerClassUsage = new Dictionary<int, List<int>>();
            var NegativeClass = new List<int>();

            foreach (var c in ClassInfo.Classes)
            {
                var list = new List<int>();
                for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
                    list.Add(0);
                
                PerClassUsage.Add(c.Number, list);
                NegativeClass.Add(0);
            }

            foreach (RacePlayerMobile m in players)
            {
                var isActive = DateTime.Now - (m.Account as Account).LastLogin < TimeSpan.FromDays(90);
                m.MigrateEvolutionSystem();
                if (m.AccessLevel == AccessLevel.Player)
                {
                    // Get the most used
                    var usageStats = PerClassUsage[m.ClassInfo.ClassInfoLink];
                    for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
                    {
                        if (m.Capacities[i].Cost != CapacityCost.Primary && m.Capacities[i].Cost != CapacityCost.Normal)
                            usageStats[i] += m.Capacities[i].Value;
                    }


                    TotalChars++;
                    if (isActive) TotalActive++;
                    if (m.EvolutionInfo.PointsAvailable < 0 )
                    {
                        if(m.EvolutionInfo.Level > 10)
                            World.Broadcast(7, false, m.PlayerName + " est en n�gatif! (" + m.Account.ToString() + ")");
                        CharsNegative++;
                        if (isActive) ActiveNegative++;
                        NegativeClass[m.ClassInfo.ClassInfoLink - 1]++;
                        ClassConfig.ShowResetGump(m);
                    }
                   
                }
            }

            World.Broadcast(5, false, ActiveNegative + " personnages actifs en n�gatif");
            World.Broadcast(5, false, TotalActive + " personnages actifs total");
            World.Broadcast(7, false, CharsNegative+  " personnages en n�gatif");
            World.Broadcast(7, false, TotalChars - CharsNegative + " personnages okay");
            World.Broadcast(7, false, TotalChars + " personnages total");
            /*foreach (var c in ClassInfo.Classes)
            {
                World.Broadcast(7, false, c.Name + ": "+ NegativeClass[c.Number -1]);
            }*/

            // Show stats
            foreach (var c in ClassInfo.Classes)
            {
                var usage = PerClassUsage[c.Number];
                var max = usage.Max();

                for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
                {
                    if (usage[i] == max)
                    {
                        //World.Broadcast(7, false, c.Name + ": " + c.Capacities[i].Name);
                    }
                }
            }
		}

        public static void CreateClasses()
        {
            ClassInfo Index;

            //ClasseType.Guerrier,
            Index = CreateClass(1, ClassBranch.Warrior, "Guerrier");
            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Blacksmithing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Theology].Cost = CapacityCost.Normal;

            //ClasseType.Mage,
            Index = CreateClass(2, ClassBranch.Mage, "Magicien");
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Necromancy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sorcery].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Nature].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Normal;

            //ClasseType.Bricoleur,
            Index = CreateClass(3, ClassBranch.Craftsman, "Bricoleur");
            Index.Capacities[CapacityName.Bows].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Tinkering].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Tailoring].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.LockPicking].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Blacksmithing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sabotage].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Hunting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Bardic].Cost = CapacityCost.Normal;

            //ClasseType.Sorcier,
            Index = CreateClass(4, ClassBranch.Mage, "Sorcier");
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Sorcery].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Necromancy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Normal;

            //ClasseType.Paladin,
            Index = CreateClass(5, ClassBranch.Warrior, "Paladin");
            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Theology].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;

            //ClasseType.Necromancien,
            Index = CreateClass(6, ClassBranch.Mage, "N�cromancien");
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Necromancy].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Stealth].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sorcery].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Hunting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Normal;

            //ClasseType.Druide,
            Index = CreateClass(7, ClassBranch.Mage, "Druide");
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Nature].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Bardic].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Tailoring].Cost = CapacityCost.Normal;

            //ClasseType.Alchimiste,
            Index = CreateClass(8, ClassBranch.Craftsman, "Alchimiste");
            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Tinkering].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sorcery].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Necromancy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Nature].Cost = CapacityCost.Normal;

            //ClasseType.Archer,
            Index = CreateClass(9, ClassBranch.Warrior, "Archer");
            Index.Capacities[CapacityName.Bows].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Normal;

            //ClasseType.Barde,
            Index = CreateClass(10, ClassBranch.Rogue, "Barde");
            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Bardic].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Stealth].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.PickPocket].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Normal;

            //ClasseType.Moine,
            Index = CreateClass(11, ClassBranch.Spiritual, "Moine");
            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.MartialArts].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Theology].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Normal;

            //ClasseType.Voleur,
            Index = CreateClass(12, ClassBranch.Rogue, "Voleur");
            Index.Capacities[CapacityName.LockPicking].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.PickPocket].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Stealth].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sabotage].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Hunting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.MartialArts].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;

            //ClasseType.Sage,
            Index = CreateClass(13, ClassBranch.Spiritual, "Sage");
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Theology].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sorcery].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;

            //ClasseType.Rodeur,
            Index = CreateClass(14, ClassBranch.Rogue, "Rodeur");
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Stealth].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Bows].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.LockPicking].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sabotage].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Nature].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Hunting].Cost = CapacityCost.Normal;

            //ClasseType.Enchanteur,
            Index = CreateClass(15, ClassBranch.Craftsman, "Enchanteur");
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Wizardry].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Sorcery].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Telepathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Bardic].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;

            //ClasseType.Forgeron,
            Index = CreateClass(16, ClassBranch.Craftsman, "Forgeron");
            Index.Capacities[CapacityName.Tinkering].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Blacksmithing].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.LockPicking].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Theology].Cost = CapacityCost.Normal;

            //ClasseType.Clerc,
            Index = CreateClass(17, ClassBranch.Spiritual, "Pr�tre");
            Index.Capacities[CapacityName.Theology].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.AnimalEmpathy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Lore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Tinkering].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Necromancy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Enchanting].Cost = CapacityCost.Normal;

            //ClasseType.Couturier,
            Index = CreateClass(18, ClassBranch.Craftsman, "Couturier");
            Index.Capacities[CapacityName.Tailoring].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Stealth].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Alchemy].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Bows].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Tinkering].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Blacksmithing].Cost = CapacityCost.Normal;

            //ClasseType.Assasin,
            Index = CreateClass(19, ClassBranch.Rogue, "Assassin");
            Index.Capacities[CapacityName.Assassination].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Stealth].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.Acrobatics].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.MartialArts].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.LockPicking].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Bows].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Hunting].Cost = CapacityCost.Normal;

            //ClasseType.Barbare
            Index = CreateClass(20, ClassBranch.Warrior, "Barbare");
            Index.Capacities[CapacityName.BetterEndurance].Cost = CapacityCost.Primary;
            Index.Capacities[CapacityName.Rage].Cost = CapacityCost.Primary;

            Index.Capacities[CapacityName.BetterAgility].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.DistanceCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.CloseCombat].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Tinkering].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Wrestling].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Healing].Cost = CapacityCost.Normal;
            Index.Capacities[CapacityName.Herblore].Cost = CapacityCost.Normal;
        }

        private static ClassInfo CreateClass(int number, ClassBranch branch, string name)
        {
            World.Broadcast(7, false, "Cr�ation de classe : " + name);
            var info = new ClassInfo(number);
            info.Name = name;
            ClassInfo.Classes.Add(info);
            for (int i = 0; i < CapacityInfo.CapacitiesCount; i++)
            {
                info.Capacities[i].Cost = CapacityCost.Penalized;
            }
            return info;
        }

	}
}
