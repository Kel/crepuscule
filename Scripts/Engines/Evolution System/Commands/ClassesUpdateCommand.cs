using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;
using Server.Accounting;
using System.Linq;

namespace Server
{
    public class ClassesUpdateCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("ClassesUpdate", AccessLevel.Administrator, new CommandEventHandler(ClassesUpdate_OnCommand));
		}

		[Description( "Effectue une migration de syst�me d'�volution vers Cr�p2")]
        private static void ClassesUpdate_OnCommand(CommandEventArgs e)
		{
            UpdateClasses();
		}

        public static void UpdateClasses()
        {
            WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, "Mise � jour des classes...");
            // First, get all players
            var players = new List<RacePlayerMobile>();
            foreach (Mobile m in World.Mobiles.Values)
            {
                if (m is RacePlayerMobile && m != null)
                    players.Add(m as RacePlayerMobile);
            }

            foreach (RacePlayerMobile m in players)
            {
                var isActive = DateTime.Now - (m.Account as Account).LastLogin < TimeSpan.FromDays(90);
                if (m.ClassInfo.LinkedClass == null)
                {
                    foreach (var c in ClassInfo.Classes)
                    {
                        if (m.ClassInfo.Name.ToLowerInvariant() == c.Name.ToLowerInvariant())
                            m.ClassInfo.ClassInfoLink = c.Number;
                    }
                }

                ClassConfig.MigrateClass(m, m.ClassInfo.LinkedClass);
                if (m.AccessLevel == AccessLevel.Player)
                {
                    if (m.EvolutionInfo.PointsAvailable < 0)
                    {
                        ClassConfig.ShowResetGump(m);
                    }
                }
            }
        }


	}
}
