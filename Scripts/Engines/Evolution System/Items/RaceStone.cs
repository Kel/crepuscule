using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Targeting;
using System.Collections;
using Server.Scripts.Commands;
using Server.Prompts;
using Server.ContextMenus;
using Server.Multis;
using Server.Multis.Deeds;
using Server.Regions;
using Server.Guilds;

namespace Server.Items
{
	
		public class RaceStone: CrepusculeItem
		{
			[Constructable] 
			public RaceStone() : base( 0xED4 ) 
			{ 
				Movable = false; 
				Hue = 33;
				Name = "Cr�ation de Personnage";
			} 

			public override void OnDoubleClick( Mobile from ) 
			{
				if ( from is RacePlayerMobile )
				{
					RacePlayerMobile mobile = from as RacePlayerMobile;
					
                    mobile.GuildInfo.PlayerGuild = GuildType.None; 
					mobile.SendGump(new SecondIdentityGump(mobile));
					

						//mobile.SendGump(new ClasseChoixGump( mobile ) );

				//goto 2196 2065 0
				}
				else from.SendMessage(33, "D'abord faut virer tous les perso.");

				
			} 

			public RaceStone( Serial serial ) : base( serial ) 
			{ 
			} 

			public override void Serialize( GenericWriter writer ) 
			{ 
				base.Serialize( writer ); 
				writer.Write( (int) 0 ); // version 
			} 

			public override void Deserialize( GenericReader reader ) 
			{ 
				base.Deserialize( reader ); 
				int version = reader.ReadInt(); 
			} 
		}

	
		
}
