﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Scripts.Commands;
using System.Linq;
using System.IO;
using Server.Guilds;
using Server.Accounting;

namespace Server.Items
{

    public class ClassBook : CrepusculeItem
    {
        private int m_ClassNumber = 0;
       
        [CommandProperty(AccessLevel.GameMaster)]
        public ClassInfo Class
        {
            get 
            {
                foreach (var classInfo in ClassInfo.Classes)
                {
                    if (classInfo.Number == m_ClassNumber)
                        return classInfo;
                }
                return null;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ClassNumber
        {
            get { return m_ClassNumber; }
            set 
            {
                
                if (value <= 0)
                    return;
                m_ClassNumber = value;
                ChangeName();
                foreach (var classInfo in ClassInfo.Classes)
                {
                    if (classInfo.Number == m_ClassNumber)
                        return;
                }    

                // New class should be created
                ClassInfo.Classes.Add(new ClassInfo(value));
            }
        }


        private void ChangeName()
        {
            Name = Class == null ? "Livre de classe (Non-Configuré)" : String.Format("Livre de classe ({0})", Class.Name);
        }


        [Constructable]
        public ClassBook() : base(4029)
        {
            this.Movable = false;
            ChangeName();
        }


        public ClassBook(Serial serial) : base(serial) { }

        public override void OnDoubleClick(Mobile from)
        {
            try
            {
                ChangeName();
                if (from.InRange(this.Location, 2) && Class != null && from is RacePlayerMobile)
                {
                    if (ClassConfig.CanChangeClass(from as RacePlayerMobile, Class))
                    {
                        base.OnDoubleClick(from);

                        from.CloseAllGumps();


                        var text = new List<string>();
                        var hues = new List<int>();
                        var list = Class.Capacities.ToOrderedList();
                        foreach (var capacity in list)
                        {
                            text.Add(String.Format("{0}, {1}",capacity.Name, CapacitiesConfig.GetCostDescription(capacity.Cost)));
                            hues.Add(capacity.Hue);
                        }

                        from.SendGump(new StringListGump(Class.Name, 200, text, hues));
                        from.SendGump(new ChangeClassGump(from as RacePlayerMobile, Class));
                    }
                }
                else
                {
                    from.SendMessage("Vous êtes trop loin");
                }
            }
            catch (Exception ex)
            {
                from.SendMessage("Erreur : " + ex.Message);
            }
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            m_ClassNumber = reader.ReadInt();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
            writer.Write(m_ClassNumber);
        }



    }
    
}
