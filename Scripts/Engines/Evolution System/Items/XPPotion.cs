﻿using System;
using Server;
using Server.Mobiles;
using Server.Accounting;

namespace Server.Items
{
    public class XPPotion : BasePotion
    {
        Mobile Player;

        [Constructable]
        public XPPotion(Mobile from, Mobile player)
            : base(0xF08, PotionEffect.Refresh)
        {
            Name = "Une fiole d'expérience";
            Player = player;
	    LootType = LootType.Blessed;
            if (player is RacePlayerMobile && from is RacePlayerMobile)
            {
                RacePlayerMobile rPlayer = player as RacePlayerMobile;
                RacePlayerMobile rFrom = from as RacePlayerMobile;
                try
                {
                    string TopicName = rPlayer.PlayerName + " - Recompense XP";
                    string Text= String.Format("{0} ({1}) a été recompensé par {2} ({3}) via une fiole d'expérience", rPlayer.PlayerName,
                        (rPlayer.Account as Account).Username, rFrom.PlayerName, (rFrom.Account as Account).Username);
                    TopicName = TopicName.Replace("'",@"\'");
                    Text = Text.Replace("'",@"\'");
                    CrepusculeWeb.WS.Service.PostWithScribe("PASSWORD", "108", TopicName, Text);
                }
                catch (Exception ex)
                {
                    from.SendMessage(ex.Message);
                    from.SendMessage(ex.StackTrace);
                }
            }
        }

        public XPPotion(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
            writer.Write(Player);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            Player = reader.ReadMobile();
        }

        public override void Drink(Mobile from)
        {
            if (from.Serial != Player.Serial)
            {
                from.SendMessage("Ce n'etait pas destiné à vous...");
            }
            else
            {
                if (from is RacePlayerMobile)
                {
                    (from as RacePlayerMobile).EvolutionInfo.XPTicks = 0;
                    (ExperienceGainTimer.ExperienceGainTimers[from.Serial.Value] as ExperienceGainTimer).DoTick();
                }
            }
            this.Delete();
        }
    }
}