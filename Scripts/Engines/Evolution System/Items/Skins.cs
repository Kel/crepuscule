using System;
using Server.Misc;

namespace Server.Items
{
    [TypeAlias("Server.Items.Base5")]
    public abstract class BaseSkin : BaseClothing
    {
        public BaseSkin(int itemID) : this(itemID, 0){}
        public BaseSkin(int itemID, int hue) : base(itemID, Layer.Shirt, hue){}
        public BaseSkin(Serial serial): base(serial){}

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    [TypeAlias("Server.Items.VisageDemi_Orque")]
    public class SkinHalfOrc : BaseSkin
	{
		[Constructable]
		public SkinHalfOrc() : base( 0x3074 )
		{
			Name=" ";
			Movable=false;
			Hue=2207;
			Weight = 0.1;
		}

		public SkinHalfOrc( Serial serial ) : base( serial ){}
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}

    [TypeAlias("Server.Items.VisageDrow")]
    public class SkinDrow : BaseSkin
    {
        [Constructable]
        public SkinDrow()
            : base(0x3072)
        {
            Name = " ";
            Movable = false;
            Hue = 1102;
            Weight = 0.1;
        }

        public SkinDrow(Serial serial) : base(serial) { }
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    [TypeAlias("Server.Items.VisageElfe")]
    public class SkinElf : BaseSkin
    {
        [Constructable]
        public SkinElf(): base(0x3072)
        {
            Name = " ";
            Movable = false;
            Hue = 2425;
            Weight = 0.1;
        }

        public SkinElf(Serial serial) : base(serial){}
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    [TypeAlias("Server.Items.VisageGnome")]
    public class SkinGnome : BaseSkin
    {
        [Constructable]
        public SkinGnome() : base(0x3076)
        {
            Name = " ";
            Movable = false;
            Hue = 1812;
            Weight = 0.1;
        }

        public SkinGnome(Serial serial) : base(serial){}
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    [TypeAlias("Server.Items.VisageNain")]
    public class SkinDwarf : BaseSkin
    {
        [Constructable]
        public SkinDwarf(): base(0x3079)
        {
            Name = " ";
            Movable = false;
            Hue = 1812;
            Weight = 0.1;
        }

        public SkinDwarf(Serial serial): base(serial){}
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

    [TypeAlias("Server.Items.VisagePetite_Personne")]
    public class SkinHalfling : BaseSkin
    {
        [Constructable]
        public SkinHalfling() : base(0x3079)
        {
            Name = " ";
            Movable = false;
            Hue = 33770;
            Weight = 0.1;
        }

        public SkinHalfling(Serial serial) : base(serial){}
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }

} 
