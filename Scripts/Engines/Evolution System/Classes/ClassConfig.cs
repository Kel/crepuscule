﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Gumps;

namespace Server.Engines
{
    public static class ClassConfig
    {
        public static void Initialize()
        {
            EventSink.Login += new LoginEventHandler(EventSink_Login);
        }

        static void EventSink_Login(LoginEventArgs e)
        {
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                var from = e.Mobile as RacePlayerMobile;
                if (from.EvolutionInfo.IsResetting)
                {
                    ShowResetGump(from);
                }
            }
        }

        /// <summary>
        /// Shows PA Reset gump with a message
        /// </summary>
        public static void ShowResetGump(RacePlayerMobile mobile)
        {
            mobile.EvolutionInfo.IsResetting = true;
            mobile.Squelched = true;
            mobile.Frozen = true;

            if (mobile.NetState != null)
            {
                // The actual reset
                for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
                    mobile.Capacities[i].Value = 0;

                // Show gump
                mobile.CloseAllGumps();
                mobile.SendGump(new CharacterSheetGump(mobile, true));
                mobile.SendGump(new NotifyResetGump(mobile));
            }
        }

        /// <summary>
        /// Handles change of the class
        /// </summary>
        public static void ChangeClass(RacePlayerMobile Player, ClassInfo NewClass)
        {
            var Class = Player.ClassInfo;
        
            // It is actually an automatic procedure, so do some particular stuff
            Player.ClassInfo.Branch = NewClass.Branch;
        

            // Set the capacities
            for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
            {
                var Actual = Class.Capacities[i];
                var New = NewClass.Capacities[i];

                Actual.Value = 0;

                // Evaluate the cost
                if (New.RestrictedToRace != RaceType.None && New.RestrictedToRace == Player.EvolutionInfo.Race) Actual.Cost = CapacityCost.Primary;
                else if (New.RestrictedToRace != RaceType.None) Actual.Cost = CapacityCost.Unavailable;
                else
                {
                    Actual.Cost = New.Cost;
                }
                
            }

            // Reset the class
            Class.Name = NewClass.Name;
            Class.ClassInfoLink = NewClass.Number;
            EvolutionConfig.SetStats(Player);
            EvolutionConfig.SetSkillCaps(Player);
            
            // Send message to confirm
            Player.LocalOverheadMessage(Network.MessageType.Emote, Player.EmoteHue, false, "*Vous devenez " + NewClass.Name + "*");
            Player.SendMessage("Votre personnage a changé de classe, les points d'aptitudes ont étés réinitialisés.");

            // Show reset gump
            ShowResetGump(Player);
        }


        /// <summary>
        /// To be used during the migration only
        /// </summary>
        public static void MigrateClass(RacePlayerMobile Player, ClassInfo NewClass)
        {
            if (NewClass == null)
            {
                WorldContext.BroadcastMessage(AccessLevel.Administrator, 7, Player.PlayerName + " n'as pas de classe liée!");
                return;
            }
            var Class = Player.ClassInfo;
            Player.ClassInfo.SilentSetBranch(NewClass.Branch);
            

            // Set the capacities
            for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
            {
                var Actual = Class.Capacities[i];
                var New = NewClass.Capacities[i];

                // Evaluate the cost
                if (New.RestrictedToRace != RaceType.None && New.RestrictedToRace == Player.EvolutionInfo.Race) Actual.Cost = CapacityCost.Primary;
                else if (New.RestrictedToRace != RaceType.None) Actual.Cost = CapacityCost.Unavailable;
                else
                {
                    Actual.Cost = New.Cost;
                }

            }

            // Reset the class & trim the skills
            Class.Name = NewClass.Name;
            Class.ClassInfoLink = NewClass.Number;
            
            EvolutionConfig.SetStats(Player);
            EvolutionConfig.SetSkillCaps(Player);
            EvolutionConfig.TrimSkills(Player);
        }


        /// <summary>
        /// Checks if the player can change the class
        /// </summary>
        public static bool CanChangeClass(RacePlayerMobile Player, ClassInfo NewClass)
        {
            if (Player.ClassInfo.Branch != NewClass.Branch)
            {
                Player.SendMessage("Le changement de classe est impossible, car c'est une classe de branche diffèrente de la vôtre.");
                Player.SendMessage("Si vous souhaitez tout de même changer de classe, celà est possible avec soumission d'un BG de changement.");
                return false;
            }
            if (Player.EvolutionInfo.Level < NewClass.MinLevel)
            {
                Player.SendMessage("Le changement de classe est impossible, car cette classe requiert le niveau " + NewClass.MinLevel);
                return false;
            }
            return true;
        }


        /// <summary>
        /// Gets a class info by it's number
        /// </summary>
        public static ClassInfo GetClassByNumber(int number)
        {
            foreach (var classInfo in ClassInfo.Classes)
            {
                if (classInfo.Number == number)
                    return classInfo;
            }
            return null;
        }
    }

    public enum ClassBranch
    {
        Warrior,
        Rogue,
        Mage,
        Spiritual,
        Craftsman
    }
}
