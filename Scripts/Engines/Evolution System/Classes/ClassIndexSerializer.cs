﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Scripts.Commands;
using System.Linq;
using System.IO;
using Server.Guilds;
using Server.Accounting;

namespace Server.Engines
{
    /// <summary>
    /// Basically a serialization container for the class info (a bit dirty, but simple)
    /// </summary>
    public class ClassIndexSerializer
    {
        private static readonly string DataDirectory = Path.Combine(Core.BaseDirectory, @"Data\Binary");
        private const string DataFileName = "Classes.bin";
        private static string DataFile { get { return Path.Combine(DataDirectory, DataFileName); } }


        public static void Configure()
        {
            EventSink.WorldLoad += new WorldLoadEventHandler(EventSink_WorldLoad);
            EventSink.WorldSave += new WorldSaveEventHandler(EventSink_WorldSave);
        }

        static void EventSink_WorldLoad()
        {
            try
            {
                var reader = new BinaryFileReader(new BinaryReader(File.Open(DataFile, FileMode.Open)));
                ClassInfo.Classes.Clear();

                int version = reader.ReadInt();
                var classes = reader.ReadInt();

                for (int i = 0; i < classes; ++i)
                    ClassInfo.Classes.Add(new ClassInfo(null, reader));

                //Console.WriteLine("Loaded {0} Classes", classes);

                reader.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Classes: " + ex.Message);
                // TODO: Something
            }
        
        }

        static void EventSink_WorldSave(WorldSaveEventArgs e)
        {
            try
            {
                var writer = new BinaryFileWriter( File.Open(DataFile, FileMode.Create), true );

                writer.Write((int)0); // version
                writer.Write(ClassInfo.Classes.Count);
                foreach (var c in ClassInfo.Classes)
                    c.Serialize(writer);
                //Console.WriteLine("Saved {0} Classes", ClassInfo.Classes.Count);

                writer.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Classes: " + ex.Message);
                // TODO: Something
            }
        }




    }
    
}
