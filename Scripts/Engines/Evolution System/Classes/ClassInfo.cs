﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines 
{
    [PropertyObject]
    public class ClassInfo
    {
        #region Fields & Properties
        private ClassBranch m_Branch;
        internal int ClassInfoLink;

        [CommandProperty(AccessLevel.GameMaster)]
        public ClassInfo LinkedClass { get { return ClassConfig.GetClassByNumber(ClassInfoLink); } }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Number { get; private set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public string Name { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public Capacities Capacities { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MinLevel { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public ClassBranch Branch 
        { 
            get { return m_Branch; }
            set
            {
                m_Branch = value;
                if (Player != null)
                {
                    // Reset the capacities
                    for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
                    {
                        Capacities[i].Cost = CapacityCost.Unavailable;
                        Capacities[i].Value = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Player, can be null in case if it's a settings
        /// </summary>
        internal RacePlayerMobile Player = null;
        #endregion

        #region Constructors & Serialization
        public ClassInfo(int number) : this(null, number) { }
        public ClassInfo(PlayerMobile player, int number) 
        {
            Player = player as RacePlayerMobile;
            Number = number;
            Name = "Sans nom";
            Capacities = new Capacities(Player);
        }
        public ClassInfo(PlayerMobile player, GenericReader reader)
        {
            Player = player as RacePlayerMobile;
            var version = reader.ReadInt();

            switch(version)
            {
                case 1:
                {
                    ClassInfoLink = reader.ReadInt();
                    goto case 0;
                }
                case 0:
                {
                    Number = reader.ReadInt();
                    Name = reader.ReadString();
                    m_Branch = (ClassBranch)reader.ReadInt();
                    MinLevel = reader.ReadInt();
                    Capacities = new Capacities(Player, reader);

                    if (version == 0 && Player != null)
                    {
                        foreach (var c in ClassInfo.Classes)
                        {
                            if (this.Name == c.Name)
                                ClassInfoLink = c.Number;
                        }
                    }

                    break;
                }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)1); //version
            // 1
            writer.Write(ClassInfoLink);
            // 0
            writer.Write(Number);
            writer.Write(Name);
            writer.Write((int)m_Branch);
            writer.Write(MinLevel);
            Capacities.Serialize(writer);
        }

        public override string ToString()
        {
            return Name;
        }
        #endregion

        #region Static Members
        /// <summary>
        /// The list of IG classes available
        /// </summary>
        public static List<ClassInfo> Classes = new List<ClassInfo>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Changes the branch without resetting the values
        /// </summary>
        public void SilentSetBranch(ClassBranch branch)
        {
            m_Branch = branch;
            if (Player != null)
            {
                // Reset the capacities
                for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
                {
                    Capacities[i].Cost = CapacityCost.Unavailable;
                }
            }
        }

        /// <summary>
        /// Computes a skill cap for a particular level, everything is taken into account
        /// </summary>
        public double GetSkillCapForLevel(SkillName skill, int level)
        {
            var SkillRelatedCost = CapacityCost.Unavailable;
            var CapacityList = new List<CapacityInfo>();
            for (int i = 0; i < CapacityInfo.CapacitiesCount; ++i)
            {
                var capacity = Capacities[i];
                var modifier = capacity.GetSkillCapModifier(skill);
                if (modifier != null)
                {
                    if ((int)capacity.Cost < (int)SkillRelatedCost)
                        SkillRelatedCost = capacity.Cost;
                    CapacityList.Add(capacity);
                }
            }

            // Gets the maximum authorized cap
            var MaxCap = GetSkillCapForLevelCost(SkillRelatedCost, level);
            var Cap = EvolutionConfig.InitialCap[SkillRelatedCost];

            // Now, we can compute the skill cap
            foreach (var capacity in CapacityList)
            {
                var modifier = capacity.GetSkillCapModifier(skill);
                if (modifier != null)
                {
                    Cap += modifier.CapPerLevel * capacity.Value;
                }
            }

            // Fit to cap
            if (Cap >= MaxCap)
                Cap = MaxCap;
            return Cap;
        }

        /// <summary>
        /// Gets the maximum skill cap given the cost of the capacity
        /// </summary>
        public double GetSkillCapForLevelCost(CapacityCost cost, int level)
        {
            double result = 0;

            if (!EvolutionConfig.IsHighLevel(level)) // Normal
            {
                result = (level - 1) * EvolutionConfig.CapsPerLevel[cost] + EvolutionConfig.InitialCap[cost];
                if (result >= EvolutionConfig.MaxCapLow[cost])
                    result = EvolutionConfig.MaxCapLow[cost];
            }
            else // High level
            {
                result = EvolutionConfig.MaxCapLow[cost];
                result += (level - EvolutionConfig.MaxLevel) * EvolutionConfig.CapsPerHighLevel[cost];
                if (result >= EvolutionConfig.MaxCap[cost])
                    result = EvolutionConfig.MaxCap[cost];
            }
            return result;
        }
        #endregion
    }
}
