﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    public static class CapacitiesConfig
    {
        public static string GetName(CapacityName Name)
        {
            if (Name == CapacityName.Acrobatics) return "Acrobatie";
            if (Name == CapacityName.Alchemy) return "Alchimie";
            if (Name == CapacityName.AnimalEmpathy) return "Empathie Animale";
            if (Name == CapacityName.Assassination) return "Assassinat";
            if (Name == CapacityName.Bardic) return "Bardisme";
            if (Name == CapacityName.BetterEndurance) return "Entrainement";
            if (Name == CapacityName.Blacksmithing) return "Forge";
            if (Name == CapacityName.BloodMagic) return "Magie Sanguine";
            if (Name == CapacityName.Bows) return "Arcs";
            if (Name == CapacityName.CloseCombat) return "Armes de Mélée";
            if (Name == CapacityName.DistanceCombat) return "Armes à Distance";
            if (Name == CapacityName.Enchanting) return "Enchantement";
            if (Name == CapacityName.Healing) return "Médecine";
            if (Name == CapacityName.Herblore) return "Herborestrie";
            if (Name == CapacityName.Hunting) return "Chasse";
            if (Name == CapacityName.LockPicking) return "Crochetage";
            if (Name == CapacityName.Lore) return "Erudition";
            if (Name == CapacityName.MartialArts) return "Arts Martiaux";
            if (Name == CapacityName.Nature) return "Magie Naturelle";
            if (Name == CapacityName.Necromancy) return "Nécromancie";
            if (Name == CapacityName.PickPocket) return "Vol à la Tire";
            if (Name == CapacityName.Rage) return "Rage";
            if (Name == CapacityName.Sabotage) return "Sabotage";
            if (Name == CapacityName.Sorcery) return "Sorcellerie";
            if (Name == CapacityName.Stealth) return "Furtivité";
            if (Name == CapacityName.Tailoring) return "Couture";
            if (Name == CapacityName.Telepathy) return "Télépathie";
            if (Name == CapacityName.Theology) return "Théologie";
            if (Name == CapacityName.BetterAgility) return "Agilité Accrue";
            if (Name == CapacityName.Tinkering) return "Bricolage";
            if (Name == CapacityName.Wizardry) return "Magie Arcanique";
            if (Name == CapacityName.Wrestling) return "Lutte";

            return "???";

        }

        public static string GetName(SkillName Name)
        {
            if (Name == SkillName.Alchemy) return "Alchimie";
            if (Name == SkillName.Anatomy) return "Anatomie";
            if (Name == SkillName.AnimalLore) return "Zoologie";
            if (Name == SkillName.AnimalTaming) return "Dréssage";
            if (Name == SkillName.Archery) return "Archerie";
            if (Name == SkillName.ArmsLore) return "Maîtrise d'Armes";
            if (Name == SkillName.Begging) return "Mendicité";
            if (Name == SkillName.Blacksmith) return "Ferronerie";
            if (Name == SkillName.Camping) return "Campement";
            if (Name == SkillName.Carpentry) return "Charpenterie";
            if (Name == SkillName.Cartography) return "Cartographie";
            if (Name == SkillName.Chivalry) return "Pretrise";
            if (Name == SkillName.Cooking) return "Cuisine";
            if (Name == SkillName.DetectHidden) return "Détéction d'Invisible";
            if (Name == SkillName.Discordance) return "Désaccord";
            if (Name == SkillName.EvalInt) return "Evaluation d'Intelligence";
            if (Name == SkillName.Fencing) return "Armes d'estoc";
            if (Name == SkillName.Fishing) return "Peche";
            if (Name == SkillName.Fletching) return "Création d'arcs/fléches";
            if (Name == SkillName.Focus) return "Concentration de guerre";
            if (Name == SkillName.Forensics) return "Autopsie";
            if (Name == SkillName.Healing) return "Guerison";
            if (Name == SkillName.Herding) return "Pâtre";
            if (Name == SkillName.Hiding) return "Cachette d'ombres";
            if (Name == SkillName.Inscribe) return "Transcription";
            if (Name == SkillName.ItemID) return "Estimation d'objets";
            if (Name == SkillName.Lockpicking) return "Crochetage";
            if (Name == SkillName.Lumberjacking) return "Bûcheron";
            if (Name == SkillName.Macing) return "Armes de choc";
            if (Name == SkillName.Magery) return "Magie";
            if (Name == SkillName.MagicResist) return "Résistance à la magie";
            if (Name == SkillName.Meditation) return "Méditation";
            if (Name == SkillName.Mining) return "Excavation";
            if (Name == SkillName.Musicianship) return "Virtuosité";
            if (Name == SkillName.Necromancy) return "Nécromancie";
            if (Name == SkillName.Parry) return "Esquive";
            if (Name == SkillName.Peacemaking) return "Reconciliation";
            if (Name == SkillName.Poisoning) return "Empoisonnement";
            if (Name == SkillName.Provocation) return "Provocation";
            if (Name == SkillName.RemoveTrap) return "Désarmer";
            if (Name == SkillName.Snooping) return "Furetage";
            if (Name == SkillName.SpiritSpeak) return "Spiritisme";
            if (Name == SkillName.Stealing) return "Vol à tire";
            if (Name == SkillName.Stealth) return "Furtivité";
            if (Name == SkillName.Swords) return "Armes de taille";
            if (Name == SkillName.Tactics) return "Tactique";
            if (Name == SkillName.Tailoring) return "Tissage";
            if (Name == SkillName.TasteID) return "Goûteur";
            if (Name == SkillName.Tinkering) return "Bricolage";
            if (Name == SkillName.Tracking) return "Pistage";
            if (Name == SkillName.Veterinary) return "Soin Animal";
            if (Name == SkillName.Wrestling) return "Lutte";


            return "???";

        }

        public static int GetCostValue(CapacityCost Cost)
        {
            if (Cost == CapacityCost.Primary) return 1;
            if (Cost == CapacityCost.Normal) return 2;
            if (Cost == CapacityCost.Penalized) return 3;
            if (Cost == CapacityCost.Unavailable) return 999;

            return 999;
        }

        public static string GetCostDescription(CapacityCost Cost)
        {
            if (Cost == CapacityCost.Primary) return "Prédilection";
            if (Cost == CapacityCost.Normal) return "Normale";
            if (Cost == CapacityCost.Penalized) return "Hors-Classe";
            if (Cost == CapacityCost.Unavailable) return "/";

            return "???";
        }

        public static int GetCostHue(CapacityCost Cost)
        {
            if (Cost == CapacityCost.Primary) return 672;
            if (Cost == CapacityCost.Normal) return 249;
            if (Cost == CapacityCost.Penalized) return 533;
            if (Cost == CapacityCost.Unavailable) return 972;

            return 0;
        }
    }
}
