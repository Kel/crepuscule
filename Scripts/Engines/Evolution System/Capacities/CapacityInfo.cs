﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Engines;

namespace Server {

    /// <summary>
    /// The information of the capacity, storeable
    /// </summary>
    [PropertyObject]
    public class CapacityInfo : IComparable<CapacityInfo>
    {
        #region Fields & Properties
        private CapacityName m_Type;
        private int m_Value = 0;
        private string m_Description = "";
        private RaceType m_RestrictedToRace = RaceType.None;
        private SkillCapModifier m_Skill1;
        private SkillCapModifier m_Skill2;
        private SkillCapModifier m_Skill3;
        private SkillCapModifier m_Skill4;
        private SkillCapModifier m_Skill5;
        private StatsCapModifier m_Stats;

        internal RacePlayerMobile Player;

        [CommandProperty(AccessLevel.GameMaster)]
        public SkillCapModifier Skill1 { get { return m_Skill1; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public SkillCapModifier Skill2 { get { return m_Skill2; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public SkillCapModifier Skill3 { get { return m_Skill3; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public SkillCapModifier Skill4 { get { return m_Skill4; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public SkillCapModifier Skill5 { get { return m_Skill5; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public StatsCapModifier Stats { get { return m_Stats; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityName Type { get { return m_Type; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public string Name { get { return CapacitiesConfig.GetName(m_Type); } }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Hue { get { return CapacitiesConfig.GetCostHue(Cost); } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityCost Cost { get; set; }
        [CommandProperty(AccessLevel.GameMaster)]
        public string Description { get { return m_Description; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public RaceType RestrictedToRace { get { return m_RestrictedToRace; } }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Value 
        { 
            get{ return m_Value;}
            set
            {
                int oldValue = m_Value;
                if (m_Value <= 0) m_Value = 0;
                if (m_Value > EvolutionConfig.MaxCapacityCap)  m_Value = EvolutionConfig.MaxCapacityCap; 
                else m_Value = value;

                if (Player != null)
                    SetValue(oldValue, m_Value);
            }
        }
        #endregion

        #region Constructors & Serialize
        public CapacityInfo(RacePlayerMobile player, CapacityName type) 
        {
            Player = player;
            m_Type = type;
            SetType(type);
        }
        public CapacityInfo(RacePlayerMobile player, GenericReader reader )
        {
            Player = player;
            
            var version = reader.ReadInt();
            m_Type = (CapacityName)reader.ReadInt();
            Cost = (CapacityCost)reader.ReadInt();
            m_Value = reader.ReadInt();
            SetType(m_Type);
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)Type);
            writer.Write((int)Cost);
            writer.Write(m_Value);
        }

        public override string ToString()
        {
            if (Cost == CapacityCost.Unavailable)
                return String.Format("({0})", Cost.ToString());
            else
            {
                return String.Format("({0}, {1})", Cost.ToString(), Value);
            }
        }
        #endregion

        #region SetType & SetValue
        public void SetType(CapacityName type)
        {
            // Reset
            m_Stats  = null;
            m_Skill1 = null;
            m_Skill2 = null;
            m_Skill3 = null;
            m_Skill4 = null;
            m_Skill5 = null;

            switch (type)
            {
                case CapacityName.Acrobatics:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Parry, 1.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Focus, 1.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Hiding, 1.0);
                    m_Stats = new StatsCapModifier()
                    {
                        ModDex = true,
                        ModDexLevelInterval = 2
                    };
                    m_Description = "Ajoute 1 en dextérité tous les 2 niveaux d'aptitude";
                    break; 
                }
                case CapacityName.Alchemy:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Alchemy, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Poisoning, 1.0);
                    m_Skill3 = new SkillCapModifier(SkillName.TasteID, 1.0);
                    break;
                }
                case CapacityName.AnimalEmpathy:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.AnimalLore, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.AnimalTaming, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Veterinary, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Herding, 2.0);
                    m_Skill5 = new SkillCapModifier(SkillName.Fishing, 1.0);
                    break;
                }
                case CapacityName.Assassination:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Poisoning, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Fencing, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Hiding, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Anatomy, 1.0);
                    break;
                }
                case CapacityName.Bardic:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Discordance, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Musicianship, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Peacemaking, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Provocation, 2.0);
                    break;
                }
                case CapacityName.BetterEndurance:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Focus, 3.0);
                    m_Skill2 = new SkillCapModifier(SkillName.MagicResist, 2.0);
                    m_Stats = new StatsCapModifier()
                    {
                        ModDex = true,
                        ModStr = true,
                        ModDexLevelInterval = 3,
                        ModStrLevelInterval = 3
                    };
                    m_Description = "Ajoute 1 en dextérité et force tous les 3 niveaux d'aptitude";
                    break;
                }
                case CapacityName.Blacksmithing:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Mining, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Blacksmith, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.ArmsLore, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.ItemID, 1.0);
                    break;
                }
                case CapacityName.BloodMagic:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Meditation, 1.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Magery, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Necromancy, 2.0);
                    m_Skill5 = new SkillCapModifier(SkillName.SpiritSpeak, 1.0);
                    m_RestrictedToRace = RaceType.Tiefling;
                    m_Description = "Avec chaque point, permet de débloquer la sorcellerie et nécromancie. Disponible uniquement pour Tieflings.";
                    break;
                }
                case CapacityName.Bows:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Lumberjacking, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Fletching, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Carpentry, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.ArmsLore, 1.0);
                    break;
                }
                case CapacityName.CloseCombat:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Swords, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Macing, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Fencing, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Tactics, 2.0);
                    m_Skill5 = new SkillCapModifier(SkillName.ArmsLore, 2.0);
                    break;
                }
                case CapacityName.DistanceCombat:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Archery, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Tactics, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Fletching, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Tracking, 1.0);
                    break;
                }
                case CapacityName.Enchanting:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.MagicResist, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Inscribe, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.ItemID, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Magery, 1.0);
                    break;
                }
                case CapacityName.Healing:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Anatomy, 3.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Healing, 3.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Forensics, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Veterinary, 1.0);
                    break;
                }
                case CapacityName.Herblore:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.TasteID, 3.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Cooking, 3.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Camping, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Fishing, 1.0);
                    break;
                }
                case CapacityName.Hunting:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.DetectHidden, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Tracking, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Anatomy, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Camping, 1.0);
                    m_Skill5 = new SkillCapModifier(SkillName.TasteID, 1.0);
                    break;
                }
                case CapacityName.Lore:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.ItemID, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.ArmsLore, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.AnimalLore, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill5 = new SkillCapModifier(SkillName.Inscribe, 1.0);
                    break;
                }
                case CapacityName.MartialArts:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Wrestling, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Parry, 1.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Focus, 1.0);
                    m_Description = "Ajoute des bonus de force et d'ésquive au combat à mains nues";
                    break;
                }
                case CapacityName.Nature:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Meditation, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Necromancy, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Veterinary, 1.0);
                    break;
                }
                case CapacityName.Necromancy:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Meditation, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Necromancy, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.SpiritSpeak, 2.0);
                    break;
                }
                case CapacityName.PickPocket:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Snooping, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Stealing, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Begging, 2.0);
                    break;
                }
                case CapacityName.Rage:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Wrestling, 1.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Focus, 1.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Swords, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Macing, 1.0);

                    m_Stats = new StatsCapModifier()
                    {
                        ModStr = true,
                        ModStrLevelInterval = 2
                    };
                    m_Description = "Ajoute 1 en force tous les 2 niveaux d'aptitude";
                    break;
                }
                case CapacityName.Sabotage:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.RemoveTrap, 3.0);
                    m_Skill2 = new SkillCapModifier(SkillName.ItemID, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Lockpicking, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.MagicResist, 1.0);
                    break;
                }
                case CapacityName.Sorcery:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Meditation, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Magery, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Anatomy, 1.0);
                    break;
                }
                case CapacityName.Stealth:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Stealth, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Hiding, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.DetectHidden, 2.0);
                    break;
                }
                case CapacityName.Tailoring:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Tailoring, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Fishing, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Cooking, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.ItemID, 1.0);
                    break;
                }
                case CapacityName.Telepathy:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.SpiritSpeak, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Meditation, 1.0);
                    m_Stats = new StatsCapModifier()
                    {
                        ModInt = true,
                        ModIntLevelInterval = 2
                    };
                    m_Description = "Ajoute 1 en intelligence tous les 2 niveaux d'aptitude";
                    break;
                }
                case CapacityName.Theology:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Meditation, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Chivalry, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Macing, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Healing, 1.0);
                    break;
                }
                case CapacityName.BetterAgility:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Parry, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Tactics, 1.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Stealing, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Anatomy, 1.0);
                    break;
                }
                case CapacityName.Tinkering:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Lumberjacking, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Carpentry, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Tinkering, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.ItemID, 1.0);
                    break;
                }
                case CapacityName.LockPicking:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.RemoveTrap, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Tinkering, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Lockpicking, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.ItemID, 1.0);
                    break;
                }
                case CapacityName.Wizardry:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.EvalInt, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Meditation, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Magery, 2.0);
                    m_Skill4 = new SkillCapModifier(SkillName.MagicResist, 1.0);
                    break;
                }
                case CapacityName.Wrestling:
                {
                    m_Skill1 = new SkillCapModifier(SkillName.Tactics, 2.0);
                    m_Skill2 = new SkillCapModifier(SkillName.Wrestling, 2.0);
                    m_Skill3 = new SkillCapModifier(SkillName.Parry, 1.0);
                    m_Skill4 = new SkillCapModifier(SkillName.Anatomy, 1.0);
                    m_Skill5 = new SkillCapModifier(SkillName.Focus, 1.0);
                    break;
                }
            }
        }

        public void SetValue(int OldValue, int NewValue)
        {
            var Change = NewValue - OldValue;
            
            // Change detected, set stats & skillcaps
            EvolutionConfig.SetStats(Player);
            EvolutionConfig.SetSkillCaps(Player);
        }

        /// <summary>
        /// Gets a modifier if exists, returns null if none
        /// </summary>
        public SkillCapModifier GetSkillCapModifier(SkillName skill)
        {
            if (Skill1 != null && Skill1.Skill == skill) return Skill1;
            if (Skill2 != null && Skill2.Skill == skill) return Skill2;
            if (Skill3 != null && Skill3.Skill == skill) return Skill3;
            if (Skill4 != null && Skill4.Skill == skill) return Skill4;
            if (Skill5 != null && Skill5.Skill == skill) return Skill5;
            return null;
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Different capacities
        /// </summary>
        public static int CapacitiesCount = 0;

        /// <summary>
        /// Initialize in a static constructor
        /// </summary>
        static CapacityInfo()
        {
            CapacitiesCount = Enum.GetNames(typeof(CapacityName)).Length;
        }

        /// <summary>
        /// Creates a filled capacity list
        /// </summary>
        public static CapacityInfo[] CreateCapacityList()
        {
            var result = new List<CapacityInfo>();
            var names  = Enum.GetNames(typeof(CapacityName));
            foreach (var name in names)
            {
                result.Add(new CapacityInfo(null, (CapacityName)Enum.Parse(typeof(CapacityName), name))
                {
                    Cost = CapacityCost.Penalized, // Default cost
                    Value = 0 // Default value
                });
            }
            return result.ToArray();
        }
        #endregion

        #region IComparable<CapacityInfo> Members

        public int CompareTo(CapacityInfo other)
        {
            return String.Compare(this.Name, other.Name);
        }

        #endregion
    }


    #region Modifiers
    public class SkillCapModifier
    {
        public SkillName Skill { get; set; }
        public double CapPerLevel { get; set; }

        public SkillCapModifier(SkillName skill, double perLevel)
        {
            Skill = skill;
            CapPerLevel = perLevel;
        }

        public override string ToString()
        {
            return String.Format("{0} +{1}", Skill, CapPerLevel);
        }
    }

    public class StatsCapModifier
    {
        public bool ModInt { get; set; }
        public bool ModDex { get; set; }
        public bool ModStr { get; set; }

        public short ModIntLevelInterval { get; set; }
        public short ModDexLevelInterval { get; set; }
        public short ModStrLevelInterval { get; set; }

        public double CapPerLevel { get; set; }

        public StatsCapModifier()
        {
        }

        public override string ToString()
        {
            return "StatMod";
        }
    }
    #endregion
}
