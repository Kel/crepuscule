﻿using Server.Network;
using System;
using System.Reflection;
using Server.Engines;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;

namespace Server 
{

    [PropertyObject]
    public class Capacities 
    {
        private int m_Cap;
        private CapacityInfo[] m_Capacities;
        internal RacePlayerMobile Player;

        public Capacities(RacePlayerMobile player)
        {
            Player = player;
            m_Cap = EvolutionConfig.MaxCapacityCap;

            // Create new capacity list
            m_Capacities = CapacityInfo.CreateCapacityList();
        }

        #region Serialization
        public Capacities(RacePlayerMobile player, GenericReader reader)
        {
            Player = player;
            m_Capacities = new CapacityInfo[CapacityInfo.CapacitiesCount];

            var Version = reader.ReadInt();
            m_Cap = reader.ReadInt();
            int length = reader.ReadInt();
            for (int i = 0; i < length; i++)
            {
                m_Capacities[i] = new CapacityInfo(player, reader);
            }
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write(0);
            writer.Write(this.m_Cap);
            writer.Write(this.m_Capacities.Length);
            for (int i = 0; i < this.m_Capacities.Length; i++)
            {
                var capacity = m_Capacities[i];
                capacity.Serialize(writer);
            }
        }

        public override string ToString()
        {
            return "(Infos des Aptitudes)";
        }

        public List<CapacityInfo> ToOrderedList()
        {
            var result = m_Capacities.ToList();
            result.Sort();
            return result;
        }
        #endregion

        #region Indexer

        public CapacityInfo this[CapacityName name]
        {
            get
            {
                return this[(int)name];
            }
        }

        public CapacityInfo this[int capacityID]
        {
            get
            {
                if ((capacityID < 0) || (capacityID >= this.m_Capacities.Length))
                {
                    return null;
                }
                var capacity = this.m_Capacities[capacityID];
                if (capacity == null)
                {
                    this.m_Capacities[capacityID] = capacity = new CapacityInfo(Player, (CapacityName)capacityID);
                }
                return capacity;
            }
        }
        #endregion

        #region Properties
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Acrobatics { get { return this[CapacityName.Acrobatics]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Alchemy { get { return this[CapacityName.Alchemy]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Assassination { get { return this[CapacityName.Assassination]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Bardic { get { return this[CapacityName.Bardic]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Tinkering { get { return this[CapacityName.Tinkering]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Bows { get { return this[CapacityName.Bows]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo CloseCombat { get { return this[CapacityName.CloseCombat]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo DistanceCombat { get { return this[CapacityName.DistanceCombat]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo BetterAgility { get { return this[CapacityName.BetterAgility]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo BloodMagic { get { return this[CapacityName.BloodMagic]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Wrestling { get { return this[CapacityName.Wrestling]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Tailoring { get { return this[CapacityName.Tailoring]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Hunting { get { return this[CapacityName.Hunting]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Enchanting { get { return this[CapacityName.Enchanting]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo BetterEndurance { get { return this[CapacityName.BetterEndurance]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Lore { get { return this[CapacityName.Lore]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo AnimalEmpathy { get { return this[CapacityName.AnimalEmpathy]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Blacksmithing { get { return this[CapacityName.Blacksmithing]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Stealth { get { return this[CapacityName.Stealth]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Herblore { get { return this[CapacityName.Herblore]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Wizardry { get { return this[CapacityName.Wizardry]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Sorcery { get { return this[CapacityName.Sorcery]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Necromancy { get { return this[CapacityName.Necromancy]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Theology { get { return this[CapacityName.Theology]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Nature { get { return this[CapacityName.Nature]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Healing { get { return this[CapacityName.Healing]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo LockPicking { get { return this[CapacityName.LockPicking]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Rage { get { return this[CapacityName.Rage]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Sabotage { get { return this[CapacityName.Sabotage]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo Telepathy { get { return this[CapacityName.Telepathy]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo PickPocket { get { return this[CapacityName.PickPocket]; } set { } }
        [CommandProperty(AccessLevel.GameMaster)]
        public CapacityInfo MartialArts { get { return this[CapacityName.MartialArts]; } set { } }

        #endregion
    }
}

