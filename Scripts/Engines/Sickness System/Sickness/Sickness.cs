using System;
using Server.Misc;
using Server.Network;
using System.Collections;
using Server.Items;
using Server.Targeting;

namespace Server.Mobiles
{
   public class Sickness : BaseCreature
      {
      [Constructable]
      public Sickness():base(AIType.AI_Melee, FightMode.Closest, 30, 1, 0.14, 0.22)
      {
         Body = 0x400;
         BaseSoundID = -1;
         Name = ( "Sickness" );
         SetStr( 110, 125 );
         SetDex( 105, 125 );
         SetInt( 20, 35 );
         SetHits( 100, 120 );
         SetSkill( SkillName.Swords, 205.0, 220.0 );
         SetSkill( SkillName.Tactics, 210.0, 220.0 );
         SetSkill( SkillName.Focus, 210.0, 220.0);
         Hidden = true;
         AccessLevel = AccessLevel.GameMaster;

         int rnd = Utility.Random( 1, 3 );

         switch ( rnd )
         {
            case 1:
            {
               ColdSicknessSword sick = new ColdSicknessSword();
               sick.Movable = false;
               sick.LootType = LootType.Blessed;
               AddItem( sick );
               break;
            }
            case 2:
            {
               FluSicknessSword sick = new FluSicknessSword();
               sick.Movable = false;
               sick.LootType = LootType.Blessed;
               AddItem( sick );
               break;
            }
            case 3:
            {
               HeadacheSicknessSword sick = new HeadacheSicknessSword();
               sick.Movable = false;
               sick.LootType = LootType.Blessed;
               AddItem( sick );
               break;
            }

            case 4:
            {
               VirusSicknessSword sick = new VirusSicknessSword();
               sick.Movable = false;
               sick.LootType = LootType.Blessed;
               AddItem( sick );
               break;
            }
         }
      }

       public Sickness( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();

      }
   }

   public class ColdSickness : BaseCreature
      {
      [Constructable]
      public ColdSickness():base(AIType.AI_Melee, FightMode.Closest, 30, 1, 0.14, 0.22)
      {
         Body = 0x400;
         BaseSoundID = -1;
         Name = ( "Tuberculose" );
         SetStr( 110, 125 );
         SetDex( 105, 125 );
         SetInt( 20, 35 );
         SetHits( 100, 120 );
         SetSkill( SkillName.Swords, 205.0, 220.0 );
         SetSkill( SkillName.Tactics, 210.0, 220.0 );
         SetSkill( SkillName.Focus, 210.0, 220.0);
         Hidden = true;
         AccessLevel = AccessLevel.GameMaster;

         ColdSicknessSword sick = new ColdSicknessSword();
         sick.Movable = false;
         sick.LootType = LootType.Blessed;
         AddItem( sick );
      }

       public ColdSickness( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();

      }
   }

   public class FluSickness : BaseCreature
      {
      [Constructable]
      public FluSickness():base(AIType.AI_Melee, FightMode.Closest, 30, 1, 0.14, 0.22)
      {
         Body = 0x400;
         BaseSoundID = -1;
         Name = ( "Variole" );
         SetStr( 110, 125 );
         SetDex( 105, 125 );
         SetInt( 20, 35 );
         SetHits( 100, 120 );
         SetSkill( SkillName.Swords, 205.0, 220.0 );
         SetSkill( SkillName.Tactics, 210.0, 220.0 );
         SetSkill( SkillName.Focus, 210.0, 220.0);
         Hidden = true;
         AccessLevel = AccessLevel.GameMaster;

         FluSicknessSword sick = new FluSicknessSword();
         sick.Movable = false;
         sick.LootType = LootType.Blessed;
         AddItem( sick );
      }

       public FluSickness( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();

      }
   }

   public class HeadacheSickness : BaseCreature
      {
      [Constructable]
      public HeadacheSickness():base(AIType.AI_Melee, FightMode.Closest, 30, 1, 0.14, 0.22)
      {
         Body = 0x400;
         BaseSoundID = -1;
         Name = ( "Migr�ne" );
         SetStr( 110, 125 );
         SetDex( 105, 125 );
         SetInt( 20, 35 );
         SetHits( 100, 120 );
         SetSkill( SkillName.Swords, 205.0, 220.0 );
         SetSkill( SkillName.Tactics, 210.0, 220.0 );
         SetSkill( SkillName.Focus, 210.0, 220.0);
         Hidden = true;
         AccessLevel = AccessLevel.GameMaster;

         HeadacheSicknessSword sick = new HeadacheSicknessSword();
         sick.Movable = false;
         sick.LootType = LootType.Blessed;
         AddItem( sick );
      }

       public HeadacheSickness( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();

      }
   }

   public class VirusSickness : BaseCreature
      {
      [Constructable]
      public VirusSickness():base(AIType.AI_Melee, FightMode.Closest, 30, 1, 0.14, 0.22)
      {
         Body = 0x400;
         BaseSoundID = -1;
         Name = ( "Fi�vre Jaune" );
         SetStr( 110, 125 );
         SetDex( 105, 125 );
         SetInt( 20, 35 );
         SetHits( 100, 120 );
         SetSkill( SkillName.Swords, 205.0, 220.0 );
         SetSkill( SkillName.Tactics, 210.0, 220.0 );
         SetSkill( SkillName.Focus, 210.0, 220.0);
         Hidden = true;
         AccessLevel = AccessLevel.GameMaster;

         VirusSicknessSword sick = new VirusSicknessSword();
         sick.Movable = false;
         sick.LootType = LootType.Blessed;
         AddItem( sick );
      }

       public VirusSickness( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();

      }
   }
}
