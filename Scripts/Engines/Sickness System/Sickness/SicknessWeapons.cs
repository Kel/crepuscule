using System;
using Server.Network;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
   [FlipableAttribute( 0xF5E, 0xF5F )]
   public class ColdSicknessSword : BaseSword
   {
      public override int AosStrengthReq{ get{ return 0; } }
      public override int AosMinDamage{ get{ return 0; } }
      public override int AosMaxDamage{ get{ return 0; } }
      public override int AosSpeed{ get{ return 40; } }

      public override int OldStrengthReq{ get{ return 0; } }
      public override int OldMinDamage{ get{ return 0; } }
      public override int OldMaxDamage{ get{ return 0; } }
      public override int OldSpeed{ get{ return 40; } }

      public override WeaponAnimation DefAnimation{ get{ return WeaponAnimation.Slash1H; } }

      [Constructable]
      public ColdSicknessSword() : base( 0xF5E )
      {
         Weight = 0.5;
         Layer = Layer.OneHanded;
         Name = "Sword of the Cold Sickness";
      }

      public override bool AllowEquipedCast( Mobile from )
      {
         return true;
      }

      public ColdSicknessSword( Serial serial ) : base( serial )
      {
      }

      public override void OnHit( Mobile attacker, Mobile defender )
      {
         if ( defender is BaseCreature )
         {
            attacker.Delete();
            return;
         }

               PlayerMobile def = (PlayerMobile) defender;

         if ( def.Sick )
         {
            if ( !attacker.Player )
            {
               attacker.Delete();
            }
            return;
         }

         def.Sickness = SickFlag.Cold;

         if ( attacker.Player == false )
            attacker.Delete();
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   [FlipableAttribute( 0xF5E, 0xF5F )]
   public class FluSicknessSword : BaseSword
   {
      public override int AosStrengthReq{ get{ return 0; } }
      public override int AosMinDamage{ get{ return 0; } }
      public override int AosMaxDamage{ get{ return 0; } }
      public override int AosSpeed{ get{ return 40; } }

      public override int OldStrengthReq{ get{ return 0; } }
      public override int OldMinDamage{ get{ return 0; } }
      public override int OldMaxDamage{ get{ return 0; } }
      public override int OldSpeed{ get{ return 40; } }

      public override WeaponAnimation DefAnimation{ get{ return WeaponAnimation.Slash1H; } }

      [Constructable]
      public FluSicknessSword() : base( 0xF5E )
      {
         Weight = 0.5;
         Layer = Layer.OneHanded;
         Name = "Sword of the Flu Sickness";
      }

      public override bool AllowEquipedCast( Mobile from )
      {
         return true;
      }

      public FluSicknessSword( Serial serial ) : base( serial )
      {
      }

      public override void OnHit( Mobile attacker, Mobile defender )
      {
         if ( defender is BaseCreature )
         {
            attacker.Delete();
            return;
         }

               PlayerMobile def = (PlayerMobile) defender;

         if ( def.Sick )
         {
            if ( !attacker.Player )
            {
               attacker.Delete();
            }
            return;
         }

         def.Sickness = SickFlag.Flu;

         if ( attacker.Player == false )
            attacker.Delete();
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   [FlipableAttribute( 0xF5E, 0xF5F )]
   public class HeadacheSicknessSword : BaseSword
   {
      public override int AosStrengthReq{ get{ return 0; } }
      public override int AosMinDamage{ get{ return 0; } }
      public override int AosMaxDamage{ get{ return 0; } }
      public override int AosSpeed{ get{ return 40; } }

      public override int OldStrengthReq{ get{ return 0; } }
      public override int OldMinDamage{ get{ return 0; } }
      public override int OldMaxDamage{ get{ return 0; } }
      public override int OldSpeed{ get{ return 40; } }

      public override WeaponAnimation DefAnimation{ get{ return WeaponAnimation.Slash1H; } }

      [Constructable]
      public HeadacheSicknessSword() : base( 0xF5E )
      {
         Weight = 0.5;
         Layer = Layer.OneHanded;
         Name = "Sword of the Headache Sickness";
      }

      public override bool AllowEquipedCast( Mobile from )
      {
         return true;
      }

      public HeadacheSicknessSword( Serial serial ) : base( serial )
      {
      }

      public override void OnHit( Mobile attacker, Mobile defender )
      {
         if ( defender is BaseCreature )
         {
            attacker.Delete();
            return;
         }

               PlayerMobile def = (PlayerMobile) defender;

         if ( def.Sick )
         {
            if ( !attacker.Player )
            {
               attacker.Delete();
            }
            return;
         }

         def.Sickness = SickFlag.Headache;

         if ( attacker.Player == false )
            attacker.Delete();
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   [FlipableAttribute( 0xF5E, 0xF5F )]
   public class VirusSicknessSword : BaseSword
   {
      public override int AosStrengthReq{ get{ return 0; } }
      public override int AosMinDamage{ get{ return 0; } }
      public override int AosMaxDamage{ get{ return 0; } }
      public override int AosSpeed{ get{ return 40; } }

      public override int OldStrengthReq{ get{ return 0; } }
      public override int OldMinDamage{ get{ return 0; } }
      public override int OldMaxDamage{ get{ return 0; } }
      public override int OldSpeed{ get{ return 40; } }

      public override WeaponAnimation DefAnimation{ get{ return WeaponAnimation.Slash1H; } }

      [Constructable]
      public VirusSicknessSword() : base( 0xF5E )
      {
         Weight = 0.5;
         Layer = Layer.OneHanded;
         Name = "Sword of the Virus Sickness";
      }

      public override bool AllowEquipedCast( Mobile from )
      {
         return true;
      }

      public VirusSicknessSword( Serial serial ) : base( serial )
      {
      }

      public override void OnHit( Mobile attacker, Mobile defender )
      {
         if ( defender is BaseCreature )
         {
            attacker.Delete();
            return;
         }

               PlayerMobile def = (PlayerMobile) defender;

         if ( def.Sick )
         {
            if ( !attacker.Player )
            {
               attacker.Delete();
            }
            return;
         }

         def.Sickness = SickFlag.Virus;

         if ( attacker.Player == false )
            attacker.Delete();
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
