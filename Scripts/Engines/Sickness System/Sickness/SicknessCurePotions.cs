using System;
using Server;
using Server.Mobiles;
using Server.Items;

namespace Server.Items
{
   public class ColdCurePotion: CrepusculeItem
   {
      [Constructable]
      public ColdCurePotion() : base( 0xF07 )
      {
         Name = "Tuberculose potion de guerison";
         Stackable = false;
         Weight = 1.0;
         Hue = 170;
      }

      public ColdCurePotion( Serial serial ) : base( serial )
      {
      }

      public override void OnDoubleClick( Mobile from )
      {
         PlayerMobile pmfrom = (PlayerMobile) from;

         if ( !Movable )
            return;

         if ( from.InRange( this.GetWorldLocation(), 1 ) )
         {
            if ( HasFreeHand( from ) )
            {
               if ( pmfrom.Sickness != SickFlag.Cold )
               {
                  from.SendMessage( "Vous n'avez pas de tuberculose" );
                  return;
               }
               else
               {
                  pmfrom.Sick = false;
                  pmfrom.SickContagious = false;

                  from.RevealingAction();

                  from.PlaySound( 0x2D6 );
                  from.AddToBackpack( new Bottle() );
                  from.FixedEffect( 0x373A, 10, 15 );
                  from.PlaySound( 0x1E0 );

                  if ( from.Body.IsHuman /*&& !from.Mounted*/ )
                     from.Animate( 34, 5, 1, true, false, 0 );

                  this.Delete();
               }
            }
            else
            {
               from.SendLocalizedMessage( 502172 ); // You must have a free hand to drink a potion.
            }
         }
         else
         {
            from.SendLocalizedMessage( 502138 ); // That is too far away for you to use
         }
      }

      private static bool HasFreeHand( Mobile m )
      {
         Item handOne = m.FindItemOnLayer( Layer.OneHanded );
         Item handTwo = m.FindItemOnLayer( Layer.TwoHanded );

         if ( handTwo is BaseWeapon )
            handOne = handTwo;

         return ( handOne == null || handTwo == null );
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   public class FluCurePotion: CrepusculeItem
   {
      [Constructable]
      public FluCurePotion() : base( 0xF07 )
      {
         Name = "Variole potion de guerison";
         Stackable = false;
         Weight = 1.0;
         Hue = 180;
      }

      public FluCurePotion( Serial serial ) : base( serial )
      {
      }

      public override void OnDoubleClick( Mobile from )
      {
         PlayerMobile pmfrom = (PlayerMobile) from;

         if ( !Movable )
            return;

         if ( from.InRange( this.GetWorldLocation(), 1 ) )
         {
            if ( HasFreeHand( from ) )
            {
               if ( pmfrom.Sickness != SickFlag.Flu )
               {
                  from.SendMessage( "Vous n'avez pas de Variole!" );
                  return;
               }
               else
               {
                  pmfrom.Sick = false;
                  pmfrom.SickContagious = false;

                  from.RevealingAction();

                  from.PlaySound( 0x2D6 );
                  from.AddToBackpack( new Bottle() );
                  from.FixedEffect( 0x373A, 10, 15 );
                  from.PlaySound( 0x1E0 );

                  if ( from.Body.IsHuman /*&& !from.Mounted*/ )
                     from.Animate( 34, 5, 1, true, false, 0 );

                  this.Delete();
               }
            }
            else
            {
               from.SendLocalizedMessage( 502172 ); // You must have a free hand to drink a potion.
            }
         }
         else
         {
            from.SendLocalizedMessage( 502138 ); // That is too far away for you to use
         }
      }

      private static bool HasFreeHand( Mobile m )
      {
         Item handOne = m.FindItemOnLayer( Layer.OneHanded );
         Item handTwo = m.FindItemOnLayer( Layer.TwoHanded );

         if ( handTwo is BaseWeapon )
            handOne = handTwo;

         return ( handOne == null || handTwo == null );
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   public class HeadacheCurePotion: CrepusculeItem
   {
      [Constructable]
      public HeadacheCurePotion() : base( 0xF07 )
      {
         Name = "Migrene potion de guerison";
         Stackable = false;
         Weight = 1.0;
         Hue = 140;
      }

      public HeadacheCurePotion( Serial serial ) : base( serial )
      {
      }

      public override void OnDoubleClick( Mobile from )
      {
         PlayerMobile pmfrom = (PlayerMobile) from;

         if ( !Movable )
            return;

         if ( from.InRange( this.GetWorldLocation(), 1 ) )
         {
            if ( HasFreeHand( from ) )
            {
               if ( pmfrom.Sickness != SickFlag.Headache )
               {
                  from.SendMessage( "Vous n'avez pas de migrene" );
                  return;
               }
               else
               {
                  pmfrom.Sick = false;
                  pmfrom.SickContagious = false;

                  from.RevealingAction();

                  from.PlaySound( 0x2D6 );
                  from.AddToBackpack( new Bottle() );
                  from.FixedEffect( 0x373A, 10, 15 );
                  from.PlaySound( 0x1E0 );

                  if ( from.Body.IsHuman /*&& !from.Mounted*/ )
                     from.Animate( 34, 5, 1, true, false, 0 );

                  this.Delete();
               }
            }
            else
            {
               from.SendLocalizedMessage( 502172 ); // You must have a free hand to drink a potion.
            }
         }
         else
         {
            from.SendLocalizedMessage( 502138 ); // That is too far away for you to use
         }
      }

      private static bool HasFreeHand( Mobile m )
      {
         Item handOne = m.FindItemOnLayer( Layer.OneHanded );
         Item handTwo = m.FindItemOnLayer( Layer.TwoHanded );

         if ( handTwo is BaseWeapon )
            handOne = handTwo;

         return ( handOne == null || handTwo == null );
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   public class VirusCurePotion: CrepusculeItem
   {
      [Constructable]
      public VirusCurePotion() : base( 0xF07 )
      {
         Name = "Fi�vre Jaune potion de guerison";
         Stackable = false;
         Weight = 1.0;
         Hue = 150;
      }

      public VirusCurePotion( Serial serial ) : base( serial )
      {
      }

      public override void OnDoubleClick( Mobile from )
      {
         PlayerMobile pmfrom = (PlayerMobile) from;

         if ( !Movable )
            return;

         if ( from.InRange( this.GetWorldLocation(), 1 ) )
         {
            if ( HasFreeHand( from ) )
            {
               if ( pmfrom.Sickness != SickFlag.Virus )
               {
                  from.SendMessage( "Vous n'avez pas de Fi�vre Jaune" );
                  return;
               }
               else
               {
                  pmfrom.Sick = false;
                  pmfrom.SickContagious = false;

                  from.RevealingAction();

                  from.PlaySound( 0x2D6 );
                  from.AddToBackpack( new Bottle() );
                  from.FixedEffect( 0x373A, 10, 15 );
                  from.PlaySound( 0x1E0 );

                  if ( from.Body.IsHuman /*&& !from.Mounted*/ )
                     from.Animate( 34, 5, 1, true, false, 0 );

                  this.Delete();
               }
            }
            else
            {
               from.SendLocalizedMessage( 502172 ); // You must have a free hand to drink a potion.
            }
         }
         else
         {
            from.SendLocalizedMessage( 502138 ); // That is too far away for you to use
         }
      }

      private static bool HasFreeHand( Mobile m )
      {
         Item handOne = m.FindItemOnLayer( Layer.OneHanded );
         Item handTwo = m.FindItemOnLayer( Layer.TwoHanded );

         if ( handTwo is BaseWeapon )
            handOne = handTwo;

         return ( handOne == null || handTwo == null );
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }

   public class VampTurnCurePotion: CrepusculeItem
   {
      [Constructable]
      public VampTurnCurePotion() : base( 0xF07 )
      {
         Name = "Inf�ction vampirique potion de guerison";
         Stackable = false;
         Weight = 1.0;
         Hue = 160;
      }

      public VampTurnCurePotion( Serial serial ) : base( serial )
      {
      }

      public override void OnDoubleClick( Mobile from )
      {
         PlayerMobile pmfrom = (PlayerMobile) from;

         if ( !Movable )
            return;

         if ( from.InRange( this.GetWorldLocation(), 1 ) )
         {
            if ( HasFreeHand( from ) )
            {
               if ( pmfrom.Sickness != SickFlag.VampTurn )
               {
                  from.SendMessage( "Vous n'avez pas d'infection vampirique!" );
                  return;
               }
               else
               {
                  pmfrom.Sickness = SickFlag.None;
                  pmfrom.SickContagious = false;

                  from.RevealingAction();

                  from.PlaySound( 0x2D6 );
                  from.AddToBackpack( new Bottle() );
                  from.FixedEffect( 0x373A, 10, 15 );
                  from.PlaySound( 0x1E0 );

                  if ( from.Body.IsHuman /*&& !from.Mounted*/ )
                     from.Animate( 34, 5, 1, true, false, 0 );

                  this.Delete();
               }
            }
            else
            {
               from.SendLocalizedMessage( 502172 ); // You must have a free hand to drink a potion.
            }
         }
         else
         {
            from.SendLocalizedMessage( 502138 ); // That is too far away for you to use
         }
      }

      private static bool HasFreeHand( Mobile m )
      {
         Item handOne = m.FindItemOnLayer( Layer.OneHanded );
         Item handTwo = m.FindItemOnLayer( Layer.TwoHanded );

         if ( handTwo is BaseWeapon )
            handOne = handTwo;

         return ( handOne == null || handTwo == null );
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
