using System;
using Server;
using Server.Gumps;
using Server.Items;
using Server.Mobiles;
using Server.Enchanting.Items;
using Server.Engines;

namespace Server.Enchanting
{
	public class EnchantingHelper
	{
		public static readonly bool ArtifactCraftable = false;
		public static readonly bool BraceletsRingsOnly = true;
		public static readonly int MaxPropsAllowed = 5; // 5
		public static readonly double ExplodeChance = 0.35; // .35
		public static readonly double DestroyChance = 0.15; // .45
		public static readonly int ExplodeMinDmg = 20;
		public static readonly int ExplodeMaxDmg = 40;

		public static readonly bool[] Enabled = new bool[]
		{
			// true if the spellcraft is enabled
			true,// str bonus = 0,
			true,// dex bonus = 1,
			true,// int bonus = 2,
			true,// hp bonus = 3,
			true,// stam bonus = 4,
			true,// mana bonus = 5,
			true,// physical resist = 6,
			true,// fire resist = 7,
			true,// cold resist = 8,
			true,// poison resist = 9,
			true,// energy resist = 10,
			true,// hit point regeneration = 11,
			true,// mana regeneration = 12,
			true,// stamina regeneration = 13,
			true,// faster cast recovery = 14,
			true,// faster cast speed = 15,
			true,// lower mana cost = 16,
			true,// lower reagent cost = 17,
			true,// mage armor = 18,
			true,// mage weapon = 19,
			true,// spell channeling = 20,
			true,// spell damage increase = 21,
			true,// hit cold area = 22,
			true,// hit energy area = 23,
			true,// hit fire area = 24,
			true,// hit physical area = 25,
			true,// hit poison area = 26,
			true,// hit dispel = 27,
			true,// hit fireball = 28,
			true,// hit harm = 29,
			true,// hit lightning = 30,
			true,// hit magic arrow = 31,
			true,// hit lower attack = 32,
			true,// hit lower defense = 33,
			true,// hit leech hits = 34,
			true,// hit leech mana = 35,
			true,// hit leech stamina = 36,
			true,// use best weapon skill = 37,
			true,// weapon damage increase = 38,
			true,// swing speed increase = 39,
			true,// hit chance increase = 40,
			true,// defense chance increase = 41,
			true,// enhance potions = 42,
			true,// lower stat requirements = 43,
			true,// luck = 44,
			true,// reflect physical = 45,
			true,// self repair = 46,
			false,// durable = 47,
			false,// substantial = 48,
			false,// massive = 49,
			false,// fortified = 50,
			false,// indestructible = 51,
			false,// ruin = 52,
			false,// might = 53,
			false,// force = 54,
			false,// power = 55,
			false,// vanquishing = 56,
			false,// accurate = 57,
			false,// surpassingly accurate = 58,
			false,// eminently accurate = 59,
			false,// exceedingly accurate = 60,
			false,// supremely accurate = 61,
			false,// defense = 62,
			false,// guarding = 63,
			false,// hardening = 64,
			false,// fortification = 65,
			false,// invulnerability = 66,
		};

		public static Type[] m_Loot = new Type[]
		{
			typeof( BonusStrJewel ), 	typeof( BonusDexJewel ), 	typeof( BonusIntJewel ),
			typeof( BonusHitsJewel ),	typeof( BonusStamJewel ),	typeof( BonusManaJewel ),
			typeof( PhysicalResistJewel ),	typeof( FireResistJewel ),	typeof( ColdResistJewel ),
			typeof( PoisonResistJewel ),	typeof( EnergyResistJewel ),	typeof( RegenHitsJewel ),
			typeof( RegenManaJewel ),	typeof( RegenStamJewel ),	typeof( CastRecoveryJewel ),
			typeof( CastSpeedJewel ),	typeof( LowerManaCostJewel ),	typeof( LowerRegCostJewel ),
			typeof( MageArmorJewel ),	typeof( MageWeaponJewel ),	typeof( SpellChannelingJewel ),
			typeof( SpellDamageJewel ),	typeof( HitColdAreaJewel ),	typeof( HitEnergyAreaJewel ),
			typeof( HitFireAreaJewel ),	typeof( HitPhysicalAreaJewel ),	typeof( HitPoisonAreaJewel ),
			typeof( HitDispelJewel ),	typeof( HitFireballJewel ),	typeof( HitHarmJewel ),
			typeof( HitLightningJewel ),	typeof( HitMagicArrowJewel ),	typeof( HitLowerAttackJewel ),
			typeof( HitLowerDefendJewel ),	typeof( HitLeechHitsJewel ),	typeof( HitLeechManaJewel ),
			typeof( HitLeechStamJewel ),	typeof( UseBestSkillJewel ),	typeof( WeaponDamageJewel ),
			typeof( WeaponSpeedJewel ),	typeof( AttackChanceJewel ),	typeof( DefendChanceJewel ),
			typeof( EnhancePotionsJewel ),	typeof( LowerStatReqJewel ),	typeof( LuckJewel ),
			typeof( ReflectPhysicalJewel ),	typeof( SelfRepairJewel ),	typeof( DurableJewel ),
			typeof( SubstantialJewel ),	typeof( MassiveJewel ),		typeof( FortifiedJewel ),
			typeof( IndestructibleJewel ),	typeof( RuinJewel ),		typeof( MightJewel ),
			typeof( ForceJewel ),		typeof( PowerJewel ),		typeof( VanqJewel ),
			typeof( AccurateJewel ),	typeof( SurpassinglyJewel ),	typeof( EminentlyJewel ),
			typeof( ExceedinglyJewel ),	typeof( SupremelyJewel ),	typeof( DefenseJewel ),
			typeof( GuardingJewel ),	typeof( HardeningJewel ),	typeof( FortificationJewel )
		};

		public static Item RandomCraft( double chance )
		{
			if ( Utility.RandomDouble() <= chance )
			{
				int val = Utility.Random( Core.AOS ? 47 : 20 ) + ((Core.AOS) ? 0 : 47);

				Item craft = Activator.CreateInstance( m_Loot[val] ) as Item;
                if (craft != null && GameItemType.FindType(craft.GetType()) != null)
                    GameItemType.FindType(craft.GetType()).SetToItem(craft);

				return craft;
			}
			else
			{
				return null;
			}
		}

        public static void FinalizeEnchantment(object item,int enchantmentID)
        {
            if (item is CrepusculeItem)
            {
                (item as CrepusculeItem).Enchanted = true;
                (item as CrepusculeItem).EnchantmentID = enchantmentID;
            }
        }

		public static int Scale( int min, int max, int low, int high )
		{
			return low + AOS.Scale( high-low, Utility.RandomMinMax( min, max ) );
		}

		public static void ApplyAttribute( AosAttributes attrs, int min, int max, AosAttribute attr, int low, int high )
		{
			ApplyAttribute( attrs, min, max, attr, low, high, 1 );
		}

		public static void ApplyAttribute( AosAttributes attrs, int min, int max, AosAttribute attr, int low, int high, int scale )
		{
			if ( attr == AosAttribute.CastSpeed )
				attrs[attr] += Scale( min, max, low / scale, high / scale ) * scale;
			else
				attrs[attr] = Scale( min, max, low / scale, high / scale ) * scale;

			if ( attr == AosAttribute.SpellChanneling )
				attrs[AosAttribute.CastSpeed] -= 1;
		}

		public static void ApplyAttribute( AosArmorAttributes attrs, int min, int max, AosArmorAttribute attr, int low, int high )
		{
			attrs[attr] = Scale( min, max, low, high );
		}

		public static void ApplyAttribute( AosArmorAttributes attrs, int min, int max, AosArmorAttribute attr, int low, int high, int scale )
		{
			attrs[attr] = Scale( min, max, low / scale, high / scale ) * scale;
		}

		public static void ApplyAttribute( AosWeaponAttributes attrs, int min, int max, AosWeaponAttribute attr, int low, int high )
		{
			attrs[attr] = Scale( min, max, low, high );
		}

		public static void ApplyAttribute( AosWeaponAttributes attrs, int min, int max, AosWeaponAttribute attr, int low, int high, int scale )
		{
			attrs[attr] = Scale( min, max, low / scale, high / scale ) * scale;
		}

		public static void ApplyAttribute( AosElementAttributes attrs, int min, int max, AosElementAttribute attr, int low, int high )
		{
			attrs[attr] = Scale( min, max, low, high );
		}

		public static void ApplyAttribute( AosElementAttributes attrs, int min, int max, AosElementAttribute attr, int low, int high, int scale )
		{
			attrs[attr] = Scale( min, max, low / scale, high / scale ) * scale;
		}

		public static void ApplyResistance( BaseArmor ar, int min, int max, ResistanceType res, int low, int high )
		{
			switch ( res )
			{
				case ResistanceType.Physical: ar.PhysicalBonus += Scale( min, max, low, high ); break;
				case ResistanceType.Fire: ar.FireBonus += Scale( min, max, low, high ); break;
				case ResistanceType.Cold: ar.ColdBonus += Scale( min, max, low, high ); break;
				case ResistanceType.Poison: ar.PoisonBonus += Scale( min, max, low, high ); break;
				case ResistanceType.Energy: ar.EnergyBonus += Scale( min, max, low, high ); break;
			}
		}

		public static bool CheckSpellCrafted( Mobile m, object item )
		{
			bool result;

            if (item is CrepusculeItem && (item as CrepusculeItem).Enchanted)
            {
                m.SendMessage("Cet objet est d�j� enchant�.");
                result = false;
            }
            else if (item is BaseArmor)
                result = CheckSpellCrafted(m, (BaseArmor)item);
            else if (item is BaseWeapon)
                result = CheckSpellCrafted(m, (BaseWeapon)item);
            else if (item is BaseJewel)
                result = CheckSpellCrafted(m, (BaseJewel)item);
            else
                result = false;

			return result;
		}

		public static bool CheckSpellCrafted( Mobile m, BaseArmor armor )
		{
			if ( !armor.IsChildOf( m ) )
			{
				m.SendMessage( "Vous ne pouvez enchanter que les objets qui sont dans votre sac." );
				return false;
			}

			if ( Core.AOS )
			{
				if ( !ArtifactCraftable && armor.ArtifactRarity > 0 )
				{
					m.SendMessage( "Vous ne pouvez pas enchanter ceci." );
					return false;
				}

				int props = 0;

				foreach( int i in Enum.GetValues(typeof( AosAttribute)) )
					if ( armor.Attributes[(AosAttribute)i] > 0 ) ++props;

				foreach( int i in Enum.GetValues(typeof( AosArmorAttribute)) )
					if ( armor.ArmorAttributes[(AosArmorAttribute)i] > 0 ) ++props;

				if ( props >= MaxPropsAllowed )
				{
					m.SendMessage( "Vous ne pouvez plus enchanter ceci." );
					return false;
				}
			}

			if ( !CheckExplosion( m, armor ) )
				return false;

			if ( !m.Backpack.ConsumeTotal( typeof( MagicJewel ), 1 ) )
			{
				m.SendMessage( "Vous n'avez pas assez de gemmes arcaniques." );
				return false;
			}

			return true;
		}

		public static bool CheckSpellCrafted( Mobile m, BaseShield shield )
		{
			if ( !shield.IsChildOf( m ) )
			{
				m.SendMessage( "Vous ne pouvez enchanter que les objets qui sont dans votre sac." );
				return false;
			}

			if ( Core.AOS )
			{
				if ( !ArtifactCraftable && shield.ArtifactRarity > 0 )
				{
					m.SendMessage( "Vous ne pouvez pas enchanter ceci." );
					return false;
				}

				int props = 0;

				foreach( int i in Enum.GetValues(typeof( AosAttribute)) )
					if ( shield.Attributes[(AosAttribute)i] > 0 ) ++props;

				foreach( int i in Enum.GetValues(typeof( AosArmorAttribute)) )
					if ( shield.ArmorAttributes[(AosArmorAttribute)i] > 0 ) ++props;

				if ( props >= MaxPropsAllowed )
				{
					m.SendMessage( "Vous ne pouvez plus enchanter ceci." );
					return false;
				}
			}

			if ( !CheckExplosion( m, shield ) )
				return false;

			if ( !m.Backpack.ConsumeTotal( typeof( MagicJewel ), 1 ) )
			{
				m.SendMessage( "Vous n'avez pas assez de gemmes arcaniques." );
				return false;
			}

			return true;
		}

		public static bool CheckSpellCrafted( Mobile m, BaseWeapon weapon )
		{
			if ( !weapon.IsChildOf( m ) )
			{
				m.SendMessage( "Vous ne pouvez enchanter que les objets qui sont dans votre sac." );
				return false;
			}

			if ( Core.AOS )
			{
				int props = 0;

				if ( !ArtifactCraftable && weapon.ArtifactRarity > 0 )
				{
					m.SendMessage( "Vous ne pouvez pas enchanter ceci." );
					return false;
				}

				foreach( int i in Enum.GetValues(typeof( AosAttribute)) )
					if ( weapon.Attributes[(AosAttribute)i] > 0 ) ++props;

				foreach( int i in Enum.GetValues(typeof( AosWeaponAttribute)) )
					if ( weapon.WeaponAttributes[(AosWeaponAttribute)i] > 0 ) ++props;

				if ( props >= MaxPropsAllowed )
				{
					m.SendMessage( "Vous ne pouvez plus enchanter ceci." );
					return false;
				}
			}

			if ( !CheckExplosion( m, weapon ) )
				return false;

			if ( !m.Backpack.ConsumeTotal( typeof( MagicJewel ), 1 ) )
			{
				m.SendMessage( "Vous n'avez pas assez de gemmes arcaniques." );
				return false;
			}

			return true;
		}

		public static bool CheckSpellCrafted( Mobile m, BaseJewel jewel )
		{
			if ( !jewel.IsChildOf( m ) )
			{
				m.SendMessage( "Vous ne pouvez enchanter que les objets qui sont dans votre sac." );
				return false;
			}

			if ( Core.AOS )
			{
				int props = 0;

				if ( !( jewel is BaseBracelet || jewel is BaseRing ) && BraceletsRingsOnly )
				{
					m.SendMessage( "You can only spellcraft jewelry that are bracelets or rings." );
					return false;
				} 
				if ( !ArtifactCraftable && jewel.ArtifactRarity > 0 )
				{
					m.SendMessage( "Vous ne pouvez pas enchanter ceci." );
					return false;
				}

				foreach( int i in Enum.GetValues(typeof( AosAttribute)) )
					if ( jewel.Attributes[(AosAttribute)i] > 0 ) ++props;

				foreach( int i in Enum.GetValues(typeof( AosElementAttribute)) )
					if ( jewel.Resistances[(AosElementAttribute)i] > 0 ) ++props;

				if ( props >= MaxPropsAllowed )
				{
					m.SendMessage( "Vous ne pouvez plus enchanter ceci." );
					return false;
				}
			}

			if ( !CheckExplosion( m, jewel ) )
				return false;

			if ( !m.Backpack.ConsumeTotal( typeof( MagicJewel ), 1 ) )
			{
				m.SendMessage( "Vous n'avez pas assez de gemmes arcaniques." );
				return false;
			}

			return true;
		}

		public static bool CheckExplosion( Mobile from, Item target )
		{
			if ( Utility.RandomDouble() <= ExplodeChance )
			{
				from.FixedParticles( 0x36BD, 20, 10, 5044, EffectLayer.Head );
				from.PlaySound( 0x307 );
				from.Damage( Utility.RandomMinMax( ExplodeMinDmg, ExplodeMaxDmg ) );

				if ( Utility.RandomDouble() <= DestroyChance )
				{
					target.Delete();
					from.SendMessage( "Les flux magiques se d�ch�nent et l'objet est d�truit." );
				}
				else
				{
                    from.SendMessage("Les flux magiques se d�ch�nent, mais l'objet reste intact.");
				}
				return false;
			}
			return true;
		}
	}
}