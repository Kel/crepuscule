using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class BaseEnchantingItem : CrepusculeItem
	{
		protected int m_CraftID;

		[CommandProperty( AccessLevel.GameMaster )]
		public int CraftID
		{
			get { return m_CraftID; } 
			set { m_CraftID = value; InvalidateProperties(); }
		}

		public BaseEnchantingItem( int amount, int craft ) : base( 0xF26 )
		{
			Name = "Joyau d'enchantement";
			Stackable = false;
			Weight = 0.1;
			Amount = amount;
			CraftID = craft;
		}

		public BaseEnchantingItem( Serial serial ) : base( serial )
		{
		}

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties(list);

			if ( m_CraftID > 46 )
				list.Add( 1050045," \tNon Aos Only Craft\t " );

			if ( m_CraftID == 0 )
				list.Add( 1050045," \tEnchantement de force\t " );

			if ( m_CraftID == 1 )
                list.Add(1050045, " \tEnchantement de dexterit�\t ");

			if ( m_CraftID == 2 )
                list.Add(1050045, " \tEnchantement de l'intelligence\t ");

			if ( m_CraftID == 3 )
                list.Add(1050045, " \tEnchantement de constitution\t ");

			if ( m_CraftID == 4 )
                list.Add(1050045, " \tEnchantement de l'endurance\t ");

			if ( m_CraftID == 5 )
                list.Add(1050045, " \tEnchantement de flux magique\t ");

			if ( m_CraftID == 6 )
                list.Add(1050045, " \tEnchantement de r�sistance\t ");

			if ( m_CraftID == 7 )
                list.Add(1050045, " \tEnchantement de prot�ction de feu\t ");

			if ( m_CraftID == 8 )
                list.Add(1050045, " \tEnchantement de prot�ction de froid\t ");

			if ( m_CraftID == 9 )
                list.Add(1050045, " \tEnchantement de prot�ction de poison\t ");

			if ( m_CraftID == 10 )
                list.Add(1050045, " \tEnchantement de prot�ction de l'�nergie\t ");

			if ( m_CraftID == 11 )
                list.Add(1050045, " \tEnchantement de m�tabolisme\t ");

			if ( m_CraftID == 12 )
                list.Add(1050045, " \tEnchantement de r�g�n�ration magique\t ");

			if ( m_CraftID == 13 )
                list.Add(1050045, " \tEnchantement de respiration\t ");

			if ( m_CraftID == 14 )
                list.Add(1050045, " \tEnchantement de concentration\t ");

			if ( m_CraftID == 15 )
                list.Add(1050045, " \tEnchantement de rapidit� magique\t ");

			if ( m_CraftID == 16 )
                list.Add(1050045, " \tEnchantement de co�t magique\t ");

			if ( m_CraftID == 17 )
                list.Add(1050045, " \tEnchantement de prix magique\t ");

			if ( m_CraftID == 18 )
                list.Add(1050045, " \tEnchantement de l'armure magique\t ");

			if ( m_CraftID == 19 )
                list.Add(1050045, " \tEnchantement de l'arme magique\t ");

			if ( m_CraftID == 20 )
                list.Add(1050045, " \tEnchantement de flux arcaniques\t ");

			if ( m_CraftID == 21 )
                list.Add(1050045, " \tEnchantement des d�gats arcaniques\t ");

			if ( m_CraftID == 22 )
                list.Add(1050045, " \tEnchantement de zone de froid\t ");

			if ( m_CraftID == 23 )
                list.Add(1050045, " \tEnchantement de zone de l'energie\t ");

			if ( m_CraftID == 24 )
                list.Add(1050045, " \tEnchantement de zone de feu\t ");

			if ( m_CraftID == 25 )
                list.Add(1050045, " \tEnchantement de zone\t ");

			if ( m_CraftID == 26 )
                list.Add(1050045, " \tEnchantement de zone de poison\t ");

			if ( m_CraftID == 27 )
                list.Add(1050045, " \tEnchantement de dissipation\t ");

			if ( m_CraftID == 28 )
                list.Add(1050045, " \tEnchantement de boule de feu\t ");

			if ( m_CraftID == 29 )
                list.Add(1050045, " \tEnchantement de blessure\t ");

			if ( m_CraftID == 30 )
				list.Add( 1050045," \tEnchantement de l'�clair\t " );

			if ( m_CraftID == 31 )
                list.Add(1050045, " \tEnchantement de fl�che magique\t ");

			if ( m_CraftID == 32 )
                list.Add(1050045, " \tEnchantement d'affaiblissement d'attaque\t ");

			if ( m_CraftID == 33 )
                list.Add(1050045, " \tEnchantement d'affaiblissement de defense\t ");

			if ( m_CraftID == 34 )
                list.Add(1050045, " \tEnchantement de vampirisation\t ");

			if ( m_CraftID == 35 )
                list.Add(1050045, " \tEnchantement de drain de mana\t ");

			if ( m_CraftID == 36 )
                list.Add(1050045, " \tEnchantement de drain de fatigue\t ");

			if ( m_CraftID == 37 )
                list.Add(1050045, " \tEnchantement d'utilisation d'arme\t ");

			if ( m_CraftID == 38 )
                list.Add(1050045, " \tEnchantement de d�gats d'arme\t ");

			if ( m_CraftID == 39 )
                list.Add(1050045, " \tEnchantement de rapidit� d'arme\t ");

			if ( m_CraftID == 40 )
                list.Add(1050045, " \tEnchantement de chance offensive\t ");

			if ( m_CraftID == 41 )
                list.Add(1050045, " \tEnchantement de chance deffensive\t ");

			if ( m_CraftID == 42 )
                list.Add(1050045, " \tEnchantement des potions\t ");

			if ( m_CraftID == 43 )
                list.Add(1050045, " \tEnchantement des pr�requis\t ");

			if ( m_CraftID == 44 )
                list.Add(1050045, " \tEnchantement de chance\t ");

			if ( m_CraftID == 45 )
                list.Add(1050045, " \tEnchantement de reflexion physique\t ");

			if ( m_CraftID == 46 )
                list.Add(1050045, " \tEnchantement d'auto-r�paration\t ");
		}

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);
            from.SendMessage("Choisissez le livre d'enchantement");
            from.Target = new InternalTarget(this);
        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
			writer.Write( m_CraftID );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
			m_CraftID = reader.ReadInt();
		}

        #region InternalTarget
        private class InternalTarget : Target
        {
            private BaseEnchantingItem m_Owner;

            public InternalTarget(BaseEnchantingItem owner)
                : base(12, false, TargetFlags.None)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is BookOfSpellCrafts)
                {
                    (o as BookOfSpellCrafts).OnDragDrop(from, m_Owner);
                }
            }

            protected override void OnTargetFinish(Mobile from)
            {

            }
        }
        #endregion
	}
}