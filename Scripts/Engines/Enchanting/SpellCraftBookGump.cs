using System;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Items;
using Server.Enchanting;
using Server.Enchanting.Items;
using Server.Enchanting.Crafts;
using Server.Targeting;

namespace Server.Enchanting.Gumps
{
	public class SpellCraftBook : Gump
	{
		private Mobile m_Owner;
		private int gumpX, gumpY, m_Count, m_Page;

		public SpellCraftBook( Mobile owner, BookOfSpellCrafts book ) : base( 10, 10 )
		{
			owner.CloseGump( typeof( SpellCraftBook ) );

			m_Owner = owner;

			Closable = true;
			Disposable = true;
			Dragable = true;
			Resizable = false;

			AddImage( 50, 50, 0x1F4 );
			AddHtml( 76, 60, 185, 20, "<center>Grimoire d'enchantement</center", false, false );
			AddButton( 80, 85, 0x1523, 0x1523, 1, GumpButtonType.Page, 1 );
			AddLabel( 95, 85, 0, Core.AOS ? "Personnels" : "Durability Crafts" );
			AddButton( 80, 105, 0x1523, 0x1523, 2, GumpButtonType.Page, 2 );
			AddLabel( 95, 105, 0, Core.AOS ? "R�sistances" : "Weapon Damage Crafts" );
			AddButton( 80, 125, 0x1523, 0x1523, 3, GumpButtonType.Page, 3 );
			AddLabel( 95, 125, 0, Core.AOS ? "R�g�nerations" : "Weapon Accuracy Crafts" );
			AddButton( 80, 145, 0x1523, 0x1523, 4, GumpButtonType.Page, 4 );
			AddLabel( 95, 145, 0, Core.AOS ? "Arcaniques" : "Armor Protection Crafts" );

			if ( Core.AOS )
			{
				AddButton( 80, 165, 0x1523, 0x1523, 5, GumpButtonType.Page, 5 );
				AddLabel( 95, 165, 0, "Armes"  );
				AddButton( 80, 185, 0x1523, 0x1523, 8, GumpButtonType.Page, 8 );
				AddLabel( 95, 185, 0, "Autres" );
			}

			AddLabel( 80, 230, 0, "Total enchantements:" );
			AddHtml( 205, 230, 40, 20, String.Format( "<div align=right>{0}</div>", book.Count ), false, false );

			AddPage( 1 );

			if ( Core.AOS )
                AddHtml(262, 60, 185, 20, "<center>Personnels</center>", false, false);
			
			m_Page = 1;
			gumpX = 275; gumpY = 85;

			if ( Core.AOS )
			{
				if( book.HasCraft( 0 ) )
				{
					AddCraft( gumpX, gumpY, 7, "Force" );
					gumpY += 20;
				}

				if( book.HasCraft( 1 ) )
				{
                    AddCraft(gumpX, gumpY, 8, "Dexterit�");
					gumpY += 20;
				}

				if( book.HasCraft( 2 ) )
				{
					AddCraft( gumpX, gumpY, 9, "Intelligence" );
					gumpY += 20;
				}

				if( book.HasCraft( 3 ) )
				{
                    AddCraft(gumpX, gumpY, 10, "Constitution");
					gumpY += 20;
				}

				if( book.HasCraft( 4 ) )
				{
                    AddCraft(gumpX, gumpY, 11, "Endurance");
					gumpY += 20;
				}

				if( book.HasCraft( 5 ) )
				{
					AddCraft( gumpX, gumpY, 12, "Flux magique" );
					gumpY += 20;
				}
			}

			AddPage( 2 );

			if ( Core.AOS )
                AddHtml(262, 60, 185, 20, "<center>R�sistances</center>", false, false);
			
			gumpX = 275; gumpY = 85;
			m_Page = 2;

			if ( Core.AOS )
			{
				if( book.HasCraft( 6 ) )
				{
                    AddCraft(gumpX, gumpY, 13, "R�sistance");
					gumpY += 20;
				}

				if( book.HasCraft( 7 ) )
				{
                    AddCraft(gumpX, gumpY, 14, "Prot�ction de feu");
					gumpY += 20;
				}

				if( book.HasCraft( 8 ) )
				{
                    AddCraft(gumpX, gumpY, 15, "Prot�ction de froid");
					gumpY += 20;
				}

				if( book.HasCraft( 9 ) )
				{
                    AddCraft(gumpX, gumpY, 16, "Prot�ction de poison");
					gumpY += 20;
				}

				if( book.HasCraft( 10 ) )
				{
                    AddCraft(gumpX, gumpY, 17, "Prot�ction de l'�nergie");
					gumpY += 20;
				}
			}
			

			AddPage( 3 );

			if ( Core.AOS )
				AddHtml( 262, 60, 185, 20, "<center>R�g�neration</center>", false, false );
			
			gumpX = 275; gumpY = 85;
			m_Page = 3;

			if ( Core.AOS )
			{
				if( book.HasCraft( 11 ) )
				{
                    AddCraft(gumpX, gumpY, 18, "M�tabolisme");
					gumpY += 20;
				}

				if( book.HasCraft( 12 ) )
				{
                    AddCraft(gumpX, gumpY, 19, "R�g�n�ration magique");
					gumpY += 20;
				}

				if( book.HasCraft( 13 ) )
				{
                    AddCraft(gumpX, gumpY, 20, "Respiration");
					gumpY += 20;
				}
			}
			

			AddPage( 4 );

			if ( Core.AOS )
                AddHtml(262, 60, 185, 20, "<center>Arcaniques</center>", false, false);
			
			gumpX = 275; gumpY = 85;
			m_Page = 4;

			if ( Core.AOS )
			{
				if( book.HasCraft( 14 ) )
				{
                    AddCraft(gumpX, gumpY, 21, "Concentration");
					gumpY += 20;
				}

				if( book.HasCraft( 15 ) )
				{
                    AddCraft(gumpX, gumpY, 22, "Rapidit� magique");
					gumpY += 20;
				}

				if( book.HasCraft( 16 ) )
				{
                    AddCraft(gumpX, gumpY, 23, "Co�t magique");
					gumpY += 20;
				}

				if( book.HasCraft( 17 ) )
				{
                    AddCraft(gumpX, gumpY, 24, "Prix magique");
					gumpY += 20;
				}

//				if( book.HasCraft( 18 ) )
//				{
//                    AddCraft(gumpX, gumpY, 25, "Armure magique");
//					gumpY += 20;
//				}

				if( book.HasCraft( 19 ) )
				{
                    AddCraft(gumpX, gumpY, 26, "Arme magique");
					gumpY += 20;
				}

				if( book.HasCraft( 20 ) )
				{
                    AddCraft(gumpX, gumpY, 27, "Flux arcaniques");
					gumpY += 20;
				}

				if( book.HasCraft( 21 ) )
				{
                    AddCraft(gumpX, gumpY, 28, "D�gats arcaniques");
					gumpY += 20;
				}
			}
			
			if ( Core.AOS )
			{
				AddPage( 5 );
				AddHtml( 262, 60, 185, 20, "<center>Armes</center>", false, false );

				gumpX = 275; gumpY = 85;
				m_Count = 0;
				m_Page = 5;

				if( book.HasCraft( 22 ) )
					{
                        AddCraft(gumpX, gumpY, 29, ++m_Count, m_Page, "Zone de froid");
						gumpY += 20;
				}

				if( book.HasCraft( 23 ) )
				{
                    AddCraft(gumpX, gumpY, 30, ++m_Count, m_Page, "Zone de l'energie");
					gumpY += 20;
				}

				if( book.HasCraft( 24 ) )
				{
                    AddCraft(gumpX, gumpY, 31, ++m_Count, m_Page, "Zone de feu");
					gumpY += 20;
				}

				if( book.HasCraft( 25 ) )
				{
					AddCraft( gumpX, gumpY, 32, ++m_Count, m_Page, "Zone" );
					gumpY += 20;
				}

				if( book.HasCraft( 26 ) )
				{
                    AddCraft(gumpX, gumpY, 33, ++m_Count, m_Page, "Zone de poison");
					gumpY += 20;
				}

				if( book.HasCraft( 27 ) )
				{
                    AddCraft(gumpX, gumpY, 34, ++m_Count, m_Page, "Dissipation");
					gumpY += 20;
				}

				if( book.HasCraft( 28 ) )
				{
                    AddCraft(gumpX, gumpY, 35, ++m_Count, m_Page, "Boule de feu");
					gumpY += 20;
				}
				if( book.HasCraft( 29 ) )
				{
                    AddCraft(gumpX, gumpY, 36, ++m_Count, m_Page, "Blessure");
					gumpY += 20;
				}

				if( book.HasCraft( 30 ) )
				{
					AddCraft( gumpX, gumpY, 37, ++m_Count, m_Page, "Eclair" );
					gumpY += 20;
				}

				if( book.HasCraft( 31 ) )
				{
                    AddCraft(gumpX, gumpY, 38, ++m_Count, m_Page, "Fl�che magique ");
					gumpY += 20;
				}

				if( book.HasCraft( 32 ) )
				{
                    AddCraft(gumpX, gumpY, 39, ++m_Count, m_Page, "Faible attaque");
					gumpY += 20;
				}

				if( book.HasCraft( 33 ) )
				{
                    AddCraft(gumpX, gumpY, 40, ++m_Count, m_Page, "Faible defense");
					gumpY += 20;
				}

				if( book.HasCraft( 34 ) )
				{
                    AddCraft(gumpX, gumpY, 41, ++m_Count, m_Page, "Vampirisation");
					gumpY += 20;
				}

				if( book.HasCraft( 35 ) )
				{
                    AddCraft(gumpX, gumpY, 42, ++m_Count, m_Page, "Drain de mana");
					gumpY += 20;
				}
				if( book.HasCraft( 36 ) )
				{
                    AddCraft(gumpX, gumpY, 43, ++m_Count, m_Page, "Drain de fatigue");
					gumpY += 20;
				}

//				if( book.HasCraft( 37 ) )
//				{
//                    AddCraft(gumpX, gumpY, 44, ++m_Count, m_Page, "Utilisation d'arme");
//					gumpY += 20;
//				}

				if( book.HasCraft( 38 ) )
				{
                    AddCraft(gumpX, gumpY, 45, ++m_Count, m_Page, "D�gats d'arme");
					gumpY += 20;
				}

				if( book.HasCraft( 39 ) )
				{
                    AddCraft(gumpX, gumpY, 46, ++m_Count, m_Page, "Rapidit� d'arme");
					gumpY += 20;
				}

				AddPage( 8 );
				AddHtml( 262, 60, 185, 20, "<center>Autres</center>", false, false );

				gumpX = 275; gumpY = 85;

				if( book.HasCraft( 40 ) )
				{
                    AddCraft(gumpX, gumpY, 47, "Chance offensive");
					gumpY += 20;
				}

				if( book.HasCraft( 41 ) )
				{
                    AddCraft(gumpX, gumpY, 48, "Chance deffensive");
					gumpY += 20;
				}

				if( book.HasCraft( 42 ) )
				{
					AddCraft( gumpX, gumpY, 49, "Potions" );
					gumpY += 20;
				}
				if( book.HasCraft( 43 ) )
				{
					AddCraft( gumpX, gumpY, 50, "Pr�requis" );
					gumpY += 20;
				}

				if( book.HasCraft( 44 ) )
				{
					AddCraft( gumpX, gumpY, 51, "Chance" );
					gumpY += 20;
				}

				if( book.HasCraft( 45 ) )
				{
                    AddCraft(gumpX, gumpY, 52, "Reflexion physique");
					gumpY += 20;
				}

				if( book.HasCraft( 46 ) )
				{
                    AddCraft(gumpX, gumpY, 53, "Auto-r�paration");
					gumpY += 20;
				}
			}		
		}

		public void AddCraft( int x, int y, int buttonID, string text )
		{
			AddButton( gumpX, gumpY, 0x846, 0x845, buttonID, GumpButtonType.Reply, 0 );
			AddLabel( gumpX + 20, gumpY - 2, 0, text );
		}

		public void AddCraft( int x, int y, int buttonID, int count, int currPage, string text )
		{
			if ( count == 8 || count == 15 )
			{
				AddButton( 365, 235, 5601, 5605, 50 + buttonID, GumpButtonType.Page, currPage + 1 );
				AddPage( currPage + 1 );
				AddButton( 340, 235, 5603, 5607, 51 + buttonID, GumpButtonType.Page, currPage );
				m_Page = currPage + 1;
				gumpX = 275; gumpY = 85;
			}

			AddButton( gumpX, gumpY, 0x846, 0x845, buttonID, GumpButtonType.Reply, 0 );
			AddLabel( gumpX + 20, gumpY - 2, 0, text );
		}

		public override void OnResponse( NetState state, RelayInfo info )
		{
			Mobile from = state.Mobile;
			int craft = info.ButtonID - 7;

			if ( craft < 0 )
				return;

			if ( !EnchantingHelper.Enabled[craft] )
			{
				from.SendMessage( "Cet action n'est pas disponible pour le moment." );
				return;
			}
		
			from.SendMessage( "Choisissez l'objet � enchanter." );

			switch( craft )
			{
				case 0:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( BonusStr.Callback ) ); break;
				case 1:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( BonusDex.Callback ) ); break;
				case 2:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( BonusInt.Callback ) ); break;
				case 3:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( BonusHits.Callback ) ); break;
				case 4:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( BonusStam.Callback ) ); break;
				case 5:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( BonusMana.Callback ) ); break;
				case 6:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( PhysicalResist.Callback ) ); break;
				case 7:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( FireResist.Callback ) ); break;
				case 8:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( ColdResist.Callback ) ); break;
				case 9:	from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( PoisonResist.Callback ) ); break;
				case 10: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( EnergyResist.Callback ) ); break;
				case 11: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( RegenHits.Callback ) ); break;
				case 12: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( RegenMana.Callback ) ); break;
				case 13: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( RegenStam.Callback ) ); break;
				case 14: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( CastRecovery.Callback ) ); break;
				case 15: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( CastSpeed.Callback ) ); break;
				case 16: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( LowerManaCost.Callback ) ); break;
				case 17: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( LowerRegCost.Callback ) ); break;
				//case 18: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( MageArmor.Callback ) ); break;
				case 19: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( MageWeapon.Callback ) ); break;
				case 20: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( SpellChanneling.Callback ) ); break;
				case 21: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( SpellDamage.Callback ) ); break;
				case 22: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitColdArea.Callback ) ); break;
				case 23: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitEnergyArea.Callback ) ); break;
				case 24: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitFireArea.Callback ) ); break;
				case 25: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitPhysicalArea.Callback ) ); break;
				case 26: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitPoisonArea.Callback ) ); break;
				case 27: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitDispel.Callback ) ); break;
				case 28: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitFireball.Callback ) ); break;
				case 29: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitHarm.Callback ) ); break;
				case 30: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitLightning.Callback ) ); break;
				case 31: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitMagicArrow.Callback ) ); break;
				case 32: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitLowerAttack.Callback ) ); break;
				case 33: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitLowerDefend.Callback ) ); break;
				case 34: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitLeechHits.Callback ) ); break;
				case 35: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitLeechMana.Callback ) ); break;
				case 36: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( HitLeechStam.Callback ) ); break;
				//case 37: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( UseBestSkill.Callback ) ); break;
				case 38: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( WeaponDamage.Callback ) ); break;
				case 39: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( WeaponSpeed.Callback ) ); break;
				case 40: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( AttackChance.Callback ) ); break;
				case 41: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( DefendChance.Callback ) ); break;
				case 42: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( EnhancePotions.Callback ) ); break;
				case 43: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( LowerStatReq.Callback ) ); break;
				case 44: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Luck.Callback ) ); break;
				case 45: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( ReflectPhysical.Callback ) ); break;
				case 46: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( SelfRepair.Callback ) ); break;
				// Non-AOS Callbacks
				case 47: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Durable.Callback ) ); break;
				case 48: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Substantial.Callback ) ); break;
				case 49: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Massive.Callback ) ); break;
				case 50: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Fortified.Callback ) ); break;
				case 51: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Indestructible.Callback ) ); break;
				case 52: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Ruin.Callback ) ); break;
				case 53: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Might.Callback ) ); break;
				case 54: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Force.Callback ) ); break;
				case 55: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Power.Callback ) ); break;
				case 56: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Vanq.Callback ) ); break;
				case 57: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Accurate.Callback ) ); break;
				case 58: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Surpassingly.Callback ) ); break;
				case 59: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Eminently.Callback ) ); break;
				case 60: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Exceedingly.Callback ) ); break;
				case 61: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Supremely.Callback ) ); break;
				case 62: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Defense.Callback ) ); break;
				case 63: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Guarding.Callback ) ); break;
				case 64: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Hardening.Callback ) ); break;
				case 65: from.BeginTarget( 1, false, TargetFlags.None, new TargetCallback( Fortification.Callback ) ); break;
				default: break;
			}
		}
	}
}
