using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
	public class Invulnerability
	{
		private static readonly double MinSkill = 90.0;
		private static readonly double MaxSkill = 140.0;

		public static void Callback( Mobile from, object target )
		{
			if ( !(target is BaseArmor) )
			{
				from.SendMessage( "This craft cannot be placed on that item" );
			}
			else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
			{
				return;
			}
			else
			{
				if ( !(  from.CheckSkill( SkillName.Inscribe, MinSkill, MaxSkill ) ) )
				{
					from.SendMessage( "Vous ne r�ussissez pas � appliquer l'enchantement...." );
				}
				else
				{
					((BaseArmor)target).ProtectionLevel = ArmorProtectionLevel.Invulnerability;
					((BaseArmor)target).Identified = true;
				}
			}
		}
	}
}