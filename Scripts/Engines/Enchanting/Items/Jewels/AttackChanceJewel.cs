using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class AttackChanceJewel : BaseEnchantingItem
	{
		[Constructable]
		public AttackChanceJewel() : this( 1 )
		{
		}

		[Constructable]
		public AttackChanceJewel( int amount ) : base( amount, 40 )
		{
		}

		public AttackChanceJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
