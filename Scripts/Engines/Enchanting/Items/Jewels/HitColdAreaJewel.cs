using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class HitColdAreaJewel : BaseEnchantingItem
	{
		[Constructable]
		public HitColdAreaJewel() : this( 1 )
		{
		}

		[Constructable]
		public HitColdAreaJewel( int amount ) : base( amount, 22 )
		{
		}

		public HitColdAreaJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
