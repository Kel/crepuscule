using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class HitEnergyAreaJewel : BaseEnchantingItem
	{
		[Constructable]
		public HitEnergyAreaJewel() : this( 1 )
		{
		}

		[Constructable]
		public HitEnergyAreaJewel( int amount ) : base( amount, 23 )
		{
		}

		public HitEnergyAreaJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
