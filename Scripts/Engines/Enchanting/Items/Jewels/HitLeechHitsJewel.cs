using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class HitLeechHitsJewel : BaseEnchantingItem
	{
		[Constructable]
		public HitLeechHitsJewel() : this( 1 )
		{
		}

		[Constructable]
		public HitLeechHitsJewel( int amount ) : base( amount, 34 )
		{
		}

		public HitLeechHitsJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
