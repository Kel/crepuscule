using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class ColdResistJewel : BaseEnchantingItem
	{
		[Constructable]
		public ColdResistJewel() : this( 1 )
		{
		}

		[Constructable]
		public ColdResistJewel( int amount ) : base( amount, 8 )
		{
		}

		public ColdResistJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
