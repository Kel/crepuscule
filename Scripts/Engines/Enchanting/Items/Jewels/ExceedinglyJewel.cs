using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class ExceedinglyJewel : BaseEnchantingItem
	{
		[Constructable]
		public ExceedinglyJewel() : this( 1 )
		{
		}

		[Constructable]
		public ExceedinglyJewel( int amount ) : base( amount, 60 )
		{
		}

		public ExceedinglyJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
