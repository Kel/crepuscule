using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class HitLightningJewel : BaseEnchantingItem
	{
		[Constructable]
		public HitLightningJewel() : this( 1 )
		{
		}

		[Constructable]
		public HitLightningJewel( int amount ) : base( amount, 30 )
		{
		}

		public HitLightningJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
