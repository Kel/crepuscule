using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class HitLeechStamJewel : BaseEnchantingItem
	{
		[Constructable]
		public HitLeechStamJewel() : this( 1 )
		{
		}

		[Constructable]
		public HitLeechStamJewel( int amount ) : base( amount, 36 )
		{
		}

		public HitLeechStamJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
