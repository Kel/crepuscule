using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class HitLowerAttackJewel : BaseEnchantingItem
	{
		[Constructable]
		public HitLowerAttackJewel() : this( 1 )
		{
		}

		[Constructable]
		public HitLowerAttackJewel( int amount ) : base( amount, 32 )
		{
		}

		public HitLowerAttackJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
