using System;
using Server;
using Server.Mobiles;
using Server.Targeting;

namespace Server.Enchanting.Items
{
	public class RegenManaJewel : BaseEnchantingItem
	{
		[Constructable]
		public RegenManaJewel() : this( 1 )
		{
		}

		[Constructable]
		public RegenManaJewel( int amount ) : base( amount, 12 )
		{
		}

		public RegenManaJewel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt(); // version
		}
	}
}
