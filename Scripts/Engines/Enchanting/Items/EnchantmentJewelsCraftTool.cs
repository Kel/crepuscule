﻿using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
    public class EnchantmentJewelsCraftTool : BaseTool
    {
        [Constructable]
        public EnchantmentJewelsCraftTool() : this(50) { }
        [Constructable]
        public EnchantmentJewelsCraftTool(int uses) : base(uses, 0x1F1E) { Weight = 3.0; Name = "Outils de joyaux d'enchantement"; }
        public EnchantmentJewelsCraftTool(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}