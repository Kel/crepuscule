using System;
using Server;
using Server.Targeting;
using Server.Mobiles;
using Server.Enchanting.Gumps;

namespace Server.Enchanting.Items
{	
	public class BookOfSpellCrafts : Item
	{
		public static void Initialize() 
		{ 
           	Server.Commands.Register( "AllEnchants", AccessLevel.GameMaster, new CommandEventHandler( AllCrafts_OnCommand ) );
		    Server.Commands.Register( "AddEnchant", AccessLevel.GameMaster, new CommandEventHandler( AddCraft_OnCommand ) );
       	}

		private static void AllCrafts_OnCommand( CommandEventArgs e )
		{
			e.Mobile.BeginTarget( -1, false, TargetFlags.None, new TargetCallback( AllCrafts_OnTarget ) );
			e.Mobile.SendMessage( "Choisissez le livre d'enchantement � remplir." );
		}

        [Usage("AddEnchant <num>")]
		private static void AddCraft_OnCommand( CommandEventArgs e )
		{
			if ( e.GetInt32(0) < 0 || e.GetInt32(0) > 46 )
			{
				e.Mobile.SendMessage( "Le num�ro doit �tre compris entre 0 et 46." );
			}
			else
			{
				e.Mobile.BeginTarget( -1, false, TargetFlags.None, new TargetStateCallback( AddCraft_OnTarget ), e.GetInt32(0) );
                e.Mobile.SendMessage("Choisissez le livre d'enchantement � remplir.");
			}
		}

		private static void AllCrafts_OnTarget( Mobile from, object target )
		{
			if ( target is BookOfSpellCrafts )
			{
				BookOfSpellCrafts book = target as BookOfSpellCrafts;

				book.Content = ulong.MaxValue;

				from.SendMessage( "Livre d'enchantements rempli." );
			}
		}

		private static void AddCraft_OnTarget( Mobile from, object target, object state )
		{
			int num = (int)state;

			if ( target is BookOfSpellCrafts )
			{
				BookOfSpellCrafts book = target as BookOfSpellCrafts;

				if ( book.HasCraft( num ) )
				{
					from.SendMessage( "Le livre poss�de d�j� cet enchantement." );
				}
				else
				{
					book.Content |= (ulong)1 << num;
					book.InvalidateProperties();
					from.SendMessage( "L'enchantement a �t� ajout�." );
				}
			}

		}
					
		private ulong m_Content;

		public int Count
		{
			get
			{
				return GetCraftCount();
			}
		}

		public ulong Content
		{
			get
			{ 
				return m_Content;
			}
			set
			{
				m_Content = value;
			}
		}

		[Constructable]
		public BookOfSpellCrafts() : base( 0x2254 )
		{
			Name = "Livre d'enchantements";
			Hue = 0x461;
		}

		public BookOfSpellCrafts( Serial serial ) : base( serial )
		{
		}

		public bool HasCraft( int CraftID )
		{
			return ( CraftID >= 0 && CraftID < 67 && (m_Content & ((ulong)1 << CraftID)) != 0 );
		}

		private int GetCraftCount()
		{
			int count = 0;

			if ( Core.AOS )
			{
				for( int i = 0; i < 47; ++i )
				{
					if ( HasCraft( i ) ) ++count;
				}
			}
			else
			{
				for( int i = 47; i < 67; ++i )
				{
					if ( HasCraft( i ) ) ++count;
				}
			}

			return count;
		}

		public override void OnDoubleClick( Mobile from )
		{
            if (from is RacePlayerMobile)
            {
                RacePlayerMobile fromRPM = from as RacePlayerMobile;
                if (fromRPM.Capacities[CapacityName.Enchanting].Value >= 30)
                {
                    from.SendGump(new SpellCraftBook(from, this));
                }
                else
                {
                    from.SendMessage("Vous ne comprenez rien, vous avez besoin 30 en enchantement pour pouvoir lire ce livre");
                }
            }
		}

		public override bool OnDragDrop( Mobile from, Item dropped )
		{
			if ( dropped is BaseEnchantingItem && dropped.Amount == 1 )
			{
                BaseEnchantingItem craft = (BaseEnchantingItem)dropped;

				if ( HasCraft( craft.CraftID ) )
				{
                    from.SendMessage("Le livre poss�de d�j� cet enchantement.");
					return false;
				}
				else if ( Core.AOS && craft.CraftID > 46 )
				{
					from.SendMessage( "Ce type d'enchantements ne peut pas �tre plac� dans le livre." );
					return false;
				}
				else if ( !Core.AOS && craft.CraftID < 47 )
				{
                    from.SendMessage("Ce type d'enchantements ne peut pas �tre plac� dans le livre.");
					return false;
				}
				else
				{
					int val = craft.CraftID;

					if ( val >= 0 && val < 67 )
					{
						m_Content |= (ulong)1 << val;

						InvalidateProperties();

						craft.Delete();
						from.CloseGump( typeof( SpellCraftBook ) );
						from.SendGump( new SpellCraftBook( from, this ) );
						return true;
					}
					return false;
				}
			}
			return false;
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();

			switch( version )
			{
				// version 0 reads second integer, version 1 no longer
				// needs it. No going to case 0 or serialization error
				// will occur!!
				case 1:
					m_Content = reader.ReadULong(); break;
				case 0:
					m_Content = reader.ReadULong();
					int m_Count = reader.ReadInt(); break;
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int)1 ); // version
			writer.Write( m_Content );
		}
	}
}