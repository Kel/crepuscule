using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class LowerManaCost
  {
    private static readonly int Minimum = 1;
    private static readonly int Maximum = 8;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseJewel || (target is BaseArmor && !(target is BaseShield)) ) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        if ( target is BaseJewel )
          AddProp( (BaseJewel)target, scalar );

        else if ( target is BaseArmor )
          AddProp( (BaseArmor)target, scalar );

                EnchantingHelper.FinalizeEnchantment(target,16);
      }
    }

    private static void AddProp( BaseJewel item, double scalar )
    {
      EnchantingHelper.ApplyAttribute( ((BaseJewel)item).Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.LowerManaCost, Minimum, Maximum );
    }

    private static void AddProp( BaseArmor item, double scalar )
    {
      EnchantingHelper.ApplyAttribute( ((BaseArmor)item).Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.LowerManaCost, Minimum, Maximum );
    } 
  }
}