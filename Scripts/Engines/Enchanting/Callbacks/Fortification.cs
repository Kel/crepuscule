using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
	public class Fortification
	{
		private static readonly double MinSkill = 80.0;
		private static readonly double MaxSkill = 130.0;

		public static void Callback( Mobile from, object target )
		{
			if ( !(target is BaseArmor) )
			{
				from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
			}
			else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
			{
				return;
			}
			else
			{
				if ( !(  from.CheckSkill( SkillName.Inscribe, MinSkill, MaxSkill ) ) )
				{
					from.SendMessage( "Vous ne r�ussissez pas � appliquer l'enchantement...." );
				}
				else
				{
					((BaseArmor)target).ProtectionLevel = ArmorProtectionLevel.Fortification;
					((BaseArmor)target).Identified = true;
                    EnchantingHelper.FinalizeEnchantment(target,65);
				}
			}
		}
	}
}