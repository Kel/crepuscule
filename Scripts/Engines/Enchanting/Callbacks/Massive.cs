using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
	public class Massive
	{
		private static readonly double MinSkill = 60.0;
		private static readonly double MaxSkill = 110.0;

		public static void Callback( Mobile from, object target )
		{
			if ( !(target is BaseWeapon || target is BaseArmor ) )
			{
				from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
			}
			else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
			{
				return;
			}
			else
			{
				if ( !(  from.CheckSkill( SkillName.Inscribe, MinSkill, MaxSkill ) ) )
				{
					from.SendMessage( "Vous ne r�ussissez pas � appliquer l'enchantement...." );
				}
				else
				{
					if ( target is BaseArmor )
					{
						((BaseArmor)target).Durability = ArmorDurabilityLevel.Massive;
						((BaseArmor)target).Identified = true;
					}
					else if ( target is BaseWeapon )
					{
						((BaseWeapon)target).DurabilityLevel = WeaponDurabilityLevel.Massive;
						((BaseWeapon)target).Identified = true;
					}
                    EnchantingHelper.FinalizeEnchantment(target,49);
				}
			}
		}
	}
}