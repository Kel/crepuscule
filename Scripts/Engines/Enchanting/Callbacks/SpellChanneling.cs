using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class SpellChanneling
  {
    private static readonly int Minimum = 1;
    private static readonly int Maximum = 1;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseWeapon || target is BaseShield) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        if ( target is BaseWeapon )
          ApplyProp( (BaseWeapon)target, scalar );

        else if ( target is BaseShield )
          ApplyProp( (BaseShield)target, scalar );

                EnchantingHelper.FinalizeEnchantment(target,20);
      }
    }

    private static void ApplyProp( BaseWeapon item, double scalar )
    {
      EnchantingHelper.ApplyAttribute( item.Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.SpellChanneling, Minimum, Maximum );
    }

    private static void ApplyProp( BaseShield item, double scalar )
    {
      EnchantingHelper.ApplyAttribute( item.Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.SpellChanneling, Minimum, Maximum );
    } 
  }
}