using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class HitLeechStam
  {
    private static readonly int Minimum = 2;
    private static readonly int Maximum = 50;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseWeapon) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        EnchantingHelper.ApplyAttribute( ((BaseWeapon)target).WeaponAttributes, (int) (20 * scalar), (int) (100 * scalar), AosWeaponAttribute.HitLeechStam, Minimum, Maximum, 2 );
                EnchantingHelper.FinalizeEnchantment(target,36);
      }
    }
  }
}