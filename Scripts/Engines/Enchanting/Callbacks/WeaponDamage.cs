using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class WeaponDamage
  {
    private static readonly int Minimum = 1;
    private static readonly int Maximum = 50;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseWeapon || target is BaseJewel) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;
        
        if ( target is BaseWeapon )
          EnchantingHelper.ApplyAttribute( ((BaseWeapon)target).Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.WeaponDamage, Minimum, Maximum );

        else if ( target is BaseJewel )
          EnchantingHelper.ApplyAttribute( ((BaseJewel)target).Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.WeaponDamage, Minimum, Maximum );

                EnchantingHelper.FinalizeEnchantment(target,38);
      }
    }
  }
}