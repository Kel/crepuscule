using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class PhysicalResist
  {
    private static readonly int Minimum = 1;
    private static readonly int Maximum = 15;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseWeapon || target is BaseJewel || (target is BaseArmor && !(target is BaseShield)) ) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }

      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        if ( target is BaseArmor )
          AddResist( (BaseArmor)target, scalar );

        else if ( target is BaseJewel )
          AddResist( (BaseJewel)target, scalar );

        else if ( target is BaseWeapon )
          AddResist( (BaseWeapon)target, scalar );

                EnchantingHelper.FinalizeEnchantment(target,6);
      }
    }

    private static void AddResist( BaseArmor armor, double scalar )
    {
      EnchantingHelper.ApplyResistance( armor, (int) (20 * scalar), (int) (100 * scalar), ResistanceType.Physical, Minimum, Maximum );
    }

    private static void AddResist( BaseJewel jewel, double scalar )
    {
      EnchantingHelper.ApplyAttribute( jewel.Resistances, (int) (20 * scalar), (int) (100 * scalar), AosElementAttribute.Physical, Minimum, Maximum );
    }

    private static void AddResist( BaseWeapon weapon, double scalar )
    {
      EnchantingHelper.ApplyAttribute( weapon.WeaponAttributes, (int) (20 * scalar), (int) (100 * scalar), AosWeaponAttribute.ResistPhysicalBonus, Minimum, Maximum );
    }
  }
}