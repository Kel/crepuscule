using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class SelfRepair
  {
    private static readonly int Minimum = 1;
    private static readonly int Maximum = 5;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseArmor) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        EnchantingHelper.ApplyAttribute( ((BaseArmor)target).ArmorAttributes, (int) (20 * scalar), (int) (100 * scalar), AosArmorAttribute.SelfRepair, Minimum, Maximum );
                EnchantingHelper.FinalizeEnchantment(target,46);
      }
    }
  }
}