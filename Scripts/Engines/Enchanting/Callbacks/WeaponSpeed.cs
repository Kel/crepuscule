using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class WeaponSpeed
  {
    private static readonly int Minimum = 5;
    private static readonly int Maximum = 30;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseWeapon) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        EnchantingHelper.ApplyAttribute( ((BaseWeapon)target).Attributes, (int) (20 * scalar), (int) (100 * scalar), AosAttribute.WeaponSpeed, Minimum, Maximum, 5 );
                EnchantingHelper.FinalizeEnchantment(target,39);
      }
    }
  }
}