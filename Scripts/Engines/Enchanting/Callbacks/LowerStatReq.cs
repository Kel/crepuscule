using System;
using Server;
using Server.Items;
using Server.Enchanting;

namespace Server.Enchanting.Crafts
{
  public class LowerStatReq
  {
    private static readonly int Minimum = 10;
    private static readonly int Maximum = 100;

    public static void Callback( Mobile from, object target )
    {
      if ( !(target is BaseWeapon || target is BaseArmor ) )
      {
        from.SendMessage( "Cet enchantement ne peut pas �tre plac� sur cet objet" );
      }
      else if ( !EnchantingHelper.CheckSpellCrafted( from, target ) )
      {
        return;
      }
      else
      {
        double scalar = (from.Skills[SkillName.Inscribe].Value + from.Skills[SkillName.ItemID].Value) / 200.0;

        if ( target is BaseArmor )
          ApplyProp( (BaseArmor)target, scalar );

        else if ( target is BaseWeapon )
          ApplyProp( (BaseWeapon)target, scalar );

                EnchantingHelper.FinalizeEnchantment(target,43);
      }
    }

    private static void ApplyProp( BaseArmor item, double scalar )
    {
      EnchantingHelper.ApplyAttribute( item.ArmorAttributes, (int) (20 * scalar), (int) (100 * scalar), AosArmorAttribute.LowerStatReq, Minimum, Maximum, 10 );
    }

    private static void ApplyProp( BaseWeapon item, double scalar )
    {
      EnchantingHelper.ApplyAttribute( item.WeaponAttributes, (int) (20 * scalar), (int) (100 * scalar), AosWeaponAttribute.LowerStatReq, Minimum, Maximum, 10 );
    }
  }
}