using System;
using Server;
using Server.Items;
using System.Text;
using Server.Mobiles;
using Server.Network;
using Server.Spells;
using Server.Spells.Druid;


namespace Server.Scripts.Commands
{
	public class CastDruidSpells
	{
		public static void Initialize()
		{
			Server.Commands.CommandPrefix = ".";

			Properties.Register();

			Register( "druide1", AccessLevel.Player, new CommandEventHandler( SummonFirefly_OnCommand ) );

			Register( "druide2", AccessLevel.Player, new CommandEventHandler( HollowReed_OnCommand ) );

			Register( "druide3", AccessLevel.Player, new CommandEventHandler( PackOfBeasts_OnCommand ) );

			Register( "druide4", AccessLevel.Player, new CommandEventHandler( SpringOfLife_OnCommand ) );

			Register( "druide5", AccessLevel.Player, new CommandEventHandler( GraspingRoots_OnCommand ) );

			Register( "druide6", AccessLevel.Player, new CommandEventHandler( BlendWithForest_OnCommand ) );

			Register( "druide7", AccessLevel.Player, new CommandEventHandler( SwarmOfInsects_OnCommand ) );

			Register( "druide8", AccessLevel.Player, new CommandEventHandler( VolcanicEruption_OnCommand ) );

			Register( "druide9", AccessLevel.Player, new CommandEventHandler( SummonTreefellow_OnCommand ) );

			Register( "druide10", AccessLevel.Player, new CommandEventHandler( StoneCircle_OnCommand ) );

			Register( "druide11", AccessLevel.Player, new CommandEventHandler( EnchantedGrove_OnCommand ) );

			Register( "druide12", AccessLevel.Player, new CommandEventHandler( LureStone_OnCommand ) );

			Register( "druide13", AccessLevel.Player, new CommandEventHandler( NaturesPassage_OnCommand ) );

			Register( "druide14", AccessLevel.Player, new CommandEventHandler( MushroomGateway_OnCommand ) );

			Register( "druide15", AccessLevel.Player, new CommandEventHandler( RestorativeSoil_OnCommand ) );

			Register( "druide16", AccessLevel.Player, new CommandEventHandler( ShieldOfEarth_OnCommand ) );

		}

		public static void Register( string command, AccessLevel access, CommandEventHandler handler )
		{
			Server.Commands.Register( command, access, handler );
		}

		public static bool HasSpell( Mobile from, int spellID )
		{
			Spellbook book = Spellbook.Find( from, spellID );

			return ( book != null && book.HasSpell( spellID ) );
		}

		[Usage( "druide1" )]
		[Description( "Casts SummonFirefly spell." )]
		public static void SummonFirefly_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 551 ) )
					{
					new FireflySpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}
		[Usage( "druide2" )]
		[Description( "Casts HollowReed spell." )]
		public static void HollowReed_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;
			
         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 552 ) )
					{
					new HollowReedSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide3" )]
		[Description( "Casts PackOfBeasts spell." )]
		public static void PackOfBeasts_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 553 ) )
					{
					new PackOfBeastSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide4" )]
		[Description( "Casts SpringOfLife spell." )]
		public static void SpringOfLife_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 554 ) )
					{
					new SpringOfLifeSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide5" )]
		[Description( "Casts GraspingRoots spell." )]
		public static void GraspingRoots_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 555 ) )
					{
					new GraspingRootsSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide6" )]
		[Description( "Casts BlendWithForest spell." )]
		public static void BlendWithForest_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 556 ) )
					{
					new  BlendWithForestSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide7" )]
		[Description( "Casts SwarmOfInsects spell." )]
		public static void SwarmOfInsects_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 557 ) )
					{
					new SwarmOfInsectsSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide8" )]
		[Description( "Casts VolcanicEruption spell." )]
		public static void VolcanicEruption_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 558 ) )
					{
					new VolcanicEruptionSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide9" )]
		[Description( "Casts SummonTreefellow spell." )]
		public static void SummonTreefellow_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 559 ) )
					{
					new TreefellowSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide10" )]
		[Description( "Casts StoneCircle spell." )]
		public static void StoneCircle_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 560 ) )
					{
					new StoneCircleSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide11" )]
		[Description( "Casts EnchantedGrove spell." )]
		public static void EnchantedGrove_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 561 ) )
					{
					new EnchantedGroveSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide12" )]
		[Description( "Casts LureStone spell." )]
		public static void LureStone_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 562 ) )
					{
					new LureStoneSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide13" )]
		[Description( "Casts NaturesPassage spell." )]
		public static void NaturesPassage_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 563 ) )
					{
					new NaturesPassageSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide14" )]
		[Description( "Casts MushroomGateway spell." )]
		public static void MushroomGateway_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 564 ) )
					{
					new MushroomGatewaySpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "druide15" )]
		[Description( "Casts RestorativeSoil spell." )]
		public static void RestorativeSoil_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 565 ) )
					{
					new RestorativeSoilSpell( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}
		[Usage( "druide16" )]
		[Description( "Casts ShieldOfEarth spell." )]
		public static void ShieldOfEarth_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 566 ) )
					{

					new ShieldOfEarthSpell( e.Mobile, null ).Cast();
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 

		}
	}
}
