using System; 
using System.Collections; 
using Server; 
using Server.Items; 
using Server.Network; 
using Server.Spells; 
using Server.Spells.Druid; 
using Server.Prompts; 

namespace Server.Gumps 
{ 
   public class DruidSpellbookGump : Gump 
   { 
      private DruidSpellbook m_Book; 
       
      int gth = 0x903; 
      private void AddBackground() 
      { 
         AddPage( 0 ); 
          
         AddImage( 100, 10, 0x89B, 0 ); 
         //   AddImage( 255, 10, 0x8AD, 0x48B ); 
          
         // AddLabel( 140, 45, gth, "Ohm - Earth" ); 
         // AddLabel( 140, 60, gth, "Ess - Air" ); 
         // AddLabel( 140, 75, gth, "Crur - Fire" ); 
         // AddLabel( 140, 90, gth, "Sepa - Water" ); 
         // AddLabel( 140, 110, gth, "Kes - One" ); 
         // AddLabel( 140, 125, gth, "Sec - Whole" ); 
         // AddLabel( 140, 140, gth, "En  - Call" ); 
         // AddLabel( 140, 155, gth, "Vauk - Banish" ); 
         // AddLabel( 140, 170, gth, "Tia - Befoul" ); 
         // AddLabel( 140, 185, gth, "Ante - Cleanse" ); 
          
      } 
       
      public static bool HasSpell( Mobile from, int spellID ) 
      { 
         Spellbook book = Spellbook.Find( from, spellID ); 
         return ( book != null && book.HasSpell( spellID ) ); 
      } 
       
       
      public DruidSpellbookGump( Mobile from, DruidSpellbook book ) : base( 150, 200 ) 
      { 
          
         m_Book = book; 
         AddBackground(); 
         AddPage( 1 ); 
         AddLabel( 150, 17, gth, "Magie Naturelle" ); 
         int sbtn = 0x93A; 
         int dby = 40; 
         int dbpy = 40;; 
         AddButton( 396, 14, 0x89E, 0x89E, 17, GumpButtonType.Page, 2 ); 
          
         if (HasSpell( from, 551) ) 
         { 
            AddLabel( 145, dbpy, gth, "Luciole" ); 
            AddButton( 125, dbpy + 3, sbtn, sbtn, 1, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 552) ) 
         { 
            AddLabel( 145, dby, gth, "Infusion" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 2, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 553) ) 
         { 
            AddLabel( 145, dby, gth, "Animaux Sauvages" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 3, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 554) ) 
         { 
            AddLabel( 145, dby, gth, "Source de Vie" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 4, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 555) ) 
         { 
            AddLabel( 145, dby, gth, "Racines" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 5, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 556) ) 
         { 
            AddLabel( 145, dby, gth, "Lien avec la For�t" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 6, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 557) ) 
         { 
            AddLabel( 145, dby, gth, "Insectes" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 7, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 558) ) 
         { 
            AddLabel( 145, dby, gth, "�ruption" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 8, GumpButtonType.Reply, 1 ); 
         } 
         if (HasSpell( from, 559) ) 
         { 
            AddLabel( 315, dbpy, gth, "Gardien V�g�tal" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 9, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 560) ) 
         { 
            AddLabel( 315, dbpy, gth, "Cercle de Pierres" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 10, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 561) ) 
         { 
            AddLabel( 315, dbpy, gth, "Bois Enchant�" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 11, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 562) ) 
         { 
            AddLabel( 315, dbpy, gth, "Pierre Attirante" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 12, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 563) ) 
         { 
            AddLabel( 315, dbpy, gth, "Passage Naturel" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 13, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 564) ) 
         { 
            AddLabel( 315, dbpy, gth, "Portail Naturel" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 14, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 565) ) 
         { 
            AddLabel( 315, dbpy, gth, "Sol Fortifiant" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 15, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 566) ) 
         { 
            AddLabel( 315, dby, gth, "Protection Terrestre" ); 
            AddButton( 295, dby + 3, sbtn, sbtn, 16, GumpButtonType.Reply, 1 ); 
         } 
          
         int i = 2; 
          
         if (HasSpell( from, 551) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Luciole" ); 
            AddHtml( 130, 59, 123, 132, "Une petite luciole illumine le Druide.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Souffre" ); 
            AddLabel( 295, 77, gth, "Ange d�truit" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  2" ); 
            AddLabel( 295, 187, gth, "Mana requise:  40" ); 
            i++; 
         } 
          
         if (HasSpell( from, 552) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Infusion" ); 
            AddHtml( 130, 59, 123, 132, "La nature augmente la force, dext�rit� et intelligence temporairement.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            AddLabel( 295, 77, gth, "Racine de mandagore" ); 
            AddLabel( 295, 97, gth, "Belladone" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  4" ); 
            AddLabel( 295, 187, gth, "Mana requise:  10" ); 
            i++; 
         } 
          
         if (HasSpell( from, 553) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Animaux Sauvages" ); 
            AddHtml( 130, 59, 123, 132, "Des animaux sauvages viennent au service du Druide.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            AddLabel( 295, 77, gth, "Perle noire" ); 
            AddLabel( 295, 97, gth, "Bois attrofi�" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  32" ); 
            AddLabel( 295, 187, gth, "Mana requise:  25" ); 
            i++; 
         } 
         if (HasSpell( from, 554) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Source de Vie" ); 
            AddHtml( 130, 59, 123, 132, "Une puissance source reg�n�re la vie.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Eau de source" ); 
            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  38" ); 
            AddLabel( 295, 187, gth, "Mana requise:  25" ); 
            i++; 
         } 
         if (HasSpell( from, 555) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Racines" ); 
            AddHtml( 130, 59, 123, 132, "Les racines retiennent la cr�ature.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 97, gth, "Toile d'araign�e" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  12" ); 
            AddLabel( 295, 187, gth, "Mana requise:  25" ); 
            i++; 
         } 
         if (HasSpell( from, 556) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Lien avec la Foret" ); 
            AddHtml( 130, 59, 123, 132, "Le Druide se camoufle parmis les arbres.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            AddLabel( 295, 77, gth, "Belladone" ); 
            AddLabel( 295, 167, gth, "Capacit� requise: 26" ); 
            AddLabel( 295, 187, gth, "Mana requise:  20" ); 
            i++; 
         } 
         if (HasSpell( from, 557) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Insectes" ); 
            AddHtml( 130, 59, 123, 132, "Invoque des insectes pour troubler la cible.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Ail" ); 
            AddLabel( 295, 77, gth, "Belladone" ); 
            AddLabel( 295, 97, gth, "Ange d�truit" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  20" ); 
            AddLabel( 295, 187, gth, "Mana requise:  25" ); 
            i++; 
         } 
         if (HasSpell( from, 558) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "�ruption" ); 
            AddHtml( 130, 59, 123, 132, "La lave sort de la terre et br�le tout sur son passage.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Souffre" ); 
            AddLabel( 295, 77, gth, "Ange d�truit" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  68" ); 
            AddLabel( 295, 187, gth, "Mana requise:  60" ); 
            i++; 
         } 
         if (HasSpell( from, 559) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Gardien V�g�tal" ); 
            AddHtml( 130, 59, 123, 132, "Une cr�ature se forme pour d�fendre le Druide.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            AddLabel( 295, 77, gth, "Perle noire" ); 
            AddLabel( 295, 97, gth, "Bois attrofi�" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  50" ); 
            AddLabel( 295, 187, gth, "Mana requise:  30" ); 
            i++; 
         } 
         if (HasSpell( from, 560) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Cercle de Pierres" ); 
            AddHtml( 130, 59, 123, 132, "Un cercle de pierres jailli du sol.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Perle noire" ); 
            AddLabel( 295, 77, gth, "Ginseng" ); 
            AddLabel( 295, 97, gth, "Eau de source" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  44" ); 
            AddLabel( 295, 187, gth, "Mana requise:  25" ); 
            i++; 
         } 
         if (HasSpell( from, 561) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Bois Enchant�" ); 
            AddHtml( 130, 59, 123, 132, "Un bosquet reg�n�ratif prend forme.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Bois attrofi�" ); 
            AddLabel( 295, 77, gth, "Racine de mandagore" ); 
            AddLabel( 295, 97, gth, "Eau de source" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  80" ); 
            AddLabel( 295, 187, gth, "Mana requise:  20" ); 
            i++; 
         } 
         if (HasSpell( from, 562) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Pierre Attirante" ); 
            AddHtml( 130, 59, 123, 132, "Une pierre attire les cr�atures.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Perle noire" ); 
            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  8" ); 
            AddLabel( 295, 187, gth, "Mana requise:  20" ); 
            i++; 
         } 
         if (HasSpell( from, 563) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Passage Naturel" ); 
            AddHtml( 130, 59, 123, 132, "Le Druide fusionne avec la nature pour voyager.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Perle noire" ); 
            AddLabel( 295, 77, gth, "Mousse de sang" ); 
            AddLabel( 295, 97, gth, "Racine de mandagore" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  56" ); 
            AddLabel( 295, 187, gth, "Mana requise:  30" ); 
            i++; 
         } 
         if (HasSpell( from, 564) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Portail Naturel" ); 
            AddHtml( 130, 59, 123, 132, "Un portail traverse les courants de la vie.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Perle noire" ); 
            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 97, gth, "Racine de mandagore" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  74" ); 
            AddLabel( 295, 187, gth, "Mana requise:  40" ); 
            i++; 
         } 
         if (HasSpell( from, 565) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Sol Fortifiant" ); 
            AddHtml( 130, 59, 123, 132, "Le sol transmet la vie.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Ail" ); 
            AddLabel( 295, 77, gth, "Ginseng" ); 
            AddLabel( 295, 97, gth, "Eau de source" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  62" ); 
            AddLabel( 295, 187, gth, "Mana requise:  40" ); 
            i++; 
         } 
         if (HasSpell( from, 566) ) 
         { 
            AddPage( i ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Protection Terrestre" ); 
            AddHtml( 130, 59, 123, 132, "Une haie se place devant le Druide.", false, false ); 
            AddLabel( 295, 37, gth, "R�actifs:" ); 
            AddLabel( 295, 57, gth, "Ginseng" ); 
            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 167, gth, "Capacit� requise:  16" ); 
            AddLabel( 295, 187, gth, "Mana requise:  15" ); 
            i++; 
         } 
          
         AddPage( i ); 
         AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
      } 
       
       
       
       
       
      public override void OnResponse( NetState state, RelayInfo info ) 
      { 
         Mobile from = state.Mobile; 
         switch ( info.ButtonID ) 
         { 
            case 0: 
               { 
                  break; 
               } 
                
            case 1: 
               { 
                  new FireflySpell( from, null ).Cast(); 
                  break; 
               } 
                   
            case 2: 
               { 
                  new HollowReedSpell( from, null ).Cast(); 
                  break; 
               } 
                   
            case 3: 
               { 
                  new PackOfBeastSpell( from, null ).Cast(); 
                  break; 
               } 
                      
            case 4: 
               { 
                  new SpringOfLifeSpell( from, null ).Cast(); 
                  break; 
               } 
                         
            case 5: 
               { 
                  new GraspingRootsSpell( from, null ).Cast(); 
                  break; 
               } 
                            
            case 6: 
               { 
                  new BlendWithForestSpell( from, null ).Cast(); 
                  break; 
               } 
                               
            case 7: 
               { 
                  new SwarmOfInsectsSpell( from, null ).Cast(); 
                  break; 
               } 
                                  
            case 8: 
               { 
                  new VolcanicEruptionSpell( from, null ).Cast(); 
                  break; 
               } 
                                     
            case 9: 
               { 
                  new TreefellowSpell( from, null ).Cast(); 
                  break; 
               } 
                                        
            case 10: 
               { 
                  new StoneCircleSpell( from, null ).Cast(); 
                  break; 
               } 
                                           
                                           
            case 11: 
               { 
                  new EnchantedGroveSpell( from, null ).Cast(); 
                  break; 
               } 
                                              
            case 12: 
               { 
                  new LureStoneSpell( from, null ).Cast(); 
                  break; 
               } 
                                                 
            case 13: 
               { 
                  new NaturesPassageSpell( from, null ).Cast(); 
                  break; 
               } 
                                                    
            case 14: 
               { 
                  new MushroomGatewaySpell( from, null ).Cast(); 
                  break; 
               } 
                                                       
            case 15: 
               { 
                  new RestorativeSoilSpell( from, null ).Cast(); 
                  break; 
               } 
                                                          
            case 16: 
               { 
                  new ShieldOfEarthSpell( from, null ).Cast(); 
                  break; 
               } 
                                                          
            case 17: 
               { 
                  from.PlaySound(0x55); 
                  break; 
               } 
          
            case 18: 
               { 
                  from.PlaySound(0x55); 
                  break; 
               } 
                                                                   
            case 19: 
               { 
                  from.PlaySound(0x55); 
                  break; 
               } 
                                                                      
         } 
      } 
   } 
}
