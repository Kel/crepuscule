using System;
using Server.Mobiles;
using Server.Network;
using Server.Gumps;
using Server.Spells;

namespace Server.Items
{
   public class DruidSpellbook : Spellbook
   {
      public override SpellbookType SpellbookType{ get{ return SpellbookType.Druid; } }
      public override int BookOffset{ get{ return 551; } }
      public override int BookCount{ get{ return 16; } }

      public override Item Dupe( int amount )
      {
         Spellbook book = new DruidSpellbook();

         book.Content = this.Content;

         return base.Dupe( book, amount );
      }

      [Constructable]
      public DruidSpellbook() : this( (ulong)0 )
      {
      }

      [Constructable]
      public DruidSpellbook( ulong content ) : base( content, 0xEFA )
      {
         Hue = 1415;
         Name = "Livre de nature";
      }

      public override void OnDoubleClick( Mobile from )
      {
	  	if ( from.InRange( GetWorldLocation(), 1 ) )
		{
			from.CloseGump( typeof( DruidSpellbookGump ) );
			from.SendGump( new DruidSpellbookGump( from, this ) );
		}
      }

      public DruidSpellbook( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
