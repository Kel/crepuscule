using System;
using Server;
using Server.Items;

namespace Server.Items
{
    public class BagOfDruidReagents : Bag
    {

        [Constructable]
        public BagOfDruidReagents()
            : this(50)
        {
        }

        [Constructable]
        public BagOfDruidReagents(int amount)
        {
            this.Movable = true;
            this.Hue = 542;
            this.Name = "Sac de r�actifs druidiques";

            DropReagents(amount);
        }

        public void DropReagents(int amount)
        {
            DropOrStackItem(new BlackPearl(amount));
            DropOrStackItem(new Bloodmoss(amount));
            DropOrStackItem(new Garlic(amount));
            DropOrStackItem(new Ginseng(amount));
            DropOrStackItem(new MandrakeRoot(amount));
            DropOrStackItem(new Nightshade(amount));
            DropOrStackItem(new SulfurousAsh(amount));
            DropOrStackItem(new SpidersSilk(amount));
            DropOrStackItem(new PetrafiedWood(amount));
            DropOrStackItem(new DestroyingAngel(amount));
            DropOrStackItem(new SpringWater(amount));
        }


        public BagOfDruidReagents(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}


