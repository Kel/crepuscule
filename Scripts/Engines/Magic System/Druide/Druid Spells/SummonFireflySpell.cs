using System;
using Server.Network;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Spells.Druid
{
	public class FireflySpell : DruidSpell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"Luciole", "Kes En Crur",
				SpellCircle.First,
				269,
				9020,
				false,
				Reagent.SulfurousAsh,
            		Reagent.DestroyingAngel
			);

		public FireflySpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}
		public override double CastDelay{ get{ return 1.0; } }
      	public override double RequiredSkill{ get{ return 0.0; } }
      	public override int RequiredMana{ get{ return 40; } }

		public override bool CheckCast()
		{
			if ( !base.CheckCast() )
				return false;

		

			return true;
		}

		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 1;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Nature].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie naturelle.");
                    return;
                }
            }
            //Caster.Target = new InternalTarget(this);
			if ( CheckSequence() )
			{
					SpellHelper.Summon( new FireflyFamiliar(), Caster, 0x217, TimeSpan.FromSeconds( 4.0 * Caster.Skills[SkillName.Meditation].Value ), false, false );
					}

			FinishSequence();
		}
	}
}
