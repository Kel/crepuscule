using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Mobiles;

namespace Server.Mobiles
{
	[CorpseName( "corps de luciole" )]
	public class FireflyFamiliar : BaseCreature
	{
	
		public FireflyFamiliar(): base( AIType.AI_Animal, FightMode.Agressor, 10, 1, 0.2, 0.4 )
		{
			Name = "luciole";
			Body = 0x3A;
			Hue = 0x0;
			BaseSoundID = 466;

			SetStr( 1 );
			SetDex( 200 );
			SetInt( 10 );

			SetHits( 1 );
			SetStam( 60 );
			SetMana( 0 );

			SetDamage( 1, 2 );

			SetDamageType( ResistanceType.Fire, 100 );

			SetResistance( ResistanceType.Physical, 10, 15 );
			SetResistance( ResistanceType.Fire, 99 );
			SetResistance( ResistanceType.Cold, 10, 15 );
			SetResistance( ResistanceType.Poison, 10, 15 );
			SetResistance( ResistanceType.Energy, 10,15 );

			SetSkill( SkillName.Wrestling, 10.0 );
			SetSkill( SkillName.Tactics, 10.0 );
			AddItem( new LightSource() );
			AddItem( new LightSource() );
			ControlSlots = 0;
		}


		public FireflyFamiliar( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
