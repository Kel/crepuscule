using System;
using System.Collections;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Spells.Druid
{
   public class SwarmOfInsectsSpell : DruidSpell
   {
      private static SpellInfo m_Info = new SpellInfo(
            "Insectes", "Ess Ohm En Sec Tia",
            SpellCircle.Seventh,
            263,
            9032,
            false,
            Reagent.Garlic,
            Reagent.Nightshade,
            Reagent.DestroyingAngel
         );

      public override double CastDelay{ get{ return 2.0; } }
      public override double RequiredSkill{ get{ return 20.0; } }
      public override int RequiredMana{ get{ return 25; } }

      public SwarmOfInsectsSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
      {
      }

      public override void OnCast()
      {
           if (Caster is RacePlayerMobile) { int apt_besoin = 10;
          RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
          if (Casteur.Capacities[CapacityName.Nature].Value < apt_besoin)
          {
              Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
              Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie naturelle.");
              return;
          }
         }Caster.Target = new InternalTarget( this );
      }

      public void Target( Mobile m )
      {
         if ( CheckHSequence( m ) )
         {
            SpellHelper.Turn( Caster, m );

            SpellHelper.CheckReflect( (int)this.Circle, Caster, ref m );

            CheckResisted( m ); // Check magic resist for skill, but do not use return value


            double damage = (Caster.Skills[CastSkill].Value - m.Skills[SkillName.AnimalLore].Value);


               new InternalTimer( m, damage).Start();
         }

         FinishSequence();
      }

      private static Hashtable m_Table = new Hashtable();

      private class InternalTimer : Timer
      {
         private Mobile m_Mobile;
         private double m_Damage;
         private double m_Base;

         public InternalTimer( Mobile m, double damage ) : base( TimeSpan.FromSeconds( 0.1 ), TimeSpan.FromSeconds( 3.0 ), 6)
         {
            m_Mobile = m;
            m_Damage = ((damage / 50) + 1);
            m_Base = (damage / 50);
         }

         protected override void OnTick()
         {
         	m_Mobile.FixedParticles( 0x91B, 1, 240, 9916, 0, 3, EffectLayer.Head );
            m_Mobile.PlaySound( 0x1E5 );
            m_Mobile.Damage( (int)m_Damage );
            m_Damage += m_Base ;
         }
      }

      private class InternalTarget : Target
      {
         private SwarmOfInsectsSpell m_Owner;

         public InternalTarget( SwarmOfInsectsSpell owner ) : base( 12, false, TargetFlags.Harmful )
         {
            m_Owner = owner;
         }

         protected override void OnTarget( Mobile from, object o )
         {
            if ( o is Mobile )
               m_Owner.Target( (Mobile) o );
         }

         protected override void OnTargetFinish( Mobile from )
         {
            m_Owner.FinishSequence();
         }
      }
   }
}
