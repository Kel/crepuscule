using System;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;

namespace Server.Spells.Druid
{
	public class TreefellowSpell : DruidSpell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"Gardien V�g�tal", "Kes En Sepa Ohm",
				SpellCircle.Eighth,
				269,
				9020,
				false,
				Reagent.BlackPearl,
            		Reagent.Bloodmoss,
            		Reagent.PetrafiedWood
			);

		public TreefellowSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}
		public override double CastDelay{ get{ return 5.0; } }
      	public override double RequiredSkill{ get{ return 50.0; } }
      	public override int RequiredMana{ get{ return 30; } }

		public override bool CheckCast()
		{
			if ( !base.CheckCast() )
				return false;

			if ( (Caster.Followers + 2) > Caster.FollowersMax )
			{
				Caster.SendLocalizedMessage( 1049645 ); // You have too many followers to summon that creature.
				return false;
			}

			return true;
		}

		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 25;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Nature].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie naturelle.");
                    return;
                }
            }
            //Caster.Target = new InternalTarget(this);
			if ( CheckSequence() )
			{
				if ( Core.AOS )
					SpellHelper.Summon( new SummonedTreefellow(), Caster, 0x217, TimeSpan.FromSeconds( 4.0 * Caster.Skills[SkillName.Meditation].Value ), false, false );
				else
					SpellHelper.Summon( new Treefellow(), Caster, 0x217, TimeSpan.FromSeconds( 4.0 * Caster.Skills[SkillName.Meditation].Value ), false, false );
			}

			FinishSequence();
		}
	}
}
