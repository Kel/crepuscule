using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;

namespace Server.Spells.Druid
{
   public class HollowReedSpell : DruidSpell
   {
      private static SpellInfo m_Info = new SpellInfo(
            "Infusion", "En Crur Aeta Sec En Ess ",
            SpellCircle.Second,
            203,
            9061,
            false,
            Reagent.Bloodmoss,
            Reagent.MandrakeRoot,
            Reagent.Nightshade
         );

      public override double CastDelay{ get{ return 1.0; } }
      public override double RequiredSkill{ get{ return 4.0; } }
      public override int RequiredMana{ get{ return 10; } }

      public HollowReedSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
      {
      }

      public override void OnCast()
      {
           if (Caster is RacePlayerMobile) { int apt_besoin = 2;
          RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
          if (Casteur.Capacities[CapacityName.Nature].Value < apt_besoin)
          {
              Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
              Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie naturelle.");
              return;
          }
         }Caster.Target = new InternalTarget( this );
      }

      public void Target( Mobile m )
      {
         if ( !Caster.CanSee( m ) )
         {
            Caster.SendLocalizedMessage( 500237 ); // Target can not be seen.
         }
         else if ( CheckBSequence( m ) )
         {
            SpellHelper.Turn( Caster, m );
            SpellHelper.AddStatBonus( Caster, m, StatType.Str );
            SpellHelper.AddStatBonus( Caster, m, StatType.Dex );
            SpellHelper.AddStatBonus( Caster, m, StatType.Int );

            m.PlaySound( 0x15 );
            m.FixedParticles( 0x373A, 10, 15, 5018, EffectLayer.Waist );

         }

         FinishSequence();
      }

      private class InternalTarget : Target
      {
         private HollowReedSpell m_Owner;

         public InternalTarget( HollowReedSpell owner ) : base( 12, false, TargetFlags.Beneficial )
         {
            m_Owner = owner;
         }

         protected override void OnTarget( Mobile from, object o )
         {
            if ( o is Mobile )
            {
               m_Owner.Target( (Mobile)o );
            }
         }

         protected override void OnTargetFinish( Mobile from )
         {
            m_Owner.FinishSequence();
         }
      }
   }
}
