using System;
using System.Collections;
using Server.Network;
using Server.Items;
using Server.Mobiles;
using Server.Targeting;
using Server.Spells.Druid;

namespace Server.Spells.Druid
{
   public class SpringOfLifeSpell : DruidSpell
   {
      private static SpellInfo m_Info = new SpellInfo(
            "Source de Vie", "En Sepa Aete",
            SpellCircle.Fourth,
            204,
            9061,
            false,
            Reagent.SpringWater
         );

      public override double CastDelay{ get{ return 1.0; } }
      public override double RequiredSkill{ get{ return 38.0; } }
      public override int RequiredMana{ get{ return 25; } }

      public SpringOfLifeSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
      {
      }

      public override void OnCast()
      {
           if (Caster is RacePlayerMobile) { int apt_besoin = 19;
          RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
          if (Casteur.Capacities[CapacityName.Nature].Value < apt_besoin)
          {
              Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
              Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie naturelle.");
              return;
          }
         }Caster.Target = new InternalTarget( this );
      }

      public void Target( IPoint3D p )
      {
         if ( !Caster.CanSee( p ) )
         {
            Caster.SendLocalizedMessage( 500237 ); // Target can not be seen.
         }
         else if ( CheckSequence() )
         {
            SpellHelper.Turn( Caster, p );

            SpellHelper.GetSurfaceTop( ref p );

            ArrayList targets = new ArrayList();

            IPooledEnumerable eable = Caster.Map.GetMobilesInRange( new Point3D( p ), 3 );

            foreach ( Mobile m in eable )
            {
               if ( Caster.CanBeBeneficial( m, false ) )
                  targets.Add( m );
            }

            eable.Free();

            Effects.PlaySound( p, Caster.Map, 0x11 );

            int val = (int)(Caster.Skills[CastSkill].Value/20.0 + 5);

            if ( targets.Count > 0 )
            {
               for ( int i = 0; i < targets.Count; ++i )
               {
                  Mobile m = (Mobile)targets[i];

                  if ( m.BeginAction( typeof( SpringOfLifeSpell ) ) )
                  {
                     Caster.DoBeneficial( m );
                     m.FixedParticles( 0x375A, 9, 20, 5027, EffectLayer.Head );

                     int toHeal = (int)(Caster.Skills[DamageSkill].Value * 0.5);
                     toHeal += Utility.Random( 1, 15 );

                     m.Heal( toHeal );

                     new InternalTimer( m, Caster, val ).Start();
                     m.FixedParticles( 0x375A, 9, 20, 5027, EffectLayer.Waist );
                     m.PlaySound( 0xAF );
                  }
               }
            }
         }

         FinishSequence();
      }

      private class InternalTimer : Timer
      {
         private Mobile m_Owner;
         private int m_Val;

         public InternalTimer( Mobile target, Mobile caster, int val ) : base( TimeSpan.FromSeconds( 0 ) )
         {
            Delay = TimeSpan.FromSeconds( 30 );
            Priority = TimerPriority.TwoFiftyMS;

            m_Owner = target;
            m_Val = val;
         }

         protected override void OnTick()
         {
            m_Owner.EndAction( typeof( SpringOfLifeSpell ) );
        
           
         }
      }

      private class InternalTarget : Target
      {
         private SpringOfLifeSpell m_Owner;

         public InternalTarget( SpringOfLifeSpell owner ) : base( 12, true, TargetFlags.None )
         {
            m_Owner = owner;
         }

         protected override void OnTarget( Mobile from, object o )
         {
            IPoint3D p = o as IPoint3D;

            if ( p != null )
               m_Owner.Target( p );
         }

         protected override void OnTargetFinish( Mobile from )
         {
            m_Owner.FinishSequence();
         }
      }
   }
}
