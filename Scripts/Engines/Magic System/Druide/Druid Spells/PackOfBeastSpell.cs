using System;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;

namespace Server.Spells.Druid
{
   public class PackOfBeastSpell : DruidSpell
   {
      private static SpellInfo m_Info = new SpellInfo(
            "Animaux Sauvages", "En Sec Ohm Ess Sepa",
            SpellCircle.Third,
            266,
            9040,
            false,
            Reagent.BlackPearl,
            Reagent.Bloodmoss,
            Reagent.PetrafiedWood
         );

      public override double CastDelay{ get{ return 6.0; } }
      public override double RequiredSkill{ get{ return 32.0; } }
      public override int RequiredMana{ get{ return 25; } }

      public PackOfBeastSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
      {
      }

      private static Type[] m_Types = new Type[]
         {
            typeof( BrownBear ),
            typeof( TimberWolf ),
            typeof( Panther ),
            typeof( GreatHart ),
            typeof( Hind ),
            typeof( Alligator ),
            typeof( Boar ),
            typeof( GiantRat )
         };

      public override void OnCast()
      {
          if (Caster is RacePlayerMobile)
          {
              int apt_besoin = 16;
              RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
              if (Casteur.Capacities[CapacityName.Nature].Value < apt_besoin)
              {
                  Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                  Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie naturelle.");
                  return;
              }
          }
         if ( CheckSequence() )
         {
            try
            {

               Type beasttype = ( m_Types[Utility.Random( m_Types.Length )] );

               BaseCreature creaturea = (BaseCreature)Activator.CreateInstance( beasttype );
               BaseCreature creatureb = (BaseCreature)Activator.CreateInstance( beasttype );
               BaseCreature creaturec = (BaseCreature)Activator.CreateInstance( beasttype );
               BaseCreature creatured = (BaseCreature)Activator.CreateInstance( beasttype );


               SpellHelper.Summon( creaturea, Caster, 0x215, TimeSpan.FromSeconds( 4.0 * Caster.Skills[CastSkill].Value ), false, false );
               SpellHelper.Summon( creatureb, Caster, 0x215, TimeSpan.FromSeconds( 4.0 * Caster.Skills[CastSkill].Value ), false, false );

               Double morebeast = 0 ;

               morebeast = Utility.Random( 10 ) + ( Caster.Skills[CastSkill].Value * 0.1 );


               if ( morebeast > 11 )
               {
                  SpellHelper.Summon( creaturec, Caster, 0x215, TimeSpan.FromSeconds( 4.0 * Caster.Skills[CastSkill].Value ), false, false );
               }

               if ( morebeast > 18 )
               {
                  SpellHelper.Summon( creatured, Caster, 0x215, TimeSpan.FromSeconds( 4.0 * Caster.Skills[CastSkill].Value ), false, false );
               }


            }
            catch
            {
            }
         }

         FinishSequence();
      }

      public override TimeSpan GetCastDelay()
      {
         return TimeSpan.FromSeconds( 7.5 );
      }
   }
}
