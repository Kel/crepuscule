using System;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;

namespace Server.Spells.Eighth
{
	public class AirElementalSpell : Spell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"Elemental d'air", "Kal Vas Xen Hur",
				SpellCircle.Eighth,
				269,
				9010,
				false,
				Reagent.Bloodmoss,
				Reagent.MandrakeRoot,
				Reagent.SpidersSilk
			);

		public AirElementalSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}

		public override bool CheckCast()
		{
			if ( !base.CheckCast() )
				return false;

			if ( (Caster.Followers + 2) > Caster.FollowersMax )
			{
				Caster.SendLocalizedMessage( 1049645 ); // You have too many followers to summon that creature.
				return false;
			}

			return true;
		}

		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 43;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Wizardry].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie arcanique.");
                    return;
                }
            }
			if ( CheckSequence() )
			{
				TimeSpan duration = TimeSpan.FromSeconds( (2 * Caster.Skills.Magery.Fixed) / 5 );

				if ( Core.AOS )
					SpellHelper.Summon( new SummonedAirElemental(), Caster, 0x217, duration, false, false );
				else
					SpellHelper.Summon( new AirElemental(), Caster, 0x217, duration, false, false );
			}

			FinishSequence();
		}
	}
}