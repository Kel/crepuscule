using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;
using Server.Items;

namespace Server.Spells.Second
{
	public class RemoveTrapSpell : Spell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"D�samor�age magique", "An Jux",
				SpellCircle.Second,
				212,
				9001,
				Reagent.Bloodmoss,
				Reagent.SulfurousAsh
			);

		public RemoveTrapSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}

		public override void OnCast()
		{
             if (Caster is RacePlayerMobile) { int apt_besoin = 12;
            RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
            if (Casteur.Capacities[CapacityName.Wizardry].Value < apt_besoin)
            {
                Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en magie arcanique.");
                return;
            }
			}Caster.Target = new InternalTarget( this );
			Caster.SendMessage( "Que voulez vous d�samor�er?" );
		}

		public void Target( TrapableContainer item )
		{
			if ( !Caster.CanSee( item ) )
			{
				Caster.SendLocalizedMessage( 500237 ); // Target can not be seen.
			}
			else if ( item.TrapType != TrapType.None && item.TrapType != TrapType.MagicTrap )
			{
				base.DoFizzle();
			}
			else if ( CheckSequence() )
			{
				SpellHelper.Turn( Caster, item );

				Point3D loc = item.GetWorldLocation();

				Effects.SendLocationParticles( EffectItem.Create( loc, item.Map, EffectItem.DefaultDuration ), 0x376A, 9, 32, 5015 );
				Effects.PlaySound( loc, item.Map, 0x1F0 );

				item.TrapType = TrapType.None;
				item.TrapPower = 0;
			}

			FinishSequence();
		}

		private class InternalTarget : Target
		{
			private RemoveTrapSpell m_Owner;

			public InternalTarget( RemoveTrapSpell owner ) : base( 12, false, TargetFlags.None )
			{
				m_Owner = owner;
			}

			protected override void OnTarget( Mobile from, object o )
			{
				if ( o is TrapableContainer )
				{
					m_Owner.Target( (TrapableContainer)o );
				}
				else
				{
					from.SendMessage( "You can't disarm that" );
				}
			}

			protected override void OnTargetFinish( Mobile from )
			{
				m_Owner.FinishSequence();
			}
		}
	}
}