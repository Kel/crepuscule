using System;
using System.Collections;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Spells.Necromancy
{
	public class HorrificBeastSpell : TransformationSpell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"Monstruosité", "Rel Xen Vas Bal",
				SpellCircle.Sixth,
				203,
				9031,
				Reagent.BatWing,
				Reagent.DaemonBlood
			);

		public override double RequiredSkill{ get{ return 50.0; } }
		public override int RequiredMana{ get{ return 40; } }

		public override int Body{ get{ return 746; } }

		public HorrificBeastSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}

        public override void OnCast()
        {
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 29;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if ((Casteur.Capacities[CapacityName.Necromancy].Value + Casteur.Capacities[CapacityName.BloodMagic].Value) < apt_besoin)
                {
                    Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en necromancie.");
                    return;
                }
            }
            base.OnCast();
        }


		public override void PlayEffect( Mobile m )
		{
			m.PlaySound( 0x165 );
			m.FixedParticles( 0x3728, 1, 13, 9918, 92, 3, EffectLayer.Head );
		}
	}
}