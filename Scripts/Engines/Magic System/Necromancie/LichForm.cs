using System;
using System.Collections;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Spells.Necromancy
{
	public class LichFormSpell : TransformationSpell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"Liche", "Rel Xen Corp Ort",
				SpellCircle.Sixth,
				203,
				9031,
				Reagent.GraveDust,
				Reagent.DaemonBlood,
				Reagent.NoxCrystal
			);

		public override double RequiredSkill{ get{ return 60.0; } }
		public override int RequiredMana{ get{ return 25; } }

		public override int Body{ get{ return 749; } }

		public override int FireResistOffset{ get{ return -25; } }
		public override int ColdResistOffset{ get{ return +10; } }
		public override int PoisResistOffset{ get{ return +10; } }

		public override double TickRate{ get{ return 2.5; } }

		public LichFormSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}

		public override void PlayEffect( Mobile m )
		{
			m.PlaySound( 0x19C );
			m.FixedParticles( 0x3709, 1, 30, 9904, 1108, 6, EffectLayer.RightFoot );
		}

        public override void OnCast()
        {
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 35;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if ((Casteur.Capacities[CapacityName.Necromancy].Value + Casteur.Capacities[CapacityName.BloodMagic].Value) < apt_besoin)
                {
                    Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en necromancie.");
                    return;
                }
            }
            base.OnCast();
        }
		public override void OnTick( Mobile m )
		{
			--m.Hits;
		}
	}
}