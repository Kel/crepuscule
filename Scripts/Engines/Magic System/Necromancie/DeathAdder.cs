using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Network;

namespace Server.Mobiles
{
	[CorpseName( "a death adder corpse" )]
	public class DeathAdderFamiliar : BaseFamiliar
	{
		public DeathAdderFamiliar()
		{
			Name = "a Death Adder";
			Body = 0x15;
			Hue = Utility.RandomSnakeHue();
			BaseSoundID = 219;

			SetStr( 200 );
			SetDex( 60 );
			SetInt( 100 );

			SetHits( 200 );
			SetStam( 60 );
			SetMana( 0 );

			SetDamage( 4, 8 );

			SetDamageType( ResistanceType.Physical, 50 );
			SetDamageType( ResistanceType.Poison, 50 );

			SetResistance( ResistanceType.Physical, 10, 15 );
			SetResistance( ResistanceType.Fire, 20, 25 );
			SetResistance( ResistanceType.Cold, 0, 5 );
			SetResistance( ResistanceType.Poison, 40, 45 );
			SetResistance( ResistanceType.Energy, 10, 15 );

			SetSkill( SkillName.Wrestling, 75.1, 80.0 );
			SetSkill( SkillName.Tactics, 40.0,45.1 );

			ControlSlots = 1;
		}

		public DeathAdderFamiliar( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}