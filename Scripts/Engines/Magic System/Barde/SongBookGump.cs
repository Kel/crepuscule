using System; 
using System.Collections; 
using Server; 
using Server.Items; 
using Server.Network; 
using Server.Spells; 
using Server.Spells.Song; 
using Server.Prompts;
using Server.Targeting; 
 

namespace Server.Gumps 
{ 
   public class SongBookGump : Gump 
   { 
      private SongBook m_Book; 
       
      int gth = 0x903; 
      private void AddBackground() 
      { 
         AddPage( 0 ); 
         AddImage( 100, 10, 0x89B, 0 ); 
      } 
       
      public static bool HasSpell( Mobile from, int spellID ) 
      { 
         Spellbook book = Spellbook.Find( from, spellID ); 
         return ( book != null && book.HasSpell( spellID ) ); 
      } 
       
      public SongBookGump( Mobile from, SongBook book ) : base( 150, 200 ) 
      { 
          
         m_Book = book; 
         AddBackground(); 
         AddPage( 1 ); 
         AddLabel( 150, 17, gth, "Livre de chant" ); 
         int sbtn = 0x93A; 
         int dby = 40; 
         int dbpy = 40;; 
         AddButton( 396, 14, 0x89E, 0x89E, 17, GumpButtonType.Page, 2 ); 

	     AddLabel(306, 17, 0, @"l'instrument");
	     AddButton(291, 21, sbtn, sbtn, 20, GumpButtonType.Reply, 0 );

         if (HasSpell( from, 351) ) 
         { 
            AddLabel( 145, dbpy, gth, "Chant de soin" ); 
            AddButton( 125, dbpy + 3, sbtn, sbtn, 1, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 352) ) 
         { 
            AddLabel( 145, dby, gth, "Chant de concentration" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 2, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 353) ) 
         { 
            AddLabel( 145, dby, gth, "Musique du feu" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 3, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 354) ) 
         { 
            AddLabel( 145, dby, gth, "M�lodie de Cerius" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 4, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 355) ) 
         { 
            AddLabel( 145, dby, gth, "Chant du feu" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 5, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 356) ) 
         { 
            AddLabel( 145, dby, gth, "M�lodie de Trisunie" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 6, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 357) ) 
         { 
            AddLabel( 145, dby, gth, "M�lodie de Risu" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 7, GumpButtonType.Reply, 1 ); 
            dby = dby + 20; 
         } 
         if (HasSpell( from, 358) ) 
         { 
            AddLabel( 145, dby, gth, "Chant Amon" ); 
            AddButton( 125, dby + 3, sbtn, sbtn, 8, GumpButtonType.Reply, 1 ); 
         } 
         if (HasSpell( from, 359) ) 
         { 
            AddLabel( 315, dbpy, gth, "M�lodie Amon" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 9, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 360) ) 
         { 
            AddLabel( 315, dbpy, gth, "Chant de chevalier" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 10, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 361) ) 
         { 
            AddLabel( 315, dbpy, gth, "Ballade de mage" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 11, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 362) ) 
         { 
            AddLabel( 315, dbpy, gth, "Chant Spheral" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 12, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 363) ) 
         { 
            AddLabel( 315, dbpy, gth, "Chant Prosali" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 13, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 364) ) 
         { 
            AddLabel( 315, dbpy, gth, "M�lodie Ygdra" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 14, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 365) ) 
         { 
            AddLabel( 315, dbpy, gth, "Ballade d'artiste" ); 
            AddButton( 295, dbpy + 3, sbtn, sbtn, 15, GumpButtonType.Reply, 1 ); 
            dbpy = dbpy + 20; 
         } 
         if (HasSpell( from, 366) ) 
         { 
            AddLabel( 315, dby, gth, "Ballade de hache" ); 
            AddButton( 295, dby + 3, sbtn, sbtn, 16, GumpButtonType.Reply, 1 ); 
         } 
          
         int i = 2; 
          
         if (HasSpell( from, 351) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant de soin" ); 
            AddHtml( 130, 59, 123, 132, "Guerit le party lentement. [Effect de zone]", false, false ); 
//            AddLabel( 295, 37, gth, "R�actifs:" ); 
//            AddLabel( 295, 57, gth, "Souffre" ); 
//            AddLabel( 295, 77, gth, "Ange d�truit" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 70" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
          
         if (HasSpell( from, 352) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant de concentration" ); 
            AddHtml( 130, 59, 123, 132, "Vos compagnons deviennent plus intelligents.  [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Racine de mandagore" ); 
            //            AddLabel( 295, 97, gth, "Belladonne" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 60" ); 
            AddLabel( 295, 80, gth, "Mana requise: 15" ); 
            i++; 
         } 
          
         if (HasSpell( from, 353) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Musique du feu" ); 
            AddHtml( 130, 59, 123, 132, "Augmente la resistance de groupe. [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Perle noire" ); 
            //            AddLabel( 295, 97, gth, "Bois attrofi�" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 35" ); 
            AddLabel( 295, 80, gth, "Mana requise: 7" ); 
            i++; 
         } 
         if (HasSpell( from, 354) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "M�lodie de Cerius" ); 
            AddHtml( 130, 59, 123, 132, "Diminue la resistance d'un de vos ennemis.", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Eau de source" ); 
            //            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 30" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 355) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant du feu" ); 
            AddHtml( 130, 59, 123, 132, "Augmente la resistance de groupe [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Eau de source" ); 
            //            AddLabel( 295, 97, gth, "Toile d'araign�e" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 70" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 356) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "M�lodie de Trisunie" ); 
            AddHtml( 130, 59, 123, 132, "Diminue la resistance d'un de vos ennemis", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Eau de source" ); 
            //            AddLabel( 295, 97, gth, "Toile d'araign�e" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 70" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 357) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "M�lodie de Risu" ); 
            AddHtml( 130, 59, 123, 132, "Une m�lodie affreuse, qui endommage vos ennemis.", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Eau de source" ); 
            //            AddLabel( 295, 97, gth, "Toile d'araign�e" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 30" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 358) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant Amon" ); 
            AddHtml( 130, 59, 123, 132, "Augmente la resistance de froid. [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Belladonne" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 55" ); 
            AddLabel( 295, 80, gth, "Mana requise: 18" ); 
            i++; 
         } 
         if (HasSpell( from, 359) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "M�lodie Amon" ); 
            AddHtml( 130, 59, 123, 132, "Diminue la resistance au froid d'un de vos ennemis.", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Aille" ); 
            //            AddLabel( 295, 77, gth, "Belladonne" ); 
            //            AddLabel( 295, 97, gth, "Ange d�truit" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 45" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 360) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant de chevalier" ); 
            AddHtml( 130, 59, 123, 132, "Augmente la resistance physique du groupe. [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Souffre" ); 
            //            AddLabel( 295, 77, gth, "Ange d�truit" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 35" ); 
            AddLabel( 295, 80, gth, "Mana requise: 7" ); 
            i++; 
         } 
         if (HasSpell( from, 361) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Ballade de mage" ); 
            AddHtml( 130, 59, 123, 132, "Vos compagnons mages se concentrent plus vite. [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 77, gth, "Perle noire" ); 
            //            AddLabel( 295, 97, gth, "Bois attrofi�" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 80" ); 
            AddLabel( 295, 80, gth, "Mana requise: 15" ); 
            i++; 
         } 
         if (HasSpell( from, 362) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant Spheral" ); 
            AddHtml( 130, 59, 123, 132, "*cette page est arrach�e, l'effet est inconnu*", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Perle noire" ); 
            //            AddLabel( 295, 77, gth, "Ginseng" ); 
            //            AddLabel( 295, 97, gth, "Eau de source" ); 
            //AddLabel( 295, 60, gth, "Capacit� requise: 65" ); 
            AddLabel( 295, 80, gth, "Mana requise: 15" ); 
            i++; 
         } 
         if (HasSpell( from, 363) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Chant Prosali" ); 
            AddHtml( 130, 59, 123, 132, "Augmente la resistance de poison.  [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Bois attrofi�" ); 
            //            AddLabel( 295, 77, gth, "Racine de mandagore" ); 
            //            AddLabel( 295, 97, gth, "Eau de source" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 35" ); 
            AddLabel( 295, 80, gth, "Mana requise: 7" ); 
            i++; 
         } 
         if (HasSpell( from, 364) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "M�lodie Ygdra" ); 
            AddHtml( 130, 59, 123, 132, "Diminue la resistance de poison d'un de vos ennemis.", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Perle noire" ); 
            //            AddLabel( 295, 77, gth, "Eau de source" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 30" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 365) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Ballade d'artiste" ); 
            AddHtml( 130, 59, 123, 132, "Augmente l'agilit� de partie. [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Perle noire" ); 
            //            AddLabel( 295, 77, gth, "Mousse de sang" ); 
            //            AddLabel( 295, 97, gth, "Racine de mandagore" ); 
			AddLabel( 295, 60, gth, "Capacit� requise: 70" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
         if (HasSpell( from, 366) ) 
         { 
            AddPage( i ); 
            AddButton( 396, 14, 0x89E, 0x89E, 18, GumpButtonType.Page, i+1 ); 
            AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
            AddLabel( 150, 37, gth, "Ballade de hache" ); 
            AddHtml( 130, 59, 123, 132, "Augmente la force de vos compagnons. [Effect de zone]", false, false ); 
            //            AddLabel( 295, 37, gth, "R�actifs:" ); 
            //            AddLabel( 295, 57, gth, "Perle noire" ); 
            //            AddLabel( 295, 77, gth, "Eau de source" ); 
            //            AddLabel( 295, 97, gth, "Racine de mandagore" ); 
            AddLabel( 295, 60, gth, "Capacit� requise: 70" ); 
            AddLabel( 295, 80, gth, "Mana requise: 12" ); 
            i++; 
         } 
          
        AddPage( i ); 
        AddButton( 123, 15, 0x89D, 0x89D, 19, GumpButtonType.Page, i-1 ); 
      } 
       
      public override void OnResponse( NetState state, RelayInfo info )
      {
         Mobile from = state.Mobile; 

		//if book has a nearby instrument assigned then play that tune, else target an instrument
		 if ( m_Book.Instrument == null || !(from.InRange( m_Book.Instrument.GetWorldLocation(), 1 )) )
			{
				from.SendMessage( "Vous avez besoin d'un instrument! " );
				from.SendMessage( "Choisissez un instrument. " );
                from.Target = new InternalTarget( m_Book );
			}
		else
		  {
         switch ( info.ButtonID ) 
         { 
            case 0:
            {
               break;
            }
            case 1:
            {
               new ArmysPaeonSong( from, null).Cast();
               break;
            }
            case 2:
            {
               new EnchantingEtudeSong( from, null).Cast();
               break;
            }
            case 3:
            {
               new EnergyCarolSong( from, null).Cast();
               break;
            }
            case 4:
            {
               new EnergyThrenodySong( from, null).Cast();
               break;
            }
            case 5:
            {
               new FireCarolSong( from, null).Cast();
               break;
            }
            case 6:
            {
               new FireThrenodySong( from, null).Cast();
               break;
            }
            case 7:
            {
               new FoeRequiemSong( from, null).Cast();
               break;
            }
            case 8:
            {
               new IceCarolSong( from, null).Cast();
               break;
            }
            case 9:
            {
               new IceThrenodySong( from, null).Cast();
               break;
            }
            case 10:
            {
               new KnightsMinneSong( from, null).Cast();
               break;
            }
            case 11:
            {
               new MagesBalladSong( from, null).Cast();
               break;
            }
            case 12:
            {
               new MagicFinaleSong( from, null).Cast();
               break;
            }
             case 13:
            {
               new PoisonCarolSong( from, null).Cast();
               break;
            }
            case 14:
            {
               new PoisonThrenodySong( from, null).Cast();
               break;
            }
            case 15:
            {
               new SheepfoeMamboSong( from, null).Cast();
               break;
            }
            case 16:
            {
               new SinewyEtudeSong( from, null).Cast();
               break;
            }
            case 20:
            {
               from.Target = new InternalTarget( m_Book );
	       break;
            }
		 }
         } 
      } 

      private class InternalTarget : Target
      {
	      private SongBook Book;
			
	      public InternalTarget( SongBook book ) : base( 1, false, TargetFlags.None ) 
	        {
		        Book = book;
	        }
			
		  protected override void OnTarget( Mobile from, object target )
		  {
			 if ( target is BaseInstrument )
			 {
			Book.Instrument = (BaseInstrument)target;
			 }
			 else
			from.SendMessage( "That is not an instrument!" );
		  }
      }

   } 
}
