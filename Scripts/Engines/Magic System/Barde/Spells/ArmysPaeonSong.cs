using System;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Gumps;
using Server.Spells;

namespace Server.Spells.Song
{
	public class ArmysPaeonSong : Song
	{
		
		private static SpellInfo m_Info = new SpellInfo(
			"Chant de Soin", "Paeonus",
			SpellCircle.First,
			212,
			9041
			);
	
		private SongBook m_Book;
		public override double CastDelay{ get{ return 3; } }
		public override double RequiredSkill{ get{ return 22.0; } }
		public override int RequiredMana{ get{ return 20; } }
		
		public ArmysPaeonSong( Mobile caster, Item scroll) : base( caster, scroll, m_Info )
		{
		}
		
		public override void OnCast()
		{
//			if ( m_Book.Instrument == null || !(Caster.InRange( m_Book.Instrument.GetWorldLocation(), 1 )) )
//			{
//				Caster.SendMessage( "Vous avez besoin d'un instrument pour jouer cette m�lodie!" );
//			}
//			else if( CheckSequence() )

//get songbook instrument
//			    Spellbook book = Spellbook.Find( Caster, -1, SpellbookType.Song );
//				m_Book=(SongBook) book;
//				Caster.PlaySound( m_Book.Instrument.SuccessSound );

            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 15;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Bardic].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vous vous trompez dans les notes, vous ratez votre chant.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en bardisme.");
                    return;
                }
            }

			if( CheckSequence() )
			{
				ArrayList targets = new ArrayList();

				foreach ( Mobile m in Caster.GetMobilesInRange( 5 ) )
				{
					if ( Caster.CanBeBeneficial( m, false, true ) && !(m is Golem) )
						targets.Add( m );
				}

				for ( int i = 0; i < targets.Count; ++i )
				{
					Mobile m = (Mobile)targets[i];
					
					TimeSpan duration = TimeSpan.FromSeconds( Caster.Skills[SkillName.Musicianship].Value * 2.5 ); 
					int rounds = (int)( Caster.Skills[SkillName.Musicianship].Value * .16 );
						
					new ExpireTimer( m, 0, rounds, TimeSpan.FromSeconds( 2 ) ).Start();
						
					m.FixedParticles( 0x376A, 9, 32, 5030, 0x21, 3, EffectLayer.Waist );
//					m.PlaySound( 0x1F2 );
					
				}
			}
			
			FinishSequence();
		}

		private class ExpireTimer : Timer
		{
			private Mobile m_Mobile;
			private int m_Round;
			private int m_Totalrounds;

			public ExpireTimer( Mobile m, int round, int totalrounds, TimeSpan delay ) : base( delay )
			{
				m_Mobile = m;
				m_Round = round;
				m_Totalrounds = totalrounds;
			}
			
			protected override void OnTick()
			{
				if ( m_Mobile != null )
				{
										
					m_Mobile.Hits += 5;
					
					if ( m_Round >= m_Totalrounds )
					{
						m_Mobile.SendMessage( "L'effet du chant disparait." );
						
					}
					else
					{
						m_Round += 1;
						new ExpireTimer( m_Mobile, m_Round, m_Totalrounds, TimeSpan.FromSeconds( 2 ) ).Start();
					}
				}
			}
		}
	}
}
