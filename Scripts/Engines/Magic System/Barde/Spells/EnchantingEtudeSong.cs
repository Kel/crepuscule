using System;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Gumps;
using Server.Spells;

namespace Server.Spells.Song
{
	public class EnchantingEtudeSong : Song
	{
	
		private static SpellInfo m_Info = new SpellInfo(
			"Chant de concentration", "Enchantendre",
			SpellCircle.First,
			212,
			9041
			);
		
		private SongBook m_Book;
		public override double CastDelay{ get{ return 3; } }
		public override double RequiredSkill{ get{ return 42.0; } }
		public override int RequiredMana{ get{ return 20; } }

		public EnchantingEtudeSong( Mobile caster, Item scroll) : base( caster, scroll, m_Info )
		{
		}
		
		public override void OnCast()
		{
            try
            {
                if (Caster is RacePlayerMobile)
                {
                    int apt_besoin = 12;
                    RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                    if (Casteur.Capacities[CapacityName.Bardic].Value < apt_besoin)
                    {
                        Casteur.SendMessage("Vous vous trompez dans les notes, vous ratez votre chant.");
                        Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en bardisme.");
                        return;
                    }
                }

                //get songbook instrument
                /*Spellbook book = Spellbook.Find(Caster, -1, SpellbookType.Song);

                m_Book = (SongBook)book;
                Caster.PlaySound(m_Book.Instrument.SuccessSound);*/

                if (CheckSequence())
                {
                    ArrayList targets = new ArrayList();

                    foreach (Mobile m in Caster.GetMobilesInRange(3))
                    {
                        if (Caster.CanBeBeneficial(m, false, true) && !(m is Golem))
                            targets.Add(m);
                    }


                    for (int i = 0; i < targets.Count; ++i)
                    {
                        Mobile m = (Mobile)targets[i];

                        Mobile source = Caster;

                        int amount = (int)(Caster.Skills[SkillName.Musicianship].Base * 0.2);
                        string intt = "int";

                        double duration = (Caster.Skills[SkillName.Musicianship].Base * 2.5);

                        StatMod mod = new StatMod(StatType.Int, intt, +amount, TimeSpan.FromSeconds(duration));

                        m.AddStatMod(mod);

                        m.FixedParticles(0x375A, 10, 15, 5017, 0x1F8, 3, EffectLayer.Waist);

                    }
                }

                FinishSequence();
            }
            catch { }
		}
	}
}
