using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;
using Server.Items;

namespace Server.Spells.Song
{
	public class IceThrenodySong : Song
	{
		
		private static SpellInfo m_Info = new SpellInfo(
				"M�lodie Amon", "Chillinum",
				SpellCircle.First,
				212,9041
			);
		
		public IceThrenodySong( Mobile caster, Item scroll) : base( caster, scroll, m_Info )
		{
			
		}
		
		private SongBook m_Book;
		public override double CastDelay{ get{ return 2; } }
		public override double RequiredSkill{ get{ return 30.0; } }
		public override int RequiredMana{ get{ return 12; } }

		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 6;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Bardic].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vous vous trompez dans les notes, vous ratez votre chant.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en bardisme.");
                    return;
                }
            }

			Caster.Target = new InternalTarget( this );
		}

		public void Target( Mobile m )
		{
			PlayerMobile p = m as PlayerMobile;
			
			if ( !Caster.CanSee( m ) )
			{
				Caster.SendLocalizedMessage( 500237 ); // Target can not be seen.
			}
            else if ( CheckHSequence( m ) )
			{
								
				
//get songbook instrument
			    /*Spellbook book = Spellbook.Find( Caster, -1, SpellbookType.Song );
				m_Book=(SongBook) book;
				Caster.PlaySound( m_Book.Instrument.SuccessSound );*/

				Mobile source = Caster;
				SpellHelper.Turn( source, m );

				SpellHelper.CheckReflect( (int)this.Circle, ref source, ref m );
				
				m.FixedParticles( 0x374A, 10, 30, 5013, 0x480, 2, EffectLayer.Waist );
				
				int amount = (int)( Caster.Skills[SkillName.Musicianship].Base * 0.4 );
				TimeSpan duration = TimeSpan.FromSeconds( Caster.Skills[SkillName.Musicianship].Base * 2.5 ); 
				
				//m.SendMessage( "Your ice resistance has decreased." );
				ResistanceMod mod1 = new ResistanceMod( ResistanceType.Cold, - amount );
				
				m.AddResistanceMod( mod1 );

				ExpireTimer timer1 = new ExpireTimer( m, mod1, duration );
				timer1.Start();
			}
			
			FinishSequence();
		}

		private class ExpireTimer : Timer
		{
			private Mobile m_Mobile;
			private ResistanceMod m_Mods;

			public ExpireTimer( Mobile m, ResistanceMod mod, TimeSpan delay ) : base( delay )
			{
				m_Mobile = m;
				m_Mods = mod;
			}

			public void DoExpire()
			{
				PlayerMobile p = m_Mobile as PlayerMobile;
				m_Mobile.RemoveResistanceMod( m_Mods );
				
				Stop();
			}

			protected override void OnTick()
			{
				if ( m_Mobile != null )
				{
					//m_Mobile.SendMessage( "The effect of Ice Threnody wears off." );
					DoExpire();
				}
			}
		}

		private class InternalTarget : Target
		{
			private IceThrenodySong m_Owner;

			public InternalTarget( IceThrenodySong owner ) : base( 12, false, TargetFlags.Harmful )
			{
				m_Owner = owner;
			}

			protected override void OnTarget( Mobile from, object o )
			{
				if ( o is Mobile )
					m_Owner.Target( (Mobile)o );
			}

			protected override void OnTargetFinish( Mobile from )
			{
				m_Owner.FinishSequence();
			}
		}
	}
}
