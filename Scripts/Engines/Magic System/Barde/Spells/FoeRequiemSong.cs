using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;
using Server.Items;

namespace Server.Spells.Song
{
	public class FoeRequiemSong : Song
	{
		
		private static SpellInfo m_Info = new SpellInfo(
                "M�lodie de Risu", "Sonicus",
				SpellCircle.Sixth,
				212,9041
			);
		
		public FoeRequiemSong( Mobile caster, Item scroll) : base( caster, scroll, m_Info )
		{
			
		}
		
		private SongBook m_Book;
		public override double CastDelay{ get{ return 3; } }
		public override double RequiredSkill{ get{ return 20.0; } }
		public override int RequiredMana{ get{ return 7; } }
		
		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 20;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Bardic].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vous vous trompez dans les notes, vous ratez votre chant.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en bardisme.");
                    return;
                }
            }

			Caster.Target = new InternalTarget( this );
		}

		public void Target( Mobile m )
		{
			if ( !Caster.CanSee( m ) )
			{
				Caster.SendLocalizedMessage( 500237 ); // Target can not be seen.
			}
			else if ( CheckHSequence( m ) )
			{
//get songbook instrument
			    /*Spellbook book = Spellbook.Find( Caster, -1, SpellbookType.Song );
				m_Book=(SongBook) book;
				Caster.PlaySound( m_Book.Instrument.SuccessSound );*/

				Mobile source = Caster;
				
				SpellHelper.Turn( Caster, m );

				double damage = ( Utility.Random( 35, 33 ) + Utility.Random( 10, 5 ) );

				if ( CheckResisted( m ) )
				{
					m.SendLocalizedMessage( 501783 ); // You feel yourself resisting magical energy.
					damage *= .4;
				}
				
				//sound damage, all resistances
				SpellHelper.Damage( this, m, damage, 30, 30, 30, 30, 30 );

				m.FixedParticles( 0x374A, 10, 15, 5028, EffectLayer.Head );
                                source.MovingParticles( m, 0x379F, 7, 0, false, true, 3043, 4043, 0x211 );
				m.PlaySound( 0x1EA );
			}
			
			FinishSequence();
		}
		
		private class InternalTarget : Target
		{
			private FoeRequiemSong m_Owner;

			public InternalTarget( FoeRequiemSong owner ) : base( 12, false, TargetFlags.Harmful )
			{
				m_Owner = owner;
			}

			protected override void OnTarget( Mobile from, object o )
			{
				if ( o is Mobile )
					m_Owner.Target( (Mobile)o );
			}

			protected override void OnTargetFinish( Mobile from )
			{
				m_Owner.FinishSequence();
			}
		}
	}
}
