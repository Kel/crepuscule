using System;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Gumps;
using Server.Spells;

namespace Server.Spells.Song
{
	public class EnergyCarolSong : Song
	{
		
		private static SpellInfo m_Info = new SpellInfo(
				"Musique du feu", "Energious",
				SpellCircle.First,
				212,9041
			);
		
		private SongBook m_Book;
		public override double CastDelay{ get{ return 3; } }
		public override double RequiredSkill{ get{ return 30.0; } }
		public override int RequiredMana{ get{ return 25; } }

		public EnergyCarolSong( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}
		
		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 6;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Bardic].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vous vous trompez dans les notes, vous ratez votre chant.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en bardisme.");
                    return;
                }
            }

//get songbook instrument
			    /*Spellbook book = Spellbook.Find( Caster, -1, SpellbookType.Song );
				m_Book=(SongBook) book;
				Caster.PlaySound( m_Book.Instrument.SuccessSound );*/
			if( CheckSequence() )
			{
				ArrayList targets = new ArrayList();

				foreach ( Mobile m in Caster.GetMobilesInRange( 3 ) )
				{
					if ( Caster.CanBeBeneficial( m, false, true ) && !(m is Golem) )
						targets.Add( m );
				}
				
				
				for ( int i = 0; i < targets.Count; ++i )
				{
					Mobile m = (Mobile)targets[i];
					
					TimeSpan duration = TimeSpan.FromSeconds( Caster.Skills[SkillName.Musicianship].Value * 2.5 ); 
					int amount = (int)( Caster.Skills[SkillName.Musicianship].Value * .4 );
						
					m.SendMessage( "Votre resistance aux energies � �t� augment�e." );
					ResistanceMod mod1 = new ResistanceMod( ResistanceType.Energy, + amount );
						
					m.FixedParticles( 0x373A, 10, 15, 5012, 0x14, 3, EffectLayer.Waist );
						
					m.AddResistanceMod( mod1 );
						
					new ExpireTimer( m, mod1, duration ).Start();
					
				}
			}
			
			FinishSequence();
		}

		private class ExpireTimer : Timer
		{
			private Mobile m_Mobile;
			private ResistanceMod m_Mods;

			public ExpireTimer( Mobile m, ResistanceMod mod, TimeSpan delay ) : base( delay )
			{
				m_Mobile = m;
				m_Mods = mod;
			}

			public void DoExpire()
			{
				m_Mobile.RemoveResistanceMod( m_Mods );
				
				Stop();
			}

			protected override void OnTick()
			{
				if ( m_Mobile != null )
				{
					
					DoExpire();
				}
			}
		}
	}
}
