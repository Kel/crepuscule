using System;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.Network;
using Server.Items;
using Server.Targeting;
using Server.Gumps;
using Server.Spells;

namespace Server.Spells.Song
{
	public class SinewyEtudeSong : Song
	{
		
		private static SpellInfo m_Info = new SpellInfo(
                "Ballade de hache", "Fortitus",
				SpellCircle.First,
				212,9041
			);

		private SongBook m_Book;
		public override double CastDelay{ get{ return 3; } }
		public override double RequiredSkill{ get{ return 50.0; } }
		public override int RequiredMana{ get{ return 40; } }
		
		public SinewyEtudeSong( Mobile caster, Item scroll) : base( caster, scroll, m_Info )
		{
			
		}
		
		public override void OnCast()
		{
            if (Caster is RacePlayerMobile)
            {
                int apt_besoin = 16;
                RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
                if (Casteur.Capacities[CapacityName.Bardic].Value < apt_besoin)
                {
                    Casteur.SendMessage("Vous vous trompez dans les notes, vous ratez votre chant.");
                    Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en bardisme.");
                    return;
                }
            }
//get songbook instrument
			   /* Spellbook book = Spellbook.Find( Caster, -1, SpellbookType.Song );
				m_Book=(SongBook) book;
				Caster.PlaySound( m_Book.Instrument.SuccessSound );*/

			if( CheckSequence() )
			{
				ArrayList targets = new ArrayList();

				foreach ( Mobile m in Caster.GetMobilesInRange( 3 ) )
				{
					if ( Caster.CanBeBeneficial( m, false, true ) && !(m is Golem) )
						targets.Add( m );
				}
				
				
				for ( int i = 0; i < targets.Count; ++i )
				{
					Mobile m = (Mobile)targets[i];
					
					Mobile source = Caster;
						
					int amount = (int)( Caster.Skills[SkillName.Musicianship].Base * 0.2 );
					string str = "str";
						
					double duration = ( Caster.Skills[SkillName.Musicianship].Base * 2.5 ); 
						
					StatMod mod = new StatMod( StatType.Str, str, + amount, TimeSpan.FromSeconds( duration ) );
						
					m.AddStatMod( mod );
						
					m.FixedParticles( 0x375A, 10, 15, 5017, 0x224, 3, EffectLayer.Waist );
					
				}
			}
			
			FinishSequence();
		}
	}
}
