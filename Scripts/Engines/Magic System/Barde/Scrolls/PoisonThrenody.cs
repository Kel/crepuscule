using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class PoisonThrenodyScroll : SpellScroll
	{
		[Constructable]
		public PoisonThrenodyScroll() : this( 1 )
		{
                  Name = "M�lodie Ygdra notes";
		}

		[Constructable]
		public PoisonThrenodyScroll( int amount ) : base( 364, 0x14ED, amount )
		{
                  Name = "M�lodie Ygdra notes";
		          Hue = 0x96;
        }

		public PoisonThrenodyScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new PoisonThrenodyScroll( amount ), amount );
		}
	}
}