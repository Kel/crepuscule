using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class PoisonCarolScroll : SpellScroll
	{
		[Constructable]
		public PoisonCarolScroll() : this( 1 )
		{
                  Name = "Chant Prosali notes";
		}

		[Constructable]
		public PoisonCarolScroll( int amount ) : base( 363, 0x14ED, amount )
		{
                  Name = "Chant Prosali notes";
		          Hue = 0x96;
        }

		public PoisonCarolScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new PoisonCarolScroll( amount ), amount );
		}
	}
}