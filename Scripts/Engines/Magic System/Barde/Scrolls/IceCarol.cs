using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class IceCarolScroll : SpellScroll
	{
		[Constructable]
		public IceCarolScroll() : this( 1 )
		{
                  Name = "Chant Amon notes";
		}

		[Constructable]
		public IceCarolScroll( int amount ) : base( 358, 0x14ED, amount )
		{
                  Name = "Chant Amon notes";
		          Hue = 0x96;
        }

		public IceCarolScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new IceCarolScroll( amount ), amount );
		}
	}
}