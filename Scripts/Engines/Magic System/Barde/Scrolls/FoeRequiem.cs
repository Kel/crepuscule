using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class FoeRequiemScroll : SpellScroll
	{
		[Constructable]
		public FoeRequiemScroll() : this( 1 )
		{
                  Name = "M�lodie de Risu notes";
		}

		[Constructable]
		public FoeRequiemScroll( int amount ) : base( 357, 0x14ED, amount )
		{
                  Name = "M�lodie de Risu notes";
		          Hue = 0x96;
        }

		public FoeRequiemScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new FoeRequiemScroll( amount ), amount );
		}
	}
}