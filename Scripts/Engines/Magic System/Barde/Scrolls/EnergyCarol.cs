using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class EnergyCarolScroll : SpellScroll
	{
		[Constructable]
		public EnergyCarolScroll() : this( 1 )
		{
                  Name = "Musique du feu notes";
		}

		[Constructable]
		public EnergyCarolScroll( int amount ) : base( 353, 0x14ED, amount )
		{
                  Name = "Musique du feu notes";
			      Hue = 0x96;
        }

		public EnergyCarolScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new EnergyCarolScroll( amount ), amount );
		}
	}
}