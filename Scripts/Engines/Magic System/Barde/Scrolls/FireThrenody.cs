using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class FireThrenodyScroll : SpellScroll
	{
		[Constructable]
		public FireThrenodyScroll() : this( 1 )
		{
                  Name = "M�lodie de Trisunie notes";
		}

		[Constructable]
		public FireThrenodyScroll( int amount ) : base( 356, 0x14ED, amount )
		{
                  Name = "M�lodie de Trisunie notes";
		          Hue = 0x96;
        }

		public FireThrenodyScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new FireThrenodyScroll( amount ), amount );
		}
	}
}