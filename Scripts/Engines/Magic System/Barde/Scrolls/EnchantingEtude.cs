using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class EnchantingEtudeScroll : SpellScroll
	{
		[Constructable]
		public EnchantingEtudeScroll() : this( 1 )
		{
                  Name = "Chant de concentration notes";
		}

		[Constructable]
		public EnchantingEtudeScroll( int amount ) : base( 352, 0x14ED, amount )
		{
                  Name = "Chant de concentration notes";
		          Hue = 0x96;
        }

		public EnchantingEtudeScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new EnchantingEtudeScroll( amount ), amount );
		}
	}
}