using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class EnergyThrenodyScroll : SpellScroll
	{
		[Constructable]
		public EnergyThrenodyScroll() : this( 1 )
		{
                  Name = "M�lodie de Cerius notes";
		}

		[Constructable]
		public EnergyThrenodyScroll( int amount ) : base( 354, 0x14ED, amount )
		{
                  Name = "M�lodie de Cerius notes";
		          Hue = 0x96;
        }

		public EnergyThrenodyScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new EnergyThrenodyScroll( amount ), amount );
		}
	}
}