using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class MagesBalladScroll : SpellScroll
	{
		[Constructable]
		public MagesBalladScroll() : this( 1 )
		{
                  Name = "Ballade de mage notes";
		}

		[Constructable]
		public MagesBalladScroll( int amount ) : base( 361, 0x14ED, amount )
		{
                  Name = "Ballade de mage notes";
		          Hue = 0x96;
        }

		public MagesBalladScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new MagesBalladScroll( amount ), amount );
		}
	}
}