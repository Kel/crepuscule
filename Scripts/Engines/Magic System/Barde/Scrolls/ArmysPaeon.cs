using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class ArmysPaeonScroll : SpellScroll
	{
		[Constructable]
		public ArmysPaeonScroll() : this( 1 )
		{
                  Name = "Chant de soin notes";
		}

		[Constructable]
		public ArmysPaeonScroll( int amount ) : base( 351, 0x14ED, amount )
		{
                Name = "Chant de soin notes";
				Hue = 0x96;
        }

		public ArmysPaeonScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new ArmysPaeonScroll( amount ), amount );
		}
	}
}