using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class SinewyEtudeScroll : SpellScroll
	{
		[Constructable]
		public SinewyEtudeScroll() : this( 1 )
		{
                  Name = "Ballade de hache notes";
		}

		[Constructable]
		public SinewyEtudeScroll( int amount ) : base( 366, 0x14ED, amount )
		{
                  Name = "Ballade de hache notes";
		          Hue = 0x96;
        }

		public SinewyEtudeScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new SinewyEtudeScroll( amount ), amount );
		}
	}
}