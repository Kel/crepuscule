using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class FireCarolScroll : SpellScroll
	{
		[Constructable]
		public FireCarolScroll() : this( 1 )
		{
                  Name = "Chant du feu notes";
		}

		[Constructable]
		public FireCarolScroll( int amount ) : base( 355, 0x14ED, amount )
		{
                  Name = "Chant du feu notes";
		          Hue = 0x96;
        }

		public FireCarolScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new FireCarolScroll( amount ), amount );
		}
	}
}