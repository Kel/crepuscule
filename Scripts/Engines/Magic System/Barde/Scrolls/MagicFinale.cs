using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class MagicFinaleScroll : SpellScroll
	{
		[Constructable]
		public MagicFinaleScroll() : this( 1 )
		{
                  Name = "Chant Spheral notes";
		}

		[Constructable]
		public MagicFinaleScroll( int amount ) : base( 362, 0x14ED, amount )
		{
                  Name = "Chant Spheral notes";
		          Hue = 0x96;
        }

		public MagicFinaleScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new MagicFinaleScroll( amount ), amount );
		}
	}
}