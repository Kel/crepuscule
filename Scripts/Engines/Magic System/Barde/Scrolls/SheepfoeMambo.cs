using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class SheepfoeMamboScroll : SpellScroll
	{
		[Constructable]
		public SheepfoeMamboScroll() : this( 1 )
		{
                  Name = "Ballade d'artiste notes";
		}

		[Constructable]
		public SheepfoeMamboScroll( int amount ) : base( 365, 0x14ED, amount )
		{
                  Name = "Ballade d'artiste notes";
		          Hue = 0x96;
        }

		public SheepfoeMamboScroll( Serial serial ) : base( serial )
		{
		}

		public override void OnDoubleClick( Mobile from )
		{
			from.SendMessage( "Ces notes doivent �tre dans un livre de chants." );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}


		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new SheepfoeMamboScroll( amount ), amount );
		}
	}
}