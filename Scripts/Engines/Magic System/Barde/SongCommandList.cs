using System;
using Server;
using Server.Items;
using System.Text;
using Server.Mobiles;
using Server.Network;
using Server.Spells;
using Server.Spells.Song;


namespace Server.Scripts.Commands
{
	public class CastSongSpells
	{
		public static void Initialize()
		{
			Server.Commands.CommandPrefix = ".";

			Properties.Register();

			Register( "chant1", AccessLevel.Player, new CommandEventHandler( ArmysPaeon_OnCommand ) );

			Register( "chant2", AccessLevel.Player, new CommandEventHandler( EnchantingEtude_OnCommand ) );

			Register( "chant3", AccessLevel.Player, new CommandEventHandler( EnergyCarol_OnCommand ) );

			Register( "chant4", AccessLevel.Player, new CommandEventHandler( EnergyThrenody_OnCommand ) );

			Register( "chant5", AccessLevel.Player, new CommandEventHandler( FireCarol_OnCommand ) );

			Register( "chant6", AccessLevel.Player, new CommandEventHandler( FireThrenody_OnCommand ) );

			Register( "chant7", AccessLevel.Player, new CommandEventHandler( FoeRequiem_OnCommand ) );

			Register( "chant8", AccessLevel.Player, new CommandEventHandler( IceCarol_OnCommand ) );

			Register( "chant9", AccessLevel.Player, new CommandEventHandler( IceThrenody_OnCommand ) );

			Register( "chant10", AccessLevel.Player, new CommandEventHandler( KnightsMinne_OnCommand ) );

			Register( "chant11", AccessLevel.Player, new CommandEventHandler( MagesBallad_OnCommand ) );

			Register( "chant12", AccessLevel.Player, new CommandEventHandler( MagicFinale_OnCommand ) );

			Register( "chant13", AccessLevel.Player, new CommandEventHandler( PoisonCarol_OnCommand ) );

			Register( "chant14", AccessLevel.Player, new CommandEventHandler( PoisonThrenody_OnCommand ) );

			Register( "chant15", AccessLevel.Player, new CommandEventHandler( SheepfoeMambo_OnCommand ) );

			Register( "chant16", AccessLevel.Player, new CommandEventHandler( SinewyEtude_OnCommand ) );

		}

		public static void Register( string command, AccessLevel access, CommandEventHandler handler )
		{
			Server.Commands.Register( command, access, handler );
		}

		public static bool HasSpell( Mobile from, int spellID )
		{
			Spellbook book = Spellbook.Find( from, spellID );

			return ( book != null && book.HasSpell( spellID ) );
		}

		[Usage( "chant1" )]
		[Description( "Casts ArmysPaeon" )]
		public static void ArmysPaeon_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 351 ) )
					{
					new ArmysPaeonSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}
		[Usage( "chant2" )]
		[Description( "Casts EnchantingEtude" )]
		public static void EnchantingEtude_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;
			
         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 352 ) )
					{
					new EnchantingEtudeSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant3" )]
		[Description( "Casts EnergyCarol" )]
		public static void EnergyCarol_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 353 ) )
					{
					new EnergyCarolSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant4" )]
		[Description( "Casts EnergyThrenody" )]
		public static void EnergyThrenody_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 354 ) )
					{
					new EnergyThrenodySong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant5" )]
		[Description( "Casts FireCarol" )]
		public static void FireCarol_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 355 ) )
					{
					new FireCarolSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant6" )]
		[Description( "Casts FireThrenody" )]
		public static void FireThrenody_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 356 ) )
					{
					new  FireThrenodySong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant7" )]
		[Description( "Casts FoeRequiem" )]
		public static void FoeRequiem_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 357 ) )
					{
					new FoeRequiemSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant8" )]
		[Description( "Casts IceCarol" )]
		public static void IceCarol_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 358 ) )
					{
					new IceCarolSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant9" )]
		[Description( "Casts IceThrenody" )]
		public static void IceThrenody_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 359 ) )
					{
					new IceThrenodySong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant10" )]
		[Description( "Casts KnightsMinne" )]
		public static void KnightsMinne_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 360 ) )
					{
					new KnightsMinneSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant11" )]
		[Description( "Casts MagesBallad" )]
		public static void MagesBallad_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 361 ) )
					{
					new MagesBalladSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant12" )]
		[Description( "Casts MagicFinale" )]
		public static void MagicFinale_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 362 ) )
					{
					new MagicFinaleSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant13" )]
		[Description( "Casts PoisonCarol" )]
		public static void PoisonCarol_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 363 ) )
					{
					new PoisonCarolSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant14" )]
		[Description( "Casts PoisonThrenody" )]
		public static void PoisonThrenody_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 364 ) )
					{
					new PoisonThrenodySong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}

		[Usage( "chant15" )]
		[Description( "Casts SheepfoeMambo" )]
		public static void SheepfoeMambo_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 365 ) )
					{
					new SheepfoeMamboSong( e.Mobile, null ).Cast(); 
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 			

		}
		[Usage( "chant16" )]
		[Description( "Casts SinewyEtude" )]
		public static void SinewyEtude_OnCommand( CommandEventArgs e )
		{
				Mobile from = e.Mobile;

         			if ( !Multis.DesignContext.Check( e.Mobile ) )
            				return; // They are customizing

				if ( HasSpell( from, 366 ) )
					{

					new SinewyEtudeSong( e.Mobile, null ).Cast();
					}
				else
					{
									from.SendLocalizedMessage( 500015 ); // You do not have that spell!
					} 

		}
	}
}
