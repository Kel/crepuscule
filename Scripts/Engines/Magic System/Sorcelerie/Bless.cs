using System;
using Server.Targeting;
using Server.Mobiles;
using Server.Network;

namespace Server.Spells.Third
{
	public class BlessSpell : Spell
	{
		private static SpellInfo m_Info = new SpellInfo(
				"Benediction", "Rel Sanct",
				SpellCircle.Third,
				203,
				9061,
				Reagent.Garlic,
				Reagent.MandrakeRoot
			);

		public BlessSpell( Mobile caster, Item scroll ) : base( caster, scroll, m_Info )
		{
		}

		public override void OnCast()
		{
             if (Caster is RacePlayerMobile) { int apt_besoin = 12;
            RacePlayerMobile Casteur = (RacePlayerMobile)Caster;
            if ((Casteur.Capacities[CapacityName.Sorcery].Value + Casteur.Capacities[CapacityName.BloodMagic].Value) < apt_besoin)
            {
                Casteur.SendMessage("Vos mains tremblent, vous ratez votre sort.");
                Casteur.SendMessage("Vous avez besoin de minimum " + apt_besoin + " en sorcellerie.");
                return;
            }
			}Caster.Target = new InternalTarget( this );
		}

		public void Target( Mobile m )
		{
			if ( !Caster.CanSee( m ) )
			{
				Caster.SendLocalizedMessage( 500237 ); // Target can not be seen.
			}
			else if ( CheckBSequence( m ) )
			{
				SpellHelper.Turn( Caster, m );

				SpellHelper.AddStatBonus( Caster, m, StatType.Str ); SpellHelper.DisableSkillCheck = true;
				SpellHelper.AddStatBonus( Caster, m, StatType.Dex );
				SpellHelper.AddStatBonus( Caster, m, StatType.Int ); SpellHelper.DisableSkillCheck = false;

				m.FixedParticles( 0x373A, 10, 15, 5018, EffectLayer.Waist );
				m.PlaySound( 0x1EA );
			}

			FinishSequence();
		}

		private class InternalTarget : Target
		{
			private BlessSpell m_Owner;

			public InternalTarget( BlessSpell owner ) : base( 12, false, TargetFlags.Beneficial )
			{
				m_Owner = owner;
			}

			protected override void OnTarget( Mobile from, object o )
			{
				if ( o is Mobile )
				{
					m_Owner.Target( (Mobile)o );
				}
			}

			protected override void OnTargetFinish( Mobile from )
			{
				m_Owner.FinishSequence();
			}
		}
	}
}