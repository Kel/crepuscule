using System;
using Server;

namespace Server.Items
{
	public class Talc : Item
	{
		

		[Constructable]
		public Talc() : this( 1 ){
                Name = "Poussiere de Talc";
		Hue = 0X882;
		{
		}

}		[Constructable]
		public Talc( int amount ) : base( 0x103D )
		{
			Stackable = false;
			Amount = amount;
		}

		public Talc( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}