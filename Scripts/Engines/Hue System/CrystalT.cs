using System;
using Server;

namespace Server.Items
{
	public class CrystalT : Item
	{
		

		[Constructable]
		public CrystalT() : this( 1 ){
                Name = "Crystal";
		Hue = 0X79B;
		{
		}

}		[Constructable]
		public CrystalT( int amount ) : base( 0x223B)
		{
			Stackable = false;
			Amount = amount;
		}

		public CrystalT( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}