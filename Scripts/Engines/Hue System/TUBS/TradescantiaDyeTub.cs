using System; 
using Server.Items; 
using System.Collections; 
using Server; 
using Server.Mobiles; 
using Server.Network; 
using Server.ContextMenus; 
using Server.Multis; 
using Server.Targeting; 

namespace Server.Items 
{ 

    public class TradescantiaDyeTub : DyeTub, IUsesRemaining 
    { 

        [Flags] 
        private enum TradescantiaDyeTubFlags 
        { 
            None                = 0x00000000, 
            DeleteOnZeroCharges = 0x00000001, 
            ShowUsesRemaining   = 0x00000002, 
            Rechargable         = 0x00000004 
        }; 

        private bool GetFlag(TradescantiaDyeTubFlags flag) 
        { 
            return ((m_Flags & flag) != 0); 
        } 

        private void SetFlag(TradescantiaDyeTubFlags flag, bool value) 
        { 
            if (value) 
                m_Flags |= flag; 
            else 
                m_Flags &= ~flag; 
        } 

        private int m_UsesRemaining; 
        private TradescantiaDyeTubFlags m_Flags; 

        [CommandProperty(AccessLevel.GameMaster)] 
        public int UsesRemaining { get { return m_UsesRemaining; } set { m_UsesRemaining = value; InvalidateProperties(); } } 

        [CommandProperty(AccessLevel.GameMaster)] 
        public bool DeleteOnZeroCharges 
        { 
            get { return GetFlag(TradescantiaDyeTubFlags.DeleteOnZeroCharges); } 
            set { SetFlag(TradescantiaDyeTubFlags.DeleteOnZeroCharges, value); } 
        } 

        [CommandProperty(AccessLevel.GameMaster)] 
        public bool ShowUsesRemaining 
        { 
            get { return GetFlag(TradescantiaDyeTubFlags.ShowUsesRemaining); } 
            set { SetFlag(TradescantiaDyeTubFlags.ShowUsesRemaining, value); InvalidateProperties(); } 
        } 

        public override void GetProperties(ObjectPropertyList list) 
        { 
            base.GetProperties(list); 
            if (m_UsesRemaining != -1 && ShowUsesRemaining) 
                list.Add(1060584, m_UsesRemaining.ToString()); // uses remaining: ~1_val~ 
        } 
        [Constructable] 
        public TradescantiaDyeTub( int uses ) 
        { 
            Name = "Teinture De Tradescantia"; 
                    UsesRemaining = uses; 
                    ShowUsesRemaining = true; 
            LootType = LootType.Regular; 
            Hue = DyedHue = 0x825; 
            Redyable = false; 
            uses = Utility.RandomMinMax(1,3); 
        } 

        [Constructable] 
        public TradescantiaDyeTub() : this( 1 ) 
        { 
        } 
                 

        public TradescantiaDyeTub( Serial serial ) : base( serial ) 
        { 

        } 

        public override void OnSingleClick( Mobile from ) 
        { 
            base.LabelTo( from, "Tradescantia Dye Tub: {0} charges left", m_UsesRemaining ); 
        } 

        public override void OnDoubleClick( Mobile from ) 
        { 

            if (UsesRemaining == 0) 
            { 
                from.SendMessage( "Il n'y a plus de charges!" ); 
                this.Delete(); 
                return; 
            } 

            if ( !IsChildOf( from.Backpack ) )  
            {  
                from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it. 
                 
                return; 
            } 

            from.SendLocalizedMessage( TargetMessage ); 
            from.Target = new InternalTarget( this ); 
        } 

        public override void Serialize( GenericWriter writer ) 
        { 
            base.Serialize( writer ); 
            writer.Write( (int) 0 ); // version 
                    writer.Write((int)m_UsesRemaining); 
                    writer.Write((int)m_Flags); 
        } 

        public override void Deserialize( GenericReader reader ) 
        { 
            base.Deserialize( reader ); 
            int version = reader.ReadInt(); 

            switch ( version ) 
            { 
                case 0: 
                { 
                                m_UsesRemaining = reader.ReadInt(); 
                                m_Flags = (TradescantiaDyeTubFlags)reader.ReadInt(); 

                    break; 
                } 
            } 
        } 

        private class InternalTarget : Target 
        { 
            private TradescantiaDyeTub m_Tub; 
            public InternalTarget( TradescantiaDyeTub tub ) : base( -1, false, TargetFlags.None ) 
            { 
                m_Tub = tub; 
            } 
            protected override void OnTarget( Mobile from, object targeted ) 
            { 
                if( targeted is BaseClothing ) 
                { 
                    Item item = (Item)targeted; 
                    if ( item is IDyable && m_Tub.AllowDyables ) 
                        if ( !from.InRange( m_Tub.GetWorldLocation(), 1 ) || !from.InRange( item.GetWorldLocation(), 1 ) ) 
                        { 
                            from.SendLocalizedMessage( 500446 ); // That is too far away. 
                        } 
                        else if ( item.Parent is Mobile ) 
                        { 
                            from.SendLocalizedMessage( 500861 ); // Can't Dye clothing that is being worn. 
                        } 
                        else if ( m_Tub.UsesRemaining == 0 && m_Tub.DeleteOnZeroCharges ) 
                        { 
                            from.SendMessage( "Plus de charge!" ); 
                            //this.Delete(); 
                        } 
                        else 
                        { 
                            item.Hue = m_Tub.Hue; 
                            from.PlaySound( 0x23F ); 
                            m_Tub.UsesRemaining--; 
                        } 

                } 
            } 
        } 
    } 
}  
