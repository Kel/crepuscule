using System; 
using Server.Items; 
using System.Collections; 
using Server; 
using Server.Mobiles; 
using Server.Network; 
using Server.ContextMenus; 
using Server.Multis; 
using Server.Targeting; 

namespace Server.Items 
{ 

    public class RocheVolcaniqueDyeTub : DyeTub, IUsesRemaining 
    { 

        [Flags] 
        private enum RocheVolcaniqueDyeTubFlags 
        { 
            None                = 0x00000000, 
            DeleteOnZeroCharges = 0x00000001, 
            ShowUsesRemaining   = 0x00000002, 
            Rechargable         = 0x00000004 
        }; 

        private bool GetFlag(RocheVolcaniqueDyeTubFlags flag) 
        { 
            return ((m_Flags & flag) != 0); 
        } 

        private void SetFlag(RocheVolcaniqueDyeTubFlags flag, bool value) 
        { 
            if (value) 
                m_Flags |= flag; 
            else 
                m_Flags &= ~flag; 
        } 

        private int m_UsesRemaining; 
        private RocheVolcaniqueDyeTubFlags m_Flags; 

        [CommandProperty(AccessLevel.GameMaster)] 
        public int UsesRemaining { get { return m_UsesRemaining; } set { m_UsesRemaining = value; InvalidateProperties(); } } 

        [CommandProperty(AccessLevel.GameMaster)] 
        public bool DeleteOnZeroCharges 
        { 
            get { return GetFlag(RocheVolcaniqueDyeTubFlags.DeleteOnZeroCharges); } 
            set { SetFlag(RocheVolcaniqueDyeTubFlags.DeleteOnZeroCharges, value); } 
        } 

        [CommandProperty(AccessLevel.GameMaster)] 
        public bool ShowUsesRemaining 
        { 
            get { return GetFlag(RocheVolcaniqueDyeTubFlags.ShowUsesRemaining); } 
            set { SetFlag(RocheVolcaniqueDyeTubFlags.ShowUsesRemaining, value); InvalidateProperties(); } 
        } 

        public override void GetProperties(ObjectPropertyList list) 
        { 
            base.GetProperties(list); 
            if (m_UsesRemaining != -1 && ShowUsesRemaining) 
                list.Add(1060584, m_UsesRemaining.ToString()); // uses remaining: ~1_val~ 
        } 
        [Constructable] 
        public RocheVolcaniqueDyeTub( int uses ) 
        { 
            Name = "Teinture De Roche Volcanique"; 
                    UsesRemaining = uses; 
                    ShowUsesRemaining = true; 
            LootType = LootType.Regular; 
            Hue = DyedHue = 0x8BA; 
            Redyable = false; 
            uses = Utility.RandomMinMax(1,3); 
        } 

        [Constructable] 
        public RocheVolcaniqueDyeTub() : this( 1 ) 
        { 
        } 
                 

        public RocheVolcaniqueDyeTub( Serial serial ) : base( serial ) 
        { 

        } 

        public override void OnSingleClick( Mobile from ) 
        { 
            base.LabelTo( from, "RocheVolcanique Dye Tub: {0} charges left", m_UsesRemaining ); 
        } 

        public override void OnDoubleClick( Mobile from ) 
        { 

            if (UsesRemaining == 0) 
            { 
                from.SendMessage( "Il n'y a plus de charges!" ); 
                this.Delete(); 
                return; 
            } 

            if ( !IsChildOf( from.Backpack ) )  
            {  
                from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it. 
                 
                return; 
            } 

            from.SendLocalizedMessage( TargetMessage ); 
            from.Target = new InternalTarget( this ); 
        } 

        public override void Serialize( GenericWriter writer ) 
        { 
            base.Serialize( writer ); 
            writer.Write( (int) 0 ); // version 
                    writer.Write((int)m_UsesRemaining); 
                    writer.Write((int)m_Flags); 
        } 

        public override void Deserialize( GenericReader reader ) 
        { 
            base.Deserialize( reader ); 
            int version = reader.ReadInt(); 

            switch ( version ) 
            { 
                case 0: 
                { 
                                m_UsesRemaining = reader.ReadInt(); 
                                m_Flags = (RocheVolcaniqueDyeTubFlags)reader.ReadInt(); 

                    break; 
                } 
            } 
        } 

        private class InternalTarget : Target 
        { 
            private RocheVolcaniqueDyeTub m_Tub; 
            public InternalTarget( RocheVolcaniqueDyeTub tub ) : base( -1, false, TargetFlags.None ) 
            { 
                m_Tub = tub; 
            } 
            protected override void OnTarget( Mobile from, object targeted ) 
            { 
                if( targeted is BaseClothing ) 
                { 
                    Item item = (Item)targeted; 
                    if ( item is IDyable && m_Tub.AllowDyables ) 
                        if ( !from.InRange( m_Tub.GetWorldLocation(), 1 ) || !from.InRange( item.GetWorldLocation(), 1 ) ) 
                        { 
                            from.SendLocalizedMessage( 500446 ); // That is too far away. 
                        } 
                        else if ( item.Parent is Mobile ) 
                        { 
                            from.SendLocalizedMessage( 500861 ); // Can't Dye clothing that is being worn. 
                        } 
                        else if ( m_Tub.UsesRemaining == 0 && m_Tub.DeleteOnZeroCharges ) 
                        { 
                            from.SendMessage( "Plus de charge!" ); 
                            //this.Delete(); 
                        } 
                        else 
                        { 
                            item.Hue = m_Tub.Hue; 
                            from.PlaySound( 0x23F ); 
                            m_Tub.UsesRemaining--; 
                        } 

                } 
            } 
        } 
    } 
}  
