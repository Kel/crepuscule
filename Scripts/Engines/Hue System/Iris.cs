using System;
using Server;

namespace Server.Items
{
	public class Iris : Item
	{
	

		[Constructable]
		public Iris() : this( 1 ){
                Name = "Iris";
		{
		}
  
             }[Constructable]
	  	public Iris( int amount ) : base( 0x234C )
		{
			Stackable = false;
			Amount = amount;
		}

		public Iris( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}