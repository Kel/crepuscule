using System;
using Server;

namespace Server.Items
{
	public class Cendre : Item
	{
		

		[Constructable]
		public Cendre() : this( 1 ){
                Name = "Cendre";
		Hue = 0X7F9;
		{
		}

}		[Constructable]
		public Cendre( int amount ) : base( 0x11EB )
		{
			Stackable = false;
			Amount = amount;
		}

		public Cendre( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}