using System;
using Server;

namespace Server.Items
{
	public class CDeLave : Item
	{
		

		[Constructable]
		public CDeLave() : this( 1 ){
                Name = "Concentré De Lave";
		Hue = 0X7EF;
		{
		}

}		[Constructable]
		public CDeLave( int amount ) : base( 0x182B )
		{
			Stackable = false;
			Amount = amount;
		}

		public CDeLave( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}