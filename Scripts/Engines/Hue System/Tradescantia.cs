using System;
using Server;

namespace Server.Items
{
	public class Tradescantia : Item
	{
public override int Hue { get { return 0x78A; } }

		

		[Constructable]
		public Tradescantia() : this( 1 ){
                Name = "Tradescantia";
		Hue = 0X78A;
		
		{
		}

}		[Constructable]
		public Tradescantia( int amount ) : base( 0xC51 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Tradescantia( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}