using System;
using Server;

namespace Server.Items
{
	public class Harpyegg : Item
	{
		

		[Constructable]
		public Harpyegg() : this( 1 ){
                Name = "Un Oeuf de Harpie";
		Hue = 0X6 ;
		{
		}

}		[Constructable]
		public Harpyegg( int amount ) : base( 0x2809 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Harpyegg( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}