using System;
using Server;

namespace Server.Items
{
	public class Corail : Item
	{
		

		[Constructable]
		public Corail() : this( 1 ){
                Name = "Corail";
		Hue = 0X896;
		{
		}

}		[Constructable]
		public Corail( int amount ) : base( 0x1FA4)
		{
			Stackable = false;
			Amount = amount;
		}

		public Corail( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}