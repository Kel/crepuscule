using System;
using Server;

namespace Server.Items
{
	public class Mousson : Item
	{
		

		[Constructable]
		public Mousson() : this( 1 ){
                Name = "Mousson";
		Hue = 0X795;
		{
		}

}		[Constructable]
		public Mousson( int amount ) : base( 0x11AA )
		{
			Stackable = false;
			Amount = amount;
		}

		public Mousson( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}