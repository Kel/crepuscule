using System;
using Server;

namespace Server.Items
{
	public class OeilDeBalron : Item
	{
		

		[Constructable]
		public OeilDeBalron() : this( 1 ){
                Name = "Oeil De Balron";
		{
		}

}		[Constructable]
		public OeilDeBalron( int amount ) : base( 0x318D )
		{
			Stackable = false;
			Amount = amount;
		}

		public OeilDeBalron( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}