using System;
using Server;

namespace Server.Items
{
	public class Lychnis : Item
	{
		

		[Constructable]
		public Lychnis() : this( 1 ){
                Name = "Lychnis";
		{
		}

}		[Constructable]
		public Lychnis( int amount ) : base( 0xC3C )
		{
			Stackable = false;
			Amount = amount;
		}

		public Lychnis( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}