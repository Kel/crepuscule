using System;
using Server;

namespace Server.Items
{
	public class Chrysantheme : Item
	{
		

		[Constructable]
		public Chrysantheme() : this( 1 ){
                Name = "Chrysantheme";
		Hue = 0X85F;
		{
		}

}		[Constructable]
		public Chrysantheme( int amount ) : base( 0x234B)
		{
			Stackable = false;
			Amount = amount;
		}

		public Chrysantheme( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}