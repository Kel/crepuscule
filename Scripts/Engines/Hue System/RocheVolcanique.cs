using System;
using Server;

namespace Server.Items
{
	public class RocheVolcanique : Item
	{
		

		[Constructable]
		public RocheVolcanique() : this( 1 ){
                Name = "Une Roche Volcanique";
		{
		}

}		[Constructable]
		public RocheVolcanique( int amount ) : base( 0xF7F )
		{
			Stackable = false;
			Amount = amount;
		}

		public RocheVolcanique( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}