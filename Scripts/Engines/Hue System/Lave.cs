using System;
using Server;

namespace Server.Items
{
	public class Lave : Item
	{
		

		[Constructable]
		public Lave() : this( 1 ){
                Name = "Lave";
		{
		}

}		[Constructable]
		public Lave( int amount ) : base( 0x1A78 )
		{
			Stackable = false;
			Amount = amount;
		}

		public Lave( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}