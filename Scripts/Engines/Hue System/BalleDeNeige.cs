using System;
using Server;

namespace Server.Items
{
	public class BalleDeNeige : Item
	{
		

		[Constructable]
		public BalleDeNeige() : this( 1 ){
                Name = "Balle De Neige";
		Hue = 0X801;
		{
		}

}		[Constructable]
		public BalleDeNeige( int amount ) : base( 0x2FD8 )
		{
			Stackable = false;
			Amount = amount;
		}

		public BalleDeNeige( Serial serial ) : base( serial )
		{
		}

		

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}