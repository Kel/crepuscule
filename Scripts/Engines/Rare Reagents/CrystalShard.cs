using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class CrystalShard : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} tessons de cristal", Amount );
			}
		}

		[Constructable]
		public CrystalShard() : this( 1 )
		{
		
		}

		[Constructable]
		public CrystalShard( int amount ) : base( 0xF8E, amount )
		{
            Name = "tesson de cristal";
			Hue = 1152;
		}

		public CrystalShard( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}