using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class TrollScalp : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} scalpe de troll", Amount );
			}
		}

		[Constructable]
		public TrollScalp() : this( 1 )
		{
			
		}

		[Constructable]
		public TrollScalp( int amount ) : base( 0x6190, amount )
		{
			Hue = 0x2E;
            Name = "scalpe de troll";
		}

		public TrollScalp( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new TrollScalp( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}