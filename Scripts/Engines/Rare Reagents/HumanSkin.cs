using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HumanSkin : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} peau d'homme", Amount );
			}
		}

		[Constructable]
		public HumanSkin() : this( 1 )
		{
			
		}

		[Constructable]
		public HumanSkin( int amount ) : base( 0x0DF8, amount )
		{
            Name = "peau d'homme";
		}

		public HumanSkin( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HumanSkin( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}