using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class SwampGas : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format("{0} gazes de mar�cage", Amount);
			}
		}

		[Constructable]
		public SwampGas() : this( 1 )
		{
			
		}

		[Constructable]
		public SwampGas( int amount ) : base( 0xE24, amount )
		{
			Hue = 0x3C;
            Name = "gazes de mar�cage";
		}

		public SwampGas( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new SwampGas( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}