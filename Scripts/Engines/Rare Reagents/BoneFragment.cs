using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class BoneFragment : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} fragment d'os", Amount );
			}
		}

		[Constructable]
		public BoneFragment() : this( 1 )
		{
		}

		[Constructable]
		public BoneFragment( int amount ) : base( 0xF8E, amount )
		{
            Name = "fragment d'os";
			Hue = 1153;
		}

		public BoneFragment( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new BoneFragment( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}