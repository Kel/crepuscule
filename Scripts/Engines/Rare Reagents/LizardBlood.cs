using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class LizardBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format("{0} sang de l�zard", Amount);
			}
		}

		[Constructable]
		public LizardBlood() : this( 1 )
		{
			
		}

		[Constructable]
		public LizardBlood( int amount ) : base( 0xE24, amount )
		{
			Hue = 30;
            Name = "sang de l�zard";
		}

		public LizardBlood( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new LizardBlood( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}