using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class GnomeBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sang de gnome", Amount );
			}
		}

		[Constructable]
		public GnomeBlood() : this( 1 )
		{
			
		}

		[Constructable]
		public GnomeBlood( int amount ) : base( 0xE24, amount )
		{
			Hue = 33;
            Name = "sang de gnome";
		}

		public GnomeBlood( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new GnomeBlood( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}