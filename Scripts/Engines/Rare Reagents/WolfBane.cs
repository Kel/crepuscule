using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class WolfBane : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} poison de loup", Amount );
			}
		}

		[Constructable]
		public WolfBane() : this( 1 )
		{
			
		}

		[Constructable]
		public WolfBane( int amount ) : base( 0xF88, amount )
		{
			Hue = 51;
            Name = "poison de loup";
		}

		public WolfBane( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new WolfBane( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}