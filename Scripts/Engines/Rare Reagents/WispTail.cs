using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class WispTail : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} mince queue", Amount );
			}
		}

		[Constructable]
		public WispTail() : this( 1 )
		{
			
		}

		[Constructable]
		public WispTail( int amount ) : base( 0xE24, amount )
		{
            Name = "mince queue";
			Hue = 52;
		}

		public WispTail( Serial serial ) : base( serial )
		{
		}


		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}