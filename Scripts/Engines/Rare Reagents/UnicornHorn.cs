using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class UnicornHorn : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} corne de licorne", Amount );
			}
		}

		[Constructable]
		public UnicornHorn() : this( 1 )
		{
			
		}

		[Constructable]
		public UnicornHorn( int amount ) : base( 0x0F42, amount )
		{
            Name = "corne de licorne";
			Hue = 1153;
		}

		public UnicornHorn( Serial serial ) : base( serial )
		{
		}


		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}