using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HolyWater : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} eau benite", Amount );
			}
		}

		[Constructable]
		public HolyWater() : this( 1 )
		{
			
		}

		[Constructable]
		public HolyWater( int amount ) : base( 0xE24, amount )
		{
			Hue = 1153;
            Name = "eau benite";
		}

		public HolyWater( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HolyWater( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}