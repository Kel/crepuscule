using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class EyeOfToad : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} oeil de grenouille", Amount );
			}
		}

		[Constructable]
		public EyeOfToad() : this( 1 )
		{
			Hue = 0x230;
		}

		[Constructable]
		public EyeOfToad( int amount ) : base( 0x0F87, amount )
		{
            Name = "oeil de grenouille";
			Hue = 75;
		}

		public EyeOfToad( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new EyeOfToad( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}