using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class CowDung : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} crottes de vache", Amount );
			}
		}

		[Constructable]
		public CowDung() : this( 1 )
		{
			Hue = 0x31;
		}

		[Constructable]
		public CowDung( int amount ) : base( 0x97A, amount )
		{
            Name = "crotte de vache";
			Hue = 44;
		}

		public CowDung( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new CowDung( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}