using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class TrollBogie : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format("{0} crotte de nez de troll", Amount);
			}
		}

		[Constructable]
		public TrollBogie() : this( 1 )
		{
			
		}

		[Constructable]
		public TrollBogie( int amount ) : base( 0x97A, amount )
		{
			Hue = 0x3C;
            Name = "crotte de nez de troll";
		}

		public TrollBogie( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new TrollBogie( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}