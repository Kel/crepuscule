using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class Mistletoe : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} orteil", Amount );
			}
		}

		[Constructable]
		public Mistletoe() : this( 1 )
		{
			
		}

		[Constructable]
		public Mistletoe( int amount ) : base( 0xF88, amount )
		{
			Hue = 0x41;
            Name = "orteil";
		}

		public Mistletoe( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Mistletoe( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}