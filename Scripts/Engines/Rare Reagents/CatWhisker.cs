using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class CatWhisker : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} moustaches de chat", Amount );
			}
		}

		[Constructable]
		public CatWhisker() : this( 1 )
		{
		}

		[Constructable]
		public CatWhisker( int amount ) : base( 0xF8D, amount )
		{
            Name = "moustache de chat";
			Hue = 52;
		}

		public CatWhisker( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new CatWhisker( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}