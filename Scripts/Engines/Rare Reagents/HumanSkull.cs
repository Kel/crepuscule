using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HumanSkull : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} cr�ne d'homme", Amount );
			}
		}

		[Constructable]
		public HumanSkull() : this( 1 )
		{
			
		}

		[Constructable]
		public HumanSkull( int amount ) : base( 0xE73, amount )
		{
            Name = "cr�ne d'homme";
			Hue = 1153;
		}

		public HumanSkull( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HumanSkull( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}