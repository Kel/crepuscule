using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class StoneShard : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} tesson de rock", Amount );
			}
		}

		[Constructable]
		public StoneShard() : this( 1 )
		{
			
		}

		[Constructable]
		public StoneShard( int amount ) : base( 0xF8E, amount )
		{
            Name = "tesson de rock";
			Hue = 1124;
		}

		public StoneShard( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new StoneShard( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}