using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HorseHair : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} cheveux de cheval", Amount );
			}
		}

		[Constructable]
		public HorseHair() : this( 1 )
		{
			
		}

		[Constructable]
		public HorseHair( int amount ) : base( 0xF8D, amount )
		{
            Name = "cheveux de cheval";
			Hue = 45;
		}

		public HorseHair( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HorseHair( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}