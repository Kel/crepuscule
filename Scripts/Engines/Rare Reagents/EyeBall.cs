using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class EyeBall : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} oeil", Amount );
			}
		}

		[Constructable]
		public EyeBall() : this( 1 )
		{
			
		}

		[Constructable]
		public EyeBall( int amount ) : base( 0x0F87, amount )
		{
            Name = "oeil";
			Hue = 1153;
		}

		public EyeBall( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new EyeBall( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}