using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HumanBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sang d'humain", Amount );
			}
		}

		[Constructable]
		public HumanBlood() : this( 1 )
		{
			
		}

		[Constructable]
		public HumanBlood( int amount ) : base( 0xE24, amount )
		{
			Hue = 31;
            Name = "sang d'humain";
		}

		public HumanBlood( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HumanBlood( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}