using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HairOfDog : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} poils de chien", Amount );
			}
		}

		[Constructable]
		public HairOfDog() : this( 1 )
		{
			
		}

		[Constructable]
		public HairOfDog( int amount ) : base( 0xF8D, amount )
		{
            Name = "poils de chien";
			Hue = 46;
		}

		public HairOfDog( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HairOfDog( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}