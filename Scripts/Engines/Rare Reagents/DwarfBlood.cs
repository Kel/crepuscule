using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class DwarfBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sang de nain", Amount );
			}
		}

		[Constructable]
		public DwarfBlood() : this( 1 )
		{
		
		}

		[Constructable]
		public DwarfBlood( int amount ) : base( 0xE24, amount )
		{
			Hue = 35;
            Name = "sang de nain";
		}

		public DwarfBlood( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}