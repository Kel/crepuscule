using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HumanHair : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} cheveux d'un homme", Amount );
			}
		}

		[Constructable]
		public HumanHair() : this( 1 )
		{
			
		}

		[Constructable]
		public HumanHair( int amount ) : base( 0xF8D, amount )
		{
            Name = "cheveux d'un homme";
			Hue = 42;
		}

		public HumanHair( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HumanHair( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}