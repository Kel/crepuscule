using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class SwampMoss : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format("{0} plantes de mar�cage", Amount);
			}
		}

		[Constructable]
		public SwampMoss() : this( 1 )
		{
			
		}

		[Constructable]
		public SwampMoss( int amount ) : base( 0xF88, amount )
		{
			Hue = 0x3C;
            Name = "plantes de mar�cage";
		}

		public SwampMoss( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new SwampMoss( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}