using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class EyeOfNewt : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} oeil de triton", Amount );
			}
		}

		[Constructable]
		public EyeOfNewt() : this( 1 )
		{
			
		}

		[Constructable]
		public EyeOfNewt( int amount ) : base( 0x0F87, amount )
		{
            Name = "oeil de triton";
		}

		public EyeOfNewt( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new EyeOfNewt( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}