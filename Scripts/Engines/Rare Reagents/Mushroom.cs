using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class Mushroom : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} champignon sp�cial", Amount );
			}
		}

		[Constructable]
		public Mushroom() : this( 1 )
		{
			
		}

		[Constructable]
		public Mushroom( int amount ) : base( 0x0D16, amount )
		{
            Name = "champignon sp�cial";
		}

		public Mushroom( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Mushroom( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}