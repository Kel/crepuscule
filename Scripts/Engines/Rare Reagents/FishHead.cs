using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class FishHead : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} t�te de poisson", Amount );
			}
		}

		[Constructable]
		public FishHead() : this( 1 )
		{
			
		}

		[Constructable]
		public FishHead( int amount ) : base( 0x1C18, amount )
		{
            Name = "t�te de poisson";
		}

		public FishHead( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new FishHead( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}