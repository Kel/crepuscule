using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class ElfBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sang d'elfe", Amount );
			}
		}

		[Constructable]
		public ElfBlood() : this( 1 )
		{
			
		}

		[Constructable]
		public ElfBlood( int amount ) : base( 0xE24, amount )
		{
            Name = "sang d'elfe";
			Hue = 34;
		}

		public ElfBlood( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new ElfBlood( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}