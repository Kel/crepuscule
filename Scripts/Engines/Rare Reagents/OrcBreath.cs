using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class OrcBreath : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format("{0} haleine d'orque", Amount);
			}
		}

		[Constructable]
		public OrcBreath() : this( 1 )
		{
			
		}

		[Constructable]
		public OrcBreath( int amount ) : base( 0x1C18, amount )
		{
			Hue = 56;
            Name = "haleine d'orque en bouteille";
		}

		public OrcBreath( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new OrcBreath( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}