using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class LizardTongue : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} langue de l�zard", Amount );
			}
		}

		[Constructable]
		public LizardTongue() : this( 1 )
		{
			
		}

		[Constructable]
		public LizardTongue( int amount ) : base( 0x0F2F, amount )
		{
            Name = "langue de l�zard";
		}

		public LizardTongue( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new LizardTongue( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}