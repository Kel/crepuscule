using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class DragonBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sang de dragon", Amount );
			}
		}

		[Constructable]
		public DragonBlood() : this( 1 )
		{
		
		}

		[Constructable]
		public DragonBlood( int amount ) : base( 0xE24, amount )
		{
			Hue = 37;
            Name = "sang de dragon";
		}

		public DragonBlood( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new DragonBlood( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}