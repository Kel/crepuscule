using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HalflingBlood : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sang de halfelin", Amount );
			}
		}

		[Constructable]
		public HalflingBlood() : this( 1 )
		{
			
		}

		[Constructable]
		public HalflingBlood( int amount ) : base( 0xE24, amount )
		{
			Hue = 32;
            Name = "sang de halfelin";
		}

		public HalflingBlood( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HalflingBlood( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}