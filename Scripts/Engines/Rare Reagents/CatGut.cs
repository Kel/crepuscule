using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class CatGut : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} intenstins des chats", Amount );
			}
		}

		[Constructable]
		public CatGut() : this( 1 )
		{
		}

		[Constructable]
		public CatGut( int amount ) : base( 0x97A, amount )
		{
            Name = "intenstins de chat";
			Hue = 41;
		}

		public CatGut( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new CatGut( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}