using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class Teeth : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} dents", Amount );
			}
		}

		[Constructable]
		public Teeth() : this( 1 )
		{
			
		}

		[Constructable]
		public Teeth( int amount ) : base( 0x0F87, amount )
		{
            Name = "dents";
			Hue = 1153;
		}

		public Teeth( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new Teeth( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}