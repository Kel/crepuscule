using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class HorsePoop : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} crotte de cheval", Amount );
			}
		}

		[Constructable]
		public HorsePoop() : this( 1 )
		{
			
		}

		[Constructable]
		public HorsePoop( int amount ) : base( 0x97A, amount )
		{
            Name = "crotte de cheval";
			Hue = 42;
		}

		public HorsePoop( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new HorsePoop( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}