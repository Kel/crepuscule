using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class BlackSand : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} sable noir", Amount );
			}
		}

		[Constructable]
		public BlackSand() : this( 1 )
		{
			
		}

		[Constructable]
		public BlackSand( int amount ) : base( 0x0EEA, amount )
		{
			Hue = 0x388;
            Name = "sable noir";
		}

		public BlackSand( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new BlackSand( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}