using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class SwampWater : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format("{0} eau de mar�cage", Amount);
			}
		}

		[Constructable]
		public SwampWater() : this( 1 )
		{
			
		}

		[Constructable]
		public SwampWater( int amount ) : base( 0xE24, amount )
		{
			Hue = 0x3C;
            Name = "eau de mar�cage";
		}

		public SwampWater( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new SwampWater( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}