using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class UnicornHair : BaseReagent, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
				return String.Format( "{0} scalpe de licorne", Amount );
			}
		}

		[Constructable]
		public UnicornHair() : this( 1 )
		{
			
		}

		[Constructable]
		public UnicornHair( int amount ) : base( 0xF8D, amount )
		{
            Name = "scalpe de licorne";
			Hue = 1153;
		}

		public UnicornHair( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new UnicornHair( amount ), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}