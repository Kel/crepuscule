#region Script Header
/*
 *                         MobileGenerator.cs
 *                     --------------------------
 * This script was written by Shadow Wolf.
 * Do with this script as you wish except for removing this header.
 * If you remove this header I revoke all permission for use of this 
 * script and you will be considered a theif.
 *
 * Last Updated: March 22, 2006  08:55:00 Eastern Time
 */
#endregion
using System;
using System.Collections;
using System.IO;
using Server;
using Server.Items;
using Server.Gumps;
using ShadowWolf.Settings;

namespace ShadowWolf
{
	public class MobileGen
	{
		// <summary>
		// Output directory needs to be set here if you wish to have a custom output directory.  
		// Needs to be a full valid directory path.  I recommend setting the custom output directory.
		//
		// Example:
		//
		// private static string m_CustomOutputDir = @"C:\Program Files\RunUO\Scripts\Custom\Mobile Gen";
		// </summary>

        public static void Initialize()
		{
			Server.Commands.Register( "MobileGen", AccessLevel.Seer, new CommandEventHandler( MobileGen_OnCommand ) );
		}

		[ Usage( "MobileGen" ) ]
		public static void MobileGen_OnCommand( CommandEventArgs e )
		{
			e.Mobile.SendGump( new MobileGenGump( e.Mobile, MGSettings.statEntry, MGSettings.skillEntry ) );
		}

        public static string TryAssignReplace( Mobile from, string[] args, string[] args2 )
        {
            string m_Template = MGSettings.Template;
            string makerchar = from.Name; // User Name
            string name = args[0]; // Start Of Stats
            string corpseName = args[1];
            string title = args[2];
            string AI = args[3];
            string FM = args[4];
            string RangePerception = args[5];
            string RangeFight = args[6];
            string ActiveSpeed = args[7];
            string PassiveSpeed = args[8];
            string body = args[9];
            string bsID = args[10];
            string kills = args[11];
            string minStr = args[12];
            string maxStr = args[13];
            string minDex = args[14];
            string maxDex = args[15];
            string minInt = args[16];
            string maxInt = args[17];
            string minHits = args[18];
            string maxHits = args[19];
            string minDamage = args[20];
            string maxDamage = args[21];
            string physDamage = args[22];
            string fireDamage = args[23];
            string coldDamage = args[24];
            string energyDamage = args[25];
            string poisonDamage = args[26];
            string physResistMin = args[27];
            string physResistMax = args[28];
            string fireResistMin = args[29];
            string fireResistMax = args[30];
            string coldResistMin = args[31];
            string coldResistMax = args[32];
            string energyResistMin = args[33];
            string energyResistMax = args[34];
            string poisonResistMin = args[35];
            string poisonResistMax = args[36];
            string fame = args[37];
            string karma = args[38];
            string VArmor = args[39];
            string ns = args[40];
            string minAnatomy = args2[0] as string; // Start Of Skills
            string maxAnatomy = args2[1] as string;
            string minArchery = args2[2] as string;
            string maxArchery = args2[3] as string;
            string minEvalInt = args2[4] as string;
            string maxEvalInt = args2[5] as string;
            string minFencing = args2[6] as string;
            string maxFencing = args2[7] as string;
            string minFocus = args2[8] as string;
            string maxFocus = args2[9] as string;
            string minHealing = args2[10] as string;
            string maxHealing = args2[11] as string;
            string minMacing = args2[12] as string;
            string maxMacing = args2[13] as string;
            string minMagery = args2[14] as string;
            string maxMagery = args2[15] as string;
            string minMeditation = args2[16] as string;
            string maxMeditation = args2[17] as string;
            string minNecromancy = args2[18] as string;
            string maxNecromancy = args2[19] as string;
            string minParry = args2[20] as string;
            string maxParry = args2[21] as string;
            string minMagicResist = args2[22] as string;
            string maxMagicResist = args2[23] as string;
            string minSwords = args2[24] as string;
            string maxSwords = args2[25] as string;
            string minTactics = args2[26] as string;
            string maxTactics = args2[27] as string;
            string minWrestling = args2[28] as string;
            string maxWrestling = args2[29] as string;

            string output = m_Template.Replace("{namespace}", ns); // Start Of Stats
            output = output.Replace("{makerchar}", makerchar);
            output = output.Replace("{name}", name);
            output = output.Replace("{corpseName}", corpseName);
            output = output.Replace("{title}", title);
            output = output.Replace("{AI}", AI);
            output = output.Replace("{FM}", FM);
            output = output.Replace("{namespace}", ns);
            output = output.Replace("{RangePerception}", RangePerception);
            output = output.Replace("{RangeFight}", RangeFight);
            output = output.Replace("{ActiveSpeed}", ActiveSpeed);
            output = output.Replace("{PassiveSpeed}", PassiveSpeed);
            output = output.Replace("{body}", body);
            output = output.Replace("{bsID}", bsID);
            output = output.Replace("{kills}", kills);
            output = output.Replace("{minStr}", minStr);
            output = output.Replace("{maxStr}", maxStr);
            output = output.Replace("{minDex}", minDex);
            output = output.Replace("{maxDex}", maxDex);
            output = output.Replace("{minInt}", minInt);
            output = output.Replace("{maxInt}", maxInt);
            output = output.Replace("{minHits}", minHits);
            output = output.Replace("{maxHits}", maxHits);
            output = output.Replace("{minDamage}", minDamage);
            output = output.Replace("{maxDamage}", maxDamage);
            output = output.Replace("{physDamage}", physDamage);
            output = output.Replace("{fireDamage}", fireDamage);
            output = output.Replace("{coldDamage}", coldDamage);
            output = output.Replace("{energyDamage}", energyDamage);
            output = output.Replace("{poisonDamage}", poisonDamage);
            output = output.Replace("{physResistMin}", physResistMin);
            output = output.Replace("{physResistMax}", physResistMax);
            output = output.Replace("{fireResistMin}", fireResistMin);
            output = output.Replace("{fireResistMax}", fireResistMax);
            output = output.Replace("{coldResistMin}", coldResistMin);
            output = output.Replace("{coldResistMax}", coldResistMax);
            output = output.Replace("{energyResistMin}", energyResistMin);
            output = output.Replace("{energyResistMax}", energyResistMax);
            output = output.Replace("{poisonResistMin}", poisonResistMin);
            output = output.Replace("{poisonResistMax}", poisonResistMax);
            output = output.Replace("{fame}", karma);
            output = output.Replace("{karma}", fame);
            output = output.Replace("{VArmor}", VArmor);
            output = output.Replace("{minAnatomy}", minAnatomy); // Start Of Skills
            output = output.Replace("{maxAnatomy}", maxAnatomy);
            output = output.Replace("{minArchery}", minArchery);
            output = output.Replace("{maxArchery}", maxArchery);
            output = output.Replace("{minEvalInt}", minEvalInt);
            output = output.Replace("{maxEvalInt}", maxEvalInt);
            output = output.Replace("{minFencing}", minFencing);
            output = output.Replace("{maxFencing}", maxFencing);
            output = output.Replace("{minFocus}", minFocus);
            output = output.Replace("{maxFocus}", maxFocus);
            output = output.Replace("{minHealing}", minHealing);
            output = output.Replace("{maxHealing}", maxHealing);
            output = output.Replace("{minMacing}", minMacing);
            output = output.Replace("{maxMacing}", maxMacing);
            output = output.Replace("{minMagery}", minMagery);
            output = output.Replace("{maxMagery}", maxMagery);
            output = output.Replace("{minMeditation}", minMeditation);
            output = output.Replace("{maxMeditation}", maxMeditation);
            output = output.Replace("{minNecromancy}", minNecromancy);
            output = output.Replace("{maxNecromancy}", maxNecromancy);
            output = output.Replace("{minParry}", minParry);
            output = output.Replace("{maxParry}", maxParry);
            output = output.Replace("{minMagicResist}", minMagicResist);
            output = output.Replace("{maxMagicResist}", maxMagicResist);
            output = output.Replace("{minSwords}", minSwords);
            output = output.Replace("{maxSwords}", maxSwords);
            output = output.Replace("{minTactics}", minTactics);
            output = output.Replace("{maxTactics}", maxTactics);
            output = output.Replace("{minWrestling}", minWrestling);
            output = output.Replace("{maxWrestling}", maxWrestling);

            return output;
        }

		public static void GenerateMobile( Mobile from, string[] statentry, string[] skillentry )
		{
			
            bool fail = false;
			
            StreamWriter writer = null;
			string path = Path.Combine( Core.BaseDirectory, string.Format( @"Scripts\Generated\{0}.cs", statentry[0] ) );

			try
			{
				string folder = Path.GetDirectoryName( path );

				if ( ! Directory.Exists( folder ) )
				{
					Directory.CreateDirectory( folder );
				}

				writer = new StreamWriter( path, false );
				writer.Write( TryAssignReplace( from, statentry, skillentry ) );
			}
			catch
			{
				from.SendMessage( 0x40, "Something messed up!" );
				fail = true;
			}
			finally
			{
				if ( writer != null )
					writer.Close();
			}

			if ( ! fail )
			{
				from.SendMessage( 0x40, "Script Saved - {0}", path );
			}
		}
    }
}
