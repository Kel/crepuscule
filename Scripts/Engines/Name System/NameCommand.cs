using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.Targeting;
using Server.Misc;
using Server.Gumps;
using Server.Multis;
using Server.Engines.Help;

namespace Server.Scripts.Commands
{

	public class RenomCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("renom", AccessLevel.Player, new CommandEventHandler(Renom_OnCommand));
            Server.Commands.Register("remon", AccessLevel.Player, new CommandEventHandler(Renom_OnCommand)); //typo
		}

        [Usage("<texte>")] 
	    [Description( "Donne un nom au joueur")]
        public static void Renom_OnCommand(CommandEventArgs e)
		{
			if ( e.Length != 1 ){
				e.Mobile.SendMessage( "Utilisation : .renom <texte>" );
			}else{
                e.Mobile.Target = new ChangeNameTarget((string)e.GetString(0));
			}
		} 
	}

	public class ChangeNameTarget : Target
	{
		private string m_Value;

		public ChangeNameTarget( string value ) : base( -1, false, TargetFlags.None )
		{
			m_Value = value;
		}

		protected override void OnTarget( Mobile from, object targeted )
		{
			if ( targeted is RacePlayerMobile )
			{
				var targ = (RacePlayerMobile)targeted;

                if (targ.Incognito == 0)
                {
                    int v_Serial = (int)from.Serial.Value;

                    if (targ.Serial.Value != from.Serial.Value)
                        targ.SetName(v_Serial, (string)m_Value, from);
                }
                else
                {
                    from.SendMessage("Il est impossible de mémoriser cette personne pour l'instant");
                }
			}
		}
	}



}
