﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Mobiles
{
    public class NameAndSerial
    {
        private int m_Serial;
        private string m_Name;
        public int Serial
        {
            get { return m_Serial; }
            set { m_Serial = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
    }

}
