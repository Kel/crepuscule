using System;
using Server;
using Server.Mobiles;
using Server.Network;

namespace Server.Items
{
    public class foulard_assassin : Base10
    {
        private string m_title;
        private short m_uses;

        [Constructable]
        public foulard_assassin()
            : this(0)
        {
        }


        [Constructable]
        public foulard_assassin(int hue)
            : base(0x35F3)
        {
            Name = "Foulard sombre";
            Weight = 0.1;
        }

        public foulard_assassin(Serial serial)
            : base(serial)
        {
        }

        public override bool OnEquip(Mobile from)
        {
            if (from is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)from;
                pm.Incognito = 1;
            }
            return base.OnEquip(from);
        }

        public override void OnRemoved(object parent)
        {
            if (parent is RacePlayerMobile)
            {
                RacePlayerMobile pm = (RacePlayerMobile)parent;
                pm.Incognito = 0;
            }
            base.OnRemoved(parent);
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)1); // version
            writer.Write((string)m_title);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
            if (version == 0)
            {
                m_title = reader.ReadString();
            }
            m_title = reader.ReadString();
        }
    }
}