﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using System.Collections;

namespace Server
{
    public class PlayerKnown
    {
        public PlayerKnown(RacePlayerMobile _Known, IdentityType _Type)
        {
            Known = _Known;
            Type = _Type;
        }
        public RacePlayerMobile Known;
        public IdentityType Type;
    }

    [Flags]
    public enum IdentityType
    {
        First,
        Second,
        Both = First | Second
    }


    public class PlayerKnownDictionary : Dictionary<Serial,PlayerKnown>
    {
        public bool TryAddCurrent(RacePlayerMobile mobile)
        {
            return TryAdd(mobile, mobile.CurrentIdentityType);
        }

        public bool TryRemoveCurrent(RacePlayerMobile mobile)
        {
            return TryRemove(mobile, mobile.CurrentIdentityType);
        }

        public bool TryAdd(RacePlayerMobile mobile, IdentityType identity)
        {
            if (!this.ContainsKey(mobile.Serial))
            {
                PlayerKnown newKnown = new PlayerKnown(mobile, identity);
                this.Add(mobile.Serial, newKnown);
                return true;
            }
            else
            {
                if (identity == IdentityType.First)
                {
                    if (this[mobile.Serial].Type == IdentityType.Second)
                    {
                        this[mobile.Serial].Type |= IdentityType.First;
                        return true;
                    }
                }
                else if (identity == IdentityType.Second)
                {
                    if (this[mobile.Serial].Type == IdentityType.First)
                    {
                        this[mobile.Serial].Type |= IdentityType.Second;
                        return true;
                    }
                }
                else if (identity == IdentityType.Both)
                {
                    this[mobile.Serial].Type = IdentityType.Both;
                    return true;
                }
                return false;
            }
        }

        public bool TryRemove(RacePlayerMobile mobile, IdentityType identity)
        {
            if (this.ContainsKey(mobile.Serial))
            {
                if (identity == IdentityType.First)
                {
                    if (this[mobile.Serial].Type == IdentityType.Both)
                    {
                        this[mobile.Serial].Type = IdentityType.Second;
                        return true;
                    }
                    if (this[mobile.Serial].Type == IdentityType.First)
                    {
                        this.Remove(mobile.Serial);
                        return true;
                    }
                }
                else if (identity == IdentityType.Second)
                {
                    if (this[mobile.Serial].Type == IdentityType.Both)
                    {
                        this[mobile.Serial].Type = IdentityType.First;
                        return true;
                    }
                    if (this[mobile.Serial].Type == IdentityType.Second)
                    {
                        this.Remove(mobile.Serial);
                        return true;
                    }
                }
                else if (identity == IdentityType.Both)
                {
                    this.Remove(mobile.Serial);
                    return true;
                }
            }
            return false;
        }


        public List<PlayerKnown> GetFirstList()
        {
            List<PlayerKnown> result = new List<PlayerKnown>();
            foreach (PlayerKnown known in this.Values)
            {
                if (known.Type == IdentityType.First
                    || known.Type == (IdentityType.First | IdentityType.Second))
                    result.Add(known);
            }
            return result;
        }

        public List<PlayerKnown> GetSecondList()
        {
            List<PlayerKnown> result = new List<PlayerKnown>();
            foreach (PlayerKnown known in this.Values)
            {
                if (known.Type == IdentityType.Second
                    || known.Type == (IdentityType.First | IdentityType.Second))
                    result.Add(known);
            }
            return result;
        }
    }

}
