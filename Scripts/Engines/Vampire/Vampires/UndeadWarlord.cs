using System;
using Server.Misc;
using Server.Network;
using System.Collections;
using Server.Items;
using Server.Targeting;

namespace Server.Mobiles
{
	public class UndeadWarlord : BaseCreature
	{
		[Constructable]
		public UndeadWarlord():base( AIType.AI_Melee, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			Body = 0x190;
			Name = "Sir de guerre mortvivant";
			Hue = -1;

			this.InitStats(Utility.Random(359,399), Utility.Random(138,151), Utility.Random(76,97));

			this.Skills[SkillName.Wrestling].Base = Utility.Random(74,80);
			this.Skills[SkillName.Swords].Base = Utility.Random(90,95);
			this.Skills[SkillName.Anatomy].Base = Utility.Random(120,125);
			this.Skills[SkillName.MagicResist].Base = Utility.Random(90,94);
			this.Skills[SkillName.Tactics].Base = Utility.Random(90,95);

			this.Fame = Utility.Random(5000,9999);
			this.Karma = Utility.Random(-5000,-9999);
			this.VirtualArmor = 40;

			BoneArms arms = new BoneArms();
			arms.Hue = 1109;
			arms.LootType = LootType.Blessed;
			AddItem( arms );

			BoneGloves gloves = new BoneGloves();
			gloves.Hue = 1109;
			gloves.LootType = LootType.Blessed;
			AddItem( gloves );

			BoneChest tunic = new BoneChest();
			tunic.Hue = 1109;
			tunic.LootType = LootType.Blessed;
			AddItem( tunic );
			BoneLegs legs = new BoneLegs();
			legs.Hue = 1109;
			legs.LootType = LootType.Blessed;
			AddItem( legs );

			BoneHelm helm = new BoneHelm();
			helm.Hue = 1109;
			helm.LootType = LootType.Blessed;
			AddItem( helm );

		
			AddItem( new Buckler());
			Hue = 1109;
			
			VikingSword weapon = new VikingSword();
			Hue = 1109;

			weapon.Movable = true;

			AddItem( weapon );
		}

		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool Unprovokable{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Deadly; } }

		[CommandProperty( AccessLevel.GameMaster )]
		public override int HitsMax { get { return 769; } }

		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.AngelsandUndead; }
		//}

		public UndeadWarlord( Serial serial ) : base( serial )
		{
		}

	

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
