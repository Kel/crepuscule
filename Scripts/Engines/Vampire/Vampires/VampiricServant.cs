using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "corps d'un vampire" )]
	public class VampiricServant : BaseCreature
	{
		[Constructable]
		public VampiricServant () : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			SpeechHue = Utility.RandomDyedHue();
			Title = "serveur vampire";

			if ( this.Female = Utility.RandomBool() )
			{
				Body = 184;
				Name = NameList.RandomName( "vampire" );
				AddItem( new PlainDress(32) );
			}
			else
			{
				Body = 183;
				Name = NameList.RandomName( "vampire" );
				AddItem( new LongPants(32) );
			}

			SetStr( 188, 320 );
			SetDex( 101, 150 );
			SetInt( 198, 257 );

			SetHits( 125, 150 );

			SetDamage( 20, 25 );

			SetDamageType( ResistanceType.Physical, 50 );
			SetDamageType( ResistanceType.Cold, 50 );

			SetResistance( ResistanceType.Physical, 50, 60 );
			SetResistance( ResistanceType.Fire, 30, 40 );
			SetResistance( ResistanceType.Cold, 30, 40 );
			SetResistance( ResistanceType.Poison, 60, 70 );
			SetResistance( ResistanceType.Energy, 20, 30 );

			SetSkill( SkillName.EvalInt, 40.1, 50.0 );
			SetSkill( SkillName.Magery, 45.1, 55.0 );
			SetSkill( SkillName.Meditation, 60.1, 70.0 );
			SetSkill( SkillName.MagicResist, 50.5, 60 );
			SetSkill( SkillName.Tactics, 50.1, 60.0 );
			SetSkill( SkillName.Wrestling, 80.1, 90 );

			Fame = 8000;
			Karma = -8000;

			VirtualArmor = 40;
		
			LongHair hair = new LongHair();
			hair.Hue = 2406;
			hair.Layer = Layer.Hair;
			hair.Movable = false;

			AddItem( hair );
			
			//PackItem( new OrdersBook() );

			PackGold( 50, 100 );
			//PackItem ( new Token( 20, 30 ) );
			
		}
		
		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool ShowFameTitle{ get{ return false; } }
		
		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.KnightsAndVampires; }
		//}

		public VampiricServant( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
