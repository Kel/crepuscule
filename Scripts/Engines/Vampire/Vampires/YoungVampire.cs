using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "corps d'un vampire" )]
	public class YoungVampire1 : BaseCreature
	{
		[Constructable]
		public YoungVampire1 () : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			SpeechHue = Utility.RandomDyedHue();
			Title = "Jeune vampire";

			if ( this.Female = Utility.RandomBool() )
			{
				Body = 184;
				Name = NameList.RandomName( "vampire" );
				AddItem( new PlainDress(2406) );
			}
			else
			{
				Body = 183;
				Name = NameList.RandomName( "vampire" );
				AddItem( new LongPants(2406) );
			}

			SetStr( 188, 320 );
			SetDex( 101, 150 );
			SetInt( 198, 257 );

			SetHits( 125, 150 );

			SetDamage( 20, 25 );

			SetDamageType( ResistanceType.Physical, 50 );
			SetDamageType( ResistanceType.Cold, 50 );

			SetResistance( ResistanceType.Physical, 60, 70 );
			SetResistance( ResistanceType.Fire, 50, 60 );
			SetResistance( ResistanceType.Cold, 30, 40 );
			SetResistance( ResistanceType.Poison, 80, 90 );
			SetResistance( ResistanceType.Energy, 30, 40 );

			SetSkill( SkillName.EvalInt, 60.1, 70.0 );
			SetSkill( SkillName.Magery, 65.1, 75.0 );
			SetSkill( SkillName.Meditation, 80.1, 90.0 );
			SetSkill( SkillName.MagicResist, 70.5, 100 );
			SetSkill( SkillName.Tactics, 60.1, 70.0 );
			SetSkill( SkillName.Wrestling, 80.1, 90 );

			Fame = 8000;
			Karma = -8000;

			VirtualArmor = 40;
		
			LongHair hair = new LongHair();
			hair.Hue = 2406;
			hair.Layer = Layer.Hair;
			hair.Movable = false;

			AddItem( hair );

			PackGold( 300, 600 );
			//PackItem ( new Token( 200, 300 ) );
			PackScroll( 1, 7 );
			PackArmor( 1, 5, 0.30 );
			PackWeapon( 1, 5, 0.30 );
			
		}
		
		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool Unprovokable{ get{ return true; } }
		public override bool ShowFameTitle{ get{ return false; } }
		
		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.KnightsAndVampires; }
		//}

		public void DrainLife()
		{
			Map map = this.Map;

			if ( map == null )
				return;

			IPooledEnumerable eable = map.GetMobilesInRange( this.Location, 2 );
			ArrayList list = new ArrayList();

			foreach ( Mobile m in eable )
			{
				if ( m == this || !CanBeHarmful( m ) )
					continue;

				if ( m is BaseCreature && (((BaseCreature)m).Controled || ((BaseCreature)m).Summoned || ((BaseCreature)m).Team != this.Team) )
					list.Add( m );
				else if ( m.Player )
					list.Add( m );
			}

			eable.Free();

			foreach ( Mobile m in list )
			{
				DoHarmful( m );

				m.FixedParticles( 0x374A, 10, 15, 5013, 0x496, 0, EffectLayer.Waist );
				m.PlaySound( 0x213 );

				m.Say( "Ahh! La soif!" );

				int toDrain = Utility.RandomMinMax( 5, 20 );

				Hits += toDrain;
				m.Damage( toDrain, this );
			}
		}

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public YoungVampire1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
