using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0xDF1, 0xDF0 )]
	public class HeavenlyStaff : BaseStaff
	{
		public override int ArtifactRarity{ get{ return 11; } }
		
		public override WeaponAbility PrimaryAbility{ get{ return WeaponAbility.WhirlwindAttack; } }
		public override WeaponAbility SecondaryAbility{ get{ return WeaponAbility.ParalyzingBlow; } }

		public override int AosIntelligenceReq{ get{ return 90;} }
		public override int AosDexterityReq{ get{ return 55;} }

		public override int AosStrengthReq{ get{ return 20; } }
		public override int AosMinDamage{ get{ return 10; } }
		public override int AosMaxDamage{ get{ return 16; } }
		public override int AosSpeed{ get{ return 43; } }

		public override int OldStrengthReq{ get{ return 35; } }
		public override int OldMinDamage{ get{ return 8; } }
		public override int OldMaxDamage{ get{ return 33; } }
		public override int OldSpeed{ get{ return 35; } }

		[Constructable]
		public HeavenlyStaff() : base( 0xDF1 )
		{
			Name = "Bat�n �trange";
			Hue = 1153; 
			
			WeaponAttributes.HitEnergyArea = 100;
			WeaponAttributes.HitLeechMana = 50;
			Attributes.BonusInt = 15;
			Attributes.WeaponDamage = 25;
			Attributes.SpellChanneling = 1;
			Attributes.CastSpeed = 5;
			
		}

		public HeavenlyStaff( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
