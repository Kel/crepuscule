// By Tru
using System;
using Server.Network;
using Server.Mobiles;
namespace Server.Items
{
 public class AkashasHeart : Food
	{
		[Constructable]
		public AkashasHeart() : this( 1 )
		{
		}

		[Constructable]
		public AkashasHeart( int amount ) : base( 0x171f )
		{
                        this.Name = "Coeur";
                        this.Stackable = false;
			this.Weight = 1.0;
			this.FillFactor = 100;
                        this.Amount = amount;
                        this.ItemID = 7405;
		}
  
                public override void OnDoubleClick( Mobile from )
		{
			PlayerMobile pm = from as PlayerMobile;

			if ( !IsChildOf( from.Backpack ) )
			{
				from.SendLocalizedMessage( 1042001 ); // That must be in your pack for you to use it.
			}
                        else if ( ((PlayerMobile)from).Vampirism == false )
			{
                                from.SendMessage( "This is the heart of a Vampire." );
				//from.SendLocalizedMessage( 1042594 ); // You do not understand how to use this.
			}
			else if ( pm.m_VampType == VampType.Master || pm.m_VampType == VampType.Ancient )
			{
				from.SendMessage( "Eating this heart would gain you nothing." );
			}
			else
			{
                                pm.m_VampType = VampType.Master;
				pm.SendMessage( "You feel more powerful." );
				Delete();
			}
		}
		public AkashasHeart( Serial serial ) : base( serial )
		{
		}

		public override Item Dupe( int amount )
		{
			return base.Dupe( new AkashasHeart(), amount );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
