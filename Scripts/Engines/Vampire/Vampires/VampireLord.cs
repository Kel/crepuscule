using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "corps d'un vampire" )]
	public class VampireLord1 : BaseCreature
	{
		[Constructable]
		public VampireLord1 () : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			SpeechHue = Utility.RandomDyedHue();
			Title = "vampire lord";

			if ( this.Female = Utility.RandomBool() )
			{
				Body = 184;
				Name = NameList.RandomName( "vampire" );
				AddItem( new FancyDress( 0x1 ) );
			}
			else
			{
				Body = 183;
				Name = NameList.RandomName( "vampire" );
				AddItem( new LongPants( 0x1) );
				AddItem( new Cloak( 32 ) );
				AddItem( new FancyShirt() );
			}

			SetStr( 420, 600 );
			SetDex( 121, 170 );
			SetInt( 250, 357 );

			SetHits( 600, 750 );

			SetDamage( 35, 50 );

			SetDamageType( ResistanceType.Physical, 50 );
			SetDamageType( ResistanceType.Cold, 50 );

			SetResistance( ResistanceType.Physical, 80, 90 );
			SetResistance( ResistanceType.Fire, 60, 70 );
			SetResistance( ResistanceType.Cold, 60, 70 );
			SetResistance( ResistanceType.Poison, 120 );
			SetResistance( ResistanceType.Energy, 60, 70 );

			SetSkill( SkillName.EvalInt, 90.1, 100.0 );
			SetSkill( SkillName.Magery, 95.1, 100.0 );
			SetSkill( SkillName.Meditation, 100.0 );
			SetSkill( SkillName.MagicResist, 150.5, 200 );
			SetSkill( SkillName.Tactics, 90.1, 100 );
			SetSkill( SkillName.Wrestling, 120 );
			SetSkill( SkillName.Hiding, 120 );

			Fame = 30000;
			Karma = -30000;

			VirtualArmor = 65;
			
			AddItem( new ThighBoots( 0x1 ) );
			
			PonyTail hair = new PonyTail();
			hair.Hue = 2406;
			hair.Layer = Layer.Hair;
			hair.Movable = false;

			AddItem( hair );

			PackGem(4, 10);
			PackGold( 800, 1200 );
			//PackItem ( new Token( 600, 1000 ) );
			PackScroll( 1, 7 );
			PackScroll( 1, 7 );
			PackArmor( 1, 5, 0.70 );
			PackWeapon( 1, 5, 0.70 );
			PackMagicItems( 1, 5 );
			PackMagicItems( 3, 5, 0.60, 0.50 );
			PackSlayer();
		}
		
		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool Unprovokable{ get{ return true; } }
		public override bool AutoDispel{ get{ return true; } }
		public override bool ShowFameTitle{ get{ return false; } }
		
		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.KnightsAndVampires; }
		//}

		public void DrainLife()
		{
			Map map = this.Map;

			if ( map == null )
				return;

			IPooledEnumerable eable = map.GetMobilesInRange( this.Location, 2 );
			ArrayList list = new ArrayList();

			foreach ( Mobile m in eable )
			{
				if ( m == this || !CanBeHarmful( m ) )
					continue;

				if ( m is BaseCreature && (((BaseCreature)m).Controled || ((BaseCreature)m).Summoned || ((BaseCreature)m).Team != this.Team) )
					list.Add( m );
				else if ( m.Player )
					list.Add( m );
			}

			eable.Free();

			foreach ( Mobile m in list )
			{
				DoHarmful( m );

				m.FixedParticles( 0x374A, 10, 15, 5013, 0x496, 0, EffectLayer.Waist );
				m.PlaySound( 0x213 );

				m.Say( "Give yourself to eternal night!" );

				int toDrain = Utility.RandomMinMax( 35, 50 );

				Hits += toDrain;
				m.Damage( toDrain, this );
			}
		}

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public VampireLord1( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
