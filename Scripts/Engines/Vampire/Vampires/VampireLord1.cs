using System;
using Server.Misc;
using Server.Network;
using System.Collections;
using Server.Items;
using Server.Targeting;

namespace Server.Mobiles
{
   public class VampireLord : BaseCreature
   {

      [Constructable]
      public VampireLord():base(AIType.AI_Mage, FightMode.Weakest, 15, 1, 0.2, 0.6)
      {
         Body = 0x190;
       BaseSoundID = 412;
         Name = NameList.RandomName( "vampire" );
         Title = "vampire";

         SetStr( 126, 156 );
         SetDex( 105, 135 );
         SetInt( 130, 153 );
       SetHits( 360, 395 );
         SetSkill( SkillName.Wrestling, 91.3, 97.8 );
         SetSkill( SkillName.Tactics, 91.5, 99.0 );
         SetSkill( SkillName.MagicResist, 150.5, 200.0 );
         SetSkill( SkillName.Magery, 91.7, 99.0 );
       SetSkill( SkillName.Poisoning, 100.1, 101.0 );
         SetSkill( SkillName.EvalInt, 100.1, 100.1 );
         SetSkill( SkillName.Meditation, 121.1, 128.1 );

         VirtualArmor = 36;

         LeatherGloves gloves = new LeatherGloves();
         gloves.Hue = 1157;
         AddItem( gloves );
         Cloak cloak = new Cloak();
         cloak.Hue = 1157;
         AddItem( cloak );
         Kilt kilt = new Kilt();
         kilt.Hue = 1157;
         AddItem( kilt );

         Item hair = new Item( Utility.RandomList( 0x203B, 0x2049, 0x2048, 0x204A ) );
         hair.Hue = 1175;
         hair.Layer = Layer.Hair;
         hair.Movable = false;
         AddItem( hair );
         Item beard = new Item( Utility.RandomList( 0x0000, 0x203E, 0x203F, 0x2040, 0x2041, 0x2067, 0x2068, 0x2069 ) );
         beard.Hue = hair.Hue;
         beard.Layer = Layer.FacialHair;
         beard.Movable = false;
         AddItem( beard );

         Sandals sandals = new Sandals();
         sandals.Hue = 1157;
         AddItem( sandals );

      }

      public override bool AlwaysMurderer{ get{ return true; } }
      public override bool Unprovokable{ get{ return true; } }
     public override Poison PoisonImmune{ get{ return Poison.Lethal; } }

      [CommandProperty( AccessLevel.GameMaster )]
      public override int HitsMax { get { return 593; } }

      public VampireLord( Serial serial ) : base( serial )
      {
      }

      public override bool OnBeforeDeath()
      {
         LostSoul rm = new LostSoul();
         rm.Map = this.Map;
         rm.Location = this.Location;
         Effects.SendLocationEffect( Location,Map, 0x3709, 13, 0x3B2, 0 );

         Container bag = new Bag();

         switch ( Utility.Random( 9 ))
         {
            case 0: bag.DropItem( new Amber(3) ); break;
            case 1: bag.DropItem( new Amethyst(3) ); break;
            case 2: bag.DropItem( new Citrine(3) ); break;
            case 3: bag.DropItem( new Diamond(3) ); break;
            case 4: bag.DropItem( new Emerald(3) ); break;
            case 5: bag.DropItem( new Ruby(3) ); break;
            case 6: bag.DropItem( new Sapphire(3) ); break;
            case 7: bag.DropItem( new StarSapphire(3) ); break;
            case 8: bag.DropItem( new Tourmaline(3) ); break;
         }

         LeatherGloves gloves = new LeatherGloves();
         gloves.Hue = 1157;
         bag.DropItem( gloves );
         Cloak cloak = new Cloak();
         cloak.Hue = 1157;
         bag.DropItem( cloak );
         Kilt kilt = new Kilt();
         kilt.Hue = 1157;
         bag.DropItem( kilt );
         Sandals sandals = new Sandals();
         sandals.Hue = 1157;
         bag.DropItem( sandals );

         bag.DropItem( new Gold(3000,5500));
         rm.AddItem( bag );

         this.Delete();

         return false;
      }

      public override void Serialize( GenericWriter writer )
      {

         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void OnMovement( Mobile m, Point3D oldLocation )
      {
         if ( m.InRange( this, 40 ))
         {
            //if   ( Map.GlobalLight >= 10 || m.Region.Name == "Wind" )
             if ( m.Region.Name == "Covetous" || m.Region.Name == "Despise" || m.Region.Name == "Solen Hive" || m.Region.Name == "Orc Dungeon" || m.Region.Name == "Destard" || m.Region.Name == "Hythloth" || m.Region.Name == "Wrong" || m.Region.Name == "Wind" || m.Region.Name == "Fire" || m.Region.Name == "Ice" || m.Region.Name == "Mt Kendall" || m.Region.Name == "Ice Isle Cave 1" || m.Region.Name == "Ice Isle Cave 2" || m.Region.Name == "Avatar Isle Cave" || m.Region.Name == "Rock Dungeon" || m.Region.Name == "Spider Cave" || m.Region.Name == "Spectre Dungeon" || m.Region.Name == "Blood Dungeon" || m.Region.Name == "Ankh Dungeon" || m.Region.Name == "Wisp Dungeon" || m.Region.Name == "Exodus Dungeon" || m.Region.Name == "Sorcerer's Dungeon" || m.Region.Name == "Doom")
            {
            }
            else
            {
               Say("The light, the liiiight!");
               this.Delete();
            }
         }
         if ( m.InRange( this, 2 ) && Combatant == m )
         {
            int takeabite=Utility.Random( 1, 5 ); // min 1 max 5
            if(takeabite == 1)
            {
               int sucking = Utility.Random( 2, 5 ); // min 2 max 10 = damage & heal to npc

               if ( this.Hits <= ( this.HitsMax - sucking )) // checks to see if he needs the HP from the player
               {
                  this.Hits += sucking;
                  m.Damage(sucking);
                  m.SendMessage("{0} has sucked some life force out of you.", this.Name );
               }

            }
         }
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
