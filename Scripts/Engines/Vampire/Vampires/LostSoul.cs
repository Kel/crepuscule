using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
   [CorpseName( "un esprit liber�" )]
   public class LostSoul : BaseCreature
   {
      [Constructable]
      public LostSoul() : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
      {
         Name = "un esprit perdu";
         Body = 26;
         Hue = 1153;
         BaseSoundID = 0x482;

         SetStr( 56, 60 );
         SetDex( 56, 65 );
         SetInt( 36, 60 );

         SetHits( 96, 100 );

         SetDamage( 7, 11 );

         SetDamageType( ResistanceType.Physical, 50 );
         SetDamageType( ResistanceType.Cold, 50 );

         SetResistance( ResistanceType.Physical, 25, 30 );
         SetResistance( ResistanceType.Cold, 15, 25 );
         SetResistance( ResistanceType.Poison, 100, 200 );

         SetSkill( SkillName.EvalInt, 5.1, 7.0 );
         SetSkill( SkillName.Magery, 5.1, 7.0 );
         SetSkill( SkillName.MagicResist, 155.1, 170.0 );
         SetSkill( SkillName.Tactics, 5.1, 6.0 );
         SetSkill( SkillName.Wrestling, 5.1, 6.0 );

         Fame = 10000;
         Karma = -10000;

         VirtualArmor = 28;

         Katana weapon = new Katana();

         weapon.DamageLevel = (WeaponDamageLevel)Utility.Random( 0, 5 );
         weapon.DurabilityLevel = (WeaponDurabilityLevel)Utility.Random( 0, 5 );
         weapon.AccuracyLevel = (WeaponAccuracyLevel)Utility.Random( 0, 5 );

         PackItem( weapon );
         PackScroll( 3, 8 );
         PackScroll( 3, 8 );

      }

      public override Poison PoisonImmune{ get{ return Poison.Lethal; } }

      public LostSoul( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 );
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();
      }
   }
}

