using System;
using Server.Network;
using Server.Items;

namespace Server.Items
{
	[FlipableAttribute( 0xDF1, 0xDF0 )]
	public class AngelicStaff : BaseStaff
	{
	
		
		public override WeaponAbility PrimaryAbility{ get{ return WeaponAbility.WhirlwindAttack; } }
		public override WeaponAbility SecondaryAbility{ get{ return WeaponAbility.ParalyzingBlow; } }

		public override int AosIntelligenceReq{ get{ return 30;} }
		public override int AosDexterityReq{ get{ return 30;} }

		public override int AosStrengthReq{ get{ return 20; } }
		public override int AosMinDamage{ get{ return 12; } }
		public override int AosMaxDamage{ get{ return 20; } }
		public override int AosSpeed{ get{ return 39; } }

		public override int OldStrengthReq{ get{ return 35; } }
		public override int OldMinDamage{ get{ return 8; } }
		public override int OldMaxDamage{ get{ return 33; } }
		public override int OldSpeed{ get{ return 35; } }

		[Constructable]
		public AngelicStaff() : base( 0xDF1 )
		{
			Name = "Bat�n Angelique";
			Hue = 1150; 
			
			WeaponAttributes.HitEnergyArea = 59;
			WeaponAttributes.HitLeechMana = 10;
			
			Attributes.WeaponDamage = 10;
			Attributes.SpellChanneling = 1;
			
			
		}

		public AngelicStaff( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}
