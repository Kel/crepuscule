using System;
using System.Collections;
using Server.Items;

namespace Server.Mobiles
{
   [CorpseName( "corps d'un vampire" )]
   public class newVampire : BaseCreature
   {
      [Constructable]
      public newVampire() : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
      {
         Body = 0x190;
         Name = NameList.RandomName( "vampire" );
         Hue = 1150;
         BaseSoundID = 412;

         SetStr( 216, 305 );
         SetDex( 96, 115 );
         SetInt( 966, 1045 );

         SetHits( 560, 595 );

         SetDamage( 15, 27 );

         SetDamageType( ResistanceType.Physical, 20 );
         SetDamageType( ResistanceType.Cold, 40 );
         SetDamageType( ResistanceType.Energy, 40 );

         SetResistance( ResistanceType.Physical, 55, 65 );
         SetResistance( ResistanceType.Fire, 25, 30 );
         SetResistance( ResistanceType.Cold, 50, 60 );
         SetResistance( ResistanceType.Poison, 50, 60 );
         SetResistance( ResistanceType.Energy, 25, 30 );

         SetSkill( SkillName.EvalInt, 120.1, 130.0 );
         SetSkill( SkillName.Magery, 120.1, 130.0 );
         SetSkill( SkillName.Meditation, 100.1, 101.0 );
         SetSkill( SkillName.Poisoning, 100.1, 101.0 );
         SetSkill( SkillName.MagicResist, 175.2, 200.0 );
         SetSkill( SkillName.Tactics, 90.1, 100.0 );
         SetSkill( SkillName.Wrestling, 120.1, 130.0 );

         Fame = 23000;
         Karma = -23000;

         VirtualArmor = 60;

                        AddItem( new Boots( Utility.RandomNeutralHue() ) );


         Item hair = new Item( Utility.RandomList( 0x203B, 0x2049, 0x2048, 0x204A ) );
         hair.Hue = 0x54321;
         hair.Layer = Layer.Hair;
         hair.Movable = false;
         AddItem( hair );
                  VampireSlayer weapon = new VampireSlayer();

         weapon.Movable = false;

         AddItem( weapon );

      VampireRobe Robe = new VampireRobe();
      Robe.Movable = false;
      AddItem( Robe );

         PackMagicItems( 4, 5, 0.60, 0.45  );
         PackMagicItems( 3, 5, 0.60, 0.45  );
         PackMagicItems( 3, 5, 0.60, 0.45  );
         PackMagicItems( 4, 5 );
         PackMagicItems( 4, 5 );
         PackMagicItems( 4, 5 );
         PackMagicItems( 4, 5 );
         PackGold( 3500,4050 );

          switch ( Utility.Random( 50 ))
         {
            case 0: PackItem( new VampireSlayer() ); break;
            case 1: PackItem( new VampireRobe() ); break;
            default: break;
           } }


      public override bool AutoDispel{ get{ return true; } }
      public override bool AlwaysMurderer{ get{ return true; } }
      public override Poison PoisonImmune{ get{ return Poison.Deadly; } }
      public override int TreasureMapLevel{ get{ return 5; } }
      public override bool BardImmune{ get{ return true; } }


      public newVampire( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 );
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();
      }
         public void SpawnVampireBat( Mobile m )
      {
         Map map = this.Map;

         if ( map == null )
            return;

         VampireBat spawned = new VampireBat();

         spawned.Team = this.Team;
         spawned.Map = map;

         bool validLocation = false;

         for ( int j = 0; !validLocation && j < 10; ++j )
         {
            int x = X + Utility.Random( 3 ) - 1;
            int y = Y + Utility.Random( 3 ) - 1;
            int z = map.GetAverageZ( x, y );

            if ( validLocation = map.CanFit( x, y, this.Z, 16, false, false ) )
               spawned.Location = new Point3D( x, y, Z );
            else if ( validLocation = map.CanFit( x, y, z, 16, false, false ) )
               spawned.Location = new Point3D( x, y, z );
         }

         if ( !validLocation )
            spawned.Location = this.Location;

         spawned.Combatant = m;
      }

      public void EatVampireBat()
      {
         Map map = this.Map;

         if ( map == null )
            return;

         ArrayList toEat = new ArrayList();

         IPooledEnumerable eable = map.GetMobilesInRange( this.Location, 2 );

         foreach ( Mobile m in eable )
         {
            if ( m is VampireBat )
               toEat.Add( m );
         }

         eable.Free();

         if ( toEat.Count > 0 )
         {
            PlaySound( Utility.Random( 0x3B, 2 ) ); // Eat sound

            foreach ( Mobile m in toEat )
            {
               Hits += (m.Hits / 2);
               m.Delete();
            }
         }
      }

      public override void OnGotMeleeAttack( Mobile attacker )
      {
         base.OnGotMeleeAttack( attacker );

         if ( this.Hits > (this.HitsMax / 4) )
         {
            if ( 0.25 >= Utility.RandomDouble() )
               SpawnVampireBat( attacker );
         }
         else if ( 0.25 >= Utility.RandomDouble() )
         {
            EatVampireBat();
         }
      }
   }
}
