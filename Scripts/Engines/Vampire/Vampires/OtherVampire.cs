using System;
using Server.Misc;
using Server.Network;
using System.Collections;
using Server.Items;
using Server.Targeting;

namespace Server.Mobiles
{
   public class Vampirebat1 : BaseCreature
   {
      [Constructable]
      public Vampirebat1():base( AIType.AI_Melee, FightMode.Weakest, 10, 1, 0.2, 0.4 )
      {
       Body = 317;
         Name = "a vampire chauve souris";
       BaseSoundID = 0x3F3;

         this.InitStats(Utility.Random(59,99), Utility.Random(83,91), Utility.Random(76,97));

         this.Skills[SkillName.Wrestling].Base = Utility.Random(74,80);
         this.Skills[SkillName.Swords].Base = Utility.Random(90,95);
         this.Skills[SkillName.Anatomy].Base = Utility.Random(90,105);
         this.SetSkill( SkillName.MagicResist, 150.5, 200.0 );
         this.Skills[SkillName.Tactics].Base = Utility.Random(80,85);

         this.VirtualArmor = 40;

         Container pack = new Backpack();

         pack.Movable = false;

         AddItem( pack );
      }

      public override bool AlwaysMurderer{ get{ return true; } }
      public override bool Unprovokable{ get{ return true; } }
      public override Poison PoisonImmune{ get{ return Poison.Deadly; } }

      [CommandProperty( AccessLevel.GameMaster )]
      public override int HitsMax { get { return 533; } }

      public Vampirebat1( Serial serial ) : base( serial )
      {
      }

      public override bool OnBeforeDeath()
      {
         VampireLord rm = new VampireLord();
         rm.Map = this.Map;
         rm.Location = this.Location;
         Effects.SendLocationEffect( Location,Map, 0x3709, 13, 0x3B2, 0 );

         this.Delete();

         return false;
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
