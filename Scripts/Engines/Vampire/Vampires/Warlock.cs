// created on 09/6/2003 at 2:52 AM
// BY: Laviathon
using System;
using Server.Misc;
using Server.Network;
using System.Collections;
using Server.Items;
using Server.Targeting;
using Server.Engines.CannedEvil;

namespace Server.Mobiles
{
   public class Warlock : BaseChampion
   {
         public override ChampionSkullType SkullType{ get{ return ChampionSkullType.Power; } }

      [Constructable]
      public Warlock() :base( AIType.AI_Mage )
      {
         Body = 0x190;
         Name = NameList.RandomName( "Male" );
         Title = "Warlock";

         SetStr( 250, 400 );
         SetDex( 250, 400 );
         SetInt( 250, 400 );

         SetHits( 2000 );
         SetStam( 400, 800 );

         SetDamage( 29, 35 );

         SetSkill( SkillName.Magery, 120.0, 200.0 );
         SetSkill( SkillName.EvalInt, 110.0, 200.0 );
         SetSkill( SkillName.Meditation, 115.0, 200.0 );
         SetSkill( SkillName.MagicResist, 120.0, 200.0);
         SetSkill( SkillName.Tactics, 115.0, 200.0);
         SetSkill( SkillName.Wrestling, 115.0, 200.0);

         SetResistance( ResistanceType.Fire, 55, 65 );
         SetResistance( ResistanceType.Cold, 30, 40 );
         SetResistance( ResistanceType.Poison, 40, 50 );
         SetResistance( ResistanceType.Physical, 40, 55 );
         SetResistance( ResistanceType.Energy, 35, 45 );

         Fame = 24000;
         Karma = -24000;

         VirtualArmor = 30;

         LeatherGloves gloves = new LeatherGloves();
         gloves.Hue = 1157;
         AddItem( gloves );
         Cloak cloak = new Cloak();
         cloak.Hue = 1157;
         AddItem( cloak );
         Kilt kilt = new Kilt();
         kilt.Hue = 1157;
         AddItem( kilt );
       Shirt shirt = new Shirt();
       shirt.Hue = 1157;
       AddItem( shirt );


         Item hair = new Item( Utility.RandomList( 0x203B, 0x2049, 0x2048, 0x204A ) );
         hair.Hue = 1175;
         hair.Layer = Layer.Hair;
         hair.Movable = false;
         AddItem( hair );
         Item beard = new Item( Utility.RandomList( 0x0000, 0x203E, 0x203F, 0x2040, 0x2041, 0x2067, 0x2068, 0x2069 ) );
         beard.Hue = hair.Hue;
         beard.Layer = Layer.FacialHair;
         beard.Movable = false;
         AddItem( beard );

         Sandals sandals = new Sandals();
         sandals.Hue = 1157;
         AddItem( sandals );

         PackGold( 6000, 7000 );
         PackWeapon( 3, 5 );
         PackWeapon( 5, 5 );

      }
      public override bool AlwaysMurderer{ get{ return true; } }
      public override bool Unprovokable{ get{ return true; } }
      public override Poison PoisonImmune{ get{ return Poison.Lethal; } }

      [CommandProperty( AccessLevel.GameMaster )]
      public override int HitsMax { get{ return 593; } }

      public Warlock( Serial serial ) : base( serial )
      {
         Container bag = new Bag();

          switch ( Utility.Random( 7 ))
         {
            case 0: bag.DropItem( new Amethyst(3) ); break;
            case 1: bag.DropItem( new Diamond(3) ); break;
            case 2: bag.DropItem( new Emerald(3) ); break;
            case 3: bag.DropItem( new Ruby(3) ); break;
            case 4: bag.DropItem( new Sapphire(3) ); break;
            case 5: bag.DropItem( new StarSapphire(3) ); break;

         }
      }
          public override void Serialize( GenericWriter writer )
      {

         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }
      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
