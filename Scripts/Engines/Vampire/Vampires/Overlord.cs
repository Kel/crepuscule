using System;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "corps de liche" )]
	public class Overlord : BaseCreature
	{
		[Constructable]
		public Overlord () : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			Name = NameList.RandomName( "ancient lich" );
			Title = "";
			Hue = 1109;
			Body = 9;
			BaseSoundID = 357;

			SetStr( 800, 900 );
			SetDex( 50, 90 );
			SetInt( 450, 475 );

			SetHits( 649, 744 );

			SetDamage( 20, 27 );

			SetDamageType( ResistanceType.Physical, 100 );
			SetDamageType( ResistanceType.Fire, 100 );

			SetResistance( ResistanceType.Physical, 45, 60 );
			SetResistance( ResistanceType.Fire, 50, 60 );
			SetResistance( ResistanceType.Cold, 30, 40 );
			SetResistance( ResistanceType.Poison, 20, 30 );
			SetResistance( ResistanceType.Energy, 30, 40 );

			SetSkill( SkillName.EvalInt, 85 );
			SetSkill( SkillName.Magery, 110.0 );
			SetSkill( SkillName.MagicResist, 150.0 );
			SetSkill( SkillName.Tactics, 100.0 );
			SetSkill( SkillName.Wrestling, 105.0 );
			SetSkill( SkillName.Necromancy, 120.0 );
			SetSkill( SkillName.SpiritSpeak, 120.0 );
			SetSkill( SkillName.Anatomy, 105.0 );

			Fame = 25000;
			Karma = -100000;

			VirtualArmor = 85;
			
				switch ( Utility.Random( 8 ))
			{
				case 0: PackItem( new SpidersSilk( 3 ) ); break;
				case 1: PackItem( new BlackPearl( 3 ) ); break;
				case 2: PackItem( new Bloodmoss( 3 ) ); break;
				case 3: PackItem( new Garlic( 3 ) ); break;
				case 4: PackItem( new MandrakeRoot( 3 ) ); break;
				case 5: PackItem( new Nightshade( 3 ) ); break;
				case 6: PackItem( new SulfurousAsh( 3 ) ); break;
				case 7: PackItem( new Ginseng( 3 ) ); break;
			}
			

			PackGold( 3300, 5000 );
			PackScroll( 1, 7 );
			PackScroll( 1, 7 );
			PackMagicItems( 1, 5 );
			PackWeapon( 1, 5, 0.7 );
			PackArmor( 1, 5, 0.7 );
			PackSlayer();
		}

		public override bool CanRummageCorpses{ get{ return true; } }
		public override Poison PoisonImmune{ get{ return Poison.Regular; } }
		public override int Meat{ get{ return 1; } }

		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.AngelsandUndead; }
		//}

		public Overlord( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
