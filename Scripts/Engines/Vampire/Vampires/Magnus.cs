using System;
using System.Collections;
using Server;
using Server.Items;
namespace Server.Mobiles

{
	[CorpseName( "corps d'un vampire" )]
	public class Magnus : BaseCreature
	{
		[Constructable]
		public Magnus() : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			Name = "Un ancien vampire";
			Body = 183;
			BaseSoundID = 412;

			SetStr( 700, 850 );
			SetDex( 400, 550 );
			SetInt( 505, 750 );

			SetHits( 5000 );
			SetStam( 102, 300 );

			SetDamage( 35, 45 );

			SetDamageType( ResistanceType.Physical, 50 );
			SetDamageType( ResistanceType.Cold, 50 );

			SetResistance( ResistanceType.Physical, 80, 90 );
			SetResistance( ResistanceType.Fire, 60, 70 );
			SetResistance( ResistanceType.Cold, 60, 70 );
			SetResistance( ResistanceType.Poison, 120 );
			SetResistance( ResistanceType.Energy, 60, 70 );

			SetSkill( SkillName.EvalInt, 120.0 );
			SetSkill( SkillName.Magery, 120.0 );
			SetSkill( SkillName.Meditation, 120.0 );
			SetSkill( SkillName.MagicResist, 150.0 );
			SetSkill( SkillName.Tactics, 97.6, 100.0 );
			SetSkill( SkillName.Wrestling, 97.6, 100.0 );

			Fame = 50000;
			Karma = -50000;
			AddItem( new LongPants( 0x1 ) );
			AddItem( new Cloak( 0x1 ) );
			AddItem( new FancyShirt() );
			AddItem( new ThighBoots( 0x1 ) );
			
			LongHair hair = new LongHair();
			hair.Hue = 2406 ;
			hair.Layer = Layer.Hair;
			hair.Movable = false;

			AddItem( hair );

			VirtualArmor = 30;
			Female = false;

			PackGem(4, 10);
			PackGold( 5000 );
			//PackItem ( new Token( 5000 ) );
			PackScroll( 1, 7 );
			PackScroll( 1, 7 );
			PackArmor( 1, 5, 0.99 );
			PackWeapon( 1, 5, 0.99 );
			PackMagicItems( 3, 6, 0.99, 1.0 );
			PackMagicItems( 3, 6, 0.99, 1.0 );
			PackSlayer();
			
			if ( 0.9 > Utility.RandomDouble() ) // 0.9 = 90% = chance to drop
			switch ( Utility.Random( 32 )) //number of alternatives
				{
  					 // Armors 
   						case 0: PackItem( new ArmorOfFortune() ); break;
   						case 1: PackItem( new GauntletsOfNobility() ); break;
   						case 2: PackItem( new HelmOfInsight() ); break;
   						case 3: PackItem( new HolyKnightsBreastplate() ); break;
   						case 4: PackItem( new InquisitorsResolution() ); break;
   						case 5: PackItem( new JackalsCollar() ); break;
   						case 6: PackItem( new LeggingsOfBane() ); break;
   						case 7: PackItem( new MidnightBracers() ); break; 
   						case 8: PackItem( new OrnateCrownOfTheHarrower() ); break; 
   						case 9: PackItem( new ShadowDancerLeggings() ); break; 
   						case 10: PackItem( new TunicOfFire() ); break; 
   						case 11: PackItem( new VoiceOfTheFallenKing() ); break; 
   					// Shields 
  				 		case 12: PackItem( new Aegis() ); break; 
   						case 13: PackItem( new ArcaneShield() ); break; 
   					// Weapons 
   						case 14: PackItem( new AxeOfTheHeavens() ); break; 
   						case 15: PackItem( new BladeOfInsanity() ); break; 
   						case 16: PackItem( new BladeOfTheRighteous() ); break; 
   						case 17: PackItem( new BoneCrusher() ); break; 
   						case 18: PackItem( new BreathOfTheDead() ); break; 
   						case 19: PackItem( new Frostbringer() ); break; 
   						case 20: PackItem( new LegacyOfTheDreadLord() ); break; 
   						case 21: PackItem( new SerpentsFang() ); break; 
   						case 22: PackItem( new StaffOfTheMagi() ); break; 
   						case 23: PackItem( new TheBeserkersMaul() ); break; 
   						case 24: PackItem( new TheDragonSlayer() ); break; 
   						case 25: PackItem( new TheTaskmaster() ); break; 
   						case 26: PackItem( new TitansHammer() ); break; 
   						case 27: PackItem( new ZyronicClaw() ); break; 
   					// Jewels 
   						case 28: PackItem( new BraceletOfHealth() ); break; 
   						case 29: PackItem( new OrnamentOfTheMagician() ); break; 
   						case 30: PackItem( new RingOfTheElements() ); break; 
   						case 31: PackItem( new RingOfTheVile() ); break; 
}
		}
		
		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool Unprovokable{ get{ return true; } }
		public override bool AutoDispel{ get{ return true; } }
		public override bool ShowFameTitle{ get{ return false; } }

		public void DrainLife()
		{
			Map map = this.Map;

			if ( map == null )
				return;

			IPooledEnumerable eable = map.GetMobilesInRange( this.Location, 2 );
			ArrayList list = new ArrayList();

			foreach ( Mobile m in eable )
			{
				if ( m == this || !CanBeHarmful( m ) )
					continue;

				if ( m is BaseCreature && (((BaseCreature)m).Controled || ((BaseCreature)m).Summoned || ((BaseCreature)m).Team != this.Team) )
					list.Add( m );
				else if ( m.Player )
					list.Add( m );
			}

			eable.Free();

			foreach ( Mobile m in list )
			{
				DoHarmful( m );

				m.FixedParticles( 0x374A, 10, 15, 5013, 0x496, 0, EffectLayer.Waist );
				m.PlaySound( 0x213 );

				m.Say( "You cannot kill me!" );

				int toDrain = Utility.RandomMinMax( 35, 50 );

				Hits += toDrain;
				m.Damage( toDrain, this );
			}
		}

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public Magnus( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
