using System; 
using Server; 

namespace Server.Items 
{ 
   public class VampireSlayer : WarFork 
   { 
       
      public override int ArtifactRarity{ get{ return 17; } } 

      [Constructable] 
      public VampireSlayer() 
      { 
         Hue = 0x47E; 
         WeaponAttributes.HitPoisonArea = 20; 
         Attributes.BonusDex = 5; 
         Attributes.AttackChance = 5; 
         Attributes.WeaponDamage = 10; 
         WeaponAttributes.UseBestSkill = 1; 
         Attributes.SpellChanneling = 1; 
         Attributes.SpellDamage = 3; 
         Attributes.ReflectPhysical = 5; 
         WeaponAttributes.HitLightning = 10; 
         Name = "Bane of the Vampire"; 
                                            
      } 

      public override void GetDamageTypes( Mobile wielder, out int phys, out int fire, out int cold, out int pois, out int nrgy ) 
      { 
         phys = 22; 
         fire = 16; 
         cold = 31; 
         nrgy = 10; 
         pois = 75; 
      } 

      public VampireSlayer( Serial serial ) : base( serial ) 
      { 
      } 

      public override void Serialize( GenericWriter writer ) 
      { 
         base.Serialize( writer ); 

         writer.Write( (int) 0 ); 
      } 
       
      public override void Deserialize(GenericReader reader) 
      { 
         base.Deserialize( reader ); 

         int version = reader.ReadInt(); 
      } 
   } 
}