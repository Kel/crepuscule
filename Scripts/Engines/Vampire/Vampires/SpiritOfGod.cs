using System; 
using System.Collections; 
using Server.Items; 

namespace Server.Mobiles 
{ 
  [CorpseName( "le corps" )] 
  public class SpiritOfGod : BaseCreature 
  { 
     [Constructable] 
     public SpiritOfGod() : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 ) 
     { 
        Name = "L'esprit de dieux"; 
        Hue = 1153; 
        Body = 58; 
        BaseSoundID = 466; 

        SetStr( 200, 215 ); 
        SetDex( 66, 75 ); 
        SetInt( 1000, 1200 ); 

        SetHits( 1000, 1250 ); 
        SetStam( 35, 75 ); 

        SetDamage( 15, 23 ); 

 
        SetDamageType( ResistanceType.Energy, 58 ); 

        SetResistance( ResistanceType.Physical, 90 ); 
        SetResistance( ResistanceType.Fire, 60 ); 
        SetResistance( ResistanceType.Cold, 60 ); 
        SetResistance( ResistanceType.Poison, 60 ); 
        SetResistance( ResistanceType.Energy, 60 ); 

        SetSkill( SkillName.EvalInt, 120 ); 
        SetSkill( SkillName.Magery, 130 ); 
        SetSkill( SkillName.MagicResist, 275 ); 
         

        Fame = 0; 
        Karma = -35000; 

        VirtualArmor = 70; 
     	
     	if ( 1.0 > Utility.RandomDouble() ) 
			switch ( Utility.Random( 1 ))
				{ 
					case 0: PackItem( new HeavenlyStaff() ); break;
				}

        

     } 

     public override Poison PoisonImmune{ get{ return Poison.Greater; } } 
      

     public SpiritOfGod( Serial serial ) : base( serial ) 
     { 
     } 

     public override void Serialize( GenericWriter writer ) 
     { 
        base.Serialize( writer ); 
        writer.Write( (int) 0 ); 
     } 

     public override void Deserialize( GenericReader reader ) 
     { 
        base.Deserialize( reader ); 
        int version = reader.ReadInt(); 
     } 
        public void SpawnRevivedAngel( Mobile m ) 
     { 
        Map map = this.Map; 

        if ( map == null ) 
           return; 

        RevivedAngel spawned = new RevivedAngel(); 

        spawned.Team = this.Team; 
        spawned.Map = map; 

        bool validLocation = false; 

        for ( int j = 0; !validLocation && j < 10; ++j ) 
        { 
           int x = X + Utility.Random( 3 ) - 1; 
           int y = Y + Utility.Random( 3 ) - 1; 
           int z = map.GetAverageZ( x, y ); 

           if ( validLocation = map.CanFit( x, y, this.Z, 16, false, false ) ) 
              spawned.Location = new Point3D( x, y, Z ); 
           else if ( validLocation = map.CanFit( x, y, z, 16, false, false ) ) 
              spawned.Location = new Point3D( x, y, z ); 
        } 

        if ( !validLocation ) 
           spawned.Location = this.Location; 

        spawned.Combatant = m; 
     } 

     

     public override void OnGotMeleeAttack( Mobile attacker ) 
     { 
        base.OnGotMeleeAttack( attacker ); 

        if ( this.Hits > (this.HitsMax / 4) ) 
        { 
           if ( 0.25 >= Utility.RandomDouble() ) 
              SpawnRevivedAngel( attacker ); 
        } 
        
     } 
  } 
} 
