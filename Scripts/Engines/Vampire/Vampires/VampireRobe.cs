using System;
using Server.Network;
using Server.Targeting;
using Server.Items;

namespace Server.Items
{
   [Flipable (0x1F04, 0x1F03)]
   public class VampireRobe : BaseOuterTorso

{

      public override int PhysicalResistance{ get{ return 50; } }
      public override int FireResistance{ get{ return 12; } }
      public override int ColdResistance{ get{ return 26; } }
      public override int PoisonResistance{ get{ return 31; } }
      public override int EnergyResistance{ get{ return 18; } }

      [Constructable]
      public VampireRobe() : base( 0x1F04 )
      {
              Hue = 1153;
              Weight = 1.0;
              Layer = Layer.OuterTorso;
              Name = "Toge de vampire";

                }

      public VampireRobe( Serial serial ) : base( serial )
      {
      }

      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );

         writer.Write( (int) 0 ); // version
      }

      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );

         int version = reader.ReadInt();
      }
   }
}
