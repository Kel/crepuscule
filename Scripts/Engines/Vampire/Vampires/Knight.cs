using System; 
using System.Collections; 
using Server.Items; 
using Server.ContextMenus; 
using Server.Misc; 
using Server.Network; 

namespace Server.Mobiles 
{ 
	public class Knight : BaseCreature
	{
		[Constructable]
			public Knight() : base( AIType.AI_Melee, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			InitStats( 500, 500, 500 );
			Title = ", Chevalier";

			SpeechHue = Utility.RandomDyedHue();

			Hue = Utility.RandomSkinHue();

			if ( Female = Utility.RandomBool() )
			{
				Body = 0x191;
				Name = NameList.RandomName( "female" );
				
				AddItem( new PlateChest() );
				AddItem( new PlateArms() );
				AddItem( new PlateGorget() );
				AddItem( new PlateLegs() );
				AddItem( new Cloak ( 8 ) );
				AddItem( new OrderShield () );

			    switch( Utility.Random( 3 ) )
				{
					case 0: AddItem( new Doublet( 8 ) ); break;
					case 1: AddItem( new Tunic( 8 ) ); break;
					case 2: AddItem( new BodySash( 8 ) ); break;
				}
			}
			else
			{
				Body = 0x190;
				Name = NameList.RandomName( "male" );

				AddItem( new PlateChest() );
				AddItem( new PlateArms() );
				AddItem( new PlateGorget() );
				AddItem( new PlateLegs() );
				AddItem( new Cloak ( 8 ) );
				AddItem( new OrderShield () );

				switch( Utility.Random( 3 ) )
				{
					case 0: AddItem( new Doublet( 8 ) ); break;
					case 1: AddItem( new Tunic( 8 ) ); break;
					case 2: AddItem( new BodySash( 8 ) ); break;
				}
			}

			Item hair = new Item( Utility.RandomList( 0x203B, 0x203C, 0x203D, 0x2044, 0x2045, 0x2047, 0x2049, 0x204A ) );

			hair.Hue = Utility.RandomHairHue();
			hair.Layer = Layer.Hair;
			hair.Movable = false;

			AddItem( hair );

			if( Utility.RandomBool() && !this.Female )
			{
				Item beard = new Item( Utility.RandomList( 0x203E, 0x203F, 0x2040, 0x2041, 0x204B, 0x204C, 0x204D ) );

				beard.Hue = hair.Hue;
				beard.Layer = Layer.FacialHair;
				beard.Movable = false;

				AddItem( beard );
			}
			
			new Horse().Rider = this;

			Broadsword weapon = new Broadsword();

			weapon.Movable = false;
			weapon.Crafter = this;
			weapon.Quality = WeaponQuality.Exceptional;

			AddItem( weapon );


			Skills[SkillName.Anatomy].Base = 120.0;
			Skills[SkillName.Tactics].Base = 120.0;
			Skills[SkillName.Swords].Base = 120.0;
			Skills[SkillName.Parry].Base = 100.0;
			Skills[SkillName.MagicResist].Base = 80.0;
			Skills[SkillName.DetectHidden].Base = 100.0;

		}
		
		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.KnightsAndVampires; }
		//}

		public Knight( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 
	} 
}
