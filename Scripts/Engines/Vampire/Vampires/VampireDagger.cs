using System;
using Server;
using Server.Network;
using Server.Targeting;
using Server.Items;
using Server.Mobiles;
using Server.Misc;
using System.Collections;

namespace Server.Items
{
   [FlipableAttribute( 0xF52, 0xF51 )]
   public class VampireDagger : BaseKnife
   {
     InternalTimer i_Timer;
     private static LightSource ls = new LightSource();
      private static bool Lit = false;
      public override WeaponAbility PrimaryAbility{ get{ return WeaponAbility.InfectiousStrike; } }
      public override WeaponAbility SecondaryAbility{ get{ return WeaponAbility.ShadowStrike; } }
      public override int AosStrengthReq{ get{ return 15; } }
      public override int AosMinDamage{ get{ return 1; } }
      public override int AosMaxDamage{ get{ return 12; } }
      public override int AosSpeed{ get{ return 55; } }
      public override int OldStrengthReq{ get{ return 1; } }
      public override int OldMinDamage{ get{ return 3; } }
      public override int OldMaxDamage{ get{ return 11; } }
      public override int OldSpeed{ get{ return 55; } }
      public override SkillName DefSkill{ get{ return SkillName.Fencing; } }
      public override WeaponType DefType{ get{ return WeaponType.Piercing; } }
      public override WeaponAnimation DefAnimation{ get{ return WeaponAnimation.Pierce1H; } }
      public override bool HandlesOnMovement{ get{ return true;} }
      [Constructable]
      public VampireDagger() : base( 0x13B9 )
      {
         Weight = 0.5;
         Consecrated = true;
         Slayer = SlayerName.Silver;
         Name = "dague du vampire";
     }
     public override bool OnEquip( Mobile m )
     {
        i_Timer = new InternalTimer( m, this );
        i_Timer.Start();
        base.OnEquip(m);
        return true;
      }
     public override void OnRemoved( Object o )
     {
        if( o is Mobile)
        {
           ((Mobile)o).RemoveItem(ls);
        }
        this.Hue = 0x0;
        base.OnRemoved( o );
     }
      public VampireDagger( Serial serial ) : base( serial )
      {
     }
      public override void Serialize( GenericWriter writer )
      {
         base.Serialize( writer );
         writer.Write( (int) 0 ); // version
      }
      public override void Deserialize( GenericReader reader )
      {
         base.Deserialize( reader );
         int version = reader.ReadInt();
      }
      private class InternalTimer : Timer
      {
         private Mobile p_Mobile;
         private Item ed_Item;

         public InternalTimer( Mobile from, Item item ) : base( TimeSpan.FromSeconds( 1.0 ), TimeSpan.FromSeconds( 1.0 ) )
         {
            p_Mobile = from;
            ed_Item = item;
         }
         protected override void OnTick()
         {
            if ( !p_Mobile.CheckAlive() )
            {
               p_Mobile.RemoveItem(ls);
               ed_Item.Hue = 0x0;
               Stop();
            }
            else if ( ed_Item.Parent != p_Mobile )
            {
               p_Mobile.RemoveItem(ls);
               ed_Item.Hue = 0x0;
               Stop();
            }
            else if( p_Mobile.FindItemOnLayer( Layer.FirstValid ) is VampireDagger )
            {
               IPooledEnumerable eable = ed_Item.Map.GetMobilesInRange(p_Mobile.Location, 15 ) ;
               foreach( Mobile m in eable )
               {
                if ( m is PlayerMobile && ((PlayerMobile)m).Vampirism == true)
                  {
                     if(!Lit)
                     {
                        p_Mobile.AddItem(ls);
                        Lit = true;
                     }
                     ls.Location = p_Mobile.Location;
                     ed_Item.Hue = 0x60;
                  }
                  else
                  {
                     if(Lit)
                     {
                        p_Mobile.RemoveItem(ls);
                        Lit = false;
                     }
                     ed_Item.Hue = 0x0;
                  }
               }
               eable.Free();
            }
         }
      }
   }
}
