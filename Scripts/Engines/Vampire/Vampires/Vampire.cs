using System;
using System.Collections;
using Server;
using Server.Items;

namespace Server.Mobiles
{
	[CorpseName( "corps d'un vampire" )]
	public class Vampire : BaseCreature
	{
		[Constructable]
		public Vampire () : base( AIType.AI_Mage, FightMode.Weakest, 10, 1, 0.2, 0.4 )
		{
			SpeechHue = Utility.RandomDyedHue();
			Title = "vampire";

			if ( this.Female = Utility.RandomBool() )
			{
				Body = 184;
				Name = NameList.RandomName( "vampire" );
				AddItem( new PlainDress( 0x1 ) );
			}
			else
			{
				Body = 183;
				Name = NameList.RandomName( "vampire" );
				AddItem( new LongPants( 0x1 ) );
			}

			SetStr( 388, 520 );
			SetDex( 101, 150 );
			SetInt( 198, 257 );

			SetHits( 250, 300 );

			SetDamage( 20, 35 );

			SetDamageType( ResistanceType.Physical, 50 );
			SetDamageType( ResistanceType.Cold, 50 );

			SetResistance( ResistanceType.Physical, 70, 80 );
			SetResistance( ResistanceType.Fire, 60, 70 );
			SetResistance( ResistanceType.Cold, 40, 50 );
			SetResistance( ResistanceType.Poison, 100 );
			SetResistance( ResistanceType.Energy, 50, 60 );

			SetSkill( SkillName.EvalInt, 80.1, 90.0 );
			SetSkill( SkillName.Magery, 85.1, 95.0 );
			SetSkill( SkillName.Meditation, 90.1, 100.0 );
			SetSkill( SkillName.MagicResist, 150.5, 190 );
			SetSkill( SkillName.Tactics, 80.1, 90.0 );
			SetSkill( SkillName.Wrestling, 100 );

			Fame = 15000;
			Karma = -15000;

			VirtualArmor = 55;
			
			AddItem( new ThighBoots( 0x1 ) );
		
			LongHair hair = new LongHair();
			hair.Hue = 2406;
			hair.Layer = Layer.Hair;
			hair.Movable = false;

			AddItem( hair );

			PackGem(1, 3);
			PackGold( 600, 1000 );
			//PackItem ( new Token( 500, 900 ) );
			PackScroll( 1, 7 );
			PackScroll( 1, 7 );
			PackArmor( 1, 5, 0.70 );
			PackWeapon( 1, 5, 0.70 );
			PackMagicItems( 1, 5 );
		}
		
		public override bool AlwaysMurderer{ get{ return true; } }
		public override bool AutoDispel{ get{ return true; } }
		public override bool ShowFameTitle{ get{ return false; } }
		
		//public override OppositionGroup OppositionGroup
		//{
			//get{ return OppositionGroup.KnightsAndVampires; }
		//}

		public void DrainLife()
		{
			Map map = this.Map;

			if ( map == null )
				return;

			IPooledEnumerable eable = map.GetMobilesInRange( this.Location, 2 );
			ArrayList list = new ArrayList();

			foreach ( Mobile m in eable )
			{
				if ( m == this || !CanBeHarmful( m ) )
					continue;

				if ( m is BaseCreature && (((BaseCreature)m).Controled || ((BaseCreature)m).Summoned || ((BaseCreature)m).Team != this.Team) )
					list.Add( m );
				else if ( m.Player )
					list.Add( m );
			}

			eable.Free();

			foreach ( Mobile m in list )
			{
				DoHarmful( m );

				m.FixedParticles( 0x374A, 10, 15, 5013, 0x496, 0, EffectLayer.Waist );
				m.PlaySound( 0x213 );

				m.Say( "Your blood is my lifespring!" );

				int toDrain = Utility.RandomMinMax( 20, 40 );

				Hits += toDrain;
				m.Damage( toDrain, this );
			}
		}

		public override void OnGaveMeleeAttack( Mobile defender )
		{
			base.OnGaveMeleeAttack( defender );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public override void OnGotMeleeAttack( Mobile attacker )
		{
			base.OnGotMeleeAttack( attacker );

			if ( 0.1 >= Utility.RandomDouble() )
				DrainLife();
		}

		public Vampire( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
}
