using System; 
using Server.Network; 
using Server.Items; 
using Server.Gumps; 

namespace Server.Items 
{ 
	public class VampireOrb: CrepusculeItem 
	{ 
		[Constructable] 
		public VampireOrb() : base( 6249 ) 
		{ 
			Movable = true; 
			Name = "a glowing Vampire Orb";  
			Hue = 2118;
			LootType = LootType.Blessed;
		} 

		public VampireOrb( Serial serial ) : base( serial ) 
		{ 
		} 

		public override void Serialize( GenericWriter writer ) 
		{ 
			base.Serialize( writer ); 

			writer.Write( (int) 0 ); // version 
		} 

		public override void Deserialize( GenericReader reader ) 
		{ 
			base.Deserialize( reader ); 

			int version = reader.ReadInt(); 
		} 

		public override void OnDoubleClick( Mobile from ) 
		{ 
			
			if ( !from.InRange( GetWorldLocation(), 2 ) ) 
			{ 
				from.SendLocalizedMessage( 500446 ); // That is too far away. 
			} 
			else 
			{ 

			if ( from.Mounted )
			{
				from.SendLocalizedMessage( 1042146 ); // You can not use this while mounted. 
			}
			else
			{

			if ( from.Title != "The Vampire" )
			{
				from.FixedEffect( 0x3728, 10, 13 );
				Delete();	
			}
			else
			{
			if ( from.BodyMod == 317 )//test for Vampire Bat body
			{
				// Vampire body with default light skin hue...
				from.BodyMod = 0;
				from.HueMod = -1;
			}
			else
			{
				// Vampire Bat body with no hue...
				from.BodyMod = 317;
				from.HueMod = 1154;
			}
		} 
	} 
} 
}
}
}

