using System;
using Server;
using System.Collections;
using Server.Mobiles;
using Server.Gumps;
using Server.Targeting;
using Server.Items;
using Server.Prompts;
using Server.Targets;
using System.Reflection;

namespace Server.Scripts.Commands
{
	public class VampireCommandes
	{
		public static void Initialize()
		{
			Server.Commands.Register( "mordre", AccessLevel.Player, new CommandEventHandler( mordre_OnCommand ) );
			Server.Commands.Register( "boiresang", AccessLevel.Player, new CommandEventHandler( boiresang_OnCommand ) );
			Server.Commands.Register( "vrat", AccessLevel.Player, new CommandEventHandler( vrat_OnCommand ) );
			Server.Commands.Register( "vchat", AccessLevel.Player, new CommandEventHandler( vchat_OnCommand ) );
		}

	
		[Usage( "mordre" )] 
		[Description( "mordre pour transformer en vampire." )] 
		public static void mordre_OnCommand( CommandEventArgs e ) 
		{ 
			if ( ((PlayerMobile)e.Mobile).VampireType == VampType.Ancient )
				e.Mobile.Target = new TurnTarget();
			//break;
		}

		[Usage( "boiresang" )]
		[Description( "boire son sang." )]
		public static void boiresang_OnCommand( CommandEventArgs e )
		{
			if ( ((PlayerMobile)e.Mobile).Vampirism == true && ((PlayerMobile)e.Mobile).VampHunger == true && e.Mobile.Alive)
				e.Mobile.Target = new FeedTarget();
			
		}

		[Usage( "vrat" )]
		[Description( "se transformer en rat." )]
		public static void vrat_OnCommand( CommandEventArgs e )
		{
			Mobile m = e.Mobile;
			if ( ((PlayerMobile)e.Mobile).Vampirism == true && e.Mobile.Alive && ( ((PlayerMobile)e.Mobile).VampireType == VampType.Master || ((PlayerMobile)e.Mobile).VampireType == VampType.Ancient ) )
			{
						if ( e.Mobile.Body == 238 )//test for Vampire Bat body
						{
							// Vampire body with default light skin hue...
							if (e.Mobile.Female == true)
							{
								e.Mobile.Body = 401;
							}
							else
							{
								e.Mobile.Body = 400;
							}


							
							e.Mobile.HueMod = -1;
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z + 4 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z - 4 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z + 4 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z - 4 ), m.Map, 0x3728, 13 );

							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 11 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 7 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 3 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z - 1 ), m.Map, 0x3728, 13 );
							e.Mobile.NameMod = null;
							m.PlaySound( 0x228 );
						}
						else if ( e.Mobile.Body == 400 || e.Mobile.Body == 401 )
						{
							

							e.Mobile.NameMod = "rat";
							// Vampire Bat body with no hue...
							e.Mobile.Body = 238;
							
							//e.Mobile.HueMod = 0;
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z + 4 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z - 4 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z + 4 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z - 4 ), m.Map, 0x3728, 13 );

							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 11 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 7 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 3 ), m.Map, 0x3728, 13 );
							Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z - 1 ), m.Map, 0x3728, 13 );

							m.PlaySound( 0x228 );
						}
			}
		}

		[Usage( "vchat" )]
		[Description( "se transformer en chat." )]
		public static void vchat_OnCommand( CommandEventArgs e )
		{
			Mobile m = e.Mobile;
			if ( ((PlayerMobile)e.Mobile).Vampirism == true && e.Mobile.Alive && ((PlayerMobile)e.Mobile).VampireType == VampType.Ancient)
			{
				if ( e.Mobile.Body == 201 )//test for Vampire Bat body
				{
					// Vampire body with default light skin hue...
					if (e.Mobile.Female == true)
					{
						e.Mobile.Body = 401;
					}
					else
					{
						e.Mobile.Body = 400;
					}


					e.Mobile.NameMod = null;
					e.Mobile.HueMod = -1;

					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z + 4 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z - 4 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z + 4 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z - 4 ), m.Map, 0x3728, 13 );

					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 11 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 7 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 3 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z - 1 ), m.Map, 0x3728, 13 );

					m.PlaySound( 0x228 );

				}
				else  if ( e.Mobile.Body == 400 || e.Mobile.Body == 401 )
				{


					e.Mobile.NameMod = "chat";
					

					
					// Vampire Bat body with no hue...
					e.Mobile.Body = 201;
					
					e.Mobile.HueMod = 1104;

					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z + 4 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y, m.Z - 4 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z + 4 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X, m.Y + 1, m.Z - 4 ), m.Map, 0x3728, 13 );

					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 11 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 7 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z + 3 ), m.Map, 0x3728, 13 );
					Effects.SendLocationEffect( new Point3D( m.X + 1, m.Y + 1, m.Z - 1 ), m.Map, 0x3728, 13 );

					m.PlaySound( 0x228 );
				}
			}
		}



	}
}







