using System;
 using System.IO;
 using System.Collections;
 using Server;
 using Server.Misc;
 using Server.Items;
 using Server.Gumps;
 using Server.Network;
 using Server.Mobiles;
 using Server.Targeting;
 using Server.Targets;
 using System.Reflection;
 using Server.ContextMenus;
 using Server.Regions;
 using Server.Prompts;
 namespace Server.Targets
 {
 	public class FeedTarget : Target
 	{
 		public FeedTarget() : base( 12, false, TargetFlags.Harmful )
 		{
 		}
 		protected override void OnTarget( Mobile from, object targeted )
 		{
 			if ( targeted is Mobile )
 			{
 				Mobile targ = (Mobile)targeted;
 				
 				if (targ is PlayerMobile)
 				{
 					PlayerMobile playertarg = targ as PlayerMobile;
 					if((playertarg.Vampirism == true) && (playertarg.Blood >= 20))
 					{
 						from.SendMessage( 0x35, "La cible n'a pas assez de sang..." );
 						return;
 					}
 				}
 				if ( !targ.InRange( from, 1 ))
 				{
 					from.SendMessage( 0x35, "Vous devez vous approchez plus..." );
 					return;
 				}
 				if ( targ is PlayerMobile )
 				{
 					PlayerMobile playertarg = targ as PlayerMobile;
 					playertarg.Damage( Utility.Random( 30, 45 ) );
 					from.Hunger += 20 ;
 					from.Thirst += 20 ;
 					playertarg.Blood -= 20;
 					from.DoHarmful( targ );
 					playertarg.Sickness = SickFlag.Headache;
 					//targ.Hits -= 3; //tick away some health
 					targ.Stam -= 10; //tick away some stamina
 					targ.SendMessage( 0x35, "Vous vous mordez." );
 					targ.SendMessage( "Vouc commencez perdre le sang." );
					from.SendMessage( 0x35, "Vous goutez le sang!" );
 				}
 				else
 				{
 					targ.Damage( Utility.Random( 5, 10 ) );
 					from.Hunger += 10 ;
 					from.Thirst += 10 ;
 					from.SendMessage( 0x35, "Vous goutez le sang!" );
 				}
 			}
 			else
 			{
 				from.SendMessage( 0x35, "Vous ne pouvez pas vous nourrir sur cela." );
 			}
 		}
 	}
 	public class TurnTarget : Target
 	{
 		public TurnTarget() : base( 12, false, TargetFlags.Harmful )
 		{
 		}
 		protected override void OnTarget( Mobile from, object targeted )
 		{
 			if ( targeted is Mobile )
 			{
 				Mobile targ = (Mobile)targeted;
 				
 				if (targ.Blessed == true)
 					return;
 				
 				if ( targ is PlayerMobile )
 				{
 					PlayerMobile pmobile = targ as PlayerMobile;
 					if ((pmobile.Vampirism == true) && (pmobile.VampireType != VampType.None)) // && ((dovPlayerMobile)targ).Blood <= 30) || ((dovPlayerMobile)targ).Sickness = SickFlag.PermImmunity)
 					{
 						from.SendMessage( 0x35, "Vous ne pouvez pas vous nourrir sur les vampires!" );
 						return;
 					}
 					//else if (pmobile.Blood <= 30)
 					//{
 					//	from.SendMessage( 0x35, "That looks to strong to turn try feeding off of it!" );
 					//	return;
 					//}
 					if ( !targ.InRange( from, 1 ))
 					{
 					from.SendMessage( 0x35, "Vous devez vous rapprocher plus" );
 					return;
 					}
 					else
 					{
 						pmobile.Damage( Utility.Random( 15, 20 ) );
 						from.Hunger += 20 ;
 						from.Thirst += 20 ;
 						pmobile.Blood = 0;
 						from.DoHarmful( targ );
 						pmobile.Sickness = SickFlag.VampTurn;
 						pmobile.SendMessage( 0x35, "Vous venez d'�tre mordu par un vampire!" );
 						pmobile.SendMessage( "Vous vous sentez mal" );
 						from.SendMessage( 0x35, "Vous goutez le sang" );
 					}
 					
 				}
 				else if ( targ is BaseCreature )
 				{
 					targ.Damage( Utility.Random( 30, 40 ) );
 					from.Hunger += 10 ;
 					from.Thirst += 10 ;
 					from.SendMessage( 0x35, "Vous pouvez uniquement vous nourrir dessus." );
 					
 				}
 				
 			}
 			else
 			{
 				from.SendMessage( 0x35, "Vous ne pouvez pas vous nourrir." );
 			}
 		}
 	}
 	//****** End of vamp Addition ************************
 }
