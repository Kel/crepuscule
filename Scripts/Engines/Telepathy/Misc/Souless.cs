using System;
using System.Collections;
using Server.Items;
using Server.Targeting;
using Server.Misc;
using Server.Spells.Third;
using Server.Engines;

namespace Server.Mobiles
{
    [CorpseName("a corpse")]
    public class Souless : BaseCreature
    {
        private Mobile m_Owner;
        private int m_OldBody;
        [CommandProperty(AccessLevel.GameMaster)]
        public Mobile Owner
        {
            get { return m_Owner; }
            set { m_Owner = value; }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int OldBody
        {
            get { return m_OldBody; }
            set { m_OldBody = value; }
        }
        private MindSkill skill;
        [Constructable]
        public Souless(MindSkill m_skill)
            : base(AIType.AI_Melee, FightMode.Agressor, 10, 1, 0.2, 0.4)
        {

            Body = 777;

            CantWalk = true;
            skill = m_skill;

        }
        public override void OnDoubleClick(Mobile from)
        {
            if (m_Owner != null && m_Owner == from)
            {
                m_Owner.Map = this.Map;
                m_Owner.Location = this.Location;
                m_Owner.BodyValue = m_OldBody;
                m_Owner.Blessed = this.Blessed;
                m_Owner.Direction = this.Direction;
                this.Delete();
                m_Owner.SendMessage("Votre conscience revient � votre corps");

                (m_Owner as RacePlayerMobile).AstralTravel = false;

                if (!m_Owner.CanBeginAction(typeof(MindFollow)))
                    m_Owner.EndAction(typeof(MindFollow));
                if (!m_Owner.CanBeginAction(typeof(MindAstralTravel)))
                    m_Owner.EndAction(typeof(MindAstralTravel));
            }
        }
        public override bool OnBeforeDeath()
        {

            if (m_Owner != null)
                m_Owner.Map = this.Map;
            m_Owner.Location = this.Location;
            m_Owner.Blessed = this.Blessed;
            m_Owner.Direction = this.Direction;
            AFKKiller();
            m_Owner.Kill();
            if (m_Owner.Female) m_Owner.BodyValue = 403;
            else m_Owner.BodyValue = 402;
            Delete();
            return false;


        }
        public void AFKKiller()
        {
            ArrayList toGive = new ArrayList();

            ArrayList list = Aggressors;
            for (int i = 0; i < list.Count; ++i)
            {
                AggressorInfo info = (AggressorInfo)list[i];

                if (info.Attacker.Player && (DateTime.Now - info.LastCombatTime) < TimeSpan.FromSeconds(30.0) && !toGive.Contains(info.Attacker))
                    toGive.Add(info.Attacker);
            }

            list = Aggressed;
            for (int i = 0; i < list.Count; ++i)
            {
                AggressorInfo info = (AggressorInfo)list[i];

                if (info.Defender.Player && (DateTime.Now - info.LastCombatTime) < TimeSpan.FromSeconds(30.0) && !toGive.Contains(info.Defender))
                    toGive.Add(info.Defender);
            }

            if (toGive.Count == 0)
                return;



            for (int i = 0; i < toGive.Count; ++i)
            {


                Mobile m = (Mobile)toGive[i % toGive.Count];

                if (m != null)
                {
                    m.DoHarmful(m_Owner);
                }

            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public override bool ClickTitle { get { return false; } }
        public Souless(Serial serial)
            : base(serial)
        {
        }


        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0);
            writer.Write(m_Owner);
            writer.Write(m_OldBody);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            m_Owner = reader.ReadMobile();
            m_OldBody = reader.ReadInt();
        }
    }
}
