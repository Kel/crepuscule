﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Engines
{
    public class MindTalkAdd : MindSkill
    {
        public override int NeededAptitude { get { return 21; } }
        public override int NeededSkill { get { return 40; } }
        public override int NeededMana { get { return 50; } }

        public MindTalkAdd(RacePlayerMobile caster) : base(caster) { }

        public override void OnCast()
        {
            if (CanCast())
            {
                if (!TelepathyHelper.CanTelepath(Caster))
                {
                    TelepathyHelper.AlertMessage(Caster, 0);
                    return;
                }

                Caster.SendGump(new TelepathGump(Caster));

            }
        }


    }


    public class AddTelepathTarget : Target
    {
        private static int MaxFriends;
        public AddTelepathTarget(int nbr)
            : base(5, true, TargetFlags.None)
        {
            MaxFriends = nbr;
        }

        protected override void OnTarget(Mobile m, object targ)
        {

            PlayerMobile from = ((PlayerMobile)m);

            if (!(targ is PlayerMobile))
            {
                AlertMessage(from, 3);
                return;
            }

            PlayerMobile target = ((PlayerMobile)targ);

            if (IsFriend(from, target))
            {
                AlertMessage(from, 0, target);
                return;
            }

            if (ItsMe(from, target))
            {
                AlertMessage(from, 1);
                return;
            }

            if (!CanHaveFriends(from))
            {
                AlertMessage(from, 0);
                return;
            }

            from.TelepathieVictim.Add(target);
            AlertMessage(target, 2);
            AlertMessage(from, 1, target);
        }

        private static bool CanHaveFriends(PlayerMobile from)
        {
            if (((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value <= 0)
                return true;

            return (from.TelepathieVictim.Count < ((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value);
        }

        private static bool ItsMe(Mobile from, Mobile m)
        {
            return (from == m);
        }

        private static bool IsFriend(PlayerMobile from, Mobile target)
        {
            for (int i = 0; i < from.TelepathieVictim.Count; ++i)
            {
                if (from.TelepathieVictim[i] == target)
                    return true;
            }

            return false;
        }

        private static void AlertMessage(Mobile from, int Message)
        {
            string texte = null;

            switch (Message)
            {
                case 0: texte = "Vos dons de télépathie ont aussi des limites !"; break;
                case 1: texte = "Vous ne pouvez pas prendre contact avec vous meme"; break;
                case 2: texte = "Quelqu'un prend contact avec vous"; break;
                case 3: texte = "Vous devez pointer un joueur !"; break;
            }

            if (from != null && texte != null)
                from.SendMessage(texte);
        }

        private static void AlertMessage(Mobile from, int Message, Mobile m)
        {
            string texte = null;

            switch (Message)
            {
                case 0: texte = "Vous êtes deja en contact avec {0}"; break;
                case 1: texte = "Vous prenez contact avec {0}"; break;
            }

            if (from != null && texte != null && m != null)
                from.SendMessage(texte, m.Name);
        }

    }
}
