﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Network;
using Server.Mobiles;
using Server.Targeting;
using System.Collections;

namespace Server.Engines
{
    public class MindChaos : MindSkill
    {
        public override int NeededAptitude { get { return 29; } }
        public override int NeededSkill { get { return 70; } }
        public override int NeededMana { get { return 30; } }

        public MindChaos(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
                Caster.Target = new InternalTarget(this);
        }


        public void Target(Mobile m)
        {
            if (!Caster.CanSee(m))
            {
                Caster.SendLocalizedMessage(500237); // Target can not be seen.
            }
            else if (!Caster.CanBeginAction(typeof(MindChaos)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else
            {
                if (Caster.CanBeHarmful(m) && CheckSequence())
                { 
                    //Get values
                    Mobile target = m;
                    if (target != null && Caster != null && target is RacePlayerMobile)
                    {
                        RacePlayerMobile rpmTarget = target as RacePlayerMobile;
                        if(!CheckIfCanBlock(Caster,rpmTarget))
                        {
                            Caster.BeginAction(typeof(MindChaos));
                            rpmTarget.Hallucinating = true;

                            rpmTarget.Say("*devient tout blanc, visiblement souffrant*");
                            rpmTarget.SendMessage("Vous vous sentez très mal tout d'un coup");
                            rpmTarget.Hits -= 1;

                            HallucinationTimer hallucinationTimer = new HallucinationTimer(target, Caster.Capacities[CapacityName.Telepathy].Value - rpmTarget.Capacities[CapacityName.Telepathy].Value);
                            hallucinationTimer.Start();

                            Timer.DelayCall(TimeSpan.FromSeconds(180.0), new TimerStateCallback(EndAction), Caster);
                        }
                    }
                }
            }
        }

        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindChaos));
        }


        #region InternalTarget
        private class InternalTarget : Target
        {
            private MindChaos m_Owner;

            public InternalTarget(MindChaos owner)
                : base(12, false, TargetFlags.None)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is Mobile)
                    m_Owner.Target((Mobile)o);
            }

            protected override void OnTargetFinish(Mobile from)
            {
                
            }
        }
        #endregion

        #region Hallucination part


        public static void SendHallucinationItem(IEntity e, int itemID, int speed, int duration, int renderMode, int hue)
        {
            Map map = e.Map;

            if (map != null)
            {
                Packet regular = null;

                IPooledEnumerable eable = map.GetClientsInRange(e.Location);

                foreach (NetState state in eable)
                {
                    RacePlayerMobile pm = state.Mobile as RacePlayerMobile;
                    state.Mobile.ProcessDelta();

                    if (state.Mobile is PlayerMobile)
                    {
                        if (pm.Hallucinating)
                        {
                         
                            if (regular == null)
                                regular = new LocationEffect(e, itemID, speed, duration, renderMode, hue);

                            state.Send(regular);
                        }
                    }
                }

                eable.Free();
            }
        }

        internal class HallucinationTimer : Timer
        {
            private Mobile m;
            private int Duration = 3;
            //Timespan between visual changes
            public HallucinationTimer(Mobile from, int duration)
                : base(TimeSpan.FromSeconds(2))
            {
                Priority = TimerPriority.OneSecond;
                Duration = duration;
                m = from;
            }

            protected override void OnTick()
            {
                HallucinationTimer hallucinationTimer = new HallucinationTimer(m, Duration);
                SoberTimer soberTimer = new SoberTimer(m, Duration);
                RacePlayerMobile pm = m as RacePlayerMobile;
                //int id = Utility.Random( 1, 1026 );
                int hhue = Utility.Random(2, 1200);

                if (pm.Hallucinating)
                {
                    ArrayList targets = new ArrayList();

                    IPooledEnumerable eable = pm.GetItemsInRange(50);

                    foreach (Item t in eable)
                    {
                        int id = t.ItemID;
                        if (t.Visible)
                        {
                            targets.Add(t);
                            SendHallucinationItem(t, id, 5, 5000, 4410, hhue);
                        }
                    }
                    eable.Free();

                    hallucinationTimer.Start();
                    soberTimer.Start();
                }
                else
                {
                    Stop();
                }
            }
        }


        internal class SoberTimer : Timer
        {
            private Mobile m;
            private int Duration;
            public SoberTimer(Mobile from, int duration)
                : base(TimeSpan.FromMinutes(duration))
            {
                Duration = duration;
                Priority = TimerPriority.FiveSeconds;
                m = from;
            }

            protected override void OnTick()
            {
                SoberTimer soberTimer = new SoberTimer(m, Duration);
                RacePlayerMobile pm = m as RacePlayerMobile;

                if (pm.Hallucinating)
                {
                    pm.Hallucinating = false;
                    pm.SendMessage("Vous reprenez conscience.");
                    Stop();
                }
            }
        }
        #endregion
    }
}
