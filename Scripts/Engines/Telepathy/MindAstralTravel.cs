﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;
using System.Collections;
using Server.Items;
using Server.Spells.Fifth;

namespace Server.Engines
{
    public class MindAstralTravel : MindSkill
    {
        public override int NeededAptitude { get { return 35; } }
        public override int NeededSkill { get { return 80; } }
        public override int NeededMana { get { return 70; } }

        private int m_NewBody;
        private int m_OldBody;
        private Souless m_Fake;
        public ArrayList m_PeerMod;


        public MindAstralTravel(RacePlayerMobile caster) : base(caster) { }



        public override void OnCast()
        {
            if (CanCast())
            {
                string Msg = "Vous n'arrivez pas à vous concentrer.";
                if (Caster.Mounted)
                {
                    Caster.SendLocalizedMessage(1042561); //Please dismount first.
                }
                else if (!Caster.CanBeginAction(typeof(MindAstralTravel)))
                {
                    Caster.SendLocalizedMessage(1005559); // This spell is already in effect.
                }
                else if (Server.Spells.Necromancy.TransformationSpell.UnderTransformation(Caster))
                {
                    Caster.SendMessage(Msg);
                }
                else if (DisguiseGump.IsDisguised(Caster))
                {
                    Caster.SendMessage(Msg);
                }
                else if (Caster.BodyMod == 183 || Caster.BodyMod == 184)
                {
                    Caster.SendMessage(Msg);
                }
                else if (!Caster.CanBeginAction(typeof(IncognitoSpell)) || Caster.IsBodyMod)
                {
                    DoFizzle();
                }
                else if (CheckSequence())
                {
                    if (Caster.BeginAction(typeof(MindAstralTravel)))
                    {
                        if (Caster.Female)
                        {
                            m_NewBody = 403;
                        }
                        else
                        {
                            m_NewBody = 402;
                        }

                        m_OldBody = Caster.Body;

                        if (m_NewBody != 0)
                        {
                            Caster.PlaySound(0x2DF);
                            Caster.SendMessage("Vous vous plongez dans un voyage astral.");

                            Souless dg = new Souless(this);

                            dg.Body = Caster.Body;

                            dg.Hue = Caster.Hue;
                            dg.Name = Caster.Name;
                            dg.SpeechHue = Caster.SpeechHue;
                            dg.Fame = Caster.Fame;
                            dg.Karma = Caster.Karma;
                            dg.EmoteHue = Caster.EmoteHue;
                            dg.Title = Caster.Title;
                            dg.Criminal = (Caster.Criminal);
                            dg.AccessLevel = Caster.AccessLevel;
                            dg.Str = Caster.Str;
                            dg.Int = Caster.Int;
                            dg.Hits = Caster.Hits;
                            dg.Dex = Caster.Dex;
                            dg.Mana = Caster.Mana;
                            dg.Stam = Caster.Stam;

                            dg.VirtualArmor = (Caster.VirtualArmor);
                            dg.SetSkill(SkillName.Wrestling, Caster.Skills[SkillName.Wrestling].Value);
                            dg.SetSkill(SkillName.Tactics, Caster.Skills[SkillName.Tactics].Value);
                            dg.SetSkill(SkillName.Anatomy, Caster.Skills[SkillName.Anatomy].Value);

                            dg.SetSkill(SkillName.Magery, Caster.Skills[SkillName.Magery].Value);
                            dg.SetSkill(SkillName.MagicResist, Caster.Skills[SkillName.MagicResist].Value);
                            dg.SetSkill(SkillName.Meditation, Caster.Skills[SkillName.Meditation].Value);
                            dg.SetSkill(SkillName.EvalInt, Caster.Skills[SkillName.EvalInt].Value);

                            dg.SetSkill(SkillName.Archery, Caster.Skills[SkillName.Archery].Value);
                            dg.SetSkill(SkillName.Macing, Caster.Skills[SkillName.Macing].Value);
                            dg.SetSkill(SkillName.Swords, Caster.Skills[SkillName.Swords].Value);
                            dg.SetSkill(SkillName.Fencing, Caster.Skills[SkillName.Fencing].Value);
                            dg.SetSkill(SkillName.Lumberjacking, Caster.Skills[SkillName.Lumberjacking].Value);
                            dg.SetSkill(SkillName.Alchemy, Caster.Skills[SkillName.Alchemy].Value);
                            dg.SetSkill(SkillName.Parry, Caster.Skills[SkillName.Parry].Value);
                            dg.SetSkill(SkillName.Focus, Caster.Skills[SkillName.Focus].Value);
                            dg.SetSkill(SkillName.Necromancy, Caster.Skills[SkillName.Necromancy].Value);
                            dg.SetSkill(SkillName.Chivalry, Caster.Skills[SkillName.Chivalry].Value);
                            dg.SetSkill(SkillName.ArmsLore, Caster.Skills[SkillName.ArmsLore].Value);
                            dg.SetSkill(SkillName.Poisoning, Caster.Skills[SkillName.Poisoning].Value);
                            dg.SetSkill(SkillName.SpiritSpeak, Caster.Skills[SkillName.SpiritSpeak].Value);
                            dg.SetSkill(SkillName.Stealing, Caster.Skills[SkillName.Stealing].Value);
                            dg.SetSkill(SkillName.Inscribe, Caster.Skills[SkillName.Inscribe].Value);
                            dg.Kills = (Caster.Kills);

                            //m_PeerMod = new ArrayList();
                            //double loss = (0 - Caster.Skills[SkillName.AnimalTaming].Base);
                            //SkillMod sk = new DefaultSkillMod(SkillName.AnimalTaming, true, loss);
                            //Caster.AddSkillMod(sk);
                            //m_PeerMod.Add(sk);
                            //double loss1 = (0 - Caster.Skills[SkillName.AnimalLore].Base);
                            //SkillMod sk1 = new DefaultSkillMod(SkillName.AnimalLore, true, loss1);
                            //Caster.AddSkillMod(sk1);
                            //m_PeerMod.Add(sk1);

                            //double loss3 = (0 - Caster.Skills[SkillName.Necromancy].Base);
                            //SkillMod sk3 = new DefaultSkillMod(SkillName.Necromancy, true, loss3);
                            //Caster.AddSkillMod(sk3);
                            //m_PeerMod.Add(sk3);
                            //double loss4 = (0 - Caster.Skills[SkillName.TasteID].Base);
                            //SkillMod sk4 = new DefaultSkillMod(SkillName.TasteID, true, loss4);
                            //Caster.AddSkillMod(sk4);
                            //m_PeerMod.Add(sk4);
                            // Clear Items 
                            RemoveFromAllLayers(dg);

                            // Then copy 
                            CopyFromLayer(Caster, dg, Layer.FirstValid);
                            CopyFromLayer(Caster, dg, Layer.TwoHanded);
                            CopyFromLayer(Caster, dg, Layer.Shoes);
                            CopyFromLayer(Caster, dg, Layer.Pants);
                            CopyFromLayer(Caster, dg, Layer.Shirt);
                            CopyFromLayer(Caster, dg, Layer.Helm);
                            CopyFromLayer(Caster, dg, Layer.Gloves);
                            CopyFromLayer(Caster, dg, Layer.Ring);
                            CopyFromLayer(Caster, dg, Layer.Talisman);
                            CopyFromLayer(Caster, dg, Layer.Neck);
                            CopyFromLayer(Caster, dg, Layer.Hair);
                            CopyFromLayer(Caster, dg, Layer.Waist);
                            CopyFromLayer(Caster, dg, Layer.InnerTorso);
                            CopyFromLayer(Caster, dg, Layer.Bracelet);
                            CopyFromLayer(Caster, dg, Layer.Unused_xF);
                            CopyFromLayer(Caster, dg, Layer.FacialHair);
                            CopyFromLayer(Caster, dg, Layer.MiddleTorso);
                            CopyFromLayer(Caster, dg, Layer.Earrings);
                            CopyFromLayer(Caster, dg, Layer.Arms);
                            CopyFromLayer(Caster, dg, Layer.Cloak);
                            CopyFromLayer(Caster, dg, Layer.OuterTorso);
                            CopyFromLayer(Caster, dg, Layer.OuterLegs);
                            CopyFromLayer(Caster, dg, Layer.LastUserValid);
                            CopyFromLayer(Caster, dg, Layer.Mount);
                            dg.Owner = Caster;
                            dg.OldBody = m_OldBody;
                            m_Fake = dg;
                            dg.Map = Caster.Map;
                            dg.Location = Caster.Location;
                            dg.Frozen = true;
                            BaseArmor.ValidateMobile(Caster);
                            Caster.BodyValue = m_NewBody;
                            Caster.Blessed = true;
                            StopTimer(Caster);
                            Caster.AstralTravel = true;
                            Timer t = new InternalTimer(Caster, m_OldBody, m_Fake, this);

                            m_Timers[Caster] = t;

                            t.Start();

                            //Caster.BeginAction(typeof(MindAstralTravel)); 
                            //Timer.DelayCall(TimeSpan.FromSeconds(360.0), new TimerStateCallback(EndAction), Caster);
                        }
                    }
                    else
                    {
                        Caster.SendLocalizedMessage(1005559); // This spell is already in effect.
                    }
                }


            }
        }


        private void CopyFromLayer(Mobile from, Mobile mimic, Layer layer)
        {
            if (from.FindItemOnLayer(layer) != null)
                mimic.AddItem(Mobile.LiftItemDupe(from.FindItemOnLayer(layer), 1));

            if (mimic.FindItemOnLayer(layer) != null && mimic.FindItemOnLayer(layer).LootType != LootType.Blessed)
                mimic.FindItemOnLayer(layer).LootType = LootType.Newbied;
        }

        private void DeleteFromLayer(Mobile from, Layer layer)
        {
            if (from.FindItemOnLayer(layer) != null)
                from.RemoveItem(from.FindItemOnLayer(layer));
        }
        private void RemoveFromAllLayers(Mobile from)
        {
            DeleteFromLayer(from, Layer.FirstValid);
            DeleteFromLayer(from, Layer.TwoHanded);
            DeleteFromLayer(from, Layer.Shoes);
            DeleteFromLayer(from, Layer.Pants);
            DeleteFromLayer(from, Layer.Shirt);
            DeleteFromLayer(from, Layer.Helm);
            DeleteFromLayer(from, Layer.Gloves);
            DeleteFromLayer(from, Layer.Ring);
            DeleteFromLayer(from, Layer.Talisman);
            DeleteFromLayer(from, Layer.Neck);
            DeleteFromLayer(from, Layer.Hair);
            DeleteFromLayer(from, Layer.Waist);
            DeleteFromLayer(from, Layer.InnerTorso);
            DeleteFromLayer(from, Layer.Bracelet);
            DeleteFromLayer(from, Layer.Unused_xF);
            DeleteFromLayer(from, Layer.FacialHair);
            DeleteFromLayer(from, Layer.MiddleTorso);
            DeleteFromLayer(from, Layer.Earrings);
            DeleteFromLayer(from, Layer.Arms);
            DeleteFromLayer(from, Layer.Cloak);
            DeleteFromLayer(from, Layer.OuterTorso);
            DeleteFromLayer(from, Layer.OuterLegs);
            DeleteFromLayer(from, Layer.LastUserValid);
            DeleteFromLayer(from, Layer.Mount);

        }
        //public void RemovePeerMod()
        //{
        //    if (m_PeerMod == null)
        //        return;

        //    for (int i = 0; i < m_PeerMod.Count; ++i)
        //        ((SkillMod)m_PeerMod[i]).Remove();
        //    m_PeerMod = null;
        //}
        private static Hashtable m_Timers = new Hashtable();

        public static bool StopTimer(Mobile m)
        {
            Timer t = (Timer)m_Timers[m];

            if (t != null)
            {
                if (t is InternalTimer) (t as InternalTimer).Recall();
                t.Stop();
                m_Timers.Remove(m);
            }

            return (t != null);
        }

        private class InternalTimer : Timer
        {
            private RacePlayerMobile m_Owner;
            private int m_OldBody;
            private Souless fake;
            private Point3D loc;
            private MindAstralTravel m_spell;
            public InternalTimer(Mobile owner, int body, Souless m_Fake, MindAstralTravel spell)
                : base(TimeSpan.FromSeconds(0))
            {
                m_Owner = owner as RacePlayerMobile;
                m_OldBody = body;
                fake = m_Fake;
                m_spell = spell;

                int val = 10 + ((int)owner.Skills[SkillName.SpiritSpeak].Value /2);

                if (val > 60)
                    val = 60;
                //double loss2 = (0 - m_Owner.Capacities[CapacityName.Telepathy].Value * 3);
                //SkillMod sk2 = new DefaultSkillMod(SkillName.SpiritSpeak, true, loss2);
                //m_Owner.AddSkillMod(sk2);
                //m_spell.m_PeerMod.Add(sk2);

                Delay = TimeSpan.FromSeconds(val);
                Priority = TimerPriority.TwoFiftyMS;
            }

            public void Recall()
            {
                if (!m_Owner.CanBeginAction(typeof(MindAstralTravel)))
                {
                    if (fake != null && !fake.Deleted)
                    {
                        loc = new Point3D(fake.X, fake.Y, fake.Z);
                        m_Owner.Location = loc;
                        m_Owner.Blessed = fake.Blessed;
                        fake.Delete();

                    }
                    m_Owner.BodyValue = m_OldBody;
                    //m_spell.RemovePeerMod();
                    m_Owner.EndAction(typeof(MindAstralTravel));
                    m_Owner.AstralTravel = false;
                    BaseArmor.ValidateMobile(m_Owner);
                }
            }

            protected override void OnTick()
            {
                StopTimer(m_Owner);
            }
        }

    }
}