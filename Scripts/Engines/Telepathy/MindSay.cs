﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Targeting;
using Server.Mobiles;

namespace Server.Engines
{
    public class MindSay : MindSkill
    {
        public override int NeededAptitude { get { return 21; } }
        public override int NeededSkill { get { return 45; } }
        public override int NeededMana { get { return 20; } }

        public MindSay(RacePlayerMobile caster) : base(caster) { }

        public void OnCast(string Text)
        {
            if (CanCast() && !String.IsNullOrEmpty(Text))
            {
                if (!TelepathyHelper.CanTelepath(Caster))
                {
                    TelepathyHelper.AlertMessage(Caster, 0);
                    return;
                }

                if (!TelepathyHelper.HaveFriends(Caster))
                {
                    TelepathyHelper.AlertMessage(Caster, 1);
                    return;
                }

                for (int i = 0; i < Caster.TelepathieVictim.Count; ++i)
                {
                    if (Caster.TelepathieVictim[i] != null && Caster.TelepathieVictim[i] is Mobile)
                        TelepathyHelper.SaySomething((Mobile)Caster.TelepathieVictim[i], Text);
                }

                TelepathyHelper.SaySomething(Caster, Text);

            }
        }

    }


}
