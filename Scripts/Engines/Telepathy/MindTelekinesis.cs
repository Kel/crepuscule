﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Regions;
using Server.Mobiles;
using Server.Items;
using Server.Spells;

namespace Server.Engines
{
    public class MindTelekenesis : MindSkill
    {
        public override int NeededAptitude { get { return 9; } }
        public override int NeededSkill { get { return 20; } }
        public override int NeededMana { get { return 5; } }

        public MindTelekenesis(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
                Caster.Target = new InternalTarget(this);
        }

        public void Target(ITelekinesisable obj)
        {
            if (CheckSequence())
            {
                SpellHelper.Turn(Caster, obj);

                obj.OnTelekinesis(Caster);
            }

        }

        public void Target(Container item)
        {
            if (!Caster.CanBeginAction(typeof(MindTelekenesis)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else if (CheckSequence())
            {
                TelepathyHelper.Turn(Caster, item);

                object root = item.RootParent;

                if (!item.IsAccessibleTo(Caster))
                {
                    item.OnDoubleClickNotAccessible(Caster);
                }
                else if (!item.CheckItemUse(Caster, item))
                {
                }
                else if (root != null && root is Mobile && root != Caster)
                {
                    item.OnSnoop(Caster);
                }
                else if (Caster.Region.OnDoubleClick(Caster, item))
                {
                    Caster.BeginAction(typeof(MindTelekenesis)); 
                    Effects.SendLocationParticles(EffectItem.Create(item.Location, item.Map, EffectItem.DefaultDuration), 0x376A, 9, 32, 5022);
                    Effects.PlaySound(item.Location, item.Map, 0x1F5);

                    item.DisplayTo(Caster);
                    item.OnItemUsed(Caster, item);

                    Timer.DelayCall(TimeSpan.FromSeconds(15.0), new TimerStateCallback(EndAction), Caster);
                }
            }

        }


        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindTelekenesis));
        }

        public class InternalTarget : Target
        {
            private MindTelekenesis m_Owner;

            public InternalTarget(MindTelekenesis owner)
                : base(12, false, TargetFlags.None)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is ITelekinesisable)
                    m_Owner.Target((ITelekinesisable)o);
                else if (o is Container)
                    m_Owner.Target((Container)o);
                else
                    from.SendLocalizedMessage(501857); // This spell won't work on that!
            }

            protected override void OnTargetFinish(Mobile from)
            {
                
            }
        }
    }
}