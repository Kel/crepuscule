﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;

namespace Server.Engines
{
    public class MindAttack : MindSkill
    {
        public override int NeededAptitude { get { return 22; } }
        public override int NeededSkill { get { return 55; } }
        public override int NeededMana { get { return 10; } }

        public MindAttack(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
                Caster.Target = new InternalTarget(this);
        }

        public void Target(Mobile m)
        {
            if (!Caster.CanSee(m))
            {
                Caster.SendLocalizedMessage(500237); // Target can not be seen.
            }
            else if (!Caster.CanBeginAction(typeof(MindAttack)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else
            {
                if (Caster.CanBeHarmful(m) && CheckSequence())
                { 
                    //Get values
                    Mobile target = m;

                    TelepathyHelper.Turn(Caster, target);
                    int damage = (int)(((Caster.Capacities[CapacityName.Telepathy].Value * 3) + Caster.Int) / 4);

                    if (damage > 75)
                        damage = 75;

                    if (target != null && Caster != null && Caster.HarmfulCheck(target))
                    {
                        Caster.BeginAction(typeof(MindAttack)); 
                        
                        if (target is RacePlayerMobile)
                        { //PVP
                            if (!CheckIfCanBlock(Caster, target))
                            {
                                TelepathyHelper.Damage(Caster, target, Utility.RandomMinMax(damage, damage + 5), 80);
                                target.FixedParticles(0x374A, 10, 15, 5038, 1181, 2, EffectLayer.Head);
                                target.PlaySound(0x213);
                            }
                        }
                        else
                        { //PVM
                            TelepathyHelper.Damage(Caster, target, Utility.RandomMinMax(damage - 5, damage), 100);
                            target.FixedParticles(0x374A, 10, 15, 5038, 1181, 2, EffectLayer.Head);
                            target.PlaySound(0x213);
                        }

                        
                        Timer.DelayCall(TimeSpan.FromSeconds(30.0), new TimerStateCallback(EndAction), Caster);
                    }
                }
            }
        }

        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindAttack));
        }


        #region InternalTarget
        private class InternalTarget : Target
        {
            private MindAttack m_Owner;

            public InternalTarget(MindAttack owner)
                : base(12, false, TargetFlags.Harmful)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is Mobile)
                    m_Owner.Target((Mobile)o);
            }

            protected override void OnTargetFinish(Mobile from)
            {
                
            }
        }
        #endregion
    }
}