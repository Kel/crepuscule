﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalShield : MindSkillCrystal
    {
        [Constructable]
        public CrystalShield() : base(0x19) { Name = "Cristal de barrière mentale"; }
        public CrystalShield(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);

            from.EvolutionInfo.m_MindShieldOff = !from.EvolutionInfo.m_MindShieldOff;
            if (!from.EvolutionInfo.m_MindShieldOff)
                from.SendMessage(60, "Vous vous concentrez, réactivant votre barrière mental");
            else
                from.SendMessage(40, "Vous vous relaxez, désactivant votre barrière mental");

        
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}