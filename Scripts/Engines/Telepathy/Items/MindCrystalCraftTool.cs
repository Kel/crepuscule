﻿using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
    public class MindCrystalCraftTool : BaseTool
    {
        [Constructable]
        public MindCrystalCraftTool() : this(50) { }
        [Constructable]
        public MindCrystalCraftTool(int uses) : base(uses, 0x1ED0) { Weight = 3.0; Name = "Outils de cristallisation"; }
        public MindCrystalCraftTool(Serial serial) : base(serial) { }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}