﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalRecall : MindSkillCrystal
    {
        [Constructable]
        public CrystalRecall() : base(0x186B) { Name = "Cristal de retour d'esprit"; }
        public CrystalRecall(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);
            
            MindAstralTravel.StopTimer(from);
            MindFollow.StopTimer(from);
        
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}