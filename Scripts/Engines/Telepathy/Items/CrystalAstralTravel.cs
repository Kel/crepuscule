﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalAstralTravel : MindSkillCrystal
    {
        [Constructable]
        public CrystalAstralTravel() : base(0x1869) { Name = "Cristal de voyage astral"; }
        public CrystalAstralTravel(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);
            MindAstralTravel skill = new MindAstralTravel(from);
            skill.OnCast();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}