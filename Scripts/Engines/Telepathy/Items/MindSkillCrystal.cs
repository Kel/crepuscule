﻿using System;
using Server;
using Server.Mobiles;

namespace Server.Items
{
    public class MindSkillCrystal : CrepusculeItem
    {

        public MindSkillCrystal(int itemID) : base(itemID) { }
        public MindSkillCrystal(Serial serial) : base(serial) { }

        public override void OnDoubleClick(Mobile from)
        {
            base.OnDoubleClick(from);
            if (!IsChildOf(from.Backpack) || !(from is RacePlayerMobile))
            {
                from.SendLocalizedMessage(1042001); // That must be in your pack for you to use it.
                return;
            }
            ExecuteSkill(from as RacePlayerMobile);
        }

        public virtual void ExecuteSkill(RacePlayerMobile from)
        {

        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}