﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalTalkAdd : MindSkillCrystal
    {
        [Constructable]
        public CrystalTalkAdd() : base(0x186C) { Name = "Cristal de prise de contact"; this.Hue = 1200; }
        public CrystalTalkAdd(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);

            MindTalkAdd skill = new MindTalkAdd(from);
            skill.OnCast();
        
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}