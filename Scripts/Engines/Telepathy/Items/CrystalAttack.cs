﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalAttack : MindSkillCrystal
    {
        [Constructable]
        public CrystalAttack() : base(0x186E) { Name = "Cristal d'attaque mentale"; }
        public CrystalAttack(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);
            MindAttack skill = new MindAttack(from);
            skill.OnCast();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}