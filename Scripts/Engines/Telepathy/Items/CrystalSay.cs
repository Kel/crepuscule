﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalSay : MindSkillCrystal
    {
        [Constructable]
        public CrystalSay() : base(0x186C) { Name = "Cristal de parole"; this.Hue = 89; }
        public CrystalSay(Serial serial) : base(serial) { }

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);

            MindSay skill = new MindSay(from);
            skill.OnCast();

        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}