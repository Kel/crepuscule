﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalImpression : MindSkillCrystal
    {
        [Constructable]
        public CrystalImpression() : base(0x186F) { Name = "Cristal d'impressions"; }
        public CrystalImpression(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);
            MindImpression skill = new MindImpression(from);
            skill.OnCast();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}