﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalSleep : MindSkillCrystal
    {
        [Constructable]
        public CrystalSleep() : base(0x186C) { Name = "Cristal de sommeil"; }
        public CrystalSleep(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);

            MindSleep skill = new MindSleep(from);
            skill.OnCast();
        
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}