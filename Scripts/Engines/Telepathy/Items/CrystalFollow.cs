﻿using System;
using Server;
using Server.Engines;
using Server.Mobiles;

namespace Server.Items
{
    public class CrystalFollow : MindSkillCrystal
    {
        [Constructable]
        public CrystalFollow() : base(0x186A) { Name = "Cristal de vision mentale"; }
        public CrystalFollow(Serial serial) : base(serial){}

        public override void ExecuteSkill(RacePlayerMobile from)
        {
            base.ExecuteSkill(from);
            MindFollow skill = new MindFollow(from);
            skill.OnCast();
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}