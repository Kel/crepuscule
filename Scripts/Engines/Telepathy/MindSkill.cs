﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;

namespace Server.Engines
{
    public class MindSkill
    {
        protected RacePlayerMobile Caster = null;

        public virtual int NeededAptitude{get{return 20;}}
        public virtual int NeededSkill { get { return 20; } }
        public virtual int NeededMana { get { return 0; } }

        public MindSkill(RacePlayerMobile caster)
        {
            Caster = caster;
        }

        public virtual void OnCast()
        {

        }

        public virtual bool CanCast()
        {
            if (Caster != null)
            {
                if (Caster.Capacities[CapacityName.Telepathy].Value < NeededAptitude || 
                    Caster.Skills[SkillName.SpiritSpeak].Value < NeededSkill)
                {
                    Caster.SendMessage("Vous avez très mal à la tête, vous n'arrivez pas à utiliser la capacité.");
                    Caster.SendMessage("Vous avez besoin de minimum " + NeededAptitude + " en télépathie et " + NeededSkill + " en spiritisme.");
                    return false;
                }
            }
            return true;
        }

        public virtual bool CheckSequence()
        {
            if (Caster.Deleted || !Caster.Alive)
            {
                DoFizzle();
            }
            else if (Caster.Mana < NeededMana)
            {
                Caster.LocalOverheadMessage(MessageType.Regular, 0x22, 502625); // Insufficient mana for this spell.
            }
            else if (Caster.Frozen || Caster.Paralyzed || Caster.BodyValue == 402 || Caster.BodyValue == 403)
            {
                Caster.SendLocalizedMessage(502646); // You cannot cast a spell while frozen.
                DoFizzle();
            }
            else// if (CheckFizzle())
            {
                Caster.Mana -= NeededMana;
                Caster.ClearHands();
                return true;
            }
            
            return false;
        }

        public virtual bool CheckIfCanBlock(RacePlayerMobile caster, Mobile target)
        {
            return TelepathyHelper.CheckIfCanBlock(caster, target);
        }


        public virtual void DoFizzle()
        {
            Caster.LocalOverheadMessage(MessageType.Regular, 0x3B2, 502632); // The spell fizzles.

            if (Caster.Player)
            {
                Caster.PlaySound(0x5C);
            }
        }

    }
}