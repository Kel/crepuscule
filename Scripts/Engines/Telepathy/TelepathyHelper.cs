﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Network;

namespace Server.Engines
{
    public class TelepathyHelper
    {
        public static void Turn(Mobile from, object to)
        {
            IPoint3D target = to as IPoint3D;

            if (target == null)
                return;

            if (target is Item)
            {
                Item item = (Item)target;

                if (item.RootParent != from)
                    from.Direction = from.GetDirectionTo(item.GetWorldLocation());
            }
            else if (from != target)
            {
                from.Direction = from.GetDirectionTo(target);
            }
        }

        public static bool CheckIfCanBlock(RacePlayerMobile caster, Mobile target)
        {
            if (target.AccessLevel > caster.AccessLevel)
            {
                caster.SendMessage("Impossible d'effectuer cette action sur quelqu'un ayant un niveau d'accès supérieur au vôtre.");
                return true;
            }

            RacePlayerMobile rpmTarget = target as RacePlayerMobile;

            int pcTel = 0;
            int pcResist = 0;
            int pcFinal = 0;

            if (target.Player && target.Alive)
            {
                RacePlayerMobile targ = (RacePlayerMobile)target;
                if (targ.EvolutionInfo.m_MindShieldOff)
                {
                    caster.SendMessage("Vous penétrez aisément sa barrière mentale.");
                    return false;
                }

                if (caster.Capacities[CapacityName.Telepathy].Value > 50)
                    pcTel = 50;
                else
                    pcTel = caster.Capacities[CapacityName.Telepathy].Value;

                pcTel += (int)(caster.Skills[SkillName.Meditation].Value / 4);
                pcTel += (int)(caster.Skills[SkillName.EvalInt].Value / 4);

                if (targ.Capacities[CapacityName.BetterEndurance].Value > 50)
                    pcResist = 50;
                else
                    pcResist = targ.Capacities[CapacityName.BetterEndurance].Value;

                pcResist += (int)(targ.Skills[SkillName.MagicResist].Value / 4);
                pcResist += (int)(targ.Skills[SkillName.Focus].Value / 4);

                int intDiff = (int)((caster.Int - targ.Int) / 3);
                pcFinal = pcTel - pcResist;
                pcFinal += intDiff > 20 ? 20 : intDiff;

                int telepathieDiff = (int)((caster.Capacities[CapacityName.Telepathy].Value - targ.Capacities[CapacityName.Telepathy].Value) / 5);
                pcFinal += telepathieDiff > 10 ? 10 : telepathieDiff;
            }

            // Succès sans être aperçu
            int random = Utility.Random(0, 100);

            if (pcFinal >= (random - 10))
            {
                caster.SendMessage("Vous penétrez aisément sa barrière mentale.");
                return false;
            }
            // Succès en étant aperçu anonymement
            else if ( pcFinal > (random - 15))
            {
                target.SendMessage("Vous sentez que quelqu'un a penétré votre barrière mentale");
                caster.SendMessage("Vous penétrez difficilement sa barrière mentale.");
                return false;
            }
            // Succès en étant aperçu
            else if (pcFinal > (random - 20))
            {
                target.SendMessage("Vous sentez que "+caster.GetKnownName(rpmTarget)+" a penétré votre barrière mentale");
                caster.SendMessage("Vous penétrez péniblement sa barrière mentale.");
                return false;
            }
            // Raté en étant aperçu
            else
            {
                target.SendMessage("Vous sentez que "+caster.GetKnownName(rpmTarget)+" a tenté de penetrer votre barrière mentale");
                caster.SendMessage("Vous n'arrivez pas à penétrer sa barrière mentale.");
                return true;
            }
        }
        public static void Damage(RacePlayerMobile caster, Mobile target, int damage, int energy)
        {
            //Damage calculation
            int intReduction = target.Int / 6;
            energy -= intReduction;
            if (energy <= 0) energy = 0;
            if (target is RacePlayerMobile)
            {
                int tel = (target as RacePlayerMobile).Capacities[CapacityName.Telepathy].Value;
                int telReduction = tel / 2;
                damage -= telReduction;
                if (damage < 5) damage = 5;
            }

            AOS.Damage(target, caster, (int)damage, 0, 0, 0, 0, energy);

        }

        #region MindTalk
        public static bool AptitudeNeeded = true;
        public static int MaxFriends = 5;

        public static bool CanTelepath(PlayerMobile from)
        {
	    if (!from.Telepathy) from.Telepathy = true;
            //if (from.Telepathy)
                return true;


            //if (!AptitudeNeeded)
            //    return true;

            //return false;
        }

        public static bool HaveFriends(PlayerMobile from)
        {
            return (from.TelepathieVictim.Count > 0);
        }

        public static bool CanHaveFriends(PlayerMobile from)
        {
            if (((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value <= 0)
                return true;

            return (from.TelepathieVictim.Count < ((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value);
        }

        public static void AlertMessage(Mobile from, int Message)
        {
            string texte = null;

            switch (Message)
            {
                case 0: texte = "Vos compétences en télépathie ne vous le permettent pas"; break;
                case 1: texte = "Vous n'avez pris contact avec personne"; break;
                case 2: texte = "Avec qui désirez vous prendre contact ?"; break;
                case 3: texte = "Vous dites par télépathie:"; break;
                case 4: texte = "Vous entendez une voix dans votre tête:"; break;
                case 5: texte = "Vos dons de télépathie ont aussi des limites !"; break;
                case 6: texte = "Quelqu'un perd contact avec vous"; break;
            }

            if (from != null && texte != null)
                from.SendMessage(texte);
        }

        public static void AlertMessage(Mobile from, int Message, Mobile m)
        {
            string texte = null;

            switch (Message)
            {
                case 0: texte = "Vous perdez contact avec {0}"; break;
            }

            if (from != null && texte != null && m != null)
                from.SendMessage(texte, m.Name);
        }


        public static void SaySomething(PlayerMobile from, string texte)
        {
            AlertMessage(from, 3);
            from.SendMessage(97, texte);

            Packet p = null;

            foreach (NetState ns in from.GetClientsInRange(8))
            {
                Mobile mob = ns.Mobile;

                if (mob != null && mob.AccessLevel >= AccessLevel.GameMaster && mob.AccessLevel > from.AccessLevel)
                {
                    if (p == null && mob is RacePlayerMobile)
                    {
                        RacePlayerMobile mobRPM = mob as RacePlayerMobile;
                        string name = mobRPM.GetKnownName(from as RacePlayerMobile);

                        p = new UnicodeMessage(from.Serial, from.Body, MessageType.Regular, from.SpeechHue, 3, from.Language, ((RacePlayerMobile)from).PlayerName, String.Format("[Telepath]: {0}", texte));
                    }

                    ns.Send(p);
                }
            }
        }

        public static void SaySomething(Mobile from, string texte)
        {
            AlertMessage(from, 4);
            from.SendMessage(97, texte);
        }
        #endregion

    }
}
