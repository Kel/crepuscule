﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Mobiles;
using System.Collections;
using Server.Items;
using Server.Spells.Fifth;
using Server.Gumps;

namespace Server.Engines
{
    public class MindFollow : MindSkill
    {
        public override int NeededAptitude { get { return 50; } }
        public override int NeededSkill { get { return 90; } }
        public override int NeededMana { get { return 100; } }

        private int m_NewBody;
        private int m_OldBody;
        private Souless m_Fake;
        public ArrayList m_PeerMod;
        private Mobile m_Target;

        public Mobile Target
        {
            get { return m_Target; }
            set { m_Target = value; }
        }

        public MindFollow(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
            {
                Caster.CloseAllGumps();
                Caster.SendGump(new MindFollowGump(Caster, this));
            }
        }

        public void Proceed(Mobile target)
        {
            if (Caster.BeginAction(typeof(MindFollow)))
            {
                Target = target;
                if (Caster.Female)
                {
                    m_NewBody = 800;
                }
                else
                {
                    m_NewBody = 800;
                }

                m_OldBody = Caster.Body;
                Caster.Hidden = true;
                Caster.Frozen = true;
                Caster.PlaySound(0x2DF);
                Caster.SendMessage("Vous vous plongez dans un voyage astral.");

                Souless dg = new Souless(this);

                dg.Body = Caster.Body;

                dg.Hue = Caster.Hue;
                dg.Name = Caster.Name;
                dg.SpeechHue = Caster.SpeechHue;
                dg.Fame = Caster.Fame;
                dg.Karma = Caster.Karma;
                dg.EmoteHue = Caster.EmoteHue;
                dg.Title = Caster.Title;
                dg.Criminal = (Caster.Criminal);
                dg.AccessLevel = Caster.AccessLevel;
                dg.Str = Caster.Str;
                dg.Int = Caster.Int;
                dg.Hits = Caster.Hits;
                dg.Dex = Caster.Dex;
                dg.Mana = Caster.Mana;
                dg.Stam = Caster.Stam;

                dg.VirtualArmor = (Caster.VirtualArmor);
                dg.SetSkill(SkillName.Wrestling, Caster.Skills[SkillName.Wrestling].Value);
                dg.SetSkill(SkillName.Tactics, Caster.Skills[SkillName.Tactics].Value);
                dg.SetSkill(SkillName.Anatomy, Caster.Skills[SkillName.Anatomy].Value);

                dg.SetSkill(SkillName.Magery, Caster.Skills[SkillName.Magery].Value);
                dg.SetSkill(SkillName.MagicResist, Caster.Skills[SkillName.MagicResist].Value);
                dg.SetSkill(SkillName.Meditation, Caster.Skills[SkillName.Meditation].Value);
                dg.SetSkill(SkillName.EvalInt, Caster.Skills[SkillName.EvalInt].Value);

                dg.SetSkill(SkillName.Archery, Caster.Skills[SkillName.Archery].Value);
                dg.SetSkill(SkillName.Macing, Caster.Skills[SkillName.Macing].Value);
                dg.SetSkill(SkillName.Swords, Caster.Skills[SkillName.Swords].Value);
                dg.SetSkill(SkillName.Fencing, Caster.Skills[SkillName.Fencing].Value);
                dg.SetSkill(SkillName.Lumberjacking, Caster.Skills[SkillName.Lumberjacking].Value);
                dg.SetSkill(SkillName.Alchemy, Caster.Skills[SkillName.Alchemy].Value);
                dg.SetSkill(SkillName.Parry, Caster.Skills[SkillName.Parry].Value);
                dg.SetSkill(SkillName.Focus, Caster.Skills[SkillName.Focus].Value);
                dg.SetSkill(SkillName.Necromancy, Caster.Skills[SkillName.Necromancy].Value);
                dg.SetSkill(SkillName.Chivalry, Caster.Skills[SkillName.Chivalry].Value);
                dg.SetSkill(SkillName.ArmsLore, Caster.Skills[SkillName.ArmsLore].Value);
                dg.SetSkill(SkillName.Poisoning, Caster.Skills[SkillName.Poisoning].Value);
                dg.SetSkill(SkillName.SpiritSpeak, Caster.Skills[SkillName.SpiritSpeak].Value);
                dg.SetSkill(SkillName.Stealing, Caster.Skills[SkillName.Stealing].Value);
                dg.SetSkill(SkillName.Inscribe, Caster.Skills[SkillName.Inscribe].Value);
                dg.Kills = (Caster.Kills);

                //m_PeerMod = new ArrayList();
                //double loss = (0 - Caster.Skills[SkillName.AnimalTaming].Base);
                //SkillMod sk = new DefaultSkillMod(SkillName.AnimalTaming, true, loss);
                //Caster.AddSkillMod(sk);
                //m_PeerMod.Add(sk);
                //double loss1 = (0 - Caster.Skills[SkillName.AnimalLore].Base);
                //SkillMod sk1 = new DefaultSkillMod(SkillName.AnimalLore, true, loss1);
                //Caster.AddSkillMod(sk1);
                //m_PeerMod.Add(sk1);

                //double loss3 = (0 - Caster.Skills[SkillName.Necromancy].Base);
                //SkillMod sk3 = new DefaultSkillMod(SkillName.Necromancy, true, loss3);
                //Caster.AddSkillMod(sk3);
                //m_PeerMod.Add(sk3);
                //double loss4 = (0 - Caster.Skills[SkillName.TasteID].Base);
                //SkillMod sk4 = new DefaultSkillMod(SkillName.TasteID, true, loss4);
                //Caster.AddSkillMod(sk4);
                //m_PeerMod.Add(sk4);
                // Clear Items 
                RemoveFromAllLayers(dg);

                // Then copy 
                CopyFromLayer(Caster, dg, Layer.FirstValid);
                CopyFromLayer(Caster, dg, Layer.TwoHanded);
                CopyFromLayer(Caster, dg, Layer.Shoes);
                CopyFromLayer(Caster, dg, Layer.Pants);
                CopyFromLayer(Caster, dg, Layer.Shirt);
                CopyFromLayer(Caster, dg, Layer.Helm);
                CopyFromLayer(Caster, dg, Layer.Gloves);
                CopyFromLayer(Caster, dg, Layer.Ring);
                CopyFromLayer(Caster, dg, Layer.Talisman);
                CopyFromLayer(Caster, dg, Layer.Neck);
                CopyFromLayer(Caster, dg, Layer.Hair);
                CopyFromLayer(Caster, dg, Layer.Waist);
                CopyFromLayer(Caster, dg, Layer.InnerTorso);
                CopyFromLayer(Caster, dg, Layer.Bracelet);
                CopyFromLayer(Caster, dg, Layer.Unused_xF);
                CopyFromLayer(Caster, dg, Layer.FacialHair);
                CopyFromLayer(Caster, dg, Layer.MiddleTorso);
                CopyFromLayer(Caster, dg, Layer.Earrings);
                CopyFromLayer(Caster, dg, Layer.Arms);
                CopyFromLayer(Caster, dg, Layer.Cloak);
                CopyFromLayer(Caster, dg, Layer.OuterTorso);
                CopyFromLayer(Caster, dg, Layer.OuterLegs);
                CopyFromLayer(Caster, dg, Layer.LastUserValid);
                CopyFromLayer(Caster, dg, Layer.Mount);
                dg.Owner = Caster;
                dg.OldBody = m_OldBody;
                m_Fake = dg;
                dg.Map = Caster.Map;
                dg.Location = Caster.Location;
                dg.Frozen = true;
                BaseArmor.ValidateMobile(Caster);
                Caster.BodyValue = m_NewBody;
                Caster.Blessed = true;
                StopTimer(Caster);

                Timer t = new InternalTimer(Caster, m_OldBody, m_Fake, this);

                m_Timers[Caster] = t;

                t.Start();
            }
            else
            {
                Caster.SendLocalizedMessage(1005559); // This spell is already in effect.
            }

        }

        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindFollow));
        }

        private void CopyFromLayer(Mobile from, Mobile mimic, Layer layer)
        {
            if (from.FindItemOnLayer(layer) != null)
                mimic.AddItem(Mobile.LiftItemDupe(from.FindItemOnLayer(layer), 1));

            if (mimic.FindItemOnLayer(layer) != null && mimic.FindItemOnLayer(layer).LootType != LootType.Blessed)
                mimic.FindItemOnLayer(layer).LootType = LootType.Newbied;
        }

        private void DeleteFromLayer(Mobile from, Layer layer)
        {
            if (from.FindItemOnLayer(layer) != null)
                from.RemoveItem(from.FindItemOnLayer(layer));
        }
        private void RemoveFromAllLayers(Mobile from)
        {
            DeleteFromLayer(from, Layer.FirstValid);
            DeleteFromLayer(from, Layer.TwoHanded);
            DeleteFromLayer(from, Layer.Shoes);
            DeleteFromLayer(from, Layer.Pants);
            DeleteFromLayer(from, Layer.Shirt);
            DeleteFromLayer(from, Layer.Helm);
            DeleteFromLayer(from, Layer.Gloves);
            DeleteFromLayer(from, Layer.Ring);
            DeleteFromLayer(from, Layer.Talisman);
            DeleteFromLayer(from, Layer.Neck);
            DeleteFromLayer(from, Layer.Hair);
            DeleteFromLayer(from, Layer.Waist);
            DeleteFromLayer(from, Layer.InnerTorso);
            DeleteFromLayer(from, Layer.Bracelet);
            DeleteFromLayer(from, Layer.Unused_xF);
            DeleteFromLayer(from, Layer.FacialHair);
            DeleteFromLayer(from, Layer.MiddleTorso);
            DeleteFromLayer(from, Layer.Earrings);
            DeleteFromLayer(from, Layer.Arms);
            DeleteFromLayer(from, Layer.Cloak);
            DeleteFromLayer(from, Layer.OuterTorso);
            DeleteFromLayer(from, Layer.OuterLegs);
            DeleteFromLayer(from, Layer.LastUserValid);
            DeleteFromLayer(from, Layer.Mount);

        }
        //public void RemovePeerMod()
        //{
        //    if (m_PeerMod == null)
        //        return;

        //    for (int i = 0; i < m_PeerMod.Count; ++i)
        //        ((SkillMod)m_PeerMod[i]).Remove();
        //    m_PeerMod = null;
        //}
        private static Hashtable m_Timers = new Hashtable();

        public static bool StopTimer(Mobile m)
        {
            Timer t = (Timer)m_Timers[m];

            if (t != null)
            {
                if (t is InternalTimer) (t as InternalTimer).Recall();
                t.Stop();
                m_Timers.Remove(m);
            }

            return (t != null);
        }

        private class InternalTimer : Timer
        {
            private RacePlayerMobile Owner;
            private int OldBody;
            private Souless FakeBody;
            private Point3D Location;
            private MindFollow Skill;
            private InternalTeleportTimer TeleportTimer;

            public InternalTimer(Mobile owner, int body, Souless fake, MindFollow skill)
                : base(TimeSpan.FromSeconds(0))
            {
                Owner = owner as RacePlayerMobile;
                OldBody = body;
                FakeBody = fake;
                Skill = skill;

                int val = (int)owner.Skills[SkillName.SpiritSpeak].Value;

                if (val > 100)
                    val = 100;
                //double loss2 = (0 - Owner.Capacities[CapacityName.Telepathy].Value * 3);
                //SkillMod sk2 = new DefaultSkillMod(SkillName.SpiritSpeak, true, loss2);
                //Owner.AddSkillMod(sk2);
                //Skill.m_PeerMod.Add(sk2);

                Delay = TimeSpan.FromSeconds(val);
                Priority = TimerPriority.TwoFiftyMS;

                int count = ((int) val/2)-1;

                TeleportTimer = new InternalTeleportTimer(Owner, Skill.Target, count);
                TeleportTimer.Start();
                    
            }

            public void Recall()
            {

                if (!Owner.CanBeginAction(typeof(MindFollow)))
                {
                    if (TeleportTimer != null)
                    {
                        TeleportTimer.Stopped = true;
                        if (TeleportTimer.Running)
                            TeleportTimer.Stop();
                        TeleportTimer = null;
                    }
                    if (FakeBody != null && !FakeBody.Deleted)
                    {
                        Location = new Point3D(FakeBody.X, FakeBody.Y, FakeBody.Z);
                        Owner.Location = Location;
                        Owner.Blessed = FakeBody.Blessed;
                        FakeBody.Delete();

                    }
                    Owner.BodyValue = OldBody;
                    //Skill.RemovePeerMod();

                    Owner.Hidden = false;
                    Owner.Frozen = false;

                    Owner.EndAction(typeof(MindFollow));

                    BaseArmor.ValidateMobile(Owner);
                }
            }

            protected override void OnTick()
            {
                StopTimer(Owner);
            }
        }

        private class InternalTeleportTimer : Timer
        {
            private RacePlayerMobile m_Owner;
            private Mobile m_Target;
            public bool Stopped=false;

            public InternalTeleportTimer(RacePlayerMobile caster, Mobile target, int count)
                : base(TimeSpan.FromSeconds(0), TimeSpan.FromMilliseconds(500))
            {
                m_Owner = caster;
                m_Target = target;
                Priority = TimerPriority.TwoFiftyMS;
            }

            protected override void OnTick()
            {
                if (m_Target != null && m_Owner != null
                    && m_Target != null && m_Target is RacePlayerMobile
                    && !m_Target.Deleted && m_Target.Alive 
                    && m_Target.AccessLevel <= m_Owner.AccessLevel
                    && !Stopped)
                {
                    m_Owner.Location = m_Target.Location;
                    m_Owner.Map = m_Target.Map;
                }
            }
        }

        public class AddPermFollowTarget : Target
        {
            private static int MaxFriends;
            public AddPermFollowTarget(int nbr)
                : base(5, true, TargetFlags.None)
            {
                MaxFriends = nbr;
            }

            protected override void OnTarget(Mobile m, object targ)
            {

                PlayerMobile from = ((PlayerMobile)m);

                if (!(targ is PlayerMobile))
                {
                    AlertMessage(from, 2);
                    return;
                }

                PlayerMobile target = ((PlayerMobile)targ);

                if (IsFriend(from, target))
                {
                    AlertMessage(from, 0, target);
                    return;
                }

                if (ItsMe(from, target))
                {
                    AlertMessage(from, 1);
                    return;
                }

                if (!CanHaveFriends(from))
                {
                    AlertMessage(from, 0);
                    return;
                }

                if (target.AccessLevel > from.AccessLevel)
                {
                    AlertMessage(from, 0);
                }

                from.MindFollowPermanentVictim.Add(target);
                from.MindFollowPermanentVictimDates.Add(DateTime.Now);
                AlertMessage(from, 1, target);
            }

            private static bool CanHaveFriends(PlayerMobile from)
            {
                if (((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value <= 0)
                    return true;

                return (from.MindFollowPermanentVictim.Count < ((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value);
            }

            private static bool ItsMe(Mobile from, Mobile m)
            {
                return (from == m);
            }

            private static bool IsFriend(PlayerMobile from, Mobile target)
            {
                for (int i = 0; i < from.MindFollowPermanentVictim.Count; ++i)
                {
                    if (from.MindFollowPermanentVictim[i] == target)
                        return true;
                }

                return false;
            }

            private static void AlertMessage(Mobile from, int Message)
            {
                string texte = null;

                switch (Message)
                {
                    case 0: texte = "Vos dons de télépathie ont aussi des limites !"; break;
                    case 1: texte = "Vous ne pouvez pas prendre contact avec vous meme"; break;
                    case 2: texte = "Vous devez pointer un joueur !"; break;
                }

                if (from != null && texte != null)
                    from.SendMessage(texte);
            }

            private static void AlertMessage(Mobile from, int Message, Mobile m)
            {
                string texte = null;

                switch (Message)
                {
                    case 0: texte = "Vous êtes deja en contact avec {0}"; break;
                    case 1: texte = "Vous prenez contact avec {0}"; break;
                }

                if (from != null && texte != null && m != null)
                    from.SendMessage(texte, m.Name);
            }
        }

        public class AddTempFollowTarget : Target
        {
            private static int MaxFriends;
            public AddTempFollowTarget(int nbr)
                : base(5, true, TargetFlags.None)
            {
                MaxFriends = nbr;
            }

            protected override void OnTarget(Mobile m, object targ)
            {

                PlayerMobile from = ((PlayerMobile)m);

                if (!(targ is PlayerMobile))
                {
                    AlertMessage(from, 2);
                    return;
                }

                PlayerMobile target = ((PlayerMobile)targ);

                if (IsFriend(from, target))
                {
                    AlertMessage(from, 0, target);
                    return;
                }

                if (ItsMe(from, target))
                {
                    AlertMessage(from, 1);
                    return;
                }

                if (!CanHaveFriends(from))
                {
                    AlertMessage(from, 0);
                    return;
                }

                if (target.AccessLevel > from.AccessLevel)
                {
                    AlertMessage(from, 0);
                }

                from.MindFollowTemporaryVictim.Add(target);
                from.MindFollowTemporaryVictimDates.Add(DateTime.Now);
                AlertMessage(from, 1, target);
            }

            private static bool CanHaveFriends(PlayerMobile from)
            {
                if (((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value <= 0)
                    return true;

                return (from.MindFollowTemporaryVictim.Count < ((RacePlayerMobile)from).Capacities[CapacityName.Telepathy].Value);
            }

            private static bool ItsMe(Mobile from, Mobile m)
            {
                return (from == m);
            }

            private static bool IsFriend(PlayerMobile from, Mobile target)
            {
                for (int i = 0; i < from.MindFollowTemporaryVictim.Count; ++i)
                {
                    if (from.MindFollowTemporaryVictim[i] == target)
                        return true;
                }

                return false;
            }

            private static void AlertMessage(Mobile from, int Message)
            {
                string texte = null;

                switch (Message)
                {
                    case 0: texte = "Vos dons de télépathie ont aussi des limites !"; break;
                    case 1: texte = "Vous ne pouvez pas prendre contact avec vous meme"; break;
                    case 2: texte = "Vous devez pointer un joueur !"; break;
                }

                if (from != null && texte != null)
                    from.SendMessage(texte);
            }

            private static void AlertMessage(Mobile from, int Message, Mobile m)
            {
                string texte = null;

                switch (Message)
                {
                    case 0: texte = "Vous êtes deja en contact avec {0}"; break;
                    case 1: texte = "Vous prenez contact avec {0}"; break;
                }

                if (from != null && texte != null && m != null)
                    from.SendMessage(texte, m.Name);
            }
        }
    }
}