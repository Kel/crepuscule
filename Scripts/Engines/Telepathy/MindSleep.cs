﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Network;
using Server.Mobiles;
using Server.Targeting;
using System.Collections;
using Server.Items;

namespace Server.Engines
{
    public class MindSleep : MindSkill
    {
        public override int NeededAptitude { get { return 29; } }
        public override int NeededSkill { get { return 60; } }
        public override int NeededMana { get { return 50; } }

        private SleepingBody m_Body;
        private bool m_Blessed;

        public MindSleep(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
                Caster.Target = new InternalTarget(this);
        }


        public void Target(Mobile m)
        {
            if (!(m is RacePlayerMobile))
            {
                Caster.SendMessage("Impossible d'effectuer cette action sur cette cible.");
            }
            if (!(m is RacePlayerMobile) && Caster.AccessLevel <= m.AccessLevel )
            {
                Caster.SendMessage("Impossible d'utiliser sur cette cible.");
            }
            else if (!Caster.CanSee(m))
            {
                Caster.SendLocalizedMessage(500237); // Target can not be seen.
            }
            else if (!Caster.CanBeginAction(typeof(MindSleep)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else
            {
                if (m != null && m is RacePlayerMobile && Caster.CanBeHarmful(m) && CheckSequence())
                {
                    Caster.BeginAction(typeof(MindSleep));
                    if (!CheckIfCanBlock(Caster, m))
                    {
                        TelepathyHelper.Turn(Caster, m);

                        Effects.SendLocationParticles(EffectItem.Create(new Point3D(m.X, m.Y, m.Z + 16), Caster.Map, EffectItem.DefaultDuration), 0x376A, 10, 15, 5045);
                        m.PlaySound(0x3C4);

                        m.Hidden = true;
                        m.Frozen = true;
                        m.Squelched = true;

                        ArrayList sleepequip = new ArrayList();

                        Item hat = m.FindItemOnLayer(Layer.Helm);
                        if (hat != null)
                        {
                            sleepequip.Add(hat);
                        }
                        SleepingBody body = new SleepingBody(m, m_Blessed);
                        body.Map = m.Map;
                        body.Location = m.Location;
                        m_Body = body;
                        m.Z -= 100;

                        m.SendMessage("Vous vous endormez lentement...");

                        RemoveTimer(m);

                        int seconds = 10 + (int)(Caster.Capacities[CapacityName.Telepathy].Value * 0.5);
                        TimeSpan duration = TimeSpan.FromSeconds(seconds);

                        Timer t = new InternalTimer(m, duration, m_Body);
                        m_Table[m] = t;
                        t.Start();
                    }

                    Timer.DelayCall(TimeSpan.FromSeconds(600.0), new TimerStateCallback(EndAction), Caster);
                }
            }
        }

        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindSleep));
        }


        #region Internal 

        private static Hashtable m_Table = new Hashtable();

        public static void RemoveTimer(Mobile m)
        {
            Timer t = (Timer)m_Table[m];

            if (t != null)
            {
                t.Stop();
                m_Table.Remove(m);
            }
        }

        private class InternalTimer : Timer
        {
            private Mobile m_Mobile;
            private Item m_Body;

            public InternalTimer(Mobile m, TimeSpan duration, Item body)
                : base(duration)
            {
                m_Mobile = m;
                m_Body = body;
            }

            protected override void OnTick()
            {
                m_Mobile.RevealingAction();
                m_Mobile.Frozen = false;
                m_Mobile.Squelched = false;

                if (m_Body != null)
                {
                    m_Body.Delete();
                    m_Mobile.SendMessage("Vous vous reveillez.");
                    m_Mobile.Z = m_Body.Z;
                    m_Mobile.Animate(21, 6, 1, false, false, 0);
                }
                RemoveTimer(m_Mobile);
            }
        }

        public class InternalTarget : Target
        {
            private MindSleep m_Owner;

            public InternalTarget(MindSleep owner)
                : base(12, false, TargetFlags.Beneficial)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is Mobile)
                {
                    m_Owner.Target((Mobile)o);
                }
            }

            protected override void OnTargetFinish(Mobile from)
            {
            }
        }

        #endregion


    }
}
