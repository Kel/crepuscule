﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Regions;
using Server.Mobiles;
using Server.Items;
using Server.Spells;
using Server.Gumps;
using System.Collections.Generic;

namespace Server.Engines
{
    public enum MindEmotionEnum
    {
        Affection,
        Amour,
        Bonheur,
        Culpabilite,
        Ennui,
        Envie,
        Fanatisme,
        Frustration,
        Haine,
        Honte,
        Humiliation,
        Jalousie,
        Joie,
        Luxure,
        Passion, 
        Peur,
        Panique,
        Repentance,
        Satisfaction,
        Solitude,
        Timidite,
        Tristesse
    }

    public class MindEmotion : MindSkill
    {
        public override int NeededAptitude { get { return 1; } }
        public override int NeededSkill { get { return 15; } }
        public override int NeededMana { get { return 7; } }

        private GumpAction CurrentAction = null;
        public MindEmotion(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
            {
                Caster.CloseGump(typeof(MindEmotionGump));
                Caster.SendGump(new MindEmotionGump(Caster, GetActions(), 0, this));
            }
        }


        public void Target(Mobile target)
        {
            if (!Caster.CanBeginAction(typeof(MindEmotion)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else if (CurrentAction != null && CheckSequence())
            {
                TelepathyHelper.Turn(Caster, target);
                Caster.BeginAction(typeof(MindEmotion));

                target.SendMessage(0x3B2, "Vous ressentez une emotion que quelq'un vous transmet dans votre tête: "+ CurrentAction.Name);
                target.LocalOverheadMessage(MessageType.Regular, 0x3B2, false, "*"+ CurrentAction.Name + " ressentie*");

                Timer.DelayCall(TimeSpan.FromSeconds(25.0), new TimerStateCallback(EndAction), Caster);
            }

        }

        public void Proceed(GumpAction action)
        {
            CurrentAction = action;
            Caster.Target = new InternalTarget(this);
        }

        public List<GumpAction> GetActions()
        {
            List<GumpAction> list = new List<GumpAction>();
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 1) list.Add(new GumpAction("Affection",(int)MindEmotionEnum.Affection));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 7) list.Add(new GumpAction("Amour", (int)MindEmotionEnum.Amour));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 6) list.Add(new GumpAction("Bonheur", (int)MindEmotionEnum.Bonheur));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 8) list.Add(new GumpAction("Culpabilite", (int)MindEmotionEnum.Culpabilite));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 18) list.Add(new GumpAction("Ennui", (int)MindEmotionEnum.Ennui));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 20) list.Add(new GumpAction("Envie", (int)MindEmotionEnum.Envie));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 19) list.Add(new GumpAction("Fanatisme", (int)MindEmotionEnum.Fanatisme));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 5) list.Add(new GumpAction("Frustration", (int)MindEmotionEnum.Frustration));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 21) list.Add(new GumpAction("Haine", (int)MindEmotionEnum.Haine));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 17) list.Add(new GumpAction("Honte", (int)MindEmotionEnum.Honte));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 9) list.Add(new GumpAction("Humiliation", (int)MindEmotionEnum.Humiliation));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 10) list.Add(new GumpAction("Jalousie", (int)MindEmotionEnum.Jalousie));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 2) list.Add(new GumpAction("Joie", (int)MindEmotionEnum.Joie));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 22) list.Add(new GumpAction("Luxure", (int)MindEmotionEnum.Luxure));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 12) list.Add(new GumpAction("Passion", (int)MindEmotionEnum.Passion));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 11) list.Add(new GumpAction("Peur", (int)MindEmotionEnum.Peur));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 15) list.Add(new GumpAction("Panique", (int)MindEmotionEnum.Panique));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 14) list.Add(new GumpAction("Repentance", (int)MindEmotionEnum.Repentance));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 13) list.Add(new GumpAction("Satisfaction", (int)MindEmotionEnum.Satisfaction));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 4) list.Add(new GumpAction("Solitude", (int)MindEmotionEnum.Solitude));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 16) list.Add(new GumpAction("Timidite", (int)MindEmotionEnum.Timidite));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 3) list.Add(new GumpAction("Tristesse", (int)MindEmotionEnum.Tristesse));
            return list;
        }


        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindEmotion));
        }

        public class InternalTarget : Target
        {
            private MindEmotion m_Owner;

            public InternalTarget(MindEmotion owner)
                : base(12, false, TargetFlags.None)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if(o != null && o is Mobile)
                {
                    Mobile target = o as Mobile;
                    if ( target is RacePlayerMobile
                        && !target.Deleted && target.Alive && !target.Hidden
                        && from.AccessLevel <= target.AccessLevel
                        && target.NetState != null)
                    {
                        m_Owner.Target(target);
                    }
                    else
                        from.SendLocalizedMessage(501857); // This spell won't work on that!
                }else
                        from.SendLocalizedMessage(501857); // This spell won't work on that!
            }

            protected override void OnTargetFinish(Mobile from)
            {
                
            }
        }
    }
}