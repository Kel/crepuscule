﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Gumps;
using Server.Mobiles;
using Server.Network;

namespace Server.Engines
{
    public class TelepathGump : Gump
    {
        public void AddBlueBack(int width, int height)
        {
            AddBackground(55, 60, width - 00, height - 00, 0xE10);
            AddBackground(63, 65, width - 16, height - 11, 0x053);
            AddImageTiled(70, 74, width - 29, height - 29, 0xE14);
            AddAlphaRegion(70, 74, width - 29, height - 29);
        }

        public void AddButtonLabeled(int x, int y, int buttonID, string text)
        {
            AddButton(x, y - 1, 4005, 4007, buttonID, GumpButtonType.Reply, 0x174);
            AddLabel(x + 35, y, 0x174, text);
        }

        public void AddButtonLabeledDel(int x, int y, int buttonID, string text)
        {
            AddButton(x, y - 1, 4017, 4019, buttonID, GumpButtonType.Reply, 0);
            AddLabel(x + 35, y, 0x174, text);
        }
        public void AddButtonDel(int x, int y, int buttonID)
        {
            AddButton(x, y - 1, 4017, 4019, buttonID, GumpButtonType.Reply, 0);
        }

        public TelepathGump(Mobile m)
            : base(150, 120)
        {

            RacePlayerMobile from = m as RacePlayerMobile;
            int victimes = from.TelepathieVictim.Count;

            // Fond
            AddBlueBack(280, 185 + (victimes * 20));

            //Titre
            AddLabel(150, 75, 0x26, "Télépathie");

            if (from.Telepathy)
            {
                for (int i = 0; i < victimes; ++i)
                {
                    if (from.TelepathieVictim[i] != null && from.TelepathieVictim[i] is RacePlayerMobile)
                    {
                        AddImageTiled(80, 100 + (i * 20), 182, 23, 0x52);
                        AddImageTiled(81, 101 + (i * 20), 180, 21, 0xBBC);
                        AddLabelCropped(91, 101 + (i * 20), 180, 21, 0, (from.TelepathieVictim[i] as RacePlayerMobile).GetKnownName(from));
                        AddButtonDel(270, 100 + (i * 20), i + 100);
                    }
                }
            }

            if (TelepathyHelper.CanHaveFriends(from))
                AddButtonLabeled(100, 150 + (victimes * 20), 1, "Prendre contact");
            AddLabel(240, 206 + (victimes * 20), 1, "");
        }

        public override void OnResponse(NetState sender, RelayInfo info)
        {
            PlayerMobile from = ((PlayerMobile)sender.Mobile);
            int val = info.ButtonID;

            switch (val)
            {
                case 0: break;
                case 1:
                    {
                        if (!TelepathyHelper.CanHaveFriends(from))
                        {
                            TelepathyHelper.AlertMessage(from, 5);
                            return;
                        }

                        TelepathyHelper.AlertMessage(from, 2);

                        from.Target = new AddTelepathTarget(((RacePlayerMobile)sender.Mobile).Capacities[CapacityName.Telepathy].Value);

                        break;
                    }
            }

            if (val >= 100)
            {
                int valeur = (val - 100);

                if (from.TelepathieVictim[valeur] is Mobile)
                {
                    TelepathyHelper.AlertMessage(from, 0, (Mobile)from.TelepathieVictim[valeur]);
                    TelepathyHelper.AlertMessage((Mobile)from.TelepathieVictim[valeur], 6);
                    from.TelepathieVictim.RemoveAt(valeur);
                }

            }
        }
    }

}
