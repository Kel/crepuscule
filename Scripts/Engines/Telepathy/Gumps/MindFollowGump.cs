﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Mobiles;
using Server.Items;
using Server.Network;
using Server.Engines;
using Server.Spells.Fifth;

namespace Server.Gumps
{
    public class MindFollowGump : Gump
    {
        #region Properties
        private MindFollow m_Skill;

        public MindFollow Skill
        {
            get { return m_Skill; }
            set { m_Skill = value; }
        }
        #endregion

        public void AddBlueBack(int width, int height)
        {
            AddBackground(55, 60, width - 00, height - 00, 0xE10);
            AddBackground(63, 65, width - 16, height - 11, 0x053);
            AddImageTiled(70, 74, width - 29, height - 29, 0xE14);
            AddAlphaRegion(70, 74, width - 29, height - 29);
        }

        public void AddButtonLabeled(int x, int y, int buttonID, string text)
        {
            AddButton(x, y - 1, 4005, 4007, buttonID, GumpButtonType.Reply, 0x174);
            AddLabel(x + 35, y, 0x174, text);
        }

        public void AddButtonLabeledDel(int x, int y, int buttonID, string text)
        {
            AddButton(x, y - 1, 4017, 4019, buttonID, GumpButtonType.Reply, 0);
            AddLabel(x + 35, y, 0x174, text);
        }

        public void AddButtonDel(int x, int y, int buttonID)
        {
            AddButton(x, y - 1, 4017, 4019, buttonID, GumpButtonType.Reply, 0);
        }

        public void AddButtonFollow(int x, int y, int buttonID)
        {
            AddButton(x, y - 1, 1209, 1210, buttonID, GumpButtonType.Reply, 0);
        }

        public MindFollowGump(Mobile m, MindFollow skill)
            : base(150, 120)
        {
            Skill = skill;

            RacePlayerMobile from = m as RacePlayerMobile;
            CleanTemporaryTargets(from);

            int permanentVictims = from.MindFollowPermanentVictim.Count;
            int tempVictims = from.MindFollowTemporaryVictim.Count;
            int hauteurGump = 225 + ((tempVictims + permanentVictims) * 20);

            // Fond
            AddBlueBack(280, 225 + ((tempVictims + permanentVictims) * 20));

            //Titre
            AddLabel(150, 75, 0x26, "Vision mentale");

            //Sous-Titre
            AddLabel(135, 100, 0x26, "Contacts permanents");

            if (from.Telepathy)
            {
                for (int i = 0; i < permanentVictims; ++i)
                {
                    if (from.MindFollowPermanentVictim[i] != null && from.MindFollowPermanentVictim[i] is RacePlayerMobile)
                    {
                        AddButtonFollow(82, 130 + (i*20), i + 300);
                        AddImageTiled(100, 125 + (i * 20), 162, 23, 0x52);
                        AddImageTiled(101, 126 + (i * 20), 160, 21, 0xBBC);
                        AddLabelCropped(111, 126 + (i * 20), 160, 21, 0, (from.MindFollowPermanentVictim[i] as RacePlayerMobile).GetKnownName(from));
                        if ((DateTime)from.MindFollowPermanentVictimDates[i] <= DateTime.Now.AddDays(-30))
                            AddButtonDel(270, 125 + (i * 20), i + 100);
                    }
                }
            }

            if ( permanentVictims < 2 )
                AddButtonLabeled(100, 150 + (permanentVictims * 20), 0, "Ajouter contact permanent");

            //Sous-Titre
            AddLabel(135, 175 + (permanentVictims * 20), 0x26, "Contacts temporaires");

            if (from.Telepathy)
            {
                for (int i = 0; i < tempVictims; ++i)
                {
                    if (from.MindFollowTemporaryVictim[i] != null && from.MindFollowTemporaryVictim[i] is RacePlayerMobile)
                    {
                        AddButtonFollow(82, 205 + (permanentVictims * 20) + (i * 20), i + 400);
                        AddImageTiled(100, 200 + (permanentVictims * 20) + (i * 20), 162, 23, 0x52);
                        AddImageTiled(101, 201 + (permanentVictims * 20) + (i * 20), 160, 21, 0xBBC);
                        AddLabelCropped(111, 201 + (permanentVictims * 20) + (i * 20), 160, 21, 0, (from.MindFollowTemporaryVictim[i] as RacePlayerMobile).GetKnownName(from));
                        AddButtonDel(270, 200 + (permanentVictims * 20) + (i * 20), i + 200);
                    }
                }
            }

            if ( tempVictims < 5 )
                AddButtonLabeled(100, 225 + ((tempVictims + permanentVictims) * 20), 1, "Ajouter contact temporaire");

            AddLabel(240, 250 + ((tempVictims + permanentVictims) * 20), 1, "");
        }

        public override void OnResponse(NetState sender, RelayInfo info)
        {
            PlayerMobile from = ((PlayerMobile)sender.Mobile);
            int val = info.ButtonID;

            if (!TelepathyHelper.CanHaveFriends(from))
            {
                TelepathyHelper.AlertMessage(from, 5);
                return;
            }

            switch (val)
            {
                case 0:
                    {
                        if (from.MindFollowPermanentVictim.Count < 2)
                        {
                            TelepathyHelper.AlertMessage(from, 2);
                            from.Target = new Server.Engines.MindFollow.AddPermFollowTarget(((RacePlayerMobile)sender.Mobile).Capacities[CapacityName.Telepathy].Value);
                        }
                        else
                            TelepathyHelper.AlertMessage(from, 0);

                        break;
                    }
                case 1:
                    {
                        if (from.MindFollowTemporaryVictim.Count < 5)
                        {
                            TelepathyHelper.AlertMessage(from, 2);
                            from.Target = new Server.Engines.MindFollow.AddTempFollowTarget(((RacePlayerMobile)sender.Mobile).Capacities[CapacityName.Telepathy].Value);
                        }
                        else
                            TelepathyHelper.AlertMessage(from, 0);

                        break;
                    }
            }

            if (val >= 300)
            {
                if ( !CheckCast((RacePlayerMobile)from))
                    return;

                RacePlayerMobile followed;

                if (val >= 400)
                {
                    val -= 400;
                    followed = (RacePlayerMobile)from.MindFollowTemporaryVictim[val];
                }
                else
                {
                    val -= 300;
                    followed = (RacePlayerMobile)from.MindFollowPermanentVictim[val];
                }

                if (from is RacePlayerMobile
                    && followed != null && followed is RacePlayerMobile
                    && !followed.Deleted && followed.Alive && !followed.Hidden
                    && followed.AccessLevel <= from.AccessLevel
                    && followed.NetState != null)
                {
                    if (!TelepathyHelper.CheckIfCanBlock(from as RacePlayerMobile, followed))
                    {
                        Skill.Proceed(followed);
                    }
                }
                else
                {
                    from.SendMessage("Vous n'arrivez pas a vous concentrer.");
                }
            }

            if (val >= 100)
            {
                if (val >= 200)
                {
                    val -= 200;

                    if (val >= 0 && val < from.MindFollowTemporaryVictim.Count)
                    {
                        if (from.MindFollowTemporaryVictim[val] is Mobile)
                        {
                            TelepathyHelper.AlertMessage(from, 0, (Mobile)from.MindFollowTemporaryVictim[val]);
                            from.MindFollowTemporaryVictim.RemoveAt(val);
                            from.MindFollowTemporaryVictimDates.RemoveAt(val);
                        }
                    }
                }
                else
                {
                    val -= 100;

                    if (val >= 0 && val < from.MindFollowPermanentVictim.Count)
                    {
                        if (from.MindFollowPermanentVictim[val] is Mobile)
                        {
                            if ((DateTime)from.MindFollowPermanentVictimDates[val] <= DateTime.Now.AddDays(-30))
                            {
                                TelepathyHelper.AlertMessage(from, 0, (Mobile)from.MindFollowPermanentVictim[val]);
                                from.MindFollowPermanentVictim.RemoveAt(val);
                                from.MindFollowPermanentVictimDates.RemoveAt(val);
                            }
                            else
                                TelepathyHelper.AlertMessage(from, 0);

                        }
                    }
                }
            }

        }

        private void CleanTemporaryTargets(RacePlayerMobile from)
        {
            int hours = 168 + (int)(from.Capacities[CapacityName.Telepathy].Value * 2.4);

            if (hours >= 336)
                hours = 336;

            for (int i = from.MindFollowTemporaryVictim.Count - 1; i > 0; i--)
            {
                if (DateTime.Compare((DateTime)from.MindFollowTemporaryVictimDates[i], DateTime.Now.AddHours(-hours)) < 0)
                {
                    from.MindFollowTemporaryVictim.RemoveAt(i);
                    from.MindFollowTemporaryVictimDates.RemoveAt(i);
                }
            }
        }

        private bool CheckCast(RacePlayerMobile Caster)
        {
            string Msg = "Vous n'arrivez pas à vous concentrer.";
            if (Caster.Mounted)
            {
                Caster.SendLocalizedMessage(1042561); //Please dismount first.
                return false;
            }
            else if (!Caster.CanBeginAction(typeof(MindFollow)))
            {
                Caster.SendLocalizedMessage(1005559); // This spell is already in effect.
                return false;
            }
            else if (Server.Spells.Necromancy.TransformationSpell.UnderTransformation(Caster))
            {
                Caster.SendMessage(Msg);
                return false;
            }
            else if (DisguiseGump.IsDisguised(Caster))
            {
                Caster.SendMessage(Msg);
                return false;
            }
            else if (Caster.BodyMod == 183 || Caster.BodyMod == 184)
            {
                Caster.SendMessage(Msg);
                return false;
            }
            else if (!Caster.CanBeginAction(typeof(IncognitoSpell)) || Caster.IsBodyMod)
            {
                Caster.LocalOverheadMessage(MessageType.Regular, 0x3B2, 502632); // The spell fizzles.

                if (Caster.Player)
                {
                    Caster.PlaySound(0x5C);
                }
                return false;
            }
            else if (Caster.CanBeginAction(typeof(MindFollow)) && this.Skill.CheckSequence())
            {
                return true;
            }

            return false;
        }
    }

}

