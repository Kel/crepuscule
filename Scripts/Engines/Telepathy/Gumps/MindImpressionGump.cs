﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Server.Mobiles;
using Server.Items;
using Server.Network;
using Server.Engines;

namespace Server.Gumps
{

    public class MindImpressionGump : ActionListGump
    {
        #region Properties
        private MindImpression m_Skill;
        public MindImpression Skill
        {
            get { return m_Skill; }
            set { m_Skill = value; }
        }
        #endregion

        public MindImpressionGump(Mobile owner, List<GumpAction> list, int page, MindImpression skill)
            : base(owner, list, page)
        {
            m_Skill = skill;
        }


        protected override void OnPageTurn(Mobile from, Mobile owner, List<GumpAction> list, int page)
        {
            from.SendGump(new MindImpressionGump(owner, list, page, m_Skill));
        }


        protected override void OnActionClicked(Mobile from, GumpAction clicked)
        {
            Skill.Proceed(clicked);
        }


    }

}
