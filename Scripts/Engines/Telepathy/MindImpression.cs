﻿using System;
using Server.Targeting;
using Server.Network;
using Server.Regions;
using Server.Mobiles;
using Server.Items;
using Server.Spells;
using Server.Gumps;
using System.Collections.Generic;

namespace Server.Engines
{
    public enum MindImpressionEnum
    {
        Danger,
        Prudence, 
        Vitesse, 
        Securite, 
        Confort, 
        Malaise
    }

    public class MindImpression : MindSkill
    {
        public override int NeededAptitude { get { return 11; } }
        public override int NeededSkill { get { return 25; } }
        public override int NeededMana { get { return 7; } }

        private GumpAction CurrentAction = null;
        public MindImpression(RacePlayerMobile caster): base(caster){}

        public override void OnCast()
        {
            if (CanCast())
            {
                Caster.CloseGump(typeof(MindImpressionGump));
                Caster.SendGump(new MindImpressionGump(Caster, GetActions(), 0, this));
            }
        }


        public void Target(Mobile target)
        {
            if (!Caster.CanBeginAction(typeof(MindImpression)))
            {
                Caster.SendLocalizedMessage(501789); // You must wait before trying again.
            }
            else if (CurrentAction != null && CheckSequence())
            {
                TelepathyHelper.Turn(Caster, target);
                Caster.BeginAction(typeof(MindImpression));

                target.SendMessage(0x3B2, "Vous ressentez une emotion que quelq'un vous transmet dans votre tête: "+ CurrentAction.Name);
                target.LocalOverheadMessage(MessageType.Regular, 0x3B2, false, "*"+ CurrentAction.Name + " ressentie*");

                Timer.DelayCall(TimeSpan.FromSeconds(25.0), new TimerStateCallback(EndAction), Caster);
            }

        }

        public void Proceed(GumpAction action)
        {
            CurrentAction = action;
            Caster.Target = new InternalTarget(this);
        }

        public List<GumpAction> GetActions()
        {
            List<GumpAction> list = new List<GumpAction>();
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 11) list.Add(new GumpAction("Danger", (int)MindImpressionEnum.Danger));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 13) list.Add(new GumpAction("Prudence", (int)MindImpressionEnum.Prudence));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 12) list.Add(new GumpAction("Vitesse", (int)MindImpressionEnum.Vitesse));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 14) list.Add(new GumpAction("Securite", (int)MindImpressionEnum.Securite));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 15) list.Add(new GumpAction("Confort", (int)MindImpressionEnum.Confort));
            if (Caster.Capacities[CapacityName.Telepathy].Value >= 16) list.Add(new GumpAction("Malaise", (int)MindImpressionEnum.Malaise));
            
            return list;
        }


        private static void EndAction(object state)
        {
            ((Mobile)state).EndAction(typeof(MindImpression));
        }

        public class InternalTarget : Target
        {
            private MindImpression m_Owner;

            public InternalTarget(MindImpression owner)
                : base(12, false, TargetFlags.None)
            {
                m_Owner = owner;
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if(o != null && o is Mobile)
                {
                    Mobile target = o as Mobile;
                    if ( target is RacePlayerMobile
                        && !target.Deleted && target.Alive && !target.Hidden
                        && from.AccessLevel <= target.AccessLevel
                        && target.NetState != null)
                    {
                        m_Owner.Target(target);
                    }
                    else
                        from.SendLocalizedMessage(501857); // This spell won't work on that!
                }else
                        from.SendLocalizedMessage(501857); // This spell won't work on that!
            }

            protected override void OnTargetFinish(Mobile from)
            {
                
            }
        }
    }
}