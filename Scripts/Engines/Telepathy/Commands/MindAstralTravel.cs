﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindAstralTravelCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindAstralTravel", AccessLevel.Player, new CommandEventHandler(MindAstralTravelCommand_OnCommand));
        }

        private static void MindAstralTravelCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalAstralTravel)) != null)
                {
                    MindAstralTravel attack = new MindAstralTravel(args.Mobile as RacePlayerMobile);
                    attack.OnCast();
                }
            }
        }

    }

}

