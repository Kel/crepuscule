﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindFollowCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindFollow", AccessLevel.Player, new CommandEventHandler(MindFollowCommand_OnCommand));
        }

        private static void MindFollowCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalFollow)) != null)
                {
                    MindFollow attack = new MindFollow(args.Mobile as RacePlayerMobile);
                    attack.OnCast();
                }
            }
        }

    }

}

