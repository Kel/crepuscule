﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindEmotionCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindEmotion", AccessLevel.Player, new CommandEventHandler(MindEmotionCommand_OnCommand));
        }

        private static void MindEmotionCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalEmotion)) != null)
                {
                    MindEmotion attack = new MindEmotion(args.Mobile as RacePlayerMobile);
                    attack.OnCast();
                }
            }
        }

    }

}

