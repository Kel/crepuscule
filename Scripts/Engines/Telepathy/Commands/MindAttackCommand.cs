﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindAttackCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindAttack", AccessLevel.Player, new CommandEventHandler(MindAttackCommand_OnCommand));
        }

        private static void MindAttackCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalAttack)) != null)
                {
                    MindAttack attack = new MindAttack(args.Mobile as RacePlayerMobile);
                    attack.OnCast();
                }
            }
        }

    }

}

