﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindTelekenesisCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindTelekinesis", AccessLevel.Player, new CommandEventHandler(MindTelekenesisCommand_OnCommand));
        }

        private static void MindTelekenesisCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalTelekinesis)) != null)
                {
                    MindTelekenesis skill = new MindTelekenesis(args.Mobile as RacePlayerMobile);
                    skill.OnCast();
                }
            }
        }

    }

}

