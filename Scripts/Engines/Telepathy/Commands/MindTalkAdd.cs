﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindTalkAddCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindTalkAdd", AccessLevel.Player, new CommandEventHandler(MindTalkAddCommand_OnCommand));
        }

        private static void MindTalkAddCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalTalkAdd)) != null)
                {
                    MindTalkAdd attack = new MindTalkAdd(args.Mobile as RacePlayerMobile);
                    attack.OnCast();
                }
            }
        }

    }

}

