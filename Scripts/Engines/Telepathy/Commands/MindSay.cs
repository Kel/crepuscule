﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindSayCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindSay", AccessLevel.Player, new CommandEventHandler(MindSayCommand_OnCommand));
            Commands.Register("MS", AccessLevel.Player, new CommandEventHandler(MindSayCommand_OnCommand));
        }

        private static void MindSayCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalSay)) != null)
                {
                    if (!String.IsNullOrEmpty(args.ArgString))
                    {
                        MindSay attack = new MindSay(args.Mobile as RacePlayerMobile);
                        attack.OnCast(args.ArgString);
                    }
                }
            }
        }

    }

}

