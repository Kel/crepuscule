﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindRecallCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindRecall", AccessLevel.Player, new CommandEventHandler(MindRecallCommand_OnCommand));
        }

        private static void MindRecallCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalRecall)) != null)
                {
                    MindAstralTravel.StopTimer(args.Mobile);
                    MindFollow.StopTimer(args.Mobile);
                }
            }
        }

    }

}
