﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindImpressionCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindImpression", AccessLevel.Player, new CommandEventHandler(MindImpressionCommand_OnCommand));
        }

        private static void MindImpressionCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalImpression)) != null)
                {
                    MindImpression attack = new MindImpression(args.Mobile as RacePlayerMobile);
                    attack.OnCast();
                }
            }
        }

    }

}

