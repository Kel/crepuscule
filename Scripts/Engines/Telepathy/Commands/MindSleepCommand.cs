﻿using System;
using System.Collections;
using Server;
using Server.Network;
using Server.Mobiles;
using System.Collections.Generic;
using System.Linq;
using Server.Items;

namespace Server.Engines
{
    public class MindSleepCommand
    {
        public static void Initialize()
        {
            Commands.Register("MindSleep", AccessLevel.Player, new CommandEventHandler(MindSleepCommand_OnCommand));
        }

        private static void MindSleepCommand_OnCommand(CommandEventArgs args)
        {
            if (args.Mobile != null && args.Mobile is RacePlayerMobile && args.Mobile.Backpack != null)
            {
                if (args.Mobile.Backpack.FindItemByType(typeof(CrystalSleep)) != null)
                {
                    MindSleep skill = new MindSleep(args.Mobile as RacePlayerMobile);
                    skill.OnCast();
                }
            }
        }

    }

}

