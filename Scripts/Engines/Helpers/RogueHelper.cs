﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines.Helpers
{
    class RogueHelper
    {
        #region Stay Hidden

        // SH: Stay Hidden system
        // System to determine chance to stay hidden doing an action like
        // stealing or looting
        private static bool m_SHActive = true;
        // Maximum chance to stay hidden
        private static double m_SHMaxChance = 90.0;
        // Minimum chance to stay hidden
        private static double m_SHMinChance = 20.0;
        // Minimum skill requiered to activate HL system
        private static double m_SHReqSkill = 50.0;

        public static bool StayHidden(Mobile from)
        {
            return StayHidden(from, false);
        }

        public static bool StayHidden(Mobile from, bool active)
        {
            bool stayHidden = InternalStayHidden(from);

            if (active && !stayHidden)
            {
                from.RevealingAction();
            }

            return stayHidden;
        }

        private static bool InternalStayHidden(Mobile from)
        {
            // Check if mobile is hidden, if not skip
            if (!from.Hidden)
            {
                return false;
            }

            // Check if system is active
            if (!m_SHActive)
            {
                return false;
            }

            // Check requiered skill
            if (from.Skills[SkillName.Hiding].Value < m_SHReqSkill || from.Skills[SkillName.Stealth].Value < m_SHReqSkill)
            {
                return false;
            }

            // Calculate chance
            double hSkill = ((from.Skills[SkillName.Hiding].Value
                + from.Skills[SkillName.Hiding].Value) / 2
                - m_SHReqSkill) / (100 - m_SHReqSkill)
                * (m_SHMaxChance - m_SHMinChance)
                + m_SHMinChance;

            return !(hSkill < Utility.Random(100));
        }

        #endregion

        #region Ninja Loot

        // NL: Ninja Loot system
        // System to determine chance to make a ninja loot
        // Base caught chance
        private static double m_NLBase = 100.0;
        // Stealing skill ratio
        private static double m_NLStealingRatio = 0.8;
        // Dex ratio
        private static double m_NLDexRatio = 0.1;
        // Hidden bonus
        private static double m_NLHiddenBonus = 5.0;
        // Mounted malus
        private static double m_NLMountedMalus = 50.0;
        // Equiped malus
        private static double m_NLEquipedMalus = 50.0;

        public static bool NinjaLoot(Mobile from, Item toSteal)
        {
            double chance = m_NLBase;

            // Add malus/bonus
            // Mounted ?
            if (from.Mounted)
            {
                chance += m_NLMountedMalus;
            }
            
            // Hidden ?
            if (from.Hidden)
            {
                chance -= m_NLHiddenBonus;
            }

            // Stealing
            chance -= from.Skills[SkillName.Stealing].Value * m_NLStealingRatio;

            // Dex
            chance -= from.Dex * m_NLDexRatio;

            // Check if item is equiped
            if (toSteal.Parent is Mobile)
            {
                chance += m_NLEquipedMalus;
            }

            // Weight malus
            if (toSteal.Weight > 10)
            {
                chance *= (1 - (toSteal.Weight - 10) / 10);
            }

            return !(chance > Utility.Random(100));
        }

        #endregion
    }
}
