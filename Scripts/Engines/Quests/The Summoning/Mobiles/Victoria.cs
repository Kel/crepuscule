using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.ContextMenus;
using Server.Engines.Quests;
using Server.Engines.Quests.Necro;

namespace Server.Engines.Quests.Doom
{
	public class Victoria : BaseQuester
	{
		public override int TalkNumber{ get{ return 6159; } } // Ask about Chyloth
		public override bool ClickTitle{ get{ return true; } }
		public override bool IsActiveVendor{ get{ return true; } }
		public override bool DisallowAllMoves{ get{ return false; } }

		[Constructable]
		public Victoria() : base( "the Sorceress" )
		{
		}

		public Victoria( Serial serial ) : base( serial )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Female = true;
			Hue = 0x8835;
			Body = 0x191;

			Name = "Victoria";
		}

		private SummoningAltar m_Altar;

		private const int AltarRange = 24;

		public SummoningAltar Altar
		{
			get
			{
				if ( m_Altar == null || m_Altar.Deleted || m_Altar.Map != this.Map || !Utility.InRange( m_Altar.Location, this.Location, AltarRange ) )
				{
					foreach ( Item item in GetItemsInRange( AltarRange ) )
					{
						if ( item is SummoningAltar )
						{
							m_Altar = (SummoningAltar)item;
							break;
						}
					}
				}

				return m_Altar;
			}
		}

		public override void InitOutfit()
		{
			EquipItem( new GrandGrimoire() );

			EquipItem( SetHue( new Sandals(), 0x455 ) );
			EquipItem( SetHue( new SkullCap(), 0x455 ) );
			EquipItem( SetHue( new PlainDress(), 0x455 ) );

			EquipItem( SetHue( new LongHair(), 0x482 ) );
		}


		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}