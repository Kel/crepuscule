using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Collector
{
	public class AlbertaGiacco : BaseQuester
	{
		[Constructable]
		public AlbertaGiacco() : base( "the respected painter" )
		{
		}

		public AlbertaGiacco( Serial serial ) : base( serial )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x83F2;

			Female = true;
			Body = 0x191;
			Name = "Alberta Giacco";
		}

		public override void InitOutfit()
		{
			AddItem( new FancyShirt() );
			AddItem( new Skirt( 0x59B ) );
			AddItem( new Boots() );
			AddItem( new FeatheredHat( 0x59B ) );
			AddItem( new FullApron( 0x59B ) );

			AddItem( new PonyTail( 0x457 ) );
		}

		public override bool CanTalkTo( PlayerMobile to )
		{
            return false;
		}

		public override void OnTalk( PlayerMobile player, bool contextMenu )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}