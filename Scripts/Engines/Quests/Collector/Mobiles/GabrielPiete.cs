using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Collector
{
	public class GabrielPiete : BaseQuester
	{
		[Constructable]
		public GabrielPiete() : base( "the renowned minstrel" )
		{
		}

		public GabrielPiete( Serial serial ) : base( serial )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x83EF;

			Female = false;
			Body = 0x190;
			Name = "Gabriel Piete";
		}

		public override void InitOutfit()
		{
			AddItem( new FancyShirt() );
			AddItem( new LongPants( 0x5F7 ) );
			AddItem( new Shoes( 0x5F7 ) );

			AddItem( new TwoPigTails( 0x460 ) );
			AddItem( new Mustache( 0x460 ) );
		}

		public override bool CanTalkTo( PlayerMobile to )
		{
            return false;
		}

		public override void OnTalk( PlayerMobile player, bool contextMenu )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}