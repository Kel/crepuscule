using System;
using Server;
using Server.Mobiles;
using Server.Targeting;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Collector
{
	public class EnchantedPaints : QuestItem
	{
		[Constructable]
		public EnchantedPaints() : base( 0xFC1 )
		{
			LootType = LootType.Blessed;

			Weight = 1.0;
		}

		public EnchantedPaints( Serial serial ) : base( serial )
		{
		}

		public override bool CanDrop( PlayerMobile player )
		{
            return false;
		}
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}