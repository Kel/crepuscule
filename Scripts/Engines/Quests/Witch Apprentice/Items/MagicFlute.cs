using System;
using Server;
using Server.Mobiles;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Hag
{
	public class MagicFlute: CrepusculeItem
	{
		public override int LabelNumber{ get{ return 1055051; } } // magic flute

		[Constructable]
		public MagicFlute() : base( 0x1421 )
		{
			Hue = 0x8AB;
		}

		public override void OnDoubleClick( Mobile from )
		{
			if ( !IsChildOf( from.Backpack ) )
			{
				SendLocalizedMessageTo( from, 1042292 ); // You must have the object in your backpack to use it.
				return;
			}

			from.PlaySound( 0x3D );
		}

		public MagicFlute( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}