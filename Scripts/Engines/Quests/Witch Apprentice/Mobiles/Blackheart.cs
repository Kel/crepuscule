using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Hag
{
	public class Blackheart : BaseQuester
	{
		[Constructable]
		public Blackheart() : base( "the Drunken Pirate" )
		{
		}

		public Blackheart( Serial serial ) : base( serial )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x83EF;

			Female = false;
			Body = 0x190;
			Name = "Captain Blackheart";
		}

		public override void InitOutfit()
		{
			AddItem( new FancyShirt() );
			AddItem( new LongPants( 0x66D ) );
			AddItem( new ThighBoots() );
			AddItem( new TricorneHat( 0x1 ) );
			AddItem( new BodySash( 0x66D ) );

			LeatherGloves gloves = new LeatherGloves();
			gloves.Hue = 0x66D;
			AddItem( gloves );

			AddItem( new LongBeard( 0x455 ) );

			Item sword = new Cutlass();
			sword.Movable = false;
			AddItem( sword );
		}


		private void Heave()
		{
			this.PublicOverheadMessage( Network.MessageType.Regular, 0x3B2, 500849 ); // *hic*

			Timer.DelayCall( TimeSpan.FromSeconds( Utility.RandomMinMax( 60, 180 ) ), new TimerCallback( Heave ) );
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			Heave();
		}
	}
}