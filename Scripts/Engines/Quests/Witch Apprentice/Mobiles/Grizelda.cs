using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Hag
{
	public class Grizelda : BaseQuester
	{
		public override bool ClickTitle{ get{ return true; } }

		[Constructable]
		public Grizelda() : base( "the Hag" )
		{
		}

		public Grizelda( Serial serial ) : base( serial )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x83EA;

			Female = true;
			Body = 0x191;
			Name = "Grizelda";
		}

		public override void InitOutfit()
		{
			AddItem( new Robe( 0x1 ) );
			AddItem( new Sandals() );
			AddItem( new WizardsHat( 0x1 ) );
			AddItem( new GoldBracelet() );

			AddItem( new LongHair( 0x0 ) );

			Item staff = new GnarledStaff();
			staff.Movable = false;
			AddItem( staff );
		}

		public override void OnTalk( PlayerMobile player, bool contextMenu )
		{
			
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}