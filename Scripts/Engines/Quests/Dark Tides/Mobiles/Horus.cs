using System;
using System.Collections;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Network;
using Server.ContextMenus;
using Server.Engines.Quests;
using Server.Engines.Quests.Necro;
using System.Collections.Generic;

namespace Server.Engines.Quests.Necro
{
	public class Horus : BaseQuester
	{
		[Constructable]
		public Horus() : base( "the Guardian" )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x83F3;
			Body = 0x190;

			Name = "Horus";
		}

		public override void InitOutfit()
		{
			AddItem( SetHue( new PlateLegs(), 0x849 ) );
			AddItem( SetHue( new PlateChest(), 0x849 ) );
			AddItem( SetHue( new PlateArms(), 0x849 ) );
			AddItem( SetHue( new PlateGloves(), 0x849 ) );
			AddItem( SetHue( new PlateGorget(), 0x849 ) );

			AddItem( SetHue( new Bardiche(), 0x482 ) );

			AddItem( SetHue( new Boots(), 0x001 ) );
			AddItem( SetHue( new Cloak(), 0x482 ) );

			AddItem( Server.Items.Hair.CreateByID( Utility.RandomList( 0x203B, 0x203C, 0x203D, 0x2044, 0x2045, 0x2047, 0x2049, 0x204A ), 0x45D ) );
			AddItem( Server.Items.Beard.CreateByID( Utility.RandomList( 0x203E, 0x203F, 0x2040, 0x2041, 0x204B, 0x204C, 0x204D ), 0x45D ) );
		}

		public override int GetAutoTalkRange( PlayerMobile m )
		{
			return 3;
		}




		private class SpeakPasswordEntry : ContextMenuEntry
		{
			private Horus m_Horus;
			private PlayerMobile m_From;

			public SpeakPasswordEntry( Horus horus, PlayerMobile from, bool enabled ) : base( 6193, 3 )
			{
				m_Horus = horus;
				m_From = from;

				if ( !enabled )
					Flags |= CMEFlags.Disabled;
			}

			public override void OnClick()
			{

			}
		}

		public Horus( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}