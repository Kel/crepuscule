using System;
using Server;
using Server.Gumps;
using Server.Items;
using Server.Mobiles;
using Server.Engines.Quests;
using Server.Engines.Quests.Necro;

namespace Server.Engines.Quests.Necro
{
	public class Mardoth : BaseQuester
	{
		[Constructable]
		public Mardoth() : base( "the Ancient Necromancer" )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x8849;
			Body = 0x190;

			Name = "Mardoth";
		}

		public override void InitOutfit()
		{
			AddItem( new Sandals( 0x1 ) );
			AddItem( new Robe( 0x66D ) );
			AddItem( new BlackStaff() );
			AddItem( new WizardsHat( 0x1 ) );

			AddItem( new Mustache( 0x482 ) );
			AddItem( new LongHair( 0x482 ) );

			Item gloves = new BoneGloves();
			gloves.Hue = 0x66D;
			AddItem( gloves );

			Item gorget = new PlateGorget();
			gorget.Hue = 0x1;
			AddItem( gorget );
		}

		public override int GetAutoTalkRange( PlayerMobile m )
		{
			return 3;
		}

		public override void OnMovement( Mobile m, Point3D oldLocation )
		{
			base.OnMovement( m, oldLocation );

			if ( m is PlayerMobile && !m.Frozen && !m.Alive && InRange( m, 4 ) && !InRange( oldLocation, 4 ) && InLOS( m ) )
			{
				if ( m.Map == null || !m.Map.CanFit( m.Location, 16, false, false ) )
				{
					m.SendLocalizedMessage( 502391 ); // Thou can not be resurrected there!
				}
				else
				{
					Direction = GetDirectionTo( m );

					m.PlaySound( 0x214 );
					m.FixedEffect( 0x376A, 10, 16 );

					m.CloseGump( typeof( ResurrectGump ) );
					m.SendGump( new ResurrectGump( m, ResurrectMessage.Healer ) );
				}
			}
		}

		public Mardoth( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

        public override void OnTalk(PlayerMobile player, bool contextMenu)
        {
            
        }
    }
}