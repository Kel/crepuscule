using System;
using Server;
using Server.Items;
using Server.Mobiles;

namespace Server.Engines.Quests.Necro
{
	public class KronusScrollBox : MetalBox
	{
		[Constructable]
		public KronusScrollBox()
		{
			ItemID = 0xE80;
			Movable = false;

			for ( int i = 0; i < 40; i++ )
			{
				Item scroll = Loot.RandomScroll();
				scroll.Movable = false;
				DropItem( scroll );
			}
		}

		public KronusScrollBox( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}