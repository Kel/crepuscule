using System;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Engines.Quests;
using Server.Engines.Quests.Necro;

namespace Server.Engines.Quests.Necro
{
	public class DarkTidesTeleporter : DynamicTeleporter
	{
		[Constructable]
		public DarkTidesTeleporter()
		{
		}

		public override bool GetDestination( PlayerMobile player, ref Point3D loc, ref Map map )
		{
			

			return false;
		}

		public DarkTidesTeleporter( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}