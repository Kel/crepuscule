using System;
using Server;
using Server.Mobiles;
using Server.Items;
using Server.Gumps;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Haven
{
	public class Uzeraan : BaseQuester
	{
		[Constructable]
		public Uzeraan() : base( "the Conjurer" )
		{
		}

		public override void InitBody()
		{
			InitStats( 100, 100, 25 );

			Hue = 0x83F3;

			Female = false;
			Body = 0x190;
			Name = "Uzeraan";
		}

		public override void InitOutfit()
		{
			AddItem( new Robe( 0x4DD ) );
			AddItem( new WizardsHat( 0x8A5 ) );
			AddItem( new Shoes( 0x8A5 ) );

			AddItem( new LongHair( 0x455 ) );
			AddItem( new LongBeard( 0x455 ) );

			BlackStaff staff = new BlackStaff();
			staff.Movable = false;
			AddItem( staff );
		}

		public override int GetAutoTalkRange( PlayerMobile pm )
		{
			return 3;
		}



		public override void OnMovement( Mobile m, Point3D oldLocation )
		{
			base.OnMovement( m, oldLocation );

			if ( m is PlayerMobile && !m.Frozen && !m.Alive && InRange( m, 4 ) && !InRange( oldLocation, 4 ) && InLOS( m ) )
			{
				if ( m.Map == null || !m.Map.CanFit( m.Location, 16, false, false ) )
				{
					m.SendLocalizedMessage( 502391 ); // Thou can not be resurrected there!
				}
				else
				{
					Direction = GetDirectionTo( m );

					m.PlaySound( 0x214 );
					m.FixedEffect( 0x376A, 10, 16 );

					m.CloseGump( typeof( ResurrectGump ) );
					m.SendGump( new ResurrectGump( m, ResurrectMessage.Healer ) );
				}
			}
		}

		public Uzeraan( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}