using System;
using Server;
using Server.Items;
using Server.Mobiles;
using Server.Engines.Quests;

namespace Server.Engines.Quests.Haven
{
	public class UzeraanTurmoilTeleporter : DynamicTeleporter
	{
		[Constructable]
		public UzeraanTurmoilTeleporter()
		{
		}


		public UzeraanTurmoilTeleporter( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}

        public override bool GetDestination(PlayerMobile player, ref Point3D loc, ref Map map)
        {
            throw new NotImplementedException();
        }
    }
}