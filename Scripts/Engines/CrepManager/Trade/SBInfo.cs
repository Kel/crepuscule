using System;
using System.Collections;
using Server.Items;
using System.Collections.Generic;
using Server.Engines;
using System.Linq;

namespace Server.Mobiles
{
	public abstract class SBInfo
	{
        public abstract IShopSellInfo SellInfo { get; }
        public abstract ArrayList BuyInfo { get; }


		public SBInfo()
		{
		}
	}
}