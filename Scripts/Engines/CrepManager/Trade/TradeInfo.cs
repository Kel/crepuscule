﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using System.Collections;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Achat/Vente")]
    public class TradesList
    {

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Listes")]
        public List<TradeInfo> Lists 
        { 
            get{ return ManagerConfig.Trades; }
            set{ ManagerConfig.Trades = value;}
        }


        #region Constructors & Serialization
        public TradesList()
        {
        }

        public override string ToString()
        {
            return "(Achat/Vente)";
        }
        #endregion

    }

    [PropertyObject]
    [Description("Catégorie")]
    public class TradeInfo : ISelfDeletion
    {
        public static Dictionary<string, GenericSellInfo> SellInfos = new Dictionary<string, GenericSellInfo>();
        public static Dictionary<string, ArrayList> BuyInfos = new Dictionary<string, ArrayList>();
        public string InternalName { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Titre")]
        public string Name { get; set; }

        public static void Initialize()
        {
            // Reset
            SellInfos = new Dictionary<string, GenericSellInfo>();
            BuyInfos = new Dictionary<string, ArrayList>();

            foreach (var type in ManagerConfig.Items.Keys)
            {
                var value = ManagerConfig.Items[type];

                // Player can buy this type of goods
                if (value.Buy.Buyable)
                {
                    foreach (var vendor in value.Buy.Vendors.Vendors)
                    {
                        if (!BuyInfos.ContainsKey(vendor.InternalName))
                            BuyInfos.Add(vendor.InternalName, new ArrayList());

                        // Not sure if a copy is really needed, removing might enhance perf significantly
                        var copy = new GenericBuyInfo(value.Name, value.Type, value.Buy.Price, value.Buy.Amount, value.Buy.MaxAmount, value.ItemID, value.Hue);
                        BuyInfos[vendor.InternalName].Add(copy);
                    }
                }

                // Player can sell this type of goods
                if (value.Sell.Sellable)
                {
                    foreach (var vendor in value.Sell.Vendors.Vendors)
                    {
                        if (!SellInfos.ContainsKey(vendor.InternalName))
                            SellInfos.Add(vendor.InternalName, new GenericSellInfo());

                        SellInfos[vendor.InternalName].Add(value.Type, value.Sell.Price);
                    }
                }

            }
        }

        public IShopSellInfo SellInfo
        {
            get
            {
                if (!SellInfos.ContainsKey(InternalName))
                    return new GenericSellInfo();

                return SellInfos[InternalName];
            }
        }
        public ArrayList BuyInfo
        {
            get
            {
                if (!BuyInfos.ContainsKey(InternalName))
                    return new ArrayList();

                var infos = new ArrayList();
                foreach (GenericBuyInfo value in BuyInfos[InternalName])
                {
                    // Copy is needed because of the changing amount
                    var copy = new GenericBuyInfo(value.Name, value.Type, value.Price, value.Amount, value.MaxAmount, value.ItemID, value.Hue);
                    infos.Add(copy);
                }

                return infos;
            }
        }


        #region Constructors & Serialization
        public TradeInfo()
            : base()
        {
            InternalName = Guid.NewGuid().ToString();
            Name = "Sans Titre";
        }

        public TradeInfo(GenericReader reader)
  
        {
            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        InternalName = reader.ReadString();
                        Name = reader.ReadString();
                        break;
                    }
            }
        }


        public virtual void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(InternalName);
            writer.Write(Name);

        }

        public override string ToString()
        {
            return String.Format("{0}, {1} objets", Name, BuyInfo.Count);
        }

        public static TradeInfo Find(string internalName)
        {
            return ManagerConfig.Trades
                .Where(trade => trade.InternalName == internalName)
                .FirstOrDefault();
        }

        #endregion

        #region ISelfDeletion Members

        public bool CanBeDeleted()
        {
            return  BuyInfo.Count == 0;
        }

        #endregion
    }
}


