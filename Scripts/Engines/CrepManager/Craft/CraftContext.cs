﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Server.Mobiles;
using Server.Gumps;
using Server.Items;

namespace Server.Engines.Craft
{
    #region Craft Context
    public enum ConsumeType
    {
        All, Half, None
    }


    public class CraftContext
    {
        internal static Dictionary<RacePlayerMobile, CraftContext> ContextTable = new Dictionary<RacePlayerMobile, CraftContext>();
        public Dictionary<int, GameItemType> SelectedResources = new Dictionary<int, GameItemType>();
        public GameItemType LastMadeItem { get; set; }
        public GameItemType LastMadeItemResource { get; set; }

        public bool IsInventionCategory { get; set; }
        public bool HasLastItem { get { return LastMadeItem != null && LastMadeItemResource != null; } }
        public bool DoNotColor { get; set; }
        public RacePlayerMobile Owner { get; set; }
        public GameCraftSystem System { get; set; }
        public GameCraftCategory Category { get; set; }
        public List<GameItemCraft> Items { get; set; }
        public List<int> Counters { get; set; }
        public int Page { get; set; }
        public BaseTool Tool { get; set; }

        public CraftContext(RacePlayerMobile owner, GameCraftSystem system, BaseTool tool) : this(owner, system, null, tool) { }
        public CraftContext(RacePlayerMobile owner, GameCraftSystem system):this(owner, system, null, null) { }
        public CraftContext(RacePlayerMobile owner, GameCraftSystem system, GameCraftCategory category, BaseTool tool)
        {
            Tool = tool;
            Owner = owner;
            System = system;
            Category = category;
            IsInventionCategory = false;

            Page = 0;
            if (category == null && System.Categories.Count > 0)
                Category = System.Categories[0];
            
            // For easy retrieval
            if (ContextTable.ContainsKey(owner))
                ContextTable[owner] = this;
            else
                ContextTable.Add(owner, this);

            RebuildCounters();
            RebuildList();
        }

        public void RebuildCounters()
        {
            // Rebuild counters
            if (Owner.AccessLevel >= AccessLevel.GameMaster)
            {
                Counters = System.Categories
                    .Select(c => c.Items.Count)
                    .ToList();
            }
            else
            {
                Counters = System.Categories
                    .Select(c => c.Items.Where(item => Owner.GetCraftXP(item.Parent.Type) >= 100 || item.Blueprint.Everyone).Count())
                    .ToList();
            }
        }

        public void RebuildList()
        {
            if (Owner.AccessLevel >= AccessLevel.GameMaster)
            {
                if (!IsInventionCategory)
                {
                    Items = Category.Items.ToList();
                }
            }
            else // Player
            {
                if (!IsInventionCategory)
                {
                    Items = Category.Items
                        .Where(item => item.Blueprint.Everyone || Owner.GetCraftXP(item.Parent.Type) >= 100)
                        .ToList();
                }
                else
                {
                    Items = System.Inventions
                        .Where(item => item.Craft.Blueprint.Inventable && Owner.GetCraftXP(item.Type) < 100)
                        .Select(item => item.Craft)
                        .ToList();
                }
            }
        }

        public void ShowGumpWithToolCheck()
        {
            if (Tool != null && !Tool.Deleted && Tool.UsesRemaining > 0)
                ShowGump();
        }

        public void ShowGumpWithToolCheck(object message)
        {
            if (Tool != null && !Tool.Deleted && Tool.UsesRemaining > 0)
                ShowGump(message);
            else if (message is int && (int)message > 0)
                Owner.SendLocalizedMessage((int)message);
            else if (message is string)
                Owner.SendMessage((string)message);
        }

        public void ShowGump()
        {

            Owner.CloseGump(typeof(GameCraftGump));
            Owner.SendGump(new GameCraftGump(this));
        }

        public void ShowGump(object message)
        {
            Owner.CloseGump(typeof(GameCraftGump));

            if (message is int && (int)message > 0)
                Owner.SendGump(new GameCraftGump(this, (int)message));
            else if (message is string)
                Owner.SendGump(new GameCraftGump(this, (string)message));
        }

        public void ShowPreviousPage()
        {
            Page -= 1;
            Owner.CloseGump(typeof(GameCraftGump));
            Owner.SendGump(new GameCraftGump(this));
        }

        public void ShowNextPage()
        {
            Page += 1;
            Owner.CloseGump(typeof(GameCraftGump));
            Owner.SendGump(new GameCraftGump(this));
        }

        public GameItemType GetSelectedResource(int index)
        {
            if (SelectedResources.ContainsKey(index))
                return SelectedResources[index];
            return null;
        }

        public void SelectResource(int index, GameItemType item)
        {
            if (SelectedResources.ContainsKey(index))
                SelectedResources[index] = item;
            else
                SelectedResources.Add(index, item);
        }

        public void OnMade(GameItemType item, GameItemType resource)
        {
            // Add crafting experience
            Owner.EvolutionInfo.CraftExperience.AddXP(item.Type, Utility.Random(10));

            LastMadeItem = item;
            LastMadeItemResource = resource;
        }
    }

    #endregion

    public class ForgeAttribute : Attribute 
    {
        public ForgeAttribute()
        {
        }
    }

    public class AnvilAttribute : Attribute 
    {
        public AnvilAttribute()
        {
        }
    }

    public enum CraftECA 
    {
        ChanceMinusSixty,
        FiftyPercentChanceMinusTenPercent,
        ChanceMinusSixtyToFourtyFive
    }
}