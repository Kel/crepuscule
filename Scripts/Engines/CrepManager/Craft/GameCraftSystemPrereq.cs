﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Prérequis")]
    public class GameCraftSystemPrereq
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Chaleur?")]
        public bool NeedHeat { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Four?")]
        public bool NeedOven { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Forge?")]
        public bool NeedAnvilAndForge { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Outil Equipé?")]
        public bool NeedEquippedTool { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Utiliser Outil?")]
        public bool NeedUseTool { get; set; }

        #region Constructors & Serialization
        public GameCraftSystemPrereq() 
        {

        }
        public GameCraftSystemPrereq(GenericReader reader)
        {

            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        NeedHeat = reader.ReadBool();
                        NeedOven = reader.ReadBool();
                        NeedAnvilAndForge = reader.ReadBool();
                        NeedEquippedTool = reader.ReadBool();
                        NeedUseTool = reader.ReadBool();
                        break;
                    }
            }
        }


        public virtual void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(NeedHeat);
            writer.Write(NeedOven);
            writer.Write(NeedAnvilAndForge);
            writer.Write(NeedEquippedTool);
            writer.Write(NeedUseTool);
        }

        public override string ToString()
        {
            return String.Format("(Prérequis)");
        }
        #endregion

    }
}
