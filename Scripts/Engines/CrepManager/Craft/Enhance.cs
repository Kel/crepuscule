using System;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Mobiles;

namespace Server.Engines.Craft
{
    public enum EnhanceResult
    {
        NotInBackpack,
        BadItem,
        BadResource,
        AlreadyEnhanced,
        Success,
        Failure,
        Broken,
        NoResources,
        NoSkill
    }

    public class Enhance
    {
        public static void Do(Mobile from, GameCraftSystem system, BaseTool tool)
        {
            BeginTarget(from, system, tool);
        }

        public static void BeginTarget(Mobile from, GameCraftSystem craftSystem, BaseTool tool)
        {
            var context = craftSystem.GetContext(from);

            if (context == null)
                return;


            from.Target = new ResourceTarget(craftSystem, tool);
            from.SendMessage("Selectionnez la ressource � utiliser pour l'am�lioration");
        }


        public static EnhanceResult Invoke(Mobile from, GameCraftSystem craftSystem, BaseTool tool,
            CrepusculeItem item, GameItemType resource, ref object resMessage)
        {
            int maxAmount = 1;
            if (item == null)
                return EnhanceResult.BadItem;

            if (!item.IsChildOf(from.Backpack))
                return EnhanceResult.NotInBackpack;

            if (!(item is BaseArmor) && !(item is BaseWeapon))
                return EnhanceResult.BadItem;

            var itemType = GameItemType.FindType(item);
            if (itemType == null)
                return EnhanceResult.BadItem;

            if (resource == null || resource.Craft.IsStandard)
                return EnhanceResult.BadResource;

            var craftItem = craftSystem.CraftItems.SearchFor(item.GetType());
            if (craftItem == null || craftItem.Ressource1 == null)
                return EnhanceResult.BadItem;

            int quality = 0;
            if (!craftItem.CheckSkills(from, resource, craftSystem, ref quality, false))
                return EnhanceResult.NoSkill;

            // resourse match?
            if (!resource.Craft.ParentRessource.Equals(craftItem.Ressource1.Type))
                return EnhanceResult.BadResource;

            if (item.CraftResourceType != null && !item.CraftResourceType.Craft.IsStandard)
                return EnhanceResult.AlreadyEnhanced;

            // get attribute infos
            CraftAttributeInfo attributes = resource.Craft.CraftAttributes;
            if (attributes == null || attributes.IsBlank)
                return EnhanceResult.BadResource;

            if (!craftItem.Consume.ConsumeRes(from, craftSystem, ConsumeType.None, resource, ref maxAmount, ref resMessage))
                return EnhanceResult.NoResources;

            int phys = 0, fire = 0, cold = 0, pois = 0, nrgy = 0;
            int dura = 0, luck = 0, lreq = 0, dinc = 0;
            int baseChance = 0;

            bool physBonus = false;
            bool fireBonus = false;
            bool coldBonus = false;
            bool nrgyBonus = false;
            bool poisBonus = false;
            bool duraBonus = false;
            bool luckBonus = false;
            bool lreqBonus = false;
            bool dincBonus = false;

            if (item is BaseWeapon)
            {
                BaseWeapon weapon = (BaseWeapon)item;

                if(!weapon.Attributes.IsEmpty)
                    return EnhanceResult.AlreadyEnhanced;

                baseChance = 20;

                dura = weapon.MaxHits;
                luck = weapon.Attributes.Luck;
                lreq = weapon.WeaponAttributes.LowerStatReq;
                dinc = weapon.Attributes.WeaponDamage;

                fireBonus = (attributes.WeaponFireDamage > 0);
                coldBonus = (attributes.WeaponColdDamage > 0);
                nrgyBonus = (attributes.WeaponEnergyDamage > 0);
                poisBonus = (attributes.WeaponPoisonDamage > 0);

                duraBonus = (attributes.WeaponDurability > 0);
                luckBonus = (attributes.WeaponLuck > 0);
                lreqBonus = (attributes.WeaponLowerRequirements > 0);
                dincBonus = (dinc > 0);
            }
            else
            {
                BaseArmor armor = (BaseArmor)item;

                if (!armor.Attributes.IsEmpty)
                    return EnhanceResult.AlreadyEnhanced;

                baseChance = 20;

                phys = armor.PhysicalResistance;
                fire = armor.FireResistance;
                cold = armor.ColdResistance;
                pois = armor.PoisonResistance;
                nrgy = armor.EnergyResistance;

                dura = armor.MaxHitPoints;
                luck = armor.Attributes.Luck;
                lreq = armor.ArmorAttributes.LowerStatReq;

                physBonus = (attributes.ArmorPhysicalResist > 0);
                fireBonus = (attributes.ArmorFireResist > 0);
                coldBonus = (attributes.ArmorColdResist > 0);
                nrgyBonus = (attributes.ArmorEnergyResist > 0);
                poisBonus = (attributes.ArmorPoisonResist > 0);

                duraBonus = (attributes.ArmorDurability > 0);
                luckBonus = (attributes.ArmorLuck > 0);
                lreqBonus = (attributes.ArmorLowerRequirements > 0);
                dincBonus = false;
            }

            
            int skill = from.Skills[craftSystem.MainSkill].Fixed / 10;

            if (skill >= 100)
                baseChance -= (skill - 90) / 10;

            EnhanceResult res = EnhanceResult.Success;

            if (physBonus)
                CheckResult(ref res, baseChance + phys);

            if (fireBonus)
                CheckResult(ref res, baseChance + fire);

            if (coldBonus)
                CheckResult(ref res, baseChance + cold);

            if (nrgyBonus)
                CheckResult(ref res, baseChance + nrgy);

            if (poisBonus)
                CheckResult(ref res, baseChance + pois);

            if (duraBonus)
                CheckResult(ref res, baseChance + (dura / 40));

            if (luckBonus)
                CheckResult(ref res, baseChance + 10 + (luck / 2));

            if (lreqBonus)
                CheckResult(ref res, baseChance + (lreq / 4));

            if (dincBonus)
                CheckResult(ref res, baseChance + (dinc / 4));

            switch (res)
            {
                case EnhanceResult.Broken:
                    {
                        if (!craftItem.Consume.ConsumeRes(from, craftSystem, ConsumeType.Half, resource, ref maxAmount, ref resMessage))
                            return EnhanceResult.NoResources;

                        item.Delete();
                        break;
                    }
                case EnhanceResult.Success:
                    {
                        if (!craftItem.Consume.ConsumeRes(from, craftSystem, ConsumeType.All, resource, ref maxAmount, ref resMessage))
                            return EnhanceResult.NoResources;

                        if (item is BaseWeapon)
                            ((BaseWeapon)item).CraftResource = resource.Type;
                        else
                            ((BaseArmor)item).CraftResource = resource.Type;

                        // Enhancing adds xp
                        var player = from as RacePlayerMobile;
                        if(player != null)
                            player.AddCraftXP(itemType.Type, Utility.Random(4));

                        break;
                    }
                case EnhanceResult.Failure:
                    {
                        if (!craftItem.Consume.ConsumeRes(from, craftSystem, ConsumeType.Half, resource, ref maxAmount, ref resMessage))
                            return EnhanceResult.NoResources;

                        break;
                    }
            }

            return res;
        }

        public static void CheckResult(ref EnhanceResult res, int chance)
        {
            if (res != EnhanceResult.Success)
                return; // we've already failed..

            int random = Utility.Random(100);

            if (10 > random)
                res = EnhanceResult.Failure;
            else if (chance > random)
                res = EnhanceResult.Broken;
        }

        private class ResourceTarget : Target
        {
            private GameCraftSystem m_CraftSystem;
            private BaseTool m_Tool;
            private Type m_ResourceType;

            public ResourceTarget(GameCraftSystem craftSystem, BaseTool tool)
                : base(2, false, TargetFlags.None)
            {
                m_CraftSystem = craftSystem;
                m_Tool = tool;
            }

            protected override void OnTarget(Mobile from, object targeted)
            {
                if (targeted is Item && targeted != null)
                {
                    var type = GameItemType.FindType(targeted.GetType());
                    if (type != null && !type.Craft.IsStandard)
                    {
                        from.Target = new InternalTarget(m_CraftSystem, m_Tool, type);
                        from.SendLocalizedMessage(1061004); // Target an item to enhance with the properties of your selected material.
                    }
                    else
                    {
                        from.SendLocalizedMessage(1061010);  // You must select a special material in order to enhance an item with its properties.
                        m_CraftSystem.GetContext(from).ShowGump();
                    }
                }
                else
                {
                    from.SendLocalizedMessage(1061010);  // You must select a special material in order to enhance an item with its properties.
                    m_CraftSystem.GetContext(from).ShowGump();
                }
            }

            private class InternalTarget : Target
            {
                private GameCraftSystem m_CraftSystem;
                private BaseTool m_Tool;
                private GameItemType m_SelectedResource;

                public InternalTarget(GameCraftSystem craftSystem, BaseTool tool, GameItemType selectedResource)
                    : base(2, false, TargetFlags.None)
                {
                    m_CraftSystem = craftSystem;
                    m_Tool = tool;
                    m_SelectedResource = selectedResource;
                }

                protected override void OnTarget(Mobile from, object targeted)
                {
                    if (targeted is CrepusculeItem)
                    {

                        object message = null;
                        EnhanceResult res = Enhance.Invoke(from, m_CraftSystem, m_Tool, (CrepusculeItem)targeted, m_SelectedResource, ref message);

                        switch (res)
                        {
                            case EnhanceResult.NotInBackpack: message = 1061005; break; // The item must be in your backpack to enhance it.
                            case EnhanceResult.AlreadyEnhanced: message = 1061012; break; // This item is already enhanced with the properties of a special material.
                            case EnhanceResult.BadItem: message = 1061011; break; // You cannot enhance this type of item with the properties of the selected special material.
                            case EnhanceResult.BadResource: message = 1061010; break; // You must select a special material in order to enhance an item with its properties.
                            case EnhanceResult.Broken: message = 1061080; break; // You attempt to enhance the item, but fail catastrophically. The item is lost.
                            case EnhanceResult.Failure: message = 1061082; break; // You attempt to enhance the item, but fail. Some material is lost in the process.
                            case EnhanceResult.Success: message = 1061008; break; // You enhance the item with the properties of the special material.
                            case EnhanceResult.NoSkill: message = 1044153; break; // You don't have the required skills to attempt this item.
                        }

                        if(message is int)
                            from.SendLocalizedMessage((int)message);
                        m_CraftSystem.GetContext(from).ShowGump();
                    }
                }
            }
        }
    }
}