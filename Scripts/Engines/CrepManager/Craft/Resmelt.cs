using System;
using Server;
using Server.Targeting;
using Server.Items;
using Server.Mobiles;

namespace Server.Engines.Craft
{
	public class Resmelt
	{
		public Resmelt()
		{
		}

		public static void Do( Mobile from, GameCraftSystem craftSystem, BaseTool tool )
		{
            if (!craftSystem.CanCraft(from, tool, null))
			{
                craftSystem.GetContext(from).ShowGump();
			}
			else
			{
				from.Target = new InternalTarget( craftSystem, tool );
				from.SendLocalizedMessage( 1044273 ); // Target an item to recycle.
			}
		}

		private class InternalTarget : Target
		{
            private GameCraftSystem m_CraftSystem;
			private BaseTool m_Tool;

            public InternalTarget(GameCraftSystem craftSystem, BaseTool tool) : base(2, false, TargetFlags.None)
			{
				m_CraftSystem = craftSystem;
				m_Tool = tool;
			}

            protected override void OnTarget(Mobile from, object targeted)
            {
                if (!m_CraftSystem.CanCraft(from, m_Tool, null))
                {
                    m_CraftSystem.GetContext(from).ShowGump();
                }
                else
                {
                    bool success = false;
                    bool isStoreBought = false;

                    if(targeted is CrepusculeItem)
                        success = Resmelt(from, (CrepusculeItem)targeted);

                    if (targeted is BaseArmor)
                    {
                        isStoreBought = !((BaseArmor)targeted).PlayerConstructed;
                    }
                    else if (targeted is BaseWeapon)
                    {
                        isStoreBought = !((BaseWeapon)targeted).PlayerConstructed;
                    }

                    if (success)
                    {
                        from.SendLocalizedMessage(isStoreBought ? 500418 : 1044270); // You melt the item down into ingots.
                        m_CraftSystem.GetContext(from).ShowGump();
                    }
                    else
                    {
                        from.SendLocalizedMessage(1044272); // You can't melt that down into ingots.
                        m_CraftSystem.GetContext(from).ShowGump();
                    }
                }
            }

			private bool Resmelt( Mobile from, CrepusculeItem item )
			{
				try
				{
                    if (item.CraftResourceType == null)
                        return false;

                    var typeofItem = item.GetType();
                    var itemType = GameItemType.FindType(typeofItem);
                    var resourceType = item.CraftResourceType;
                    if (itemType == null || !itemType.Craft.Resmeltable)
                        return false;

                    // The item type has resource attached
                    var craftResource = itemType.Craft.Ressource1;
                    if (craftResource == null)
						return false;

                    // Make sure the item we want to smelt is in the menu
                    if (m_CraftSystem.CraftItems.SearchFor(typeofItem) == null)
						return false;

                    // Check the amount
                    if (craftResource.Amount < 2)
                        return false; // Not enough metal to resmelt

                    Item ingot = GameItemType.CreateItem(item.CraftResource);

                    if (ingot != null &&
                        ((item is BaseArmor && ((BaseArmor)item).PlayerConstructed)
                        || (item is BaseWeapon && ((BaseWeapon)item).PlayerConstructed)
                        || (item is BaseClothing && ((BaseClothing)item).PlayerConstructed)))
                    {
                        var player  = from as RacePlayerMobile;
                        var percent = player.GetCraftXPPercentage(typeofItem);

                        // TODO: Improve, give more xp advantages
                        if(percent > 80) ingot.Amount = craftResource.Amount ;
                        else ingot.Amount = craftResource.Amount / 2;

                        // Resmelting adds XP, yay! Kel likes melting stuff!
                        player.AddCraftXP(typeofItem, 1);

                    }
                    else
                        ingot.Amount = 1;

					item.Delete();
					from.AddToBackpack( ingot );

					from.PlaySound( 0x2A );
					from.PlaySound( 0x240 );
					return true;
				}
				catch
				{
				}

				return false;
			}


		}
	}
}