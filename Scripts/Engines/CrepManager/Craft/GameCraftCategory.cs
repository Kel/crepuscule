﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines.Craft
{

    [PropertyObject]
    [Description("Sous-Catégorie")]
    public class GameCraftCategory : ISelfDeletion
    {
        public string InternalName { get; set; }
        public List<GameItemCraft> Items { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom")]
        public string Name { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Total Objets")]
        public int TotalItems { get { return Items.Count; } }

        #region Constructors & Serialization
        public GameCraftCategory()
        {
            InternalName = Guid.NewGuid().ToString();
            Items = new List<GameItemCraft>();
            Name = "Nouvelle Catégorie";
        }

        public GameCraftCategory(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        Items = new List<GameItemCraft>();
                        InternalName = reader.ReadString();
                        Name = reader.ReadString();
                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(InternalName);
            writer.Write(Name);
        }

        public override string ToString()
        {
            return String.Format("{0}, {1} objets", Name, TotalItems);
        }

        #endregion

        #region ISelfDeletion Members

        public bool CanBeDeleted()
        {
            return TotalItems == 0;
        }

        #endregion
    }
}
