﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;

namespace Server.Engines.Craft
{
    [PropertyObject]
    [Description("Achat/Vente")]
    public class CraftSystemsList
    {

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Listes")]
        public List<GameCraftSystem> Lists
        {
            get { return ManagerConfig.CraftSystems; }
            set { ManagerConfig.CraftSystems = value; }
        }


        #region Constructors & Serialization
        public CraftSystemsList()
        {
        }

        public override string ToString()
        {
            return "(Craft)";
        }
        #endregion

    }

    [PropertyObject]
    [Description("Catégorie")]
    public class GameCraftSystem : GameCraftSystemPrereq, ISelfDeletion
    {
        public string InternalName { get; set; }
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Titre")]
        public string Name { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Réparer?")]
        public bool Repair { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Refondre?")]
        public bool Resmelt { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Améliorer?")]
        public bool Enhance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Capacité")]
        public SkillName MainSkill { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Sous-Catégories")]
        public List<GameCraftCategory> Categories { get; private set; }
        public List<GameItemType> Inventions { get; private set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Temps de fabrication")]
        public RangeValue CraftEffect { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Délais")]
        public double Delay { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Son")]
        public SoundEffect Sound { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("ECA")]
        public virtual CraftECA ECA { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Menu Craft")]
        public PropertyAction ShowGump
        {
            get { return new PropertyAction("(Montrer)"); }
            set
            {
                new CraftContext(value.Owner as RacePlayerMobile, this).ShowGump();
            }
        }

        #region Compatibility
        public CraftItems CraftItems
        {
            get
            {
                var result = new CraftItems();
                result.AddRange(Categories.SelectMany(cat => cat.Items));
                return result;
            }
        }


        public CraftContext GetContext(Mobile m)
        {
            if (m == null || m.Deleted)
                return null;


            var owner = m as RacePlayerMobile;
            if (owner != null && CraftContext.ContextTable.ContainsKey(owner))
            {
                var c = CraftContext.ContextTable[owner];
                if (c == null)
                    c = new CraftContext(owner, this);

                return c;
            }
            return null;
        }

        #endregion

        #region Constructors & Serialization
        public GameCraftSystem(): this(true)
        {

        }

        public GameCraftSystem(bool withEmptyOne) : base()
        {
            CraftEffect = new RangeValue();
            Categories = new List<GameCraftCategory>();
            Inventions = new List<GameItemType>();
            InternalName = Guid.NewGuid().ToString();
            Name = "Sans Titre";
            CraftEffect.Min = 1;
            CraftEffect.Max = 2;
            Delay = 1.25;
            ECA = CraftECA.ChanceMinusSixtyToFourtyFive;
            Sound = new SoundEffect(0x249);

            if(withEmptyOne)
                Categories.Add(new GameCraftCategory());
        }

        public GameCraftSystem(GenericReader reader) : base(reader)
        {
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    InternalName = reader.ReadString();
                    Name = reader.ReadString();
                    MainSkill = (SkillName)reader.ReadInt();
                    CraftEffect = reader.ReadRange();
                    Delay = reader.ReadDouble();
                    Sound = reader.ReadSound();
                    Repair = reader.ReadBool();
                    Resmelt = reader.ReadBool();
                    Enhance = reader.ReadBool();
                    ECA = (CraftECA)reader.ReadInt();

                    Categories = new List<GameCraftCategory>();
                    int catCount = reader.ReadInt();
                    for (int i = 0; i < catCount; i++)
                        Categories.Add(new GameCraftCategory(reader));

                    RebuildInventionList();
                    break;
                }
            }
        }
            

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); //version
            writer.Write(InternalName);
            writer.Write(Name);
            writer.Write((int)MainSkill);
            writer.Write(CraftEffect);
            writer.Write(Delay);
            writer.Write(Sound);
            writer.Write(Repair);
            writer.Write(Resmelt);
            writer.Write(Enhance);
            writer.Write((int)ECA);

            writer.Write(Categories.Count);
            foreach (var item in Categories)
                item.Serialize(writer);
        }

        public override string ToString()
        {
            return Name;
        }

        public void RebuildInventionList()
        {
            Inventions = Categories
                .SelectMany(c => c.Items)
                .Where(i => i.Blueprint.Inventable == true)
                .Select(i => i.Parent)
                .ToList();
        }

        public static GameCraftSystem Find(string internalName)
        {
            return ManagerConfig.CraftSystems
                .Where(system => system.InternalName == internalName)
                .FirstOrDefault();
        }

        public GameCraftCategory FindCategory(string internalName)
        {
            return Categories
                .Where(cat => cat.InternalName == internalName)
                .FirstOrDefault();
        }
        #endregion

        public virtual void PlayCraftEffect(Mobile from)
        {
            //if ( from.Body.Type == BodyType.Human && !from.Mounted )
            //	from.Animate( 9, 5, 1, true, false, 0 );

            if (Sound.Number > 0)
                from.PlaySound(Sound.Number);
        }

        public virtual object PlayEndingEffect(Mobile from, bool failed, bool lostMaterial, bool toolBroken, bool invent, int quality, GameItemType item, GameItemType typeRes)
        {

            if (toolBroken)
                from.SendLocalizedMessage(1044038); // You have worn out your tool

            if (failed)
            {
                var boomChance = Utility.RandomDouble();
                if (invent)
                {
                    int emote = Utility.Random(3);
                    if (emote == 0) from.PlaySound(from.Female ? 799 : 1071); // groan
                    if (emote == 1) from.PlaySound(from.Female ? 816 : 1090); // sigh
                    if (emote == 2) from.PlaySound(874); // grownl
                    if (emote == 3) from.PlaySound(from.Female ? 822 : 1096); // groan
                }
                if (IsSpell(item.Type))
                {
                    if (boomChance < 0.05)
                        DoFailDamage(from);

                    if (lostMaterial)
                        return 1044043; // You failed to create the item, and some of your materials are lost.
                    else
                        return 1044157; // You failed to create the item, but no materials were lost.
                }
                if (IsPotion(item.Type))
                {
                    from.AddToBackpack(new Bottle());

                    if (boomChance < 0.1)
                        DoFailDamage(from);

                    return 500287; // You fail to create a useful potion.
                }
                else
                {
                    if (lostMaterial)
                        return 1044043; // You failed to create the item, and some of your materials are lost.
                    else
                        return 1044157; // You failed to create the item, but no materials were lost.

                }
            }
            else
            {
                if (invent) from.PlaySound(from.Female ? 779 : 1050); // ahha

                if (IsPotion(item.Type))
                {
                    from.PlaySound(0x240); // Sound of a filling bottle
                    if (quality == -1)
                        return 1048136; // You create the potion and pour it into a keg.
                    else
                        return 500279; // You pour the potion into a bottle...
                }
                else
                {
                    if (quality == 0)
                        return 502785; // You were barely able to make this item.  It's quality is below average.
                    else if (quality == 2)
                        return 1044155; // You create an exceptional quality item.
                    else
                        return 1044154; // You create the item.
                }
            }
        }

        private static void DoFailDamage(Mobile from)
        {
            from.PlaySound(from.Female ? 812 : 1086);
            Effects.SendLocationEffect(new Point3D(from.X, from.Y, from.Z), from.Map, 0x3709, 17);
            from.MovingParticles(from, 0x36D4, 7, 0, false, true, 9502, 4019, 0x160);
            from.PlaySound(Core.AOS ? 0x15E : 0x44B);
            from.Damage(Utility.Random(5, 20));
        }

        private static Type typeofPotion = typeof(BasePotion);
        private static Type typeofSpell = typeof(ISpell);
        public static bool IsPotion(Type type)
        {
            return typeofPotion.IsAssignableFrom(type);
        }
        public static bool IsSpell(Type type)
        {
            return typeofSpell.IsAssignableFrom(type);
        }

        public virtual bool CanCraft(Mobile from, BaseTool tool, GameItemType itemType)
        {
            if (tool == null || tool.Deleted || tool.UsesRemaining < 0)
            {
                from.SendLocalizedMessage(1044038); // You have worn out your tool!
                return false;
            }
            if (!BaseTool.CheckAccessible(tool, from))
            {
                from.SendLocalizedMessage(1044263); // The tool must be on your person to use.
                return false;
            }
            if (NeedUseTool && !BaseTool.CheckTool(tool, from))
            {
                from.SendLocalizedMessage(1048146); // If you have a tool equipped, you must use that tool.
                return false;
            }
            if (NeedAnvilAndForge && ! GameItemCraft.CheckAnvilAndForge(from, 2))
            {
                from.SendLocalizedMessage(1044267); // You must be near an anvil and a forge to smith items.
                return false;
            }


            // TODO all other conditions, configurable of course

            return true;
        }

        #region GetHueFor
        /// <summary>
        /// Given a ressource, gets the hue for an item
        /// </summary>
        public static int GetHueFor(Item item, Type typeofRessource)
        {
            var typeofItem = item.GetType();
            if (ManagerConfig.Items.ContainsKey(typeofItem))
            {
                var type = ManagerConfig.Items[typeofItem];
                foreach (var res in type.Craft.Ressources)
                {
                    if (res.RetainsColorFrom)
                        return res.ItemType.Hue;
                }
            }

            if (typeofRessource != null && ManagerConfig.Items.ContainsKey(typeofRessource))
            {
                var type = ManagerConfig.Items[typeofRessource];
                return type.Hue;
            }
            return 0;
            
        }

        /// <summary>
        /// Given a ressource, gets the hue for an item
        /// </summary>
        public static int GetHueFor(Item item)
        {
            return GetHueFor(item, null);
        }
        #endregion

        #region ISelfDeletion Members

        public bool CanBeDeleted()
        {
            return (Categories != null && Categories.Count == 0);
        }

        #endregion
    }

    #region CraftItems
    public class CraftItems : List<GameItemCraft>
    {
        public CraftItems()
        {

        }

        public GameItemCraft SearchForSubclass(Type type)
        {
            for (int i = 0; i < Count; i++)
            {
                var craftItem = this[i];
                var itemType = craftItem.Parent.Type;
                if (itemType == type || type.IsSubclassOf(itemType))
                    return craftItem;
            }

            return null;
        }

        public GameItemCraft SearchFor(Type type)
        {
            for (int i = 0; i < Count; i++)
            {
                var craftItem = this[i];
                var itemType = craftItem.Parent.Type;
                if (itemType == type)
                    return craftItem;
            }
            return null;
        }

    }
    #endregion
}

