using Server.Items;
using System;
using Server;
using System.Collections.Generic;
using Server.Engines;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Mobiles;

namespace Server.Items
{
    [Flipable(0x14ED, 0x14EE)]
	public class Blueprint : CrepusculeItem, IScissorable
	{
        private Type m_BlueprintOf = null;

        [CommandProperty(AccessLevel.GameMaster)]
        public Type BlueprintOf
        {
            get { return m_BlueprintOf; }
            set
            {
                if (value != null)
                {
                    var itemType = GameItemType.FindType(value);
                    if(itemType != null)
                    {
                        m_BlueprintOf = value;
                        Name = "Schema de " + itemType.Name; 
                    }
                }
            }
        }

        [Constructable]
		public Blueprint() : base(0x14ED)
		{
            
		}

        public Blueprint(Serial serial)
            : base(serial)
		{
		}


        public override void OnDoubleClick(Mobile from)
        {
            var player = from as RacePlayerMobile;
            if (player != null && BlueprintOf != null)
            {
                var itemType = GameItemType.FindType(BlueprintOf);
                if(itemType != null)
                {
                    if (player.GetCraftXP(BlueprintOf) >= 100)
                    {
                        player.SendMessage("Vous connaissez d�j� �a.");
                    }
                    else
                    {
                        player.AddCraftXP(BlueprintOf, 100);
                        player.SendMessage("Vous apprenez un peu plus sur " + itemType.Name);
                        this.Delete();
                    }
                }
            }
        }

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
            writer.Write(m_BlueprintOf);
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
            m_BlueprintOf = reader.ReadType();
		}

	}
}
