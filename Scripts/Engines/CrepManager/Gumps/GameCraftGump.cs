﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines;
using Server.Engines.Craft;
using Server.Mobiles;

namespace Server.Gumps
{

    public class GameCraftGump : Gump
    {
        #region Constructors & Private fields
        private const int RightShift = 67;
        private const int ListEntries = 8;
        private const int LeftLineHeigth = 25;
        private int LastOptionIndex = 0;
        private CraftContext Context;
        private int LastMessageNumber = 0;
        private string LastMessageString ;

        
        public GameCraftGump(CraftContext context): base(0, 0)
        {
            Context = context;
            Context.Owner.CloseGump(typeof(TreeSelectGump));
            Initialize();
        }

        public GameCraftGump(CraftContext context, int lastMessage): base(0, 0)
        {
            LastMessageNumber = lastMessage;
            Context = context;
            Context.Owner.CloseGump(typeof(TreeSelectGump));
            Initialize();
        }

        public GameCraftGump(CraftContext context, string lastMessage)
            : base(0, 0)
        {
            LastMessageString = lastMessage;
            Context = context;
            Context.Owner.CloseGump(typeof(TreeSelectGump));
            Initialize();
        }

        private enum InternalButton
        {
            Repair = 200,
            Resmelt = 201,
            Enhance = 202,
            MakeLast = 203,
            Invent = 204
        }
        #endregion

        private void Initialize()
        {
            this.Closable = true;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);
            this.AddBackground(52, 5, 737, 589, 9270);
            this.AddBackground(68, 52, 210, 354, 3500);
            this.AddBackground(66, 399, 211, 178, 3500);
            this.AddBackground(70, 19, 208, 31, 9350);
            this.AddLabel(82, 24, 0, Context.System.Name);
            try
            {

                // Categories
                var categoriesCount = Context.System.Categories.Count;
                for (int i = 0; i < categoriesCount; i++)
                {
                    var category = Context.System.Categories[i];
                    AddCategoryItem(i, category.Name, Context.Counters[i], 100 + i);
                }

                if(Context.Owner.AccessLevel == AccessLevel.Player)
                    AddCategoryItem(categoriesCount, "Inventions", -1, (int)InternalButton.Invent);

                // Options
                if (Context.Tool != null)
                {
                    if (Context.System.Repair) AddOptionItem("Réparer ", (int)InternalButton.Repair);
                    if (Context.System.Resmelt) AddOptionItem("Fondre", (int)InternalButton.Resmelt);
                    if (Context.System.Enhance) AddOptionItem("Améliorer", (int)InternalButton.Enhance);
                    if (Context.HasLastItem) AddOptionItem("Fabriquer le dernier", (int)InternalButton.MakeLast);
                }

                var from = Context.Page * ListEntries;
                var to = Math.Min(Context.Items.Count, (Context.Page + 1) * ListEntries);

                // Items, 1st Pass
                for (int i = from; i < to; i++)
                    AddCraftItem(i, Context.Items[i], 1, Context.IsInventionCategory);

                // Items, 2nd Pass
                for (int i = from; i < to; i++)
                    AddCraftItem(i, Context.Items[i], 2, Context.IsInventionCategory);


                this.AddBackground(283, 19, 491, 31, 9350);

                // Info text area
                this.AddBackground(269, 509, 505, 68, 3500);
                if (LastMessageNumber > 0)
                    AddHtmlLocalized(287, 525, 470, 45, LastMessageNumber, 0, false, false);
                else if (!String.IsNullOrEmpty(LastMessageString))
                    AddHtml(287, 525, 470, 45, LastMessageString, false, false);

                if (Context.Page > 0)
                {
                    this.AddLabel(317, 25, 0, @"Page Précédente");
                    this.AddButton(289, 24, 5537, 5539, 1, GumpButtonType.Reply, 0);
                }

                if ((Context.Page + 1) * ListEntries < Context.Items.Count)
                {
                    this.AddButton(747, 25, 5540, 5542, 2, GumpButtonType.Reply, 0);
                    this.AddLabel(655, 26, 0, @"Page Suivante");
                }

                this.AddImage(6, 131, 10440);
            }
            catch {  }

        }


        private void AddCategoryItem(int screenIndex, string caption, int itemsCount, int buttonID)
        {
            int x = 88;
            int y = 70 + (screenIndex * LeftLineHeigth);
            this.AddButton(x, y, 5540, 5542, buttonID, GumpButtonType.Reply, buttonID);
            this.AddLabel(x + 24, y, 0, caption);
            if(itemsCount >= 0)
                this.AddLabel(240, y, 0, itemsCount.ToString());
        }

        private void AddOptionItem(string caption, int buttonID)
        {
            int x = 88;
            int y = 418 + (LastOptionIndex * LeftLineHeigth);
            this.AddButton(86, y, 5540, 5542, buttonID, GumpButtonType.Reply, buttonID);
            this.AddLabel(x + 24, y, 0, caption);
            LastOptionIndex++;
        }

        private void AddCraftItem(int index, GameItemCraft item, int pass, bool isInvention)
        {
            var screenIndex = index % ListEntries;
            var BookmarkButtonID = 1000 + index;
            var PropsButtonID = 2000 + index;
            var MakeButtonID = 3000 + index;
            var ResourceButtonID = 4000 + index;
            var ySkip = 57 * screenIndex;
            var rightShift = (item.HasMutableResources && !isInvention) ? RightShift : 0;

            if (pass == 1)
            {
                // Activer pour la copie par après
                //
                //if (!isInvention)
                //    this.AddButton(762, ySkip + 64, 9780, 9781, BookmarkButtonID, GumpButtonType.Reply, BookmarkButtonID);


                this.AddBackground(282, ySkip + 52, 492 - rightShift, 55, 9350);
                if(!isInvention)
                	this.AddLabel(365, ySkip + 57, 0, String.Format("{0} [{1}%]", item.Parent.Name, (GetSuccessChance(index, item) * 100)));
                else
                    this.AddLabel(365, ySkip + 57, 0, item.Parent.Name);

                this.AddLabelCropped(365, ySkip + 78, 361 - rightShift, 20, 760, item.GetRessourceString());
                this.AddButton(729 - rightShift, ySkip + 57, 4011, 4012, PropsButtonID, GumpButtonType.Reply, PropsButtonID);
                if (Context.Tool != null)
                {
                    this.AddButton(729 - rightShift, ySkip + 81, 4023, 4024, MakeButtonID, GumpButtonType.Reply, MakeButtonID);
                }

                if(!isInvention)
                    AddLabelCropped(650 - rightShift, ySkip + 57, 70, 18, 760, Context.Owner.EvolutionInfo.CraftExperience.GetXP(item.Parent.Type).ToString() + " exp.");

                if (item.HasMutableResources && !isInvention)
                {
                    this.AddBackground(709, ySkip + 52, 65, 55, 9350);
                    this.AddButton(711, ySkip + 54, 2118, 2117, ResourceButtonID, GumpButtonType.Reply, ResourceButtonID);
                }
            }
            else if (pass == 2) // Only on second pass
            {
                if (item.HasMutableResources && !isInvention)
                {
                    var selectedRes = Context.GetSelectedResource(index);
                    var res = selectedRes ?? item.Ressource1.ItemType;
                    this.AddItem(723, ySkip + 59, res.ItemID, res.Hue);
                    this.AddLabel(712, ySkip + 72, 0, item.Ressource1.Amount + "x");
                    this.AddItem(290, ySkip + 59, item.Parent.ItemID, res.Hue);
                }
                else
                {
                    this.AddItem(290, ySkip + 59, item.Parent.ItemID, item.Parent.Hue);
                }
            }
        }

        private double GetSuccessChance(int index, GameItemCraft item)
        {
            double chance = item.GetSuccessChance(Context.Owner, Context.GetSelectedResource(index), Context.System, false);
            chance = chance < 0.0 ? 0.0 : chance > 1.0 ? 1.0 : chance;
            chance = Math.Round(chance, 4);
            return chance;
        }

        private bool NeedToRender(int index)
        {
            var from = Context.Page * ListEntries;
            var to = (Context.Page + 1) * ListEntries;
            return index >= from && index < to;
        }

        private GameItemCraft GetItem(int index)
        {
            if(index >= 0 && index < Context.Items.Count)
                return Context.Items[index];
            return null;
        }

        public override void OnResponse(Network.NetState sender, RelayInfo info)
        {
            switch (info.ButtonID)
            {
                case 0: // Closed
                    {
                        break;
                    }
                case 1: // Previous
                    {
                        if (Context.Page > 0)
                            Context.ShowPreviousPage();
                        break;
                    }
                case 2: // Next
                    {
                        if ((Context.Page + 1) * ListEntries < Context.Items.Count)
                            Context.ShowNextPage();

                        break;
                    }
                default:
                    {
                        if (info.ButtonID >= 100 && info.ButtonID < 200) // Category
                        {
                            try
                            {
                                var index = info.ButtonID - 100;
                                Context.IsInventionCategory = false;
                                Context.Category = Context.System.Categories[index];
                                Context.RebuildList();
                                Context.Page = 0;
                                Context.SelectedResources = new Dictionary<int, GameItemType>();
                                Context.ShowGump();
                            }
                            catch (Exception ex) { WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, ex.Message); }
                        }
                        else if (info.ButtonID >= 200 && info.ButtonID < 300) // Option
                        {
                            var index = info.ButtonID - 200;
                            var option = (InternalButton)info.ButtonID;


                            if (option == InternalButton.Repair) // Repair
                            {
                                Repair.Do(Context.Owner, Context.System, Context.Tool);
                            }
                            else if (option == InternalButton.Resmelt) // Resmelt
                            {
                                Resmelt.Do(Context.Owner, Context.System, Context.Tool);
                            }
                            else if (option == InternalButton.Enhance) // Enhance
                            {
                                Enhance.Do(Context.Owner, Context.System, Context.Tool);
                            }
                            else if (option == InternalButton.MakeLast) // MakeLast
                            {
                                Context.LastMadeItem.Craft.Craft(Context.Owner, Context.System, Context.LastMadeItemResource, Context.Tool, false);
                            }
                            else if (option == InternalButton.Invent) // Invent
                            {
                                Context.IsInventionCategory = true;
                                Context.RebuildList();
                                Context.Page = 0;
                                Context.SelectedResources = new Dictionary<int, GameItemType>();
                                Context.ShowGump();
                            }

                        }
                        else if (info.ButtonID >= 1000 && info.ButtonID < 2000) // Bookmark
                        {
                            var index = info.ButtonID - 1000;
                            Context.ShowGump();
                            // Ok, ça, ça va lancer un "entrainement", et analyser juste si c'est possible de faire cet objet ou pas
                            // Cela sera fait de façon RP (ou ça va faire les blueprint, je ne sais pas encore)
  
                        }
                        else if (info.ButtonID >= 2000 && info.ButtonID < 3000) // Properties
                        {
                            var index = info.ButtonID - 2000;
                            // Afficher une page de propriétés
                            var item = GetItem(index);
                            Context.ShowGump();
                            if (item != null)
                            {
                                if (Context.Owner.AccessLevel >= AccessLevel.GameMaster)
                                {
                                    Context.Owner.SendGump(new APropertiesGump(Context.Owner, item.Parent));
                                }
                                else
                                {
                                    Context.Owner.CloseGump(typeof(CraftPropsGump));
                                    Context.Owner.SendGump(new CraftPropsGump(Context, item));
                                }
                            }

                        }
                        else if (info.ButtonID >= 3000 && info.ButtonID < 4000) // Make
                        {
                            var index = info.ButtonID - 3000;
                            // Faire l'objet, m'enfin => c'est utile ça lol

                            var typeRes = GetItem(index).Ressource1.ItemType;
                            var selected = Context.GetSelectedResource(index);
                            if(selected != null)
                                typeRes = selected;

                            GetItem(index).Craft(Context.Owner, Context.System, typeRes, Context.Tool, Context.IsInventionCategory);

                        }
                        else if (info.ButtonID >= 4000 && info.ButtonID < 5000) // Select resource
                        {
                            var index = info.ButtonID - 4000;
                            // Choisir la resource mutable à utiliser
                            var item = GetItem(index);
                            var rootRes = item.Ressource1.ItemType;

                            CAGCategory root = new CAGCategory("Choix de Ressource", null);
                            root.AddNode(new CAGCraftResource(root, Context, rootRes, rootRes.ItemID, rootRes.Hue, index));
                            foreach (var res in rootRes.Craft.MutableResources)
                            {
                                root.AddNode(new CAGCraftResource(root, Context, res, res.ItemID, res.Hue, index));
                            }

                            Context.Owner.SendGump(new TreeSelectGump(Context.Owner, root, 0));
                        }


                        break;
                    }

            }
        }

    }



    public class CAGCraftResource : CAGNode
    {

        public CraftContext Context { get; private set; }
        public GameItemType Type { get; private set; }
        public int ItemID { get; private set; }
        public int Hue { get; private set; }
        public int Index { get; private set; }
        public CAGCategory Parent { get; private set; }

        public override string Caption { get { return (Type == null ? "bad type" : Type.Name); } }
        public override void OnClick(Mobile from, int page)
        {
             if(Type != null)
             {
                 Context.SelectResource(Index, Type);
                 Context.ShowGump("La ressource séléctionnée: " + Type.Name);
             }
        }

        public CAGCraftResource(CAGCategory parent, CraftContext context, GameItemType type, int itemID, int hue, int index)
        {
            Context = context;
            Parent = parent;
            Type = type;
            ItemID = itemID;
            Hue = hue;
            Index = index;
        }

    }
}