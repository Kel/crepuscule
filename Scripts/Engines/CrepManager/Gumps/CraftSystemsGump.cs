﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines;
using Server.Engines.Craft;

namespace Server.Gumps
{
    public class CraftSystemsGump : ActionListGump
    {
        public CraftSystemsGump(Mobile owner, List<GumpAction> list, int page)
            : base(owner, list, page) { }

        protected override void OnPageTurn(Mobile from, Mobile owner, List<GumpAction> list, int page)
        {
            from.CloseGump(typeof(CraftSystemsGump));
            from.SendGump(new CraftSystemsGump(owner, list, page));
        }

        protected override void OnActionClicked(Mobile from, GumpAction clicked)
        {
            from.SendGump(new CraftSystemsGump(m_Owner, m_ActionList, m_Page));
            from.CloseGump(typeof(APropertiesGump));
            from.SendGump(new APropertiesGump(from, clicked.Tag as GameCraftSystem));

        }
    }
}
