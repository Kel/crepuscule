﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines;

namespace Server.Gumps
{
    public class CreaturesGump : ActionListGump
    {
        public CreaturesGump(Mobile owner, List<GumpAction> list, int page)
            : base(owner, list, page) { }

        protected override void OnPageTurn(Mobile from, Mobile owner, List<GumpAction> list, int page)
        {
            from.CloseGump(typeof(CreaturesGump));
            from.SendGump(new CreaturesGump(owner, list, page));
        }

        protected override void OnActionClicked(Mobile from, GumpAction clicked)
        {
            from.SendGump(new CreaturesGump(m_Owner, m_ActionList, m_Page));
            from.CloseGump(typeof(APropertiesGump));
            from.SendGump(new APropertiesGump(from, clicked.Tag as CreatureType));

        }
    }
}
