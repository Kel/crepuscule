﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines;
using Server.Engines.Craft;

namespace Server.Gumps
{
    public class CraftPropsGump : Server.Gumps.Gump
    {
        public CraftPropsGump(CraftContext context, GameItemCraft item)
            : base(0, 0)
        {
            this.Closable = true;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;

            this.AddPage(0);
            this.AddBackground(278, 151, 283, 301, 9390);
            
            this.AddItem(308, 195, item.Parent.ItemID);
            this.AddHtml(306, 182, 230, 23, String.Format("<CENTER>{0}</CENTER>", item.Parent.Name), (bool)false, (bool)false);

            this.AddLabel(411, 212, 760, "* " + (item.CraftFolder.HasValue ? item.CraftFolder.Value.Name : ""));
            this.AddLabel(411, 233, 760, "* " + (CapacitiesConfig.GetName(item.Skill.Skill)));
            this.AddLabel(411, 254, 760, "* Requis: "+ item.GetComplexityString());
            
            this.AddLabel(309, 280, 760, @"Matières premières:");

            string html = item.Ressources
                .Select(res => res.Amount + " x " + res.Name)
                .Aggregate((a, b) => a + "<br />" + b);
            this.AddHtml(311, 304, 224, 113, html, (bool)false, (bool)false);
        }
    }
}