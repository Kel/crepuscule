using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(BaseArmor)), Description("Armure")]
    public class BaseArmorProps : IItemTemplate, IItemMakeTemplate
    {
        [CommandProperty(AccessLevel.GameMaster)]
        public ArmorMeditationAllowance MeditationAllowance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int BaseArmorRating { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int StrBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int DexBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int IntBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int StrRequirement { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int DexRequirement { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int IntRequirement { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxHitPoints { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int HitPoints { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public ArmorQuality Quality { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public ArmorDurabilityLevel Durability { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public ArmorProtectionLevel ProtectionLevel { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int PhysicalBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int FireBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ColdBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int PoisonBonus { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int EnergyBonus { get; set; }



        public BaseArmorProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)MeditationAllowance);
            writer.Write(BaseArmorRating);
            writer.Write(StrBonus);
            writer.Write(DexBonus);
            writer.Write(IntBonus);
            writer.Write(StrRequirement);
            writer.Write(DexRequirement);
            writer.Write(IntRequirement);
            writer.Write(MaxHitPoints);
            writer.Write(HitPoints);
            writer.Write((int)Quality);
            writer.Write((int)Durability);
            writer.Write((int)ProtectionLevel);
            writer.Write(PhysicalBonus);
            writer.Write(FireBonus);
            writer.Write(ColdBonus);
            writer.Write(PoisonBonus);
            writer.Write(EnergyBonus);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        MeditationAllowance = (ArmorMeditationAllowance)reader.ReadInt();
                        BaseArmorRating = reader.ReadInt();
                        StrBonus = reader.ReadInt();
                        DexBonus = reader.ReadInt();
                        IntBonus = reader.ReadInt();
                        StrRequirement = reader.ReadInt();
                        DexRequirement = reader.ReadInt();
                        IntRequirement = reader.ReadInt();
                        MaxHitPoints = reader.ReadInt();
                        HitPoints = reader.ReadInt();
                        Quality = (ArmorQuality)reader.ReadInt();
                        Durability = (ArmorDurabilityLevel)reader.ReadInt();
                        ProtectionLevel = (ArmorProtectionLevel)reader.ReadInt();
                        PhysicalBonus = reader.ReadInt();
                        FireBonus = reader.ReadInt();
                        ColdBonus = reader.ReadInt();
                        PoisonBonus = reader.ReadInt();
                        EnergyBonus = reader.ReadInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as BaseArmor;
            if (victim != null)
            {
                victim.MeditationAllowance = MeditationAllowance;
                victim.BaseArmorRating = BaseArmorRating;
                victim.StrBonus = StrBonus;
                victim.DexBonus = DexBonus;
                victim.IntBonus = IntBonus;
                victim.StrRequirement = StrRequirement;
                victim.DexRequirement = DexRequirement;
                victim.IntRequirement = IntRequirement;
                victim.MaxHitPoints = MaxHitPoints;
                victim.HitPoints = HitPoints;
                victim.Quality = Quality;
                victim.Durability = Durability;
                victim.ProtectionLevel = ProtectionLevel;
                victim.PhysicalBonus = PhysicalBonus;
                victim.FireBonus = FireBonus;
                victim.ColdBonus = ColdBonus;
                victim.PoisonBonus = PoisonBonus;
                victim.EnergyBonus = EnergyBonus;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as BaseArmor;
            if (from != null)
            {
                MeditationAllowance = from.MeditationAllowance;
                BaseArmorRating = from.BaseArmorRating;
                StrBonus = from.StrBonus;
                DexBonus = from.DexBonus;
                IntBonus = from.IntBonus;
                StrRequirement = from.StrRequirement;
                DexRequirement = from.DexRequirement;
                IntRequirement = from.IntRequirement;
                MaxHitPoints = from.MaxHitPoints;
                HitPoints = from.HitPoints;
                Quality = from.Quality;
                Durability = from.Durability;
                ProtectionLevel = from.ProtectionLevel;
                PhysicalBonus = from.PhysicalBonus;
                FireBonus = from.FireBonus;
                ColdBonus = from.ColdBonus;
                PoisonBonus = from.PoisonBonus;
                EnergyBonus = from.EnergyBonus;
            }
        }
    }
}
