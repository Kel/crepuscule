using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(Key)), Description("Cl�")]
    public class KeyProps : IItemTemplate, IItemMakeTemplate
    {
        [CommandProperty(AccessLevel.GameMaster)]
        public String Description { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxRange { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public uint KeyValue { get; set; }

        public KeyProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Description);
            writer.Write(MaxRange);
            writer.Write(KeyValue);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        Description = reader.ReadString();
                        MaxRange = reader.ReadInt();
                        KeyValue = reader.ReadUInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propri�t�s)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as Key;
            if (victim != null)
            {
                victim.Description = Description;
                victim.MaxRange = MaxRange;
                victim.KeyValue = KeyValue;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as Key;
            if (from != null)
            {
                Description = from.Description;
                MaxRange = from.MaxRange;
                KeyValue = from.KeyValue;
            }
        }
    }
}
