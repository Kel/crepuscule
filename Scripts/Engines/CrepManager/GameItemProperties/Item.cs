using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(Item)), Description("Objet")]
    public class ItemProps : IItemTemplate, IItemMakeTemplate
    {
        public int Amount { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public Layer Layer { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public LightType Light { get; set; }

        public bool Movable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Stackable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public Double Weight { get; set; }


        public ItemProps() 
        {
            Movable = true;
            Stackable = false;
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Amount);
            writer.Write((int)Layer);
            writer.Write((int)Light);
            writer.Write(Movable);
            writer.Write(Stackable);
            writer.Write(Weight);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        Amount = reader.ReadInt();
                        Layer = (Layer)reader.ReadInt();
                        Light = (LightType)reader.ReadInt();
                        Movable = reader.ReadBool();
                        Stackable = reader.ReadBool();
                        Weight = reader.ReadDouble();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as Item;
            if (victim != null)
            {
                //victim.Amount = Amount;
                victim.Layer = Layer;
                victim.Light = Light;
                //victim.Movable = Movable;
                victim.Stackable = Stackable;
                victim.Weight = Weight;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as Item;
            if (from != null)
            {
                Amount = from.Amount;
                Layer = from.Layer;
                Light = from.Light;
                Movable = from.Movable;
                Stackable = from.Stackable;
                Weight = from.Weight;
            }
        }
    }
}
