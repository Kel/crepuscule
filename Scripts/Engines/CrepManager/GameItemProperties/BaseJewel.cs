using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(BaseJewel)), Description("Joyaux")]
    public class BaseJewelProps : IItemTemplate, IItemMakeTemplate
    {

        [CommandProperty(AccessLevel.GameMaster)]
        public GemType GemType { get; set; }



        public BaseJewelProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)GemType);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        GemType = (GemType)reader.ReadInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as BaseJewel;
            if (victim != null)
            {
                victim.GemType = GemType;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as BaseJewel;
            if (from != null)
            {
                GemType = from.GemType;
            }
        }
    }
}
