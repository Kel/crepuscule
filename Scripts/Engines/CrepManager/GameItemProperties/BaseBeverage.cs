using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(BaseBeverage)), Description("Boisson")]
    public class BaseBeverageProps : IItemTemplate, IItemMakeTemplate
    {
        [CommandProperty(AccessLevel.GameMaster)]
        public BeverageQualityType BeverageQuality { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public BeverageType Content { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Quantity { get; set; }

        public BaseBeverageProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)BeverageQuality);
            writer.Write((int)Content);
            writer.Write(Quantity);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        BeverageQuality = (BeverageQualityType)reader.ReadInt();
                        Content = (BeverageType)reader.ReadInt();
                        Quantity = reader.ReadInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as BaseBeverage;
            if (victim != null)
            {
                victim.BeverageQuality = BeverageQuality;
                victim.Content = Content;
                victim.Quantity = Quantity;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as BaseBeverage;
            if (from != null)
            {
                BeverageQuality = from.BeverageQuality;
                Content = from.Content;
                Quantity = from.Quantity;
            }
        }
    }
}
