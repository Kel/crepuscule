using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(BasePoleArm)), Description("Pique")]
    public class BasePoleArmProps : IItemTemplate, IItemMakeTemplate
    {
        [CommandProperty(AccessLevel.GameMaster)]
        public int UsesRemaining { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool ShowUsesRemaining { get; set; }



        public BasePoleArmProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(UsesRemaining);
            writer.Write(ShowUsesRemaining);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        UsesRemaining = reader.ReadInt();
                        ShowUsesRemaining = reader.ReadBool();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as BasePoleArm;
            if (victim != null)
            {
                victim.UsesRemaining = UsesRemaining;
                victim.ShowUsesRemaining = ShowUsesRemaining;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as BasePoleArm;
            if (from != null)
            {
                UsesRemaining = from.UsesRemaining;
                ShowUsesRemaining = from.ShowUsesRemaining;
            }
        }
    }
}
