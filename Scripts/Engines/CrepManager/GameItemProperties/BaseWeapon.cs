using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(BaseWeapon)), Description("Arme")]
    public class BaseWeaponProps : IItemTemplate, IItemMakeTemplate
    {

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Cursed { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool Consecrated { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Hits { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxHits { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int PoisonCharges { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public WeaponQuality Quality { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public WeaponDamageLevel DamageLevel { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public WeaponDurabilityLevel DurabilityLevel { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxRange { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public WeaponAnimation Animation { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public WeaponType Type { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public SkillName Skill { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int HitSound { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MissSound { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MinDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Speed { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int StrRequirement { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int DexRequirement { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int IntRequirement { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public WeaponAccuracyLevel AccuracyLevel { get; set; }


        public BaseWeaponProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Cursed);
            writer.Write(Consecrated);
            writer.Write(Hits);
            writer.Write(MaxHits);
            writer.Write(PoisonCharges);
            writer.Write((int)Quality);
            writer.Write((int)DamageLevel);
            writer.Write((int)DurabilityLevel);
            writer.Write(MaxRange);
            writer.Write((int)Animation);
            writer.Write((int)Type);
            writer.Write((int)Skill);
            writer.Write(HitSound);
            writer.Write(MissSound);
            writer.Write(MinDamage);
            writer.Write(MaxDamage);
            writer.Write(Speed);
            writer.Write(StrRequirement);
            writer.Write(DexRequirement);
            writer.Write(IntRequirement);
            writer.Write((int)AccuracyLevel);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        Cursed = reader.ReadBool();
                        Consecrated = reader.ReadBool();
                        Hits = reader.ReadInt();
                        MaxHits = reader.ReadInt();
                        PoisonCharges = reader.ReadInt();
                        Quality = (WeaponQuality)reader.ReadInt();
                        DamageLevel = (WeaponDamageLevel)reader.ReadInt();
                        DurabilityLevel = (WeaponDurabilityLevel)reader.ReadInt();
                        MaxRange = reader.ReadInt();
                        Animation = (WeaponAnimation)reader.ReadInt();
                        Type = (WeaponType)reader.ReadInt();
                        Skill = (SkillName)reader.ReadInt();
                        HitSound = reader.ReadInt();
                        MissSound = reader.ReadInt();
                        MinDamage = reader.ReadInt();
                        MaxDamage = reader.ReadInt();
                        Speed = reader.ReadInt();
                        StrRequirement = reader.ReadInt();
                        DexRequirement = reader.ReadInt();
                        IntRequirement = reader.ReadInt();
                        AccuracyLevel = (WeaponAccuracyLevel)reader.ReadInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as BaseWeapon;
            if (victim != null)
            {
                victim.Cursed = Cursed;
                victim.Consecrated = Consecrated;
                victim.Hits = Hits;
                victim.MaxHits = MaxHits;
                victim.PoisonCharges = PoisonCharges;
                victim.Quality = Quality;
                victim.DamageLevel = DamageLevel;
                victim.DurabilityLevel = DurabilityLevel;
                victim.MaxRange = MaxRange;
                victim.Animation = Animation;
                victim.Type = Type;
                victim.Skill = Skill;
                victim.HitSound = HitSound;
                victim.MissSound = MissSound;
                victim.MinDamage = MinDamage;
                victim.MaxDamage = MaxDamage;
                victim.Speed = Speed;
                victim.StrRequirement = StrRequirement;
                victim.DexRequirement = DexRequirement;
                victim.IntRequirement = IntRequirement;
                victim.AccuracyLevel = AccuracyLevel;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as BaseWeapon;
            if (from != null)
            {
                Cursed = from.Cursed;
                Consecrated = from.Consecrated;
                Hits = from.Hits;
                MaxHits = from.MaxHits;
                PoisonCharges = from.PoisonCharges;
                Quality = from.Quality;
                DamageLevel = from.DamageLevel;
                DurabilityLevel = from.DurabilityLevel;
                MaxRange = from.MaxRange;
                Animation = from.Animation;
                Type = from.Type;
                Skill = from.Skill;
                HitSound = from.HitSound;
                MissSound = from.MissSound;
                MinDamage = from.MinDamage;
                MaxDamage = from.MaxDamage;
                Speed = from.Speed;
                StrRequirement = from.StrRequirement;
                DexRequirement = from.DexRequirement;
                IntRequirement = from.IntRequirement;
                AccuracyLevel = from.AccuracyLevel;
            }
        }
    }
}
