using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{
    [Template(typeof(BaseWand)), Description("Baguette")]
    public class BaseWandProps : IItemTemplate, IItemMakeTemplate
    {
        [CommandProperty(AccessLevel.GameMaster)]
        public WandEffect Effect { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Charges { get; set; }



        public BaseWandProps() { }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)Effect);
            writer.Write(Charges);
        }
        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        Effect = (WandEffect)reader.ReadInt();
                        Charges = reader.ReadInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        public void ApplyTemplate(Item item)
        {
            var victim = item as BaseWand;
            if (victim != null)
            {
                victim.Effect = Effect;
                victim.Charges = Charges;
            }
        }

        public void MakeTemplate(Item item)
        {
            var from = item as BaseWand;
            if (from != null)
            {
                Effect = from.Effect;
                Charges = from.Charges;
            }
        }
    }
}
