﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;

namespace Server.Engines
{

    [Template(typeof(BaseTool)), Description("Outil")]
    public class BaseToolProps : IItemTemplate, IItemMakeTemplate
    {
        [CommandProperty(AccessLevel.GameMaster), Description("Utilisations")]
        public int UsesRemaining { get; set; }

        #region Constructors & Serialization
        public BaseToolProps()
        {

        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(SystemInternalName);
            writer.Write(UsesRemaining);
        }


        public void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();
            switch (version)
            {
                case 0:
                    {
                        SystemInternalName = reader.ReadString();
                        m_System.SetValueByKey(this, SystemInternalName);

                        UsesRemaining = reader.ReadInt();
                        break;
                    }
            }
        }

        public override string ToString()
        {
            return "(Propriétés)";
        }

        #endregion

        #region Dynamic Craft System (Lazy Loading & Properties)

        private WeakChoice<GameCraftSystem> m_System = new WeakChoice<GameCraftSystem>("SystemInternalName", "SystemsList", "InternalName");
        public string SystemInternalName { get; set; }
        public List<GameCraftSystem> SystemsList { get { return ManagerConfig.CraftSystems; } }

        [Description("Liste de Craft")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameCraftSystem> System
        {
            get { return m_System; }
            set { }
        }


        #endregion

        #region IItemTemplate Members

        public void ApplyTemplate(Item item)
        {
            var victim = item as BaseTool;
            if (victim != null)
            {
                if (System.HasValue)
                    victim.System.SetValueDirectly(victim, System.Value);

                victim.UsesRemaining = UsesRemaining;
            }
        }

        #endregion

        #region IItemMakeTemplate Members

        public void MakeTemplate(Item item)
        {
            var from = item as BaseTool;
            if (from != null)
            {
                //SystemInternalName = ((BaseTool)item).CraftSystem.GetType().GetShortName();
                //System.SetValueByKey(this, SystemInternalName);
                UsesRemaining = ((BaseTool)item).UsesRemaining;
            }
        }

        #endregion
    }
}
