﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Scripts.Commands;
using System.Linq;
using System.IO;
using Server.Guilds;
using Server.Accounting;

namespace Server.Engines
{
    /// <summary>
    /// Basically a serialization container for the class info (a bit dirty, but simple)
    /// </summary>
    public class ItemsSerializer
    {
        public static bool SaveNeeded = true;
        private static readonly string DataDirectory = Path.Combine(Core.BaseDirectory, @"Data\Binary");
        private static readonly string BackupsDirectory = Path.Combine(Core.BaseDirectory, @"Data\Backups");
        private const string DataFileName = "Items.bin";
        private static string DataFile { get { return Path.Combine(DataDirectory, DataFileName); } }
        private static string BackupFile { get { return Path.Combine(BackupsDirectory, String.Format("Items {0}-{1}-{2} {3}-{4}-{5}.bin",
            DateTime.Now.Year,
            DateTime.Now.Month,
            DateTime.Now.Day,
            DateTime.Now.Hour,
            DateTime.Now.Minute,
            DateTime.Now.Second)); } }

        public static void Configure()
        {
            EventSink.WorldLoad += new WorldLoadEventHandler(EventSink_WorldLoad);
            EventSink.WorldSave += new WorldSaveEventHandler(EventSink_WorldSave);
        }

        public static void EventSink_WorldPreload()
        {
            if (ManagerConfig.ItemTypes.Count == 0) ManagerConfig.Configure();
            GameItemType.InitializeTemplateTypes();

            Console.WriteLine("World: Preloading items...");
            if (!File.Exists(DataFile))
                return;

            try
            {
                var reader = new BinaryFileReader(new BinaryReader(File.Open(DataFile, FileMode.Open)));
                ManagerConfig.Items.Clear();

                int version = reader.ReadInt();

                // Load all craft systems
                int systems = reader.ReadInt();
                for (int i = 0; i < systems; ++i)
                {
                    ManagerConfig.CraftSystems.Add(new Craft.GameCraftSystem(reader));
                }

                // Load all trade systems
                int trades = reader.ReadInt();
                for (int i = 0; i < trades; ++i)
                {
                    ManagerConfig.Trades.Add(new TradeInfo(reader));
                }

                // Load all item types
                var items = reader.ReadInt();
                for (int i = 0; i < items; ++i)
                {
                    var template = new GameItemType(reader);
                    if (template.Type == null)
                    {
                        Console.WriteLine("Item: " + template.Name + " not found!");
                    }
                    else if (!ManagerConfig.Items.ContainsKey(template.Type))
                    {
                        ManagerConfig.Items.Add(template.Type, template);
                        template.Craft.AddToCraftSystem();
                    }
                }

                foreach(var t in GameItemType.NotFoundTemplates)
                    Console.WriteLine("Error occured on template loading, template " + t + " was not found.");


                // Load all loot packs
                int loots = reader.ReadInt();
                for (int i = 0; i < loots; ++i)
                {
                    ManagerConfig.Loots.Add(new GameLootPack(reader));
                }


                reader.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Game Items: " + ex.Message);
                Console.WriteLine("Game Items: " + ex.StackTrace);
            }

            // Rebuild inventions
            foreach (var s in ManagerConfig.CraftSystems)
                s.RebuildInventionList();

        }

        static void EventSink_WorldLoad()
        {
            var CreatedItems = new List<Item>();

            // Make Templates
            if (ManagerConfig.ItemTypes.Count == 0) ManagerConfig.Configure();
            foreach (var itemType in ManagerConfig.ItemTypes)
            {
                if (!ManagerConfig.Items.ContainsKey(itemType))
                {
                    try
                    {
                        var instance = GameItemType.MakeTemplate(itemType);
                        if (instance != null && instance is Item)
                            CreatedItems.Add(instance as Item);
                        
                    }
                    catch (Exception)
                    {
                        //Console.WriteLine("Item not templated: " + ex.Message);
                    }
                }
            }

            //Double check 
            for (int i = 0; i < CreatedItems.Count; i++)
            {
                if (CreatedItems[i] != null && !CreatedItems[i].Deleted)
                {
                    //Console.WriteLine("Deleting: " + CreatedItems[i].GetType().Name);
                    CreatedItems[i].Delete();
                    World.RemoveItem(CreatedItems[i]);
                }
            }

            // Build ressource tree
            try
            {
                CAGCategory.RebuildRessourceNode();
            }
            catch { }

        }


        static void EventSink_WorldSave(WorldSaveEventArgs e)
        {
            if (!SaveNeeded) return;
            SaveNeeded = false;

            MakeBackup();

            try
            {
                // TODO: Replace by AsyncWriter in order to optimize the time of save
                var writer = new BinaryFileWriter(File.Open(DataFile, FileMode.Create), true);

                writer.Write((int)0); // version

                // Write Craft Systems
                writer.Write(ManagerConfig.CraftSystems.Count);
                foreach (var c in ManagerConfig.CraftSystems)
                    c.Serialize(writer);

                // Write Trade Systems
                writer.Write(ManagerConfig.Trades.Count);
                foreach (var c in ManagerConfig.Trades)
                    c.Serialize(writer);

                // Write Items
                writer.Write(ManagerConfig.Items.Count);
                foreach (var c in ManagerConfig.Items.Values)
                    c.Serialize(writer);

                // Write Loot packs
                writer.Write(ManagerConfig.Loots.Count);
                foreach (var c in ManagerConfig.Loots)
                    c.Serialize(writer);

                writer.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Game Items: " + ex.Message);
                Console.WriteLine("Game Items: " + ex.StackTrace);
            }
        }

        static void MakeBackup()
        {
            if (!Directory.Exists(BackupsDirectory))
                Directory.CreateDirectory(BackupsDirectory);

            var b = BackupFile;
            if (File.Exists(DataFile) && !File.Exists(b))
                File.Copy(DataFile, b);
        }


    }

}
