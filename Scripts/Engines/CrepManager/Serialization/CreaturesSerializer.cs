﻿using System;
using System.Collections;
using Server.Network;
using Server.Mobiles;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Engines;
using System.Collections.Generic;
using Server.Scripts.Commands;
using System.Linq;
using System.IO;
using Server.Guilds;
using Server.Accounting;

namespace Server.Engines
{
    /// <summary>
    /// Basically a serialization container for the class info (a bit dirty, but simple)
    /// </summary>
    public class CreaturesSerializer
    {
        public static bool SaveNeeded = true;
        private static readonly string DataDirectory = Path.Combine(Core.BaseDirectory, @"Data\Binary");
        private static readonly string BackupsDirectory = Path.Combine(Core.BaseDirectory, @"Data\Backups");
        private const string DataFileName = "Creatures.bin";
        private static string DataFile { get { return Path.Combine(DataDirectory, DataFileName); } }
        private static string BackupFile { get { return Path.Combine(BackupsDirectory, String.Format("Creatures {0}-{1}-{2} {3}-{4}-{5}.bin",
            DateTime.Now.Year,
            DateTime.Now.Month,
            DateTime.Now.Day,
            DateTime.Now.Hour,
            DateTime.Now.Minute,
            DateTime.Now.Second)); } }

        public static void Configure()
        {
            EventSink.WorldLoad += new WorldLoadEventHandler(EventSink_WorldLoad);
            EventSink.WorldSave += new WorldSaveEventHandler(EventSink_WorldSave);
        }

        public static void EventSink_WorldPreload()
        {
            try
            {
                Console.WriteLine("World: Preloading mobiles...");

                var reader = new BinaryFileReader(new BinaryReader(File.Open(DataFile, FileMode.Open)));
                ManagerConfig.Creatures.Clear();

                int version = reader.ReadInt();
                var creatures = reader.ReadInt();

                for (int i = 0; i < creatures; ++i)
                {
                    var template = new CreatureType(reader);
                    var type = Type.GetType(template.TypeName);

                    if (type == null)
                    {
                        Console.WriteLine("Creatures: " + template.TypeName + " not found!");
                    }
                    else if (!ManagerConfig.Creatures.ContainsKey(type))
                    {
                        ManagerConfig.Creatures.Add(type, template);
                    }
                }


                reader.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Creatures: " + ex.Message);
            }
        }

        static void EventSink_WorldLoad()
        {

            // Check if we did not forget any mobs (new ones maybe?)
            if (ManagerConfig.CreatureTypes.Count == 0) ManagerConfig.Configure();
            foreach (var creatureType in ManagerConfig.CreatureTypes)
            {
                if (!ManagerConfig.Creatures.ContainsKey(creatureType))
                {
                    // New mob
                    //Console.WriteLine("New monster detected: "+ creatureType.Name);

                    // Create an instance of such mob
                    try
                    {
                        CreatureType.MakeTemplate(creatureType);
                    }
                    catch (Exception)
                    {
                        //Console.WriteLine("Creature not templated: " + ex.Message);
                    }
                }
            }

        }



        static void EventSink_WorldSave(WorldSaveEventArgs e)
        {
            if (!SaveNeeded) return;
            SaveNeeded = false;

            MakeBackup();

            try
            {
                // TODO: Replace by AsyncWriter in order to optimize the time of save
                var writer = new BinaryFileWriter(File.Open(DataFile, FileMode.Create), true);

                writer.Write((int)0); // version
                writer.Write(ManagerConfig.Creatures.Count);
                foreach (var c in ManagerConfig.Creatures.Values)
                    c.Serialize(writer);
                

                writer.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Creatures: " + ex.Message);
            }
        }

        static void MakeBackup()
        {
            if (!Directory.Exists(BackupsDirectory))
                Directory.CreateDirectory(BackupsDirectory);

            var b = BackupFile;
            if (File.Exists(DataFile) && !File.Exists(b))
                File.Copy(DataFile, b);
        }



    }

}
