﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject, NoSort]
    [Description("Créature")]
    public class CreatureType
    {
        #region Properties
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom")]
        public string Name { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("BodyValue")]
        public int BodyValue { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Son")]
        public int BaseSoundID { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Karma")]
        public int Karma { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fame")]
        public int Fame { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Vitesse Active")]
        public double ActiveSpeed{ get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Vitesse Passive")]
        public double PassiveSpeed { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("IA")]
        public AIType AI { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Mode de Combat")]
        public FightMode FightMode { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Couleur")]
        public int Hue { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Force")]
        public RangeValue Str { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Dextérité")]
        public RangeValue Dex { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Intelligence")]
        public RangeValue Int { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Vie")]
        public RangeValue Hits { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fatigue")]
        public RangeValue Stam { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Mana")]
        public RangeValue Mana { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Dégats"), StartGroup("Dégats et Résistances")]
        public RangeValue Damage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Dégats Elem.")]
        public CreatureDamage ElementalDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Résist. Elem.")]
        public CreatureResist ElementalResist { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Armure")]
        public int VirtualArmor { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Apprivoisage"), StartGroup("Propriétés Avancées")]
        public CreatureTaming Taming { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Loot")]
        public CreatureLootPacks Loot { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Loot (Niveau)")]
        public LootPackLevel LootPackLevel { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Capacités")]
        public CreatureSkills Skills { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public CreatureRolePlay RolePlay { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public CreatureEquipment Equipment { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Script")]
        public string ScriptName 
        {
            get 
            {
                if (!String.IsNullOrEmpty(TypeName))
                {
                    var split = TypeName.Split('.');
                    if(split != null && split.Length > 0)
                        return split[split.Length - 1];
                }
                return TypeName;;
                
            }
        }



        [CommandProperty(AccessLevel.GameMaster)]
        public int AutoRestat
        {
            get { return AutoRestatValue; }
            set
            {
                if (value > 1 && value < 999)
                {
                    AutoRestatValue = value;
                    AutoRestatAction(value);
                }
            }
        }


        private WeakChoice<TradeInfo> m_Trades = new WeakChoice<TradeInfo>("TradesInternalName", "TradesList", "InternalName");
        public string TradesInternalName { get; set; }
        public List<TradeInfo> TradesList { get { return ManagerConfig.Trades; } }

        [Description("Achat/Vente")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Trades
        {
            get { return m_Trades; }
            set {  }
        }



        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Réinitialiser"), StartGroup("Actions")]
        public PropertyAction DoReset
        {
            get { return new PropertyAction("Réinitialiser"); }
            set
            {
                Reset();
            }
        }

        public Type Type
        {
            get
            {
                return Type.GetType(TypeName);
            }
        }

        // Internal props
        public string TypeName { get; set; }
        public int AutoRestatValue = 100;
        #endregion

        #region Constructors & Serialization
        public CreatureType(string typeName) 
        {
            Str = new RangeValue();
            Dex = new RangeValue();
            Int = new RangeValue();
            Hits = new RangeValue();
            Stam = new RangeValue();
            Mana = new RangeValue();
            Damage = new RangeValue();
            ElementalDamage = new CreatureDamage();
            ElementalResist = new CreatureResist();
            Taming = new CreatureTaming();
            Skills = new CreatureSkills();
            TypeName = typeName;
            RolePlay = new CreatureRolePlay();
            Equipment = new CreatureEquipment();
            AutoRestatValue = 100;
            Loot = new CreatureLootPacks(this);
            LootPackLevel = Engines.LootPackLevel.VeryPoor;
            // Initialize a mob of the type and set the template
        }
        public CreatureType(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch(version)
            {
                case 5:
                {
                    BaseSoundID = reader.ReadInt();
                    Karma = reader.ReadInt();
                    AI = (AIType)reader.ReadInt();
                    FightMode = (FightMode)reader.ReadInt();
                    Fame = reader.ReadInt();
                    ActiveSpeed = reader.ReadDouble();
                    PassiveSpeed = reader.ReadDouble();
                    goto case 4;
                }
                case 4:
                {
                    Equipment = new CreatureEquipment(reader);
                    goto case 3;
                }
                case 3:
                {
                    if (Equipment == null) Equipment = new CreatureEquipment();
                    BodyValue = reader.ReadInt();
                    Hue = reader.ReadInt();
                    goto case 2;
                }
                case 2:
                {
                    LootPackLevel = (Engines.LootPackLevel)reader.ReadInt();
                    Loot = new CreatureLootPacks(reader, this);
                    goto case 1;
                }
                case 1:
                {
                    TradesInternalName = reader.ReadString();
                    Trades.SetValueByKey(this, TradesInternalName);
                    goto case 0;
                }
                case 0:
                {
                    Name = reader.ReadString();
                    TypeName = reader.ReadString();
                    Str = new RangeValue(reader);
                    Dex = new RangeValue(reader);
                    Int = new RangeValue(reader);
                    Hits = new RangeValue(reader);
                    Stam = new RangeValue(reader);
                    Mana = new RangeValue(reader);
                    Damage = new RangeValue(reader);
                    ElementalDamage = new CreatureDamage(reader);
                    ElementalResist = new CreatureResist(reader);
                    VirtualArmor = reader.ReadInt();
                    Taming = new CreatureTaming(reader);
                    Skills = new CreatureSkills(reader);
                    RolePlay = new CreatureRolePlay(reader);
                    AutoRestatValue = reader.ReadInt();
                    break;
                }
            }

            if (Loot == null)
                Loot = new CreatureLootPacks(this);
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)5); //version
            writer.Write(BaseSoundID);
            writer.Write(Karma);
            writer.Write((int)AI);
            writer.Write((int)FightMode);
            writer.Write(Fame);
            writer.Write(ActiveSpeed);
            writer.Write(PassiveSpeed);
            Equipment.Serialize(writer);
            writer.Write(BodyValue);
            writer.Write(Hue);
            writer.Write((int)LootPackLevel);
            Loot.Serialize(writer);
            writer.Write(TradesInternalName);
            writer.Write(Name);
            writer.Write(TypeName);
            Str.Serialize(writer);
            Dex.Serialize(writer);
            Int.Serialize(writer);
            Hits.Serialize(writer);
            Stam.Serialize(writer);
            Mana.Serialize(writer);
            Damage.Serialize(writer);
            ElementalDamage.Serialize(writer);
            ElementalResist.Serialize(writer);
            writer.Write(VirtualArmor);
            Taming.Serialize(writer);
            Skills.Serialize(writer);
            RolePlay.Serialize(writer);
            writer.Write(AutoRestatValue);
        }

        public override string ToString()
        {
            return Name;
        }
        #endregion

        #region SetToCreature & SetFromCreature
        public void SetToCreature(BaseCreature mobile)
        {
            var bv = mobile as BaseVendor;
            if (bv != null && Trades.HasValue)
            {
                bv.Trades.SetValueDirectly(bv, Trades.Value);
                bv.LoadSBInfo();
                return; // Do not continue, that's it for vendors
            }

            //mobile.Name = Name;
            AutoRestatValue = 100;
            mobile.LootLevel = LootPackLevel;
            mobile.VirtualArmor = VirtualArmor;
            mobile.SetStr(Str.Min, Str.Max);
            mobile.SetDex(Dex.Min, Dex.Max);
            mobile.SetInt(Int.Min, Int.Max);
            mobile.SetHits(Hits.Min, Hits.Max);
            mobile.SetMana(Mana.Min, Mana.Max);
            mobile.SetStam(Stam.Min, Stam.Max);
            mobile.SetDamage(Damage.Min, Damage.Max);

            mobile.SetDamageType(ResistanceType.Cold, ElementalDamage.ColdDamage);
            mobile.SetDamageType(ResistanceType.Energy, ElementalDamage.EnergyDamage);
            mobile.SetDamageType(ResistanceType.Fire, ElementalDamage.FireDamage);
            mobile.SetDamageType(ResistanceType.Physical, ElementalDamage.PhysicalDamage);
            mobile.SetDamageType(ResistanceType.Poison, ElementalDamage.PoisonDamage);

            mobile.SetResistance(ResistanceType.Cold, ElementalResist.ColdResistance);
            mobile.SetResistance(ResistanceType.Energy, ElementalResist.EnergyResistance);
            mobile.SetResistance(ResistanceType.Fire, ElementalResist.FireResistance);
            mobile.SetResistance(ResistanceType.Physical, ElementalResist.PhysicalResistance);
            mobile.SetResistance(ResistanceType.Poison, ElementalResist.PoisonResistance);

            mobile.Tamable = Taming.Tamable;
            mobile.MinTameSkill = Taming.MinTameSkill;
            mobile.ControlSlots = Taming.ControlSlots;

            for (int i = 0; i < Skills.Length; ++i)
                mobile.SetSkill((SkillName)i, Skills[i] );

            mobile.Saying1 = RolePlay.Saying1;
            mobile.Saying2 = RolePlay.Saying2;
            mobile.Saying3 = RolePlay.Saying3;
            mobile.Saying4 = RolePlay.Saying4;
            mobile.Saying5 = RolePlay.Saying5;
            mobile.SayingsSecondsDelay = RolePlay.SayingsSecondsDelay;
            mobile.SayingsEnabled = RolePlay.SayingsEnabled;

            mobile.BodyValue = BodyValue;
            mobile.Hue = Hue;
            

            if (!(mobile is BaseVendor))
            {
                mobile.Name = Name;
                mobile.BaseSoundID = BaseSoundID;
                mobile.Karma = Karma;
                mobile.Fame = Fame;
                mobile.AI = AI;
                mobile.FightMode = FightMode;
            }

            mobile.ActiveSpeed = ActiveSpeed;
            mobile.PassiveSpeed = PassiveSpeed;

            Equipment.Equip(mobile);
        }

        public void SetFromCreature(BaseCreature mobile)
        {
            Name = mobile.Name;
            Hue = mobile.Hue;
            BaseSoundID = mobile.BaseSoundID;
            Karma = mobile.Karma;
            Fame = mobile.Fame;
            BodyValue = mobile.BodyValue;
            VirtualArmor = mobile.VirtualArmor;
            AI = mobile.AI;
            FightMode = mobile.FightMode;
            ActiveSpeed = mobile.ActiveSpeed;
            PassiveSpeed = mobile.PassiveSpeed;
            
            Str.Min = Str.Max = mobile.Str;
            Dex.Min = Dex.Max = mobile.Dex;
            Int.Min = Int.Max = mobile.Int;
            Hits.Min = Hits.Max = mobile.HitsMax;
            Mana.Min = Mana.Max = mobile.ManaMax;
            Stam.Min = Stam.Max = mobile.StamMax;
            Damage.Max = mobile.DamageMax;
            Damage.Min = mobile.DamageMin;

            ElementalDamage.ColdDamage = mobile.ColdDamage;
            ElementalDamage.EnergyDamage = mobile.EnergyDamage;
            ElementalDamage.FireDamage = mobile.FireDamage;
            ElementalDamage.PhysicalDamage = mobile.PhysicalDamage;
            ElementalDamage.PoisonDamage = mobile.PoisonDamage;

            ElementalResist.ColdResistance = mobile.ColdResistance;
            ElementalResist.EnergyResistance = mobile.EnergyResistance;
            ElementalResist.FireResistance = mobile.FireResistance;
            ElementalResist.PhysicalResistance = mobile.PhysicalResistance;
            ElementalResist.PoisonResistance = mobile.PoisonResistance;

            Taming.Tamable = mobile.Tamable;
            Taming.MinTameSkill = mobile.MinTameSkill;
            Taming.ControlSlots = mobile.ControlSlots;

            for (int i = 0; i < Skills.Length; ++i)
                Skills[i] = mobile.Skills[i].Value;

            RolePlay.Saying1 = mobile.Saying1;
            RolePlay.Saying2 = mobile.Saying2;
            RolePlay.Saying3 = mobile.Saying3;
            RolePlay.Saying4 = mobile.Saying4;
            RolePlay.Saying5 = mobile.Saying5;
            RolePlay.SayingsSecondsDelay = mobile.SayingsSecondsDelay;
            RolePlay.SayingsEnabled = mobile.SayingsEnabled;
        }

        public void Reset()
        {
            try
            {
                var creatureType = Type.GetType(TypeName);
                if (creatureType != null)
                {
                    var instance = Activator.CreateInstance(creatureType);
                    if (instance is BaseCreature)
                    {
                        var creature = instance as BaseCreature;
                        this.SetFromCreature(creature);
                        creature.Delete();
                    }
                }
            }
            catch { }
        }
        #endregion

        #region Find
        public static CreatureType FindType(Mobile mobile)
        {
            if (mobile == null) return null;
            return FindType(mobile.GetType());
        }

        public static CreatureType FindType(Type mobileType)
        {
            if(ManagerConfig.Creatures.ContainsKey(mobileType))
                return ManagerConfig.Creatures[mobileType];
            return null;
        }

        public static object MakeTemplate(Type creatureType)
        {
            var instance = Activator.CreateInstance(creatureType);
            if (instance is BaseCreature)
            {
                var creature = instance as BaseCreature;
                var type = new CreatureType(creatureType.FullName);
                type.SetFromCreature(creature);
                creature.Delete();
                ManagerConfig.Creatures[creatureType] = type;
                Console.WriteLine("New monster detected: " + creatureType.Name);
            }
            return instance;
        }
        #endregion

        #region AutoRestat
        public void AutoRestatAction(double percentage)
        {
            double modifier = percentage / 100;
            this.VirtualArmor = (int)(VirtualArmor * modifier);
            Str.Min = (int)((double)Str.Min * modifier);
            Str.Max = (int)((double)Str.Max * modifier);
            Dex.Min = (int)((double)Dex.Min * modifier);
            Dex.Max = (int)((double)Dex.Max * modifier);
            Int.Min = (int)((double)Int.Min * modifier);
            Int.Max = (int)((double)Int.Max * modifier);
            Hits.Min = (int)((double)Hits.Min * modifier);
            Hits.Max = (int)((double)Hits.Max * modifier);
            Mana.Min = (int)((double)Mana.Min * modifier);
            Mana.Max = (int)((double)Mana.Max * modifier);
            Stam.Min = (int)((double)Stam.Min * modifier);
            Stam.Max = (int)((double)Stam.Max * modifier);
            Damage.Min = (int)((double)Damage.Min * modifier);
            Damage.Max = (int)((double)Damage.Max * modifier);

            ElementalDamage.ColdDamage = (int)((double)ElementalDamage.ColdDamage * modifier);
            ElementalDamage.EnergyDamage = (int)((double)ElementalDamage.EnergyDamage * modifier);
            ElementalDamage.FireDamage = (int)((double)ElementalDamage.FireDamage * modifier);
            ElementalDamage.PhysicalDamage = (int)((double)ElementalDamage.PhysicalDamage * modifier);
            ElementalDamage.PoisonDamage = (int)((double)ElementalDamage.PoisonDamage * modifier);

            ElementalResist.ColdResistance = (int)((double)ElementalResist.ColdResistance * modifier);
            ElementalResist.EnergyResistance = (int)((double)ElementalResist.EnergyResistance * modifier);
            ElementalResist.FireResistance = (int)((double)ElementalResist.FireResistance * modifier);
            ElementalResist.PhysicalResistance = (int)((double)ElementalResist.PhysicalResistance * modifier);
            ElementalResist.PoisonResistance = (int)((double)ElementalResist.PoisonResistance * modifier);

            for (int i = 0; i < Skills.Length; ++i)
                Skills[i] = (int)(Skills[i] * modifier);

        }
        #endregion
    }


}
