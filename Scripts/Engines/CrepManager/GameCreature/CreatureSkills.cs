﻿using Server.Network;
using System;
using System.Reflection;


namespace Server.Engines
{
    [PropertyObject]
    [Description("Capacités")]
    public class CreatureSkills
    {
        private double[] m_Skills;
        public CreatureSkills()
        {
            this.m_Skills = new double[SkillInfo.Table.Length];
        }

        public CreatureSkills( GenericReader reader)
        {
            int num = reader.ReadInt();
            m_Skills = new double[SkillInfo.Table.Length];
            int TotalSkills = reader.ReadInt();
            for (int i = 0; i < TotalSkills; i++)
            {
                m_Skills[i] = reader.ReadDouble();
            }
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write(0);
            writer.Write(this.m_Skills.Length);
            for (int i = 0; i < this.m_Skills.Length; i++)
            {
                writer.Write(m_Skills[i]);
            }
        }

        public override string ToString()
        {
            return "(Capacités)";
        }


        [CommandProperty(AccessLevel.Counselor)]
        public double Alchemy
        {
            get
            {
                return this[SkillName.Alchemy];
            }
            set
            {
                this[SkillName.Alchemy] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Anatomy
        {
            get
            {
                return this[SkillName.Anatomy];
            }
            set
            {
                this[SkillName.Anatomy] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double AnimalLore
        {
            get
            {
                return this[SkillName.AnimalLore];
            }
            set
            {
                this[SkillName.AnimalLore] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double AnimalTaming
        {
            get
            {
                return this[SkillName.AnimalTaming];
            }
            set
            {
                this[SkillName.AnimalTaming] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Archery
        {
            get
            {
                return this[SkillName.Archery];
            }
            set
            {
                this[SkillName.Archery] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double ArmsLore
        {
            get
            {
                return this[SkillName.ArmsLore];
            }
            set
            {
                this[SkillName.ArmsLore] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Begging
        {
            get
            {
                return this[SkillName.Begging];
            }
            set
            {
                this[SkillName.Begging] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Blacksmith
        {
            get
            {
                return this[SkillName.Blacksmith];
            }
            set
            {
                this[SkillName.Blacksmith] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Camping
        {
            get
            {
                return this[SkillName.Camping];
            }
            set
            {
                this[SkillName.Camping] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Carpentry
        {
            get
            {
                return this[SkillName.Carpentry];
            }
            set
            {
                this[SkillName.Carpentry] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Cartography
        {
            get
            {
                return this[SkillName.Cartography];
            }
            set
            {
                this[SkillName.Cartography] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Chivalry
        {
            get
            {
                return this[SkillName.Chivalry];
            }
            set
            {
                this[SkillName.Chivalry] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Cooking
        {
            get
            {
                return this[SkillName.Cooking];
            }
            set
            {
                this[SkillName.Cooking] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double DetectHidden
        {
            get
            {
                return this[SkillName.DetectHidden];
            }
            set
            {
                this[SkillName.DetectHidden] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Discordance
        {
            get
            {
                return this[SkillName.Discordance];
            }
            set
            {
                this[SkillName.Discordance] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double EvalInt
        {
            get
            {
                return this[SkillName.EvalInt];
            }
            set
            {
                this[SkillName.EvalInt] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Fencing
        {
            get
            {
                return this[SkillName.Fencing];
            }
            set
            {
                this[SkillName.Fencing] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Fishing
        {
            get
            {
                return this[SkillName.Fishing];
            }
            set
            {
                this[SkillName.Fishing] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Fletching
        {
            get
            {
                return this[SkillName.Fletching];
            }
            set
            {
                this[SkillName.Fletching] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Focus
        {
            get
            {
                return this[SkillName.Focus];
            }
            set
            {
                this[SkillName.Focus] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Forensics
        {
            get
            {
                return this[SkillName.Forensics];
            }
            set
            {
                this[SkillName.Forensics] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Healing
        {
            get
            {
                return this[SkillName.Healing];
            }
            set
            {
                this[SkillName.Healing] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Herding
        {
            get
            {
                return this[SkillName.Herding];
            }
            set
            {
                this[SkillName.Herding] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Hiding
        {
            get
            {
                return this[SkillName.Hiding];
            }
            set
            {
                this[SkillName.Hiding] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Inscribe
        {
            get
            {
                return this[SkillName.Inscribe];
            }
            set
            {
                this[SkillName.Inscribe] = value;
            }
        }

        public double this[SkillName name]
        {
            get
            {
                return this[(int) name];
            }
            set
            {
                this[(int) name] = value;
            }
        }

        public double this[int skillID]
        {
            get
            {
                if ((skillID < 0) || (skillID >= this.m_Skills.Length))
                    return 0;
                
                double skill = this.m_Skills[skillID];
                return skill;
            }
            set
            {
                if ((skillID < 0) || (skillID >= this.m_Skills.Length))
                    return;
                m_Skills[skillID] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double ItemID
        {
            get
            {
                return this[SkillName.ItemID];
            }
            set
            {
                this[SkillName.ItemID] = value;
            }
        }

        public int Length
        {
            get
            {
                return this.m_Skills.Length;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Lockpicking
        {
            get
            {
                return this[SkillName.Lockpicking];
            }
            set
            {
                this[SkillName.Lockpicking] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Lumberjacking
        {
            get
            {
                return this[SkillName.Lumberjacking];
            }
            set
            {
                this[SkillName.Lumberjacking] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Macing
        {
            get
            {
                return this[SkillName.Macing];
            }
            set
            {
                this[SkillName.Macing] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Magery
        {
            get
            {
                return this[SkillName.Magery];
            }
            set
            {
                this[SkillName.Magery] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double MagicResist
        {
            get
            {
                return this[SkillName.MagicResist];
            }
            set
            {
                this[SkillName.MagicResist] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Meditation
        {
            get
            {
                return this[SkillName.Meditation];
            }
            set
            {
                this[SkillName.Meditation] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Mining
        {
            get
            {
                return this[SkillName.Mining];
            }
            set
            {
                this[SkillName.Mining] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Musicianship
        {
            get
            {
                return this[SkillName.Musicianship];
            }
            set
            {
                this[SkillName.Musicianship] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Necromancy
        {
            get
            {
                return this[SkillName.Necromancy];
            }
            set
            {
                this[SkillName.Necromancy] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Parry
        {
            get
            {
                return this[SkillName.Parry];
            }
            set
            {
                this[SkillName.Parry] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Peacemaking
        {
            get
            {
                return this[SkillName.Peacemaking];
            }
            set
            {
                this[SkillName.Peacemaking] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Poisoning
        {
            get
            {
                return this[SkillName.Poisoning];
            }
            set
            {
                this[SkillName.Poisoning] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Provocation
        {
            get
            {
                return this[SkillName.Provocation];
            }
            set
            {
                this[SkillName.Provocation] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double RemoveTrap
        {
            get
            {
                return this[SkillName.RemoveTrap];
            }
            set
            {
                this[SkillName.RemoveTrap] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Snooping
        {
            get
            {
                return this[SkillName.Snooping];
            }
            set
            {
                this[SkillName.Snooping] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double SpiritSpeak
        {
            get
            {
                return this[SkillName.SpiritSpeak];
            }
            set
            {
                this[SkillName.SpiritSpeak] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Stealing
        {
            get
            {
                return this[SkillName.Stealing];
            }
            set
            {
                this[SkillName.Stealing] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Stealth
        {
            get
            {
                return this[SkillName.Stealth];
            }
            set
            {
                this[SkillName.Stealth] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Swords
        {
            get
            {
                return this[SkillName.Swords];
            }
            set
            {
                this[SkillName.Swords] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Tactics
        {
            get
            {
                return this[SkillName.Tactics];
            }
            set
            {
                this[SkillName.Tactics] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Tailoring
        {
            get
            {
                return this[SkillName.Tailoring];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double TasteID
        {
            get
            {
                return this[SkillName.TasteID];
            }
            set
            {
                this[SkillName.TasteID] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Tinkering
        {
            get
            {
                return this[SkillName.Tinkering];
            }
            set
            {
                this[SkillName.Tinkering] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Tracking
        {
            get
            {
                return this[SkillName.Tracking];
            }
            set
            {
                this[SkillName.Tactics] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Veterinary
        {
            get
            {
                return this[SkillName.Veterinary];
            }
            set
            {
                this[SkillName.Veterinary] = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Wrestling
        {
            get
            {
                return this[SkillName.Wrestling];
            }
            set
            {
                this[SkillName.Wrestling] = value;
            }
        }
    }
}

