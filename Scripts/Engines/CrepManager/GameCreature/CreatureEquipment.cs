﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines 
{

    [PropertyObject]
    [Description("Equipement")]
    public class CreatureEquipment
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Activé")]
        public bool Enabled { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #1")]
        public CreatureItem Slot1 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #2")]
        public CreatureItem Slot2 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #3")]
        public CreatureItem Slot3 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #4")]
        public CreatureItem Slot4 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #5")]
        public CreatureItem Slot5 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #6")]
        public CreatureItem Slot6 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #7")]
        public CreatureItem Slot7 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #8")]
        public CreatureItem Slot8 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #9")]
        public CreatureItem Slot9 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Slot #10")]
        public CreatureItem Slot10 { get; set; }


        public void Equip(BaseCreature mobile)
        {
            if (Enabled)
            {
                if (Slot1.IsValid) mobile.AddItem(Slot1.CreateItem());
                if (Slot2.IsValid) mobile.AddItem(Slot2.CreateItem());
                if (Slot3.IsValid) mobile.AddItem(Slot3.CreateItem());
                if (Slot4.IsValid) mobile.AddItem(Slot4.CreateItem());
                if (Slot5.IsValid) mobile.AddItem(Slot5.CreateItem());
                if (Slot6.IsValid) mobile.AddItem(Slot6.CreateItem());
                if (Slot7.IsValid) mobile.AddItem(Slot7.CreateItem());
                if (Slot8.IsValid) mobile.AddItem(Slot8.CreateItem());
                if (Slot9.IsValid) mobile.AddItem(Slot9.CreateItem());
                if (Slot10.IsValid) mobile.AddItem(Slot10.CreateItem());
            }
        }

        #region Constructors & Serialization
        public CreatureEquipment()
        {
            Enabled = false;
            Slot1 = new CreatureItem();
            Slot2 = new CreatureItem();
            Slot3 = new CreatureItem();
            Slot4 = new CreatureItem();
            Slot5 = new CreatureItem();
            Slot6 = new CreatureItem();
            Slot7 = new CreatureItem();
            Slot8 = new CreatureItem();
            Slot9 = new CreatureItem();
            Slot10 = new CreatureItem();
        }
        public CreatureEquipment(GenericReader reader)
        {

            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        Enabled = reader.ReadBool();
                        Slot1 = new CreatureItem(reader);
                        Slot2 = new CreatureItem(reader);
                        Slot3 = new CreatureItem(reader);
                        Slot4 = new CreatureItem(reader);
                        Slot5 = new CreatureItem(reader);
                        Slot6 = new CreatureItem(reader);
                        Slot7 = new CreatureItem(reader);
                        Slot8 = new CreatureItem(reader);
                        Slot9 = new CreatureItem(reader);
                        Slot10 = new CreatureItem(reader);

                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Enabled);
            Slot1.Serialize(writer); 
            Slot2.Serialize(writer);
            Slot3.Serialize(writer);
            Slot4.Serialize(writer);
            Slot5.Serialize(writer);
            Slot6.Serialize(writer);
            Slot7.Serialize(writer);
            Slot8.Serialize(writer);
            Slot9.Serialize(writer);
            Slot10.Serialize(writer);
        }

        public override string ToString()
        {
            return "(Equipement)";
        }
        #endregion

    }
}
