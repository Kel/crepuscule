﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Résistances")]
    public class CreatureResist
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Physique")]
        public int PhysicalResistance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Feu")]
        public int FireResistance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Froid")]
        public int ColdResistance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Poison")]
        public int PoisonResistance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Energie")]
        public int EnergyResistance { get; set; }
        
        #region Constructors & Serialization
        public CreatureResist() 
        {
            
        }
        public CreatureResist(GenericReader reader)
        {
            
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    PhysicalResistance = reader.ReadInt();
                    FireResistance= reader.ReadInt();
                    ColdResistance = reader.ReadInt();
                    PoisonResistance= reader.ReadInt();
                    EnergyResistance = reader.ReadInt();
                    break;
                }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(PhysicalResistance);
            writer.Write(FireResistance);
            writer.Write(ColdResistance);
            writer.Write(PoisonResistance);
            writer.Write(EnergyResistance);
        }

        public override string ToString()
        {
            return String.Format("Resist: {0}, {1}, {2}, {3}, {4}", 
                PhysicalResistance,
                FireResistance,
                ColdResistance,
                PoisonResistance,
                EnergyResistance
                );
        }
        #endregion

    }
}
