﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Role-Play")]
    public class CreatureRolePlay
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Activé?")]
        public bool SayingsEnabled { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Délais (sec)")]
        public int SayingsSecondsDelay { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Phrase #1")]
        public string Saying1 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Phrase #2")]
        public string Saying2 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Phrase #3")]
        public string Saying3 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Phrase #4")]
        public string Saying4 { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Phrase #5")]
        public string Saying5 { get; set; }


        #region Constructors & Serialization
        public CreatureRolePlay() 
        {

        }
        public CreatureRolePlay(GenericReader reader)
        {
            
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    SayingsEnabled = reader.ReadBool();
                    SayingsSecondsDelay = reader.ReadInt();
                    Saying1 = reader.ReadString();
                    Saying2 = reader.ReadString();
                    Saying3 = reader.ReadString();
                    Saying4 = reader.ReadString();
                    Saying5 = reader.ReadString();
                    break;
                }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(SayingsEnabled);
            writer.Write(SayingsSecondsDelay);
            writer.Write(Saying1);
            writer.Write(Saying2);
            writer.Write(Saying3);
            writer.Write(Saying4);
            writer.Write(Saying5);
        }

        public override string ToString()
        {
            return "(Role-Play)";
        }
        #endregion

    }
}
