﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Apprivoisage")]
    public class CreatureTaming
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Apprivoisable?")]
        public bool Tamable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Cases de Contrôle")]
        public int ControlSlots { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Capacité Min.")]
        public double MinTameSkill { get; set; }

        #region Constructors & Serialization
        public CreatureTaming() 
        {
            
        }
        public CreatureTaming(GenericReader reader)
        {
            
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Tamable = reader.ReadBool();
                    ControlSlots = reader.ReadInt();
                    MinTameSkill = reader.ReadDouble();
                    break;
                }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Tamable);
            writer.Write(ControlSlots);
            writer.Write(MinTameSkill);
        }

        public override string ToString()
        {
            return String.Format("{0}", 
                Tamable ? String.Format("Oui, {0}", MinTameSkill): "Non"
                );
        }
        #endregion

    }
}
