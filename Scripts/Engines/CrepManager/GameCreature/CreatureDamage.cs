﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Dégats")]
    public class CreatureDamage
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Physiques")]
        public int PhysicalDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Feu")]
        public int FireDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Froid")]
        public int ColdDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Poison")]
        public int PoisonDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Energie")]
        public int EnergyDamage { get; set; }
        
        #region Constructors & Serialization
        public CreatureDamage() 
        {
            
        }
        public CreatureDamage(GenericReader reader)
        {
            
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    PhysicalDamage = reader.ReadInt();
                    FireDamage= reader.ReadInt();
                    ColdDamage = reader.ReadInt();
                    PoisonDamage= reader.ReadInt();
                    EnergyDamage = reader.ReadInt();
                    break;
                }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(PhysicalDamage);
            writer.Write(FireDamage);
            writer.Write(ColdDamage);
            writer.Write(PoisonDamage);
            writer.Write(EnergyDamage);
        }

        public override string ToString()
        {
            return String.Format("Dégats: {0}, {1}, {2}, {3}, {4}", 
                PhysicalDamage,
                FireDamage,
                ColdDamage,
                PoisonDamage,
                EnergyDamage
                );
        }
        #endregion

    }
}
