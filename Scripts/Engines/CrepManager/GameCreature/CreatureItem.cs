﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines 
{

    [PropertyObject]
    [Description("Objet")]
    public class CreatureItem
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Type"), Nullable]
        public Type Item { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Couleur")]
        public int Hue { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Calque (Layer)")]
        public Layer Layer { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Valide?")]
        public bool IsValid
        {
            get
            {
                if (Item != null)
                {
                    var type = GameItemType.FindType(Item);
                    if (type != null)
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Creates an instance of the item
        /// </summary>
        /// <returns></returns>
        public Item CreateItem()
        {
            if (Item != null)
            {
                var type = GameItemType.FindType(Item);
                if (type != null)
                {
                    var instance = GameItemType.CreateItem(Item);
                    if (instance != null)
                    {
                        if (Layer != Server.Layer.Talisman)
                            instance.Layer = Server.Layer.Talisman;
                        instance.Hue = Hue;
                        instance.Movable = false;
                    }
                    return instance;
                }
            }
            return null;
        }

        #region Constructors & Serialization
        public CreatureItem()
        {
            Layer = Server.Layer.Talisman;
        }
        public CreatureItem(GenericReader reader)
        {

            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        Item = reader.ReadType();
                        Hue = reader.ReadInt();
                        Layer = (Layer)reader.ReadInt();
                     
                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Item);
            writer.Write(Hue);
            writer.Write((int)Layer);
        }

        public override string ToString()
        {
            if (Item != null)
            {
                var type = GameItemType.FindType(Item);
                if (type != null)
                    return type.Name;
            }
            return "-null-";
        }
        #endregion

    }
}
