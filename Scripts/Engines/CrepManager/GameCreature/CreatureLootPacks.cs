﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;

namespace Server.Engines
{

    [PropertyObject]
    [Description("Loot")]
    public class CreatureLootPacks
    {
        #region Properties

        private WeakChoice<GameLootPack> m_Loot1 = new WeakChoice<GameLootPack>("Loot1InternalName", "LootsList", "InternalName");
        private WeakChoice<GameLootPack> m_Loot2 = new WeakChoice<GameLootPack>("Loot2InternalName", "LootsList", "InternalName");
        private WeakChoice<GameLootPack> m_Loot3 = new WeakChoice<GameLootPack>("Loot3InternalName", "LootsList", "InternalName");
        private WeakChoice<GameLootPack> m_Loot4 = new WeakChoice<GameLootPack>("Loot4InternalName", "LootsList", "InternalName");
        private WeakChoice<GameLootPack> m_Loot5 = new WeakChoice<GameLootPack>("Loot5InternalName", "LootsList", "InternalName");

        public string Loot1InternalName { get; set; }
        public string Loot2InternalName { get; set; }
        public string Loot3InternalName { get; set; }
        public string Loot4InternalName { get; set; }
        public string Loot5InternalName { get; set; }

        private CreatureType Parent;
        public List<GameLootPack> LootsList { get { return ManagerConfig.Loots; } }

        [Description("Pack #1")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameLootPack> Pack1
        {
            get { return m_Loot1; }
            set { }
        }

        [Description("Pack #2")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameLootPack> Pack2
        {
            get { return m_Loot2; }
            set { }
        }

        [Description("Pack #3")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameLootPack> Pack3
        {
            get { return m_Loot3; }
            set { }
        }

        [Description("Pack #4")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameLootPack> Pack4
        {
            get { return m_Loot4; }
            set { }
        }

        [Description("Pack #5")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameLootPack> Pack5
        {
            get { return m_Loot5; }
            set { }
        }


        public GameLootPack[] Packs
        {
            get
            {
                var result = new List<GameLootPack>();
                if (Pack1.HasValue) result.Add(Pack1.Value);
                if (Pack2.HasValue) result.Add(Pack2.Value);
                if (Pack3.HasValue) result.Add(Pack3.Value);
                if (Pack4.HasValue) result.Add(Pack4.Value);
                if (Pack5.HasValue) result.Add(Pack5.Value);
                return result.ToArray();
            }
        }

        public void AddPack(GameLootPack pack)
        {
            if (pack == null)
                return;

            bool contains = Packs
                .Where(t => pack.InternalName == t.InternalName)
                .FirstOrDefault() != null;

            if (contains)
                return;

            if (!Pack1.HasValue) Pack1.SetValueDirectly(this, pack);
            else if (!Pack2.HasValue) Pack2.SetValueDirectly(this, pack);
            else if (!Pack3.HasValue) Pack3.SetValueDirectly(this, pack);
            else if (!Pack4.HasValue) Pack4.SetValueDirectly(this, pack);
            else if (!Pack5.HasValue) Pack5.SetValueDirectly(this, pack);

        }


        #endregion

        #region AutoAssign

        public void AutoAssign()
        {
            int SumMinStats = Parent.Str.Min + Parent.Dex.Min + Parent.Int.Min;

            if (SumMinStats < 250)
                Parent.LootPackLevel = LootPackLevel.VeryPoor;
            else if (SumMinStats < 350)
                Parent.LootPackLevel = LootPackLevel.Poor;
            else if (SumMinStats < 500)
                Parent.LootPackLevel = LootPackLevel.Normal;
            else if (SumMinStats < 900)
                Parent.LootPackLevel = LootPackLevel.Rich;
            else
                Parent.LootPackLevel = LootPackLevel.VeryRich;

            if (Parent.LootPackLevel >= LootPackLevel.Rich)
                AddPack(Loot.Rares);


            if (Parent.Skills.Tactics > 50 && Parent.LootPackLevel >= LootPackLevel.Poor)
                AddPack(Loot.Weapons);

            if (Parent.Skills.Wrestling > 50 && Parent.LootPackLevel >= LootPackLevel.Poor)
            {
                AddPack(Loot.Armors);
                AddPack(Loot.Shields);
            }

            if (Parent.Skills.MagicResist > 30)
                AddPack(Loot.Regs);

            if (Parent.LootPackLevel >= LootPackLevel.Normal)
            {
                AddPack(Loot.Jewelry);
                AddPack(Loot.Gems);
            }

            if ((Parent.Skills.Necromancy > 30 || Parent.Skills.Magery > 30) && Parent.LootPackLevel >= LootPackLevel.Poor)
                AddPack(Loot.Scrolls);

            if ((Parent.Skills.Necromancy > 50 || Parent.Skills.Magery > 50) && Parent.LootPackLevel >= LootPackLevel.Poor)
                AddPack(Loot.Potions);


        }


        public void Generate(Mobile from, Container cont, LootPackLevel level)
        {
            var totalMax = 3 + (((int)level) * 3);
            var alreadyGenerated = 0;

            if (Loot.Everyone != null)
                Loot.Everyone.Generate(from, cont, level);

            foreach (var loot in Packs)
            {
                alreadyGenerated += loot.Generate(from, cont, level);
                if (totalMax <= alreadyGenerated)
                    break;
            }
        }

        #endregion

        #region Constructors & Serialization
        public CreatureLootPacks(CreatureType parent)
        {
            Parent = parent;
        }

        public CreatureLootPacks(GenericReader reader, CreatureType parent)
        {
            Parent = parent;
            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        reader.ReadInt(); // PLACEHOLDER if we need to move the serialization to a list

                        Loot1InternalName = reader.ReadString();
                        Loot2InternalName = reader.ReadString();
                        Loot3InternalName = reader.ReadString();
                        Loot4InternalName = reader.ReadString();
                        Loot5InternalName = reader.ReadString();

                        // Load
                        Pack1.SetValueByKey(this, Loot1InternalName);
                        Pack2.SetValueByKey(this, Loot2InternalName);
                        Pack3.SetValueByKey(this, Loot3InternalName);
                        Pack4.SetValueByKey(this, Loot4InternalName);
                        Pack5.SetValueByKey(this, Loot5InternalName);

                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)5); // PLACEHOLDER if we need to move the serialization to a list 

            writer.Write(Loot1InternalName);
            writer.Write(Loot2InternalName);
            writer.Write(Loot3InternalName);
            writer.Write(Loot4InternalName);
            writer.Write(Loot5InternalName);

        }

        public override string ToString()
        {
            return String.Format("{0} loot pack(s)", Packs.Length);
        }
        #endregion

    }

}
