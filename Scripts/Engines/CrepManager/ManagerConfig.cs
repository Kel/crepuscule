﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Server.Mobiles;
using Server.Engines.Craft;

namespace Server.Engines
{
    public static class ManagerConfig
    {
        public static bool IsConfigured = false;
        public static Dictionary<Type, GameItemType> Items = new Dictionary<Type, GameItemType>();
        public static List<Assembly> Assemblies = new List<Assembly>();
        public static List<Type> ItemTypes = new List<Type>();
        public static List<GameCraftSystem> CraftSystems = new List<GameCraftSystem>();
        public static List<TradeInfo> Trades = new List<TradeInfo>();
        public static List<GameLootPack> Loots = new List<GameLootPack>();
        public static Dictionary<Type, CreatureType> Creatures = new Dictionary<Type, CreatureType>();
        public static List<Type> CreatureTypes = new List<Type>();

        [CallPriority(1)]
        public static void Configure()
        {
            if (IsConfigured) return;
            IsConfigured = true;

            EventSink.WorldPreload += new WorldLoadEventHandler(ItemsSerializer.EventSink_WorldPreload);
            EventSink.WorldPreload += new WorldLoadEventHandler(CreaturesSerializer.EventSink_WorldPreload);

            // Reflect assemblies
            Assemblies.Add(Core.Assembly);
            Assemblies.AddRange(ScriptCompiler.Assemblies);

            // Load all types using the reflection, load all creature types
            foreach (var assembly in Assemblies)
                ItemTypes.AddRange(assembly
                    .GetTypes()
                    .Where(type => type.IsSubclassOf(typeof(Item)))
                    .Where(type => !type.Equals(typeof(Spawner)) && !type.IsSubclassOf(typeof(Spawner)))
                    .OrderBy( type => type.Name ));

            foreach (var assembly in Assemblies)
                CreatureTypes.AddRange(assembly
                    .GetTypes()
                    .Where(type => type.IsSubclassOf(typeof(BaseCreature))));

        }

        /// <summary>
        /// Sets properly the template to the creatures
        /// </summary>
        public static void InitializeItem(Item item)
        {
            if (item != null &&
                !item.Deleted &&
                Items.ContainsKey(item.GetType()))
            {
                Items[item.GetType()].SetToItem(item);
            }
        }

        /// <summary>
        /// Sets properly the template to the creatures
        /// </summary>
        public static void InitializeCreature(BaseCreature creature)
        {
            if (creature != null &&
                !creature.Deleted &&
                Creatures.ContainsKey(creature.GetType()))
            {
                Creatures[creature.GetType()].SetToCreature(creature);
            }
        }
    }

}
