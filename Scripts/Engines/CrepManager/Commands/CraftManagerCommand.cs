using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;
using Server.Engines.Craft;

namespace Server.Scripts.Commands
{

    public class CraftManagerCommand
	{
        private static TradesList TradeList = new TradesList();
        private static CraftSystemsList SystemList = new CraftSystemsList();
        private static LootPacksList LootsList = new LootPacksList();
        

		public static void Initialize()
		{
            Server.Commands.Register("trade", AccessLevel.GameMaster, new CommandEventHandler(Trade_OnCommand));
            Server.Commands.Register("items", AccessLevel.GameMaster, new CommandEventHandler(Items_OnCommand));
            Server.Commands.Register("craft", AccessLevel.GameMaster, new CommandEventHandler(CraftManagerCommand_OnCommand));
            Server.Commands.Register("loot", AccessLevel.GameMaster, new CommandEventHandler(Loots_OnCommand));
            
		}



		[Description( "Permet de voir et �diter les items du jeu")]
        private static void Items_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                e.Mobile.SendGump(new CategorizedAddGump(e.Mobile, CAGCategory.Root.Nodes[0] as CAGCategory, 0));
            }
		}

        [Description("Permet de voir et �diter les syst�mes de craft du jeu")]
        private static void CraftManagerCommand_OnCommand(CommandEventArgs e)
        {
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                ShowCraftGump(e.Mobile);
            }
        }

        [Description("Permet de voir et �diter les listes d'achat/vente du jeu")]
        private static void Trade_OnCommand(CommandEventArgs e)
        {
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                ShowTradeGump(e.Mobile);
            }
        }

        [Description("Permet de voir et �diter les loot packs du jeu")]
        private static void Loots_OnCommand(CommandEventArgs e)
        {
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                ShowLootPacksGump(e.Mobile);
            }
        }


        public static void ShowCraftGump(Mobile mobile)
        {
            var from = mobile as RacePlayerMobile;
            var type = SystemList.GetType();
            var prop = type.GetProperty("Lists");

            from.SendGump(new ASetListGump<GameCraftSystem>(prop, from, SystemList, null, 0, null));
        }

        public static void ShowTradeGump(Mobile mobile)
        {
            var from = mobile as RacePlayerMobile;
            var type = TradeList.GetType();
            var prop = type.GetProperty("Lists");

            from.SendGump(new ASetListGump<TradeInfo>(prop, from, TradeList, null, 0, null));
        }

        public static void ShowLootPacksGump(Mobile mobile)
        {
            var from = mobile as RacePlayerMobile;
            var type = LootsList.GetType();
            var prop = type.GetProperty("Lists");

            from.SendGump(new ASetListGump<GameLootPack>(prop, from, LootsList, null, 0, null));
        }
	}
}
