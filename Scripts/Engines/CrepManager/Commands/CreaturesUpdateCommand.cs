using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;
using Server.Accounting;
using System.Linq;

namespace Server
{
    public class CreaturesUpdateCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("mobsUpdate", AccessLevel.Administrator, new CommandEventHandler(CreaturesUpdate_OnCommand));
		}

		[Description( "Met � jour tous les monstres IG")]
        private static void CreaturesUpdate_OnCommand(CommandEventArgs e)
		{
            UpdateCreatures();
		}

        public static void UpdateCreatures()
        {
            CreaturesSerializer.SaveNeeded = true;
            WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, "Mise � jour des monstres...");
            // First, get all mobs
            var mobs = new List<BaseCreature>();
            foreach (Mobile m in World.Mobiles.Values)
            {
                if (m is BaseCreature && m != null)
                    mobs.Add(m as BaseCreature);
            }

            foreach (BaseCreature m in mobs)
            {
                if (m != null && !m.Deleted)
                    m.EnsureTemplateAssigned();
            }
        }


	}
}
