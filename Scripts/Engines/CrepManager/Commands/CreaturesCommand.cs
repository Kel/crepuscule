using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;

namespace Server.Scripts.Commands
{

	public class CreaturesCommand
	{	
		public static void Initialize()
		{
            Server.Commands.Register("mobs", AccessLevel.Seer, new CommandEventHandler(CreaturesCommand_OnCommand));
            Server.Commands.Register("monsters", AccessLevel.Seer, new CommandEventHandler(CreaturesCommand_OnCommand));
		}

		[Description( "Permet de voir et �diter les cr�atures du jeu")]
        public static void CreaturesCommand_OnCommand(CommandEventArgs e)
		{
            if (e.Mobile != null && e.Mobile is RacePlayerMobile)
            {
                var from = e.Mobile as RacePlayerMobile;
                var actions = new List<GumpAction>();
                
                int i = 0;
                foreach(var creatureType in ManagerConfig.Creatures.Values)
                {
                    if (creatureType.AutoRestat != 100)
                    {
                        actions.Add(new GumpAction(String.Format("{0}, {1}%", creatureType.Name, creatureType.AutoRestat), i, creatureType));
                    }
                    else
                    {
                        actions.Add(new GumpAction(String.Format("{0}", creatureType.Name), i, creatureType));
                    }
                    
                    ++i;
                }

                // Show gump
                //from.CloseGump(typeof(CreaturesGump)); - Do not close, allow multiple
                from.SendGump(new CreaturesGump(from, actions, 0));
            }
		}
	}
}
