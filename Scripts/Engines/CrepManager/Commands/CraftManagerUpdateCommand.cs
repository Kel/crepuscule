using System;
using System.Collections;
using System.IO;
using System.Text;
using Server;
using Server.Items;
using Server.Gumps;
using Server.Mobiles;
using System.Collections.Generic;
using Server.Engines;
using Server.Accounting;
using System.Linq;

namespace Server
{
    public class CraftManagerUpdateCommand
	{
        public static void Initialize()
        {
            Server.Commands.Register("craftUpdate", AccessLevel.Administrator, new CommandEventHandler(CraftManagerUpdate_OnCommand));
        }

        [Description( "Met � jour tous les items IG")]
        private static void CraftManagerUpdate_OnCommand(CommandEventArgs e)
        {
            UpdateCrafts();
        }

        public static void UpdateCrafts()
        {
            ItemsSerializer.SaveNeeded = true;
            WorldContext.BroadcastMessage(AccessLevel.Counselor, 7, "Mise � jour des listes de vente et ressources...");

            // Reinitialize
            TradeInfo.Initialize();
          
            World.Mobiles
                .Values
                .Cast<Mobile>()
                .Where(mobile => mobile.GetType().IsSubclassOf(typeof(BaseVendor)))
                .Select(mobile => mobile as BaseVendor)
                .ToList()
                .ForEach(vendor =>
                {
                    if (!vendor.Trades.HasValue)
                        vendor.LoadSBInfo();
                });
     
            // Rebuild mutalbe lists
            foreach (var itemType in ManagerConfig.Items.Values)
            {
                itemType.Craft.RebuildMutableResourcesList();
            }

            // Cleanup
            foreach (var system in ManagerConfig.CraftSystems)
            {
                foreach (var cat in system.Categories)
                {
                    var backup = new List<GameItemCraft>(cat.Items);
                    cat.Items = new List<GameItemCraft>();
                    foreach (var i in backup)
                    {
                        if (i.CategoryInternalName.Equals(cat.InternalName))
                            cat.Items.Add(i);
                    }
                }
            }

            CAGCategory.RebuildRessourceNode();
        }
        

    }
}
