﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Engines.Craft;
using Server.Items;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject, NoSort]
    [Description("Options de fabrication")]
    public class GameItemCraft
    {
        #region Properties
        private GameItemType m_Parent;
        internal GameItemType Parent { get { return m_Parent; } }
        private Type m_ParentRessource = null;

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Craftable?")]
        public bool Craftable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fondable?")]
        public bool Resmeltable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Schema"), NoSetIfNull]
        public GameItemBlueprint Blueprint { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Capacité")]
        public GameItemCraftSkill Skill { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Consommation")]
        public GameItemCraftConsume Consume { get; set; }

        private WeakChoice<GameCraftSystem> m_System = new WeakChoice<GameCraftSystem>("SystemInternalName", "SystemsList", "InternalName");
        public string SystemInternalName { get; set; }
        public List<GameCraftSystem> SystemsList { get { return ManagerConfig.CraftSystems; } }

        [Description("Catégorie")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameCraftSystem> CraftFolder
        {
            get { return m_System; }
            set { }
        }

        private WeakChoice<GameCraftCategory> m_Category = new WeakChoice<GameCraftCategory>("CategoryInternalName", "CategoriesList", "InternalName");
        public string CategoryInternalName { get; set; }
        public List<GameCraftCategory> CategoriesList
        {
            get
            {
                if (!CraftFolder.HasValue)
                    return new List<GameCraftCategory>(); // Empty
                return CraftFolder.Value.Categories;
            }
        }

        [Description("Sous-Catégorie")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<GameCraftCategory> CraftSubFolder
        {
            get { return m_Category; }
            set
            {
                // Updates to be on the safe side
                AddToCraftSystem();
            }
        }
        


        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Découper en..")]
        public Type ScissorTo { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Découper (nb. Morceaux)")]
        public int ScissorToAmount { get; set; }


        /*----------------------------------------------------------------------------------------*/

        [CommandProperty(AccessLevel.GameMaster), Nullable]
        [Description("Ressource #1")]
        public GameItemCraftRes Ressource1 { get; set; }

        [CommandProperty(AccessLevel.GameMaster), Nullable]
        [Description("Ressource #2")]
        public GameItemCraftRes Ressource2 { get; set; }

        [CommandProperty(AccessLevel.GameMaster), Nullable]
        [Description("Ressource #3")]
        public GameItemCraftRes Ressource3 { get; set; }

        [CommandProperty(AccessLevel.GameMaster), Nullable]
        [Description("Ressource #4")]
        public GameItemCraftRes Ressource4 { get; set; }

        [CommandProperty(AccessLevel.GameMaster), Nullable]
        [Description("Ressource #5")]
        public GameItemCraftRes Ressource5 { get; set; }

        [CommandProperty(AccessLevel.GameMaster), StartGroup("Prérequis de fabrication")]
        [Description("Chaleur?")]
        public bool NeedHeat { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Four?")]
        public bool NeedOven { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Forge?")]
        public bool NeedAnvilAndForge { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Int. Min.")]
        public int NeedInt { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Dex. Min.")]
        public int NeedDex { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Str. Min.")]
        public int NeedStr { get; set; }

        /*----------------------------------------------------------------------------------------*/

        [CommandProperty(AccessLevel.GameMaster), StartGroup("Si c'est une ressource")]
        [Description("Basique?")]
        public bool IsStandard
        {
            get
            {
                if (ParentRessource != null)
                    return false;
                return true;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Attr. Ressource")]
        public CraftAttributeInfo CraftAttributes { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Diff. Récolte")]
        public double HarvestDifficulty { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Ressource Mère"), Nullable]
        public Type ParentRessource
        {
            get { return m_ParentRessource; }
            set
            {
                if (m_ParentRessource != value)
                {
                    if (m_ParentRessource != null)
                    {
                        var mom = GameItemType.FindType(m_ParentRessource);
                        mom.HasSubResources = false;
                        mom.Craft.RebuildMutableResourcesList();
                    }

                    m_ParentRessource = value;

                    if (value != null)
                    {
                        var mom = GameItemType.FindType(value);
                        mom.HasSubResources = true;
                        mom.Craft.RebuildMutableResourcesList();
                    }
                }
            }
        }
        #endregion

        #region Prerequisites Properties
        public GameCraftSystemPrereq Master { get; set; }

        public virtual bool Check(Mobile from, int range)
        {
            bool passes = true;
            if (passes && NeedAnvilAndForge) passes = CheckAnvilAndForge(from, range);
            if (passes && NeedHeat) passes = CheckHeat(from, range);
            if (passes && NeedOven) passes = CheckOven(from, range);

            // Attributes
            if (passes && NeedInt > 0) passes = CheckInt(from);
            if (passes && NeedDex > 0) passes = CheckDex(from);
            if (passes && NeedStr > 0) passes = CheckStr(from);

            // Master ones
            if (Master != null)
            {
                if (passes && Master.NeedAnvilAndForge) passes = CheckAnvilAndForge(from, range);
                if (passes && Master.NeedHeat) passes = CheckHeat(from, range); //message = 1044487; // You must be near a fire source to cook.
                if (passes && Master.NeedOven) passes = CheckOven(from, range); //message = 1044493; // You must be near an oven to bake that.
            }

            return passes;
        }

        #region Check Anvil & Forge

        private static Type typeofAnvil = typeof(AnvilAttribute);
        private static Type typeofForge = typeof(ForgeAttribute);

        public static bool CheckAnvilAndForge(Mobile from, int range)
        {
            var anvil = false;
            var forge = false;

            Map map = from.Map;

            if (map == null)
                return false;

            IPooledEnumerable eable = map.GetItemsInRange(from.Location, range);

            foreach (Item item in eable)
            {
                Type type = item.GetType();

                if (type.IsDefined(typeofAnvil, false))
                    anvil = true;
                else if (item.ItemID == 4015 || item.ItemID == 4016)
                    anvil = true;

                if (type.IsDefined(typeofForge, false))
                    forge = true;
                else if (item.ItemID == 4017 || (item.ItemID >= 6522 && item.ItemID <= 6569))
                    forge = true;

                if (anvil && forge)
                    break;
            }

            eable.Free();

            for (int x = -range; (!anvil || !forge) && x <= range; ++x)
            {
                for (int y = -range; (!anvil || !forge) && y <= range; ++y)
                {
                    Tile[] tiles = map.Tiles.GetStaticTiles(from.X + x, from.Y + y, true);

                    for (int i = 0; (!anvil || !forge) && i < tiles.Length; ++i)
                    {
                        int id = tiles[i].ID & 0x3FFF;

                        if (id == 4015 || id == 4016)
                            anvil = true;
                        else if (id == 4017 || (id >= 6522 && id <= 6569))
                            forge = true;
                    }
                }
            }

            if (!anvil || !forge)
                from.SendLocalizedMessage(1044267); // You must be near an anvil and a forge to smith items.

            return anvil && forge;
        }


        #endregion

        #region Check Heat
        private static int[] heatSources = new int[]
			{
				0x461, 0x48E, // Sandstone oven/fireplace
				0x92B, 0x96C, // Stone oven/fireplace
				0xDE3, 0xDE9, // Campfire
				0xFAC, 0xFAC, // Firepit
				0x184A, 0x184C, // Heating stand (left)
				0x184E, 0x1850, // Heating stand (right)
				0x398C, 0x399F  // Fire field
			};

        private static bool CheckHeat(Mobile from, int range)
        {
            var found = Find(from, heatSources, range);
            if (!found) from.SendLocalizedMessage(1044487); // You must be near a fire source to cook.
            return found;
        }

        #endregion

        #region Check Oven
        private static int[] ovens = new int[]
			{
				0x461, 0x46F, // Sandstone oven
				0x92B, 0x93F  // Stone oven
			};

        private static bool CheckOven(Mobile from, int range)
        {
            var found = Find(from, ovens, range);
            if (!found) from.SendLocalizedMessage(1044493); // You must be near a fire source to cook.
            return found;
        }
        #endregion

        #region Check Int/Dex/Str
        private bool CheckInt(Mobile from)
        {
            if (NeedInt > from.Int)
            {
                from.SendMessage("Vous ne comprenez pas comment fabriquer ceci");
                return false;
            }
            return true;
        }

        private bool CheckDex(Mobile from)
        {
            if (NeedDex > from.Dex)
            {
                from.SendMessage("Vous n'êtes pas suffisament agile pour fabriquer ceci");
                return false;
            }
            return true;
        }

        private bool CheckStr(Mobile from)
        {
            if (NeedStr > from.Str)
            {
                from.SendMessage("Vous n'êtes pas suffisament fort pour fabriquer ceci");
                return false;
            }
            return true;
        }
        #endregion

        #region Find Methods
        private static bool Find(Mobile from, int[] itemIDs, int range)
        {
            Map map = from.Map;

            if (map == null)
                return false;

            IPooledEnumerable eable = map.GetItemsInRange(from.Location, range);

            foreach (Item item in eable)
            {
                if ((item.Z + 16) > from.Z && (from.Z + 16) > item.Z && Find(item.ItemID, itemIDs))
                {
                    eable.Free();
                    return true;
                }
            }

            eable.Free();

            for (int x = -range; x <= range; ++x)
            {
                for (int y = -range; y <= range; ++y)
                {
                    int vx = from.X + x;
                    int vy = from.Y + y;

                    Tile[] tiles = map.Tiles.GetStaticTiles(vx, vy, true);

                    for (int i = 0; i < tiles.Length; ++i)
                    {
                        int z = tiles[i].Z;
                        int id = tiles[i].ID & 0x3FFF;

                        if ((z + 16) > from.Z && (from.Z + 16) > z && Find(id, itemIDs))
                            return true;
                    }
                }
            }

            return false;
        }

        private static bool Find(int itemID, int[] itemIDs)
        {
            bool contains = false;

            for (int i = 0; !contains && i < itemIDs.Length; i += 2)
                contains = (itemID >= itemIDs[i] && itemID <= itemIDs[i + 1]);

            return contains;
        }

        #endregion

        #endregion

        #region Resource Collection

        public bool HasMutableResources
        {
            get
            {
                return Ressource1 != null && Ressource1.ItemType.HasSubResources;
            }
        }

        private List<GameItemType> m_MutableResources = new List<GameItemType>();
        public List<GameItemType> MutableResources { get { return m_MutableResources; } }
        public void RebuildMutableResourcesList()
        {
            m_MutableResources = ManagerConfig.Items.Values
                .Where(item => item.Craft.ParentRessource != null)
                .Where(item => item.Craft.ParentRessource.Equals(Parent.Type))
                .ToList();
        }


        public List<GameItemCraftRes> Ressources
        {
            get
            {
                var result = new List<GameItemCraftRes>();
                if (Ressource1 != null && Ressource1.Type != null) result.Add(Ressource1);
                if (Ressource2 != null && Ressource2.Type != null) result.Add(Ressource2);
                if (Ressource3 != null && Ressource3.Type != null) result.Add(Ressource3);
                if (Ressource4 != null && Ressource4.Type != null) result.Add(Ressource4);
                if (Ressource5 != null && Ressource5.Type != null) result.Add(Ressource5);
                return result;
            }
        }

        public void AddCraftRessource(GameItemCraftRes res)
        {
            if (Ressource1 == null || Ressource1.Type == null) Ressource1 = res;
            else if (Ressource2 == null || Ressource2.Type == null) Ressource2 = res;
            else if (Ressource3 == null || Ressource3.Type == null) Ressource3 = res;
            else if (Ressource4 == null || Ressource4.Type == null) Ressource4 = res;
            else if (Ressource5 == null || Ressource5.Type == null) Ressource5 = res;
        }

        public string GetRessourceString()
        {
            var ress = Ressources;
            if (ress.Count == 0)
                return "";
            return ress
                .Select(r => r.Amount + " " + r.Name)
                .Aggregate((a, b) => a + ", " + b);
        }

        public void Add(GameItemCraftRes ressource)
        {
            if (Ressource1.Type == null) Ressource1 = ressource;
            else if (Ressource2.Type == null) Ressource2 = ressource;
            else if (Ressource3.Type == null) Ressource3 = ressource;
            else if (Ressource4.Type == null) Ressource4 = ressource;
            else if (Ressource5.Type == null) Ressource5 = ressource;
        }
        #endregion

        #region Constructors & Serialization
        public GameItemCraft(GameItemType parent)
        {
            m_Parent = parent;
            Craftable = false;
            Skill = new GameItemCraftSkill();
            Consume = new GameItemCraftConsume(this);
            CraftAttributes = new CraftAttributeInfo();
            Ressource1 = new GameItemCraftRes();
            Ressource2 = new GameItemCraftRes();
            Ressource3 = new GameItemCraftRes();
            Ressource4 = new GameItemCraftRes();
            Ressource5 = new GameItemCraftRes();
            Blueprint = new GameItemBlueprint(parent);
            ScissorToAmount = 1;
        }

        public GameItemCraft(GenericReader reader, GameItemType parent)
        {
            m_Parent = parent;
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Craftable = reader.ReadBool();
                    Skill = new GameItemCraftSkill(reader);
                    Consume = new GameItemCraftConsume(reader, this);
                    CraftAttributes = new CraftAttributeInfo(reader);
                    Blueprint = new GameItemBlueprint(reader, parent);

                    SystemInternalName = reader.ReadString();
                    CategoryInternalName = reader.ReadString();

                    m_ParentRessource = reader.ReadType();
                    HarvestDifficulty = reader.ReadDouble();
                    Resmeltable = reader.ReadBool();
                    ScissorTo = reader.ReadType();
                    ScissorToAmount = reader.ReadInt();

                    NeedHeat = reader.ReadBool();
                    NeedOven = reader.ReadBool();
                    NeedAnvilAndForge = reader.ReadBool();
                    NeedInt = reader.ReadInt();
                    NeedDex = reader.ReadInt();
                    NeedStr = reader.ReadInt();

                    Ressource1 = new GameItemCraftRes(reader);
                    Ressource2 = new GameItemCraftRes(reader);
                    Ressource3 = new GameItemCraftRes(reader);
                    Ressource4 = new GameItemCraftRes(reader);
                    Ressource5 = new GameItemCraftRes(reader);

                    CraftFolder.SetValueByKey(this, SystemInternalName);
                    CraftSubFolder.SetValueByKey(this, CategoryInternalName);
                    break;
                }
            }
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version

            writer.Write(Craftable);
            Skill.Serialize(writer);
            Consume.Serialize(writer);
            CraftAttributes.Serialize(writer);
            Blueprint.Serialize(writer);

            writer.Write(SystemInternalName);
            writer.Write(CategoryInternalName);

            writer.Write(m_ParentRessource);
            writer.Write(HarvestDifficulty);
            writer.Write(Resmeltable);
            writer.Write(ScissorTo);
            writer.Write(ScissorToAmount);

            writer.Write(NeedHeat);
            writer.Write(NeedOven);
            writer.Write(NeedAnvilAndForge);
            writer.Write(NeedInt);
            writer.Write(NeedDex);
            writer.Write(NeedStr);

            if(Ressource1 == null) Ressource1 = new GameItemCraftRes();
            if(Ressource2 == null) Ressource2 = new GameItemCraftRes();
            if(Ressource3 == null) Ressource3 = new GameItemCraftRes();
            if(Ressource4 == null) Ressource4 = new GameItemCraftRes();
            if(Ressource5 == null) Ressource5 = new GameItemCraftRes();

            Ressource1.Serialize(writer);
            Ressource2.Serialize(writer);
            Ressource3.Serialize(writer);
            Ressource4.Serialize(writer);
            Ressource5.Serialize(writer);
        }

        public override string ToString()
        {
            return String.Format("{0}",
                Craftable ? String.Format("Oui, {0}", Skill) : "Non"
                );
        }
        #endregion

        #region GetSuccessChance & GetSuccessChance

        public bool GetSuccessChance(Mobile from, GameItemType typeRes, GameCraftSystem craftSystem, ref int quality)
        {
            return CheckSkills(from, typeRes, craftSystem, ref quality, true, false);
        }

        public bool CheckSkills(Mobile from, GameItemType typeRes, GameCraftSystem craftSystem, ref int quality, bool gainSkills)
        {
            return CheckSkills(from, typeRes, craftSystem, ref quality, gainSkills, false);
        }

        public bool CheckSkills(Mobile from, GameItemType typeRes, GameCraftSystem craftSystem, ref int quality, bool gainSkills, bool invent)
        {
            double chance = GetSuccessChance(from, typeRes, craftSystem, gainSkills);

            // TODO: Use the Craft XP here to vary the quality
            quality = 1;

            if (invent) chance /= 2; // 2 times more difficult to invent the thing

            return (chance >= Utility.RandomDouble());
        }

        public double GetExceptionalChance(GameCraftSystem system, double chance, Mobile from)
        {
            switch (system.ECA)
            {
                default:
                case CraftECA.ChanceMinusSixty: return 0;
                case CraftECA.FiftyPercentChanceMinusTenPercent: return 0;
                case CraftECA.ChanceMinusSixtyToFourtyFive: return 0;
            }
        }

        public double GetSuccessChance(Mobile from, GameItemType typeRes, GameCraftSystem craftSystem, bool gainSkills)
        {
            // TODO: Minou, tu peux regarder ceci?  Faudrait une bonne formule!

            double minSkill = Skill.Range.Min;
            double maxSkill = Skill.Range.Max;
            double valSkill = from.Skills[Skill.Skill].Value;
            double chanceAtMin = Skill.ChanceAtMin;

            var chance = chanceAtMin + ((valSkill - minSkill) / (maxSkill - minSkill) * (1.0 - chanceAtMin));
            if (valSkill >= maxSkill)
                chance = 1.0;

            if (gainSkills) // This is a passive check. Success chance is entirely dependant on the main skill
                from.CheckSkill(Skill.Skill, minSkill, maxSkill);

            return chance;
        }

        #endregion

        #region Helpers
        public void AddToCraftSystem()
        {
            // Do some defensive programming
            if (Craftable && m_System.HasValue && m_Category.HasValue )
            {
                var system = m_System.Value;
                var category = m_Category.Value;
                if (system != null && category != null)
                {
                    // Now, we can add
                    category.Items.Add(this);
                }
            }
        }

        public string GetComplexityString()
        {
        	return String.Format("{0}%", Math.Round(Skill.Range.Min, 2));
            //if (Skill.Range.Max >= 100) return "Sophistiqué";
            //else if (Skill.Range.Max >= 80) return "Complexe";
            //else if (Skill.Range.Max >= 50) return "Normal";
            //else if (Skill.Range.Max >= 30) return "Simple";
            //else return "Très Simple";
        }
        #endregion

        #region Craft Method
        public void Craft(Mobile from, GameCraftSystem craftSystem, GameItemType typeRes, BaseTool tool, bool invent)
        {
            // Just in case
            if (Ressource1 == null) Ressource1 = new GameItemCraftRes();
            if (Ressource2 == null) Ressource2 = new GameItemCraftRes();
            if (Ressource3 == null) Ressource3 = new GameItemCraftRes();
            if (Ressource4 == null) Ressource4 = new GameItemCraftRes();
            if (Ressource5 == null) Ressource5 = new GameItemCraftRes();

            if (from.BeginAction(typeof(GameCraftSystem)))
            {
                var context = craftSystem.GetContext(from);
                var craftItem = Parent;
                bool allRequiredSkills = true;
                double chance = GetSuccessChance(from, typeRes, craftSystem, false);

                if (context != null && allRequiredSkills && chance >= 0.0)
                {
                    if (craftSystem.CanCraft(from, tool, Parent))
                    {
                        object message = null;
                        int maxAmount = 1;
                        var messageString = "";

                        if (Consume.ConsumeRes(from, craftSystem, ConsumeType.None, typeRes, ref maxAmount, ref message))
                        {
                            message = null;

                            if (Consume.ConsumeAttributes(from, ref messageString, false))
                            {
                                if(!invent)
                                    context.OnMade(this.Parent, typeRes);

                                int iMin = craftSystem.CraftEffect.Min;
                                int iMax = (craftSystem.CraftEffect.Max - iMin) + 1;
                                int iRandom = Utility.Random(iMax);
                                iRandom += iMin + 1;
                                if (invent) iRandom *= 3;
                                new InternalTimer(from, craftSystem, Parent, typeRes, tool, iRandom, invent).Start();
                            }
                            else
                            {
                                from.EndAction(typeof(GameCraftSystem));
                                context.ShowGump(message);
                            }
                        }
                        else
                        {
                            from.EndAction(typeof(GameCraftSystem));
                            context.ShowGump(message);
                        }
                    }
                    else
                    {
                        from.EndAction(typeof(GameCraftSystem));
                        context.ShowGump("Impossible de fabriquer ceci");
                    }
                }
                else
                {
                    from.EndAction(typeof(GameCraftSystem));
                    context.ShowGump(1044153); // You don't have the required skills to attempt this item.
                }
            }
            else
            {
                from.SendLocalizedMessage(500119); // You must wait to perform another action
            }
        }

        public void CompleteCraft(int quality, Mobile from, GameCraftSystem craftSystem, GameItemType typeRes, BaseTool tool, bool invent)
        {
            var context = craftSystem.GetContext(from);
            if(context == null)
                return;

            if (!craftSystem.CanCraft(from, tool, Parent))
            {
                if (tool != null && !tool.Deleted && tool.UsesRemaining > 0)
                    context.ShowGump();
                return;
            }

            int maxAmount = 1;
            object checkMessage = null;
            string checkMessageString = null;

            // Not enough resource to craft it (or attributes)
            if (!Consume.ConsumeRes(from, craftSystem, ConsumeType.None, typeRes, ref maxAmount, ref checkMessage))
            {
                context.ShowGumpWithToolCheck(checkMessage);
                return;
            }
            else if (!Consume.ConsumeAttributes(from, ref checkMessageString, false)) 
            {
                context.ShowGumpWithToolCheck(checkMessageString);
                return;
            }

            bool toolBroken = false;

            int ignored = 1;
            int endquality = 1;

            if (CheckSkills(from, typeRes, craftSystem, ref ignored, true, invent))
            {
                // Not enough resource to craft it
                if (!Consume.ConsumeRes(from, craftSystem, ConsumeType.All, typeRes, ref maxAmount, ref checkMessage))
                {
                    context.ShowGumpWithToolCheck(checkMessage);
                    return;
                }
                else if (!Consume.ConsumeAttributes(from, ref checkMessageString, true))
                {
                    context.ShowGumpWithToolCheck(checkMessageString);
                    return;
                }

                // Break tool if needed
                tool.UsesRemaining--;
                if (tool.UsesRemaining < 1)
                    toolBroken = true;
                if (toolBroken)
                    tool.Delete();

                Item item = invent ? null : GameItemType.CreateItem(Parent);

                if (invent && from is RacePlayerMobile)
                {
                    from.SendMessage("Désormais, vous pouvez fabriquer " + Parent.Name);
                    (from as RacePlayerMobile).AddCraftXP(Parent.Type, 100);
                    context.RebuildCounters();
                    context.RebuildList();
                }

                if (item != null)
                {
                    if (invent)
                    {
                        var bp = item as Blueprint;
                        bp.BlueprintOf = Parent.Type;
                    }
                    else
                    {
                        if (maxAmount > 1)
                            item.Amount = maxAmount;
                        item.OnAssignProperties();
                    }

                    if (item is BaseWeapon)
                    {
                        BaseWeapon weapon = (BaseWeapon)item;
                        weapon.Quality = (WeaponQuality)quality;
                        endquality = quality;

                        weapon.PlayerConstructed = true;
                        weapon.CraftResource = typeRes.Type;
                        weapon.Hue = typeRes.Hue;

                        if (tool is BaseRunicTool)
                            ((BaseRunicTool)tool).ApplyAttributesTo(weapon);

                        if (quality == 2)
                        {
                            if (weapon.Attributes.WeaponDamage > 35)
                                weapon.Attributes.WeaponDamage -= 20;
                            else
                                weapon.Attributes.WeaponDamage = 15;
                        }
                        
                    }
                    else if (item is BaseBeverage)
                    {
                        
                        double capacite = (from.Skills.Cooking.Value + from.Skills.TasteID.Value) / 2;

                        BaseBeverage beverage = (BaseBeverage)item;

                        beverage.CraftResource = typeRes.Type;
                        beverage.Hue = typeRes.Hue;

                        if (capacite < 10)
                            beverage.BeverageQuality = BeverageQualityType.Pacotille;
                        else if (capacite < 30)
                            beverage.BeverageQuality = BeverageQualityType.Bon_marche;
                        else if (capacite < 50)
                            beverage.BeverageQuality = BeverageQualityType.Normale;
                        else if (capacite < 70)
                            beverage.BeverageQuality = BeverageQualityType.Savoureuse;
                        else if (capacite < 85)
                            beverage.BeverageQuality = BeverageQualityType.Exceptionnelle;
                        else
                            beverage.BeverageQuality = BeverageQualityType.Extatique;

                    }
                    else if (item is BaseArmor)
                    {
                        BaseArmor armor = (BaseArmor)item;
                        armor.Quality = (ArmorQuality)quality;
                        endquality = quality;

                        armor.PlayerConstructed = true;
                        armor.CraftResource = typeRes.Type;
                        armor.Hue = typeRes.Hue;

                        if (quality == 2)
                            armor.DistributeBonuses((tool is BaseRunicTool ? 6 : 14));

                        if (Core.AOS && tool is BaseRunicTool)
                            ((BaseRunicTool)tool).ApplyAttributesTo(armor);
                    }
                    else if (item is BaseInstrument)
                    {
                        BaseInstrument instrument = (BaseInstrument)item;

                        instrument.Quality = (InstrumentQuality)quality;
                        endquality = quality;
                        instrument.CraftResource = typeRes.Type;
                        instrument.Hue = typeRes.Hue;

                    }
                    else if (item is BaseJewel)
                    {
                        BaseJewel jewel = (BaseJewel)item;
                        jewel.CraftResource = typeRes.Type;
                        jewel.Hue = typeRes.Hue;

                        /*
                         * KEL: important! Get hue from the thingy
                         * that thing for jewels may need improvement, someone should check it later
                         * APOC: Make more sense to change the color with the ingot used.
                         * Most jewels except "Diademe" change their whole color instead of the gem.

						jewel.Resource = CraftResources.GetFromType( resourceType );

						if ( 1 < Ressources.Count )
						{
							resourceType = Ressources.GetAt( 1 ).ItemType;

							if ( resourceType == typeof( StarSapphire ) )
								jewel.GemType = GemType.StarSapphire;
							else if ( resourceType == typeof( Emerald ) )
								jewel.GemType = GemType.Emerald;
							else if ( resourceType == typeof( Sapphire ) )
								jewel.GemType = GemType.Sapphire;
							else if ( resourceType == typeof( Ruby ) )
								jewel.GemType = GemType.Ruby;
							else if ( resourceType == typeof( Citrine ) )
								jewel.GemType = GemType.Citrine;
							else if ( resourceType == typeof( Amethyst ) )
								jewel.GemType = GemType.Amethyst;
							else if ( resourceType == typeof( Tourmaline ) )
								jewel.GemType = GemType.Tourmaline;
							else if ( resourceType == typeof( Amber ) )
								jewel.GemType = GemType.Amber;
							else if ( resourceType == typeof( Diamond ) )
								jewel.GemType = GemType.Diamond;
						}
                         */
                    }
                    else if (item is BaseClothing)
                    {
                        BaseClothing clothing = (BaseClothing)item;
                        clothing.Quality = (ClothingQuality)quality;
                        endquality = quality;
                        clothing.PlayerConstructed = true;
                        clothing.CraftResource = typeRes.Type;
                        clothing.Hue = typeRes.Hue;

                    }
                    else if (item is MapItem)
                    {
                        ((MapItem)item).CraftInit(from);
                    }

                    else if (item is Container)
                    {
                        //Container is not CrepusculeItem so no CraftResource;
                        if (item.Hue == 0)
                        {
                            item.Hue = typeRes.Hue;
                        }

                        if (item is LockableContainer)
                        {
                            if (from.CheckSkill(SkillName.Tinkering, -5.0, 15.0))
                            {
                                LockableContainer cont = (LockableContainer)item;

                                from.SendLocalizedMessage(500636); // Your tinker skill was sufficient to make the item lockable.

                                Key key = new Key(KeyType.Copper, 0);
                                key.OnAssignProperties();
                                key.KeyValue = Key.RandomValue();
                                cont.KeyValue = key.KeyValue;
                                cont.DropItem(key);

                                double tinkering = from.Skills[SkillName.Tinkering].Value;
                                int level = (int)(tinkering * 0.8);

                                cont.RequiredSkill = level - 4;
                                cont.LockLevel = level - 14;
                                cont.MaxLockLevel = level + 35;

                                if (cont.LockLevel == 0)
                                    cont.LockLevel = -1;
                                else if (cont.LockLevel > 95)
                                    cont.LockLevel = 95;

                                if (cont.RequiredSkill > 95)
                                    cont.RequiredSkill = 95;

                                if (cont.MaxLockLevel > 95)
                                    cont.MaxLockLevel = 95;
                            }
                            else
                            {
                                from.SendLocalizedMessage(500637); // Your tinker skill was insufficient to make the item lockable.
                            }
                        }
                    }
                    else if (item is Runebook)
                    {
                        int charges = 5 + quality + (int)(from.Skills[SkillName.Inscribe].Value / 30);
                        endquality = quality;

                        if (charges > 10)
                            charges = 10;

                        ((Runebook)item).MaxCharges = charges;
                    }
                    else if (item is BasePotion)
                    {
                        BasePotion pot = (BasePotion)item;

                        Container pack = from.Backpack;

                        if (pack != null)
                        {
                            Item[] kegs = pack.FindItemsByType(typeof(PotionKeg), true);

                            for (int i = 0; i < kegs.Length; ++i)
                            {
                                PotionKeg keg = kegs[i] as PotionKeg;

                                if (keg == null)
                                    continue;

                                if (keg.Held <= 0 || keg.Held >= 100)
                                    continue;

                                if (keg.Type != pot.PotionEffect)
                                    continue;

                                ++keg.Held;
                                item.Delete();
                                item = new Bottle();

                                endquality = -1; // signal placed in keg

                                break;
                            }
                        }
                    }
                    else if (item is BaseAddonDeed)
                    {
                            ((BaseAddonDeed)item).CraftResource = typeRes.Type;
                            item.Hue = typeRes.Hue;
                    }

                    else if (item.Hue == 0 && !typeRes.Craft.IsStandard)
                    {
                        item.Hue = typeRes.Hue;
                    }



                    if (maxAmount > 0)
                        item.Amount = maxAmount;

                    if (item is CrepusculeItem)
                        (item as CrepusculeItem).OnAfterCraft(typeRes);

                    from.AddToBackpack(item);
                }

                var mes = craftSystem.PlayEndingEffect(from, false, true, toolBroken, invent, endquality, Parent, typeRes);
                context.ShowGumpWithToolCheck(mes);
            }
            else
            {
                ConsumeType consumeType = (this.Consume.UseAllResources ? ConsumeType.All : ConsumeType.Half);
                object message = null;
          
                // Not enough resource to craft it
                if (!Consume.ConsumeRes(from, craftSystem, consumeType, typeRes, ref maxAmount, ref message))
                {
                    context.ShowGumpWithToolCheck(message);
                    return;
                }

                tool.UsesRemaining--;
                if (tool.UsesRemaining < 1)
                    toolBroken = true;
                if (toolBroken)
                    tool.Delete();

    

                // Failed
                var mes = craftSystem.PlayEndingEffect(from, true, true, toolBroken, invent, endquality, Parent, typeRes);
                context.ShowGumpWithToolCheck(mes);
            }
        }


        #endregion

        #region Internal Timer (For Craft)

        private class InternalTimer : Timer
        {
            private Mobile m_From;
            private int m_iCount;
            private int m_iCountMax;
            private GameCraftSystem m_CraftSystem;
            private GameItemType m_CraftItem;
            private GameItemType m_TypeRes;
            private BaseTool m_Tool;
            private bool m_Invent = false;

            public InternalTimer(Mobile from, GameCraftSystem craftSystem, GameItemType craftItem, GameItemType typeRes, BaseTool tool, int iCountMax, bool invent)
                : base(TimeSpan.Zero, TimeSpan.FromSeconds(craftSystem.Delay), iCountMax)
            {
                m_From = from;
                m_CraftItem = craftItem;
                m_iCount = 0;
                m_iCountMax = iCountMax;
                m_CraftSystem = craftSystem;
                m_TypeRes = typeRes;
                m_Tool = tool;
                m_Invent = invent;
            }

            protected override void OnTick()
            {
                m_iCount++;

                m_From.DisruptiveAction();

                if (m_iCount < m_iCountMax)
                {
                    m_CraftSystem.PlayCraftEffect(m_From);
                }
                else
                {
                    m_From.EndAction(typeof(GameCraftSystem));
                    var context = m_CraftSystem.GetContext(m_From);
                    bool badCraft = !m_CraftSystem.CanCraft(m_From, m_Tool, m_CraftItem);

                    if (context == null)
                        return;

                    if (badCraft)
                    {
                        if (m_Tool != null && !m_Tool.Deleted && m_Tool.UsesRemaining > 0)
                            context.ShowGump("Impossible de fabriquer ceci");

                        return;
                    }

                    int quality = 1;
                    m_CraftItem.Craft.CheckSkills(m_From, m_TypeRes, m_CraftSystem, ref quality, false);
                    m_CraftItem.Craft.CompleteCraft(quality, m_From, m_CraftSystem, m_TypeRes, m_Tool, m_Invent);
                }
            }
        }
        #endregion
    }

    #region CraftAttributeInfo
    [PropertyObject]
    [Description("Attributs de Craft")]
    public class CraftAttributeInfo
    {
        [CommandProperty(AccessLevel.GameMaster)]
        public string LabelName { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponFireDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponColdDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponPoisonDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponEnergyDamage { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponDurability { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponLuck { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponGoldIncrease { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int WeaponLowerRequirements { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorPhysicalResist { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorFireResist { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorColdResist { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorPoisonResist { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorEnergyResist { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorDurability { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorLuck { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorGoldIncrease { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorLowerRequirements { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int RunicMinAttributes { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int RunicMaxAttributes { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int RunicMinIntensity { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int RunicMaxIntensity { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ArmorRating { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool IsBlank
        {
            get
            {
                return (
                    WeaponFireDamage == 0 &&
                    WeaponColdDamage == 0 &&
                    WeaponPoisonDamage == 0 &&
                    WeaponEnergyDamage == 0 &&
                    WeaponDurability == 0 &&
                    WeaponLuck == 0 &&
                    WeaponGoldIncrease == 0 &&
                    WeaponLowerRequirements == 0 &&
                    ArmorPhysicalResist == 0 &&
                    ArmorFireResist == 0 &&
                    ArmorColdResist == 0 &&
                    ArmorPoisonResist == 0 &&
                    ArmorEnergyResist == 0 &&
                    ArmorDurability == 0 &&
                    ArmorLuck == 0 &&
                    ArmorGoldIncrease == 0 &&
                    ArmorLowerRequirements == 0 &&
                    RunicMinAttributes == 0 &&
                    RunicMaxAttributes == 0 &&
                    RunicMinIntensity == 0 &&
                    RunicMaxIntensity == 0 &&
                    ArmorRating == 0
               );
            }
        }


        #region Constructors & Serialization
        public CraftAttributeInfo()
        {
        }
        public CraftAttributeInfo(GenericReader reader)
        {
            var version = reader.ReadInt();
            switch(version)
            {
                case 0:
                {
                    LabelName = reader.ReadString();
                    WeaponFireDamage = reader.ReadInt();
                    WeaponColdDamage = reader.ReadInt();
                    WeaponPoisonDamage  = reader.ReadInt();
                    WeaponEnergyDamage  = reader.ReadInt();
                    WeaponDurability  = reader.ReadInt();
                    WeaponLuck  = reader.ReadInt();
                    WeaponGoldIncrease  = reader.ReadInt();
                    WeaponLowerRequirements  = reader.ReadInt();
                    
                    ArmorPhysicalResist  = reader.ReadInt();
                    ArmorFireResist  = reader.ReadInt();
                    ArmorColdResist  = reader.ReadInt();
                    ArmorPoisonResist  = reader.ReadInt();
                    ArmorEnergyResist  = reader.ReadInt();
                    ArmorDurability  = reader.ReadInt();
                    ArmorLuck  = reader.ReadInt();
                    ArmorGoldIncrease  = reader.ReadInt();
                    ArmorLowerRequirements  = reader.ReadInt();
                    ArmorRating = reader.ReadInt();
                    
                    RunicMinAttributes  = reader.ReadInt();
                    RunicMaxAttributes  = reader.ReadInt();
                    RunicMinIntensity  = reader.ReadInt();
                    RunicMaxIntensity  = reader.ReadInt();
                    


                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version

            writer.Write(LabelName);
            writer.Write(WeaponFireDamage);
            writer.Write(WeaponColdDamage);
            writer.Write(WeaponPoisonDamage);
            writer.Write(WeaponEnergyDamage);
            writer.Write(WeaponDurability);
            writer.Write(WeaponLuck);
            writer.Write(WeaponGoldIncrease);
            writer.Write(WeaponLowerRequirements);
            writer.Write(ArmorPhysicalResist);
            writer.Write(ArmorFireResist);
            writer.Write(ArmorColdResist);
            writer.Write(ArmorPoisonResist);
            writer.Write(ArmorEnergyResist);
            writer.Write(ArmorDurability);
            writer.Write(ArmorLuck);
            writer.Write(ArmorGoldIncrease);
            writer.Write(ArmorLowerRequirements);
            writer.Write(ArmorRating);
            writer.Write(RunicMinAttributes);
            writer.Write(RunicMaxAttributes);
            writer.Write(RunicMinIntensity);
            writer.Write(RunicMaxIntensity);
                    
        }

        public override string ToString()
        {
            return "...";
        }

        #endregion
    
    }

    #endregion
}
