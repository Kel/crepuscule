﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Schema"), NoSort]
    public class GameItemBlueprint
    {
        private bool m_Inventable;
        internal GameItemType Parent;

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Disponible?")]
        public bool Available { get { return Buyable || Inventable || Everyone; } }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Pas de schema")]
        public bool Everyone { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Achetable?"), StartGroup("Schema - Fabrication")]
        public bool Buyable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("NPC Vendeurs")]
        public VendorsInfo Vendors { get { return Parent.Buy.Vendors; } }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Prix")]
        public int Price { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Inventable?"), StartGroup("Schema - Invention")]
        public bool Inventable {
            get { return m_Inventable; }
            set
            {
                if (value == true)
                {
                    if (Parent.Craft.CraftFolder.HasValue)
                    {
                        m_Inventable = true;
                        Parent.Craft.CraftFolder.Value.RebuildInventionList();
                        return;
                    }
                }
                m_Inventable = false;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Ajouter (Sac)"), StartGroup("Actions")]
        public PropertyAction Do
        {
            get { return new PropertyAction("(Executer)"); }
            set
            {
                var bp = GameItemType.CreateItem(typeof(Blueprint)) as Blueprint;
                if (bp != null)
                {
                    bp.BlueprintOf = Parent.Type;
                    value.Owner.AddToBackpack(bp);
                }
            }
        }

        
        #region Constructors & Serialization
        public GameItemBlueprint(GameItemType parent)
        {
            Parent = parent;
            Buyable = false;
            Inventable = false;
            Price = 1000;
        }

        public GameItemBlueprint(GenericReader reader, GameItemType parent)
        {
            Parent = parent;
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Buyable = reader.ReadBool();
                    m_Inventable = reader.ReadBool();
                    Everyone = reader.ReadBool();
                    Price = reader.ReadInt();
                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version

            writer.Write(Buyable);
            writer.Write(m_Inventable);
            writer.Write(Everyone);
            writer.Write(Price);
        }

        public override string ToString()
        {
            return String.Format("Schema: " + (Available ? "disponible" : "non disponible"));
        }
        #endregion

    }
}
