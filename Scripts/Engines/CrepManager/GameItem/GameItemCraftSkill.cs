﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Capacité pour Fabriquer")]
    public class GameItemCraftSkill
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Capacité")]
        public SkillName Skill { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Pourcentages")]
        public DoubleRangeValue Range { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Chance au Min.")]
        public double ChanceAtMin { get; set; }
        
        #region Constructors & Serialization
        public GameItemCraftSkill() 
        {
            Skill = SkillName.Tinkering;
            Range = new DoubleRangeValue();
        }
        public GameItemCraftSkill(GenericReader reader)
        {
            
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Skill = (SkillName)reader.ReadInt();
                    Range = reader.ReadDoubleRange();
                    ChanceAtMin = reader.ReadDouble();
                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)Skill);
            writer.Write(Range);
            writer.Write(ChanceAtMin);
        }

        public override string ToString()
        {
            return String.Format("{1}, {0}%",
                Range, CapacitiesConfig.GetName(Skill)
                );
        }
        #endregion

    }
}
