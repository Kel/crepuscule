﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;

namespace Server.Engines
{
    [PropertyObject, NoSort]
    [Description("Objet - Propriétés")]
    public class GameItemType
    {
        #region Properties

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom")]
        public string Name { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public int ItemID { get; set; }

        private int m_Max = 0;
        [CommandProperty(AccessLevel.GameMaster)]
        public int MaxItemID
        {
            get
            {
                return m_Max;
            }
            set
            {
                m_Max = value;
                if (m_Max < ItemID)
                    m_Max = ItemID;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Hue { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public bool RandomHue { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fabrication")]
        public GameItemCraft Craft { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Achat")]
        public GameItemBuyInfo Buy { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Vente")]
        public GameItemSellInfo Sell { get; set; }


        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom de Script"), StartGroup("Objet - Propriétés Avancées")]
        public string ScriptName
        {
            get
            {
                if (Type != null)
                    return Type.Name;
                return "";
            }

        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Auto-Couleur?")]
        public bool HueFromRes { get; set; }
        public bool HasSubResources { get; set; }

        internal List<IItemTemplate> m_Templates = new List<IItemTemplate>();
        [PropertyObjectsListAttribute(AccessLevel.GameMaster)]
        public List<IItemTemplate> Templates
        {
            get { return m_Templates; }
            set { m_Templates = value; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Ajouter (Sac)"), StartGroup("Actions")]
        public PropertyAction Do
        {
            get { return new PropertyAction("(Executer)"); }
            set
            {
                var item = GameItemType.CreateItem(Type);
                if (item != null)
                    value.Owner.AddToBackpack(item);
            }
        }


        // Internal props
        public Type Type { get; set; }
        #endregion

        #region Templating
        public static List<string> NotFoundTemplates = new List<string>();
        private static List<Type> TemplateTypes = new List<Type>();
        public static void InitializeTemplateTypes()
        {
            if (TemplateTypes.Count == 0)
            {
                TemplateTypes = ScriptCompiler.Assemblies
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => !type.IsInterface)
                    .Where(type => type.GetInterfaces().Contains(typeof(IItemTemplate)))
                    .ToList();
            }
        }
        #endregion

        #region Constructors & Serialization
        public GameItemType(Type type) 
        {
            Craft = new GameItemCraft(this);
            Buy = new GameItemBuyInfo();
            Sell = new GameItemSellInfo();
            Name = type.Name;
            Type = type;
            HueFromRes = true;

            // Templating
            InitializeTemplateTypes();
            m_Templates = new List<IItemTemplate>();
            foreach (var t in TemplateTypes)
            {
                m_Templates.Add(Activator.CreateInstance(t) as IItemTemplate);
            }
        }
        public GameItemType(GenericReader reader)
        {
            Deserialize(reader);
        }

        public void Deserialize(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch (version)
            {
                case 1:
                    {
                        Type = reader.ReadType();
                        ItemID = reader.ReadInt();
                        MaxItemID = reader.ReadInt();
                        Hue = reader.ReadInt();
                        RandomHue = reader.ReadBool();
                        Name = reader.ReadString();
                        HueFromRes = reader.ReadBool();
                        Craft = new GameItemCraft(reader, this);
                        Buy = new GameItemBuyInfo(reader);
                        Sell = new GameItemSellInfo(reader);
                        HasSubResources = reader.ReadBool();

                        m_Templates = new List<IItemTemplate>();
                        var subTemplates = reader.ReadInt();
                        for (int i = 0; i < subTemplates; ++i)
                        {
                            var subTemplateStr = reader.ReadString();
                            var subTemplateType = Type.GetType(subTemplateStr);
                            if (subTemplateType == null)
                            {
                                if (!NotFoundTemplates.Contains(subTemplateStr))
                                    NotFoundTemplates.Add(subTemplateStr);
                                //Console.WriteLine("Error occured on template loading, template " + subTemplateStr + " was not found.");
                                //throw new Exception("Error occured on template loading, template " + subTemplateStr + " was not found.");

                                bool endMarkerDetected = false;
                                int zerosCount = 0;
                                while (!endMarkerDetected)
                                {
                                    var b = (int)(reader.ReadByte());
                                    if (b == 0) zerosCount = 0;
                                    if (b != zerosCount) zerosCount = 0;
                                    else
                                    {
                                        zerosCount++;
                                        if (zerosCount == 10)
                                            endMarkerDetected = true;
                                    }
                                }

                            }
                            else
                            {
                                var t = Activator.CreateInstance(subTemplateType) as IItemTemplate;
                                t.Deserialize(reader);
                                m_Templates.Add(t);

                                // ending marker
                                for (int j = 0; j < 10; j++)
                                    reader.ReadByte();
                            }
                        }

                        break;
                    }
                case 0:
                    {
                        Type = reader.ReadType();
                        ItemID = reader.ReadInt();
                        Hue = reader.ReadInt();
                        Name = reader.ReadString();
                        HueFromRes = reader.ReadBool();
                        Craft = new GameItemCraft(reader, this);
                        Buy = new GameItemBuyInfo(reader);
                        Sell = new GameItemSellInfo(reader);
                        HasSubResources = reader.ReadBool();

                        m_Templates = new List<IItemTemplate>();
                        var subTemplates = reader.ReadInt();
                        for (int i = 0; i < subTemplates; ++i)
                        {
                            var subTemplateStr = reader.ReadString();
                            var subTemplateType = Type.GetType(subTemplateStr);
                            if (subTemplateType == null)
                            {
                                if (!NotFoundTemplates.Contains(subTemplateStr))
                                    NotFoundTemplates.Add(subTemplateStr);
                                //Console.WriteLine("Error occured on template loading, template " + subTemplateStr + " was not found.");
                                //throw new Exception("Error occured on template loading, template " + subTemplateStr + " was not found.");

                                bool endMarkerDetected = false;
                                int zerosCount = 0;
                                while (!endMarkerDetected)
                                {
                                    var b = (int)(reader.ReadByte());
                                    if (b == 0) zerosCount = 0;
                                    if (b != zerosCount) zerosCount = 0;
                                    else
                                    {
                                        zerosCount++;
                                        if (zerosCount == 10)
                                            endMarkerDetected = true;
                                    }
                                }

                            }
                            else
                            {
                                var t = Activator.CreateInstance(subTemplateType) as IItemTemplate;
                                t.Deserialize(reader);
                                m_Templates.Add(t);

                                // ending marker
                                for (int j = 0; j < 10; j++)
                                    reader.ReadByte();
                            }
                        }

                        break;
                    }
            }

            // Templating
            InitializeTemplateTypes();
            if (m_Templates.Count < TemplateTypes.Count)
            {
                foreach (var t in TemplateTypes)
                {
                    var contains = m_Templates
                        .Where(template => template.GetType().FullName == t.FullName)
                        .FirstOrDefault() != null;
                    if (!contains)
                    {
                        m_Templates.Add(Activator.CreateInstance(t) as IItemTemplate);
                    }
                }
            }
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)1); //version
            writer.Write(Type);
            writer.Write(ItemID);
            writer.Write(MaxItemID);
            writer.Write(Hue);
            writer.Write(RandomHue);
            writer.Write(Name);
            writer.Write(HueFromRes);
            Craft.Serialize(writer);
            Buy.Serialize(writer);
            Sell.Serialize(writer);
            writer.Write(HasSubResources);

            writer.Write(m_Templates.Count);
            for (int i = 0; i < m_Templates.Count; ++i)
            {
                var templateType = m_Templates[i].GetType();
                writer.Write(templateType.FullName);
                m_Templates[i].Serialize(writer);

                // ending marker
                for(int j=0;j<10;j++)
                    writer.Write((byte)j);
            }
        }

        public override string ToString()
        {
            return Name;
        }
        #endregion

        #region FindType & CreateItem
        public static GameItemType FindType(Item item)
        {
            if (item == null) return null;
            return FindType(item.GetType());
        }

        public static GameItemType FindType(Type itemType)
        {
            if (itemType == null) return null;
            if (ManagerConfig.Items.ContainsKey(itemType))
                return ManagerConfig.Items[itemType];
            return null;
        }

        public static Item CreateItem(Type itemType)
        {
            if (itemType != null)
            {
                var item = Activator.CreateInstance(itemType) as Item;
                //if (item != null) item.OnAssignProperties();
                return item;
            }
            return null;
        }

        public static Item CreateItem(GameItemType itemType)
        {
            if (itemType != null && itemType.Type != null)
            {
                var item = Activator.CreateInstance(itemType.Type) as Item;
                //if (item != null) item.OnAssignProperties();
                return item;
            }
            return null;
        }

        public static int GetHue(CrepusculeItem item)
        {
            var type = item.GetItemType();
            if (type != null)
                return type.Hue;
            return 0;
        }

        public static int GetHue(Type item)
        {
            var type = FindType(item);
            if (type != null)
                return type.Hue;
            return 0;
        }
        public static int GetHue(GameItemType itemType)
        {
            if (itemType != null)
                return itemType.Hue;
            return 0;
        }
        #endregion

        #region SetToItem & SetFromItem
        public void SetToItem(Item item)
        {
            item.Name = Name;
            if (MaxItemID > ItemID)
            {
                item.ItemID = Utility.Random(ItemID, (MaxItemID - ItemID) + 1);
            }
            else item.ItemID = ItemID;
            if (RandomHue == true)
            {
                item.Hue = Utility.RandomDyedHue();
            }
            else item.Hue = Hue;

            foreach (var template in m_Templates)
                template.ApplyTemplate(item);
        }

        public void SetFromItem(Item item)
        {
            Type = item.GetType();
            Name = item.Name;
            ItemID = item.ItemID;
            Hue = item.Hue;

            if (Type.IsSubclassOf(typeof(BaseArmor)) || Type.IsSubclassOf(typeof(BaseWeapon)))
                HueFromRes = true;

            if (item.LabelNumber > 0 && String.IsNullOrEmpty(Name))
                Name = ClilocHelper.GetText(item.LabelNumber, item.Name);
            if(Name != null)
                Name = Name.ToTitleCase(); 

            foreach (var template in m_Templates)
            {
                if (template is IItemMakeTemplate)
                    (template as IItemMakeTemplate).MakeTemplate(item);
            }
        }

        public static object MakeTemplate(Type itemType)
        {
            var instance = Activator.CreateInstance(itemType);
            if (instance != null && instance is Item)
            {
                var item = instance as Item;
                var type = new GameItemType(itemType);
                ManagerConfig.Items[itemType] = type;
                type.SetFromItem(item);
                item.Delete();

                Console.WriteLine("New item detected: " + itemType.Name);
            }
            return instance;
        }


        public void Reset()
        {
            try
            {
                if (Type != null)
                {
                    var instance = Activator.CreateInstance(Type);
                    if (instance is Item)
                    {
                        var item = instance as Item;
                        this.SetFromItem(item);
                        item.Delete();
                    }
                }
            }
            catch { }
        }
        #endregion

    }

    #region VendorsInfo Property Object
    [PropertyObject]
    [Description("PNJ Vendeurs")]
    public class VendorsInfo
    {

        private WeakChoice<TradeInfo> m_Trade1 = new WeakChoice<TradeInfo>("Trade1InternalName", "TradesList", "InternalName");
        private WeakChoice<TradeInfo> m_Trade2 = new WeakChoice<TradeInfo>("Trade2InternalName", "TradesList", "InternalName");
        private WeakChoice<TradeInfo> m_Trade3 = new WeakChoice<TradeInfo>("Trade3InternalName", "TradesList", "InternalName");
        private WeakChoice<TradeInfo> m_Trade4 = new WeakChoice<TradeInfo>("Trade4InternalName", "TradesList", "InternalName");
        private WeakChoice<TradeInfo> m_Trade5 = new WeakChoice<TradeInfo>("Trade5InternalName", "TradesList", "InternalName");

        public string Trade1InternalName { get; set; }
        public string Trade2InternalName { get; set; }
        public string Trade3InternalName { get; set; }
        public string Trade4InternalName { get; set; }
        public string Trade5InternalName { get; set; }

        public List<TradeInfo> TradesList { get { return ManagerConfig.Trades; } }

        [Description("Liste #1")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Vendor1
        {
            get { return m_Trade1; }
            set { }
        }

        [Description("Liste #2")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Vendor2
        {
            get { return m_Trade2; }
            set { }
        }

        [Description("Liste #3")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Vendor3
        {
            get { return m_Trade3; }
            set { }
        }

        [Description("Liste #4")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Vendor4
        {
            get { return m_Trade4; }
            set { }
        }

        [Description("Liste #5")]
        [CommandProperty(AccessLevel.GameMaster)]
        public WeakChoice<TradeInfo> Vendor5
        {
            get { return m_Trade5; }
            set { }
        }


        public TradeInfo[] Vendors
        {
            get
            {
                var result = new List<TradeInfo>();
                if (Vendor1.HasValue) result.Add(Vendor1.Value);
                if (Vendor2.HasValue) result.Add(Vendor2.Value);
                if (Vendor3.HasValue) result.Add(Vendor3.Value);
                if (Vendor4.HasValue) result.Add(Vendor4.Value);
                if (Vendor5.HasValue) result.Add(Vendor5.Value);
                return result.ToArray();
            }
        }

        public void AddVendor(TradeInfo trade)
        {
            if (trade == null)
                return;

            bool contains = Vendors
                .Where(t => trade.InternalName == t.InternalName)
                .FirstOrDefault() != null;

            if (contains)
                return;

                 if (!Vendor1.HasValue) Vendor1.SetValueDirectly(this, trade);
            else if (!Vendor2.HasValue) Vendor2.SetValueDirectly(this, trade);
            else if (!Vendor3.HasValue) Vendor3.SetValueDirectly(this, trade);
            else if (!Vendor4.HasValue) Vendor4.SetValueDirectly(this, trade);
            else if (!Vendor5.HasValue) Vendor5.SetValueDirectly(this, trade);

        }

        
        #region Constructors & Serialization
        public VendorsInfo() 
        { 

        }

        public VendorsInfo(GenericReader reader)
        {

            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        reader.ReadInt(); // PLACEHOLDER if we need to move the serialization to a list

                        Trade1InternalName = reader.ReadString();
                        Trade2InternalName = reader.ReadString();
                        Trade3InternalName = reader.ReadString();
                        Trade4InternalName = reader.ReadString();
                        Trade5InternalName = reader.ReadString();

                        // Load
                        Vendor1.SetValueByKey(this, Trade1InternalName);
                        Vendor2.SetValueByKey(this, Trade2InternalName);
                        Vendor3.SetValueByKey(this, Trade3InternalName);
                        Vendor4.SetValueByKey(this, Trade4InternalName);
                        Vendor5.SetValueByKey(this, Trade5InternalName);

                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write((int)5); // PLACEHOLDER if we need to move the serialization to a list 

            writer.Write(Trade1InternalName);
            writer.Write(Trade2InternalName);
            writer.Write(Trade3InternalName);
            writer.Write(Trade4InternalName);
            writer.Write(Trade5InternalName);

        }

        public override string ToString()
        {
            return String.Format("Chez {0} PNJ(s)", Vendors.Length );
        }
        #endregion

    }
    #endregion

    public interface IItemTemplate
    {
        void ApplyTemplate(Item item);
        void Serialize(GenericWriter writer);
        void Deserialize(GenericReader reader);
    }

    public interface IItemMakeTemplate
    {
        void MakeTemplate(Item item);
    }

}
