﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using System.Threading;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Hériter Objet"), NoSort]
    public class InheritFromScriptInfo
    {
        private Mobile Owner;

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Type à Hériter")]
        public Type Type { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom (Script)")]
        public string Name { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom (Visible)")]
        public string NameNormal { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Raison d'Ajout")]
        public string Reason { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Hériter"), StartGroup("Actions")]
        public PropertyAction Do
        {
            get { return new PropertyAction("(Executer)"); }
            set
            {
                if (String.IsNullOrEmpty(Reason))
                {
                    value.Owner.SendMessage("Vous devez préciser une raison d'ajout");
                    return;
                }

                if (String.IsNullOrEmpty(NameNormal))
                {
                    value.Owner.SendMessage("Vous devez préciser un nom");
                    return;
                }

                try
                {
                    Owner = value.Owner;
                    ScriptEmitter.InheritFrom(Type, Name, NameNormal,  value.Owner);

                    // Log async
                    var Thread = new ThreadStart(LogOnForum);
                    Thread.Invoke();
                }
                catch (Exception ex)
                {
                    value.Owner.SendMessage(ex.Message);
                }
            }
        }

        public void LogOnForum()
        {
            if (Owner == null) return;
            if (Owner.Account.ToString().Equals("c0r3")) return; // So I wont spam everyone :)

            var name = Name;
            var from = Owner;
            string message = "";
            string title = "" ;
            

            if (Type.IsSubclassOf(typeof(Item)))
            {
                title = "Objet: " + NameNormal;
            }
            else if (Type.IsSubclassOf(typeof(Mobile)))
            {
                title = "Créature: " + NameNormal;
            }

            if (!String.IsNullOrEmpty(title))
            {
                message += "<i>";
                message += String.Format("<span style=\"font-size: 180%; line-height: normal; font-weight: bold; color: black\">{0}</span><br /><br />", title);
                message += String.Format("Créé par: {0} ({1})<br />", ClearString(Owner.Name), ClearString(Owner.Account.ToString()));
                message += String.Format("Nom du script: {0}<br />", ClearString(Name));
                message += String.Format("Raison de la création: {0}<br />", ClearString(Reason));
                message += "</i>";

                CrepusculeWeb.WS.Service.PostWithScribe("PASSWORD", "148", ClearString(title), message);
            }
        }

        private static string ClearString(string source)
        {
            return source.Replace("'", "\'");
        }
        
        #region Constructors & Serialization
        public InheritFromScriptInfo(Type type, string name)
        {
            Type = type;
            Name = name;

            if (Type.IsSubclassOf(typeof(Item)) &&  GameItemType.FindType(type) != null)
            {
                NameNormal = GameItemType.FindType(type).Name;
            }
            else if (Type.IsSubclassOf(typeof(Mobile)) && CreatureType.FindType(type) != null)
            {
                NameNormal = CreatureType.FindType(type).Name;
            }

        }

        public InheritFromScriptInfo()
        {

        }

        public override string ToString()
        {
            return "Hériter Objet";
        }
        #endregion

    }
}
