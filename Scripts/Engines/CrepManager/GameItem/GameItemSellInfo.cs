﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Informations de Vente")]
    public class GameItemSellInfo
    {

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Vendable?")]
        public bool Sellable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Prix")]
        public int Price { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("NPC Acheteurs")]
        public VendorsInfo Vendors { get; set; }
        
        #region Constructors & Serialization
        public GameItemSellInfo()
        {
            Sellable = false;
            Price = 1;
            Vendors = new VendorsInfo();
        }

        public GameItemSellInfo(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Sellable = reader.ReadBool();
                    Vendors = new VendorsInfo(reader);
                    Price = reader.ReadInt();
                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Sellable);
            Vendors.Serialize(writer);
            writer.Write(Price);
        }

        public override string ToString()
        {
            return String.Format("Vendable: {0}",
                Sellable ? String.Format("Oui, {0}po", Price) : "Non"
                );
        }
        #endregion

    }
}
