﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;
using Server.Engines.Craft;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Consommation")]
    public class GameItemCraftConsume
    {
        internal GameItemCraft Parent;

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fabriquer en Lot")]
        public bool UseAllResources { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("PV")]
        public int Hits { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fatigue")]
        public int Stam { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Mana")]
        public int Mana { get; set; }
        
        #region Constructors & Serialization
        public GameItemCraftConsume(GameItemCraft parent) 
        {
            Parent = parent;
            UseAllResources = false;
            Hits = 0;
            Stam = 5;
            Mana = 0;
        }
        public GameItemCraftConsume(GenericReader reader, GameItemCraft parent)
        {
            Parent = parent;
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    UseAllResources = reader.ReadBool();
                    Hits = reader.ReadInt();
                    Stam = reader.ReadInt();
                    Mana = reader.ReadInt();
                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(UseAllResources);
            writer.Write(Hits);
            writer.Write(Stam);
            writer.Write(Mana);
        }

        public override string ToString()
        {
            return String.Format("{0}{1}{2}",
                Hits > 0 ? String.Format("{0} pv ", Hits) : "",
                Stam > 0 ? String.Format("{0} fatigue ", Stam) : "",
                Mana > 0 ? String.Format("{0} mana", Mana) : ""
                );
        }
        #endregion

        #region Type Tables
        private static Type[][] m_TypesTable = new Type[][]
			{
				new Type[]{ typeof( Log ), typeof( Board ) },
				new Type[]{ typeof( Leather ), typeof( Hides ) },
				new Type[]{ typeof( SpinedLeather ), typeof( SpinedHides ) },
				new Type[]{ typeof( HornedLeather ), typeof( HornedHides ) },
				new Type[]{ typeof( BarbedLeather ), typeof( BarbedHides ) },
				new Type[]{ typeof( BlankMap ), typeof( BlankScroll ) },
				new Type[]{ typeof( Cloth ), typeof( UncutCloth ) },
				new Type[]{ typeof( CheeseWheel ), typeof( CheeseWedge ) }
			};

        private static Type[] m_ColoredItemTable = new Type[]
			{
				typeof( BaseWeapon ), typeof( BaseArmor ), typeof( BaseClothing ),
				typeof( BaseJewel )
			};

        private static Type[] m_ColoredResourceTable = new Type[]
			{
				typeof( BaseIngot ), typeof( BaseOre ),
				typeof( BaseLeather ), typeof( BaseHides ),
				typeof( UncutCloth ), typeof( Cloth ),
				typeof( BaseGranite ), typeof( BaseScales )
			};
        #endregion

        #region Consume Quantity

        /// <summary>
        /// Consumes the quantity or returns the type of the insuficient resource
        /// </summary>
        public GameItemType ConsumeQuantity(Container cont, Dictionary<GameItemType, int> typesAndAmounts)
        {
            var itemsCanConsume = new Dictionary<GameItemType, List<Pair<Item, int>>>();

            // Check if we have everything and construct the list
            foreach(var typeToConsume in typesAndAmounts.Keys)
            {
                var amountToConsume = typesAndAmounts[typeToConsume];
                var foundItems = cont.FindItemsByType(typeToConsume.Type, true);
                var total = 0;

                // Sublist
                var subList = new List<Pair<Item, int>>();
                if (!itemsCanConsume.ContainsKey(typeToConsume))
                {
                    itemsCanConsume.Add(typeToConsume, subList);
                    foreach (var item in foundItems)
                    {
                        var hq = item as IHasQuantity;
                        if (hq == null)
                        {
                            total += item.Amount;
                            subList.Add(new Pair<Item, int>(item, item.Amount));
                        }
                        else
                        {
                            if (hq is BaseBeverage && ((BaseBeverage)hq).Content != BeverageType.Water)
                                continue;

                            total += hq.Quantity;
                            subList.Add(new Pair<Item, int>(item, hq.Quantity));
                        }
                    }

                    // Ok, not enough 
                    if (total < amountToConsume)
                        return typeToConsume;
                }
            }

            // Now, we can consume freely
            foreach (var typeToConsume in typesAndAmounts.Keys)
            {
                var amountToConsume = typesAndAmounts[typeToConsume];
                var amountConsumed = 0;
                var sublist = itemsCanConsume[typeToConsume];

                foreach (var pair in sublist)
                {
                    var stillNeed = amountToConsume - amountConsumed;
                    var item = pair.First;
                    var hq = item as IHasQuantity;

                    // We got enough, next please
                    if (stillNeed <= 0)
                        break;

                    if (hq == null)
                    {
                        if (pair.Second < stillNeed)
                        {
                            item.Delete();
                            amountConsumed += pair.Second;
                        }
                        else
                        {
                            item.Consume(stillNeed);
                            amountConsumed += stillNeed;
                            break;
                        }
                    }
                    else
                    {
                        if (hq is BaseBeverage && ((BaseBeverage)hq).Content != BeverageType.Water)
                            continue;

                        if (pair.Second < stillNeed)
                        {
                            hq.Quantity -= pair.Second;
                            amountConsumed += pair.Second;
                        }
                        else
                        {
                            hq.Quantity -= stillNeed;
                            amountConsumed += stillNeed;
                            break;
                        }
                    }
                }
            }

            return null;
        }

        public bool IsQuantityType(IEnumerable<GameItemType> types)
        {
            foreach (var type in types)
            {
                if (typeof(IHasQuantity).IsAssignableFrom(type.Type))
                    return true;
            }
            return false;
        }


        public int GetQuantity(Container cont, GameItemType type)
        {
            var items = cont.FindItemsByType(type.Type, true);
            int amount = 0;

            for (int i = 0; i < items.Length; ++i)
            {
                IHasQuantity hq = items[i] as IHasQuantity;

                if (hq == null)
                {
                    amount += items[i].Amount;
                }
                else
                {
                    if (hq is BaseBeverage && ((BaseBeverage)hq).Content != BeverageType.Water)
                        continue;

                    amount += hq.Quantity;
                }
            }

            return amount;
        }
        #endregion

        #region Consume Ressources


        //public bool ConsumeRes(Mobile from, Type typeRes, GameCraftSystem craftSystem, ref int resHue, ref int maxAmount, ConsumeType consumeType, ref object message)
        public bool ConsumeRes(Mobile from, GameCraftSystem craftSystem, ConsumeType consumeType, GameItemType mutableResource, ref int maxAmount, ref object message)
        {
            Container ourPack = from.Backpack;

            if (ourPack == null)
                return false;

            // Initialize
            GameItemType ItemType = Parent.Parent;
            GameItemCraft Craft = Parent;
            GameItemCraftConsume Consume = Craft.Consume;
            GameItemType InsufficientResource = null;

            bool UseAllRes = Consume.UseAllResources;

            // Check craft prerequisites
            if (!Craft.Check(from, 2))
                return false;
            
            // List of resources to consume (type + amount)
            var resourcesToConsume = new Dictionary<GameItemType, int>();

            // Resource Mutation, only #1st one
            var skipFirst = false;
            if (mutableResource != null && mutableResource.Craft.ParentRessource != null)
            {
                if (Craft.Ressource1.Type == mutableResource.Craft.ParentRessource)
                {
                    resourcesToConsume.Add(mutableResource, Craft.Ressource1.Amount);
                    skipFirst = true;
                }
                else
                {
                    message = 502925; // You don't have the resources required to make that item.
                    return false;
                }
            }

            // Add the other (non-mutable) resources
            var otherRes = Craft.Ressources;
            for (int i = 0; i < otherRes.Count; ++i)
            {
                if ((i == 0 && skipFirst) || otherRes[i].ItemType == null)
                    continue;

                resourcesToConsume.Add(otherRes[i].ItemType, otherRes[i].Amount);
            }


            maxAmount = 1;
            if (UseAllRes) // We need to calculate the max amount we can consume
            {
                maxAmount = 100;
                foreach (var res in resourcesToConsume.Keys)
                    maxAmount = Math.Min(maxAmount, (int)Math.Floor((double)GetQuantity(ourPack, res) / resourcesToConsume[res]));

                if (maxAmount > 1)
                {
                    var newCol = new Dictionary<GameItemType, int>();
                    // Then, multiply the quantity per amount
                    foreach (var res in resourcesToConsume.Keys)
                        newCol.Add(res, resourcesToConsume[res] * maxAmount);
                    resourcesToConsume = newCol;
                }
            }

            // Consume ALL
            if (consumeType == ConsumeType.All)
            {
                // Why oh Why all of this? 
                //if (IsQuantityType(resourcesToConsume.Keys))
                //    InsufficientResource = ConsumeQuantity(ourPack, resourcesToConsume);
                //else
                //    index = ourPack.ConsumeTotalGrouped(types, amounts, true, new OnItemConsumed(OnResourceConsumed), new CheckItemGroup(CheckHueGrouping));

                InsufficientResource = ConsumeQuantity(ourPack, resourcesToConsume);
            }
            else if (consumeType == ConsumeType.Half) // Consume Half ( for use all resource craft type )
            {
                var keys = resourcesToConsume.Keys.ToList();
                for(int i= 0; i< keys.Count; ++i)
                {
                    var res = keys[i];
                    resourcesToConsume[res] /= 2;
                    if (resourcesToConsume[res] < 1)
                        resourcesToConsume[res] = 1;
                }

                InsufficientResource = ConsumeQuantity(ourPack, resourcesToConsume);
            }
            else // ConsumeType.None ( it's basicaly used to know if the crafter has enough resource before starting the process )
            {
                foreach (var res in resourcesToConsume.Keys)
                {
                    if (GetQuantity(ourPack, res) < resourcesToConsume[res])
                    {
                        InsufficientResource = res;
                        break;
                    }
                }
            }

            // Something is missing...
            if (InsufficientResource != null)
            {
                var res = Craft.Ressources
                    .Where(r => r.ItemType == InsufficientResource)
                    .FirstOrDefault();

                if (res == null)
                    message = 502925; // You don't have the resources required to make that item.
                else
                    message = res.Message;
                

                return false;
            }
            return true;

        }

        #endregion

        #region Consume Attributes

        public bool ConsumeAttributes(Mobile from, ref string message, bool consume)
        {
            bool consumMana = false;
            bool consumHits = false;
            bool consumStam = false;

            if (Hits > 0 && from.Hits < Hits)
            {
                message = "Vous n'avez pas assez de vie pour faire ça.";
                return false;
            }
            else
            {
                consumHits = consume;
            }

            if (Mana > 0 && from.Mana < Mana)
            {
                message = "Vous n'avez pas assez de mana pour faire ça.";
                return false;
            }
            else
            {
                consumMana = consume;
            }

            if (Stam > 0 && from.Stam < Stam)
            {
                message = "Vous n'avez pas assez d'énergie pour faire ça.";
                return false;
            }
            else
            {
                consumStam = consume;
            }

            if (consumMana)
                from.Mana -= Mana;

            if (consumHits)
                from.Hits -= Hits;

            if (consumStam)
                from.Stam -= Stam;

            return true;
        }
        #endregion
    }
}
