﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Informations D'Achat")]
    public class GameItemBuyInfo : GenericBuyInfo
    {

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Achetable?")]
        public bool Buyable { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("NPC Vendeurs")]
        public VendorsInfo Vendors { get; set; }

        
        #region Constructors & Serialization
        public GameItemBuyInfo()
        {
            Buyable = false;
            Vendors = new VendorsInfo();
            Name = "";
        }

        public GameItemBuyInfo(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Buyable = reader.ReadBool();
                    Vendors = new VendorsInfo(reader);
                    Name = reader.ReadString();
                    Price = reader.ReadInt();
                    Amount = reader.ReadInt();
                    MaxAmount = reader.ReadInt();

                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Buyable);
            Vendors.Serialize(writer);
            writer.Write(Name);
            writer.Write(Price);
            writer.Write(Amount);
            writer.Write(MaxAmount);
        }

        public override string ToString()
        {
            return String.Format("Achetable: {0}",
                Buyable ? String.Format("Oui, {0}po", Price) : "Non"
                );
        }
        #endregion

    }
}
