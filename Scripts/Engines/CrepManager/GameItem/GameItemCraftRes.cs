﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Ressource")]
    public class GameItemCraftRes
    {
        private Type m_Type = null;
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Type de Ressource")]
        public Type Type
        {
            get { return m_Type; }
            set
            {
                m_Type = value;
                if (value != null && ManagerConfig.Items.ContainsKey(value))
                {
                    if (Amount <= 0)
                        Amount = 1;
                }
            }
        }

        public GameItemType ItemType
        {
            get
            {
                if (m_Type != null && ManagerConfig.Items.ContainsKey(m_Type))
                    return ManagerConfig.Items[m_Type];
                return null;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Quantité")]
        public int Amount { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Fournit la Couleur")]
        public bool RetainsColorFrom { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Message")]
        public string Message 
        {
            get
            {
                if (ItemType != null)
                    return "Vous n'avez pas assez de " + ItemType.Name;
                else return "";
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom")]
        public string Name
        {
            get
            {
                if (ItemType != null)
                    return ItemType.Name;
                else return "";
            }
        }

        #region Constructors & Serialization
        public GameItemCraftRes() 
        {

        }
        public GameItemCraftRes(GenericReader reader)
        {
            
            var version = reader.ReadInt();

            switch(version)
            {
                case 0:
                {
                    Type = reader.ReadType();
                    Amount = reader.ReadInt();
                    RetainsColorFrom = reader.ReadBool();

                    break;
                }
            }
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Type);
            writer.Write(Amount);
            writer.Write(RetainsColorFrom);
        }

        public override string ToString()
        {
            if (Amount == 0)
                return "";
            return String.Format("{0}x {1}",
                Amount, Name
                );
        }
        #endregion

    }
}
