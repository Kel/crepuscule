﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Loot"), NoSort]
    public class GameLootItem
    {
        #region Properties
        private Type m_ItemType;
        private double m_DropChance;

        public GameItemType ItemType
        {
            get 
            {
                var git = GameItemType.FindType(m_ItemType);
                return (git == null) ? null : git;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Utilisable?")]
        public bool Ready
        {
            get { return m_ItemType != null && Amount.Min > 0 && m_DropChance > 0; }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Objet")]
        public Type Type
        {
            get { return m_ItemType; }
            set 
            { 
                m_ItemType = value;
                if (ItemType == null)
                    m_ItemType = null;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Quantité")]
        public RangeValue Amount { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Chance")]
        public double DropChance
        {
            get { return m_DropChance; }
            set 
            { 
                m_DropChance = value;
                if (m_DropChance > 1) m_DropChance = 1;
                if (m_DropChance < 0) m_DropChance = 0;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Niveau Min.")]
        public LootPackLevel MinLevel { get; set; }


        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Enchantement?"), StartGroup("Loot - Enchantement")]
        public bool RandomEnhance { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Intensité")]
        public RangeValue Intensity { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Propriétés Max.")]
        public int MaxProps { get; set; }


        #endregion

        #region Constructors & Serialization
        public GameLootItem()
        {
            Amount = new RangeValue(1, 1);
            Intensity = new RangeValue(0, 100);
            MaxProps = 5;
            DropChance = Math.Round((Utility.RandomDouble() / 3) + 0.01 , 2);
            RandomEnhance = false;
            MinLevel = LootPackLevel.VeryPoor;
        }

        public GameLootItem(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        m_ItemType = reader.ReadType();
                        Amount = reader.ReadRange();
                        m_DropChance = reader.ReadDouble();
                        RandomEnhance = reader.ReadBool();
                        Intensity = reader.ReadRange();
                        MaxProps = reader.ReadInt();
                        MinLevel = (LootPackLevel)reader.ReadInt();
                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version

            writer.Write(m_ItemType);
            writer.Write(Amount);
            writer.Write(m_DropChance);
            writer.Write(RandomEnhance);
            writer.Write(Intensity);
            writer.Write(MaxProps);
            writer.Write((int)MinLevel);
        }

        public override string ToString()
        {
            if (!Ready || ItemType == null) return "-indisponible-";
            return String.Format("{0}, {1}%", ItemType.Name, DropChance * 100);
        }
        #endregion


        #region Generate & Mutate
        public Item Construct(LootPackLevel level)
        {
            if (!Ready) return null;
            if (Utility.RandomDouble() < DropChance)
            {
                var item = GameItemType.CreateItem(m_ItemType);
                if (item != null && level >= LootPackLevel.Normal)
                    item = Mutate(item);
                return item;
            }
            else
            {
                return null;
            }
        }

        public Item Mutate(Item item)
        {
            if (item != null)
            {
                if (item is BaseWeapon || item is BaseArmor || item is BaseJewel)
                {
                    
                    int bonusProps = GetBonusProperties();
                    int min = Intensity.Min;
                    int max = Intensity.Max;

                    // Modify here for the bonus props constant (additional one)
                    if (bonusProps < MaxProps && Utility.RandomDouble() > 0.95)
                        ++bonusProps;

                    int props = 1 + bonusProps;

                    if (item is BaseWeapon)
                        BaseRunicTool.ApplyAttributesTo((BaseWeapon)item, false, 0, props, min, max);
                    else if (item is BaseArmor)
                        BaseRunicTool.ApplyAttributesTo((BaseArmor)item, false, 0, props, min, max);
                    else if (item is BaseJewel)
                        BaseRunicTool.ApplyAttributesTo((BaseJewel)item, false, 0, props, min, max);
                }
                else if (item is BaseInstrument)
                {
                    SlayerName slayer = SlayerName.None;
                    slayer = BaseRunicTool.GetRandomSlayer();

                    if (slayer == SlayerName.None)
                    {
                        item.Delete();
                        return null;
                    }

                    BaseInstrument instr = (BaseInstrument)item;

                    instr.Quality = InstrumentQuality.Regular;
                    instr.Slayer = slayer;
                }

                if (item.Stackable)
                    item.Amount = Amount.Random();
            }

            return item;
        }

        public int GetBonusProperties()
        {
            int p0 = 0, p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0;

            switch (MaxProps)
            {
                case 1: p0 = 3; p1 = 1; break;
                case 2: p0 = 6; p1 = 3; p2 = 1; break;
                case 3: p0 = 10; p1 = 6; p2 = 3; p3 = 1; break;
                case 4: p0 = 16; p1 = 12; p2 = 6; p3 = 5; p4 = 1; break;
                case 5: p0 = 30; p1 = 25; p2 = 20; p3 = 15; p4 = 9; p5 = 1; break;
            }

            int pc = p0 + p1 + p2 + p3 + p4 + p5;

            int rnd = Utility.Random(pc);

            if (rnd < p5)
                return 5;
            else
                rnd -= p5;

            if (rnd < p4)
                return 4;
            else
                rnd -= p4;

            if (rnd < p3)
                return 3;
            else
                rnd -= p3;

            if (rnd < p2)
                return 2;
            else
                rnd -= p2;

            if (rnd < p1)
                return 1;

            return 0;
        }
        #endregion
    }
}
