﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Mobiles;
using Server.Items;

namespace Server.Engines
{
    [PropertyObject]
    [Description("Loot packs")]
    public class LootPacksList
    {

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Listes")]
        public List<GameLootPack> Lists
        {
            get { return ManagerConfig.Loots; }
            set { ManagerConfig.Loots = value; }
        }


        #region Constructors & Serialization
        public LootPacksList()
        {
        }

        public override string ToString()
        {
            return "(Loot packs)";
        }
        #endregion

    }

    public enum LootPackLevel
    {
        VeryPoor,
        Poor,
        Normal,
        Rich,
        VeryRich
    }

    [PropertyObject]
    [Description("Loot Pack"), NoSort]
    public class GameLootPack : ISelfDeletion
    {
        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Nom")]
        public string Name { get; set; }
        public string InternalName { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Objets")]
        public List<GameLootItem> Items {get;set;}

        [CommandProperty(AccessLevel.GameMaster)]
        [Description("Objets Max.")]
        public int MaxItems { get; set; }

        #region Constructors & Serialization
        public GameLootPack()
        {
            Items = new List<GameLootItem>();
            InternalName = Guid.NewGuid().ToString();
            Name = "Sans Titre";
            MaxItems = 2;
        }

        public GameLootPack(GenericReader reader)
        {
            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        Name = reader.ReadString();
                        InternalName = reader.ReadString();
                        MaxItems = reader.ReadInt();

                        Items = new List<GameLootItem>();
                        var itemsInPack = reader.ReadInt();
                        for (int i = 0; i < itemsInPack; i++)
                            Items.Add(new GameLootItem(reader));
                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version

            writer.Write(Name);
            writer.Write(InternalName);
            writer.Write(MaxItems);

            writer.Write(Items.Count);
            foreach (var item in Items)
                item.Serialize(writer);
        }

        public override string ToString()
        {
            return String.Format("Pack: {0}", Name);
        }
        #endregion

        #region Generate
        public int Generate(Mobile from, Container cont, LootPackLevel level)
        {
            if (cont == null)
                return 0;

            int totalMax = Math.Min( 1 + (((int)level) * 2), MaxItems);
            int generated = 0;
            var items = Items.Where(i => i.MinLevel <= level).ToList();
            int differentItems = items.Count;

            for(int i=0; i< differentItems;i++)
            {
                // Randomly access an entry
                var entry = items[Utility.Random(0, differentItems)];
                var item = entry.Construct(level);
                if (item == null)
                    continue;

                cont.DropItem(item);
                generated++;

                if (generated >= totalMax)
                    break;
            }



            return generated;
        }

        public Item GenerateOne()
        {
            int differentItems = Items.Count;

            for (int i = 0; i < differentItems; i++)
            {
                // Randomly access an entry
                var entry = Items[Utility.Random(0, differentItems)];

                var item = entry.Construct(LootPackLevel.Normal);
                if (item == null)
                    continue;

                return item;
            }

            return null;
        }
        #endregion

        #region ISelfDeletion Members

        public bool CanBeDeleted()
        {
            return Items.Count == 0;
        }

        #endregion
    }
}
