﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Items;

namespace Server.Engines
{
    public static class Loot
    {

        #region Initializations
        public static GameLootPack Everyone = null;
        public static GameLootPack Weapons = null;
        public static GameLootPack Armors = null;
        public static GameLootPack Shields = null;
        public static GameLootPack Gems = null;
        public static GameLootPack Jewelry = null;
        public static GameLootPack Regs = null;
        public static GameLootPack Potions = null;
        public static GameLootPack Instruments = null;
        public static GameLootPack Statues = null;
        public static GameLootPack Scrolls = null;
        public static GameLootPack Rares = null;


        public static void Initialize()
        {
             Everyone  = ManagerConfig.Loots.Where(l => l.InternalName == "Everyone").FirstOrDefault();
             Weapons = ManagerConfig.Loots.Where(l => l.InternalName == "Weapons").FirstOrDefault();
             Armors = ManagerConfig.Loots.Where(l => l.InternalName == "Armors").FirstOrDefault();
             Shields = ManagerConfig.Loots.Where(l => l.InternalName == "Shields").FirstOrDefault();
             Gems = ManagerConfig.Loots.Where(l => l.InternalName == "Gems").FirstOrDefault();
             Jewelry = ManagerConfig.Loots.Where(l => l.InternalName == "Jewelry").FirstOrDefault();
             Regs = ManagerConfig.Loots.Where(l => l.InternalName == "Regs").FirstOrDefault();
             Potions = ManagerConfig.Loots.Where(l => l.InternalName == "Potions").FirstOrDefault();
             Instruments = ManagerConfig.Loots.Where(l => l.InternalName == "Instruments").FirstOrDefault();
             Statues = ManagerConfig.Loots.Where(l => l.InternalName == "Statues").FirstOrDefault();
             Scrolls = ManagerConfig.Loots.Where(l => l.InternalName == "Scrolls").FirstOrDefault();
             Rares = ManagerConfig.Loots.Where(l => l.InternalName == "Rares").FirstOrDefault();
        }
        #endregion

        #region Helpers

        internal static Item RandomPossibleReagent()
        {
            return Regs != null ? Regs.GenerateOne() : null;
        }

        internal static Item RandomGem()
        {
            return Gems != null ? Gems.GenerateOne() : null;
        }

        internal static Item RandomScroll()
        {
            return Scrolls != null ? Scrolls.GenerateOne() : null;
        }

        internal static Item RandomArmorOrShieldOrWeaponOrJewelry()
        {
            int s = Utility.Random(0, 3);
            if (s == 0) return Weapons != null ? Weapons.GenerateOne() : null;
            if (s == 1) return Jewelry != null ? Jewelry.GenerateOne() : null;
            if (s == 2) return Armors != null ? Armors.GenerateOne() : null;
            if (s == 3) return Shields != null ? Shields.GenerateOne() : null;
            return null;
        }

        internal static Item RandomPotion()
        {
            return Potions != null ? Potions.GenerateOne() : null;
        }

        internal static Item RandomStatue()
        {
            return Statues != null ? Statues.GenerateOne() : null;
        }

        internal static Item RandomWeaponOrJewelry()
        {
            int s = Utility.Random(0, 1);
            if (s == 0) return Weapons != null ? Weapons.GenerateOne() : null;
            if (s == 1) return Jewelry != null ? Jewelry.GenerateOne() : null;
            return null;
        }

        internal static BaseInstrument RandomInstrument()
        {
            return Instruments != null ? Instruments.GenerateOne() as BaseInstrument : null;
        }

        internal static Item RandomArmorOrShieldOrJewelry()
        {
            int s = Utility.Random(0, 2);
            if (s == 0) return Jewelry != null ? Jewelry.GenerateOne() : null;
            if (s == 1) return Armors != null ? Armors.GenerateOne() : null;
            if (s == 2) return Shields != null ? Shields.GenerateOne() : null;
            return null;
        }

        internal static BaseArmor RandomArmorOrShield()
        {
            int s = Utility.Random(0, 1);
            if (s == 0) return Armors != null ? Armors.GenerateOne() as BaseArmor : null;
            if (s == 1) return Shields != null ? Shields.GenerateOne() as BaseArmor : null;
            return null;
        }

        #endregion

    }
}
