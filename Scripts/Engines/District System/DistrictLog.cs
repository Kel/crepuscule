using Crepuscule.Update;
using Crepuscule.Update.Engine;
using System;
using Server;
using Server.Items;
using Server.Mobiles;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Server.Engines
{
    public class DistrictLog
    {
        #region Private Variables

        private static ArrayList m_LastChange = new ArrayList();
        private static InternalTimer m_Timer = null;
        private static object m_LockRegister = new object();

        #endregion

        #region Public Methods

        public static void Update()
        {
            lock (m_LockRegister)
            {
                try
                {
                    if (!Directory.Exists(@"web\district"))
                    {
                        Directory.CreateDirectory(@"web\district");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                #region History

                if (m_LastChange.Count > 0)
                {
                    StringBuilder tmp = GetHistory();

                    #region Reading

                    try
                    {
                        StreamReader sr = new StreamReader("web/district/history-"+DateTime.Today.ToString("ddMMyyyy")+".txt");

                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            tmp.AppendLine(line);
                        }

                        sr.Close();
                    }
                    catch (Exception)
                    {
                    }

                    #endregion

                    #region Writing

                    try
                    {
                        StreamWriter sw = new StreamWriter(new FileStream("web/district/history-"+DateTime.Today.ToString("ddMMyyyy")+".txt", FileMode.Create), System.Text.Encoding.UTF8);

                        sw.WriteLine("{0}", tmp.ToString());

                        sw.Close();
                    }
                    catch (Exception e)
                    {
                    }

                    #endregion

                    m_LastChange.Clear();
                }

                #endregion

                #region Register

                try
                {
                    StreamWriter sw = new StreamWriter(new FileStream("web/district/map.js", FileMode.Create), System.Text.Encoding.UTF8);

                    sw.WriteLine(GetRegister());

                    sw.Close();
                }
                catch (Exception)
                {
                }

                #endregion

            }

            (new Thread(DistrictLog.Send)).Start();
        }

        #region Logs

        public static void RemoveHouse(string id)
        {
            m_LastChange.Add(string.Format("Le b�timent {0} ne figure plus au registre.",id));
            StartTimer();
        }
    
        public static void CreateHouse(string id)
        {
            m_LastChange.Add(string.Format("Le b�timent {0} a �t� ajout� au registre.",id));
            StartTimer();
        }
    
        public static void MoveHouse(string oldid, string newid)
        {
            m_LastChange.Add(string.Format("Le b�timent {0} est � pr�sent le b�timent {1}.",oldid, newid));
            StartTimer();
        }
    
        public static void ChangeCommerce(string id, bool commerce)
        {
            if (commerce)
                m_LastChange.Add(string.Format("Le b�timent {0} est � pr�sent un commerce.", id));
            else
                m_LastChange.Add(string.Format("Le b�timent {0} n'est plus un commerce � pr�sent.", id));
            StartTimer();
        }
    
        public static void ChangeOwner(string id, Mobile oldowner, Mobile newowner)
        {
            if (newowner == null)
                m_LastChange.Add(string.Format("Le b�timent {0} a �t� remis en vente libre.", id));
            else if (oldowner == null)
                m_LastChange.Add(string.Format("Le b�timent {0} a �t� achet�.", id));
            else
                m_LastChange.Add(string.Format("Le b�timent {0} a �t� tranf�r� d'un propri�taire � un autre.", id));
            StartTimer();
        }
    
        public static void ChangePrice(string id, int oldprice, int newprice)
        {
            m_LastChange.Add(string.Format("Le b�timent {0} a vu son prix r��valu� de {1} � {2}.", id, oldprice, newprice));
            StartTimer();
        }
    
        public static void ChangeOwnerName(string id, string oldname, string newname)
        {
            m_LastChange.Add(string.Format("Le b�timent {0} a chang� le nom de propri�taire \"{1}\" en \"{2}\"",id, oldname, newname));
            StartTimer();
        }

        #endregion

        #endregion

        #region Private Members

        private static void Send()
        {
            FTP ftp = null;

            lock (m_LockRegister)
            {

                try
                {
                    ftp = new FTP();
                    ftp.Connect(Packager.FtpHost, Packager.FtpPort, Packager.FtpUsername, Packager.FtpPassword);

                    string ToSend = "map/map.js";
                    string FilePath = "web/district/map.js";

                    ftp.Files.Upload(ToSend, FilePath);
                    while (!ftp.Files.UploadComplete)
                    {
                        Thread.Sleep(100);
                    }

                    Console.WriteLine("Map data updated");

                    ToSend = "map/history/history-" + DateTime.Today.ToString("ddMMyyyy") + ".txt";
                    FilePath = "web/district/history-" + DateTime.Today.ToString("ddMMyyyy") + ".txt";

                    ftp.Files.Upload(ToSend, FilePath);
                    while (!ftp.Files.UploadComplete)
                    {
                        Thread.Sleep(100);
                    }

                    Console.WriteLine("Map history updated");
                }
                catch (Exception)
                {
                }

                try
                {
                    ftp.Disconnect();
                }
                catch
                {
                }
            }
        }

        private static void StartTimer()
        {
            if (m_Timer == null)
            {
                m_Timer = new InternalTimer();
                m_Timer.Start();
            }
        }

        private static string GetRegister()
        {
            StringBuilder tmp = new StringBuilder();
            tmp.AppendLine("var City = new Array();");
            District district;
            List<BoiteAuLettreComponent> list;
            BoiteAuLettreComponent house;
            int[] bounds;
            for (int i = 0; i < District.Count(); ++i)
            {
                district = District.GetDistrict(i);
                tmp.AppendFormat("\nvar District{0} = new Array();\n", district.GetId());
                tmp.AppendFormat("District{0}['abrv'] = '{0}';\n", district.GetId());
                tmp.AppendFormat("District{0}['name'] = \"{1}\";\n", district.GetId(), district.GetName());
                bounds = district.GetBounds();
                tmp.AppendFormat("\nvar Bounds{0} = [{1},{2},{3},{4}];\n", district.GetId(), bounds[0], bounds[1], bounds[2], bounds[3]);
                tmp.AppendFormat("\nDistrict{0}['bounds'] = Bounds{0};\n", district.GetId());
                tmp.AppendFormat("\nvar Houses{0} = new Array();\n", district.GetId());
                list = district.GetHouses();
                if (list != null)
                {
                    while (list.Count > 0)
                    {
                        house = null;
                        for (int j = 0; j < list.Count; ++j)
                        {
                            if (house == null || house.GetNumber() > list[j].GetNumber())
                            {
                                house = list[j];
                            }
                        }
                        list.Remove(house);
                        if (house != null)
                        {
                            string number = house.GetNumber().ToString();
                            tmp.AppendFormat("\nvar House{0}{1} = new Array();\n", district.GetId(), number);
                            tmp.AppendFormat("House{0}{1}['x'] = {2};\n", district.GetId(), number, house.X.ToString());
                            tmp.AppendFormat("House{0}{1}['y'] = {2};\n", district.GetId(), number,house.Y.ToString());
                            tmp.AppendFormat("House{0}{1}['number'] = {1};\n", district.GetId(), number);
                            tmp.AppendFormat("House{0}{1}['proprio'] = \"{2}\";\n", district.GetId(), number, house.NomProprio);
							tmp.AppendFormat("House{0}{1}['hidden'] = \"{2}\";\n", district.GetId(), number, house.MontrerProprio.ToString().ToLower());
                            tmp.AppendFormat("House{0}{1}['owned'] = {2};\n", district.GetId(), number, (house.Proprio != null).ToString().ToLower());
                            tmp.AppendFormat("House{0}{1}['price'] = {2};\n", district.GetId(), number, house.PrixVente);
                            tmp.AppendFormat("House{0}{1}['commerce'] = {2};\n", district.GetId(), number, house.IsCommerce.ToString().ToLower());
                            tmp.AppendFormat("\nHouses{0}['{1}'] = House{0}{1};\n", district.GetId(), number);
                        }
                    }
                }
                tmp.AppendFormat("\nDistrict{0}['houses'] = Houses{0};\n", district.GetId());
                tmp.AppendFormat("\nCity['{0}'] = District{0};\n", district.GetId());
            }

            /*string ret = "var City = new Array();";
            District district;
            List<BoiteAuLettreComponent> list;
            BoiteAuLettreComponent house;
            int[] bounds;
            for (int i = 0; i < District.Count(); ++i)
            {
                district = District.GetDistrict(i);
                ret = string.Format("{0}\n\nvar District{1} = new Array();", ret, district.GetId());
                ret = string.Format("{0}\nDistrict{1}['abrv'] = '{1}';", ret, district.GetId());
                ret = string.Format("{0}\nDistrict{1}['name'] = \"{2}\";", ret, district.GetId(), district.GetName());
                bounds = district.GetBounds();
                ret = string.Format("{0}\n\nvar Bounds{1} = [{2},{3},{4},{5}];", ret, district.GetId(), bounds[0], bounds[1], bounds[2], bounds[3]);
                ret = string.Format("{0}\n\nDistrict{1}['bounds'] = Bounds{1};", ret, district.GetId());
                ret = string.Format("{0}\n\nvar Houses{1} = new Array();", ret, district.GetId());
                list = district.GetHouses();
                if (list != null)
                {
                    while (list.Count > 0)
                    {
                        house = null;
                        foreach (BoiteAuLettreComponent h in list)
                        {
                            if (house == null)
                            {
                                house = h;
                            }
                            else if (house.GetNumber() > h.GetNumber())
                            {
                                house = h;
                            }
                        }
                        list.Remove(house);
                        if (house != null)
                        {
                            string number = house.GetNumber() != null ? house.GetNumber().ToString() : "Error with house number";
                            ret = string.Format("{0}\n\nvar House{1}{2} = new Array();", ret, district.GetId(), number);
                            ret = string.Format("{0}\nHouse{1}{2}['x'] = {3};", ret, district.GetId(), number, house.X != null ? house.X.ToString() : "Error with X");
                            ret = string.Format("{0}\nHouse{1}{2}['y'] = {3};", ret, district.GetId(), number, house.Y != null ? house.Y.ToString() : "Error with Y");
                            ret = string.Format("{0}\nHouse{1}{2}['number'] = {2};", ret, district.GetId(), number);
                            ret = string.Format("{0}\nHouse{1}{2}['proprio'] = \"{3}\";", ret, district.GetId(), number, house.NomProprio);
                            ret = string.Format("{0}\nHouse{1}{2}['owned'] = {3};", ret, district.GetId(), number, (house.Proprio != null).ToString().ToLower());
                            ret = string.Format("{0}\nHouse{1}{2}['price'] = {3};", ret, district.GetId(), number, house.PrixVente);
                            ret = string.Format("{0}\nHouse{1}{2}['commerce'] = {3};", ret, district.GetId(), number, house.IsCommerce != null ? house.IsCommerce.ToString().ToLower() : false.ToString());
                            ret = string.Format("{0}\n\nHouses{1}['{2}'] = House{1}{2};", ret, district.GetId(), number);
                        }
                    }
                }
                ret = string.Format("{0}\n\nDistrict{1}['houses'] = Houses{1};", ret, district.GetId());
                ret = string.Format("{0}\n\nCity['{1}'] = District{1};", ret, district.GetId());
            }

            return ret;*/
            return tmp.ToString();
        }

        private static StringBuilder GetHistory()
        {
            StringBuilder tmp = new StringBuilder();
            for (int i = 0; i < m_LastChange.Count; ++i)
                tmp.AppendLine((string)m_LastChange[i]);
            
            return tmp;
        }

        private class InternalTimer : Timer
        {
            public InternalTimer() : base(TimeSpan.FromMinutes(1.0))
            {
                Priority = TimerPriority.FiveSeconds;
            }

            protected override void OnTick()
            {
                (new Thread(DistrictLog.Update)).Start();
                this.Stop();
                m_Timer = null;
            }
        }

        #endregion

    }
}