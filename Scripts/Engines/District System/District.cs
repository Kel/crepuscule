using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Server.Items;

namespace Server.Engines
{
    public class District
    {

        #region Members

        private static Rectangle2D[] m_A = new Rectangle2D[] 
        { 
            new Rectangle2D(2355, 1911, 70, 133) 
        };
      
        private static Rectangle2D[] m_B = new Rectangle2D[]
        {
            new Rectangle2D(2424, 1924, 88, 120),
            new Rectangle2D(2511, 1960, 28, 84),
            new Rectangle2D(2538, 2007, 12, 37) 
        };
          
        private static Rectangle2D[] m_C = new Rectangle2D[]
        {
            new Rectangle2D(2501,1828,110,97),
            new Rectangle2D(2511,1924,119,37),
            new Rectangle2D(2610,1886,12,39),
            new Rectangle2D(2538,1960,257,25),
            new Rectangle2D(2538,1984,12,24),
            new Rectangle2D(2690,1986,105,15),
            new Rectangle2D(2549,1984,26,6),
            new Rectangle2D(2574,1984,57,16),
            new Rectangle2D(2754,1994,60,33),
            new Rectangle2D(2754,2026,34,20),
            new Rectangle2D(2767,1916,28,45),
            new Rectangle2D(2749,1937,19,24),
            new Rectangle2D(2629,1943,121,18)
        };
      
        private static Rectangle2D[] m_D = new Rectangle2D[]
        {
            new Rectangle2D(2549,1989,26,11),
            new Rectangle2D(2690,1998,65,2),
            new Rectangle2D(2630,1984,61,16),
            new Rectangle2D(2549,1999,27,45),
            new Rectangle2D(2575,1999,42,180),
            new Rectangle2D(2616,1999,23,154),
            new Rectangle2D(2638,1999,25,127),
            new Rectangle2D(2662,1999,25,94),
            new Rectangle2D(2686,1999,32,76),
            new Rectangle2D(2717,1999,38,60)
        };
          
        private static Rectangle2D[] m_E = new Rectangle2D[]
        {
            new Rectangle2D(2610,1828,12,59),
            new Rectangle2D(2621,1828,9,97),
            new Rectangle2D(2629,1828,121,116),
            new Rectangle2D(2749,1828,19,110),
            new Rectangle2D(2767,1828,28,89),
            new Rectangle2D(2794,1828,14,67)
        };
          
        private static Rectangle2D[] m_F = new Rectangle2D[]
        {
            new Rectangle2D(2807,1828,66,105),
            new Rectangle2D(2794,1894,32,101),
            new Rectangle2D(2825,1935,22,22),
            new Rectangle2D(2872,1828,17,87),
            new Rectangle2D(2888,1828,17,71)
        };
          
        private static Rectangle2D[] m_G = new Rectangle2D[]
        {
            new Rectangle2D(2904,1828,113,105),
            new Rectangle2D(2888,1898,17,35),
            new Rectangle2D(2872,1914,17,19)
        };
          
        private static Rectangle2D[] m_H = new Rectangle2D[]
        {
            new Rectangle2D(2846,1932,171,94),
            new Rectangle2D(2825,1953,109,74),
            new Rectangle2D(2813,1994,13,33)
        };
      
        private static Rectangle2D[] m_I = new Rectangle2D[]
        {
            new Rectangle2D(2787,2026,148,95),
            new Rectangle2D(2754,2045,34,50),
            new Rectangle2D(2718,2058,38,37),
            new Rectangle2D(2686,2074,32,21),
            new Rectangle2D(2662,2092,38,34),
            new Rectangle2D(2771,2094,17,27)
        };
          
        private static Rectangle2D[] m_J = new Rectangle2D[]
        {
            new Rectangle2D(2620,2206,232,58),
            new Rectangle2D(2699,2094,73,113),
            new Rectangle2D(2771,2195,53,12),
            new Rectangle2D(2638,2125,62,82),
            new Rectangle2D(2616,2152,23,55),
            new Rectangle2D(2575,2178,42,29)
        };
          
        private static Rectangle2D[] m_K = new Rectangle2D[]
        {
            new Rectangle2D(2771,2120,53,76),
            new Rectangle2D(2823,2120,29,87),
            new Rectangle2D(2851,2120,84,144)
        };
          
        private static Rectangle2D[] m_L = new Rectangle2D[]
        {
            new Rectangle2D(2382,2069,85,89)
        };
            
        private static ArrayList m_Districts = new ArrayList();
        private static List<BoiteAuLettreComponent> m_Houses = new List<BoiteAuLettreComponent>();
        private static object m_Lock = new object();
        private static bool m_Initialized = false;
        private string m_Name, m_Id;
        private Rectangle2D[] m_Coords;
        private int[] m_Bounds;

        #endregion

        #region Methods

        public bool Contains(Point3D location)
        {
            for (int i = 0; i < m_Coords.Length; i++)
            {
                if (m_Coords[i].Contains(location))
                    return true;
            }
            return false;
        }

        public List<BoiteAuLettreComponent> GetHouses()
        {
            List<BoiteAuLettreComponent> ret = new List<BoiteAuLettreComponent>();

            lock (m_Lock)
            {
                while (m_Houses.Contains(null))
                {
                    m_Houses.Remove(null);
                }

                for (int i = 0; i < m_Houses.Count; ++i)
                {
                    if (m_Houses[i] != null && m_Houses[i].GetDistrictId() == this.m_Id)
                    {
                        ret.Add(m_Houses[i]);
                    }
                }
            }

            return ret;
        }

        public static void AddHouse(BoiteAuLettreComponent house)
        {
            lock (m_Lock)
            {
                if (house != null && !m_Houses.Contains(house))
                {
                    m_Houses.Add(house);
                }
            }
        }

        public static void RemoveHouse(BoiteAuLettreComponent house)
        {
            lock (m_Lock)
            {
                if (house != null && m_Houses.Contains(house))
                {
                    m_Houses.Remove(house);
                }
            }
        }

        public string GetName()
        {
            return m_Name;
        }

        public string GetId()
        {
            return m_Id;
        }

        public int[] GetBounds()
        {
            return m_Bounds;
        }

        public static void SetDistricts()
        {
            m_Initialized = true;
            m_Districts.Add(new District("Quartier de la Cath�drale",         "A", m_A)); 
            m_Districts.Add(new District("Quartier Commer�ant",               "B", m_B)); 
            m_Districts.Add(new District("Moyenne-Ville (Centre)",            "C", m_C)); 
            m_Districts.Add(new District("Quartier Pourpre",                  "D", m_D)); 
            m_Districts.Add(new District("Haute-Ville",                       "E", m_E)); 
            m_Districts.Add(new District("Moyenne-Ville (Est)",               "F", m_F)); 
            m_Districts.Add(new District("Quartier de l'Ordre",               "G", m_G)); 
            m_Districts.Add(new District("Basse-Ville (Nord)",                "H", m_H)); 
            m_Districts.Add(new District("Basse-Ville (Sud)",                 "I", m_I)); 
            m_Districts.Add(new District("Quartier de l'Assembl�e (Ouest)", "J", m_J)); 
            m_Districts.Add(new District("Quartier de l'Assembl�e (Est)",   "K", m_K)); 
            m_Districts.Add(new District("Quartier du Port",                  "L", m_L)); 
        }
      
        public static District GetDistrict(int i)
        {
            if (i >= m_Districts.Count)
              return null;
            return (District) m_Districts[i];
        }
      
        public static int GetIndex(string id)
        {
            for (int i = 0; i < m_Districts.Count; ++i)
              if (((District)m_Districts[i]).GetId() == id)
                return i;
            return -1;
        }
      
        public static int Count()
        {
            return m_Districts.Count;
        }
      
        public static void Initialize()
        {
            if (!m_Initialized)
              SetDistricts();
            (new Thread(DistrictLog.Update)).Start();
        }
      
        public static District Find(Point3D location)
        {
            if (!m_Initialized)
              SetDistricts();
            for (int i = 0; i < m_Districts.Count; ++i)
              if (((District)m_Districts[i]).Contains(location))
                return (District)m_Districts[i];
            return null;
        }

        #endregion

        #region Constructor

        public District(string name, string id, Rectangle2D[] coords)
        {
            m_Name = name;
            m_Id = id;
            m_Coords = coords;
            
            int x1 = 10000, y1 = 10000, x2 = 0, y2 = 0; 
            for (int i = 0; i < m_Coords.Length; i++)
            {
              if (m_Coords[i].Start.X <= x1)
                x1 = m_Coords[i].Start.X;
              if (m_Coords[i].End.X >= x2)
                x2 = m_Coords[i].End.X;
              if (m_Coords[i].Start.Y <= y1)
                y1 = m_Coords[i].Start.Y;
              if (m_Coords[i].End.Y >= y2)
                y2 = m_Coords[i].End.Y;
            }
            m_Bounds = new int[] { x1, y1, x2, y2 };
        }

        #endregion

    }
}