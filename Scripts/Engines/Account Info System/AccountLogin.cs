using System; 
using Server; 
using System.IO; 
using Server.Gumps; 
using Server.Network;
using Server.Items;
using Server.Mobiles;
using Server.Accounting; 

namespace Server.Gumps
{ 
   	public class AccountLogin : Gump 
   	{ 
      		public static void Initialize() 
      		{ 
         		Commands.Register( "Password", AccessLevel.Player, new CommandEventHandler( AccountLogin_OnCommand ) ); 
      		} 

      		private static void AccountLogin_OnCommand( CommandEventArgs e ) 
      		{ 
         		e.Mobile.SendGump( new AccountLogin( e.Mobile ) ); 
      		}

      		private Mobile m_From; 

      		public AccountLogin( Mobile owner ) : base( 25,25 ) 
      		{ 
			Closable=true;
			Disposable=true;
			Dragable=true;
			Resizable=false;
			AddPage(0);
            AddBackground(13, 12, 333, 193, 9200);
			AddImageTiled(28, 81, 212, 25, 9304);
			AddImageTiled(28, 135, 212, 25, 9304);
			AddLabel(30, 59, 1160, @"Indentifiant:");
			AddLabel(30, 114, 1160, @"Mot de passe:");
			AddImage(255, 53, 5504);
			AddButton(250, 136, 4023, 4024, 1, GumpButtonType.Reply, 0);
			AddLabel(286, 138, 1149, @"Confirmer");
			AddTextEntry(28, 81, 212, 25, 0, 1, @"");
			AddTextEntry(28, 135, 212, 25, 0, 2, @"");
			AddLabel(28, 166, 1149, @"Saisie de l'indentifiant et");
            AddLabel(28, 178, 1149, @"du  mot de passe pour continuer");
			AddLabel(25, 16, 1160, @"Ecran de votre compte");

		}
      		public override void OnResponse( NetState state, RelayInfo info ) 
      		{ 

        		if ( info.ButtonID == 1 ) // Add Email
         		{ 
                        	Mobile from = state.Mobile;
                        	Account acct = (Account)from.Account; 
            			string user = (string)info.GetTextEntry( 1 ).Text;
            			string pass = (string)info.GetTextEntry( 2 ).Text;

				if ( user == acct.Username && acct.CheckPassword( pass ) )
				{
					from.SendMessage( 64, "Login confirm�." );
					from.SendGump( new AccountInfo( from ) );
				}
				else
				{
					from.SendMessage( 38, "Either the username or password you entered was incorrect, Please recheck your spelling and remember that passwords and usernames are case sensitive. Please try again." );
				}
			}
        	} 
	}
} 