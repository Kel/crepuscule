using System;
using System.Collections;
using Server;
using Server.Gumps;
using Server.Network;
using Server.Misc;
using Server.Mobiles;
using Server.Targeting;
using Server.Items;
using Server.Engines.Craft;
using Server.Engines;

namespace Server.Items
{
  public class GraveDiggersShovel : CrepusculeItem
  {
    private int m_Uses;
    private static ArrayList m_GraveList;

    public static void Initialize()
    {
      m_GraveList = new ArrayList();
    }
    
    private class GraveRessource
    {
      private int m_Left;
      private Point3D m_Location;
      
      public GraveRessource(Point3D location)
      {
        m_Left = Utility.Random(5,15);
        m_Location = location;
        m_GraveList.Add(this);
        (new GraveTimer(location,TimeSpan.FromMinutes( Utility.Random(50,70) ))).Start();
      }
      
      public Point3D GetLocation()
      {
        return m_Location;
      }
      
      public bool CanDig()
      {
        return m_Left > 0;
      }
      
      public void Consume()
      {
        m_Left--;
      }
      
      private class GraveTimer : Timer
      {
        private Point3D m_Location;
 
        public GraveTimer(Point3D location, TimeSpan duration) : base(duration)
        {
          Priority = TimerPriority.OneSecond;
          m_Location = location;
        }
        
        protected override void OnTick()
        {
          GraveRessource tomb = null;
          for (int i=0; i < m_GraveList.Count; ++i)
            if ((m_GraveList[i] as GraveRessource).GetLocation() == m_Location)
              tomb = (GraveRessource)m_GraveList[i];
          if (tomb != null)
            m_GraveList.Remove(tomb);
          Stop();
        }
      }
    }

    [CommandProperty(AccessLevel.GameMaster)]
    public int Uses
    {
      get { return m_Uses; }
      set { m_Uses = value; InvalidateProperties(); }
    }

    [Constructable]
    public GraveDiggersShovel() : base(0xF39)
    {
      Weight = 5.0;
      Hue = 1109;
      Name = "Pelle de pilleur de tombes";
      m_Uses = Utility.RandomMinMax(25, 50);
    }
    
    public override void Serialize(GenericWriter writer)
    {
      base.Serialize(writer);
      writer.Write((int)1); // version 

      writer.Write(m_Uses);
    }

    public override void Deserialize(GenericReader reader)
    {
      base.Deserialize(reader);
      int version = reader.ReadInt();

      m_Uses = reader.ReadInt();
      
      if (version < 1)
        reader.ReadBool();
    }

    public override void AddNameProperties(ObjectPropertyList list)
    {
      base.AddNameProperties(list);
      list.Add(1060662, "Utilisations\t{0}", m_Uses);
    }

    public GraveDiggersShovel(Serial serial) : base(serial)
    {
    }

    public override void OnDoubleClick(Mobile from)
    {
      if (!IsChildOf(from.Backpack))
      {
        from.SendMessage("Cela fonctionnerait peut-�tre mieux si c'�tait dans votre sac ?");
        return;
      }
      
      Item handOne = from.FindItemOnLayer( Layer.OneHanded );
      Item handTwo = from.FindItemOnLayer( Layer.TwoHanded );
      
      if (handOne != null || handTwo != null )
      {
        from.SendMessage("Vous devez avoir les deux mains libres pour utiliser une pelle.");
        return;
      }
      
      if (from.Mounted)
      {
        from.SendMessage("Vous tombez de votre monture et vous blessez en faisant l'imb�cile avec une pelle.");
        from.Damage(Utility.Random(20,30),null);
        IMount mount = from.Mount;
        mount.Rider = null;
        from.PlaySound( 0x140 );
        from.FixedParticles( 0x3728, 10, 15, 9955, EffectLayer.Waist );
        return;
      }

      if (from.Hidden)
        from.RevealingAction();
        
      from.Target = new GraveTarget(this);
      from.SendMessage("O� voulez-vous creuser?");  
    }

    private class DigTimer : Timer
    {
      private Mobile m_From;

      public DigTimer(Mobile from, TimeSpan duration) : base(duration)
      {
        Priority = TimerPriority.OneSecond;
        m_From = from;
      }
      
      private double[][] m_Probability = new double[][] {
        // Format : x,y
        // x = probabilit� de trouv� l'objet (en %) quand la skill est � 100%
        // y = niveau minimum pour trouver l'objet
        // Rien
        new double[] {  15.0,  0.0},
        // R�veiller les morts
        new double[] {  0.5,  1.0},
        // Os
        new double[] {  8.0,  5.0},
        // Body part (necro golem ?)
        new double[] {  5.0, 10.0},
        // Pierre d'�me 
        new double[] {  1.4, 15.0},
        // Or
        new double[] { 10.0, 20.0},
        // R�actif
        new double[] { 10.0, 30.0},
        // Gemme
        new double[] { 10.0, 45.0},
        // Parchemin magery
        new double[] { 10.0, 60.0},
        // Parchemin n�cro
        new double[] { 10.0, 70.0},
        // D�co de tombe
        new double[] { 10.0, 80.0},
        // Objet magique
        new double[] { 10.0, 90.0},
        // Gemme d'enchantement
        new double[] {  0.1, 99.0}
      };
      
      private double[] GetChance(double skill)
      {
        double[] ret = new double[m_Probability.Length];
        double d1 = 0;
        if (skill  == 0)
        {
          ret[0] = 0;
          for (int i=1; i < ret.Length; ++i)
            ret[i] = 1;
          return ret;
        }  
        if (skill >= 100)
        {
          for (int i=0; i < ret.Length; ++i)
          {
            ret[i] = d1;
            d1 += m_Probability[i][0]/100;
          }
          return ret;
        }
        
        double c  = -.5/skill;
        double cp = -.5/100;
        
        double[] d = new double[m_Probability.Length+1];
        
        for (int i=0; i < m_Probability.Length; ++i)
          d[i] = m_Probability[i][1];
        d[m_Probability.Length] = 100;
        
        double up;
        
        for (int i=0; i < m_Probability.Length; ++i)
        {
          if (skill > m_Probability[i][1])
          {
            up = (cp*d[i+1]*d[i+1]+d[i+1])-(cp*d[i]*d[i]+d[i]);
            d1 = Math.Min(skill,d[i+1]);
            ret[i] = ((c*d1*d1+d1) - (c*d[i]*d[i]+d[i]))/up*m_Probability[i][0]/100;
          }
          else
            break;
        }
        
        up = 0;
        
        for (int i=0; i < ret.Length; ++i)
          up += ret[i];
          
        d1 = 0;
        
        for (int i=0; i < ret.Length; ++i)
        {
          d1 += ret[i]/up;
          ret[i] = d1-ret[i]/up;
        }
          
        
        return ret;
      }
      
      private int GetAttribute(double skill, int variance)
      {
        if (skill == 0)
          return 0;
          
        double[] bounds = new double[variance+1];
        double step = 120/skill/variance;
        
        for (int i=0; i < variance+1; ++i)
          bounds[i] = Math.Min(i*step,1);
        
        double[] content = new double[variance];
        for (int i=0; i < variance; ++i)
          content[i] = MagicFunction(bounds[i+1])-MagicFunction(bounds[i]);
        
        skill = 0;
        for (int i=0; i < variance; ++i)
          skill += content[i];
        
        step = 0;
        for (int i=0; i < variance; ++i)
        {
          step += content[i]/skill;
          content[i] = step - content[i]/skill;
        }
        
        
        step = Utility.RandomDouble();
        
        int index = variance-1;
        while (content[index] >= step)
          index--;
          
        return index;
      }

// ----------------------------------------------------------------------------
// Code pomp� pour calculer erfc
// Copyright (C) 1984 Stephen L. Moshier (original C version - Cephes Math Library)
// Copyright (C) 1996 Leigh Brookshaw  (Java version)
// Copyright (C) 2005 Miroslav Stampar (C# version [->this<-])
// ----------------------------------------------------------------------------
         
      private const double MAXLOG = 7.09782712893383996732E2;

      private static double erfc(double a)
      {
        double x, y, z, p, q;

        double[] P = {
             2.46196981473530512524E-10,
             5.64189564831068821977E-1,
             7.46321056442269912687E0,
             4.86371970985681366614E1,
             1.96520832956077098242E2,
             5.26445194995477358631E2,
             9.34528527171957607540E2,
             1.02755188689515710272E3,
             5.57535335369399327526E2
           };
        double[] Q = {
             1.32281951154744992508E1,
             8.67072140885989742329E1,
             3.54937778887819891062E2,
             9.75708501743205489753E2,
             1.82390916687909736289E3,
             2.24633760818710981792E3,
             1.65666309194161350182E3,
             5.57535340817727675546E2
           };

        double[] R = {
             5.64189583547755073984E-1,
             1.27536670759978104416E0,
             5.01905042251180477414E0,
             6.16021097993053585195E0,
             7.40974269950448939160E0,
             2.97886665372100240670E0
           };
        double[] S = {
             2.26052863220117276590E0,
             9.39603524938001434673E0,
             1.20489539808096656605E1,
             1.70814450747565897222E1,
             9.60896809063285878198E0,
             3.36907645100081516050E0
           };
        if (a < 0.0) 
          x = -a;
        else 
          x = a;
        if (x < 1.0) 
          return 1.0 - erf(a);
        z = -a*a;
        if (z < -MAXLOG)
        {
          if (a < 0) 
            return (2.0);
          else 
            return (0.0);
        }
        z = Math.Exp(z);
        if (x < 8.0)
        {
          p = polevl(x, P, 8);
          q = p1evl(x, Q, 8);
        }
        else
        {
          p = polevl(x, R, 5);
          q = p1evl(x, S, 6);
        }
        y = (z*p)/q;
        if (a < 0) 
          y = 2.0 - y;
        if (y == 0.0)
        {
          if (a < 0) 
            return 2.0;
          else 
            return (0.0);
        }
        return y;
      } 

      private static double erf(double x)
      {
        double y, z;
        double[] T = {
             9.60497373987051638749E0,
             9.00260197203842689217E1,
             2.23200534594684319226E3,
             7.00332514112805075473E3,
             5.55923013010394962768E4
           };
        double[] U = {
             3.35617141647503099647E1,
             5.21357949780152679795E2,
             4.59432382970980127987E3,
             2.26290000613890934246E4,
             4.92673942608635921086E4
           };
        if (Math.Abs(x) > 1.0) 
          return (1.0 - erfc(x));
        z = x*x;
        y = x*polevl(z, T, 4)/p1evl(z, U, 5);
        return y;
      }
  
      private static double polevl(double x, double[] coef, int N)
      {
        double ans;
        ans = coef[0];
        for (int i = 1; i <= N; i++)
        {
          ans = ans*x + coef[i];
        }
        return ans;
      }
  
      private static double p1evl(double x, double[] coef, int N)
      {
        double ans;
        ans = x + coef[0];
        for (int i = 1; i < N; i++)
          ans = ans*x + coef[i];
        return ans;
      }
  
// ----------------------------------------------------------------------------
// Fin Code pomp�
// ----------------------------------------------------------------------------
  
      private double MagicFunction(double x)
      {
        return .5*erfc(2.35702*(x-.5));
      }
     
      protected override void OnTick()
      {
        if (m_From == null)
          return;
        
        double skill = m_From.Skills[SkillName.ItemID].Base;
        double[] chance = GetChance(skill);
        
        double rand = Utility.RandomDouble();
        int index = chance.Length-1;
        
        while (chance[index] >= rand)
          index--;
        
        AlertMessage(m_From, index);
        
        Item loot = null;
        
        switch (index)
        {
          case 0 :  break;
          case 1 :  SpawnMonster(); break;
          case 2 :  loot = new Bone(1+GetAttribute(skill,19)); break;
          case 3 :  loot = new SoulGem(); break;
          case 4 :  loot = new Gold(20+GetAttribute(skill,230)); break;
          case 5 :
          {
            switch ( Utility.Random( 10 ) )
            {
              default:  loot = new Bone(); break;
              case 0:   loot = new Brain(); break;
              case 1:   loot = new Torso(); break;
              case 2:   loot = new Head(); break;
              case 3:   loot = new Heart(); break;
              case 4:   loot = new LeftLeg(); break;
              case 5:   loot = new RightLeg(); break;
              case 6:   loot = new Entrails(); break;
              case 7:   loot = new LeftArm(); break;
              case 8:   loot = new RightArm(); break;
              case 9:   loot = new Liver(); break;
            }
            break;
          }
          case 6 :  
                // Function could return null
                while (loot == null)
                {
                    loot = Loot.RandomPossibleReagent();
                }
                loot.Amount = 2+GetAttribute(skill,28); 
                break;
          case 7 :  
                // Function could return null
                while (loot == null)
                {
                    loot = Loot.RandomGem(); 
                }
                loot.Amount = 2+GetAttribute(skill,28);
                break;
          case 8 :  
                // Function could return null
                while (loot == null) 
                {
                    loot = Loot.RandomScroll();
                }
                break;
          case 9 :  
              // Function could return null
              while (loot == null)
              {
                  loot = Loot.RandomScroll();
              }
              break;
          case 10 :  loot = new GraveItem(); break;
          case 11 :  
          {
                // Function could return null
                while (loot == null || (!(loot is BaseArmor) && !(loot is BaseJewel) && !(loot is BaseWeapon)))
                {
                    loot = Loot.RandomArmorOrShieldOrWeaponOrJewelry();
                }

                if (loot is BaseWeapon)
                    BaseRunicTool.ApplyAttributesTo(loot as BaseWeapon, GetAttribute(skill,7), 10, 20+GetAttribute(skill,80)); 
                else if (loot is BaseArmor)
                    BaseRunicTool.ApplyAttributesTo(loot as BaseArmor, GetAttribute(skill,7), 10, 20+GetAttribute(skill,80)); 
                else if (loot is BaseJewel)
                    BaseRunicTool.ApplyAttributesTo(loot as BaseJewel, GetAttribute(skill,7), 10, 20+GetAttribute(skill,80));
                break;
          }
          case 12:  loot = new MagicJewel(); break;
        }
        
        if (loot != null)
          m_From.AddToBackpack(loot);
    
        Stop();
      }
      
      private void SpawnMonster()
      {
        Mobile spawn;
        
        switch ( Utility.Random( 13 ) )
        {
          default:  spawn = new Zombie(); break;
          case 0:   spawn = new HeadlessOne(); break;
          case 1:   spawn = new RestlessSoul(); break;
          case 2:   spawn = new Skeleton(); break;
          case 3:   spawn = new Zombie(); break;
          case 4:   spawn = new Wraith(); break;
          case 5:   spawn = new Spectre(); break;
          case 6:   spawn = new Bogle(); break;
          case 7:   spawn = new Ghoul(); break;
          case 8:   spawn = new BoneMagi(); break;
          case 9:   spawn = new Mummy2(); break;
          case 10:  spawn = new BoneKnight(); break;
          case 11:  spawn = new SkeletalKnight(); break;
          case 12:  spawn = new Lich(); break;
        }
        
        spawn.MoveToWorld( m_From.Location, m_From.Map );
      }
    }


    private static void AlertMessage(Mobile from, int Message)
    {
      string text = null;

      switch (Message)
      {
        case 0:   text = "Vous ne trouvez rien !"; break;
        case 1:   text = "Vous troublez le r�veil d'un mort d'un grand coup de pelle dans le cr�ne..."; break;
        case 2:   text = "Vous d�terrez quelques os sans grande valeur."; break;
        case 3:   text = "Vous exhumez une pierre �trangement scintillante."; break;
        case 4:   text = "Vous d�couvrer quelques esp�ces sonnantes et tr�buchantes."; break;
        case 5:   text = "Vous trouvez... un bout de d�funt. Etrange dans une tombe."; break;
        case 6:   text = "Vous trouvez quelques r�actifs."; break;
        case 7:   text = "Vous trouvez quelque gemmes, les morts sont riches !"; break;
        case 8:   text = "Vous d�terrez un vieux parchemin crasseux."; break;
        case 9:   text = "Il semble que vous soyez tomb� sur quelques �crits proscrits."; break; 
        case 10:  text = "Vous d�terrez un vieil objet en mauvais �tat."; break;
        case 11:  text = "Vous exhumez les effets de quelques aventuriers et trouvez un objet qui semblerait magique."; break;
        case 12:  text = "Vous trouvez une gemme dont �mane une puissance palpable."; break;
        default : text = "Vous d�terrez quelque chose."; break;
      }

      if (from != null)
        from.SendMessage(text);
    }


    private class GraveTarget : Target
    {
      //Grave ItemIDs
      private static int[] m_Grave = new int[]
      {
        3795,
        3807,
        3808,
        3809,
        3810,
        3816
      };

      private GraveDiggersShovel m_Tool;

      public GraveTarget(GraveDiggersShovel tool) : base(12, false, TargetFlags.None)
      {
        m_Tool = tool;
      }

      public static void EndAction(object state)
      {
        ((Mobile)state).EndAction(typeof(GraveDiggersShovel));
      }

      private static bool IsGrave(int itemID)
      {
        bool isGrave = false;
              
        foreach (int check in m_Grave)
        {
          if (check == itemID)
          {
            isGrave = true;
            break;
          }
        }
     
        return isGrave;
      }
      
      private static GraveRessource GetTomb(Point3D location)
      {
        for (int i=0; i < m_GraveList.Count; ++i)
          if ((m_GraveList[i] as GraveRessource).GetLocation() == location)
            return (GraveRessource)m_GraveList[i];
        return new GraveRessource(location);
      }

      protected override void OnTarget(Mobile from, object targeted)
      {
        if (targeted is StaticTarget && !Utility.InRange(from.Location, (targeted as StaticTarget).Location, 1))
        {
          from.SendMessage("Vous ne pouvez pas creuser aussi loin.");
          return;
        }
      
        if (targeted is Item && !Utility.InRange(from.Location, (targeted as Item).Location, 1))
        {
          from.SendMessage("Vous ne pouvez pas creuser aussi loin.");
          return;
        }      
     
        bool isGrave = false;
              
        if (targeted is StaticTarget)
          isGrave = IsGrave((targeted as StaticTarget).ItemID);
              
        else if (targeted is Item)
          isGrave = IsGrave((targeted as Item).ItemID);
                
        if (!isGrave || m_Tool == null)
        {
          from.SendMessage("Ceci n'est pas une tombe.");
          return;
        }
        
        GraveRessource tomb;
        if (targeted is StaticTarget)
          tomb = GetTomb((targeted as StaticTarget).Location);
        else
          tomb = GetTomb((targeted as Item).Location);
        
        if (!tomb.CanDig())
        {
          from.SendMessage("Cette tombe a l'air d'avoir d�j� �t� profan�e r�cemment.");
          return;
        }
        
        if (from.CanBeginAction(typeof(GraveDiggersShovel)))
        {
          from.BeginAction(typeof(GraveDiggersShovel));
          Timer.DelayCall(TimeSpan.FromSeconds(10.0), new TimerStateCallback(EndAction), from);
          
          tomb.Consume();
          
          m_Tool.Uses--;
          if (m_Tool.Uses < 1)
          {
            m_Tool.Delete();
            from.SendMessage("La pelle se rompt � cause de l'usure.");
          }
          
          from.SendMessage("Vous commencez � creuser.");
          (new DigTimer(from, TimeSpan.FromSeconds(10.0))).Start();
          from.PlaySound(Utility.RandomList(0x125, 0x126));
          from.Animate(11, 1, 1, true, false, 0);
        }
        else
        {
          from.Damage(Utility.Random(5,15), null);  
          from.SendMessage("Vous vous blessez en tentant de creuser trop vite !");
        }
      }
    }  
  }
}