using System;
using Server;

namespace Server.Items
{
	public class FollowerOfTheOldLord : BodySash
	{
		[Constructable]
		public FollowerOfTheOldLord()
		{
			Name = "Disciple de l'ancien";
			Hue = 1150;           
		}

		public FollowerOfTheOldLord( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}