using System;

namespace Server.Items
{
	public class Hop: CrepusculeItem, ICommodity
	{
		string ICommodity.Description
		{
			get
			{
                return String.Format(Amount == 1 ? "{0} houblon" : "{0} houblons", Amount);
			}
		}

		[Constructable]
		public Hop() : this(1)
        {
		}

		[Constructable]
		public Hop(int amount) : base(0x1AA2)
		{
			Stackable = true;
			Weight = 0.1;
			Amount = amount;
		}

		public Hop(Serial serial) : base(serial)
		{
		}

		public override Item Dupe(int amount)
		{
			return base.Dupe(new Hop(amount), amount);
		}

		public override void Serialize(GenericWriter writer)
		{
			base.Serialize(writer);

			writer.Write((int) 0);
		}

		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize(reader);

			int version = reader.ReadInt();
		}
	}
}