using System;

namespace Server.Items
{
	public abstract class Juice: CrepusculeItem
	{
		private Mobile m_Poisoner;
		private Poison m_Poison;
		private int m_FillFactor;
		
		public virtual Item EmptyItem{ get { return null; } }
		
		[CommandProperty( AccessLevel.GameMaster )]
		public Mobile Poisoner
		{
			get { return m_Poisoner; }
			set { m_Poisoner = value; }
		}
		
		[CommandProperty( AccessLevel.GameMaster )]
		public Poison Poison
		{
			get { return m_Poison; }
			set { m_Poison = value; }
		}
		
		[CommandProperty( AccessLevel.GameMaster )]
		public int FillFactor
		{
			get { return m_FillFactor; }
			set { m_FillFactor = value; }
		}
		
		public Juice( int itemID ) : base( itemID )
		{
			this.FillFactor = 4;
		}
		
		public Juice( Serial serial ) : base( serial )
		{
		}
		
		public void Boire( Mobile from )
		{
			if ( soif( from, m_FillFactor ) )
			{
				// Play a random "Boire" sound
				from.PlaySound( Utility.Random( 0x30, 2 ) );
				
				if ( from.Body.IsHuman && !from.Mounted )
					from.Animate( 34, 5, 1, true, false, 0 );
				
				if ( m_Poison != null )
					from.ApplyPoison( m_Poisoner, m_Poison );
				
				this.Consume();
				
				Item item = EmptyItem;
				
				if ( item != null )
					from.AddToBackpack( item );
			}
		}
		
		static public bool soif( Mobile from, int fillFactor )
		{
			if ( from.Thirst >= 20 )
			{
				from.SendMessage( "You are simply too full to drink any more!" );
				return false;
			}
			
			int iThirst = from.Thirst + fillFactor;
			if ( iThirst >= 20 )
			{
				from.Thirst = 20;
				from.SendMessage( "You manage to drink the beverage, but you are full!" );
			}
			else
			{
				from.Thirst = iThirst;
				
				if ( iThirst < 5 )
					from.SendMessage( "You drink the beverage, but are still extremely thirsty." );
				else if ( iThirst < 10 )
					from.SendMessage( "You drink the beverage, and begin to feel more satiated." );
				else if ( iThirst < 15 )
					from.SendMessage( "After drinking the beverage, you feel much less thirsty." );
				else
					from.SendMessage( "You feel quite full after consuming the beverage." );
			}
			
			return true;
		}
		
		
		
		public override void OnDoubleClick( Mobile from )
		{
			if ( !Movable )
				return;
			
			if ( from.InRange( this.GetWorldLocation(), 1 ) )
				Boire( from );
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 1 ); // version
			writer.Write( m_Poisoner );
			Poison.Serialize( m_Poison, writer );
			writer.Write( m_FillFactor );
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
			
			switch ( version )
			{
				case 1:
				{
					m_Poisoner = reader.ReadMobile();

					goto case 0;
				}
				case 0:
				{
					m_Poison = Poison.Deserialize( reader );
					m_FillFactor = reader.ReadInt();
					break;
				}
			}
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfGrapeJuice : Juice
	{
		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfGrapeJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de raisin";
		}
		
		public BottleOfGrapeJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfPumpkinJuice : Juice
	{
		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfPumpkinJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="bottle of pumpkin juice";
		}
		
		public BottleOfPumpkinJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfLemonJuice : Juice
	{
		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfLemonJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de citron";
		}
		
		public BottleOfLemonJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfAppleJuice : Juice
	{
		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfAppleJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de pommes";
		}
		
		public BottleOfAppleJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfPearJuice : Juice
	{

		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfPearJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de poire";
		}
		
		public BottleOfPearJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfBananaJuice : Juice
	{

		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfBananaJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de banane";
		}
		
		public BottleOfBananaJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfWatermelonJuice : Juice
	{
		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfWatermelonJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de pasteque";
		}
		
		public BottleOfWatermelonJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
	public class BottleOfPeachJuice : Juice
		{

		public override Item EmptyItem{ get { return new Bottle(); } }
		
		[Constructable]
		public BottleOfPeachJuice() : base( 0x0f09 )
		{
			this.Weight = 0.2;
			this.FillFactor = 4;
			this.Name ="Jus de p�che";
		}
		
		public BottleOfPeachJuice( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
    public class BottleOfGrapeWine : BeverageBottle
	{

		[Constructable]
        public BottleOfGrapeWine() : base(BeverageType.Wine)
		{
			this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille du vin rouge";
		}
		
		public BottleOfGrapeWine( Serial serial ) : base( serial )
		{
		}
		
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 ); // version
		}
		
		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
	}
//-------------------------------------------------------------------------------------
    public class BottleOfCider : BeverageBottle
    {

        [Constructable]
        public BottleOfCider()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille du cidre";
        }

        public BottleOfCider(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfAle1 : BeverageBottle
    {

        [Constructable]
        public BottleOfAle1()
            : base(BeverageType.Ale)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de bi�re (Lager)";
        }

        public BottleOfAle1(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfLiquor : BeverageBottle
    {

        [Constructable]
        public BottleOfLiquor()
            : base(BeverageType.Liquor)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de peket";
        }

        public BottleOfLiquor(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfVodka : BeverageBottle
    {

        [Constructable]
        public BottleOfVodka()
            : base(BeverageType.Liquor)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de vodka";
        }

        public BottleOfVodka(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfVinBlanc : BeverageBottle
    {

        [Constructable]
        public BottleOfVinBlanc()
            : base(BeverageType.Wine)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de vin blanc";
        }

        public BottleOfVinBlanc(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfVinRose : BeverageBottle
    {

        [Constructable]
        public BottleOfVinRose()
            : base(BeverageType.Wine)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de vin ros�";
        }

        public BottleOfVinRose(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfVinGlace : BeverageBottle
    {

        [Constructable]
        public BottleOfVinGlace()
            : base(BeverageType.Wine)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de vin de gla�e";
        }

        public BottleOfVinGlace(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfVinPorto : BeverageBottle
    {

        [Constructable]
        public BottleOfVinPorto()
            : base(BeverageType.Wine)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de porto";
        }

        public BottleOfVinPorto(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfVinMouss : BeverageBottle
    {

        [Constructable]
        public BottleOfVinMouss()
            : base(BeverageType.Wine)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de vin mousseaux";
        }

        public BottleOfVinMouss(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfAle2 : BeverageBottle
    {

        [Constructable]
        public BottleOfAle2()
            : base(BeverageType.Ale)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de bi�re (Block)";
        }

        public BottleOfAle2(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
//-------------------------------------------------------------------------------------
    public class BottleOfAle3 : BeverageBottle
    {

        [Constructable]
        public BottleOfAle3()
            : base(BeverageType.Ale)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de bi�re (Ale)";
        }

        public BottleOfAle3(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfAle4 : BeverageBottle
    {

        [Constructable]
        public BottleOfAle4()
            : base(BeverageType.Ale)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de bi�re (Stout)";
        }

        public BottleOfAle4(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfAle5 : BeverageBottle
    {

        [Constructable]
        public BottleOfAle5()
            : base(BeverageType.Ale)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de bi�re (S�che)";
        }

        public BottleOfAle5(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfRhum : BeverageBottle
    {

        [Constructable]
        public BottleOfRhum()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de rhum";
        }

        public BottleOfRhum(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfGin : BeverageBottle
    {

        [Constructable]
        public BottleOfGin()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de gin";
        }

        public BottleOfGin(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfHydromel : BeverageBottle
    {

        [Constructable]
        public BottleOfHydromel()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de l'hydromel";
        }

        public BottleOfHydromel(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfCidre2 : BeverageBottle
    {

        [Constructable]
        public BottleOfCidre2()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de cidre de pomme";
        }

        public BottleOfCidre2(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfWhisky : BeverageBottle
    {

        [Constructable]
        public BottleOfWhisky()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de whisky";
        }

        public BottleOfWhisky(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------
    public class BottleOfCognac : BeverageBottle
    {

        [Constructable]
        public BottleOfCognac()
            : base(BeverageType.Cider)
        {
            this.Weight = 0.2;
            this.Quantity = 4;
            this.Name = "Bouteille de cognac";
        }

        public BottleOfCognac(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
    //-------------------------------------------------------------------------------------


}


