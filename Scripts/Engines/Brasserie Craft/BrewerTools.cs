using System;
using Server;
using Server.Engines.Craft;

namespace Server.Items
{
	public class BrewerTools : BaseTool
	{
		[Constructable]
		public BrewerTools() : base( 0xE7F )
		{
            Name = "Outils de brasseur";
			Weight = 2.0;
		}

		[Constructable]
		public BrewerTools( int uses ) : base( uses, 0xE7F )
		{
            Name = "Outils de brasseur";
			Weight = 2.0;
		}

		public BrewerTools( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			if ( Weight == 1.0 )
				Weight = 2.0;
		}
	}
}