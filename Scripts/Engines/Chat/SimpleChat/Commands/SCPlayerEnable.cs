﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Engines.Chat.SimpleChat.Core;

namespace Server.Engines.Chat.SimpleChat.Commands
{
    class SCPlayerEnable
    {
        public static void Initialize()
        {
            CommandSystem.Register("ChatEnable", AccessLevel.Player, new CommandEventHandler(Enable_OnCommand));
        }

        [Usage("ChatEnable")]
        [Description("Enable chat")]
        private static void Enable_OnCommand(CommandEventArgs e)
        {
            e.Mobile.SendMessage(SimpleChat.Core.SimpleChat.SwitchEnable(e.Mobile.Serial.Value) ? "Le chat est actif" : "Le chat est inactif");
        }
    }
}
