﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Engines.Chat.SimpleChat.Core;
using Server.Mobiles;

namespace Server.Engines.Chat.SimpleChat.Commands
{
    class SCConfig
    {
        public static void Initialize()
        {
            CommandSystem.Register("SCConfig", AccessLevel.GameMaster, new CommandEventHandler(SCConfig_OnCommand));
        }

        [Usage("SCConfig")]
        [Description("Simple chat administration")]
        private static void SCConfig_OnCommand(CommandEventArgs e)
        {
            if (e.Mobile is RacePlayerMobile)
            {
                e.Mobile.SendGump(new SCConfigGump((RacePlayerMobile)e.Mobile));
            }
        }
    }
}
