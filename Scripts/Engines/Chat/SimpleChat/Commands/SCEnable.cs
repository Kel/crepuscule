﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Engines.Chat.SimpleChat.Core;
using Server.Network;

namespace Server.Engines.Chat.SimpleChat.Commands
{
    class SCEnable
    {
        public static void Initialize()
        {
            CommandSystem.Register("SCEnable", AccessLevel.Administrator, new CommandEventHandler(Enable_OnCommand));
        }

        [Usage("SCEnable")]
        [Description("Enable simple chat")]
        private static void Enable_OnCommand(CommandEventArgs e)
        {
            String message = "[Chat] " + (SimpleChat.Core.SimpleChat.SwitchEnable() ? "Le chat vient d'être activé" : "Le chat vient d'être désactivé");

            foreach ( NetState state in NetState.Instances )
			{
				Mobile m = state.Mobile;

				if ( m != null)
					m.SendMessage(37, message);
			}
        }
    }
}
