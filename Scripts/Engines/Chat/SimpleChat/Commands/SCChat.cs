﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Engines.Chat.SimpleChat.Core;
using Server.Mobiles;

namespace Server.Engines.Chat.SimpleChat.Commands
{
    class SCChat
    {
        public static void Initialize()
        {
            CommandSystem.Register("Chat", AccessLevel.Player, new CommandEventHandler(Chat_OnCommand));
            CommandSystem.Register("C", AccessLevel.Player, new CommandEventHandler(Chat_OnCommand));
        }

        [Usage("Chat <text>")]
        [Aliases("C")]
        [Description("Send chat message")]
        private static void Chat_OnCommand(CommandEventArgs e)
        {
            if (e.Mobile is RacePlayerMobile)
            {
                if (!SimpleChat.Core.SimpleChat.Chat(e.Mobile.Serial.Value, (PlayerMobile) e.Mobile, e.ArgString))
                {
                    e.Mobile.SendMessage(SimpleChat.Core.SimpleChat.IsEnable() && SimpleChat.Core.SimpleChat.IsEnable(e.Mobile.Serial.Value) ? "Votre accès au chat a été restreint" : "Le chat est inactif");
                }
            }
        }
    }
}
