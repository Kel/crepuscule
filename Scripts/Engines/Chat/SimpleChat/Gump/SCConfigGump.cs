﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Gumps;
using Server.Mobiles;
using Server.Network;

using Server.Engines.Chat.SimpleChat.Core;

namespace Server.Engines.Chat.SimpleChat
{
    class SCConfigGump : Gump
    {
        private RacePlayerMobile m_From;
        private ArrayList m_Mobiles;
        private Hashtable m_Configs;
        private int m_Page;

        public SCConfigGump(RacePlayerMobile from)
            : this(from, WhoGump.BuildList(from), 0)
        {
        }

        public SCConfigGump(RacePlayerMobile from, ArrayList list, int page)
            : base(30,30)
        {
            from.CloseGump(typeof(SCConfigGump));
            m_From = from;
            ArrayList mobiles = list;
            m_Configs = SimpleChat.Core.SimpleChat.GetAllPlayerConfigs();

            m_Mobiles = new ArrayList();

            foreach (Mobile m in mobiles)
            {
                if (m_Configs.Contains(m.Serial.Value))
                    m_Mobiles.Add(m);
            }

            Initialize(page);
        }

        public void Initialize(int page)
        {
            m_Page = page;

            int count = m_Mobiles.Count - (page * 25);

            if (count < 0)
                count = 0;
            else if (count > 25)
                count = 25;

            int totalHeight = 1 + ((20 + 1) * (count + 1));

            AddPage(0);

            AddBackground(0, 0, 343, 10 + totalHeight + 10, 0x13BE);
            AddImageTiled(10, 10, 323, totalHeight, 0x0A40);

            int x = 10 + 1;
            int y = 10 + 1;

            int emptyWidth = 323 - 20 - 20 - (1 * 4);

            AddImageTiled(x, y, emptyWidth, 20, 0x0BBC);

            AddLabel(x + 2, y, 0, String.Format("Page {0} of {1} ({2})", page + 1, (m_Mobiles.Count + 25 - 1) / 25, m_Mobiles.Count));

            x += emptyWidth + 1;

            AddImageTiled(x, y, 20, 20, 0x0E14);

            if (page > 0)
            {
                AddButton(x + 2, y + 2, 0x15E3, 0x15E7, 1, GumpButtonType.Reply, 0);
            }

            x += 20 + 1;

            AddImageTiled(x, y, 20, 20, 0x0E14);

            if ((page + 1) * 25 < m_Mobiles.Count)
            {
                AddButton(x + 2, y + 2, 0x15E1, 0x15E5, 2, GumpButtonType.Reply, 1);
            }

            for (int i = 0, index = page * 25; i < 25 && index < m_Mobiles.Count; ++i, ++index)
            {
                x = 10 + 1;
                y += 20 + 1;

                Mobile m = (Mobile)m_Mobiles[index];

                SCPlayerConfig config = (SCPlayerConfig)m_Configs[m.Serial.Value];

                AddImageTiled(x, y, 300, 20, 0x0BBC);

                String whoLine = m is RacePlayerMobile ? ((RacePlayerMobile)m).PlayerName : m.Name;

                int hue = m.SpeechHue;

                if (config.GetBan() > DateTime.Now || config.GetMute() > DateTime.Now)
                {
                    hue = 34;
                }

                if (m.AccessLevel == AccessLevel.Player)
                {
                    hue = 20;
                }

                if (!config.IsEnable())
                {
                    hue = 0;
                }

                if (config.GetBan() > DateTime.Now)
                {
                    whoLine += " [Ban: " + config.GetBan().ToString("dd/MM HH:mm") + "]";
                }

                if (config.GetMute() > DateTime.Now)
                {
                    whoLine += " [Mute: " + config.GetMute().ToString("dd/MM HH:mm") + "]";
                }

                AddLabelCropped(x + 2, y, 300 - 2, 20, hue, m.Deleted ? "(effacé)" : whoLine);

                x += 300 + 1;

                AddImageTiled(x, y, 20, 20, 0x0E14);

                if (m.NetState != null && !m.Deleted)
                    AddButton(x + 2, y + 2, 0x15E1, 0x15E5, i + 3, GumpButtonType.Reply, 0);
            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            Mobile from = state.Mobile;

            switch (info.ButtonID)
            {
                case 0: // Closed
                    {
                        return;
                    }
                case 1: // Previous
                    {
                        if (m_Page > 0)
                            from.SendGump(new WhoGump(from, m_Mobiles, m_Page - 1));

                        break;
                    }
                case 2: // Next
                    {
                        if ((m_Page + 1) * 25 < m_Mobiles.Count)
                            from.SendGump(new WhoGump(from, m_Mobiles, m_Page + 1));

                        break;
                    }
                default:
                    {
                        int index = (m_Page * 25) + (info.ButtonID - 3);

                        if (index >= 0 && index < m_Mobiles.Count)
                        {
                            Mobile m = (Mobile)m_Mobiles[index];

                            if (m.Deleted)
                            {
                                from.SendMessage("That player has deleted their character.");
                                from.SendGump(new WhoGump(from, m_Mobiles, m_Page));
                            }
                            else if (m.NetState == null)
                            {
                                from.SendMessage("That player is no longer online.");
                                from.SendGump(new WhoGump(from, m_Mobiles, m_Page));
                            }
                            else if (m == m_From || !m.Hidden || m_From.AccessLevel > m.AccessLevel)
                            {
                                from.SendGump(new SCPlayerConfigGump(from, (SCPlayerConfig) m_Configs[m.Serial.Value], ((RacePlayerMobile) m).PlayerName));
                            }
                            else
                            {
                                from.SendMessage("You cannot see them.");
                                from.SendGump(new WhoGump(from, m_Mobiles, m_Page));
                            }
                        }

                        break;
                    }
            }
        }

        private static int GetHueFor(Mobile m)
        {
            switch (m.AccessLevel)
            {
                case AccessLevel.Administrator: return 0x516;
                case AccessLevel.Seer: return 0x144;
                case AccessLevel.GameMaster: return 0x21;
                case AccessLevel.Counselor: return 0x2;
                case AccessLevel.Player:
                default:
                    {
                        if (m.Kills >= 5)
                            return 0x21;
                        else if (m.Criminal)
                            return 0x3B1;

                        return 0x58;
                    }
            }
        }
    }
}
