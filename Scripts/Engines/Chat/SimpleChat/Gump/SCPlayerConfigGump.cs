﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Gumps;
using Server.Mobiles;
using Server.Network;

using Server.Engines.Chat.SimpleChat.Core;

namespace Server.Engines.Chat.SimpleChat
{
    class SCPlayerConfigGump : Gump
    {
        private SCPlayerConfig m_Config;
        private Mobile m_Mobile;
        private String m_Name;

        public SCPlayerConfigGump(Mobile from, SCPlayerConfig config, String name)
            : base(30, 30)
        {
            from.CloseGump(typeof(SCPlayerConfigGump));
            m_Mobile = from;
            m_Config = config;
            m_Name = name;

            Initialize();
        }

        private void Initialize()
        {
            int count = 3;

            int totalHeight = 1 + ((20 + 1) * (count + 1));

            AddPage(0);

            AddBackground(0, 0, 343, 10 + totalHeight + 10, 0x13BE);
            AddImageTiled(10, 10, 323, totalHeight, 0x0A40);

            int x = 10 + 1;
            int y = 10 + 1;

            int emptyWidth = 323 - 20 - 20 - (1 * 4);

            AddImageTiled(x, y, emptyWidth, 20, 0x0BBC);

            AddLabel(x + 2, y, 0, m_Name + " chat config");

            x += emptyWidth + 1;

            AddImageTiled(x, y, 20, 20, 0x0E14);

            x += 20 + 1;

            AddImageTiled(x, y, 20, 20, 0x0E14);

            for (int i = 0; i < 3; ++i)
            {
                x = 10 + 1;
                y += 20 + 1;

                AddImageTiled(x, y, 300, 20, 0x0BBC);

                String whoLine = string.Empty;
                int hue = 0;

                if (i == 0)
                {
                    whoLine = m_Config.IsEnable() ? "Chat enable" : "Chat disable";
                }
                else if (i == 1)
                {
                    if (m_Config.GetBan() > DateTime.Now)
                    {
                        hue = 34;
                        whoLine = "Banned " + m_Config.GetBan().ToString("dd/MM HH:mm");
                    }
                    else
                    {
                        whoLine = "Not banned";
                    }
                }
                else if (i == 2)
                {
                    if (m_Config.GetMute() > DateTime.Now)
                    {
                        hue = 34;
                        whoLine = "Muted " + m_Config.GetMute().ToString("dd/MM HH:mm");
                    }
                    else
                    {
                        whoLine = "Not muted";
                    }
                }

                AddLabelCropped(x + 2, y, 300 - 2, 20, hue, whoLine);

                x += 300 + 1;

                AddImageTiled(x, y, 20, 20, 0x0E14);

                if (i != 0)
                    AddButton(x + 2, y + 2, 0x15E1, 0x15E5, i, GumpButtonType.Reply, 0);
            }
        }

        public override void OnResponse(NetState state, RelayInfo info)
        {
            Mobile from = state.Mobile;

            switch (info.ButtonID)
            {
                case 0: // Enable
                    {
                        break;
                    }
                case 1: // Ban
                    {
                        from.SendGump(new SCSetTimeGump(from, m_Config, "Ban", m_Name));
                        break;
                    }
                case 2: // Mute
                    {
                        from.SendGump(new SCSetTimeGump(from, m_Config, "Mute", m_Name));
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }
}
