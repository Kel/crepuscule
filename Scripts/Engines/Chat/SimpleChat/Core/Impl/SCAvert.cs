﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server.Engines.Chat.SimpleChat.Core
{
    class SCAvert
    {
        private DateTime m_Date;
        private String m_Author;
        private String m_Comment;

        public DateTime GetDate()
        {
            return m_Date;
        }

        public String GetAuthor()
        {
            return m_Author;
        }

        public String GetComment()
        {
            return m_Comment;
        }

        public SCAvert(DateTime date, String author, String comment)
        {
            m_Date = date;
            m_Author = author == null ? string.Empty : author;
            m_Comment = comment == null ? string.Empty : comment;
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write(1); // version

            writer.Write(m_Date);
            writer.Write(m_Author);
            writer.Write(m_Comment);
        }

        public static SCAvert Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();

            DateTime date = reader.ReadDateTime();
            String author = reader.ReadString();
            String comment = reader.ReadString();

            return new SCAvert(date, author, comment);
        }
    }
}
