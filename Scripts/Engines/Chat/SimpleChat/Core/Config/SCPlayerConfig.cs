﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Mobiles;

namespace Server.Engines.Chat.SimpleChat.Core
{
    class SCPlayerConfig
    {
        private bool m_Enable;
        private List<SCAvert> m_Averts;
        private DateTime m_Ban;
        private DateTime m_Mute;

        public SCPlayerConfig() : this (false, DateTime.MinValue, DateTime.MinValue, null)
        {
        }

        public SCPlayerConfig(bool enable, DateTime ban, DateTime mute, List<SCAvert> averts)
        {
            m_Enable = enable;
            m_Ban = ban;
            m_Mute = mute;
            m_Averts = averts == null ? new List<SCAvert>() : averts;
        }

        public bool CanHear()
        {
            return m_Enable && m_Ban < DateTime.Now;
        }

        public bool CanSpeak()
        {
            return m_Enable && m_Mute < DateTime.Now && m_Ban < DateTime.Now;
        }

        public bool IsEnable()
        {
            return m_Enable;
        }

        public bool SwitchEnable()
        {
            m_Enable = !m_Enable;
            return m_Enable;
        }

        public List<SCAvert> GetAverts()
        {
            return m_Averts;
        }

        public DateTime Ban(DateTime ban)
        {
            m_Ban = ban;
            return m_Ban;
        }

        public DateTime Ban(TimeSpan time)
        {
            return Ban(DateTime.Now.Add(time));
        }

        public DateTime GetBan()
        {
            return m_Ban;
        }

        public DateTime Mute(DateTime mute)
        {
            m_Mute = mute;
            return m_Mute;
        }

        public DateTime Mute(TimeSpan time)
        {
            return Mute(DateTime.Now.Add(time));
        }

        public DateTime GetMute()
        {
            return m_Mute;
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write(1); // version

            writer.Write(m_Enable);
            writer.Write(m_Ban);
            writer.Write(m_Mute);

            writer.Write(m_Averts.Count);

            for (int i = 0; i < m_Averts.Count; ++i)
            {
                m_Averts[i].Serialize(writer);
            }
        }

        public static SCPlayerConfig Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();

            bool enable = reader.ReadBool();
            DateTime ban = reader.ReadDateTime();
            DateTime mute = reader.ReadDateTime();

            int count = reader.ReadInt();

            List<SCAvert> averts = new List<SCAvert>();

            for (int i = 0; i < count; ++i)
            {
                averts.Add(SCAvert.Deserialize(reader));
            }

            return new SCPlayerConfig(enable, ban, mute, averts);
        }
    }
}
