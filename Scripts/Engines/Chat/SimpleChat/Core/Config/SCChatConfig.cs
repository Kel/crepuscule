﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using Server.Mobiles;

namespace Server.Engines.Chat.SimpleChat.Core
{
    class SCChatConfig : Item
    {
        private bool m_Enable = true;

        private Hashtable m_PlayerConfigs = new Hashtable();

        public SCChatConfig() : base()
        {
        }

        public bool IsEnable()
        {
            return m_Enable;
        }

        public bool IsEnable(int playerSerial)
        {
            return m_PlayerConfigs.Contains(playerSerial) ? ((SCPlayerConfig)m_PlayerConfigs[playerSerial]).IsEnable() : false;
        }

        public bool SwitchEnable()
        {
            m_Enable = !m_Enable;
            return m_Enable;
        }

        public bool CanHear(int playerSerial)
        {
            return m_PlayerConfigs.Contains(playerSerial) ? ((SCPlayerConfig) m_PlayerConfigs[playerSerial]).CanHear() : false;
        }

        public bool CanSpeak(int playerSerial)
        {
            return m_PlayerConfigs.Contains(playerSerial) ? ((SCPlayerConfig)m_PlayerConfigs[playerSerial]).CanSpeak() : false;
        }

        public bool SwitchEnable(int playerSerial)
        {
            if (!m_PlayerConfigs.Contains(playerSerial))
            {
                m_PlayerConfigs[playerSerial] = new SCPlayerConfig();
            }

            return ((SCPlayerConfig)m_PlayerConfigs[playerSerial]).SwitchEnable();
        }

        public Hashtable GetPlayerConfigs()
        {
            return m_PlayerConfigs;
        }

        public SCChatConfig( Serial serial ) : base( serial )
		{
		}

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write(1); // version

            writer.Write(m_Enable);

            writer.Write(m_PlayerConfigs.Count);

            foreach (int key in m_PlayerConfigs.Keys)
            {
                writer.Write(key);
                ((SCPlayerConfig)m_PlayerConfigs[key]).Serialize(writer);
            }
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();

            m_Enable = reader.ReadBool();

            int count = reader.ReadInt();
            int key;
            m_PlayerConfigs = new Hashtable();
            for (int i = 0; i < count; ++i)
            {
                key = reader.ReadInt();
                m_PlayerConfigs[key] = SCPlayerConfig.Deserialize(reader);
            }
        }        
    }
}
