﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Server.Mobiles;
using Server.Network;

namespace Server.Engines.Chat.SimpleChat.Core
{
    class SimpleChat
    {
        private static SCChatConfig m_Config;
        private static Object m_Lock = new Object();
        private static bool m_AlternateHue = true;

        public static void Initialize()
        {
            Console.WriteLine("Looking for existing config");

            foreach (Item item in World.Items.Values)
            {
                if (item is SCChatConfig)
                {
                    Console.WriteLine("Config found");
                    m_Config = (SCChatConfig)item;
                    break;
                }
            }

            if (m_Config == null)
            {
                Console.WriteLine("Creating new config");
                m_Config = new SCChatConfig();
            }
        }

        public static bool Chat(int playerSerial, PlayerMobile from, String text)
        {
            if (m_Config == null && m_Config.IsEnable())
            {
                return false;
            }

            if (!m_Config.CanSpeak(playerSerial))
            {
                return false;
            }

            lock(m_Lock)
            {
                m_AlternateHue = !m_AlternateHue;
                foreach (NetState state in NetState.Instances)
                {
                    Mobile m = state.Mobile;

                    if (m != null && m is PlayerMobile && m_Config.CanHear(m.Serial.Value))
                    {
                        #region built header

                        string header = "[Chat]";
                        string name = "Inconnu";
                        int hue = from.AccessLevel >= AccessLevel.GameMaster ? (m_AlternateHue ? 14 : 522) : (m_AlternateHue ? 681 : 667);

                        if (m.AccessLevel >= AccessLevel.GameMaster)
                        {
                            name = from is RacePlayerMobile ? ((RacePlayerMobile)from).PlayerName : from.Name;
                        }
                        else
                        {
                            if (from.AccessLevel >= AccessLevel.GameMaster)
                            {
                                name = from is RacePlayerMobile ? ((RacePlayerMobile)from).PlayerName : from.Name;
                            }
                            else if (m == from)
                            {
                                name = m is RacePlayerMobile ? ((RacePlayerMobile)m).PlayerName : m.Name;
                            }
                            else
                            {
                                name = m is RacePlayerMobile && from is RacePlayerMobile ? ((RacePlayerMobile)from).GetKnownName1((RacePlayerMobile)m) : from.Name;
                            }
                        }

                        header += " " + name;

                        #endregion

                        m.SendMessage(hue, header + ": " + text);
                    }
                }
            }

            return true;
        }

        public static bool IsEnable()
        {
            return m_Config == null ? false : m_Config.IsEnable();
        }

        public static bool IsEnable(int playerSerial)
        {
            return m_Config == null ? false : m_Config.IsEnable(playerSerial);
        }

        public static Hashtable GetAllPlayerConfigs()
        {
            Hashtable output = new Hashtable();
            Hashtable playerConfigs = m_Config == null ? new Hashtable() : m_Config.GetPlayerConfigs();

            foreach (int key in playerConfigs.Keys)
            {
                Mobile m = World.FindMobile(key);

                if (m != null && m is RacePlayerMobile)
                {
                    output.Add(key, playerConfigs[key]);
                }
            }

            return output;
        }

        public static bool SwitchEnable()
        {
            return m_Config == null ? false : m_Config.SwitchEnable();
        }

        public static bool SwitchEnable(int playerSerial)
        {
            return m_Config == null ? false : m_Config.SwitchEnable(playerSerial);
        }
    }
}
