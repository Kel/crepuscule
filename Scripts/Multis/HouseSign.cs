using System;
using Server;
using Server.Multis;
using Server.Gumps;

namespace Server.Multis
{
	public class HouseSign: CrepusculeItem
	{
		private BaseHouse m_Owner;
		private Mobile m_OrgOwner;

		public HouseSign( BaseHouse owner ) : base( 0xBD2 )
		{
			m_Owner = owner;
			m_OrgOwner = m_Owner.Owner;
			Name = "a house sign";
			Movable = false;
		}

		public HouseSign( Serial serial ) : base( serial )
		{
		}

		public BaseHouse Owner
		{
			get
			{
				return m_Owner;
			}
		}

		[CommandProperty( AccessLevel.GameMaster )]
		public Mobile OriginalOwner
		{
			get
			{
				return m_OrgOwner;
			}
		}

		public override void OnAfterDelete()
		{
			base.OnAfterDelete();

			if ( m_Owner != null && !m_Owner.Deleted )
				m_Owner.Delete();
		}

		public override bool ForceShowProperties{ get{ return ObjectPropertyList.Enabled; } }

		public override void GetProperties( ObjectPropertyList list )
		{
			base.GetProperties( list );

			list.Add( 1061639, Name == "a house sign" ? "nothing" : Utility.FixHtml( Name ) ); // Name: ~1_NAME~
			list.Add( 1061640, (m_Owner == null || m_Owner.Owner == null) ? "nobody" : m_Owner.Owner.Name ); // Owner: ~1_OWNER~

			if ( m_Owner != null )
				list.Add( m_Owner.Public ? 1061641 : 1061642 ); // This House is Open to the Public : This is a Private Home
		}

		public override void OnDoubleClick( Mobile m )
		{
			if ( m_Owner != null )
			{
				if ( m_Owner.IsFriend( m ) )
					m.SendLocalizedMessage( 501293 ); // Welcome back to the house, friend!

				if ( m_Owner.IsAosRules )
					m.SendGump( new HouseGumpAOS( HouseGumpPageAOS.Information, m, m_Owner ) );
				else
					m.SendGump( new HouseGump( m, m_Owner ) );
			}
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version

			writer.Write( m_Owner );
			writer.Write( m_OrgOwner );
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			switch ( version )
			{
				/*case 1:
				{

					goto case 0;
				}*/
				case 0:
				{
					m_Owner = reader.ReadItem() as BaseHouse;
					m_OrgOwner = reader.ReadMobile();

					break;
				}
			}
		}
	}
}