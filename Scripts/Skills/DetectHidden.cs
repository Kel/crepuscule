using System;
using Server.Multis;
using Server.Targeting;
using Server.Items;
using Server.Regions;

namespace Server.SkillHandlers
{
	public class DetectHidden
	{
		public static void Initialize()
		{
			SkillInfo.Table[(int)SkillName.DetectHidden].Callback = new SkillUseCallback( OnUse );
		}

		public static TimeSpan OnUse( Mobile src )
		{

			double srcSkill = src.Skills[SkillName.DetectHidden].Value;
			int range = (int)(srcSkill / 10.0);

			bool foundAnyone = true;

			Point3D p;
			p = src.Location;

			BaseHouse house = BaseHouse.FindHouseAt( p, src.Map, 16 );

			bool inHouse = ( house != null && house.IsFriend( src ) );
					
			if ( inHouse )
				range = 22;

			if ( !src.CheckSkill( SkillName.DetectHidden, 0.0, 100.0 ) )
				range /= 2;

			if ( range > 0 )
			{
				IPooledEnumerable inRange = src.Map.GetMobilesInRange( p, range );

				foreach ( Mobile m in inRange )
				{
					if ( m.Hidden && src != m )
						{
						double ss = srcSkill + Utility.Random( 21 ) - 10;
						double ts = m.Skills[SkillName.Hiding].Value + Utility.Random( 21 ) - 10;

						if ( src.AccessLevel >= m.AccessLevel && ( ss >= ts || ( inHouse && house.IsInside( m ) ) ) )
						{
							m.RevealingAction();
							m.SendLocalizedMessage( 500814 ); // You have been revealed!
							foundAnyone = true;
						}
					}
				}
				inRange.Free();
			}

			if ( !foundAnyone )
			{
				src.SendLocalizedMessage( 500817 ); // You can see nothing hidden there.
			}
			
			return TimeSpan.FromSeconds( 1.0 );

		}	
	}
}
