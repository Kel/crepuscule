using System;
using System.Collections;
using Server;
using Server.Targeting;
using System.Collections.Generic;

namespace Server.Items
{
    public class ItemIdentification
    {
        private static int MinSkill = 30;
        private static int MaxSkill = 90;

        public static void Initialize()
        {
            SkillInfo.Table[(int)SkillName.ItemID].Callback = new SkillUseCallback(OnUse);
        }

        public static TimeSpan OnUse(Mobile from)
        {
            from.SendLocalizedMessage(500343); // What do you wish to appraise and identify?
            from.Target = new InternalTarget();

            return TimeSpan.FromSeconds(1.0);
        }

        private class InternalTarget : Target
        {
            public InternalTarget()
                : base(8, false, TargetFlags.None)
            {
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is Item)
                {
                    if (!((Item)o).IsChildOf(from.Backpack))
                    {
                        from.SendLocalizedMessage(1042001);
                        return;
                    }
                    if (from.CheckTargetSkill(SkillName.ItemID, o, 0, 100))
                    {
                        AosAttributes A;
                        var list = new List<string>();
                        int prop;
                        double skill = from.Skills[SkillName.ItemID].Value;

                        // Gets the Hidden Description
                        if (o is CrepusculeItem)
                        {
                            var item = o as CrepusculeItem;
                            if (!String.IsNullOrEmpty(item.HiddenDescription) && from.CheckTargetSkill(SkillName.ItemID, o, 10, 50))
                            {
                                item.PlayerDescription = item.HiddenDescription;
                                from.SendMessage("Vous remarquez une inscription: " + item.HiddenDescription);
                            }
                        }

                        if (o is BaseWeapon)
                        {
                            A = ((BaseWeapon)o).Attributes;
                            ((BaseWeapon)o).Identified = true;
                            AosWeaponAttributes W = ((BaseWeapon)o).WeaponAttributes;
                            if ((prop = W.HitFireArea) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Aura Flamboyante ({0})", AuraScale(prop, skill)));
                            if ((prop = W.HitEnergyArea) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Aura Foudroyante ({0})", AuraScale(prop, skill)));
                            if ((prop = W.HitColdArea) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Aura Glaciale ({0})", AuraScale(prop, skill)));
                            if ((prop = W.HitPhysicalArea) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Aura Tellurique ({0})", AuraScale(prop, skill)));
                            if ((prop = W.HitPoisonArea) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Aura Toxique ({0})", AuraScale(prop, skill)));
                            if ((prop = W.SelfRepair) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("R�paration ({0})", StatScale(prop, skill, false)));
                            if ((prop = W.MageWeapon) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Maniement Magique ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.UseBestSkill) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add("Maniement Universel");
                            if ((prop = W.HitLowerDefend) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Amollissant ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitLeechStam) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Enduvore ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitLowerAttack) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Entravant ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitFireball) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Flamboyant ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitLightning) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Foudroyant ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitHarm) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Glacial ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitMagicArrow) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Magique ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitLeechMana) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Manavore ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitDispel) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� R�voquant ({0})", BonusScale(prop, skill, true)));
                            if ((prop = W.HitLeechHits) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("Touch� Vampirique ({0})", BonusScale(prop, skill, true)));
                        }
                        else if (o is BaseArmor)
                        {
                            A = ((BaseArmor)o).Attributes;
                            AosArmorAttributes W = ((BaseArmor)o).ArmorAttributes;
                            if ((prop = W.MageArmor) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add("Armure De Mage");
                            if ((prop = W.SelfRepair) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                                list.Add(String.Format("R�paration ({0})", StatScale(prop, skill, false)));
                        }
                        else if (o is BaseJewel)
                            A = ((BaseJewel)o).Attributes;
                        else
                        {
                            from.SendLocalizedMessage(500353);
                            return;
                        }

                        if ((prop = A.BonusDex) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Agilit� Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.Luck) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Chance Accrue ({0})", LuckScale(prop, skill)));
                        if ((prop = A.DefendChance) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("D�fense Accrue ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.WeaponDamage) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Dommages Accrus ({0})", BonusScale(prop, skill, true)));
                        if ((prop = A.LowerManaCost) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Effort Magique Diminu� ({0})", BonusScale(prop, skill, true)));
                        if ((prop = A.BonusStam) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Endurance Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.BonusMana) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Energie Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.BonusStr) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Force Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.BonusInt) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Intelligence Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.LowerRegCost) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Perm�abilit� Magique Accrue ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.SpellChanneling) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add("Port Magique");
                        if ((prop = A.AttackChance) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Pr�cision Accrue ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.SpellDamage) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Puissance Magique Accrue ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.RegenStam) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("R�cup�ration Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.CastRecovery) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("R�cup�ration Magique Accrue ({0})", MagicScale(prop, skill, false)));
                        if ((prop = A.RegenHits) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("R�g�n�ration Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.ReflectPhysical) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("R�flection Physique ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.RegenMana) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("R�g�n�ration Magique Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.EnhancePotions) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Sensibilit�  Alchimique Accrue ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.BonusHits) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Vitalit� Accrue ({0})", StatScale(prop, skill, false)));
                        if ((prop = A.WeaponSpeed) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Vitesse Accrue ({0})", BonusScale(prop, skill, false)));
                        if ((prop = A.CastSpeed) != 0 && from.CheckTargetSkill(SkillName.ItemID, o, MinSkill, MaxSkill))
                            list.Add(String.Format("Vitesse Magique Accrue ({0})", MagicScale(prop, skill, false)));

                        ((CrepusculeItem)o).Identification = list;
                        from.SendMessage("Vous identifiez l'objet...");
                    }
                    else
                    {
                        from.SendLocalizedMessage(500353); // You are not certain...
                    }
                }
                else if (o is Mobile)
                {
                    ((Mobile)o).OnSingleClick(from);
                }
                else
                {
                    from.SendLocalizedMessage(500353); // You are not certain...
                }
            }
        }



        private static int Variation(int value, double skill, double step, int vmax, int smax)
        {
            value = Math.Max((int)Math.Ceiling(value / step), 0);
            if (skill < Utility.RandomMinMax(0, smax))
            {
                vmax = (int)Math.Ceiling((vmax * (100 - skill)) / 100);
                value += Utility.RandomMinMax(0, 2 * vmax) - vmax;
            }
            return value;
        }

        private static string LuckScale(int value, double skill)
        {
            value = Variation(value, skill, 20, 2, 80);
            switch (value)
            {
                case 0: return "Nulle";
                case 1: return "Tr�s Faible";
                case 2: return "Faible";
                case 3: return "Moyenne";
                case 4: return "Forte";
                case 5: return "Tr�s Forte";
                case 6: return "Exceptionnelle";
                default: return "Divine";
            }
        }

        private static string MagicScale(int value, double skill, bool type)
        {
            value = Variation(value, skill, 1, 1, 80);
            switch (value)
            {
                case 0: return (type ? "Nul" : "Nulle");
                case 1: return "Faible";
                case 2: return (type ? "Moyen" : "Moyenne");
                case 3: return (type ? "Fort" : "Forte");
                default: return (type ? "Divin" : "Divine");
            }
        }

        private static string BonusScale(int value, double skill, bool type)
        {
            value = Variation(value, skill, 10, 2, 80);
            switch (value)
            {
                case 0: return (type ? "Nul" : "Nulle");
                case 1: return "Tr�s Faible";
                case 2: return "Faible";
                case 3: return (type ? "Moyen" : "Moyenne");
                case 4: return (type ? "Fort" : "Forte");
                case 5: return (type ? "Tr�s Fort" : "Tr�s Forte");
                case 6: return (type ? "Exceptionnel" : "Exceptionnelle");
                default: return (type ? "Divin" : "Divine");
            }
        }

        private static string StatScale(int value, double skill, bool type)
        {
            value = Variation(value, skill, 2, 1, 80);
            switch (value)
            {
                case 0: return (type ? "Nul" : "Nulle");
                case 1: return "Faible";
                case 2: return (type ? "Moyen" : "Moyenne");
                case 3: return (type ? "Fort" : "Forte");
                case 4: return (type ? "Tr�s Fort" : "Tr�s Forte");
                case 5: return (type ? "Exceptionnel" : "Exceptionnelle");
                default: return (type ? "Divin" : "Divine");
            }
        }

        private static string AuraScale(int value, double skill)
        {
            value = Variation(value, skill, 10, 2, 80);
            switch (value)
            {
                case 0: return "Nulle";
                case 1: return "Imperceptible";
                case 2: return "N�gligeable";
                case 3: return "Consid�rable";
                case 4: return "Pr�sente";
                case 5: return "Palpable";
                default: return "Oppressante";
            }
        }
    }
}