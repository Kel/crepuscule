using System;
using Server;
using Server.Mobiles;
using Server.Targeting;
using Server.Items;
using Server.Network;

using Server.Engines.Helpers;

namespace Server.SkillHandlers
{
    public class Stealing
    {
        // -------------------------------------------------------------------------
        // Param�tres - Vol d'objet �quip�
        // -------------------------------------------------------------------------
        // layerSteal :
        // @Description
        //   Activation du vol d'objet �quip�
        // @Valeur
        // - true = actif
        // - false = inactif
        private static bool layerSteal = true;
        // -------------------------------------------------------------------------

        public static void Initialize()
        {
            SkillInfo.Table[33].Callback = new SkillUseCallback(OnUse);
        }

        public static readonly bool ClassicMode = false;

        public static readonly bool SuspendOnMurder = false;

        public static bool IsInGuild(Mobile m)
        {
            return (m is PlayerMobile && ((PlayerMobile)m).NpcGuild == NpcGuild.ThievesGuild);
        }

        public static bool IsInnocentTo(Mobile from, Mobile to)
        {
            return (Notoriety.Compute(from, (Mobile)to) == Notoriety.Innocent);
        }

        private class StealingTarget : Target
        {
            private Mobile m_Thief;

            public StealingTarget(Mobile thief)
                : base(1, false, TargetFlags.None)
            {
                m_Thief = thief;
                AllowNonlocal = true;
            }

            private Item TryStealItem(Item toSteal, ref bool caught)
            {
                Item stolen = null;
                object root = toSteal.RootParent;

                if (!IsEmptyHanded(m_Thief))
                {
                    m_Thief.SendLocalizedMessage(1005584); // Both hands must be free to steal.
                }
                else if (SuspendOnMurder && root is Mobile && ((Mobile)root).Player && IsInGuild(m_Thief) && m_Thief.Kills > 0)
                {
                    m_Thief.SendLocalizedMessage(502706); // You are currently suspended from the thieves guild.
                }
                else if (root is BaseVendor && ((BaseVendor)root).IsInvulnerable)
                {
                    m_Thief.SendLocalizedMessage(1005598); // You can't steal from shopkeepers.
                }
                else if (root is PlayerVendor)
                {
                    m_Thief.SendLocalizedMessage(502709); // You can't steal from vendors.
                }
                else if (!m_Thief.CanSee(toSteal))
                {
                    m_Thief.SendLocalizedMessage(500237); // Target can not be seen.
                }
                else if (toSteal.Parent == null || !toSteal.Movable || toSteal.LootType == LootType.Newbied || toSteal.CheckBlessed(root))
                {
                    m_Thief.SendLocalizedMessage(502710); // You can't steal that!
                }
                else if (!m_Thief.InRange(toSteal.GetWorldLocation(), 1))
                {
                    m_Thief.SendLocalizedMessage(502703); // You must be standing next to an item to steal it.
                }
                // ---------------------------------------------------------------------
                // Syst�me de vol d'objet �quip�
                // ---------------------------------------------------------------------
                // Syst�me inactif
                else if (toSteal.Parent is Mobile && !layerSteal)
                {
                    m_Thief.SendMessage("Vous ne pouvez aps voler les objets qui sont �quip�s");
                }
                // La cible est un PNJ
                else if (toSteal.Parent is Mobile &&
                          !(toSteal.Parent is PlayerMobile))
                {
                    m_Thief.SendMessage("Cette personne a l'air bien trop vigilante.");
                }
                // L'objet � voler ne r�pond pas aux crit�res :
                // - �tre un bijoux ou un v�tement
                // - �tre port� sur un des layers suivants : Neck, Helm, Earrings,
                //   Cloak, Waist, Ring, Bracelet
                else if (toSteal.Parent is PlayerMobile && (
                          !(toSteal is BaseClothing) ||
                          !(toSteal is BaseJewel) ||
                          toSteal.Layer != Layer.Neck ||
                          toSteal.Layer != Layer.Helm ||
                          toSteal.Layer != Layer.Earrings ||
                          toSteal.Layer != Layer.Cloak ||
                          toSteal.Layer != Layer.Waist ||
                          toSteal.Layer != Layer.Ring ||
                          toSteal.Layer != Layer.Bracelet))
                {
                    m_Thief.SendMessage("C'est trop dangereux de voler cet objet sur cette personne !");
                }
                // ---------------------------------------------------------------------
                else if (root == m_Thief)
                {
                    m_Thief.SendLocalizedMessage(502704); // You catch yourself red-handed.
                }
                else if (root is Mobile && ((Mobile)root).AccessLevel > AccessLevel.Player)
                {
                    m_Thief.SendLocalizedMessage(502710); // You can't steal that!
                }
                else if (root is Mobile && !m_Thief.CanBeHarmful((Mobile)root))
                {
                }
                else
                {
                    // -------------------------------------------------------------------
                    // Syst�me de vol d'objet
                    // -------------------------------------------------------------------
                    caught = !RogueHelper.NinjaLoot(m_Thief, toSteal);
                    // -----------------------------------------------------------------               
                    double w = toSteal.Weight + toSteal.TotalWeight;

                    if (w > 10)
                    {
                        m_Thief.SendMessage("That is too heavy to steal.");
                    }
                    else
                    {
                        if (toSteal.Stackable && toSteal.Amount > 1)
                        {
                            int maxAmount = (int)((m_Thief.Skills[SkillName.Stealing].Value / 10.0) / toSteal.Weight);

                            if (maxAmount < 1)
                                maxAmount = 1;
                            else if (maxAmount > toSteal.Amount)
                                maxAmount = toSteal.Amount;

                            int amount = Utility.RandomMinMax(1, maxAmount);

                            if (amount >= toSteal.Amount)
                            {
                                int pileWeight = (int)Math.Ceiling(toSteal.Weight * toSteal.Amount);
                                pileWeight *= 10;

                                if (m_Thief.CheckTargetSkill(SkillName.Stealing, toSteal, pileWeight - 22.5, pileWeight + 27.5))
                                    stolen = toSteal;
                            }
                            else
                            {
                                int pileWeight = (int)Math.Ceiling(toSteal.Weight * amount);
                                pileWeight *= 10;

                                if (m_Thief.CheckTargetSkill(SkillName.Stealing, toSteal, pileWeight - 22.5, pileWeight + 27.5))
                                {
                                    stolen = Mobile.LiftItemDupe(toSteal, toSteal.Amount - amount);
                                    // Bug fix, this part has been moved in core 2.0
                                    // toSteal.Amount -= amount;
                                }
                            }
                        }
                        else
                        {
                            int iw = (int)Math.Ceiling(w);
                            iw *= 10;

                            if (m_Thief.CheckTargetSkill(SkillName.Stealing, toSteal, iw - 22.5, iw + 27.5))
                                stolen = toSteal;
                        }

                        if (stolen != null)
                            m_Thief.SendLocalizedMessage(502724); // You succesfully steal the item.
                        else
                            m_Thief.SendLocalizedMessage(502723); // You fail to steal the item.
                    }
                }
                return stolen;
            }

            protected override void OnTarget(Mobile from, object target)
            {
                Item stolen = null;
                object root = null;
                bool caught = false;

                if (target is Item)
                {
                    root = ((Item)target).RootParent;
                    stolen = TryStealItem((Item)target, ref caught);
                }
                else if (target is Mobile)
                {
                    Container pack = ((Mobile)target).Backpack;

                    if (pack != null && pack.Items.Count > 0)
                    {
                        int randomIndex = Utility.Random(pack.Items.Count);
                        root = target;
                        stolen = TryStealItem((Item)pack.Items[randomIndex], ref caught);
                    }
                }
                else
                {
                    m_Thief.SendLocalizedMessage(502710); // You can't steal that!
                }

                if (stolen != null)
                    from.AddToBackpack(stolen);
                // ---------------------------------------------------------------------
                // Gestion du vol cach�
                // ---------------------------------------------------------------------
                if (caught || !RogueHelper.StayHidden(from))
                {
                    from.RevealingAction();
                }
                // ---------------------------------------------------------------------
                if (caught)
                {
                    if (root == null)
                    {
                        m_Thief.CriminalAction(false);
                    }
                    else if (root is Corpse && ((Corpse)root).IsCriminalAction(m_Thief))
                    {
                        m_Thief.CriminalAction(false);
                    }
                    else if (root is Mobile)
                    {
                        Mobile mobRoot = (Mobile)root;

                        if (!IsInGuild(mobRoot) && IsInnocentTo(m_Thief, mobRoot))
                            m_Thief.CriminalAction(false);
                        // ---------------------------------------------------------------------
                        // Correction du message aux t�moins
                        // -----------------------------------------------------------------
                        // V�rification que le t�moin voit le voleur
                        string message = String.Format("Vous remarquez {0} en train d'essayer de voler {1}.", m_Thief.Name, mobRoot.Name);
                        foreach (NetState ns in m_Thief.GetClientsInRange(8))
                        {
                            if (ns != m_Thief.NetState && ns.Mobile.CanSee(m_Thief))
                                ns.Mobile.SendMessage(message);
                        }
                    }
                }
                // -----------------------------------------------------------------
                else if (root is Corpse && ((Corpse)root).IsCriminalAction(m_Thief))
                {
                    m_Thief.CriminalAction(false);
                }

                if (root is Mobile && ((Mobile)root).Player && m_Thief is PlayerMobile && IsInnocentTo(m_Thief, (Mobile)root) && !IsInGuild((Mobile)root))
                {
                    PlayerMobile pm = (PlayerMobile)m_Thief;
                    pm.PermaFlags.Add((Mobile)root);
                    pm.Delta(MobileDelta.Noto);
                }
            }
        }

        public static bool IsEmptyHanded(Mobile from)
        {
            if (from.FindItemOnLayer(Layer.OneHanded) != null)
                return false;
            if (from.FindItemOnLayer(Layer.TwoHanded) != null)
                return false; return true;
        }

        public static TimeSpan OnUse(Mobile m)
        {
            if (!IsEmptyHanded(m))
                m.SendLocalizedMessage(1005584); // Both hands must be free to steal.
            else
            {
                m.Target = new Stealing.StealingTarget(m);
                m.SendLocalizedMessage(502698); // Which item do you want to steal?
            }
            return TimeSpan.FromSeconds(10.0);
        }
    }
}