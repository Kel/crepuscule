//using System;
//using System.Text;
//using Server.Items;
//using Server.Mobiles;
//using Server.Targeting;
//using Server.Gumps;
//using Server.Network;

//namespace Server.Custom.Capacites
//{
//    public class CryptographicDocuments : CrepusculeItem
//    {

//        #region Constructors
//        [Constructable]
//        public CryptographicDocuments()
//            : base(0x0FF4)
//        {
//            Name = "Documents cryptographiques";
//            Weight = 1.0;
//        }

//        public CryptographicDocuments(Serial serial)
//            : base(serial)
//        {
//        }
//        #endregion

//        #region Serialization

//        public override void Serialize(GenericWriter writer)
//        {
//            base.Serialize(writer);
//            writer.Write((int)0); // version
//        }

//        public override void Deserialize(GenericReader reader)
//        {
//            base.Deserialize(reader);
//            int version = reader.ReadInt();
//        }

//        #endregion

//        public override void OnDoubleClick(Mobile from)
//        {
//            base.OnDoubleClick(from);
//            from.Target = new SelectBookTarget();
//        }

//        #region Target and Gump
//        ///////////////////////////
//        //Target SelectBookTarget//
//        ///////////////////////////  
//        class SelectBookTarget : Target
//        {

//            public SelectBookTarget()
//                : base(3, false, TargetFlags.None)
//            {
//            }

//            protected override void OnTarget(Mobile from, object targ)
//            {
//                if (targ is BaseBook)
//                {
//                    RacePlayerMobile rpm;
//                    rpm = (RacePlayerMobile)from;
//                    if (rpm.Capacities[CapacityName.Lore].Value >= 5)
//                    {
//                        BaseBook target = (BaseBook)targ;
//                        from.CloseGump(typeof(DifficulteCodageChoixGump));
//                        from.SendGump(new DifficulteCodageChoixGump(target, rpm));
//                    }
//                    else
//                    {
//                        rpm.SendMessage("Votre �rudition n'est pas assez �lev�e pour pouvoir coder des livres");
//                    }
//                }
//            }
//        }

//        ////////////////////////////////
//        //Gump de choix de difficult� //
//        //////////////////////////////// 
//        class DifficulteCodageChoixGump : Gump
//        {
//            RacePlayerMobile m_pour;
//            BaseBook Livre;
//            //protected TresorerieBook m_Book;
//            public DifficulteCodageChoixGump(BaseBook b, RacePlayerMobile pour)
//                : base(0, 0)
//            {
//                // m_Book = Livre;
//                Livre = b;
//                m_pour = pour;
//                this.Closable = false;
//                this.Disposable = true;
//                this.Dragable = true;
//                this.Resizable = false;
//                this.AddPage(0);
//                this.AddBackground(74, 74, 254, 102, 9300);
//                this.AddBackground(73, 47, 257, 31, 9200);
//                this.AddLabel(145, 54, 0, @"Difficult� de codage");
//                this.AddTextEntry(99, 102, 203, 20, 0, (int)Buttons.AgeTxt, @"" + m_pour.Capacities[CapacityName.Lore].Value.ToString());
//                this.AddButton(167, 140, 247, 248, (int)Buttons.Button2, GumpButtonType.Reply, 0);

//            }

//            public enum Buttons
//            {
//                AgeTxt,
//                Button2,
//            }

//            public override void OnResponse(NetState state, RelayInfo info)
//            {
//                RacePlayerMobile from = state.Mobile as RacePlayerMobile;
//                RacePlayerMobile mobile = m_pour;

//                int button = info.ButtonID;


//                switch (button)
//                {
//                    case 0:

//                        break;

//                    case 1:
//                        TextRelay relay = info.GetTextEntry(0);
//                        int newText = -1;

//                        try
//                        {
//                            newText = Utility.ToInt32(relay.Text);
//                        }
//                        catch
//                        {
//                            from.SendMessage("Votre choix est incorrect.");
//                            from.CloseGump(typeof(DifficulteCodageChoixGump));
//                            from.SendGump(new DifficulteCodageChoixGump(Livre, mobile));
//                        }

//                        string age_introduit = (relay == null ? null : relay.Text.Trim());

//                        if (age_introduit.Length > 5 || age_introduit.Length == 0)
//                        {
//                            from.SendMessage("Votre choix est incorrect.");
//                            from.CloseGump(typeof(DifficulteCodageChoixGump));
//                            from.SendGump(new DifficulteCodageChoixGump(Livre, mobile));
//                        }
//                        else
//                        {
//                            if (newText > 0 && newText <= 30000)
//                            {
//                                if (Livre.CanRead(from))
//                                {
//                                    if (newText <= (int)from.Capacities[CapacityName.Lore].Value)
//                                    {
//                                        Livre.Besoin_Erudition = (short)newText;
//                                        from.SendMessage("Le livre est cod�.");
//                                        from.CloseGump(typeof(DifficulteCodageChoixGump));
//                                    }
//                                    else
//                                    {
//                                        from.SendMessage("Votre niveau d'�rudition n'est pas assez �lev�.");
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                from.SendMessage("Votre choix est incorrect.");
//                                from.CloseGump(typeof(DifficulteCodageChoixGump));
//                                from.SendGump(new DifficulteCodageChoixGump(Livre, mobile));
//                            }
//                        }

//                        break;
//                }
//            }
//        }

//        #endregion
//    }

//}
