﻿using System;
using System.Text;
using Server.Guilds;
using Server.Mobiles;
using Server.Engines;

namespace Server
{
    public class FormatHelper
    {
        static public string GetFormatedGoldAmount(double Amount)
		{
			try
			{
				if (Amount < 1000)
			                return String.Format("{0:N0} po", Amount);
			
			            double KSize = Amount / 1000;
				if (KSize < 1000)
					return String.Format("{0:N0}K",KSize);
				double MSize = KSize / 1000;
			        if(MSize < 1000)
					return String.Format("{0:N0}M",MSize);
			         double GSize = MSize / 1000;
			         return String.Format("{0:N0}G", GSize);
			}catch{ return "?";}
		}
        
        static public string GetFormatedSize(double Size)
		{
			if (Size < 1024)
				return String.Format("{0:N} octets",Size);

			double KoSize = Size / 1024;
			if (KoSize < 1024)
				return String.Format("{0:N2} Ko",KoSize);
			double MoSize = KoSize / 1024;
            if(MoSize < 1024)
			    return String.Format("{0:N2} Mo",MoSize);
            double GoSize = MoSize / 1024;
            return String.Format("{0:N2} Go", GoSize);
		}

        static public string GetFormatedSpeed(double Size)
		{
			if (Size < 1024)
				return String.Format("{0:N} Octets/sec",Size);
			double KoSize = (double)Size / 1024;
			if (KoSize < 1024)
				return String.Format("{0:N2} Ko/sec",KoSize);
			double MoSize = (double)KoSize / 1024;
			return String.Format("{0:N2} Mo/sec",MoSize);
		}

        static public string GetFormatedTimeElapsed(double _TotalNumSeconds)
		{
			UInt64 TotalNumSeconds = (UInt64)Math.Ceiling((double)_TotalNumSeconds);
			if (TotalNumSeconds < 60) return String.Format("00:00:{0:00}",TotalNumSeconds);
			UInt16 NumMinutes	= (UInt16)(TotalNumSeconds/60);
			UInt16 NumSeconds	= (UInt16)((TotalNumSeconds) - ((UInt64)NumMinutes * 60));
			if (NumMinutes < 60) return String.Format("00:{0:00}:{1:00}",NumMinutes,NumSeconds);
			UInt16 NumHours		= (UInt16)(NumMinutes / 60);
			NumMinutes			= (UInt16)(NumMinutes - (NumHours * 60));
			return String.Format("{0:00}:{1:00}:{2:00}",NumHours,NumMinutes,NumSeconds);
		
		}

        static public string GetGuildDescription(GuildType _Guild)
        {
            string Description = "";
            switch (_Guild)
            {
                case GuildType.Commercants:
                    Description = "Ass. des commerçants";
                    break;
                case GuildType.Confrerie:
                    Description = "Confrerie Pourpre";
                    break;
                case GuildType.Gitans:
                    Description = "Union des Gitans";
                    break;
                case GuildType.Cercle:
                    Description = "Cercle de Pierre";
                    break;
                case GuildType.Mercenaires:
                    Description = "Armée des Mercenaires";
                    break;
                case GuildType.None:
                    Description = "Indépendant";
                    break;
                case GuildType.Ombres:
                    Description = "Guilde des Ombres";
                    break;
                case GuildType.Soleil:
                    Description = "Ordre du Soleil";
                    break;
                case GuildType.Chene:
                    Description = "Fraternité du Chêne";
                    break;
                case GuildType.Empire:
                    Description = "Empire";
                    break;
                case GuildType.Planaires:
                    Description = "Société Planaire";
                    break;
            }
            return Description;
        }

        static public string GetGuildDomainDescription(GuildDomain domain)
        {
            string Description = "";
            switch (domain)
            {
                case GuildDomain.None:
                    Description = "(Non initialisé)";
                    break;
                case GuildDomain.Defense:
                    Description = "Protéction de l'Empire";
                    break;
                case GuildDomain.Knowledge:
                    Description = "Contrôle du Savoir";
                    break;
                case GuildDomain.Magic:
                    Description = "Contrôle de la Magie";
                    break;
                case GuildDomain.Religion:
                    Description = "Promotion de la Religion";
                    break;
                case GuildDomain.Wealth:
                    Description = "Contrôle de la Richesse";
                    break;

            }
            return Description;
        }

        static public string GetGuildInfluenceDescription(GuildType guild, GuildDomain domain)
        {
            double percentage = GuildsSystem.GetPercentage(guild, domain);

            if (percentage >= 50.0) return "Dominant";
            else if (percentage >= 25.0) return "Important";
            else if (percentage >= 10.0) return "Considérable";
            else if (percentage >= 5.0) return "Négligeable";
            else return "Nul";
        }

        static public string GetGuildGlobalInfluenceDescription(GuildType guild)
        {
            double percentage = GuildsSystem.GetGlobalPercentage(guild);

            if (percentage >= 50.0) return "Dominant";
            else if (percentage >= 25.0) return "Important";
            else if (percentage >= 10.0) return "Considérable";
            else if (percentage >= 5.0) return "Négligeable";
            else return "Nul";
        }

        public static string GetAgeDescription(RaceType Race, bool Female, int Age)
        {
            string result = "";
            if (Race == RaceType.Humain || Race == RaceType.Petite_Personne || Race == RaceType.Demi_Orque)
            {
                if (Female == false)
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescent";
                    else if (Age >= 19 && Age < 26)
                        result = "Jeune Homme";
                    else if (Age >= 26 && Age < 51)
                        result = "Adulte";
                    else if (Age >= 51 && Age < 71)
                        result = "Agé";
                    else if (Age >= 71)
                        result = "Vieux";
                }
                else
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescente";
                    else if (Age >= 19 && Age < 26)
                        result = "Jeune Femme";
                    else if (Age >= 26 && Age < 51)
                        result = "Adulte";
                    else if (Age >= 51 && Age < 71)
                        result = "Agée";
                    else if (Age >= 71)
                        result = "Vieille";
                }
            }
            else if (Race == RaceType.Elfe || Race == RaceType.Elfe_Noir)
            {
                if (Female == false)
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescent";
                    else if (Age >= 19 && Age < 121)
                        result = "Jeune Elfe";
                    else if (Age >= 121 && Age < 301)
                        result = "Adulte";
                    else if (Age >= 301)
                        result = "Ancien";
                }
                else
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescente";
                    else if (Age >= 19 && Age < 121)
                        result = "Jeune Elfe";
                    else if (Age >= 121 && Age < 301)
                        result = "Adulte";
                    else if (Age >= 301)
                        result = "Ancienne";
                }
            }
            else if (Race == RaceType.Gnome)
            {
                if (Female == false)
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescent";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Gnome";
                    else if (Age >= 51 && Age < 81)
                        result = "Adulte";
                    else if (Age >= 81 && Age < 121)
                        result = "Agé";
                    else if (Age >= 121)
                        result = "Vieux";
                }
                else
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescente";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Gnome";
                    else if (Age >= 51 && Age < 81)
                        result = "Adulte";
                    else if (Age >= 81 && Age < 121)
                        result = "Agée";
                    else if (Age >= 121)
                        result = "Vieille";
                }
            }
            else if (Race == RaceType.Nain)
            {
                if (Female == false)
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescent";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Nain";
                    else if (Age >= 51 && Age < 81)
                        result = "Adulte";
                    else if (Age >= 81 && Age < 121)
                        result = "Agé";
                    else if (Age >= 121)
                        result = "Vieux";
                }
                else
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescente";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Naine";
                    else if (Age >= 51 && Age < 81)
                        result = "Adulte";
                    else if (Age >= 81 && Age < 121)
                        result = "Agée";
                    else if (Age >= 121)
                        result = "Vieille";
                }
            }
            else if (Race == RaceType.Demi_Elfe)
            {
                if (Female == false)
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescent";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Demi-Elfe";
                    else if (Age >= 51 && Age < 151)
                        result = "Adulte";
                    else if (Age >= 151 && Age < 201)
                        result = "Agé";
                    else if (Age >= 201)
                        result = "Vieux";
                }
                else
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescente";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Demi-Elfe";
                    else if (Age >= 51 && Age < 151)
                        result = "Adulte";
                    else if (Age >= 151 && Age < 201)
                        result = "Agée";
                    else if (Age >= 201)
                        result = "Vieille";
                }
            }
            else if (Race == RaceType.Tiefling)
            {
                if (Female == false)
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescent";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Homme";
                    else if (Age >= 51 && Age < 151)
                        result = "Adulte";
                    else if (Age >= 151 && Age < 201)
                        result = "Agé";
                    else if (Age >= 201)
                        result = "Vieux";
                }
                else
                {
                    if (Age > 0 && Age < 4)
                        result = "Bébé";
                    else if (Age >= 4 && Age < 13)
                        result = "Enfant";
                    else if (Age >= 13 && Age < 19)
                        result = "Adolescente";
                    else if (Age >= 19 && Age < 51)
                        result = "Jeune Femme";
                    else if (Age >= 51 && Age < 151)
                        result = "Adulte";
                    else if (Age >= 151 && Age < 201)
                        result = "Agée";
                    else if (Age >= 201)
                        result = "Vieille";
                }
            }
            return result;
        }
	}

    
}
