﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ultima;
using Server.Misc;

namespace Server
{
    public static class ClilocHelper
    {
        private static StringList Cliloc { get; set; }
        public static Dictionary<int, string> ClilocTable { get; private set; }

        public static void Initialize()
        {
            Ultima.Client.Directories.Add(DataPath.CustomPath);
            Cliloc = new StringList("FRA");
            ClilocTable = new Dictionary<int, string>();
            foreach (var item in Cliloc.Entries)
            {
                if(!ClilocTable.ContainsKey(item.Number))
                    ClilocTable.Add(item.Number, item.Text);
            }
        }


        public static string GetText(int number, string manualText)
        {
            if (ClilocTable.ContainsKey(number))
            {
                var text = ClilocTable[number];
                if (String.IsNullOrEmpty(text))
                    return manualText;
                return text;
            }
            else
            {
                return manualText;
            }
        }

    }
}
