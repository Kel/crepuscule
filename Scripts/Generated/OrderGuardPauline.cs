// Script, Originally generated by CrepManager
// Generation date: 14/08/2010 05:17:09
// Generated by: esclavedubuild
//               Animateur Shinigami

using System;
using Server.Items;
using Server.Guilds;
using Server.Misc;
using Server.Engines;
using Server.Mobiles;
using System.Collections;
using Server;
using Server.Accounting;
using Server.ContextMenus;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Server.Mobiles
{
    	[InheritedFrom(typeof(Server.Mobiles.OrderGuard))]
    	public class OrderGuardPauline : Server.Mobiles.OrderGuard
    	{

        [Constructable]
		public OrderGuardPauline() : base()
		{
            // Add custom initialization if needed
		}

		public  OrderGuardPauline( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
