// Script, Originally generated by CrepManager
// Generation date: 08/08/2010 22:42:06
// Generated by: esclavedubuild
//               Animateur Shinigami

using System.Collections.Generic;
using System;
using Server.Engines;
using Server;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.StonePaversLight))]
    	public class StonePaversLightt : Server.Items.StonePaversLight
    	{

        [Constructable]
		public StonePaversLightt() : base()
		{
            // Add custom initialization if needed
		}

		public  StonePaversLightt( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
