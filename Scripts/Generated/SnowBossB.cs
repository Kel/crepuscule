// Script, Originally generated by CrepManager
// Generation date: 13/02/2011 02:35:37
// Generated by: CarotteGm
//               Conteur Conan

using System;
using Server.Misc;
using Server.Engines;
using Server.Mobiles;
using System.Collections;
using Server;
using Server.Items;
using Server.Accounting;
using Server.ContextMenus;
using Server.Guilds;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Server.Mobiles
{
    	[InheritedFrom(typeof(Server.Mobiles.EvilMageLord))]
    	public class SnowBossB : Server.Mobiles.EvilMageLord
    	{

        [Constructable]
		public SnowBossB() : base()
		{
            // Add custom initialization if needed
		}

		public  SnowBossB( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
