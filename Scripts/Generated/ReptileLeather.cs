// Script, Originally generated by CrepManager
// Generation date: 26/05/2010 05:37:53
// Generated by: Apoc
//               Apoc

using System;
using System.Collections.Generic;
using Server.Engines;
using Server;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.Leather))]
    	public class ReptileLeather : Server.Items.Leather
    	{

        [Constructable]
		public ReptileLeather() : base()
		{
            // Add custom initialization if needed
		}

		public  ReptileLeather( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
