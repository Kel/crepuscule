// Script, Originally generated by CrepManager
// Generation date: 29/10/2010 14:28:52
// Generated by: CarotteGm
//               Conteur Conan

using System;
using System.Collections.Generic;
using Server.Engines;
using Server;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.BustEast))]
    	public class RawWoodenLightCube : Server.Items.BustEast
    	{

        [Constructable]
		public RawWoodenLightCube() : base()
		{
            // Add custom initialization if needed
		}

		public  RawWoodenLightCube( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
