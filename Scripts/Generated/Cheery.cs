// Script, Originally generated by CrepManager
// Generation date: 11/08/2010 01:32:37
// Generated by: esclavedubuild
//               Animateur Shinigami

using Server;
using System;
using System.Collections.Generic;
using Server.Engines;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.Dates))]
    	public class Cheery : Server.Items.Dates
    	{

        [Constructable]
		public Cheery() : base()
		{
            // Add custom initialization if needed
		}

		public  Cheery( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
