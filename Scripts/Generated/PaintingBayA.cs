// Script, Originally generated by CrepManager
// Generation date: 7/11/2010 18:29:12
// Generated by: shaardol
//               Shaardol

using System.Collections.Generic;
using System;
using Server.Engines;
using Server;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.Bateau))]
    	public class PaintingBayA : Server.Items.Bateau
    	{

        [Constructable]
		public PaintingBayA() : base()
		{
            // Add custom initialization if needed
		}

		public  PaintingBayA( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
