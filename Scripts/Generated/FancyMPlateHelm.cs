// Script, Originally generated by CrepManager
// Generation date: 27/02/2011 01:11:51
// Generated by: esclavedubuild
//               Conteur Shinigami

using Server;
using Server.Items;
using System;
using System.Collections.Generic;
using Server.Engines;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.Bandana))]
    	public class FancyMPlateHelm : Server.Items.Bandana
    	{

        [Constructable]
		public FancyMPlateHelm() : base()
		{
            // Add custom initialization if needed
		}

		public  FancyMPlateHelm( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
