// Script, Originally generated by CrepManager
// Generation date: 7/12/2010 10:20:09
// Generated by: luxale99
//               phooka

using Server;
using System;
using Server.Mobiles;
using Server.Misc;
using Server.Engines;
using System.Collections;
using Server.Items;
using Server.Accounting;
using Server.ContextMenus;
using Server.Guilds;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Server.Mobiles
{
    	[InheritedFrom(typeof(Server.Mobiles.Snake))]
    	public class asticot : Server.Mobiles.Snake
    	{

        [Constructable]
		public asticot() : base()
		{
            // Add custom initialization if needed
		}

		public  asticot( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
