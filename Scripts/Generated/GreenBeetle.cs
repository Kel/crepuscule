// Script, Originally generated by CrepManager
// Generation date: 11/01/2011 22:41:57
// Generated by: esclavedubuild
//               Conteur Shinigami

using System;
using Server.Mobiles;
using Server;
using Server.Misc;
using Server.Engines;
using System.Collections;
using Server.Items;
using Server.Accounting;
using Server.ContextMenus;
using Server.Guilds;
using Server.Network;
using Server.Prompts;
using Server.Targeting;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Server.Mobiles
{
    	[InheritedFrom(typeof(Server.Mobiles.Beetle))]
    	public class GreenBeetle : Server.Mobiles.Beetle
    	{

        [Constructable]
		public GreenBeetle() : base()
		{
            // Add custom initialization if needed
		}

		public  GreenBeetle( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
