// Script, Originally generated by CrepManager
// Generation date: 08/08/2010 10:03:02
// Generated by: esclavedubuild
//               Animateur Shinigami

using Server;
using Server.Items;
using System;
using System.Collections.Generic;
using Server.Engines;
using System.Collections;
using Server.Network;
using System.Linq;
using System.IO;
using Server.Mobiles;

namespace Server.Items
{
    	[InheritedFrom(typeof(Server.Items.TeteOurs))]
    	public class TeteLoup : Server.Items.TeteOurs
    	{

        [Constructable]
		public TeteLoup() : base()
		{
            // Add custom initialization if needed
		}

		public  TeteLoup( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}
    	}
}
