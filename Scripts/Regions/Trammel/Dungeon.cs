using System;
using Server;
using Server.Mobiles;
using Server.Spells;
using Server.Spells.Seventh;
using Server.Spells.Fourth;
using Server.Spells.Sixth;

namespace Server.Regions
{
	public class TrammelDungeon : Region
	{
		public static void Initialize()
		{
			Region.AddRegion( new TrammelDungeon( "Covetous" ) );
			Region.AddRegion( new TrammelDungeon( "Deceit" ) );
			Region.AddRegion( new TrammelDungeon( "Despise" ) );
			Region.AddRegion( new TrammelDungeon( "Destard" ) );
			Region.AddRegion( new TrammelDungeon( "Hythloth" ) );
			Region.AddRegion( new TrammelDungeon( "Shame" ) );
			Region.AddRegion( new TrammelDungeon( "Wrong" ) );
			Region.AddRegion( new TrammelDungeon( "Terathan Keep" ) );
			Region.AddRegion( new TrammelDungeon( "Fire" ) );
			Region.AddRegion( new TrammelDungeon( "Ice" ) );
			Region.AddRegion( new TrammelDungeon( "Orc Cave" ) );
		}

		public TrammelDungeon( string name ) : base( "the dungeon", name, Map.Trammel )
		{
		}

		public override bool AllowHousing( Mobile from, Point3D p )
		{
			return false;
		}

		public override void OnEnter( Mobile m )
		{
			//base.OnEnter( m ); // You have entered the dungeon {0}
		}

		public override void OnExit( Mobile m )
		{
			//base.OnExit( m );
		}

		/*public override bool OnBeginSpellCast( Mobile m, ISpell s )
		{
			if ( s is GateTravelSpell || s is RecallSpell || s is MarkSpell )
			{
				m.SendMessage( "You cannot cast that spell here." );
				return false;
			}
			else
			{
				return base.OnBeginSpellCast( m, s );
			}
		}*/

		public override void AlterLightLevel( Mobile m, ref int global, ref int personal )
		{
			global = LightCycle.DungeonLevel;
		}
	}
}