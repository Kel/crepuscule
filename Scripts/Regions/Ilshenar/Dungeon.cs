using System;
using Server;
using Server.Mobiles;
using Server.Spells;
using Server.Spells.Seventh;
using Server.Spells.Fourth;
using Server.Spells.Sixth;

namespace Server.Regions
{
	public class IlshenarDungeon : Region
	{
		public static void Initialize()
		{
			Region.AddRegion( new IlshenarDungeon( "Rock Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Spider Cave" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Spectre Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Blood Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Wisp Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Ankh Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Exodus Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Sorcerer's Dungeon" ) ); 
			Region.AddRegion( new IlshenarDungeon( "Ancient Lair" ) ); 
		}

		public IlshenarDungeon( string name ) : base( "the dungeon", name, Map.Ilshenar )
		{
		}

		public override bool AllowHousing( Mobile from, Point3D p )
		{
			return false;
		}

		public override void OnEnter( Mobile m )
		{
			//base.OnEnter( m ); // You have entered the dungeon {0}
		}

		public override void OnExit( Mobile m )
		{
			//base.OnExit( m );
		}

		/*public override bool OnBeginSpellCast( Mobile m, ISpell s )
		{
			if ( s is GateTravelSpell || s is RecallSpell || s is MarkSpell )
			{
				m.SendMessage( "You cannot cast that spell here." );
				return false;
			}
			else
			{
				return base.OnBeginSpellCast( m, s );
			}
		}*/

		public override void AlterLightLevel( Mobile m, ref int global, ref int personal )
		{
			global = LightCycle.DungeonLevel;
		}
	}
}