using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using Server;
using Server.Network;

namespace Server.Misc
{
	public class IPLimiter
	{
		public static bool Enabled = true;
		public static bool SocketBlock = true; // true to block at connection, false to block at login request

		public const int MaxAddresses = 10;

		public static bool Verify( NetState ns )
		{
			if ( !Enabled )
				return true;

			IPAddress ourAddress = ns.Address;
			int count = 0;
            foreach (NetState compState in NetState.Instances)
			{
				if ( ourAddress.Equals( compState.Address ) )
				{
					++count;

					if ( count > MaxAddresses )
						return false;
				}
			}

			return true;
		}
	}
}