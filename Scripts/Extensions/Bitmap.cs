﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Server
{
    public static class BitmapExtensions
    {
        public static byte[] GetBytes(this Bitmap source)
        {
            using(var ms = new MemoryStream())
            {
                source.Save( ms, ImageFormat.Png );
                return ms.ToArray();
            }
        }
    }
}
