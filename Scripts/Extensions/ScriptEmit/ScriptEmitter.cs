﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Text.RegularExpressions;
using Server.Mobiles;
using Server.Engines;
using Server.Gumps;

namespace Server
{
    public static class ScriptEmitter
    {
        #region Properties & Configure
        public static bool HasNewScripts { get { return EmittedScripts.Count > 0; } }
        private static List<string> EmittedScriptNames = new List<string>();
        private static List<string> EmittedScripts = new List<string>();
        public static string ScriptsPath = "Scripts/Generated/";
        public static string ScriptsOutPath = "Scripts/Output/";
        public static string TempPath = "Data/Temp/";
        public static string CompilerOptions = "/target:library /optimize";

        public static void Configure()
        {
            
        }
        #endregion

        public static void InheritFrom(Type type, string name, string title, Mobile from)
        {
            if (type != null && type.GetCustomAttributes(typeof(InheritedFromAttribute), false).Length > 0)
            {
                // Already inherited from something, taking the base here
                type = (type.GetCustomAttributes(typeof(InheritedFromAttribute), false)[0] as InheritedFromAttribute).m_Type;
            }


            if (String.IsNullOrEmpty(name))
                throw new Exception("Le nom ne peut pas être vide");

            var re = new Regex("^[-'a-zA-Z]*$");
            if (name.Contains(" ") || !re.IsMatch(name) )
                throw new Exception("Le nom du script doit être en anglais et ne pas contenir de caractères spéciaux ni d'espaces");

            if (ScriptCompiler.FindTypeByName(name, true) != null || EmittedScriptNames.Select(s => s.ToLower()).Contains(name.ToLower())) 
                throw new Exception("Ce nom existe déjà, choisissez un autre");

            if (ScriptCompiler.Assemblies
                .Select(a => a.GetType(type.FullName))
                .FirstOrDefault() == null)
                throw new Exception("Impossible d'hériter l'objet hérité");

            if (!ReflectionHelper.CanConstruct(type))
                throw new Exception("Impossible d'hériter ce type d'objets");

            // Create a new script
            var CurrentScript = "";
            using (var writer = new StringWriter())
            {
                MakeCredits(from, writer);

                writer.WriteLine(ReflectionHelper.MakeHeader(type));
                writer.WriteLine();

                writer.WriteLine(@"namespace {0}", type.Namespace);
                writer.WriteLine(@"{");

                writer.WriteLine(@"    	[InheritedFrom(typeof({0}))]", type.FullName);
                writer.WriteLine(@"    	public class {0} : {1}", name, type.FullName);
                writer.WriteLine(@"    	{");

                writer.WriteLine(MakeConstructor(type, name));
                writer.WriteLine(MakeSerialization());

                writer.WriteLine(@"    	}");
                writer.WriteLine(@"}");


                // Add to the list
                CurrentScript = writer.ToString();
            }

            // Compile the script now
            try
            {
                var assembly = Compile(CurrentScript);
                ScriptCompiler.AddAssembly(assembly);
            }
            catch (ReflectionTypeLoadException ex)
            {
                var exeptions = ex.LoaderExceptions
                    .Select(e => e.Message)
                    .Aggregate((a,b) => a+ Environment.NewLine+b);
                File.WriteAllText(TempPath + name + ".cs", CurrentScript
                    + Environment.NewLine + @"/*" + ex.Message
                    + Environment.NewLine + exeptions
                    + Environment.NewLine + ex.StackTrace + @"*/");
                throw new Exception("Une erreur s'est produite pendant la génération. L'objet n'est pas créé.");
            }
            catch (Exception ex)
            {
                // Log 
                File.WriteAllText(TempPath + name + ".cs", CurrentScript
                    + Environment.NewLine + @"/*" + ex.Message
                    + Environment.NewLine + ex.StackTrace + @"*/");
                throw new Exception("Une erreur s'est produite pendant la génération. L'objet n'est pas créé.");
            }

            from.SendMessage("Objet créé!");
            var newType = ScriptCompiler.FindTypeByName(name);
            from.CloseAllGumps();

            // Make template
            if (newType != null && newType.IsSubclassOf(typeof(Item)))
            {
                GameItemType.MakeTemplate(newType);
                if (GameItemType.FindType(newType) != null)
                {
                    var template = GameItemType.FindType(newType);
                    template.Name = title;
                    from.SendGump(new APropertiesGump(from, template));
                }
            }
            else if (newType != null && newType.IsSubclassOf(typeof(BaseCreature)))
            {
                CreatureType.MakeTemplate(newType);
                if (CreatureType.FindType(newType) != null)
                {
                    var template = CreatureType.FindType(newType);
                    template.Name = title;
                    from.SendGump(new APropertiesGump(from, template));
                }
            }

            // Write to scripts, so the reboot will take it as a normal script
            File.WriteAllText(ScriptsPath + name + ".cs", CurrentScript);

            // Add to emitted lists
            EmittedScripts.Add(CurrentScript);
            EmittedScriptNames.Add(name);
        }

        #region Compile
        public static Assembly Compile(string source)
        {
            return Compile(new string[] { source }.ToList());
        }

        public static Assembly Compile(List<string> sources)
        {
            // Nasty, I might consider replacing this for Guid.New() file creation later
            var BinaryFilename = Path.GetTempFileName() + ".dll";

            // Let's try to compile the script now
            using (var csp = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v3.5" } }))
            {
                #pragma warning disable 618
                var cc = csp.CreateCompiler();
                #pragma warning restore  618
                var parameters = new CompilerParameters()
                {
                    GenerateExecutable = false,
                    GenerateInMemory = false,
                    OutputAssembly = BinaryFilename,
                    CompilerOptions = CompilerOptions
                };
                var assemblies = ReflectionHelper.GetAllAssemblies();
                foreach (var assembly in assemblies)
                {
                    if(assembly.FullName.Contains("Scripts.CS"))
                    {
                        parameters.ReferencedAssemblies.Add(Path.GetFullPath(Path.Combine(Core.BaseDirectory, "Scripts.CS.dll")));
                    }
                    else
                    {
                        try{ parameters.ReferencedAssemblies.Add(assembly.Location);} catch { }
                    }
                }

                var results = cc.CompileAssemblyFromSourceBatch(parameters, sources.ToArray());
                if (results.Errors.HasErrors)
                {
                    var errors = String.Format("Compiler Errors:{0}", Environment.NewLine);
                    foreach (CompilerError error in results.Errors)
                    {
                        errors += String.Format("{0}{1}", error.ErrorText, Environment.NewLine);
                    }

                    throw new CodeCompilationException(errors);
                }
                else
                {
                     // results.CompiledAssembly;

                     // Good, let's load the assembly in now
                     return Assembly.LoadFile(BinaryFilename);
                    //AppDomain.CurrentDomain.Load(results.CompiledAssembly.);
                    //return results.CompiledAssembly;
                }
 
            }


        }
        #endregion

        #region MakeConstructor
        public static string MakeConstructor(Type type, string name)
        {
            return @"
        [Constructable]
		public " + name + @"() : base()
		{
            // Add custom initialization if needed
		}

		public  " + name + @"( Serial serial ) : base( serial )
		{
		}";
        }

        #endregion

        #region MakeSerialization
        public static string MakeSerialization()
        {
            return @"
		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );
			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );
			int version = reader.ReadInt();
		}";

        }
        #endregion

        #region MakeCredits
        private static void MakeCredits(Mobile from, StringWriter writer)
        {
            writer.WriteLine("// Script, Originally generated by CrepManager");
            writer.WriteLine("// Generation date: " + DateTime.Now.ToString());
            writer.WriteLine("// Generated by: " + from.Account.ToString());
            writer.WriteLine("//               " + from.Name.ToString());
            writer.WriteLine();
        }
        #endregion

        #region CodeCompilationException
        [Serializable]
        public class CodeCompilationException : Exception
        {
            public CodeCompilationException()
                : base()
            {
            }

            public CodeCompilationException(string message)
                : base(message)
            {
            }

            public CodeCompilationException(string message, Exception innerException)
                : base(message, innerException)
            {
            }

            protected CodeCompilationException(
                System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext context)
                : base(info, context)
            {
            }
        }
        #endregion
    }
}
