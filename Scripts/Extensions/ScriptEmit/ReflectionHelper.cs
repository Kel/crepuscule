﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Server
{
    public class ReflectionHelper
    {
        public static List<string> PredefinedDependencies = new List<string>
        {
            "System",
            "System.Linq",
            "System.Collections.Generic",
            "System.IO",
            "Server",
            "Server.Items",
            "Server.Mobiles"
        };
        

        public static object CreateGeneric(string name, params Type[] types)
        {
            var t = String.Format("{0}`{1}", name, types.Length);
            var generic = Type.GetType(t).MakeGenericType(types);
            return Activator.CreateInstance(generic);
        }

        public static Type CreateGenericTypeOnly(string name, params Type[] types)
        {
            var t = String.Format("{0}`{1}", name, types.Length);
            return Type.GetType(t).MakeGenericType(types);
        }

        public static ConstructorInfo GetConstructor(Type type)
        {
            return type.GetConstructors()
                .Where(c => c.GetCustomAttributes(typeof(ConstructableAttribute), true) != null)
                .Where(c => c.GetParameters().Count() == 0)
                .FirstOrDefault();
        }

        public static bool CanConstruct(Type type)
        {
            return GetConstructor(type) != null;
        }

        public static IEnumerable<string> GetDependancies(Type type)
        {
            var namespaces = type.GetProperties()
                     .Select(p => p.PropertyType.Namespace)
                     .Distinct()
                     /*.Union(
                          type.GetInterfaces()
                         .Select(p => p.Namespace)
                         .Distinct()
                         .Union(
                              type.GetMethods()
                             .SelectMany(p => p.Module.GetTypes())
                             .Select(p => p.Namespace)
                             .Distinct()
                         )
                     )*/
                     .Union(PredefinedDependencies)
                     .Where(n => !String.IsNullOrEmpty(n));

            return namespaces;
        }

        public static string MakeHeader(Type type)
        {
            return ReflectionHelper.GetDependancies(type)
                .Select(t => String.Format("using {0};", t))
                .FusionLines();
        }

        public static IEnumerable<Assembly> GetAllAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies();
            /*return ScriptCompiler.GetReferenceAssemblies()
                .Select(n => Assembly.LoadFile(n))
                .Union(ScriptCompiler.Assemblies);*/
        }
    }
}
