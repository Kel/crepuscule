﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    [PropertyObject]
    public class UOStringProperty
    {

        private string m_MessageString;
        [CommandProperty(AccessLevel.GameMaster)]
        public string Message
        {
            get { return m_MessageString; }
            set
            {
                m_MessageString = value;
                if (!String.IsNullOrEmpty(value))
                {
                    Number = 0;
                }
            }
        }

        private int m_Number;
        [CommandProperty(AccessLevel.GameMaster)]
        public int Number
        {
            get { return m_Number; }
            set
            {
                m_Number = value;
                if (value != 0)
                {
                    Message = null;
                }
            }
        }

        #region Constructors & Serialization
        public UOStringProperty() 
        {
            
        }
        public UOStringProperty(GenericReader reader)
        {
            m_MessageString = reader.ReadString();
            m_Number = reader.ReadInt();
        }
            

        public void Serialize(GenericWriter writer)
        {
            writer.Write(m_MessageString);
            writer.Write(m_Number);
        }

        public override string ToString()
        {
            if (!String.IsNullOrEmpty(m_MessageString))
                return m_MessageString;
            else if (m_Number > 0)
                return m_Number.ToString();
            else
                return "(non-définie)";
        }
        #endregion

    }
}
