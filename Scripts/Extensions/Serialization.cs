﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Server
{
    public static class SerializationExtensions
    {
        public static Type ReadType(this GenericReader source)
        {
            var typeName = source.ReadString();
            if (String.IsNullOrEmpty(typeName)) return null;
            return Type.GetType(typeName);
        }

        public static void Write(this GenericWriter source, Type type)
        {
            source.Write(type == null ? "(null)" : type.FullName);
        }

        public static UOStringProperty ReadUOString(this GenericReader source)
        {
            return new UOStringProperty(source);
        }

        public static void Write(this GenericWriter source, UOStringProperty property)
        {
            if (property == null)
                new UOStringProperty().Serialize(source);
            else
                property.Serialize(source);
        }

        public static SoundEffect ReadSound(this GenericReader source)
        {
            return new SoundEffect(source);
        }

        public static void Write(this GenericWriter source, SoundEffect property)
        {
            property.Serialize(source);
        }

        public static RangeValue ReadRange(this GenericReader source)
        {
            return new RangeValue(source);
        }

        public static void Write(this GenericWriter source, RangeValue property)
        {
            if (property == null)
                new RangeValue().Serialize(source);
            else
                property.Serialize(source);
        }


        public static DoubleRangeValue ReadDoubleRange(this GenericReader source)
        {
            return new DoubleRangeValue(source);
        }

        public static void Write(this GenericWriter source, DoubleRangeValue property)
        {
            if (property == null)
                new DoubleRangeValue().Serialize(source);
            else
                property.Serialize(source);
        }


        public static void WriteBitArray(this GenericWriter writer, BitArray ba)
        {
            writer.Write(ba.Length);

            for (int i = 0; i < ba.Length; i++)
            {
                writer.Write(ba[i]);
            }
            return;
        }

        public static BitArray ReadBitArray(this GenericReader reader)
        {
            int size = reader.ReadInt();
            BitArray newBA = new BitArray(size);

            for (int i = 0; i < size; i++)
            {
                newBA[i] = reader.ReadBool();
            }

            return newBA;
        }


        public static void WriteRect2DArray(this GenericWriter writer, ArrayList ary)
        {
            writer.Write(ary.Count);

            for (int i = 0; i < ary.Count; i++)
            {
                writer.Write((Rectangle2D)ary[i]); //Rect2D
            }
            return;
        }

        public static ArrayList ReadRect2DArray(this GenericReader reader)
        {
            int size = reader.ReadInt();
            ArrayList newAry = new ArrayList();

            for (int i = 0; i < size; i++)
            {
                newAry.Add(reader.ReadRect2D());
            }

            return newAry;
        }
    }
}
