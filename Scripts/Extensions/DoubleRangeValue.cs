﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    #region RangeValue Property Object
    [PropertyObject]
    public class DoubleRangeValue
    {
        private double m_Min = 0;
        [CommandProperty(AccessLevel.GameMaster)]
        public double Min
        {
            get { return m_Min; }
            set 
            {
                m_Min = value;
                if (Max < value)
                    Max = value;
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public double Max { get; set; }

        #region Constructors & Serialization
        public DoubleRangeValue() { }
        public DoubleRangeValue(double min, double max)
        {
            Min = min;
            Max = max;
        }
        public DoubleRangeValue(GenericReader reader)
        {

            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        Min = reader.ReadDouble();
                        Max = reader.ReadDouble();
                        break;
                    }
            }
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Min);
            writer.Write(Max);

        }

        public override string ToString()
        {
            return String.Format("{0} - {1}",
                Min,
                Max
                );
        }
        #endregion

    }
    #endregion
}
