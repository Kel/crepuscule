﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Globalization;

namespace Server
{
    public static class FrameworkExtensions
    {
        public static string ToTitleCase(this string source)
        {
            var TextInfo = CultureInfo.CurrentCulture.TextInfo;
            return TextInfo.ToTitleCase(source.ToLower());
        }

        public static string GetDescriptedName(this Type source)
        {
            var result = source.Name;
            var attrs = source.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs.Length > 0)
            {
                var descr = attrs[0] as DescriptionAttribute;
                if (descr != null)
                    result = descr.Description;
            }
            return result;
        }

        public static string GetDescriptedName(this PropertyInfo source)
        {
            var result = source.Name;
            var attrs = source.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs.Length > 0)
            {
                var descr = attrs[0] as DescriptionAttribute;
                if (descr != null)
                    result = descr.Description;
            }
            return result;
        }

        public static bool IsNullable(this Type source)
        {
            var attrs = source.GetCustomAttributes(typeof(NullableAttribute), false);
            if (attrs.Length > 0)
                return (attrs[0] as NullableAttribute) != null;
            return false;
        }

        public static bool IsNullable(this PropertyInfo source)
        {
            var attrs = source.GetCustomAttributes(typeof(NullableAttribute), false);
            if (attrs.Length > 0)
                return (attrs[0] as NullableAttribute) != null;
            return false;
        }

        public static string GetShortName(this Type source)
        {
            if (!String.IsNullOrEmpty(source.Name))
            {
                var split = source.Name.Split('.');
                if (split != null && split.Length > 0)
                    return split[split.Length - 1];
            }
            return source.Name;
        }


        public static string FusionLines(this IEnumerable<string> source)
        {
            return source.Aggregate((a, b) => a + Environment.NewLine + b);
        }
    }
}
