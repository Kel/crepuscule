﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Server
{
    #region RangeValue Property Object
    [PropertyObject]
    public class RangeValue
    {
        private int m_Min = 0;
        [CommandProperty(AccessLevel.GameMaster)]
        public int Min
        {
            get { return m_Min; }
            set 
            {
                m_Min = value;
                if (Max < value)
                    Max = value;
            }
        }
        [CommandProperty(AccessLevel.GameMaster)]
        public int Max { get; set; }

        #region Constructors & Serialization
        public RangeValue() { }
        public RangeValue(int min, int max)
        {
            Min = min;
            Max = max;
        }
        public RangeValue(GenericReader reader)
        {

            var version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        Min = reader.ReadInt();
                        Max = reader.ReadInt();
                        break;
                    }
            }
        }

        public int Random()
        {
            if (Min >= Max) return Min;
            return Utility.Random(Min, Max - Min);
        }


        public void Serialize(GenericWriter writer)
        {
            writer.Write((int)0); //version
            writer.Write(Min);
            writer.Write(Max);

        }

        public override string ToString()
        {
            return String.Format("{0} - {1}",
                Min,
                Max
                );
        }
        #endregion

    }
    #endregion
}
