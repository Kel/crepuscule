﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;

namespace Crepuscule.Update.Engine
{
    public static class Win32
    {
        private static UInt32 MOUSEEVENTF_LEFTDOWN = 0x0002;
        private static UInt32 MOUSEEVENTF_LEFTUP = 0x0004;

        [DllImport("user32.dll", EntryPoint="mouse_event")]
        private static extern void MouseEvent(
            UInt32 dwFlags, // motion and click options 
            UInt32 dx, // horizontal position or change 
            UInt32 dy, // vertical position or change 
            UInt32 dwData, // wheel movement 
            IntPtr dwExtraInfo // application-defined information 
        );

        public static void SendClick(Point location)
        {
            Point CurrentPosition = Cursor.Position;
            
            Cursor.Position = location;
            MouseEvent(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, new System.IntPtr());
            MouseEvent(MOUSEEVENTF_LEFTUP, 0, 0, 0, new System.IntPtr());
            Cursor.Position = CurrentPosition;
        } 
    }
}
