﻿using System;
using System.Collections.Generic;
using System.Text;
using Crepuscule.Update.Engine;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Crepuscule.Update
{
    public static class Updater
    {
        public static PatchInfo PatchInfo = null;
        public static string UltimaOnlinePath = @"c:\";
        public static string PatchInfoFilename = "";
        public static List<PatchFileInfo> UpdateList = new List<PatchFileInfo>();

        static MD5 md5 = new MD5CryptoServiceProvider();

        public static bool GetPatchInfo()
        {
            try
            {
                WebClient Client = new WebClient();
                string PatchInfoFile = Path.Combine(PatchInfoFilename, "Patch.xml");
                Client.DownloadFile("http://www.au-crepuscule.com/download/update/Patch.xml", PatchInfoFile);
                PatchInfo = PatchInfo.Load(PatchInfoFile);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Une erreur est survenue, l'écriture dans votre repertoire Ultima Online est impossible, vérifiez si vous avez les droit d'administrateur ou si vous avez les permissions d'écriture dans ce repertoire", "Erreur de mises à jour", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public static bool CheckUpdates()
        {
            try
            {
                UpdateList = new List<PatchFileInfo>();
                if (GetPatchInfo())
                {
                    foreach (PatchFileInfo file in PatchInfo.Files)
                    {
                        FileInfo hddFile = new FileInfo(Path.Combine(UltimaOnlinePath, file.Filename));
                        bool NeedUpdate = true;
                        if (hddFile.Exists)
                        {
                            byte[] Hash;
                            //Compute the hash
                            using (FileStream inStream = new FileStream(hddFile.FullName, FileMode.Open))
                            {
                                Hash = md5.ComputeHash(inStream);
                            }

                            //Check by hash
                            if (CheckHash(Hash, file.FileMD5))
                                NeedUpdate = false;
                        }
                        if (NeedUpdate) UpdateList.Add(file);
                    }

                    return UpdateList.Count > 0;
                }
                else { return false; }
            }
            catch (IOException)
            {
                MessageBox.Show("Une erreur est survenue, votre client Ultima Online doit être fermé afin de pouvoir effectuer les mises à jour.", "Erreur de mises à jour", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public static bool CheckHash(byte[] ba1, byte[] ba2)
        {
            if (ba1 == null)
                throw new ArgumentNullException("ba1");
            if (ba2 == null)
                throw new ArgumentNullException("ba2");

            if (ba1.Length != ba2.Length)
                return false;

            for (int i = 0; i < ba1.Length; i++)
            {
                if (ba1[i] != ba2[i])
                    return false;
            }
            return true;
        }



    }
}
