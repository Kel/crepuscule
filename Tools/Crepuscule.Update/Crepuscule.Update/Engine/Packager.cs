﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using SevenZip;
using System.Threading;

namespace Crepuscule.Update.Engine
{
    /// <summary>
    /// Creates a LZMA packages, use for staidx and statics
    /// </summary>
    public static class Packager
    {
        // contents of the package to create
        public static string[] PackageFilenames = new string[]{
            "Staidx0.mul","Statics0.mul","Staidx2.mul","Statics2.mul"
            };

        public static string HttpUrl = "http://www.au-crepuscule.com/download/update/";
        public static string FtpHost = "ftp.au-crepuscule.com";
        public static int    FtpPort = 21;
        public static string FtpUsername = "scribe@au-crepuscule.com";
        public static string FtpPassword = "GuAZJ6PQ7ZD1";

        public static string ServerFilesPath = @"";
        public static string PackagePrefix = "Crepuscule";
        public static string TempExtension = ".tmp";

        static MD5 md5 = new MD5CryptoServiceProvider();
        static int dictionary = 1 << 23;
        static bool eos = false;

        static CoderPropID[] SevenZipPropIDs = 
				{
					CoderPropID.DictionarySize,
					CoderPropID.PosStateBits,
					CoderPropID.LitContextBits,
					CoderPropID.LitPosBits,
					CoderPropID.Algorithm,
					CoderPropID.NumFastBytes,
					CoderPropID.MatchFinder,
					CoderPropID.EndMarker
				};

       // These are the default properties, keeping it simple for now
        static object[] SevenZipProperties = 
				{
					(Int32)(dictionary),
					(Int32)(2),
					(Int32)(3),
					(Int32)(0),
					(Int32)(2),
					(Int32)(128),
					"bt4",
					eos
				};



        public static PatchInfo CreatePackage()
        {
            DateTime now = DateTime.Now;
            List<FileInfo> files = new List<FileInfo>();
            string Version = String.Format("{0}_{1}_{2}",  now.Year, now.Month, now.Day);

            //Prepare FTP
            FTP ftp = new FTP();
            ftp.Connect(FtpHost, FtpPort, FtpUsername, FtpPassword);

            //Prepare PatchInfo file
            PatchInfo patch = new PatchInfo();
            patch.PatchVersion = Version;

            foreach (string FileToPackage in PackageFilenames)
            {
                string PackageName = String.Format("{0}_{1}_{2}_{3}.{4}.7zip", PackagePrefix,
                now.Year, now.Month, now.Day,FileToPackage);
                string PackageFilename = Path.Combine(Path.GetTempPath(), PackageName);
                string FileToPackageFullName = Path.Combine(ServerFilesPath, FileToPackage);
                byte[] Hash;

                using (FileStream outStream = new FileStream(PackageFilename, FileMode.Create))
                {
                    SevenZip.Compression.LZMA.Encoder encoder = new SevenZip.Compression.LZMA.Encoder();
                    //encoder.SetCoderProperties(SevenZipPropIDs, SevenZipProperties);
                    encoder.WriteCoderProperties(outStream);

                    //Open file to package & write a new one
                    using (FileStream inStream = new FileStream(FileToPackageFullName, FileMode.Open))
                    {
                        long fileSize = inStream.Length;
                        for (int i = 0; i < 8; i++)
                            outStream.WriteByte((Byte)(fileSize >> (8 * i)));
                        encoder.Code(inStream, outStream, -1, -1, null);
                    }

                    //Compute the hash
                    using (FileStream inStream = new FileStream(FileToPackageFullName, FileMode.Open))
                    {
                        Hash = md5.ComputeHash(inStream);
                    }

                    //Add new PatchInfo entry
                    PatchFileInfo patchFile = new PatchFileInfo();
                    patchFile.FileMD5 = Hash;
                    patchFile.Filename = FileToPackage;
                    patchFile.FileUrl = HttpUrl + PackageName;
                    patch.Files.Add(patchFile);
                }

                //Upload file to ftp
                ftp.Files.Upload(PackageName, PackageFilename);
                while (!ftp.Files.UploadComplete)
                {
                    Thread.Sleep(100);
                }
                ftp.Disconnect();
            }

            //Save and upload patch info XML
            string PatchTemp = Path.Combine(Path.GetTempPath(),PatchInfo.PatchInfoFileName);
            patch.Save(PatchTemp);
            
            ftp.Files.Upload(PatchInfo.PatchInfoFileName, PatchTemp);
            while (!ftp.Files.UploadComplete)
            {
                Thread.Sleep(100);
            }

            try
            {
                ftp.Disconnect();
            }
            catch { }


            return patch;
        }

        public static void UnpackPackage(PatchInfo patch)
        {
            foreach (PatchFileInfo file in patch.Files)
            {
                string TempFileName = file.Filename + TempExtension;
                string TempFullFileName = Path.Combine(Updater.UltimaOnlinePath, TempFileName);

                string FileName = file.Filename;
                string FullFileName = Path.Combine(Updater.UltimaOnlinePath, FileName);

                if (File.Exists(TempFullFileName))
                {
                    using (FileStream inStream = new FileStream(TempFullFileName, FileMode.Open))
                    {
                        inStream.Seek(0, 0);
                        using (FileStream outStream = new FileStream(FullFileName, FileMode.Create))
                        {


                            SevenZip.Compression.LZMA.Decoder decoder = new SevenZip.Compression.LZMA.Decoder();
                            byte[] properties2 = new byte[5];
                            if (inStream.Read(properties2, 0, 5) != 5)
                                throw (new Exception("input .lzma is too short"));

                            long outSize = 0;
                            for (int i = 0; i < 8; i++)
                            {
                                int v = inStream.ReadByte();
                                if (v < 0)
                                    throw (new Exception("Can't Read 1"));
                                outSize |= ((long)(byte)v) << (8 * i);
                            }
                            decoder.SetDecoderProperties(properties2);

                            long compressedSize = inStream.Length - inStream.Position;
                            decoder.Code(inStream, outStream, compressedSize, outSize, null);
                        }
                    }

                    if (File.Exists(TempFullFileName))
                        File.Delete(TempFullFileName);
                }
            }

        }

        public static byte[] Decompress(MemoryStream newInStream)
        {
            SevenZip.Compression.LZMA.Decoder decoder = new SevenZip.Compression.LZMA.Decoder();
            newInStream.Seek(0, 0);
            MemoryStream newOutStream = new MemoryStream();
            byte[] properties2 = new byte[5];
            if (newInStream.Read(properties2, 0, 5) != 5)
                throw (new Exception("input .lzma is too short"));
            long outSize = 0;
            for (int i = 0; i < 8; i++)
            {
                int v = newInStream.ReadByte();
                if (v < 0)
                    throw (new Exception("Can't Read 1"));
                outSize |= ((long)(byte)v) << (8 * i);
            }
            decoder.SetDecoderProperties(properties2);
            long compressedSize = newInStream.Length - newInStream.Position;
            decoder.Code(newInStream, newOutStream, compressedSize, outSize, null);
            byte[] b = newOutStream.ToArray();
            return b;
        }




    }
}
