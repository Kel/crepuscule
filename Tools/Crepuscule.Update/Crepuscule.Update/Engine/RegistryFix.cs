﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace Crepuscule.Update.Engine
{
    public class RegistryFix
    {

        public static void FixPath(string newPath)
        {
            if(!newPath.EndsWith(@"\"))
                newPath += @"\";

            //[HKEY_LOCAL_MACHINE\SOFTWARE\Origin Worlds Online]
            //"HarvestStage"=dword:00000000
            //"UniqueInstanceId"=dword:471e61ed
            //"LastHarvestTime"=dword:478ce290

            var key = GetKey(@"SOFTWARE\Origin Worlds Online");
            if (key != null)
            {
                key.SetValue("HarvestStage", 0, RegistryValueKind.DWord);
                key.SetValue("UniqueInstanceId", 0x471e61ed, RegistryValueKind.DWord);
                key.SetValue("LastHarvestTime", 0x478ce290, RegistryValueKind.DWord);
            }
            

            //[HKEY_LOCAL_MACHINE\SOFTWARE\Origin Worlds Online\Ultima Online]

            key = GetKey(@"SOFTWARE\Origin Worlds Online\Ultima Online");

            //[HKEY_LOCAL_MACHINE\SOFTWARE\Origin Worlds Online\Ultima Online\1.0]
            //"ExePath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client\\client.exe"
            //"InstCDPath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client"
            //"PatchExePath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client\\uopatch.exe"
            //"StartExePath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client\\uo.exe"
            //"Upgraded"="Yes"

            key = GetKey(@"SOFTWARE\Origin Worlds Online\Ultima Online\1.0");
            if (key != null)
            {
                key.SetValue("ExePath", newPath + "client.exe");
                key.SetValue("InstCDPath", newPath);
                key.SetValue("PatchExePath", newPath + "uopatch.exe");
                key.SetValue("StartExePath", newPath + "uo.exe");
                key.SetValue("Upgraded", "Yes");
            }


            //[HKEY_LOCAL_MACHINE\SOFTWARE\Origin Worlds Online\Ultima Online Third Dawn]

            key = GetKey(@"SOFTWARE\Origin Worlds Online\Ultima Online Third Dawn");

            //[HKEY_LOCAL_MACHINE\SOFTWARE\Origin Worlds Online\Ultima Online Third Dawn\1.0]
            //"ExePath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client\\uotd.exe"
            //"InstCDPath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client"
            //"PatchExePath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client\\uopatch.exe"
            //"StartExePath"="C:\\Program Files\\EA GAMES\\Ultima Online 2D Client\\uo.exe"
            //"Upgraded"="Yes"

            key = GetKey(@"SOFTWARE\Origin Worlds Online\Ultima Online Third Dawn\1.0");
            if (key != null)
            {
                key.SetValue("ExePath", newPath + "uotd.exe");
                key.SetValue("InstCDPath", newPath);
                key.SetValue("PatchExePath", newPath + "uopatch.exe");
                key.SetValue("StartExePath", newPath + "uo.exe");
                key.SetValue("Upgraded", "Yes");
            }

        }

        public static RegistryKey GetKey(string subKey)
        {
            RegistryKey key;
            try
            {
                key = Registry.LocalMachine.OpenSubKey(subKey, true);
                if (key == null)
                {
                    key = Registry.LocalMachine.CreateSubKey(subKey);
                }
            }
            catch (Exception)
            {
                return null;
            }
 
            return key;
        }
    }
}
