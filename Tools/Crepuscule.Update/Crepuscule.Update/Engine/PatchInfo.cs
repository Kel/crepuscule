﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace Crepuscule.Update.Engine
{
    [Serializable]
    public class PatchInfo
    {
        public static string PatchInfoFileName = "Patch.xml";

        public PatchInfo()
        {
        }

        //Files
        public string PatchVersion = "";
        public List<PatchFileInfo> Files = new List<PatchFileInfo>();

        #region Load/Save code
        /// <summary>
        /// Saves the current settings
        /// </summary>
        /// <param name="filename">The filename to save to</param>
        public void Save(string filename)
        {
            Stream stream = File.Create(filename);

            XmlSerializer serializer = new XmlSerializer(typeof(PatchInfo));
            serializer.Serialize(stream, this);
            stream.Close();
        }

        /// <summary>
        /// Loads settings from a file
        /// </summary>
        /// <param name="filename">The filename to load</param>
        public static PatchInfo Load(string filename)
        {
            Stream stream = File.OpenRead(filename);
            XmlSerializer serializer = new XmlSerializer(typeof(PatchInfo));
            PatchInfo retVal = (PatchInfo)serializer.Deserialize(stream);
            stream.Close();
            return retVal;
        }

        public static PatchInfo LoadOrCreate(string filename)
        {
            try
            {
                return PatchInfo.Load(filename);
            }
            catch
            {
                PatchInfo settings = new PatchInfo();
                try { settings.Save(filename); }
                catch { }
                return settings;
            }
        }
        #endregion
    }

    [Serializable]
    public class PatchFileInfo
    {
        public PatchFileInfo(){}

        public string Filename;
        public string FileUrl;
        public byte[] FileMD5;
    }
}
