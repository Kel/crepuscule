﻿namespace Crepuscule.Update.Forms
{
    partial class OptionsMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTips = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.UOFolder = new Crepuscule.Update.Forms.Menus.FolderChoose();
            this.bReplaceRegisters = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(70)))), ((int)(((byte)(66)))));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(23, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Répertoire Ultima Online:";
            // 
            // UOFolder
            // 
            this.UOFolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(70)))), ((int)(((byte)(66)))));
            this.UOFolder.Location = new System.Drawing.Point(153, 27);
            this.UOFolder.Name = "UOFolder";
            this.UOFolder.SelectedPath = "";
            this.UOFolder.Size = new System.Drawing.Size(392, 28);
            this.UOFolder.TabIndex = 18;
            // 
            // bReplaceRegisters
            // 
            this.bReplaceRegisters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bReplaceRegisters.Location = new System.Drawing.Point(390, 61);
            this.bReplaceRegisters.Name = "bReplaceRegisters";
            this.bReplaceRegisters.Size = new System.Drawing.Size(151, 23);
            this.bReplaceRegisters.TabIndex = 19;
            this.bReplaceRegisters.Text = "Réparer l\'installation de UO";
            this.bReplaceRegisters.UseVisualStyleBackColor = true;
            this.bReplaceRegisters.Click += new System.EventHandler(this.bReplaceRegisters_Click);
            // 
            // OptionsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Crepuscule.Update.Properties.Resources.back;
            this.Controls.Add(this.bReplaceRegisters);
            this.Controls.Add(this.UOFolder);
            this.Controls.Add(this.label1);
            this.Name = "OptionsMenu";
            this.Size = new System.Drawing.Size(618, 428);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip ToolTips;
        private System.Windows.Forms.Label label1;
        private Crepuscule.Update.Forms.Menus.FolderChoose UOFolder;
        private System.Windows.Forms.Button bReplaceRegisters;
    }
}
