﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Crepuscule.Update.Properties;
using Crepuscule.Update.Engine;

namespace Crepuscule.Update.Forms
{
    public partial class OptionsMenu : UserControl
    {
        public OptionsMenu()
        {
            InitializeComponent();
            InitializeSettings();
        }


        private void InitializeSettings()
        {
            this.UOFolder.TextPath.TextChanged += new EventHandler(TextPath_TextChanged);
            this.UOFolder.SelectedPath = Program.GetUltimaOnlinePath();
        }

        #region Event Handlers
        void TextPath_TextChanged(object sender, EventArgs e) { Ultima.Files.Directory = UOFolder.SelectedPath; }

        #endregion

        private void bReplaceRegisters_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Voulez-vous effectuer une réparation du chemin d'Ultima Online dans les registres Windows?", "Réparation", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                result = MessageBox.Show("Est-ce que le chemin indiqué est correct et vous voulez effectuer une réparation du chemin pour Crépuscule 1? \r\n" +
                "Si le chemin est incorrect, veuillez appyuer sur non, indiquer le chemin correct et recommencer.", "Réparation", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    RegistryFix.FixPath(this.UOFolder.SelectedPath);
                }
            }
        }
    }

}
