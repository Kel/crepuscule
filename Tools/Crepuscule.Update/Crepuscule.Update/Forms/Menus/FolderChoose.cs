﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Crepuscule.Update.Forms.Menus
{
    public partial class FolderChoose : UserControl
    {

        public FolderChoose()
        {
            InitializeComponent();
            BrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
        }

        public string SelectedPath
        {
            get { return bText.Text; }
            set { BrowserDialog.SelectedPath = value; bText.Text = value; }
        }

        public TextBox TextPath
        {
            get { return bText; }
        }

        public bool IsValidPath
        {
            get
            {
                if (String.IsNullOrEmpty(bText.Text)) return false;
                DirectoryInfo dir = new DirectoryInfo(bText.Text);
                return dir.Exists;
            }
        }

        private void bBrowse_Click(object sender, EventArgs e)
        {
            BrowserDialog.SelectedPath = bText.Text;
            DialogResult result = BrowserDialog.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                bText.Text = BrowserDialog.SelectedPath;
            }
        }
    }
}
