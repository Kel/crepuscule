﻿namespace Crepuscule.Update
{
    partial class LauncherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LauncherForm));
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.bPubView = new System.Windows.Forms.ToolStripButton();
            this.pStatusView = new System.Windows.Forms.ToolStripButton();
            this.bChat = new System.Windows.Forms.ToolStripButton();
            this.bViewWebsite = new System.Windows.Forms.ToolStripButton();
            this.bViewForum = new System.Windows.Forms.ToolStripButton();
            this.bViewWiki = new System.Windows.Forms.ToolStripButton();
            this.bOptions = new System.Windows.Forms.ToolStripButton();
            this.bHiddenAd = new System.Windows.Forms.WebBrowser();
            this.bPub = new Crepuscule.Update.ExWebBrowser.ExtendedWebBrowser();
            this.bUpdate = new Crepuscule.Update.Iconits();
            this.bPlay = new Crepuscule.Update.Iconits();
            this.optionsMenu = new Crepuscule.Update.Forms.OptionsMenu();
            this.bClock = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(97, 35);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(97, 35);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AllowMerge = false;
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackgroundImage = global::Crepuscule.Update.Properties.Resources.bar;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bPubView,
            this.pStatusView,
            this.bChat,
            this.bClock,
            this.bViewWebsite,
            this.bViewForum,
            this.bViewWiki,
            this.bOptions});
            this.toolStrip1.Location = new System.Drawing.Point(0, 142);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(617, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // bPubView
            // 
            this.bPubView.ForeColor = System.Drawing.Color.Silver;
            this.bPubView.Image = global::Crepuscule.Update.Properties.Resources.book;
            this.bPubView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bPubView.Name = "bPubView";
            this.bPubView.Size = new System.Drawing.Size(112, 22);
            this.bPubView.Text = "Aidez le serveur!";
            this.bPubView.Click += new System.EventHandler(this.bPubView_Click);
            // 
            // pStatusView
            // 
            this.pStatusView.ForeColor = System.Drawing.Color.Silver;
            this.pStatusView.Image = global::Crepuscule.Update.Properties.Resources.server;
            this.pStatusView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pStatusView.Name = "pStatusView";
            this.pStatusView.Size = new System.Drawing.Size(58, 22);
            this.pStatusView.Text = "Statut";
            this.pStatusView.Click += new System.EventHandler(this.pStatusView_Click);
            // 
            // bChat
            // 
            this.bChat.ForeColor = System.Drawing.Color.Silver;
            this.bChat.Image = ((System.Drawing.Image)(resources.GetObject("bChat.Image")));
            this.bChat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bChat.Name = "bChat";
            this.bChat.Size = new System.Drawing.Size(52, 22);
            this.bChat.Text = "Chat";
            this.bChat.ToolTipText = "Chat";
            this.bChat.Click += new System.EventHandler(this.bChat_Click);
            // 
            // bViewWebsite
            // 
            this.bViewWebsite.ForeColor = System.Drawing.Color.Silver;
            this.bViewWebsite.Image = ((System.Drawing.Image)(resources.GetObject("bViewWebsite.Image")));
            this.bViewWebsite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bViewWebsite.Name = "bViewWebsite";
            this.bViewWebsite.Size = new System.Drawing.Size(73, 22);
            this.bViewWebsite.Text = "Site Web";
            this.bViewWebsite.Click += new System.EventHandler(this.bViewWebsite_Click);
            // 
            // bViewForum
            // 
            this.bViewForum.ForeColor = System.Drawing.Color.Silver;
            this.bViewForum.Image = ((System.Drawing.Image)(resources.GetObject("bViewForum.Image")));
            this.bViewForum.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bViewForum.Name = "bViewForum";
            this.bViewForum.Size = new System.Drawing.Size(62, 22);
            this.bViewForum.Text = "Forum";
            this.bViewForum.Click += new System.EventHandler(this.bViewForum_Click);
            // 
            // bViewWiki
            // 
            this.bViewWiki.ForeColor = System.Drawing.Color.Silver;
            this.bViewWiki.Image = ((System.Drawing.Image)(resources.GetObject("bViewWiki.Image")));
            this.bViewWiki.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bViewWiki.Name = "bViewWiki";
            this.bViewWiki.Size = new System.Drawing.Size(50, 22);
            this.bViewWiki.Text = "Wiki";
            this.bViewWiki.Click += new System.EventHandler(this.bViewWiki_Click);
            // 
            // bOptions
            // 
            this.bOptions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bOptions.Image = ((System.Drawing.Image)(resources.GetObject("bOptions.Image")));
            this.bOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bOptions.Name = "bOptions";
            this.bOptions.Size = new System.Drawing.Size(86, 22);
            this.bOptions.Text = "Paramètres";
            this.bOptions.Click += new System.EventHandler(this.bOptions_Click);
            // 
            // bHiddenAd
            // 
            this.bHiddenAd.AllowWebBrowserDrop = false;
            this.bHiddenAd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.bHiddenAd.IsWebBrowserContextMenuEnabled = false;
            this.bHiddenAd.Location = new System.Drawing.Point(177, 263);
            this.bHiddenAd.MinimumSize = new System.Drawing.Size(20, 20);
            this.bHiddenAd.Name = "bHiddenAd";
            this.bHiddenAd.ScriptErrorsSuppressed = true;
            this.bHiddenAd.Size = new System.Drawing.Size(255, 107);
            this.bHiddenAd.TabIndex = 8;
            this.bHiddenAd.WebBrowserShortcutsEnabled = false;
            // 
            // bPub
            // 
            this.bPub.AllowWebBrowserDrop = false;
            this.bPub.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.bPub.IsWebBrowserContextMenuEnabled = false;
            this.bPub.Location = new System.Drawing.Point(0, 165);
            this.bPub.MinimumSize = new System.Drawing.Size(20, 20);
            this.bPub.Name = "bPub";
            this.bPub.Size = new System.Drawing.Size(617, 426);
            this.bPub.TabIndex = 9;
            this.bPub.WebBrowserShortcutsEnabled = false;
            // 
            // bUpdate
            // 
            this.bUpdate.BackColor = System.Drawing.Color.Transparent;
            this.bUpdate.BackgroundImage = global::Crepuscule.Update.Properties.Resources.Back2;
            this.bUpdate.Blur = false;
            this.bUpdate.Icon = global::Crepuscule.Update.Properties.Resources.mail_in_a_bottle_128x128;
            this.bUpdate.IconSize = new System.Drawing.Size(80, 80);
            this.bUpdate.Location = new System.Drawing.Point(449, 5);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(128, 128);
            this.bUpdate.TabIndex = 6;
            this.bUpdate.TooltipText = "Mettre à jour!";
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // bPlay
            // 
            this.bPlay.BackColor = System.Drawing.Color.Transparent;
            this.bPlay.BackgroundImage = global::Crepuscule.Update.Properties.Resources.Back1;
            this.bPlay.Blur = false;
            this.bPlay.Icon = global::Crepuscule.Update.Properties.Resources.treasure_map_128x128;
            this.bPlay.IconSize = new System.Drawing.Size(80, 80);
            this.bPlay.Location = new System.Drawing.Point(65, 2);
            this.bPlay.Name = "bPlay";
            this.bPlay.Size = new System.Drawing.Size(128, 128);
            this.bPlay.TabIndex = 5;
            this.bPlay.TooltipText = "Jouer!";
            this.bPlay.Click += new System.EventHandler(this.bPlay_Click);
            // 
            // optionsMenu
            // 
            this.optionsMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.optionsMenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("optionsMenu.BackgroundImage")));
            this.optionsMenu.Location = new System.Drawing.Point(0, 164);
            this.optionsMenu.Name = "optionsMenu";
            this.optionsMenu.Size = new System.Drawing.Size(618, 428);
            this.optionsMenu.TabIndex = 10;
            this.optionsMenu.Visible = false;
            // 
            // bClock
            // 
            this.bClock.ForeColor = System.Drawing.Color.Silver;
            this.bClock.Image = ((System.Drawing.Image)(resources.GetObject("bClock.Image")));
            this.bClock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bClock.Name = "bClock";
            this.bClock.Size = new System.Drawing.Size(70, 22);
            this.bClock.Text = "Horloge";
            this.bClock.Click += new System.EventHandler(this.bClock_Click);
            // 
            // LauncherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Crepuscule.Update.Properties.Resources.Logo;
            this.ClientSize = new System.Drawing.Size(618, 592);
            this.Controls.Add(this.optionsMenu);
            this.Controls.Add(this.bPub);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.bUpdate);
            this.Controls.Add(this.bPlay);
            this.Controls.Add(this.bHiddenAd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LauncherForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crépuscule - Shard Ultima Online";
            this.Load += new System.EventHandler(this.LauncherForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Iconits bPlay;
        private Iconits bUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton bPubView;
        private System.Windows.Forms.ToolStripButton pStatusView;
        private System.Windows.Forms.ToolStripButton bViewWebsite;
        private System.Windows.Forms.ToolStripButton bViewForum;
        private System.Windows.Forms.ToolStripButton bViewWiki;
        private System.Windows.Forms.WebBrowser bHiddenAd;
        private Crepuscule.Update.ExWebBrowser.ExtendedWebBrowser bPub;
        private System.Windows.Forms.ToolStripButton bChat;
        private Crepuscule.Update.Forms.OptionsMenu optionsMenu;
        private System.Windows.Forms.ToolStripButton bOptions;
        private System.Windows.Forms.ToolStripButton bClock;
    }
}

