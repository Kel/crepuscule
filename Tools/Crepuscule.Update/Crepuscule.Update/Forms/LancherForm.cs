﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Crepuscule.Update.Engine;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;
using System.Threading;
using System.Collections;
using Crepuscule.Update.ExWebBrowser;

namespace Crepuscule.Update
{
    public partial class LauncherForm : Form
    {
        string PubUrl = "http://www.au-crepuscule.com/pub.php";
        string StatusUrl = "http://www.au-crepuscule.com/status.php?client=updater";
        string AboutUrl = "http://www.au-crepuscule.com/about.php";
        string ChatUrl = "http://www.au-crepuscule.com/chat.php";
        string ClockUrl = "http://www.au-crepuscule.com/clock.php";
        bool PubClicked = false;
        bool ExecuteUpdate = false;
        string DefaultBrowserPath = "";
 
        public LauncherForm() : this(false) {}

        public LauncherForm(bool Update)
        {
            InitializeComponent();
            ExecuteUpdate = Update;
            Text = "Crépuscule - Shard Ultima Online. Updater " + Program.GetVersion(true);

            try
            {
                DefaultBrowserPath = PathHelper.GetDefaultBrowserPath();
                bPub.Navigate(PubUrl);

                bPub.StartNewWindow += new EventHandler<BrowserExtendedNavigatingEventArgs>(bPub_StartNewWindow);
                
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        #region Ad managment
        void bPub_StartNewWindow(object sender, BrowserExtendedNavigatingEventArgs e)
        {
            try
            {
                if (!PubClicked && bPub.Url.ToString() == PubUrl)
                {
                    e.Cancel = true; //Stop event from being processed
                    PubClicked = true;

                    //Code to open in same window
                    bHiddenAd.Navigate(e.Url);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }


        private void DoAdQuery()
        {
            try
            {
                if (bPub.Url.ToString() == PubUrl && !PubClicked)
                {
                    Point SelfPoint = new Point(bPub.Width / 2, bPub.Height / 2);
                    Point Point = bPub.PointToScreen(SelfPoint);
                    Win32.SendClick(Point);
                }
            }
            catch { }
        }
        #endregion

        #region Update
        private void bUpdate_Click(object sender, EventArgs e)
        {
            DoUpdate();
        }

        private void LauncherForm_Load(object sender, EventArgs e)
        {
            if (ExecuteUpdate) DoUpdate();
        }

        private void DoUpdate()
        {
            if (!Updater.CheckUpdates())
            {
                MessageBox.Show("Pas de nouvelles mises à jour disponibles!",
                    "Crépuscule", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DownloadURLCollection Urls = new DownloadURLCollection();
            foreach (PatchFileInfo file in Updater.UpdateList)
            {
                Urls.Add(new DownloadURL(file.FileUrl,
                    Path.Combine(Updater.UltimaOnlinePath, file.Filename + Packager.TempExtension)));
            }
            new UpdaterForm(Urls).ShowDialog();
        }
        #endregion

        #region Play
        private void bPlay_Click(object sender, EventArgs e)
        {
            DoAdQuery();
            try
            {
                if (new FileInfo(Program.AppPath + "Crepuscule2.exe").Exists)
                    Process.Start(Program.AppPath + "Crepuscule2.exe");
                else Process.Start(Program.AppPath + "Crepuscule.exe");
                //this.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }


        #endregion

        #region Navigation
        private void pStatusView_Click(object sender, EventArgs e) { NavigateToUrl(StatusUrl); }
        private void bPubView_Click(object sender, EventArgs e) { NavigateToUrl(PubUrl); }
        private void bAbout_Click(object sender, EventArgs e) { NavigateToUrl(AboutUrl); }
        private void bChat_Click(object sender, EventArgs e) { NavigateToUrl(ChatUrl); }
        private void bClock_Click(object sender, EventArgs e) { NavigateToUrl(ClockUrl); }
        private void bViewWebsite_Click(object sender, EventArgs e) { OpenLink("http://www.au-crepuscule.com"); }
        private void bViewForum_Click(object sender, EventArgs e) { OpenLink("http://www.au-crepuscule.com/forum/"); }
        private void bViewWiki_Click(object sender, EventArgs e) { OpenLink("http://www.au-crepuscule.com/wiki/"); }
        


        private void OpenLink(string url)
        {
            try
            {
                DoAdQuery();
                Process.Start(DefaultBrowserPath, url);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void NavigateToUrl(string url)
        {
            if (!this.optionsMenu.Visible)
            {
                DoAdQuery();
            }
            else
            {
                this.optionsMenu.Visible = false;
            }
            bPub.Navigate(url);
        }


        private void bOptions_Click(object sender, EventArgs e)
        {
            if (!this.optionsMenu.Visible)
                this.optionsMenu.Visible = true;
        }

        #endregion


    }
}
