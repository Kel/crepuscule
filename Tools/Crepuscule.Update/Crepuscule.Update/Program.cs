﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Crepuscule.Update.Engine;
using System.Reflection;
using Crepuscule.Update.Properties;

namespace Crepuscule.Update
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Updater.UltimaOnlinePath = Updater.PatchInfoFilename = Program.AppPath;
            bool NeedUpdate = false;

            try
            {
                //Delete temp dir
                if (new DirectoryInfo(FileDownloader.DownloaderTempDirectory).Exists)
                    Directory.Delete(FileDownloader.DownloaderTempDirectory);

                if (Updater.CheckUpdates())
                {
                    DialogResult result = MessageBox.Show("Une mise à jour des fichiers Crépuscule a été trouvée, veuillez executer la mise à jour afin d'obtenir les derniers fichiers.\r\nVoulez-vous mettre à jour maintenant?",
                        "Crépuscule", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                    if (result == DialogResult.Yes)
                    {
                        NeedUpdate = true;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Erreur!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            Application.Run(new LauncherForm(NeedUpdate));
        }

        public static string AppPath
        {
            get { return new DirectoryInfo(Application.ExecutablePath).Parent.FullName + "\\"; }
        }

        /// <summary>
        /// Gets current UO Path
        /// </summary>
        public static string GetUltimaOnlinePath()
        {
            if (String.IsNullOrEmpty(Settings.Default.UOPath))
            {
                Ultima.Files.ReLoadDirectory();
                Settings.Default.UOPath = Ultima.Files.Directory;
            }

            return Settings.Default.UOPath;
        }


        /// <summary>
        /// Gets the engine version
        /// </summary>
        public static string GetVersion(bool _Simle)
        {
            AssemblyName an = Assembly.GetExecutingAssembly().GetName();
            System.Version version = an.Version;
            string path = Assembly.GetExecutingAssembly().Location;
            DateTime dt = System.IO.File.GetLastWriteTime(path);
            string ret;

            if (!_Simle)
            {
                string format = "v{0}.{1} build {2} revision {3}";
                ret = string.Format(
                    format,
                    version.Major,
                    version.Minor,
                    version.Build,
                    version.Revision);
            }
            else
            {
                string format = "v{0}.{1}";
                ret = string.Format(
                    format,
                    version.Major,
                    version.Minor);
            }
            return ret;
        }
    }
}
