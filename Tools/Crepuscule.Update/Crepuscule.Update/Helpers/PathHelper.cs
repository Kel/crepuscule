﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace Crepuscule.Update
{
    public static class PathHelper
    {
        public static string GetDefaultBrowserPath()
        {
            try
            {
                string key = @"htmlfile\shell\open\command";
                RegistryKey registryKey =
                Registry.ClassesRoot.OpenSubKey(key, false);
                // get default browser path
                return ((string)registryKey.GetValue(null, null)).Split('"')[1];
            }
            catch
            {
                return "iexplore";
            }
        }

    }
}
