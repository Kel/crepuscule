﻿namespace Server
{
    using Server.Network;
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using System.Net;

    public class Core
    {
        private static Thread m_SpikeThread;
        private static bool m_AOS;
        private static System.Reflection.Assembly m_Assembly;
        private static string m_BaseDirectory;
        private static bool m_Closing;
        private static bool m_Crashed;
        private static ArrayList m_DataDirectories = new ArrayList();
        private static string m_ExePath;
        private static int m_GlobalMaxUpdateRange = 0x18;
        private static int m_ItemCount;
        private static Server.Network.MessagePump m_MessagePump;
        private static int m_MobileCount;
        private static MultiTextWriter m_MultiConOut;
        private static System.Diagnostics.Process m_Process;
        private static DateTime m_ProfileStart;
        private static TimeSpan m_ProfileTime;
        private static bool m_Profiling;
        private static bool m_Service;
        private static System.Threading.Thread m_Thread;
        public static Server.Slice Slice;
        private static System.Threading.Thread timerThread;

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            HandleClosed();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.IsTerminating ? "Error:" : "Warning:");
            Console.WriteLine(e.ExceptionObject);
            if (e.IsTerminating)
            {
                m_Crashed = true;
                bool close = false;
                try
                {
                    CrashedEventArgs args = new CrashedEventArgs(e.ExceptionObject as Exception);
                    EventSink.InvokeCrashed(args);
                    close = args.Close;
                }
                catch
                {
                }
                if (!close && !m_Service)
                {
                    Console.WriteLine("This exception is fatal, press return to exit");
                    Console.ReadLine();
                }
            }
        }

        public static string FindDataFile(string path)
        {
            if (m_DataDirectories.Count == 0)
            {
                throw new InvalidOperationException("Attempted to FindDataFile before DataDirectories list has been filled.");
            }
            string str = null;
            for (int i = 0; i < m_DataDirectories.Count; i++)
            {
                str = Path.Combine((string) m_DataDirectories[i], path);
                if (File.Exists(str))
                {
                    return str;
                }
                str = null;
            }
            return str;
        }

        public static string FindDataFile(string format, params object[] args)
        {
            return FindDataFile(string.Format(format, args));
        }

        private static void HandleClosed()
        {
            if (!m_Closing)
            {
                m_Closing = true;
                Console.Write("Exiting...");
                if (!m_Crashed)
                {
                    EventSink.InvokeShutdown(new ShutdownEventArgs());
                }
                timerThread.Join();
                Console.WriteLine("done");
            }
        }

        [STAThread]
        public static void Main(string[] args)
        {
            m_Expansion = Expansion.ML;
#if DEBUG
            new DebugHack();
#endif
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(Core.CurrentDomain_UnhandledException);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(Core.CurrentDomain_ProcessExit);
            bool debug = false;
            for (int i = 0; i < args.Length; i++)
            {
                if (Insensitive.Equals(args[i], "-debug"))
                {
                    debug = true;
                }
                else if (Insensitive.Equals(args[i], "-service"))
                {
                    m_Service = true;
                }
                else if (Insensitive.Equals(args[i], "-profile"))
                {
                    Profiling = true;
                }
            }
            try
            {
                if (m_Service)
                {
                    if (!Directory.Exists("Logs"))
                    {
                        Directory.CreateDirectory("Logs");
                    }
                    Console.SetOut(m_MultiConOut = new MultiTextWriter(new TextWriter[] { Console.Out, new FileLogger("Logs/Console.log") }));
                }
                else
                {
                    Console.SetOut(m_MultiConOut = new MultiTextWriter(new TextWriter[] { Console.Out }));
                }
            }
            catch
            {
            }
            m_Thread = System.Threading.Thread.CurrentThread;
            m_Process = System.Diagnostics.Process.GetCurrentProcess();
            m_Assembly = System.Reflection.Assembly.GetEntryAssembly();
            if (m_Thread != null)
            {
                m_Thread.Name = "Core Thread";
            }
            if (BaseDirectory.Length > 0)
            {
                Directory.SetCurrentDirectory(BaseDirectory);
            }
            Server.Timer.TimerThread thread = new Server.Timer.TimerThread();
            timerThread = new System.Threading.Thread(new ThreadStart(thread.TimerMain));
            timerThread.Name = "Timer Thread";

            int platform = (int)Environment.OSVersion.Platform;
            if (platform == 4 || platform == 128)
            { // MS 4, MONO 128
                m_Unix = true;
                Console.WriteLine("Core: Unix environment detected");
            }

            while (!ScriptCompiler.Compile(debug))
            {
                Console.WriteLine("Scripts: One or more scripts failed to compile or no script files were found.");
                Console.WriteLine(" - Press return to exit, or R to try again.");
                string str = Console.ReadLine();
                if ((str == null) || (str.ToLower() != "r"))
                {
                    return;
                }
            }
            Region.Load();
            timerThread.Start();
            m_MessagePump = new MessagePump();

            for (int j = 0; j < Map.AllMaps.Count; j++)
            {
                ((Map) Map.AllMaps[j]).Tiles.Force();
            }
            NetState.Initialize();
            EventSink.InvokeServerStarted();

            // Run Spike on port 80
            m_SpikeThread = new Thread(() => {
                Spike.Kernel.MethodNameConfigure  = "SpikeConfigure";
                Spike.Kernel.MethodNameInitialize = "SpikeInitialize";
                Spike.Kernel.MethodNameTerminate  = "SpikeTerminate";
                Spike.Kernel.Run(
                        new IPEndPoint(IPAddress.Any, 8080)
                    );
            });
            m_SpikeThread.Start();

            // Main loop
            try
            {
                DateTime now, last = DateTime.Now;

                const int sampleInterval = 100;
                const float ticksPerSecond = (float)(TimeSpan.TicksPerSecond * sampleInterval);

                long sample = 0;

                while (m_Signal.WaitOne())
                {
                    Mobile.ProcessDeltaQueue();
                    Item.ProcessDeltaQueue();

                    Timer.Slice();
                    m_MessagePump.Slice();

                    NetState.FlushAll();
                    NetState.ProcessDisposedQueue();

                    if (Slice != null)
                        Slice();

                    if ((++sample % sampleInterval) == 0)
                    {
                        now = DateTime.Now;
                        m_CyclesPerSecond[m_CycleIndex++ % m_CyclesPerSecond.Length] =
                            ticksPerSecond / (now.Ticks - last.Ticks);
                        last = now;
                    }
                }
            }
            catch (Exception e)
            {
                CurrentDomain_UnhandledException(null, new UnhandledExceptionEventArgs(e, true));
            }

            // Main loop (old)
            /*try
            {
                while (!m_Closing)
                {
                    System.Threading.Thread.Sleep(1);
                    Mobile.ProcessDeltaQueue();
                    Item.ProcessDeltaQueue();
                    Server.Timer.Slice();
                    m_MessagePump.Slice();

                    NetState.FlushAll();
                    NetState.ProcessDisposedQueue();

                    if (Slice != null)
                    {
                        Slice();
                    }
                }
            }
            catch (Exception exception)
            {
                CurrentDomain_UnhandledException(null, new UnhandledExceptionEventArgs(exception, true));
            }
            if (timerThread.IsAlive)
            {
                timerThread.Abort();
            }*/
        }


        #region Runuo 2 Stuff
        private static AutoResetEvent m_Signal = new AutoResetEvent(true);
        public static void Set() { m_Signal.Set(); }
        public static readonly bool Is64Bit = Environment.Is64BitProcess;
        private static long m_CycleIndex = 1;
        private static float[] m_CyclesPerSecond = new float[100];
        private static bool m_Unix;
        private static Expansion m_Expansion;


        public static bool Unix { get { return m_Unix; } }

        public static float CyclesPerSecond
        {
            get { return m_CyclesPerSecond[(m_CycleIndex - 1) % m_CyclesPerSecond.Length]; }
        }
        
        public static Expansion Expansion
        {
            get { return m_Expansion; }
            set { m_Expansion = value; }
        }

        public static bool T2A
        {
            get { return m_Expansion >= Expansion.T2A; }
        }

        public static bool UOR
        {
            get { return m_Expansion >= Expansion.UOR; }
        }

        public static bool UOTD
        {
            get { return m_Expansion >= Expansion.UOTD; }
        }

        public static bool LBR
        {
            get { return m_Expansion >= Expansion.LBR; }
        }

        public static bool AOS
        {
            get { return m_Expansion >= Expansion.AOS; }
        }

        public static bool SE
        {
            get { return m_Expansion >= Expansion.SE; }
        }

        public static bool ML
        {
            get { return m_Expansion >= Expansion.ML; }
        }

        public static bool SA
        {
            get { return m_Expansion >= Expansion.SA; }
        }

        #endregion

        public static void VerifySerialization()
        {
            m_ItemCount = 0;
            m_MobileCount = 0;
            VerifySerialization(System.Reflection.Assembly.GetCallingAssembly());
            for (int i = 0; i < ScriptCompiler.Assemblies.Length; i++)
            {
                VerifySerialization(ScriptCompiler.Assemblies[i]);
            }
        }

        private static void VerifySerialization(System.Reflection.Assembly a)
        {
            if (a != null)
            {
                Type[] types = new Type[] { typeof(Serial) };
                foreach (Type type in a.GetTypes())
                {
                    bool flag = type.IsSubclassOf(typeof(Item));
                    if (flag || type.IsSubclassOf(typeof(Mobile)))
                    {
                        if (flag)
                        {
                            m_ItemCount++;
                        }
                        else
                        {
                            m_MobileCount++;
                        }
                        bool flag2 = false;
                        try
                        {
                            if (type.GetConstructor(types) == null)
                            {
                                if (!flag2)
                                {
                                    Console.WriteLine("Warning: {0}", type);
                                }
                                flag2 = true;
                                Console.WriteLine("       - No serialization constructor");
                            }
                            if (type.GetMethod("Serialize", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly) == null)
                            {
                                if (!flag2)
                                {
                                    Console.WriteLine("Warning: {0}", type);
                                }
                                flag2 = true;
                                Console.WriteLine("       - No Serialize() method");
                            }
                            if (type.GetMethod("Deserialize", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly) == null)
                            {
                                if (!flag2)
                                {
                                    Console.WriteLine("Warning: {0}", type);
                                }
                                flag2 = true;
                                Console.WriteLine("       - No Deserialize() method");
                            }
                            if (flag2)
                            {
                                Console.WriteLine();
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        public static System.Reflection.Assembly Assembly
        {
            get
            {
                return m_Assembly;
            }
            set
            {
                m_Assembly = value;
            }
        }

        public static string BaseDirectory
        {
            get
            {
                if (m_BaseDirectory == null)
                {
                    try
                    {
                        m_BaseDirectory = ExePath;
                        if (m_BaseDirectory.Length > 0)
                        {
                            m_BaseDirectory = Path.GetDirectoryName(m_BaseDirectory);
                        }
                    }
                    catch
                    {
                        m_BaseDirectory = "";
                    }
                }
                return m_BaseDirectory;
            }
        }

        public static bool Closing
        {
            get
            {
                return m_Closing;
            }
            set
            {
                m_Closing = value;
            }
        }

        public static ArrayList DataDirectories
        {
            get
            {
                return m_DataDirectories;
            }
        }

        public static string ExePath
        {
            get
            {
                if (m_ExePath == null)
                {
                    m_ExePath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                }
                return m_ExePath;
            }
        }

        public static int GlobalMaxUpdateRange
        {
            get
            {
                return m_GlobalMaxUpdateRange;
            }
            set
            {
                m_GlobalMaxUpdateRange = value;
            }
        }

        public static Server.Network.MessagePump MessagePump
        {
            get
            {
                return m_MessagePump;
            }
            set
            {
                m_MessagePump = value;
            }
        }

        public static MultiTextWriter MultiConsoleOut
        {
            get
            {
                return m_MultiConOut;
            }
        }

        public static System.Diagnostics.Process Process
        {
            get
            {
                return m_Process;
            }
        }

        public static TimeSpan ProfileTime
        {
            get
            {
                if (m_ProfileStart > DateTime.MinValue)
                {
                    return (m_ProfileTime + (DateTime.Now - m_ProfileStart));
                }
                return m_ProfileTime;
            }
        }

        public static bool Profiling
        {
            get
            {
                return m_Profiling;
            }
            set
            {
                if (m_Profiling != value)
                {
                    m_Profiling = value;
                    if (m_ProfileStart > DateTime.MinValue)
                    {
                        m_ProfileTime += DateTime.Now - m_ProfileStart;
                    }
                    m_ProfileStart = m_Profiling ? DateTime.Now : DateTime.MinValue;
                }
            }
        }

        public static int ScriptItems
        {
            get
            {
                return m_ItemCount;
            }
        }

        public static int ScriptMobiles
        {
            get
            {
                return m_MobileCount;
            }
        }

        public static bool Service
        {
            get
            {
                return m_Service;
            }
        }

        public static System.Threading.Thread Thread
        {
            get
            {
                return m_Thread;
            }
        }
    }
}

