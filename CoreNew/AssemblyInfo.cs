﻿using System.Reflection;
// Assembly RunUO, Version 1.0.0.0

[assembly: System.Reflection.AssemblyVersion("1.0.0.0")]
[assembly: System.Reflection.AssemblyKeyFile("")]
[assembly: System.Reflection.AssemblyTrademark("RunUO Software Team")]
[assembly: System.Reflection.AssemblyKeyName("")]
[assembly: System.Reflection.AssemblyDelaySign(false)]
[assembly: System.Reflection.AssemblyCompany("RunUO Software Team")]
[assembly: System.Reflection.AssemblyTitle("RunUO Server")]
[assembly: System.Reflection.AssemblyProduct("RunUO")]
[assembly: System.Reflection.AssemblyCopyright("RunUO Software Team")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyDescription("Ultima Online Server Software")]
[assembly: System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.RequestMinimum, SkipVerification=true)]

[assembly: AssemblyFileVersionAttribute("1.0.0.0")]
