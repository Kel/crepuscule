﻿namespace Server
{
    using Server.Network;
    using System;
    using System.Collections;

    public class Sector
    {
        private bool m_Active;
        private ArrayList m_Clients;
        private static ArrayList m_DefaultList = new ArrayList();
        private ArrayList m_Items;
        private ArrayList m_Mobiles;
        private ArrayList m_Multis;
        private Map m_Owner;
        private ArrayList m_Players;
        private ArrayList m_Regions;
        private int m_X;
        private int m_Y;

        public Sector(int x, int y, Map owner)
        {
            this.m_X = x;
            this.m_Y = y;
            this.m_Owner = owner;
            this.m_Active = false;
        }

        public void Activate()
        {
            if (!this.Active && (this.m_Owner != Map.Internal))
            {
                for (int i = 0; (this.m_Items != null) && (i < this.m_Items.Count); i++)
                {
                    ((Item) this.m_Items[i]).OnSectorActivate();
                }
                for (int j = 0; (this.m_Mobiles != null) && (j < this.m_Mobiles.Count); j++)
                {
                    Mobile mobile = (Mobile) this.m_Mobiles[j];
                    if (!mobile.Player)
                    {
                        mobile.OnSectorActivate();
                    }
                }
                this.m_Active = true;
            }
        }

        public void Deactivate()
        {
            if (this.Active && ((this.m_Players == null) || (this.m_Players.Count == 0)))
            {
                for (int i = 0; (this.m_Items != null) && (i < this.m_Items.Count); i++)
                {
                    ((Item) this.m_Items[i]).OnSectorDeactivate();
                }
                for (int j = 0; (this.m_Mobiles != null) && (j < this.m_Mobiles.Count); j++)
                {
                    ((Mobile) this.m_Mobiles[j]).OnSectorDeactivate();
                }
                this.m_Active = false;
            }
        }

        public void OnClientChange(NetState oldState, NetState newState)
        {
            if (this.m_Clients != null)
            {
                this.m_Clients.Remove(oldState);
            }
            if (newState != null)
            {
                if (this.m_Clients == null)
                {
                    this.m_Clients = new ArrayList(4);
                }
                this.m_Clients.Add(newState);
            }
        }

        public void OnEnter(Item item)
        {
            if (this.m_Items == null)
            {
                this.m_Items = new ArrayList();
            }
            this.m_Items.Add(item);
        }

        public void OnEnter(Mobile m)
        {
            if (this.m_Mobiles == null)
            {
                this.m_Mobiles = new ArrayList(4);
            }
            this.m_Mobiles.Add(m);
            if (m.NetState != null)
            {
                if (this.m_Clients == null)
                {
                    this.m_Clients = new ArrayList(4);
                }
                this.m_Clients.Add(m.NetState);
            }
            if (m.Player)
            {
                if (this.m_Players == null)
                {
                    this.m_Players = new ArrayList(4);
                }
                this.m_Players.Add(m);
                if (this.m_Players.Count == 1)
                {
                    this.Owner.ActivateSectors(this.m_X, this.m_Y);
                }
            }
        }

        public void OnEnter(Region r)
        {
            if ((this.m_Regions == null) || !this.m_Regions.Contains(r))
            {
                if (this.m_Regions == null)
                {
                    this.m_Regions = new ArrayList();
                }
                this.m_Regions.Add(r);
                this.m_Regions.Sort();
                if ((this.m_Mobiles != null) && (this.m_Mobiles.Count > 0))
                {
                    ArrayList list = new ArrayList(this.m_Mobiles);
                    for (int i = 0; i < list.Count; i++)
                    {
                        ((Mobile) list[i]).ForceRegionReEnter(true);
                    }
                }
            }
        }

        public void OnLeave(Item item)
        {
            if (this.m_Items != null)
            {
                this.m_Items.Remove(item);
            }
        }

        public void OnLeave(Mobile m)
        {
            if (this.m_Mobiles != null)
            {
                this.m_Mobiles.Remove(m);
            }
            if ((this.m_Clients != null) && (m.NetState != null))
            {
                this.m_Clients.Remove(m.NetState);
            }
            if (m.Player)
            {
                if (this.m_Players != null)
                {
                    this.m_Players.Remove(m);
                }
                if ((this.m_Players == null) || (this.m_Players.Count == 0))
                {
                    this.Owner.DeactivateSectors(this.m_X, this.m_Y);
                }
            }
        }

        public void OnLeave(Region r)
        {
            if (this.m_Regions != null)
            {
                this.m_Regions.Remove(r);
            }
        }

        public void OnMultiEnter(Item item)
        {
            if (this.m_Multis == null)
            {
                this.m_Multis = new ArrayList(4);
            }
            this.m_Multis.Add(item);
        }

        public void OnMultiLeave(Item item)
        {
            if (this.m_Multis != null)
            {
                this.m_Multis.Remove(item);
            }
        }

        public bool Active
        {
            get
            {
                return (this.m_Active && (this.m_Owner != Map.Internal));
            }
        }

        public ArrayList Clients
        {
            get
            {
                if (this.m_Clients == null)
                {
                    return m_DefaultList;
                }
                return this.m_Clients;
            }
        }

        public static ArrayList EmptyList
        {
            get
            {
                return m_DefaultList;
            }
        }

        public ArrayList Items
        {
            get
            {
                if (this.m_Items == null)
                {
                    return m_DefaultList;
                }
                return this.m_Items;
            }
        }

        public ArrayList Mobiles
        {
            get
            {
                if (this.m_Mobiles == null)
                {
                    return m_DefaultList;
                }
                return this.m_Mobiles;
            }
        }

        public ArrayList Multis
        {
            get
            {
                if (this.m_Multis == null)
                {
                    return m_DefaultList;
                }
                return this.m_Multis;
            }
        }

        public Map Owner
        {
            get
            {
                return this.m_Owner;
            }
        }

        public ArrayList Players
        {
            get
            {
                if (this.m_Players == null)
                {
                    return m_DefaultList;
                }
                return this.m_Players;
            }
        }

        public ArrayList Regions
        {
            get
            {
                if (this.m_Regions == null)
                {
                    return m_DefaultList;
                }
                return this.m_Regions;
            }
        }

        public int X
        {
            get
            {
                return this.m_X;
            }
        }

        public int Y
        {
            get
            {
                return this.m_Y;
            }
        }
    }
}

