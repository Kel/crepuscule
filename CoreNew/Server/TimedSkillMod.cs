﻿namespace Server
{
    using System;

    public class TimedSkillMod : SkillMod
    {
        private DateTime m_Expire;

        public TimedSkillMod(SkillName skill, bool relative, double value, DateTime expire) : base(skill, relative, value)
        {
            this.m_Expire = expire;
        }

        public TimedSkillMod(SkillName skill, bool relative, double value, TimeSpan delay) : this(skill, relative, value, DateTime.Now + delay)
        {
        }

        public override bool CheckCondition()
        {
            return (DateTime.Now < this.m_Expire);
        }
    }
}

