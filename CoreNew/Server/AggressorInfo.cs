﻿namespace Server
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.IO;

    public class AggressorInfo
    {
        private Mobile m_Attacker;
        private bool m_CanReportMurder;
        private bool m_CriminalAggression;
        private Mobile m_Defender;
        private static TimeSpan m_ExpireDelay = TimeSpan.FromMinutes(2.0);
        private DateTime m_LastCombatTime;
        private static Queue m_Pool = new Queue();
        private bool m_Queued;
        private bool m_Reported;

        private AggressorInfo(Mobile attacker, Mobile defender, bool criminal)
        {
            this.m_Attacker = attacker;
            this.m_Defender = defender;
            this.m_CanReportMurder = criminal;
            this.m_CriminalAggression = criminal;
            this.Refresh();
        }

        public static AggressorInfo Create(Mobile attacker, Mobile defender, bool criminal)
        {
            if (m_Pool.Count > 0)
            {
                AggressorInfo info = (AggressorInfo) m_Pool.Dequeue();
                info.m_Attacker = attacker;
                info.m_Defender = defender;
                info.m_CanReportMurder = criminal;
                info.m_CriminalAggression = criminal;
                info.m_Queued = false;
                info.Refresh();
                return info;
            }
            return new AggressorInfo(attacker, defender, criminal);
        }

        public static void DumpAccess()
        {
            using (StreamWriter writer = new StreamWriter("warnings.log", true))
            {
                writer.WriteLine("Warning: Access to queued AggressorInfo:");
                writer.WriteLine(new StackTrace());
                writer.WriteLine();
                writer.WriteLine();
            }
        }

        public void Free()
        {
            if (!this.m_Queued)
            {
                this.m_Queued = true;
                m_Pool.Enqueue(this);
            }
        }

        public void Refresh()
        {
            if (this.m_Queued)
            {
                DumpAccess();
            }
            this.m_LastCombatTime = DateTime.Now;
            this.m_Reported = false;
        }

        public Mobile Attacker
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                return this.m_Attacker;
            }
        }

        public bool CanReportMurder
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                return this.m_CanReportMurder;
            }
            set
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                this.m_CanReportMurder = value;
            }
        }

        public bool CriminalAggression
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                return this.m_CriminalAggression;
            }
            set
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                this.m_CriminalAggression = value;
            }
        }

        public Mobile Defender
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                return this.m_Defender;
            }
        }

        public bool Expired
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                if (!this.m_Attacker.Deleted && !this.m_Defender.Deleted)
                {
                    return (DateTime.Now >= (this.m_LastCombatTime + m_ExpireDelay));
                }
                return true;
            }
        }

        public static TimeSpan ExpireDelay
        {
            get
            {
                return m_ExpireDelay;
            }
            set
            {
                m_ExpireDelay = value;
            }
        }

        public DateTime LastCombatTime
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                return this.m_LastCombatTime;
            }
        }

        public bool Reported
        {
            get
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                return this.m_Reported;
            }
            set
            {
                if (this.m_Queued)
                {
                    DumpAccess();
                }
                this.m_Reported = value;
            }
        }
    }
}

