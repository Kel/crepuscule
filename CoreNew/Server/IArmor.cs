﻿namespace Server
{
    using System;

    public interface IArmor
    {
        double ArmorRating { get; }
    }
}

