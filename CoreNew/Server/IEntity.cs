﻿namespace Server
{
    public interface IEntity : IPoint3D, IPoint2D
    {
        Point3D Location { get; }

        Server.Map Map { get; }

        Server.Serial Serial { get; }
    }
}

