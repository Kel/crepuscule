﻿namespace Server
{
    using System;
    using System.IO;
    using System.Text;

    public class FileLogger : TextWriter, IDisposable
    {
        public const string DateFormat = "[MMMM dd hh:mm:ss.f tt]: ";
        private string m_FileName;
        private bool m_NewLine;

        public FileLogger(string file) : this(file, false)
        {
        }

        public FileLogger(string file, bool append)
        {
            this.m_FileName = file;
            using (StreamWriter writer = new StreamWriter(new FileStream(this.m_FileName, append ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.Read)))
            {
                writer.WriteLine(">>>Logging started on {0}.", DateTime.Now.ToString("f"));
            }
            this.m_NewLine = true;
        }

        public override void Write(char ch)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(this.m_FileName, FileMode.Append, FileAccess.Write, FileShare.Read)))
            {
                if (this.m_NewLine)
                {
                    writer.Write(DateTime.Now.ToString("[MMMM dd hh:mm:ss.f tt]: "));
                    this.m_NewLine = false;
                }
                writer.Write(ch);
            }
        }

        public override void Write(string str)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(this.m_FileName, FileMode.Append, FileAccess.Write, FileShare.Read)))
            {
                if (this.m_NewLine)
                {
                    writer.Write(DateTime.Now.ToString("[MMMM dd hh:mm:ss.f tt]: "));
                    this.m_NewLine = false;
                }
                writer.Write(str);
            }
        }

        public override void WriteLine(string line)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(this.m_FileName, FileMode.Append, FileAccess.Write, FileShare.Read)))
            {
                if (this.m_NewLine)
                {
                    writer.Write(DateTime.Now.ToString("[MMMM dd hh:mm:ss.f tt]: "));
                }
                writer.WriteLine(line);
                this.m_NewLine = true;
            }
        }

        public override System.Text.Encoding Encoding
        {
            get
            {
                return System.Text.Encoding.Default;
            }
        }

        public string FileName
        {
            get
            {
                return this.m_FileName;
            }
        }
    }
}

