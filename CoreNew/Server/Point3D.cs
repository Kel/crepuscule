﻿namespace Server
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential), Parsable]
    public struct Point3D : IPoint3D, IPoint2D
    {
        internal int m_X;
        internal int m_Y;
        internal int m_Z;
        public static readonly Point3D Zero;
        public Point3D(int x, int y, int z)
        {
            this.m_X = x;
            this.m_Y = y;
            this.m_Z = z;
        }

        public Point3D(IPoint3D p) : this(p.X, p.Y, p.Z)
        {
        }

        public Point3D(IPoint2D p, int z) : this(p.X, p.Y, z)
        {
        }

        [CommandProperty(AccessLevel.Counselor)]
        public int X
        {
            get
            {
                return this.m_X;
            }
            set
            {
                this.m_X = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int Y
        {
            get
            {
                return this.m_Y;
            }
            set
            {
                this.m_Y = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int Z
        {
            get
            {
                return this.m_Z;
            }
            set
            {
                this.m_Z = value;
            }
        }
        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", this.m_X, this.m_Y, this.m_Z);
        }

        public override bool Equals(object o)
        {
            if ((o == null) || !(o is IPoint3D))
            {
                return false;
            }
            IPoint3D pointd = (IPoint3D) o;
            return (((this.m_X == pointd.X) && (this.m_Y == pointd.Y)) && (this.m_Z == pointd.Z));
        }

        public override int GetHashCode()
        {
            return ((this.m_X ^ this.m_Y) ^ this.m_Z);
        }

        public static Point3D Parse(string value)
        {
            int index = value.IndexOf('(');
            int num2 = value.IndexOf(',', index + 1);
            string str = value.Substring(index + 1, num2 - (index + 1)).Trim();
            index = num2;
            num2 = value.IndexOf(',', index + 1);
            string str2 = value.Substring(index + 1, num2 - (index + 1)).Trim();
            index = num2;
            num2 = value.IndexOf(')', index + 1);
            string str3 = value.Substring(index + 1, num2 - (index + 1)).Trim();
            return new Point3D(Convert.ToInt32(str), Convert.ToInt32(str2), Convert.ToInt32(str3));
        }

        public static bool operator ==(Point3D l, Point3D r)
        {
            return (((l.m_X == r.m_X) && (l.m_Y == r.m_Y)) && (l.m_Z == r.m_Z));
        }

        public static bool operator !=(Point3D l, Point3D r)
        {
            if ((l.m_X == r.m_X) && (l.m_Y == r.m_Y))
            {
                return (l.m_Z != r.m_Z);
            }
            return true;
        }

        public static bool operator ==(Point3D l, IPoint3D r)
        {
            return (((l.m_X == r.X) && (l.m_Y == r.Y)) && (l.m_Z == r.Z));
        }

        public static bool operator !=(Point3D l, IPoint3D r)
        {
            if ((l.m_X == r.X) && (l.m_Y == r.Y))
            {
                return (l.m_Z != r.Z);
            }
            return true;
        }

        static Point3D()
        {
            Zero = new Point3D(0, 0, 0);
        }
    }
}

