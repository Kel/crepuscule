﻿namespace Server
{
    using System;
    using System.Collections;
    using System.Reflection;

    public class CallPriorityComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            MethodInfo mi = x as MethodInfo;
            MethodInfo info2 = y as MethodInfo;
            if ((mi == null) && (info2 == null))
            {
                return 0;
            }
            if (mi == null)
            {
                return 1;
            }
            if (info2 == null)
            {
                return -1;
            }
            return (this.GetPriority(mi) - this.GetPriority(info2));
        }

        private int GetPriority(MethodInfo mi)
        {
            object[] customAttributes = mi.GetCustomAttributes(typeof(CallPriorityAttribute), true);
            if (customAttributes == null)
            {
                return 0;
            }
            if (customAttributes.Length == 0)
            {
                return 0;
            }
            CallPriorityAttribute attribute = customAttributes[0] as CallPriorityAttribute;
            if (attribute == null)
            {
                return 0;
            }
            return attribute.Priority;
        }
    }
}

