﻿namespace Server
{
    using System;

    public enum ApplyPoisonResult
    {
        Poisoned,
        Immune,
        HigherPoisonActive,
        Cured
    }
}

