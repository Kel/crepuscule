﻿namespace Server
{
    using System;
    using System.Collections;

    [PropertyObject]
    public class Skill
    {
        private ushort m_Base;
        private ushort m_Cap;
        private SkillInfo m_Info;
        private SkillLock m_Lock;
        private Skills m_Owner;
        private static bool m_UseStatMods;

        public Skill(Skills owner, SkillInfo info, GenericReader reader)
        {
            this.m_Owner = owner;
            this.m_Info = info;
            int num = reader.ReadByte();
            switch (num)
            {
                case 0:
                    this.m_Base = reader.ReadUShort();
                    this.m_Cap = reader.ReadUShort();
                    this.m_Lock = (SkillLock) reader.ReadByte();
                    return;

                case 0xff:
                    this.m_Base = 0;
                    this.m_Cap = 0x3e8;
                    this.m_Lock = SkillLock.Up;
                    return;
            }
            if ((num & 0xc0) == 0)
            {
                if ((num & 1) != 0)
                {
                    this.m_Base = reader.ReadUShort();
                }
                if ((num & 2) != 0)
                {
                    this.m_Cap = reader.ReadUShort();
                }
                else
                {
                    this.m_Cap = 0x3e8;
                }
                if ((num & 4) != 0)
                {
                    this.m_Lock = (SkillLock) reader.ReadByte();
                }
            }
        }

        public Skill(Skills owner, SkillInfo info, int baseValue, int cap, SkillLock skillLock)
        {
            this.m_Owner = owner;
            this.m_Info = info;
            this.m_Base = (ushort) baseValue;
            this.m_Cap = (ushort) cap;
            this.m_Lock = skillLock;
        }

        public void Serialize(GenericWriter writer)
        {
            if (((this.m_Base == 0) && (this.m_Cap == 0x3e8)) && (this.m_Lock == SkillLock.Up))
            {
                writer.Write((byte) 0xff);
            }
            else
            {
                int num = 0;
                if (this.m_Base != 0)
                {
                    num |= 1;
                }
                if (this.m_Cap != 0x3e8)
                {
                    num |= 2;
                }
                if (this.m_Lock != SkillLock.Up)
                {
                    num |= 4;
                }
                writer.Write((byte) num);
                if (this.m_Base != 0)
                {
                    writer.Write((short) this.m_Base);
                }
                if (this.m_Cap != 0x3e8)
                {
                    writer.Write((short) this.m_Cap);
                }
                if (this.m_Lock != SkillLock.Up)
                {
                    writer.Write((byte) this.m_Lock);
                }
            }
        }

        public void SetLockNoRelay(SkillLock skillLock)
        {
            this.m_Lock = skillLock;
        }

        public override string ToString()
        {
            return string.Format("[{0}: {1}]", this.Name, this.Base);
        }

        public void Update()
        {
            this.m_Owner.OnSkillChange(this);
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public double Base
        {
            get
            {
                return (((double) this.m_Base) / 10.0);
            }
            set
            {
                this.BaseFixedPoint = (int) (value * 10.0);
            }
        }

        public int BaseFixedPoint
        {
            get
            {
                return this.m_Base;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                else if (value >= 0x10000)
                {
                    value = 0xffff;
                }
                ushort num = (ushort) value;
                int num2 = this.m_Base;
                if (this.m_Base != num)
                {
                    this.m_Owner.Total = (this.m_Owner.Total - this.m_Base) + num;
                    this.m_Base = num;
                    this.m_Owner.OnSkillChange(this);
                    Mobile owner = this.m_Owner.Owner;
                    if (owner != null)
                    {
                        owner.OnSkillChange(this.SkillName, ((double) num2) / 10.0);
                    }
                }
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public double Cap
        {
            get
            {
                return (((double) this.m_Cap) / 10.0);
            }
            set
            {
                this.CapFixedPoint = (int) (value * 10.0);
            }
        }

        public int CapFixedPoint
        {
            get
            {
                return this.m_Cap;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                else if (value >= 0x10000)
                {
                    value = 0xffff;
                }
                ushort num = (ushort) value;
                if (this.m_Cap != num)
                {
                    this.m_Cap = num;
                    this.m_Owner.OnSkillChange(this);
                }
            }
        }

        public int Fixed
        {
            get
            {
                return (int) (this.Value * 10.0);
            }
        }

        public SkillInfo Info
        {
            get
            {
                return this.m_Info;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public SkillLock Lock
        {
            get
            {
                return this.m_Lock;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public string Name
        {
            get
            {
                return this.m_Info.Name;
            }
        }

        public Skills Owner
        {
            get
            {
                return this.m_Owner;
            }
        }

        public int SkillID
        {
            get
            {
                return this.m_Info.SkillID;
            }
        }

        public Server.SkillName SkillName
        {
            get
            {
                return (Server.SkillName) this.m_Info.SkillID;
            }
        }

        public static bool UseStatMods
        {
            get
            {
                return m_UseStatMods;
            }
            set
            {
                m_UseStatMods = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public double Value
        {
            get
            {
                double num = this.Base;
                double num2 = 100.0 - num;
                if (num2 < 0.0)
                {
                    num2 = 0.0;
                }
                num2 /= 100.0;
                double num3 = (((m_UseStatMods ? ((double) this.m_Owner.Owner.Str) : ((double) this.m_Owner.Owner.RawStr)) * this.m_Info.StrScale) + ((m_UseStatMods ? ((double) this.m_Owner.Owner.Dex) : ((double) this.m_Owner.Owner.RawDex)) * this.m_Info.DexScale)) + ((m_UseStatMods ? ((double) this.m_Owner.Owner.Int) : ((double) this.m_Owner.Owner.RawInt)) * this.m_Info.IntScale);
                double num4 = this.m_Info.StatTotal * num2;
                num3 *= num2;
                if (num3 > num4)
                {
                    num3 = num4;
                }
                double cap = num + num3;
                this.m_Owner.Owner.ValidateSkillMods();
                ArrayList skillMods = this.m_Owner.Owner.SkillMods;
                double num6 = 0.0;
                double num7 = 0.0;
                for (int i = 0; i < skillMods.Count; i++)
                {
                    SkillMod mod = (SkillMod) skillMods[i];
                    if ((int)mod.Skill == this.m_Info.SkillID)
                    {
                        if (mod.Relative)
                        {
                            if (mod.ObeyCap)
                            {
                                num6 += mod.Value;
                            }
                            else
                            {
                                num7 += mod.Value;
                            }
                        }
                        else
                        {
                            num6 = 0.0;
                            num7 = 0.0;
                            cap = mod.Value;
                        }
                    }
                }
                cap += num7;
                if (cap < this.Cap)
                {
                    cap += num6;
                    if (cap > this.Cap)
                    {
                        cap = this.Cap;
                    }
                }
                return cap;
            }
        }
    }
}

