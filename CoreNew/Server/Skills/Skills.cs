﻿namespace Server
{
    using Server.Network;
    using System;
    using System.Reflection;

    [PropertyObject]
    public class Skills
    {
        private int m_Cap;
        private Skill m_Highest;
        private Mobile m_Owner;
        private Skill[] m_Skills;
        private int m_Total;

        public Skills(Mobile owner)
        {
            this.m_Owner = owner;
            this.m_Cap = 0x1b58;
            SkillInfo[] table = SkillInfo.Table;
            this.m_Skills = new Skill[table.Length];
        }

        public Skills(Mobile owner, GenericReader reader)
        {
            this.m_Owner = owner;
            int num = reader.ReadInt();
            switch (num)
            {
                case 0:
                    reader.ReadInt();
                    break;

                case 1:
                    break;

                case 2:
                case 3:
                    this.m_Cap = reader.ReadInt();
                    break;

                default:
                    return;
            }
            if (num < 2)
            {
                this.m_Cap = 0x1b58;
            }
            if (num < 3)
            {
                reader.ReadInt();
            }
            SkillInfo[] table = SkillInfo.Table;
            this.m_Skills = new Skill[table.Length];
            int num2 = reader.ReadInt();
            for (int i = 0; i < num2; i++)
            {
                if (i < table.Length)
                {
                    Skill skill = new Skill(this, table[i], reader);
                    if (((skill.BaseFixedPoint != 0) || (skill.CapFixedPoint != 0x3e8)) || (skill.Lock != SkillLock.Up))
                    {
                        this.m_Skills[i] = skill;
                        this.m_Total += skill.BaseFixedPoint;
                    }
                }
                else
                {
                    new Skill(this, null, reader);
                }
            }
        }

        public void OnSkillChange(Skill skill)
        {
            if (skill == this.m_Highest)
            {
                this.m_Highest = null;
            }
            else if ((this.m_Highest != null) && (skill.BaseFixedPoint > this.m_Highest.BaseFixedPoint))
            {
                this.m_Highest = skill;
            }
            this.m_Owner.OnSkillInvalidated(skill);
            NetState netState = this.m_Owner.NetState;
            if (netState != null)
            {
                netState.Send(new SkillChange(skill));
            }
        }

        public void Serialize(GenericWriter writer)
        {
            this.m_Total = 0;
            writer.Write(3);
            writer.Write(this.m_Cap);
            writer.Write(this.m_Skills.Length);
            for (int i = 0; i < this.m_Skills.Length; i++)
            {
                Skill skill = this.m_Skills[i];
                if (skill == null)
                {
                    writer.Write((byte) 0xff);
                }
                else
                {
                    skill.Serialize(writer);
                    this.m_Total += skill.BaseFixedPoint;
                }
            }
        }

        public override string ToString()
        {
            return "...";
        }

        public static bool UseSkill(Mobile from, SkillName name)
        {
            return UseSkill(from, (int) name);
        }

        public static bool UseSkill(Mobile from, int skillID)
        {
            if (from.CheckAlive())
            {
                if (!from.Region.OnSkillUse(from, skillID))
                {
                    return false;
                }
                if (!from.AllowSkillUse((SkillName) skillID))
                {
                    return false;
                }
                if ((skillID >= 0) && (skillID < SkillInfo.Table.Length))
                {
                    SkillInfo info = SkillInfo.Table[skillID];
                    if (info.Callback != null)
                    {
                        if ((from.NextSkillTime <= DateTime.Now) && (from.Spell == null))
                        {
                            from.DisruptiveAction();
                            from.NextSkillTime = DateTime.Now + info.Callback(from);
                            return true;
                        }
                        from.SendSkillMessage();
                    }
                    else
                    {
                        from.SendLocalizedMessage(0x7a12e);
                    }
                }
            }
            return false;
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Alchemy
        {
            get
            {
                return this[SkillName.Alchemy];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Anatomy
        {
            get
            {
                return this[SkillName.Anatomy];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill AnimalLore
        {
            get
            {
                return this[SkillName.AnimalLore];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill AnimalTaming
        {
            get
            {
                return this[SkillName.AnimalTaming];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Archery
        {
            get
            {
                return this[SkillName.Archery];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill ArmsLore
        {
            get
            {
                return this[SkillName.ArmsLore];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Begging
        {
            get
            {
                return this[SkillName.Begging];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Blacksmith
        {
            get
            {
                return this[SkillName.Blacksmith];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Camping
        {
            get
            {
                return this[SkillName.Camping];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Cap
        {
            get
            {
                return this.m_Cap;
            }
            set
            {
                this.m_Cap = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Carpentry
        {
            get
            {
                return this[SkillName.Carpentry];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Cartography
        {
            get
            {
                return this[SkillName.Cartography];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Chivalry
        {
            get
            {
                return this[SkillName.Chivalry];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Cooking
        {
            get
            {
                return this[SkillName.Cooking];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill DetectHidden
        {
            get
            {
                return this[SkillName.DetectHidden];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Discordance
        {
            get
            {
                return this[SkillName.Discordance];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill EvalInt
        {
            get
            {
                return this[SkillName.EvalInt];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Fencing
        {
            get
            {
                return this[SkillName.Fencing];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Fishing
        {
            get
            {
                return this[SkillName.Fishing];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Fletching
        {
            get
            {
                return this[SkillName.Fletching];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Focus
        {
            get
            {
                return this[SkillName.Focus];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Forensics
        {
            get
            {
                return this[SkillName.Forensics];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Healing
        {
            get
            {
                return this[SkillName.Healing];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Herding
        {
            get
            {
                return this[SkillName.Herding];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Hiding
        {
            get
            {
                return this[SkillName.Hiding];
            }
            set
            {
            }
        }

        public Skill Highest
        {
            get
            {
                if (this.m_Highest == null)
                {
                    Skill skill = null;
                    int baseFixedPoint = -2147483648;
                    for (int i = 0; i < this.m_Skills.Length; i++)
                    {
                        Skill skill2 = this.m_Skills[i];
                        if ((skill2 != null) && (skill2.BaseFixedPoint > baseFixedPoint))
                        {
                            baseFixedPoint = skill2.BaseFixedPoint;
                            skill = skill2;
                        }
                    }
                    if ((skill == null) && (this.m_Skills.Length > 0))
                    {
                        skill = this[0];
                    }
                    this.m_Highest = skill;
                }
                return this.m_Highest;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Inscribe
        {
            get
            {
                return this[SkillName.Inscribe];
            }
            set
            {
            }
        }

        public Skill this[SkillName name]
        {
            get
            {
                return this[(int) name];
            }
        }

        public Skill this[int skillID]
        {
            get
            {
                if ((skillID < 0) || (skillID >= this.m_Skills.Length))
                {
                    return null;
                }
                Skill skill = this.m_Skills[skillID];
                if (skill == null)
                {
                    this.m_Skills[skillID] = skill = new Skill(this, SkillInfo.Table[skillID], 0, 0x3e8, SkillLock.Up);
                }
                return skill;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill ItemID
        {
            get
            {
                return this[SkillName.ItemID];
            }
            set
            {
            }
        }

        public int Length
        {
            get
            {
                return this.m_Skills.Length;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Lockpicking
        {
            get
            {
                return this[SkillName.Lockpicking];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Lumberjacking
        {
            get
            {
                return this[SkillName.Lumberjacking];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Macing
        {
            get
            {
                return this[SkillName.Macing];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Magery
        {
            get
            {
                return this[SkillName.Magery];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill MagicResist
        {
            get
            {
                return this[SkillName.MagicResist];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Meditation
        {
            get
            {
                return this[SkillName.Meditation];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Mining
        {
            get
            {
                return this[SkillName.Mining];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Musicianship
        {
            get
            {
                return this[SkillName.Musicianship];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Necromancy
        {
            get
            {
                return this[SkillName.Necromancy];
            }
            set
            {
            }
        }

        public Mobile Owner
        {
            get
            {
                return this.m_Owner;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Parry
        {
            get
            {
                return this[SkillName.Parry];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Peacemaking
        {
            get
            {
                return this[SkillName.Peacemaking];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Poisoning
        {
            get
            {
                return this[SkillName.Poisoning];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Provocation
        {
            get
            {
                return this[SkillName.Provocation];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill RemoveTrap
        {
            get
            {
                return this[SkillName.RemoveTrap];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Snooping
        {
            get
            {
                return this[SkillName.Snooping];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill SpiritSpeak
        {
            get
            {
                return this[SkillName.SpiritSpeak];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Stealing
        {
            get
            {
                return this[SkillName.Stealing];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Stealth
        {
            get
            {
                return this[SkillName.Stealth];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Swords
        {
            get
            {
                return this[SkillName.Swords];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Tactics
        {
            get
            {
                return this[SkillName.Tactics];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Tailoring
        {
            get
            {
                return this[SkillName.Tailoring];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill TasteID
        {
            get
            {
                return this[SkillName.TasteID];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Tinkering
        {
            get
            {
                return this[SkillName.Tinkering];
            }
            set
            {
            }
        }

        public int Total
        {
            get
            {
                return this.m_Total;
            }
            set
            {
                this.m_Total = value;
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Tracking
        {
            get
            {
                return this[SkillName.Tracking];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Veterinary
        {
            get
            {
                return this[SkillName.Veterinary];
            }
            set
            {
            }
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Skill Wrestling
        {
            get
            {
                return this[SkillName.Wrestling];
            }
            set
            {
            }
        }
    }
}

