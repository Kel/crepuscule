﻿namespace Server
{
    using System;

    public class SkillInfo
    {
        private SkillUseCallback m_Callback;
        private double m_DexGain;
        private double m_DexScale;
        private double m_GainFactor;
        private double m_IntGain;
        private double m_IntScale;
        private string m_Name;
        private int m_SkillID;
        private double m_StatTotal;
        private double m_StrGain;
        private double m_StrScale;
        private static SkillInfo[] m_Table = new SkillInfo[] { 
            new SkillInfo(0, "Alchemy", 0.0, 5.0, 5.0, "Alchemist", null, 0.0, 0.5, 0.5, 1.0), new SkillInfo(1, "Anatomy", 0.0, 0.0, 0.0, "Healer", null, 0.15, 0.15, 0.7, 1.0), new SkillInfo(2, "Animal Lore", 0.0, 0.0, 0.0, "Ranger", null, 0.0, 0.0, 1.0, 1.0), new SkillInfo(3, "Item Identification", 0.0, 0.0, 0.0, "Merchant", null, 0.0, 0.0, 1.0, 1.0), new SkillInfo(4, "Arms Lore", 0.0, 0.0, 0.0, "Warrior", null, 0.75, 0.15, 0.1, 1.0), new SkillInfo(5, "Parrying", 7.5, 2.5, 0.0, "Warrior", null, 0.75, 0.25, 0.0, 1.0), new SkillInfo(6, "Begging", 0.0, 0.0, 0.0, "Beggar", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(7, "Blacksmithy", 10.0, 0.0, 0.0, "Smith", null, 1.0, 0.0, 0.0, 1.0), new SkillInfo(8, "Bowcraft/Fletching", 6.0, 16.0, 0.0, "Bowyer", null, 0.6, 1.6, 0.0, 1.0), new SkillInfo(9, "Peacemaking", 0.0, 0.0, 0.0, "Bard", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(10, "Camping", 20.0, 15.0, 15.0, "Ranger", null, 2.0, 1.5, 1.5, 1.0), new SkillInfo(11, "Carpentry", 20.0, 5.0, 0.0, "Carpenter", null, 2.0, 0.5, 0.0, 1.0), new SkillInfo(12, "Cartography", 0.0, 7.5, 7.5, "Cartographer", null, 0.0, 0.75, 0.75, 1.0), new SkillInfo(13, "Cooking", 0.0, 20.0, 30.0, "Chef", null, 0.0, 2.0, 3.0, 1.0), new SkillInfo(14, "Detecting Hidden", 0.0, 0.0, 0.0, "Scout", null, 0.0, 0.4, 0.6, 1.0), new SkillInfo(15, "Discordance", 0.0, 2.5, 2.5, "Bard", null, 0.0, 0.25, 0.25, 1.0), 
            new SkillInfo(0x10, "Evaluating Intelligence", 0.0, 0.0, 0.0, "Scholar", null, 0.0, 0.0, 1.0, 1.0), new SkillInfo(0x11, "Healing", 6.0, 6.0, 8.0, "Healer", null, 0.6, 0.6, 0.8, 1.0), new SkillInfo(0x12, "Fishing", 0.0, 0.0, 0.0, "Fisherman", null, 0.5, 0.5, 0.0, 1.0), new SkillInfo(0x13, "Forensic Evaluation", 0.0, 0.0, 0.0, "Detective", null, 0.0, 0.2, 0.8, 1.0), new SkillInfo(20, "Herding", 16.25, 6.25, 2.5, "Shepherd", null, 1.625, 0.625, 0.25, 1.0), new SkillInfo(0x15, "Hiding", 0.0, 0.0, 0.0, "Rogue", null, 0.0, 0.8, 0.2, 1.0), new SkillInfo(0x16, "Provocation", 0.0, 4.5, 0.5, "Bard", null, 0.0, 0.45, 0.05, 1.0), new SkillInfo(0x17, "Inscription", 0.0, 2.0, 8.0, "Scribe", null, 0.0, 0.2, 0.8, 1.0), new SkillInfo(0x18, "Lockpicking", 0.0, 25.0, 0.0, "Rogue", null, 0.0, 2.0, 0.0, 1.0), new SkillInfo(0x19, "Magery", 0.0, 0.0, 15.0, "Mage", null, 0.0, 0.0, 1.5, 1.0), new SkillInfo(0x1a, "Resisting Spells", 0.0, 0.0, 0.0, "Mage", null, 0.25, 0.25, 0.5, 1.0), new SkillInfo(0x1b, "Tactics", 0.0, 0.0, 0.0, "Warrior", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(0x1c, "Snooping", 0.0, 25.0, 0.0, "Pickpocket", null, 0.0, 2.5, 0.0, 1.0), new SkillInfo(0x1d, "Musicianship", 0.0, 0.0, 0.0, "Bard", null, 0.0, 0.8, 0.2, 1.0), new SkillInfo(30, "Poisoning", 0.0, 4.0, 16.0, "Assassin", null, 0.0, 0.4, 1.6, 1.0), new SkillInfo(0x1f, "Archery", 2.5, 7.5, 0.0, "Archer", null, 0.25, 0.75, 0.0, 1.0), 
            new SkillInfo(0x20, "Spirit Speak", 0.0, 0.0, 0.0, "Medium", null, 0.0, 0.0, 1.0, 1.0), new SkillInfo(0x21, "Stealing", 0.0, 10.0, 0.0, "Rogue", null, 0.0, 1.0, 0.0, 1.0), new SkillInfo(0x22, "Tailoring", 3.75, 16.25, 5.0, "Tailor", null, 0.38, 1.63, 0.5, 1.0), new SkillInfo(0x23, "Animal Taming", 14.0, 2.0, 4.0, "Tamer", null, 1.4, 0.2, 0.4, 1.0), new SkillInfo(0x24, "Taste Identification", 0.0, 0.0, 0.0, "Chef", null, 0.2, 0.0, 0.8, 1.0), new SkillInfo(0x25, "Tinkering", 5.0, 2.0, 3.0, "Tinker", null, 0.5, 0.2, 0.3, 1.0), new SkillInfo(0x26, "Tracking", 0.0, 12.5, 12.5, "Ranger", null, 0.0, 1.25, 1.25, 1.0), new SkillInfo(0x27, "Veterinary", 8.0, 4.0, 8.0, "Veterinarian", null, 0.8, 0.4, 0.8, 1.0), new SkillInfo(40, "Swordsmanship", 7.5, 2.5, 0.0, "Swordsman", null, 0.75, 0.25, 0.0, 1.0), new SkillInfo(0x29, "Mace Fighting", 9.0, 1.0, 0.0, "Armsman", null, 0.9, 0.1, 0.0, 1.0), new SkillInfo(0x2a, "Fencing", 4.5, 5.5, 0.0, "Fencer", null, 0.45, 0.55, 0.0, 1.0), new SkillInfo(0x2b, "Wrestling", 9.0, 1.0, 0.0, "Wrestler", null, 0.9, 0.1, 0.0, 1.0), new SkillInfo(0x2c, "Lumberjacking", 20.0, 0.0, 0.0, "Lumberjack", null, 2.0, 0.0, 0.0, 1.0), new SkillInfo(0x2d, "Mining", 20.0, 0.0, 0.0, "Miner", null, 2.0, 0.0, 0.0, 1.0), new SkillInfo(0x2e, "Meditation", 0.0, 0.0, 0.0, "Stoic", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(0x2f, "Stealth", 0.0, 0.0, 0.0, "Rogue", null, 0.0, 0.0, 0.0, 1.0), 
            new SkillInfo(0x30, "Remove Trap", 0.0, 0.0, 0.0, "Rogue", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(0x31, "Necromancy", 0.0, 0.0, 0.0, "Necromancer", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(50, "Focus", 0.0, 0.0, 0.0, "Stoic", null, 0.0, 0.0, 0.0, 1.0), new SkillInfo(0x33, "Chivalry", 0.0, 0.0, 0.0, "Paladin", null, 0.0, 0.0, 0.0, 1.0)
         };
        private string m_Title;

        public SkillInfo(int skillID, string name, double strScale, double dexScale, double intScale, string title, SkillUseCallback callback, double strGain, double dexGain, double intGain, double gainFactor)
        {
            this.m_Name = name;
            this.m_Title = title;
            this.m_SkillID = skillID;
            this.m_StrScale = strScale / 100.0;
            this.m_DexScale = dexScale / 100.0;
            this.m_IntScale = intScale / 100.0;
            this.m_Callback = callback;
            this.m_StrGain = strGain;
            this.m_DexGain = dexGain;
            this.m_IntGain = intGain;
            this.m_GainFactor = gainFactor;
            this.m_StatTotal = (strScale + dexScale) + intScale;
        }

        public SkillUseCallback Callback
        {
            get
            {
                return this.m_Callback;
            }
            set
            {
                this.m_Callback = value;
            }
        }

        public double DexGain
        {
            get
            {
                return this.m_DexGain;
            }
            set
            {
                this.m_DexGain = value;
            }
        }

        public double DexScale
        {
            get
            {
                return this.m_DexScale;
            }
            set
            {
                this.m_DexScale = value;
            }
        }

        public double GainFactor
        {
            get
            {
                return this.m_GainFactor;
            }
            set
            {
                this.m_GainFactor = value;
            }
        }

        public double IntGain
        {
            get
            {
                return this.m_IntGain;
            }
            set
            {
                this.m_IntGain = value;
            }
        }

        public double IntScale
        {
            get
            {
                return this.m_IntScale;
            }
            set
            {
                this.m_IntScale = value;
            }
        }

        public string Name
        {
            get
            {
                return this.m_Name;
            }
            set
            {
                this.m_Name = value;
            }
        }

        public int SkillID
        {
            get
            {
                return this.m_SkillID;
            }
        }

        public double StatTotal
        {
            get
            {
                return this.m_StatTotal;
            }
            set
            {
                this.m_StatTotal = value;
            }
        }

        public double StrGain
        {
            get
            {
                return this.m_StrGain;
            }
            set
            {
                this.m_StrGain = value;
            }
        }

        public double StrScale
        {
            get
            {
                return this.m_StrScale;
            }
            set
            {
                this.m_StrScale = value;
            }
        }

        public static SkillInfo[] Table
        {
            get
            {
                return m_Table;
            }
            set
            {
                m_Table = value;
            }
        }

        public string Title
        {
            get
            {
                return this.m_Title;
            }
            set
            {
                this.m_Title = value;
            }
        }
    }
}

