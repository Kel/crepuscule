﻿namespace Server
{
    using System;

    public abstract class SkillMod
    {
        private bool m_ObeyCap;
        private Mobile m_Owner;
        private bool m_Relative;
        private SkillName m_Skill;
        private double m_Value;

        public SkillMod(SkillName skill, bool relative, double value)
        {
            this.m_Skill = skill;
            this.m_Relative = relative;
            this.m_Value = value;
        }

        public abstract bool CheckCondition();
        public void Remove()
        {
            this.Owner = null;
        }

        public bool Absolute
        {
            get
            {
                return !this.m_Relative;
            }
            set
            {
                if (this.m_Relative == value)
                {
                    this.m_Relative = !value;
                    if (this.m_Owner != null)
                    {
                        Server.Skill skill = this.m_Owner.Skills[this.m_Skill];
                        if (skill != null)
                        {
                            skill.Update();
                        }
                    }
                }
            }
        }

        public bool ObeyCap
        {
            get
            {
                return this.m_ObeyCap;
            }
            set
            {
                this.m_ObeyCap = value;
                if (this.m_Owner != null)
                {
                    Server.Skill skill = this.m_Owner.Skills[this.m_Skill];
                    if (skill != null)
                    {
                        skill.Update();
                    }
                }
            }
        }

        public Mobile Owner
        {
            get
            {
                return this.m_Owner;
            }
            set
            {
                if (this.m_Owner != value)
                {
                    if (this.m_Owner != null)
                    {
                        this.m_Owner.RemoveSkillMod(this);
                    }
                    this.m_Owner = value;
                    if (this.m_Owner != value)
                    {
                        this.m_Owner.AddSkillMod(this);
                    }
                }
            }
        }

        public bool Relative
        {
            get
            {
                return this.m_Relative;
            }
            set
            {
                if (this.m_Relative != value)
                {
                    this.m_Relative = value;
                    if (this.m_Owner != null)
                    {
                        Server.Skill skill = this.m_Owner.Skills[this.m_Skill];
                        if (skill != null)
                        {
                            skill.Update();
                        }
                    }
                }
            }
        }

        public SkillName Skill
        {
            get
            {
                return this.m_Skill;
            }
            set
            {
                if (this.m_Skill != value)
                {
                    Server.Skill skill = (this.m_Owner == null) ? this.m_Owner.Skills[this.m_Skill] : null;
                    this.m_Skill = value;
                    if (this.m_Owner != null)
                    {
                        Server.Skill skill2 = this.m_Owner.Skills[this.m_Skill];
                        if (skill2 != null)
                        {
                            skill2.Update();
                        }
                    }
                    if (skill != null)
                    {
                        skill.Update();
                    }
                }
            }
        }

        public double Value
        {
            get
            {
                return this.m_Value;
            }
            set
            {
                if (this.m_Value != value)
                {
                    this.m_Value = value;
                    if (this.m_Owner != null)
                    {
                        Server.Skill skill = this.m_Owner.Skills[this.m_Skill];
                        if (skill != null)
                        {
                            skill.Update();
                        }
                    }
                }
            }
        }
    }
}

