﻿namespace Server
{
    using System;

    public enum SkillLock : byte
    {
        Down = 1,
        Locked = 2,
        Up = 0
    }
}

