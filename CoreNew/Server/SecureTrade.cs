﻿namespace Server
{
    using Server.Items;
    using Server.Network;
    using System;
    using System.Collections;

    public class SecureTrade
    {
        private SecureTradeInfo m_From;
        private SecureTradeInfo m_To;
        private bool m_Valid = true;

        public SecureTrade(Mobile from, Mobile to)
        {
            this.m_From = new SecureTradeInfo(this, from, new SecureTradeContainer(this));
            this.m_To = new SecureTradeInfo(this, to, new SecureTradeContainer(this));
            from.Send(new MobileStatus(from, to));
            from.Send(new UpdateSecureTrade(this.m_From.Container, false, false));
            from.Send(new SecureTradeEquip(this.m_To.Container, to));
            from.Send(new UpdateSecureTrade(this.m_From.Container, false, false));
            from.Send(new SecureTradeEquip(this.m_From.Container, from));
            from.Send(new DisplaySecureTrade(to, this.m_From.Container, this.m_To.Container, to.GetKnownName(from)));
            from.Send(new UpdateSecureTrade(this.m_From.Container, false, false));
            to.Send(new MobileStatus(to, from));
            to.Send(new UpdateSecureTrade(this.m_To.Container, false, false));
            to.Send(new SecureTradeEquip(this.m_From.Container, from));
            to.Send(new UpdateSecureTrade(this.m_To.Container, false, false));
            to.Send(new SecureTradeEquip(this.m_To.Container, to));
            to.Send(new DisplaySecureTrade(from, this.m_To.Container, this.m_From.Container, from.GetKnownName(to)));
            to.Send(new UpdateSecureTrade(this.m_To.Container, false, false));
        }

        public void Cancel()
        {
            if (this.m_Valid)
            {
                var items = this.m_From.Container.Items;
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    if (i < items.Count)
                    {
                        Item item = (Item) items[i];
                        item.OnSecureTrade(this.m_From.Mobile, this.m_To.Mobile, this.m_From.Mobile, false);
                        if (!item.Deleted)
                        {
                            this.m_From.Mobile.AddToBackpack(item);
                        }
                    }
                }
                items = this.m_To.Container.Items;
                for (int j = items.Count - 1; j >= 0; j--)
                {
                    if (j < items.Count)
                    {
                        Item item2 = (Item) items[j];
                        item2.OnSecureTrade(this.m_To.Mobile, this.m_From.Mobile, this.m_To.Mobile, false);
                        if (!item2.Deleted)
                        {
                            this.m_To.Mobile.AddToBackpack(item2);
                        }
                    }
                }
                this.Close();
            }
        }

        public void Close()
        {
            if (this.m_Valid)
            {
                this.m_From.Mobile.Send(new CloseSecureTrade(this.m_From.Container));
                this.m_To.Mobile.Send(new CloseSecureTrade(this.m_To.Container));
                this.m_Valid = false;
                NetState netState = this.m_From.Mobile.NetState;
                if (netState != null)
                {
                    netState.RemoveTrade(this);
                }
                netState = this.m_To.Mobile.NetState;
                if (netState != null)
                {
                    netState.RemoveTrade(this);
                }
                this.m_From.Container.Delete();
                this.m_To.Container.Delete();
            }
        }

        public void Update()
        {
            if (this.m_Valid)
            {
                if (this.m_From.Accepted && this.m_To.Accepted)
                {
                    var items = this.m_From.Container.Items;
                    bool flag = true;
                    for (int i = items.Count - 1; flag && (i >= 0); i--)
                    {
                        if (i < items.Count)
                        {
                            Item item = (Item) items[i];
                            if (!item.AllowSecureTrade(this.m_From.Mobile, this.m_To.Mobile, this.m_To.Mobile, true))
                            {
                                flag = false;
                            }
                        }
                    }
                    items = this.m_To.Container.Items;
                    for (int j = items.Count - 1; flag && (j >= 0); j--)
                    {
                        if (j < items.Count)
                        {
                            Item item2 = (Item) items[j];
                            if (!item2.AllowSecureTrade(this.m_To.Mobile, this.m_From.Mobile, this.m_From.Mobile, true))
                            {
                                flag = false;
                            }
                        }
                    }
                    if (!flag)
                    {
                        this.m_From.Accepted = false;
                        this.m_To.Accepted = false;
                        this.m_From.Mobile.Send(new UpdateSecureTrade(this.m_From.Container, this.m_From.Accepted, this.m_To.Accepted));
                        this.m_To.Mobile.Send(new UpdateSecureTrade(this.m_To.Container, this.m_To.Accepted, this.m_From.Accepted));
                    }
                    else
                    {
                        items = this.m_From.Container.Items;
                        for (int k = items.Count - 1; k >= 0; k--)
                        {
                            if (k < items.Count)
                            {
                                Item item3 = (Item) items[k];
                                item3.OnSecureTrade(this.m_From.Mobile, this.m_To.Mobile, this.m_To.Mobile, true);
                                if (!item3.Deleted)
                                {
                                    this.m_To.Mobile.AddToBackpack(item3);
                                }
                            }
                        }
                        items = this.m_To.Container.Items;
                        for (int m = items.Count - 1; m >= 0; m--)
                        {
                            if (m < items.Count)
                            {
                                Item item4 = (Item) items[m];
                                item4.OnSecureTrade(this.m_To.Mobile, this.m_From.Mobile, this.m_From.Mobile, true);
                                if (!item4.Deleted)
                                {
                                    this.m_From.Mobile.AddToBackpack(item4);
                                }
                            }
                        }
                        this.Close();
                    }
                }
                else
                {
                    this.m_From.Mobile.Send(new UpdateSecureTrade(this.m_From.Container, this.m_From.Accepted, this.m_To.Accepted));
                    this.m_To.Mobile.Send(new UpdateSecureTrade(this.m_To.Container, this.m_To.Accepted, this.m_From.Accepted));
                }
            }
        }

        public SecureTradeInfo From
        {
            get
            {
                return this.m_From;
            }
        }

        public SecureTradeInfo To
        {
            get
            {
                return this.m_To;
            }
        }

        public bool Valid
        {
            get
            {
                return this.m_Valid;
            }
        }
    }
}

