﻿namespace Server
{
    using System;

    public class TileList
    {
        private int m_Count = 0;
        private static Tile[] m_EmptyTiles = new Tile[0];
        private Tile[] m_Tiles = new Tile[8];

        public void Add(short id, sbyte z)
        {
            if ((this.m_Count + 1) > this.m_Tiles.Length)
            {
                Tile[] tiles = this.m_Tiles;
                this.m_Tiles = new Tile[tiles.Length * 2];
                for (int i = 0; i < tiles.Length; i++)
                {
                    this.m_Tiles[i] = tiles[i];
                }
            }
            this.m_Tiles[this.m_Count].m_ID = id;
            this.m_Tiles[this.m_Count].m_Z = z;
            this.m_Count++;
        }

        public void AddRange(Tile[] tiles)
        {
            if ((this.m_Count + tiles.Length) > this.m_Tiles.Length)
            {
                Tile[] tileArray = this.m_Tiles;
                this.m_Tiles = new Tile[(this.m_Count + tiles.Length) * 2];
                for (int j = 0; j < tileArray.Length; j++)
                {
                    this.m_Tiles[j] = tileArray[j];
                }
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                this.m_Tiles[this.m_Count++] = tiles[i];
            }
        }

        public Tile[] ToArray()
        {
            if (this.m_Count == 0)
            {
                return m_EmptyTiles;
            }
            Tile[] tileArray = new Tile[this.m_Count];
            for (int i = 0; i < this.m_Count; i++)
            {
                tileArray[i] = this.m_Tiles[i];
            }
            this.m_Count = 0;
            return tileArray;
        }

        public int Count
        {
            get
            {
                return this.m_Count;
            }
        }
    }
}

