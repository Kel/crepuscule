﻿namespace Server
{
    using System;
    using System.IO;

    public class ItemBounds
    {
        private static Rectangle2D[] m_Bounds;

        static ItemBounds()
        {
            if (File.Exists("Data/Binary/Bounds.bin"))
            {
                using (FileStream stream = new FileStream("Data/Binary/Bounds.bin", FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader reader = new BinaryReader(stream);
                    m_Bounds = new Rectangle2D[0x4000];
                    for (int i = 0; i < 0x4000; i++)
                    {
                        int x = reader.ReadInt16();
                        int y = reader.ReadInt16();
                        int num4 = reader.ReadInt16();
                        int num5 = reader.ReadInt16();
                        m_Bounds[i].Set(x, y, (num4 - x) + 1, (num5 - y) + 1);
                    }
                    return;
                }
            }
            Console.WriteLine("Warning: Data/Binary/Bounds.bin does not exist");
            m_Bounds = new Rectangle2D[0x4000];
        }

        public static Rectangle2D[] Table
        {
            get
            {
                return m_Bounds;
            }
        }
    }
}

