﻿namespace Server
{
    using System;

    public class MobileNotConnectedException : Exception
    {
        public MobileNotConnectedException(Mobile source, string message) : base(message)
        {
            this.Source = source.ToString();
        }
    }
}

