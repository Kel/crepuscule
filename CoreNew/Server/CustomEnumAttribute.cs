﻿namespace Server
{
    using System;

    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Struct | AttributeTargets.Class)]
    public class CustomEnumAttribute : Attribute
    {
        private string[] m_Names;

        public CustomEnumAttribute(string[] names)
        {
            this.m_Names = names;
        }

        public string[] Names
        {
            get
            {
                return this.m_Names;
            }
        }
    }
}

