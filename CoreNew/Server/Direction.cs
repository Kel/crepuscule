﻿namespace Server
{
    using System;

    [CustomEnum(new string[] { "North", "Right", "East", "Down", "South", "Left", "West", "Up" })]
    public enum Direction : byte
    {
        Down = 3,
        East = 2,
        Left = 5,
        Mask = 7,
        North = 0,
        Right = 1,
        Running = 0x80,
        South = 4,
        Up = 7,
        ValueMask = 0x87,
        West = 6
    }
}

