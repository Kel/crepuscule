﻿namespace Server
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    public class TileMatrixPatch
    {
        private static bool m_Enabled = true;
        private int m_LandBlocks;
        private int m_StaticBlocks;
        private static StaticTile[] m_TileBuffer = new StaticTile[0x80];

        public TileMatrixPatch(TileMatrix matrix, int index)
        {
            if (m_Enabled)
            {
                string path = Core.FindDataFile("mapdif{0}.mul", new object[] { index });
                string str2 = Core.FindDataFile("mapdifl{0}.mul", new object[] { index });
                if (File.Exists(path) && File.Exists(str2))
                {
                    this.m_LandBlocks = this.PatchLand(matrix, path, str2);
                }
                string str3 = Core.FindDataFile("stadif{0}.mul", new object[] { index });
                string str4 = Core.FindDataFile("stadifl{0}.mul", new object[] { index });
                string str5 = Core.FindDataFile("stadifi{0}.mul", new object[] { index });
                if ((File.Exists(str3) && File.Exists(str4)) && File.Exists(str5))
                {
                    this.m_StaticBlocks = this.PatchStatics(matrix, str3, str4, str5);
                }
            }
        }

        [DllImport("Kernel32")]
        private static extern unsafe int _lread(IntPtr hFile, void* lpBuffer, int wBytes);
        private unsafe int PatchLand(TileMatrix matrix, string dataPath, string indexPath)
        {
            int num6;
            using (FileStream stream = new FileStream(dataPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (FileStream stream2 = new FileStream(indexPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader reader = new BinaryReader(stream2);
                    int num = (int) (reader.BaseStream.Length / 4L);
                    for (int i = 0; i < num; i++)
                    {
                        int num3 = reader.ReadInt32();
                        int x = num3 / matrix.BlockHeight;
                        int y = num3 % matrix.BlockHeight;
                        stream.Seek(4L, SeekOrigin.Current);
                        Tile[] tileArray = new Tile[0x40];
                        fixed (Tile* tileRef = tileArray)
                        {
                            _lread(stream.Handle, (void*) tileRef, 0xc0);
                        }
                        matrix.SetLandBlock(x, y, tileArray);
                    }
                    num6 = num;
                }
            }
            return num6;
        }

        private unsafe int PatchStatics(TileMatrix matrix, string dataPath, string indexPath, string lookupPath)
        {
            int num13;
            using (FileStream stream = new FileStream(dataPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (FileStream stream2 = new FileStream(indexPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (FileStream stream3 = new FileStream(lookupPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        BinaryReader reader = new BinaryReader(stream2);
                        BinaryReader reader2 = new BinaryReader(stream3);
                        int num = (int) (reader.BaseStream.Length / 4L);
                        TileList[][] listArray = new TileList[8][];
                        for (int i = 0; i < 8; i++)
                        {
                            listArray[i] = new TileList[8];
                            for (int k = 0; k < 8; k++)
                            {
                                listArray[i][k] = new TileList();
                            }
                        }
                        for (int j = 0; j < num; j++)
                        {
                            int num5 = reader.ReadInt32();
                            int x = num5 / matrix.BlockHeight;
                            int y = num5 % matrix.BlockHeight;
                            int num8 = reader2.ReadInt32();
                            int wBytes = reader2.ReadInt32();
                            reader2.ReadInt32();
                            if ((num8 < 0) || (wBytes <= 0))
                            {
                                matrix.SetStaticBlock(x, y, matrix.EmptyStaticBlock);
                            }
                            else
                            {
                                stream.Seek((long) num8, SeekOrigin.Begin);
                                int num10 = wBytes / 7;
                                if (m_TileBuffer.Length < num10)
                                {
                                    m_TileBuffer = new StaticTile[num10];
                                }
                                StaticTile[] tileBuffer = m_TileBuffer;
                                fixed (StaticTile* tileRef = tileBuffer)
                                {
                                    _lread(stream.Handle, (void*) tileRef, wBytes);
                                    StaticTile* tilePtr = tileRef;
                                    StaticTile* tilePtr2 = tileRef + num10;
                                    while (tilePtr < tilePtr2)
                                    {
                                        listArray[tilePtr->m_X & 7][tilePtr->m_Y & 7].Add((short) ((tilePtr->m_ID & 0x3fff) + 0x4000), tilePtr->m_Z);
                                        tilePtr++;
                                    }
                                    Tile[][][] tileArray2 = new Tile[8][][];
                                    for (int m = 0; m < 8; m++)
                                    {
                                        tileArray2[m] = new Tile[8][];
                                        for (int n = 0; n < 8; n++)
                                        {
                                            tileArray2[m][n] = listArray[m][n].ToArray();
                                        }
                                    }
                                    matrix.SetStaticBlock(x, y, tileArray2);
                                }
                            }
                        }
                        num13 = num;
                    }
                }
            }
            return num13;
        }

        public static bool Enabled
        {
            get
            {
                return m_Enabled;
            }
            set
            {
                m_Enabled = value;
            }
        }

        public int LandBlocks
        {
            get
            {
                return this.m_LandBlocks;
            }
        }

        public int StaticBlocks
        {
            get
            {
                return this.m_StaticBlocks;
            }
        }
    }
}

