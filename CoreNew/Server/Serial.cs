﻿namespace Server
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Serial : IComparable
    {
        private int m_Serial;
        private static Serial m_LastMobile;
        private static Serial m_LastItem;
        public static readonly Serial MinusOne;
        public static readonly Serial Zero;
        public static Serial NewMobile
        {
            get
            {
                while (World.FindMobile(m_LastMobile += 1) != null)
                {
                }
                return m_LastMobile;
            }
        }
        public static Serial NewItem
        {
            get
            {
                while (World.FindItem(m_LastItem += 1) != null)
                {
                }
                return m_LastItem;
            }
        }
        private Serial(int serial)
        {
            this.m_Serial = serial;
        }

        public int Value
        {
            get
            {
                return this.m_Serial;
            }
        }
        public bool IsMobile
        {
            get
            {
                return ((this.m_Serial > 0) && (this.m_Serial < 0x40000000));
            }
        }
        public bool IsItem
        {
            get
            {
                return ((this.m_Serial >= 0x40000000) && (this.m_Serial <= 0x7fffffff));
            }
        }
        public bool IsValid
        {
            get
            {
                return (this.m_Serial > 0);
            }
        }
        public override int GetHashCode()
        {
            return this.m_Serial;
        }

        public int CompareTo(object o)
        {
            if (o == null)
            {
                return 1;
            }
            if (!(o is Serial))
            {
                throw new ArgumentException();
            }
            Serial serial = (Serial) o;
            int num = serial.m_Serial;
            if (this.m_Serial > num)
            {
                return 1;
            }
            if (this.m_Serial < num)
            {
                return -1;
            }
            return 0;
        }

        public override bool Equals(object o)
        {
            if ((o == null) || !(o is Serial))
            {
                return false;
            }
            Serial serial = (Serial) o;
            return (serial.m_Serial == this.m_Serial);
        }

        public static bool operator ==(Serial l, Serial r)
        {
            return (l.m_Serial == r.m_Serial);
        }

        public static bool operator !=(Serial l, Serial r)
        {
            return (l.m_Serial != r.m_Serial);
        }

        public static bool operator >(Serial l, Serial r)
        {
            return (l.m_Serial > r.m_Serial);
        }

        public static bool operator <(Serial l, Serial r)
        {
            return (l.m_Serial < r.m_Serial);
        }

        public static bool operator >=(Serial l, Serial r)
        {
            return (l.m_Serial >= r.m_Serial);
        }

        public static bool operator <=(Serial l, Serial r)
        {
            return (l.m_Serial <= r.m_Serial);
        }

        public override string ToString()
        {
            return string.Format("0x{0:X8}", this.m_Serial);
        }

        public static implicit operator int(Serial a)
        {
            return a.m_Serial;
        }

        public static implicit operator Serial(int a)
        {
            return new Serial(a);
        }

        static Serial()
        {
            m_LastMobile = Zero;
            m_LastItem = 0x40000000;
            MinusOne = new Serial(-1);
            Zero = new Serial(0);
        }
    }
}

