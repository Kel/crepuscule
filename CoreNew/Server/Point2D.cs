﻿namespace Server
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential), Parsable]
    public struct Point2D : IPoint2D
    {
        internal int m_X;
        internal int m_Y;
        public static readonly Point2D Zero;
        public Point2D(int x, int y)
        {
            this.m_X = x;
            this.m_Y = y;
        }

        public Point2D(IPoint2D p) : this(p.X, p.Y)
        {
        }

        [CommandProperty(AccessLevel.Counselor)]
        public int X
        {
            get
            {
                return this.m_X;
            }
            set
            {
                this.m_X = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int Y
        {
            get
            {
                return this.m_Y;
            }
            set
            {
                this.m_Y = value;
            }
        }
        public override string ToString()
        {
            return string.Format("({0}, {1})", this.m_X, this.m_Y);
        }

        public static Point2D Parse(string value)
        {
            int index = value.IndexOf('(');
            int num2 = value.IndexOf(',', index + 1);
            string str = value.Substring(index + 1, num2 - (index + 1)).Trim();
            index = num2;
            num2 = value.IndexOf(')', index + 1);
            string str2 = value.Substring(index + 1, num2 - (index + 1)).Trim();
            return new Point2D(Convert.ToInt32(str), Convert.ToInt32(str2));
        }

        public override bool Equals(object o)
        {
            if ((o == null) || !(o is IPoint2D))
            {
                return false;
            }
            IPoint2D pointd = (IPoint2D) o;
            return ((this.m_X == pointd.X) && (this.m_Y == pointd.Y));
        }

        public override int GetHashCode()
        {
            return (this.m_X ^ this.m_Y);
        }

        public static bool operator ==(Point2D l, Point2D r)
        {
            return ((l.m_X == r.m_X) && (l.m_Y == r.m_Y));
        }

        public static bool operator !=(Point2D l, Point2D r)
        {
            if (l.m_X == r.m_X)
            {
                return (l.m_Y != r.m_Y);
            }
            return true;
        }

        public static bool operator ==(Point2D l, IPoint2D r)
        {
            return ((l.m_X == r.X) && (l.m_Y == r.Y));
        }

        public static bool operator !=(Point2D l, IPoint2D r)
        {
            if (l.m_X == r.X)
            {
                return (l.m_Y != r.Y);
            }
            return true;
        }

        public static bool operator >(Point2D l, Point2D r)
        {
            return ((l.m_X > r.m_X) && (l.m_Y > r.m_Y));
        }

        public static bool operator >(Point2D l, Point3D r)
        {
            return ((l.m_X > r.m_X) && (l.m_Y > r.m_Y));
        }

        public static bool operator >(Point2D l, IPoint2D r)
        {
            return ((l.m_X > r.X) && (l.m_Y > r.Y));
        }

        public static bool operator <(Point2D l, Point2D r)
        {
            return ((l.m_X < r.m_X) && (l.m_Y < r.m_Y));
        }

        public static bool operator <(Point2D l, Point3D r)
        {
            return ((l.m_X < r.m_X) && (l.m_Y < r.m_Y));
        }

        public static bool operator <(Point2D l, IPoint2D r)
        {
            return ((l.m_X < r.X) && (l.m_Y < r.Y));
        }

        public static bool operator >=(Point2D l, Point2D r)
        {
            return ((l.m_X >= r.m_X) && (l.m_Y >= r.m_Y));
        }

        public static bool operator >=(Point2D l, Point3D r)
        {
            return ((l.m_X >= r.m_X) && (l.m_Y >= r.m_Y));
        }

        public static bool operator >=(Point2D l, IPoint2D r)
        {
            return ((l.m_X >= r.X) && (l.m_Y >= r.Y));
        }

        public static bool operator <=(Point2D l, Point2D r)
        {
            return ((l.m_X <= r.m_X) && (l.m_Y <= r.m_Y));
        }

        public static bool operator <=(Point2D l, Point3D r)
        {
            return ((l.m_X <= r.m_X) && (l.m_Y <= r.m_Y));
        }

        public static bool operator <=(Point2D l, IPoint2D r)
        {
            return ((l.m_X <= r.X) && (l.m_Y <= r.Y));
        }

        static Point2D()
        {
            Zero = new Point2D(0, 0);
        }
    }
}

