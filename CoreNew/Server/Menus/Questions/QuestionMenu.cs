﻿namespace Server.Menus.Questions
{
    using Server.Menus;
    using Server.Network;
    using System;

    public class QuestionMenu : IMenu
    {
        private string[] m_Answers;
        private static int m_NextSerial;
        private string m_Question;
        private int m_Serial;

        public QuestionMenu(string question, string[] answers)
        {
            this.m_Question = question;
            this.m_Answers = answers;
            do
            {
                this.m_Serial = ++m_NextSerial;
                this.m_Serial &= 0x7fffffff;
            }
            while (this.m_Serial == 0);
        }

        public virtual void OnCancel(NetState state)
        {
        }

        public virtual void OnResponse(NetState state, int index)
        {
        }

        public void SendTo(NetState state)
        {
            state.AddMenu(this);
            state.Send(new DisplayQuestionMenu(this));
        }

        public string[] Answers
        {
            get
            {
                return this.m_Answers;
            }
        }

        public string Question
        {
            get
            {
                return this.m_Question;
            }
            set
            {
                this.m_Question = value;
            }
        }

        int IMenu.EntryLength
        {
            get
            {
                return this.m_Answers.Length;
            }
        }

        int IMenu.Serial
        {
            get
            {
                return this.m_Serial;
            }
        }
    }
}

