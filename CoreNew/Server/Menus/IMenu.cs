﻿namespace Server.Menus
{
    using Server.Network;
    using System;

    public interface IMenu
    {
        void OnCancel(NetState state);
        void OnResponse(NetState state, int index);
        void SendTo(NetState state);

        int EntryLength { get; }

        int Serial { get; }
    }
}

