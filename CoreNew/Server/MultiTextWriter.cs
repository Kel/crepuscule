﻿namespace Server
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Text;

    public class MultiTextWriter : TextWriter
    {
        private ArrayList m_Streams;

        public MultiTextWriter(params TextWriter[] streams)
        {
            this.m_Streams = new ArrayList(streams);
            if (this.m_Streams.Count < 0)
            {
                throw new ArgumentException("You must specify at least one stream.");
            }
        }

        public void Add(TextWriter tw)
        {
            this.m_Streams.Add(tw);
        }

        public void Remove(TextWriter tw)
        {
            this.m_Streams.Remove(tw);
        }

        public override void Write(char ch)
        {
            for (int i = 0; i < this.m_Streams.Count; i++)
            {
                ((TextWriter) this.m_Streams[i]).Write(ch);
            }
        }

        public override void WriteLine(string line)
        {
            for (int i = 0; i < this.m_Streams.Count; i++)
            {
                ((TextWriter) this.m_Streams[i]).WriteLine(line);
            }
        }

        public override void WriteLine(string line, params object[] args)
        {
            this.WriteLine(string.Format(line, args));
        }

        public override System.Text.Encoding Encoding
        {
            get
            {
                return System.Text.Encoding.Default;
            }
        }
    }
}

