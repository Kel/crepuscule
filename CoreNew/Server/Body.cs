﻿namespace Server
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Body
    {
        private int m_BodyID;
        private static BodyType[] m_Types;
        static Body()
        {
            if (File.Exists("Data/Binary/BodyTypes.bin"))
            {
                using (BinaryReader reader = new BinaryReader(new FileStream("Data/Binary/BodyTypes.bin", FileMode.Open, FileAccess.Read, FileShare.Read)))
                {
                    m_Types = new BodyType[(int) reader.BaseStream.Length];
                    for (int i = 0; i < m_Types.Length; i++)
                    {
                        m_Types[i] = (BodyType) reader.ReadByte();
                    }
                    return;
                }
            }
            Console.WriteLine("Warning: Data/Binary/BodyTypes.bin does not exist");
            m_Types = new BodyType[0];
        }

        public Body(int bodyID)
        {
            this.m_BodyID = bodyID;
        }

        public BodyType Type
        {
            get
            {
                if ((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length))
                {
                    return m_Types[this.m_BodyID];
                }
                return BodyType.Empty;
            }
        }
        public bool IsHuman
        {
            get
            {
                return (((((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length)) && ((m_Types[this.m_BodyID] == BodyType.Human) && (this.m_BodyID != 0x192))) && (this.m_BodyID != 0x194)) && (this.m_BodyID != 970));
            }
        }
        public bool IsMale
        {
            get
            {
                if (((this.m_BodyID != 0xb7) && (this.m_BodyID != 0xb9)) && ((this.m_BodyID != 400) && (this.m_BodyID != 0x192)))
                {
                    return (this.m_BodyID == 750);
                }
                return true;
            }
        }
        public bool IsFemale
        {
            get
            {
                if (((this.m_BodyID != 0xb8) && (this.m_BodyID != 0xba)) && ((this.m_BodyID != 0x191) && (this.m_BodyID != 0x193)))
                {
                    return (this.m_BodyID == 0x2ef);
                }
                return true;
            }
        }
        public bool IsGhost
        {
            get
            {
                if ((this.m_BodyID != 0x192) && (this.m_BodyID != 0x193))
                {
                    return (this.m_BodyID == 970);
                }
                return true;
            }
        }
        public bool IsMonster
        {
            get
            {
                return (((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length)) && (m_Types[this.m_BodyID] == BodyType.Monster));
            }
        }
        public bool IsAnimal
        {
            get
            {
                return (((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length)) && (m_Types[this.m_BodyID] == BodyType.Animal));
            }
        }
        public bool IsEmpty
        {
            get
            {
                return (((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length)) && (m_Types[this.m_BodyID] == BodyType.Empty));
            }
        }
        public bool IsSea
        {
            get
            {
                return (((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length)) && (m_Types[this.m_BodyID] == BodyType.Sea));
            }
        }
        public bool IsEquipment
        {
            get
            {
                return (((this.m_BodyID >= 0) && (this.m_BodyID < m_Types.Length)) && (m_Types[this.m_BodyID] == BodyType.Equipment));
            }
        }
        public int BodyID
        {
            get
            {
                return this.m_BodyID;
            }
        }
        public static implicit operator int(Body a)
        {
            return a.m_BodyID;
        }

        public static implicit operator Body(int a)
        {
            return new Body(a);
        }

        public override string ToString()
        {
            return string.Format("0x{0:X}", this.m_BodyID);
        }

        public override int GetHashCode()
        {
            return this.m_BodyID;
        }

        public override bool Equals(object o)
        {
            if ((o == null) || !(o is Body))
            {
                return false;
            }
            Body body = (Body) o;
            return (body.m_BodyID == this.m_BodyID);
        }

        public static bool operator ==(Body l, Body r)
        {
            return (l.m_BodyID == r.m_BodyID);
        }

        public static bool operator !=(Body l, Body r)
        {
            return (l.m_BodyID != r.m_BodyID);
        }

        public static bool operator >(Body l, Body r)
        {
            return (l.m_BodyID > r.m_BodyID);
        }

        public static bool operator >=(Body l, Body r)
        {
            return (l.m_BodyID >= r.m_BodyID);
        }

        public static bool operator <(Body l, Body r)
        {
            return (l.m_BodyID < r.m_BodyID);
        }

        public static bool operator <=(Body l, Body r)
        {
            return (l.m_BodyID <= r.m_BodyID);
        }
    }
}

