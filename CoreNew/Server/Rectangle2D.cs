﻿namespace Server
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential), Parsable, NoSort, PropertyObject]
    public struct Rectangle2D
    {
        private Point2D m_Start;
        private Point2D m_End;
        public Rectangle2D(IPoint2D start, IPoint2D end)
        {
            this.m_Start = new Point2D(start);
            this.m_End = new Point2D(end);
        }

        public Rectangle2D(int x, int y, int width, int height)
        {
            this.m_Start = new Point2D(x, y);
            this.m_End = new Point2D(x + width, y + height);
        }

        public void Set(int x, int y, int width, int height)
        {
            this.m_Start = new Point2D(x, y);
            this.m_End = new Point2D(x + width, y + height);
        }

        public static Rectangle2D Parse(string value)
        {
            int index = value.IndexOf('(');
            int num2 = value.IndexOf(',', index + 1);
            string str = value.Substring(index + 1, num2 - (index + 1)).Trim();
            index = num2;
            num2 = value.IndexOf(',', index + 1);
            string str2 = value.Substring(index + 1, num2 - (index + 1)).Trim();
            index = num2;
            num2 = value.IndexOf(',', index + 1);
            string str3 = value.Substring(index + 1, num2 - (index + 1)).Trim();
            index = num2;
            num2 = value.IndexOf(')', index + 1);
            string str4 = value.Substring(index + 1, num2 - (index + 1)).Trim();
            return new Rectangle2D(Convert.ToInt32(str), Convert.ToInt32(str2), Convert.ToInt32(str3), Convert.ToInt32(str4));
        }

        [CommandProperty(AccessLevel.Counselor)]
        public Point2D Start
        {
            get
            {
                return this.m_Start;
            }
            set
            {
                this.m_Start = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public Point2D End
        {
            get
            {
                return this.m_End;
            }
            set
            {
                this.m_End = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int X
        {
            get
            {
                return this.m_Start.m_X;
            }
            set
            {
                this.m_Start.m_X = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int Y
        {
            get
            {
                return this.m_Start.m_Y;
            }
            set
            {
                this.m_Start.m_Y = value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int Width
        {
            get
            {
                return (this.m_End.m_X - this.m_Start.m_X);
            }
            set
            {
                this.m_End.m_X = this.m_Start.m_X + value;
            }
        }
        [CommandProperty(AccessLevel.Counselor)]
        public int Height
        {
            get
            {
                return (this.m_End.m_Y - this.m_Start.m_Y);
            }
            set
            {
                this.m_End.m_Y = this.m_Start.m_Y + value;
            }
        }
        public void MakeHold(Rectangle2D r)
        {
            if (r.m_Start.m_X < this.m_Start.m_X)
            {
                this.m_Start.m_X = r.m_Start.m_X;
            }
            if (r.m_Start.m_Y < this.m_Start.m_Y)
            {
                this.m_Start.m_Y = r.m_Start.m_Y;
            }
            if (r.m_End.m_X > this.m_End.m_X)
            {
                this.m_End.m_X = r.m_End.m_X;
            }
            if (r.m_End.m_Y > this.m_End.m_Y)
            {
                this.m_End.m_Y = r.m_End.m_Y;
            }
        }

        public bool Contains(Point3D p)
        {
            return ((((this.m_Start.m_X <= p.m_X) && (this.m_Start.m_Y <= p.m_Y)) && (this.m_End.m_X > p.m_X)) && (this.m_End.m_Y > p.m_Y));
        }

        public bool Contains(Point2D p)
        {
            return ((((this.m_Start.m_X <= p.m_X) && (this.m_Start.m_Y <= p.m_Y)) && (this.m_End.m_X > p.m_X)) && (this.m_End.m_Y > p.m_Y));
        }

        public bool Contains(IPoint2D p)
        {
            return ((this.m_Start <= p) && (this.m_End > p));
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})+({2}, {3})", new object[] { this.X, this.Y, this.Width, this.Height });
        }
    }
}

