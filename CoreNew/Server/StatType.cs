﻿namespace Server
{
    using System;

    [Flags]
    public enum StatType
    {
        All = 7,
        Dex = 2,
        Int = 4,
        Str = 1
    }
}

