﻿namespace Server
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Runtime.InteropServices;

    public class TileMatrix
    {
        private int m_BlockHeight;
        private int m_BlockWidth;
        private Tile[][][] m_EmptyStaticBlock;
        private int m_FileIndex;
        private ArrayList m_FileShare = new ArrayList();
        private int m_Height;
        private FileStream m_Index;
        private BinaryReader m_IndexReader;
        private static ArrayList m_Instances = new ArrayList();
        private Tile[] m_InvalidLandBlock;
        private int[][] m_LandPatches;
        private Tile[][][] m_LandTiles;
        private static TileList[][] m_Lists;
        private FileStream m_Map;
        private DateTime m_NextLandWarning;
        private DateTime m_NextStaticWarning;
        private Map m_Owner;
        private TileMatrixPatch m_Patch;
        private int[][] m_StaticPatches;
        private FileStream m_Statics;
        private Tile[][][][][] m_StaticTiles;
        private static StaticTile[] m_TileBuffer = new StaticTile[0x80];
        private static TileList m_TilesList = new TileList();
        private int m_Width;

        public TileMatrix(Map owner, int fileIndex, int mapID, int width, int height)
        {
            for (int i = 0; i < m_Instances.Count; i++)
            {
                TileMatrix matrix = (TileMatrix) m_Instances[i];
                if (matrix.m_FileIndex == fileIndex)
                {
                    matrix.m_FileShare.Add(this);
                    this.m_FileShare.Add(matrix);
                }
            }
            m_Instances.Add(this);
            this.m_FileIndex = fileIndex;
            this.m_Width = width;
            this.m_Height = height;
            this.m_BlockWidth = width >> 3;
            this.m_BlockHeight = height >> 3;
            this.m_Owner = owner;
            if (fileIndex != 0x7f)
            {
                string path = Core.FindDataFile("map{0}.mul", new object[] { fileIndex });
                if (File.Exists(path))
                {
                    this.m_Map = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                }
                string str2 = Core.FindDataFile("staidx{0}.mul", new object[] { fileIndex });
                if (File.Exists(str2))
                {
                    this.m_Index = new FileStream(str2, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    this.m_IndexReader = new BinaryReader(this.m_Index);
                }
                string str3 = Core.FindDataFile("statics{0}.mul", new object[] { fileIndex });
                if (File.Exists(str3))
                {
                    this.m_Statics = new FileStream(str3, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                }
            }
            this.m_EmptyStaticBlock = new Tile[8][][];
            for (int j = 0; j < 8; j++)
            {
                this.m_EmptyStaticBlock[j] = new Tile[8][];
                for (int k = 0; k < 8; k++)
                {
                    this.m_EmptyStaticBlock[j][k] = new Tile[0];
                }
            }
            this.m_InvalidLandBlock = new Tile[0xc4];
            this.m_LandTiles = new Tile[this.m_BlockWidth][][];
            this.m_StaticTiles = new Tile[this.m_BlockWidth][][][][];
            this.m_StaticPatches = new int[this.m_BlockWidth][];
            this.m_LandPatches = new int[this.m_BlockWidth][];
            this.m_Patch = new TileMatrixPatch(this, mapID);
        }

        [DllImport("Kernel32")]
        private static extern unsafe int _lread(IntPtr hFile, void* lpBuffer, int wBytes);
        public void Dispose()
        {
            if (this.m_Map != null)
            {
                this.m_Map.Close();
            }
            if (this.m_Statics != null)
            {
                this.m_Statics.Close();
            }
            if (this.m_IndexReader != null)
            {
                this.m_IndexReader.Close();
            }
        }

        public void Force()
        {
            if ((ScriptCompiler.Assemblies == null) || (ScriptCompiler.Assemblies.Length == 0))
            {
                throw new Exception();
            }
        }

        public Tile[] GetLandBlock(int x, int y)
        {
            if (((x < 0) || (y < 0)) || (((x >= this.m_BlockWidth) || (y >= this.m_BlockHeight)) || (this.m_Map == null)))
            {
                return this.m_InvalidLandBlock;
            }
            if (this.m_LandTiles[x] == null)
            {
                this.m_LandTiles[x] = new Tile[this.m_BlockHeight][];
            }
            Tile[] tileArray = this.m_LandTiles[x][y];
            if (tileArray == null)
            {
                for (int i = 0; (tileArray == null) && (i < this.m_FileShare.Count); i++)
                {
                    TileMatrix matrix = (TileMatrix) this.m_FileShare[i];
                    if (((x >= 0) && (x < matrix.m_BlockWidth)) && ((y >= 0) && (y < matrix.m_BlockHeight)))
                    {
                        Tile[][] tileArray2 = matrix.m_LandTiles[x];
                        if (tileArray2 != null)
                        {
                            tileArray = tileArray2[y];
                        }
                        if (tileArray != null)
                        {
                            int[] numArray = matrix.m_LandPatches[x];
                            if ((numArray != null) && ((numArray[y >> 5] & (((int) 1) << y)) != 0))
                            {
                                tileArray = null;
                            }
                        }
                    }
                }
                if (tileArray == null)
                {
                    tileArray = this.ReadLandBlock(x, y);
                }
                this.m_LandTiles[x][y] = tileArray;
            }
            return tileArray;
        }

        public Tile GetLandTile(int x, int y)
        {
            return this.GetLandBlock(x >> 3, y >> 3)[((y & 7) << 3) + (x & 7)];
        }

        public Tile[][][] GetStaticBlock(int x, int y)
        {
            if ((((x < 0) || (y < 0)) || ((x >= this.m_BlockWidth) || (y >= this.m_BlockHeight))) || ((this.m_Statics == null) || (this.m_Index == null)))
            {
                return this.m_EmptyStaticBlock;
            }
            if (this.m_StaticTiles[x] == null)
            {
                this.m_StaticTiles[x] = new Tile[this.m_BlockHeight][][][];
            }
            Tile[][][] tileArray = this.m_StaticTiles[x][y];
            if (tileArray == null)
            {
                for (int i = 0; (tileArray == null) && (i < this.m_FileShare.Count); i++)
                {
                    TileMatrix matrix = (TileMatrix) this.m_FileShare[i];
                    if (((x >= 0) && (x < matrix.m_BlockWidth)) && ((y >= 0) && (y < matrix.m_BlockHeight)))
                    {
                        Tile[][][][] tileArray2 = matrix.m_StaticTiles[x];
                        if (tileArray2 != null)
                        {
                            tileArray = tileArray2[y];
                        }
                        if (tileArray != null)
                        {
                            int[] numArray = matrix.m_StaticPatches[x];
                            if ((numArray != null) && ((numArray[y >> 5] & (((int) 1) << y)) != 0))
                            {
                                tileArray = null;
                            }
                        }
                    }
                }
                if (tileArray == null)
                {
                    tileArray = this.ReadStaticBlock(x, y);
                }
                this.m_StaticTiles[x][y] = tileArray;
            }
            return tileArray;
        }

        public Tile[] GetStaticTiles(int x, int y)
        {
            return this.GetStaticBlock(x >> 3, y >> 3)[x & 7][y & 7];
        }

        public Tile[] GetStaticTiles(int x, int y, bool multis)
        {
            Tile[][][] staticBlock = this.GetStaticBlock(x >> 3, y >> 3);
            if (!multis)
            {
                return staticBlock[x & 7][y & 7];
            }
            IPooledEnumerable multiTilesAt = this.m_Owner.GetMultiTilesAt(x, y);
            if (multiTilesAt == Map.NullEnumerable.Instance)
            {
                return staticBlock[x & 7][y & 7];
            }
            bool flag = false;
            foreach (Tile[] tileArray2 in multiTilesAt)
            {
                if (!flag)
                {
                    flag = true;
                }
                m_TilesList.AddRange(tileArray2);
            }
            multiTilesAt.Free();
            if (!flag)
            {
                return staticBlock[x & 7][y & 7];
            }
            m_TilesList.AddRange(staticBlock[x & 7][y & 7]);
            return m_TilesList.ToArray();
        }

        private unsafe Tile[] ReadLandBlock(int x, int y)
        {
            try
            {
                this.m_Map.Seek((long) ((((x * this.m_BlockHeight) + y) * 0xc4) + 4), SeekOrigin.Begin);
                Tile[] tileArray = new Tile[0x40];
                fixed (Tile* tileRef = tileArray)
                {
                    _lread(this.m_Map.Handle, (void*) tileRef, 0xc0);
                }
                return tileArray;
            }
            catch
            {
                if (DateTime.Now >= this.m_NextLandWarning)
                {
                    Console.WriteLine("Warning: Land EOS for {0} ({1}, {2})", this.m_Owner, x, y);
                    this.m_NextLandWarning = DateTime.Now + TimeSpan.FromMinutes(1.0);
                }
                return this.m_InvalidLandBlock;
            }
        }

        private unsafe Tile[][][] ReadStaticBlock(int x, int y)
        {
            Tile[][][] emptyStaticBlock;
            try
            {
                this.m_IndexReader.BaseStream.Seek((long) (((x * this.m_BlockHeight) + y) * 12), SeekOrigin.Begin);
                int num = this.m_IndexReader.ReadInt32();
                int wBytes = this.m_IndexReader.ReadInt32();
                if ((num < 0) || (wBytes <= 0))
                {
                    return this.m_EmptyStaticBlock;
                }
                int num3 = wBytes / 7;
                this.m_Statics.Seek((long) num, SeekOrigin.Begin);
                if (m_TileBuffer.Length < num3)
                {
                    m_TileBuffer = new StaticTile[num3];
                }
                StaticTile[] tileBuffer = m_TileBuffer;
                fixed (StaticTile* tileRef = tileBuffer)
                {
                    _lread(this.m_Statics.Handle, (void*) tileRef, wBytes);
                    if (m_Lists == null)
                    {
                        m_Lists = new TileList[8][];
                        for (int j = 0; j < 8; j++)
                        {
                            m_Lists[j] = new TileList[8];
                            for (int k = 0; k < 8; k++)
                            {
                                m_Lists[j][k] = new TileList();
                            }
                        }
                    }
                    TileList[][] lists = m_Lists;
                    StaticTile* tilePtr = tileRef;
                    StaticTile* tilePtr2 = tileRef + num3;
                    while (tilePtr < tilePtr2)
                    {
                        lists[tilePtr->m_X & 7][tilePtr->m_Y & 7].Add((short) ((tilePtr->m_ID & 0x3fff) + 0x4000), tilePtr->m_Z);
                        tilePtr++;
                    }
                    Tile[][][] tileArray2 = new Tile[8][][];
                    for (int i = 0; i < 8; i++)
                    {
                        tileArray2[i] = new Tile[8][];
                        for (int m = 0; m < 8; m++)
                        {
                            tileArray2[i][m] = lists[i][m].ToArray();
                        }
                    }
                    emptyStaticBlock = tileArray2;
                }
            }
            catch (EndOfStreamException)
            {
                if (DateTime.Now >= this.m_NextStaticWarning)
                {
                    Console.WriteLine("Warning: Static EOS for {0} ({1}, {2})", this.m_Owner, x, y);
                    this.m_NextStaticWarning = DateTime.Now + TimeSpan.FromMinutes(1.0);
                }
                emptyStaticBlock = this.m_EmptyStaticBlock;
            }
            return emptyStaticBlock;
        }

        public void SetLandBlock(int x, int y, Tile[] value)
        {
            if (((x >= 0) && (y >= 0)) && ((x < this.m_BlockWidth) && (y < this.m_BlockHeight)))
            {
                int[] numArray;
                IntPtr ptr;
                if (this.m_LandTiles[x] == null)
                {
                    this.m_LandTiles[x] = new Tile[this.m_BlockHeight][];
                }
                this.m_LandTiles[x][y] = value;
                if (this.m_LandPatches[x] == null)
                {
                    this.m_LandPatches[x] = new int[(this.m_BlockHeight + 0x1f) >> 5];
                }
                (numArray = this.m_LandPatches[x])[(int) (ptr = (IntPtr) (y >> 5))] = numArray[(int) ptr] | (((int) 1) << y);
            }
        }

        public void SetStaticBlock(int x, int y, Tile[][][] value)
        {
            if (((x >= 0) && (y >= 0)) && ((x < this.m_BlockWidth) && (y < this.m_BlockHeight)))
            {
                int[] numArray;
                IntPtr ptr;
                if (this.m_StaticTiles[x] == null)
                {
                    this.m_StaticTiles[x] = new Tile[this.m_BlockHeight][][][];
                }
                this.m_StaticTiles[x][y] = value;
                if (this.m_StaticPatches[x] == null)
                {
                    this.m_StaticPatches[x] = new int[(this.m_BlockHeight + 0x1f) >> 5];
                }
                (numArray = this.m_StaticPatches[x])[(int) (ptr = (IntPtr) (y >> 5))] = numArray[(int) ptr] | (((int) 1) << y);
            }
        }

        public int BlockHeight
        {
            get
            {
                return this.m_BlockHeight;
            }
        }

        public int BlockWidth
        {
            get
            {
                return this.m_BlockWidth;
            }
        }

        public FileStream DataStream
        {
            get
            {
                return this.m_Statics;
            }
            set
            {
                this.m_Statics = value;
            }
        }

        public Tile[][][] EmptyStaticBlock
        {
            get
            {
                return this.m_EmptyStaticBlock;
            }
        }

        public bool Exists
        {
            get
            {
                return (((this.m_Map != null) && (this.m_Index != null)) && (this.m_Statics != null));
            }
        }

        public int Height
        {
            get
            {
                return this.m_Height;
            }
        }

        public BinaryReader IndexReader
        {
            get
            {
                return this.m_IndexReader;
            }
            set
            {
                this.m_IndexReader = value;
            }
        }

        public FileStream IndexStream
        {
            get
            {
                return this.m_Index;
            }
            set
            {
                this.m_Index = value;
            }
        }

        public FileStream MapStream
        {
            get
            {
                return this.m_Map;
            }
            set
            {
                this.m_Map = value;
            }
        }

        public Map Owner
        {
            get
            {
                return this.m_Owner;
            }
        }

        public TileMatrixPatch Patch
        {
            get
            {
                return this.m_Patch;
            }
        }

        public int Width
        {
            get
            {
                return this.m_Width;
            }
        }
    }
}

