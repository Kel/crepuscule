﻿namespace Server
{
    using System;

    public class Notoriety
    {
        public const int Ally = 2;
        public const int CanBeAttacked = 3;
        public const int Criminal = 4;
        public const int Enemy = 5;
        public const int Innocent = 1;
        public const int Invulnerable = 7;
        private static NotorietyHandler m_Handler;
        private static int[] m_Hues = new int[] { 0, 0x59, 0x3f, 0x3b2, 0x3b2, 0x90, 0x22, 0x35 };
        public const int Murderer = 6;

        public static int Compute(Mobile source, Mobile target)
        {
            if (m_Handler != null)
            {
                return m_Handler(source, target);
            }
            return 3;
        }

        public static int GetHue(int noto)
        {
            if ((noto >= 0) && (noto < m_Hues.Length))
            {
                return m_Hues[noto];
            }
            return 0;
        }

        public static NotorietyHandler Handler
        {
            get
            {
                return m_Handler;
            }
            set
            {
                m_Handler = value;
            }
        }

        public static int[] Hues
        {
            get
            {
                return m_Hues;
            }
            set
            {
                m_Hues = value;
            }
        }
    }
}

