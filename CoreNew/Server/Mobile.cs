﻿namespace Server
{
    using Server.Accounting;
    using Server.ContextMenus;
    using Server.Guilds;
    using Server.Gumps;
    using Server.HuePickers;
    using Server.Items;
    using Server.Menus;
    using Server.Mobiles;
    using Server.Movement;
    using Server.Network;
    using Server.Prompts;
    using Server.Targeting;
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Collections.Generic;

    public delegate bool AllowBeneficialHandler(Mobile from, Mobile target);
    public delegate bool AllowHarmfulHandler(Mobile from, Mobile target);

    public class Mobile : IEntity, IPoint3D, IPoint2D, IHued
    {
        internal int m_TypeRef;

        private Server.AccessLevel m_AccessLevel;
        private static string[] m_AccessLevelNames = new string[] { "a player", "a counselor", "a game master", "a seer", "an administrator" };
        private IAccount m_Account;
        private static TimeSpan m_ActionMessageDelay = TimeSpan.FromSeconds(0.125);
        private ArrayList m_Actions;
        private ArrayList m_Aggressed;
        private ArrayList m_Aggressors;
        private static Server.AllowBeneficialHandler m_AllowBeneficialHandler;
        private int m_AllowedStealthSteps;
        private static Server.AllowHarmfulHandler m_AllowHarmfulHandler;
        private static bool m_AsciiClickMessage = true;
        private static TimeSpan m_AutoManifestTimeout = TimeSpan.FromSeconds(5.0);
        private Timer m_AutoManifestTimer;
        private bool m_AutoPageNotify;
        private int m_BAC;
        private Container m_Backpack;
        private Server.Items.BankBox m_BankBox;
        private int m_BaseSoundID;
        private bool m_Blessed;
        private Server.Body m_Body;
        private Server.Body m_BodyMod;
        private static int m_BodyWeight = 14;
        private bool m_CanHearGhosts;
        private bool m_CanSwim;
        private bool m_CantWalk;
        private int m_ChangingCombatant;
        private Mobile m_Combatant;
        private Timer m_CombatTimer;
        private Server.ContextMenus.ContextMenu m_ContextMenu;
        private Container m_Corpse;
        private static Server.CreateCorpseHandler m_CreateCorpse;
        private DateTime m_CreationTime;
        private bool m_Criminal;
        private ArrayList m_DamageEntries;
        private static TimeSpan m_DefaultHitsRate;
        private static TimeSpan m_DefaultManaRate;
        private static TimeSpan m_DefaultStamRate;
        private static IWeapon m_DefaultWeapon;
        private bool m_Deleted;
        private MobileDelta m_DeltaFlags;
        private static Queue m_DeltaQueue = new Queue();
        private int m_Dex;
        private StatLockType m_DexLock;
        private Server.Direction m_Direction;
        private static bool m_DisableDismountInWarmode;
        private static bool m_DisableHiddenSelfClick = true;
        private bool m_DisarmReady;
        private bool m_DisplayGuildTitle;
        private static bool m_DragEffects = true;
        private int m_EmoteHue;
        private DateTime m_EndQueue;
        private Timer m_ExpireAggrTimer;
        private Timer m_ExpireCombatant;
        private Timer m_ExpireCriminal;
        private static TimeSpan m_ExpireCriminalDelay = TimeSpan.FromMinutes(2.0);
        private int m_Fame;
        private bool m_Female;
        private int m_Followers;
        private int m_FollowersMax;
        private bool m_Frozen;
        private FrozenTimer m_FrozenTimer;
        private static Server.AccessLevel m_FwdAccessOverride = Server.AccessLevel.GameMaster;
        private static bool m_FwdEnabled = true;
        private static int m_FwdMaxSteps = 4;
        private static bool m_FwdUOTDOverride = false;
        private static char[] m_GhostChars = new char[] { 'o', 'O' };
        private static object m_GhostMutateContext = new object();
        private BaseGuild m_Guild;
        private static bool m_GuildClickMessage = true;
        private Mobile m_GuildFealty;
        private string m_GuildTitle;
        private static string[] m_GuildTypes = new string[] { "", " (Chaos)", " (Order)" };
        private static ArrayList m_Hears;
        private bool m_Hidden;
        private int m_Hits;
        private static RegenRateHandler m_HitsRegenRate;
        private Timer m_HitsTimer;
        private Item m_Holding;
        private int m_Hue;
        private int m_HueMod;
        private int m_Hunger;
        private bool m_InDeltaQueue;
        private static bool m_InsuranceEnabled;
        private int m_Int;
        private StatLockType m_IntLock;
        private static int[] m_InvalidBodies = new int[] { 0x20, 0x5f, 0x9c, 0xa9, 0xc4, 0xc5, 0xc6, 0xc7 };
        private ArrayList m_Items;
        private int m_Karma;
        private int m_Kills;
        private string m_Language;
        private Mobile m_LastKiller;
        private DateTime m_LastMoveTime;
        private DateTime m_LastStatGain;
        private int m_LightLevel;
        private Point3D m_Location;
        private Point3D m_LogoutLocation;
        private Server.Map m_LogoutMap;
        private Timer m_LogoutTimer;
        private int m_MagicDamageAbsorb;
        private int m_Mana;
        private static RegenRateHandler m_ManaRegenRate;
        private Timer m_ManaTimer;
        private Server.Map m_Map;
        private static int m_MaxPlayerResistance = 70;
        private bool m_Meditating;
        private int m_MeleeDamageAbsorb;
        private Item m_MountItem;
        private static ArrayList m_MoveList = new ArrayList();
        private Queue m_MoveRecords;
        private static MobileMoving[] m_MovingPacketCache = new MobileMoving[8];
        private string m_Name;
        protected string m_RealName;
        private int m_NameHue;
        private string m_NameMod;
        private Server.Network.NetState m_NetState;
        private DateTime m_NextActionMessage;
        private DateTime m_NextActionTime;
        private DateTime m_NextCombatTime;
        private DateTime m_NextSkillTime;
        private DateTime m_NextSpellTime;
        private DateTime m_NextWarmodeChange;
        private static bool m_NoSpeechLOS;
        private static ArrayList m_OnSpeech;
        private Packet m_OPLPacket;
        private bool m_Paralyzed;
        private ParalyzedTimer m_ParaTimer;
        private object m_Party;
        private bool m_Player;
        private Server.Poison m_Poison;
        private Timer m_PoisonTimer;
        private string m_Profile;
        private bool m_ProfileLocked;
        private Server.Prompts.Prompt m_Prompt;
        private ObjectPropertyList m_PropertyList;
        private bool m_Pushing;
        private Server.QuestArrow m_QuestArrow;
        private Server.Region m_Region;
        private Packet m_RemovePacket;
        private int[] m_Resistances;
        private ArrayList m_ResistMods;
        private static TimeSpan m_RunFoot = TimeSpan.FromSeconds(0.2);
        private static TimeSpan m_RunMount = TimeSpan.FromSeconds(0.1);
        private Server.Serial m_Serial;
        private int m_ShortTermMurders;
        private static Server.SkillCheckDirectLocationHandler m_SkillCheckDirectLocationHandler;
        private static Server.SkillCheckDirectTargetHandler m_SkillCheckDirectTargetHandler;
        private static Server.SkillCheckLocationHandler m_SkillCheckLocationHandler;
        private static Server.SkillCheckTargetHandler m_SkillCheckTargetHandler;
        private ArrayList m_SkillMods;
        private Server.Skills m_Skills;
        private int m_SolidHueOverride;
        private int m_SpeechHue;
        private ISpell m_Spell;
        private bool m_Squelched;
        private ArrayList m_Stabled;
        private int m_Stam;
        private static RegenRateHandler m_StamRegenRate;
        private Timer m_StamTimer;
        private int m_StatCap;
        private ArrayList m_StatMods;
        private int m_Str;
        private StatLockType m_StrLock;
        private DateTime[] m_StuckMenuUses;
        private bool m_StunReady;
        private Server.Targeting.Target m_Target;
        private bool m_TargetLocked;
        private int m_Thirst;
        private int m_TithingPoints;
        private string m_Title;
        private int m_TotalGold;
        private int m_TotalWeight;
        private int m_VirtualArmor;
        private int m_VirtualArmorMod;
        private VirtueInfo m_Virtues;
        private static Server.VisibleDamageType m_VisibleDamageType;
        private static TimeSpan m_WalkFoot = TimeSpan.FromSeconds(0.4);
        private static TimeSpan m_WalkMount = TimeSpan.FromSeconds(0.2);
        private bool m_Warmode;
        private int m_WarmodeChanges;
        private WarmodeTimer m_WarmodeTimer;
        private IWeapon m_Weapon;
        private int m_WhisperHue;
        private int m_YellHue;
        private bool m_YellowHealthbar;
        private const int WarmodeCatchCount = 4;
        private static TimeSpan WarmodeSpamCatch = TimeSpan.FromSeconds(0.5);
        private static TimeSpan WarmodeSpamDelay = TimeSpan.FromSeconds(2.0);

        #region RunUO 2 Stuff
        // Pre-7.0.0.0 Packet Flags
        public virtual int GetOldPacketFlags()
        {
            int flags = 0x0;

            if (m_Female)
                flags |= 0x02;

            if (m_Poison != null)
                flags |= 0x04;

            if (m_Blessed || m_YellowHealthbar)
                flags |= 0x08;

            if (m_Warmode)
                flags |= 0x40;

            if (m_Hidden)
                flags |= 0x80;

            return flags;
        }

        #region Hair
        private HairInfo m_Hair;
        private FacialHairInfo m_FacialHair;

        public void ConvertHair()
        {
            Item hair;

            if ((hair = FindItemOnLayer(Layer.Hair)) != null)
            {
                HairItemID = hair.ItemID;
                HairHue = hair.Hue;
                hair.Delete();
            }

            if ((hair = FindItemOnLayer(Layer.FacialHair)) != null)
            {
                FacialHairItemID = hair.ItemID;
                FacialHairHue = hair.Hue;
                hair.Delete();
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int HairItemID
        {
            get
            {
                if (m_Hair == null)
                    return 0;

                return m_Hair.ItemID;
            }
            set
            {
                if (m_Hair == null && value > 0)
                    m_Hair = new HairInfo(value);
                else if (value <= 0)
                    m_Hair = null;
                else
                    m_Hair.ItemID = value;

                Delta(MobileDelta.Hair);
            }
        }

        //		[CommandProperty( AccessLevel.GameMaster )]
        //		public int HairSerial { get { return HairInfo.FakeSerial( this ); } }

        [CommandProperty(AccessLevel.GameMaster)]
        public int FacialHairItemID
        {
            get
            {
                if (m_FacialHair == null)
                    return 0;

                return m_FacialHair.ItemID;
            }
            set
            {
                if (m_FacialHair == null && value > 0)
                    m_FacialHair = new FacialHairInfo(value);
                else if (value <= 0)
                    m_FacialHair = null;
                else
                    m_FacialHair.ItemID = value;

                Delta(MobileDelta.FacialHair);
            }
        }

        //		[CommandProperty( AccessLevel.GameMaster )]
        //		public int FacialHairSerial { get { return FacialHairInfo.FakeSerial( this ); } }

        [CommandProperty(AccessLevel.GameMaster)]
        public int HairHue
        {
            get
            {
                if (m_Hair == null)
                    return 0;
                return m_Hair.Hue;
            }
            set
            {
                if (m_Hair != null)
                {
                    m_Hair.Hue = value;
                    Delta(MobileDelta.Hair);
                }
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int FacialHairHue
        {
            get
            {
                if (m_FacialHair == null)
                    return 0;

                return m_FacialHair.Hue;
            }
            set
            {
                if (m_FacialHair != null)
                {
                    m_FacialHair.Hue = value;
                    Delta(MobileDelta.FacialHair);
                }
            }
        }

        #endregion

        public void ClearProperties()
        {
            Packet.Release(ref m_PropertyList);
            Packet.Release(ref m_OPLPacket);
        }

        public virtual void GetChildNameProperties(ObjectPropertyList list, Item item)
        {
        }

        public virtual void UpdateTotal(Item sender, TotalType type, int delta)
        {
            if (delta == 0 || sender.IsVirtualItem)
                return;

            switch (type)
            {
                case TotalType.Gold:
                    m_TotalGold += delta;
                    Delta(MobileDelta.Gold);
                    break;

                case TotalType.Items:
                    //m_TotalItems += delta;
                    break;

                case TotalType.Weight:
                    m_TotalWeight += delta;
                    Delta(MobileDelta.Weight);
                    OnWeightChange(m_TotalWeight - delta);
                    break;
            }
        }
        public virtual void OnItemBounceCleared(Item item)
        {
        }

        public virtual void OnSubItemBounceCleared( Item item )
		{
		}

        public static Item LiftItemDupe(Item oldItem, int amount)
        {
            Item item;
            try
            {
                item = (Item)Activator.CreateInstance(oldItem.GetType());
            }
            catch
            {
                Console.WriteLine("Warning: 0x{0:X}: Item must have a zero paramater constructor to be separated from a stack. '{1}'.", oldItem.Serial.Value, oldItem.GetType().Name);
                return null;
            }
            item.Visible = oldItem.Visible;
            item.Movable = oldItem.Movable;
            item.LootType = oldItem.LootType;
            item.Direction = oldItem.Direction;
            item.Hue = oldItem.Hue;
            item.ItemID = oldItem.ItemID;
            item.Location = oldItem.Location;
            item.Layer = oldItem.Layer;
            item.Name = oldItem.Name;
            item.Weight = oldItem.Weight;

            item.Amount = oldItem.Amount - amount;
            item.Map = oldItem.Map;

            oldItem.Amount = amount;
            oldItem.OnAfterDuped(item);

            if (oldItem.Parent is Mobile)
            {
                ((Mobile)oldItem.Parent).AddItem(item);
            }
            else if (oldItem.Parent is Item)
            {
                ((Item)oldItem.Parent).AddItem(item);
            }

            item.Delta(ItemDelta.Update);

            return item;
        }
        #endregion

        public Mobile()
        {
            this.m_NameHue = -1;
            this.m_WarmodeChanges = 0;
            this.m_SkillMods = new ArrayList(1);
            this.m_HueMod = -1;
            this.m_SolidHueOverride = -1;
            this.m_Region = Server.Map.Internal.DefaultRegion;
            this.m_Serial = Server.Serial.NewMobile;
            this.DefaultMobileInit();
            World.AddMobile(this);

            Type ourType = this.GetType();
            m_TypeRef = World.m_MobileTypes.IndexOf(ourType);

            if (m_TypeRef == -1)
                m_TypeRef = World.m_MobileTypes.Add(ourType);
        }

        public Mobile(Server.Serial serial)
        {
            this.m_NameHue = -1;
            this.m_WarmodeChanges = 0;
            this.m_SkillMods = new ArrayList(1);
            this.m_HueMod = -1;
            this.m_SolidHueOverride = -1;
            this.m_Region = Server.Map.Internal.DefaultRegion;
            this.m_Serial = serial;
            this.m_ExpireCombatant = new ExpireCombatantTimer(this);
            this.m_ExpireCriminal = new ExpireCriminalTimer(this);
            this.m_CombatTimer = new CombatTimer(this);
            this.m_Aggressors = new ArrayList(1);
            this.m_Aggressed = new ArrayList(1);
            this.m_NextSkillTime = DateTime.MinValue;
            this.m_DamageEntries = new ArrayList(1);
            this.m_LogoutTimer = new LogoutTimer(this);

            Type ourType = this.GetType();
            m_TypeRef = World.m_MobileTypes.IndexOf(ourType);

            if (m_TypeRef == -1)
                m_TypeRef = World.m_MobileTypes.Add(ourType);
        }

        public void AddItem(Item item)
        {
            if (((item != null) && !item.Deleted) && (item.Parent != this))
            {
                if (item.Parent is Mobile)
                {
                    ((Mobile) item.Parent).RemoveItem(item);
                }
                else if (item.Parent is Item)
                {
                    ((Item) item.Parent).RemoveItem(item);
                }
                else
                {
                    item.SendRemovePacket();
                }
                item.Parent = this;
                item.Map = this.m_Map;
                this.m_Items.Add(item);
                if (!(item is Server.Items.BankBox))
                {
                    this.TotalWeight += item.TotalWeight + item.PileWeight;
                    this.TotalGold += item.TotalGold;
                }
                item.Delta(ItemDelta.Update);
                item.OnAdded(this);
                this.OnItemAdded(item);
                if (((item.PhysicalResistance != 0) || (item.FireResistance != 0)) || (((item.ColdResistance != 0) || (item.PoisonResistance != 0)) || (item.EnergyResistance != 0)))
                {
                    this.UpdateResistances();
                }
            }
        }

        public virtual void AddNameProperties(ObjectPropertyList list)
        {
            string name = this.Name;
            if (name == null)
            {
                name = string.Empty;
            }
            string str2 = "";
            if ((this.ShowFameTitle && (this.m_Player || this.m_Body.IsHuman)) && (this.m_Fame >= 0x2710))
            {
                str2 = this.m_Female ? "Lady" : "Lord";
            }
            string title = "";
            if ((this.ClickTitle && (this.Title != null)) && (this.Title.Length > 0))
            {
                title = this.Title;
            }
            BaseGuild guild = this.m_Guild;
            if ((guild != null) && (this.m_Player || this.m_DisplayGuildTitle))
            {
                if (title.Length > 0)
                {
                    title = string.Format("{0} [{1}]", title, Utility.FixHtml(guild.Abbreviation));
                }
                else
                {
                    title = string.Format("[{0}]", Utility.FixHtml(guild.Abbreviation));
                }
            }
            list.Add(0x1005bd, "{0} \t{1}\t {2}", str2, name, title);
            if ((guild != null) && (this.m_DisplayGuildTitle || (this.m_Player && (guild.Type != GuildType.None))))
            {
                string str4;
                if ((guild.Type >= GuildType.None) && ((int)guild.Type < m_GuildTypes.Length))
                {
                    str4 = m_GuildTypes[(int) guild.Type];
                }
                else
                {
                    str4 = "";
                }
                string guildTitle = this.GuildTitle;
                if (guildTitle == null)
                {
                    guildTitle = "";
                }
                else
                {
                    guildTitle = guildTitle.Trim();
                }
                if (guildTitle.Length > 0)
                {
                    list.Add("{0}, {1} Guild{2}", Utility.FixHtml(guildTitle), Utility.FixHtml(guild.Name), str4);
                }
                else
                {
                    list.Add(Utility.FixHtml(guild.Name));
                }
            }
        }

        public virtual void AddResistanceMod(ResistanceMod toAdd)
        {
            if (this.m_ResistMods == null)
            {
                this.m_ResistMods = new ArrayList(2);
            }
            this.m_ResistMods.Add(toAdd);
            this.UpdateResistances();
        }

        public virtual void AddSkillMod(SkillMod mod)
        {
            if (mod != null)
            {
                this.ValidateSkillMods();
                if (!this.m_SkillMods.Contains(mod))
                {
                    this.m_SkillMods.Add(mod);
                    mod.Owner = this;
                    Skill skill = this.m_Skills[mod.Skill];
                    if (skill != null)
                    {
                        skill.Update();
                    }
                }
            }
        }

        private void AddSpeechItemsFrom(ArrayList list, Container cont)
        {
            for (int i = 0; i < cont.Items.Count; i++)
            {
                Item item = (Item) cont.Items[i];
                if (item.HandlesOnSpeech)
                {
                    list.Add(item);
                }
                if (item is Container)
                {
                    this.AddSpeechItemsFrom(list, (Container) item);
                }
            }
        }

        public void AddStatMod(StatMod mod)
        {
            for (int i = 0; i < this.m_StatMods.Count; i++)
            {
                StatMod mod2 = (StatMod) this.m_StatMods[i];
                if (mod2.Name == mod.Name)
                {
                    this.Delta(MobileDelta.Stat);
                    this.m_StatMods.RemoveAt(i);
                    break;
                }
            }
            this.m_StatMods.Add(mod);
            this.Delta(MobileDelta.Stat);
            this.CheckStatTimers();
        }

        public bool AddToBackpack(Item item)
        {
            if (item.Deleted)
            {
                return false;
            }
            if (!this.PlaceInBackpack(item))
            {
                item.MoveToWorld(this.Location, this.Map);
                return false;
            }
            return true;
        }

        public virtual void AggressiveAction(Mobile aggressor)
        {
            this.AggressiveAction(aggressor, false);
        }

        public virtual void AggressiveAction(Mobile aggressor, bool criminal)
        {
            if (aggressor != this)
            {
                AggressiveActionEventArgs e = AggressiveActionEventArgs.Create(this, aggressor, criminal);
                EventSink.InvokeAggressiveAction(e);
                e.Free();
                if (this.Combatant == aggressor)
                {
                    this.m_ExpireCombatant.Stop();
                    this.m_ExpireCombatant.Start();
                }
                bool flag = true;
                ArrayList aggressors = this.m_Aggressors;
                for (int i = 0; i < aggressors.Count; i++)
                {
                    AggressorInfo info = (AggressorInfo) aggressors[i];
                    if (info.Attacker == aggressor)
                    {
                        info.Refresh();
                        info.CriminalAggression = criminal;
                        info.CanReportMurder = criminal;
                        flag = false;
                    }
                }
                aggressors = aggressor.m_Aggressors;
                for (int j = 0; j < aggressors.Count; j++)
                {
                    AggressorInfo info2 = (AggressorInfo) aggressors[j];
                    if (info2.Attacker == this)
                    {
                        info2.Refresh();
                        flag = false;
                    }
                }
                bool flag2 = true;
                aggressors = this.m_Aggressed;
                for (int k = 0; k < aggressors.Count; k++)
                {
                    AggressorInfo info3 = (AggressorInfo) aggressors[k];
                    if (info3.Defender == aggressor)
                    {
                        info3.Refresh();
                        flag2 = false;
                    }
                }
                aggressors = aggressor.m_Aggressed;
                for (int m = 0; m < aggressors.Count; m++)
                {
                    AggressorInfo info4 = (AggressorInfo) aggressors[m];
                    if (info4.Defender == this)
                    {
                        info4.Refresh();
                        info4.CriminalAggression = criminal;
                        info4.CanReportMurder = criminal;
                        flag2 = false;
                    }
                }
                bool flag3 = false;
                if (flag)
                {
                    this.m_Aggressors.Add(AggressorInfo.Create(aggressor, this, criminal));
                    if (this.CanSee(aggressor) && (this.m_NetState != null))
                    {
                        this.m_NetState.Send(new MobileIncoming(this, aggressor));
                    }
                    if (this.Combatant == null)
                    {
                        flag3 = true;
                    }
                    this.UpdateAggrExpire();
                }
                if (flag2)
                {
                    aggressor.m_Aggressed.Add(AggressorInfo.Create(aggressor, this, criminal));
                    if (this.CanSee(aggressor) && (this.m_NetState != null))
                    {
                        this.m_NetState.Send(new MobileIncoming(this, aggressor));
                    }
                    if (this.Combatant == null)
                    {
                        flag3 = true;
                    }
                    this.UpdateAggrExpire();
                }
                if (flag3)
                {
                    this.Combatant = aggressor;
                }
                this.Region.OnAggressed(aggressor, this, criminal);
            }
        }

        public virtual bool AllowItemUse(Item item)
        {
            return true;
        }

        public virtual bool AllowSkillUse(SkillName name)
        {
            return true;
        }

        public virtual void Animate(int action, int frameCount, int repeatCount, bool forward, bool repeat, int delay)
        {
            Server.Map map = this.m_Map;
            if (map != null)
            {
                this.ProcessDelta();
                Packet p = null;
                IPooledEnumerable clientsInRange = map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if (state.Mobile.CanSee(this))
                    {
                        state.Mobile.ProcessDelta();
                        if (p == null)
                        {
                            p = new MobileAnimation(this, action, frameCount, repeatCount, forward, repeat, delay);
                        }
                        state.Send(p);
                    }
                }
                clientsInRange.Free();
            }
        }

        public virtual ApplyPoisonResult ApplyPoison(Mobile from, Server.Poison poison)
        {
            if (poison == null)
            {
                this.CurePoison(from);
                return ApplyPoisonResult.Cured;
            }
            if (this.CheckHigherPoison(from, poison))
            {
                this.OnHigherPoison(from, poison);
                return ApplyPoisonResult.HigherPoisonActive;
            }
            if (this.CheckPoisonImmunity(from, poison))
            {
                this.OnPoisonImmunity(from, poison);
                return ApplyPoisonResult.Immune;
            }
            Server.Poison oldPoison = this.m_Poison;
            this.Poison = poison;
            this.OnPoisoned(from, poison, oldPoison);
            return ApplyPoisonResult.Poisoned;
        }

        public virtual void Attack(Mobile m)
        {
            if (this.CheckAttack(m))
            {
                this.Combatant = m;
            }
        }

        public bool BeginAction(object toLock)
        {
            if (this.m_Actions == null)
            {
                this.m_Actions = new ArrayList(2);
                this.m_Actions.Add(toLock);
                return true;
            }
            if (!this.m_Actions.Contains(toLock))
            {
                this.m_Actions.Add(toLock);
                return true;
            }
            return false;
        }

        public Server.Targeting.Target BeginTarget(int range, bool allowGround, TargetFlags flags, TargetCallback callback)
        {
            Server.Targeting.Target target = new SimpleTarget(range, flags, allowGround, callback);
            this.Target = target;
            return target;
        }

        public Server.Targeting.Target BeginTarget(int range, bool allowGround, TargetFlags flags, TargetStateCallback callback, object state)
        {
            Server.Targeting.Target target = new SimpleStateTarget(range, flags, allowGround, callback, state);
            this.Target = target;
            return target;
        }

        public virtual bool BeneficialCheck(Mobile target)
        {
            if (this.CanBeBeneficial(target, true))
            {
                this.DoBeneficial(target);
                return true;
            }
            return false;
        }

        public void BoltEffect(int hue)
        {
            Effects.SendBoltEffect(this, true, hue);
        }

        public virtual bool CanBeBeneficial(Mobile target)
        {
            return this.CanBeBeneficial(target, true, false);
        }

        public virtual bool CanBeBeneficial(Mobile target, bool message)
        {
            return this.CanBeBeneficial(target, message, false);
        }

        public virtual bool CanBeBeneficial(Mobile target, bool message, bool allowDead)
        {
            if (target != null)
            {
                if (((this.m_Deleted || target.m_Deleted) || (!this.Alive || this.IsDeadBondedPet)) || (!allowDead && (!target.Alive || this.IsDeadBondedPet)))
                {
                    if (message)
                    {
                        this.SendLocalizedMessage(0xf4639);
                    }
                    return false;
                }
                if (target == this)
                {
                    return true;
                }
                if (this.Region.AllowBenificial(this, target))
                {
                    return true;
                }
                if (message)
                {
                    this.SendLocalizedMessage(0xf4639);
                }
            }
            return false;
        }

        public virtual bool CanBeDamaged()
        {
            return !this.m_Blessed;
        }

        public bool CanBeginAction(object toLock)
        {
            if (this.m_Actions != null)
            {
                return !this.m_Actions.Contains(toLock);
            }
            return true;
        }

        public virtual bool CanBeHarmful(Mobile target)
        {
            return this.CanBeHarmful(target, true);
        }

        public virtual bool CanBeHarmful(Mobile target, bool message)
        {
            return this.CanBeHarmful(target, message, false);
        }

        public virtual bool CanBeHarmful(Mobile target, bool message, bool ignoreOurBlessedness)
        {
            if (target != null)
            {
                if ((this.m_Deleted || (!ignoreOurBlessedness && this.m_Blessed)) || (((target.m_Deleted || target.m_Blessed) || (!this.Alive || this.IsDeadBondedPet)) || (!target.Alive || target.IsDeadBondedPet)))
                {
                    if (message)
                    {
                        this.SendLocalizedMessage(0xf463a);
                    }
                    return false;
                }
                if (target == this)
                {
                    return true;
                }
                if (this.Region.AllowHarmful(this, target))
                {
                    return true;
                }
                if (message)
                {
                    this.SendLocalizedMessage(0xf463a);
                }
            }
            return false;
        }

        public virtual bool CanBeRenamedBy(Mobile from)
        {
            return (from.m_AccessLevel > this.m_AccessLevel);
        }

        public virtual bool CanPaperdollBeOpenedBy(Mobile from)
        {
            if (!this.Body.IsHuman && !this.Body.IsGhost)
            {
                return this.IsBodyMod;
            }
            return true;
        }

        public virtual bool CanSee(Item item)
        {
            if (this.m_Map == Server.Map.Internal)
            {
                return false;
            }
            if (item.Map == Server.Map.Internal)
            {
                return false;
            }
            if (item.Parent != null)
            {
                if (item.Parent is Item)
                {
                    if (!this.CanSee((Item) item.Parent))
                    {
                        return false;
                    }
                }
                else if ((item.Parent is Mobile) && !this.CanSee((Mobile) item.Parent))
                {
                    return false;
                }
            }
            if (item is Server.Items.BankBox)
            {
                Server.Items.BankBox box = item as Server.Items.BankBox;
                if (((box != null) && (this.m_AccessLevel <= Server.AccessLevel.Counselor)) && ((box.Owner != this) || !box.Opened))
                {
                    return false;
                }
            }
            else if (item is SecureTradeContainer)
            {
                SecureTrade trade = ((SecureTradeContainer) item).Trade;
                if (((trade != null) && (trade.From.Mobile != this)) && (trade.To.Mobile != this))
                {
                    return false;
                }
            }
            if (item.Deleted || (item.Map != this.m_Map))
            {
                return false;
            }
            if (!item.Visible)
            {
                return (this.m_AccessLevel > Server.AccessLevel.Counselor);
            }
            return true;
        }

        public virtual bool CanSee(Mobile m)
        {
            if ((this.m_Deleted || m.m_Deleted) || ((this.m_Map == Server.Map.Internal) || (m.m_Map == Server.Map.Internal)))
            {
                return false;
            }
            if (this != m)
            {
                if ((m.m_Map != this.m_Map) || (m.Hidden && (this.m_AccessLevel <= m.AccessLevel)))
                {
                    return false;
                }
                if ((!m.Alive && this.Alive) && (this.m_AccessLevel <= Server.AccessLevel.Player))
                {
                    return m.Warmode;
                }
            }
            return true;
        }

        public virtual bool CanSee(object o)
        {
            if (o is Item)
            {
                return this.CanSee((Item) o);
            }
            if (o is Mobile)
            {
                return this.CanSee((Mobile) o);
            }
            return true;
        }

        public bool CanUseStuckMenu()
        {
            if (this.m_StuckMenuUses == null)
            {
                return true;
            }
            for (int i = 0; i < this.m_StuckMenuUses.Length; i++)
            {
                if ((DateTime.Now - this.m_StuckMenuUses[i]) > TimeSpan.FromDays(1.0))
                {
                    return true;
                }
            }
            return false;
        }

        private void CheckAggrExpire()
        {
            for (int i = this.m_Aggressors.Count - 1; i >= 0; i--)
            {
                if (i < this.m_Aggressors.Count)
                {
                    AggressorInfo info = (AggressorInfo) this.m_Aggressors[i];
                    if (info.Expired)
                    {
                        Mobile attacker = info.Attacker;
                        attacker.RemoveAggressed(this);
                        this.m_Aggressors.RemoveAt(i);
                        info.Free();
                        if (((this.m_NetState != null) && this.CanSee(attacker)) && Utility.InUpdateRange(this.m_Location, attacker.m_Location))
                        {
                            this.m_NetState.Send(new MobileIncoming(this, attacker));
                        }
                    }
                }
            }
            for (int j = this.m_Aggressed.Count - 1; j >= 0; j--)
            {
                if (j < this.m_Aggressed.Count)
                {
                    AggressorInfo info2 = (AggressorInfo) this.m_Aggressed[j];
                    if (info2.Expired)
                    {
                        Mobile defender = info2.Defender;
                        defender.RemoveAggressor(this);
                        this.m_Aggressed.RemoveAt(j);
                        info2.Free();
                        if (((this.m_NetState != null) && this.CanSee(defender)) && Utility.InUpdateRange(this.m_Location, defender.m_Location))
                        {
                            this.m_NetState.Send(new MobileIncoming(this, defender));
                        }
                    }
                }
            }
            this.UpdateAggrExpire();
        }

        public bool CheckAlive()
        {
            return this.CheckAlive(true);
        }

        public bool CheckAlive(bool message)
        {
            if (this.Alive)
            {
                return true;
            }
            if (message)
            {
                this.LocalOverheadMessage(MessageType.Regular, 0x3b2, 0xf8ca8);
            }
            return false;
        }

        public virtual bool CheckAttack(Mobile m)
        {
            return ((Utility.InUpdateRange(this, m) && this.CanSee(m)) && this.InLOS(m));
        }

        public virtual bool CheckContextMenuDisplay(IEntity target)
        {
            return true;
        }

        public virtual bool CheckCure(Mobile from)
        {
            return true;
        }

        public virtual bool CheckEquip(Item item)
        {
            for (int i = 0; i < this.m_Items.Count; i++)
            {
                if (((Item) this.m_Items[i]).CheckConflictingLayer(this, item, item.Layer) || item.CheckConflictingLayer(this, (Item) this.m_Items[i], ((Item) this.m_Items[i]).Layer))
                {
                    return false;
                }
            }
            return true;
        }

        public virtual bool CheckHearsMutatedSpeech(Mobile m, object context)
        {
            return ((context != m_GhostMutateContext) || (m.Alive && !m.CanHearGhosts));
        }

        public virtual bool CheckHigherPoison(Mobile from, Server.Poison poison)
        {
            return ((this.m_Poison != null) && (this.m_Poison.Level >= poison.Level));
        }

        public virtual bool CheckItemUse(Mobile from, Item item)
        {
            return true;
        }

        public virtual bool CheckLift(Mobile from, Item item, ref LRReason reject)
        {
            return true;
        }


        public virtual void CheckLightLevels(bool forceResend)
        {
        }

        public virtual bool CheckMovement(Server.Direction d, out int newZ)
        {
            return Server.Movement.Movement.CheckMovement(this, d, out newZ);
        }

        public virtual bool CheckNonlocalDrop(Mobile from, Item item, Item target)
        {
            if ((from != this) && ((from.AccessLevel <= this.AccessLevel) || (from.AccessLevel < Server.AccessLevel.GameMaster)))
            {
                return false;
            }
            return true;
        }

        public virtual bool CheckNonlocalLift(Mobile from, Item item)
        {
            if ((from != this) && ((from.AccessLevel <= this.AccessLevel) || (from.AccessLevel < Server.AccessLevel.GameMaster)))
            {
                return false;
            }
            return true;
        }

        public virtual bool CheckPoisonImmunity(Mobile from, Server.Poison poison)
        {
            return false;
        }

        public virtual bool CheckResurrect()
        {
            return true;
        }

        public bool CheckSkill(SkillName skill, double chance)
        {
            if (m_SkillCheckDirectLocationHandler == null)
            {
                return false;
            }
            return m_SkillCheckDirectLocationHandler(this, skill, chance);
        }

        public bool CheckSkill(SkillName skill, double minSkill, double maxSkill)
        {
            if (m_SkillCheckLocationHandler == null)
            {
                return false;
            }
            return m_SkillCheckLocationHandler(this, skill, minSkill, maxSkill);
        }

        public virtual bool CheckSpeechManifest()
        {
            if (this.Alive)
            {
                return false;
            }
            TimeSpan autoManifestTimeout = m_AutoManifestTimeout;
            if ((autoManifestTimeout <= TimeSpan.Zero) || (this.Warmode && (this.m_AutoManifestTimer == null)))
            {
                return false;
            }
            this.Manifest(autoManifestTimeout);
            return true;
        }

        public virtual bool CheckSpellCast(ISpell spell)
        {
            return true;
        }

        public virtual void CheckStatTimers()
        {
            if (this.Hits < this.HitsMax)
            {
                if (this.m_HitsTimer == null)
                {
                    this.m_HitsTimer = new HitsTimer(this);
                }
                this.m_HitsTimer.Start();
            }
            else
            {
                this.Hits = this.HitsMax;
            }
            if (this.Stam < this.StamMax)
            {
                if (this.m_StamTimer == null)
                {
                    this.m_StamTimer = new StamTimer(this);
                }
                this.m_StamTimer.Start();
            }
            else
            {
                this.Stam = this.StamMax;
            }
            if (this.Mana < this.ManaMax)
            {
                if (this.m_ManaTimer == null)
                {
                    this.m_ManaTimer = new ManaTimer(this);
                }
                this.m_ManaTimer.Start();
            }
            else
            {
                this.Mana = this.ManaMax;
            }
        }

        public virtual bool CheckTarget(Mobile from, Server.Targeting.Target targ, object targeted)
        {
            return true;
        }

        public bool CheckTargetSkill(SkillName skill, object target, double chance)
        {
            if (m_SkillCheckDirectTargetHandler == null)
            {
                return false;
            }
            return m_SkillCheckDirectTargetHandler(this, skill, target, chance);
        }

        public bool CheckTargetSkill(SkillName skill, object target, double minSkill, double maxSkill)
        {
            if (m_SkillCheckTargetHandler == null)
            {
                return false;
            }
            return m_SkillCheckTargetHandler(this, skill, target, minSkill, maxSkill);
        }

        public virtual void ClearFastwalkStack()
        {
            if ((this.m_MoveRecords != null) && (this.m_MoveRecords.Count > 0))
            {
                this.m_MoveRecords.Clear();
            }
            this.m_EndQueue = DateTime.Now;
        }

        public virtual void ClearHand(Item item)
        {
            if (((item != null) && item.Movable) && !item.AllowEquipedCast(this))
            {
                this.AddToBackpack(item);
            }
        }

        public virtual void ClearHands()
        {
            this.ClearHand(this.FindItemOnLayer(Layer.FirstValid));
            this.ClearHand(this.FindItemOnLayer(Layer.TwoHanded));
        }

        public void ClearQuestArrow()
        {
            this.m_QuestArrow = null;
        }

        public void ClearScreen()
        {
            Server.Network.NetState netState = this.m_NetState;
            if ((this.m_Map != null) && (netState != null))
            {
                IPooledEnumerable objectsInRange = this.m_Map.GetObjectsInRange(this.m_Location, Core.GlobalMaxUpdateRange);
                foreach (object obj2 in objectsInRange)
                {
                    if (obj2 is Mobile)
                    {
                        Mobile mobile = (Mobile) obj2;
                        if ((mobile != this) && Utility.InUpdateRange(this.m_Location, mobile.m_Location))
                        {
                            netState.Send(mobile.RemovePacket);
                        }
                    }
                    else if (obj2 is Item)
                    {
                        Item item = (Item) obj2;
                        if (this.InRange(item.Location, item.GetUpdateRange(this)))
                        {
                            netState.Send(item.RemovePacket);
                        }
                    }
                }
                objectsInRange.Free();
            }
        }

        public void ClearTarget()
        {
            this.m_Target = null;
        }

        public bool CloseAllGumps()
        {
            return this.CloseAllGumps(false);
        }

        public bool CloseAllGumps(bool throwOnOffline)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                var gumps = netState.Gumps;
                foreach(Gump gump in gumps)
                {
                    netState.Send(new Server.Network.CloseGump(gump.TypeID, 0));
                }
                return true;
            }
            if (throwOnOffline)
            {
                throw new MobileNotConnectedException(this, "Gump close packets could not be sent.");
            }
            return false;
        }

        public bool CloseGump(Type type)
        {
            return this.CloseGump(type, 0, false);
        }

        public bool CloseGump(Type type, int buttonID)
        {
            return this.CloseGump(type, buttonID, false);
        }

        public bool CloseGump(Type type, int buttonID, bool throwOnOffline)
        {
            if (this.m_NetState != null)
            {
                this.m_NetState.Send(new Server.Network.CloseGump(Gump.GetTypeID(type), buttonID));
                return true;
            }
            if (throwOnOffline)
            {
                throw new MobileNotConnectedException(this, "Gump close packet could not be sent.");
            }
            return false;
        }

        public virtual void ComputeBaseLightLevels(out int global, out int personal)
        {
            global = 0;
            personal = this.m_LightLevel;
        }

        public virtual void ComputeLightLevels(out int global, out int personal)
        {
            this.ComputeBaseLightLevels(out global, out personal);
            if (this.m_Region != null)
            {
                this.m_Region.AlterLightLevel(this, ref global, ref personal);
            }
        }

        public virtual void ComputeResistances()
        {
            int[] numArray;
            if (this.m_Resistances == null)
            {
                this.m_Resistances = new int[] { -2147483648, -2147483648, -2147483648, -2147483648, -2147483648 };
            }
            for (int i = 0; i < this.m_Resistances.Length; i++)
            {
                this.m_Resistances[i] = 0;
            }
            (numArray = this.m_Resistances)[0] = numArray[0] + this.BasePhysicalResistance;
            (numArray = this.m_Resistances)[1] = numArray[1] + this.BaseFireResistance;
            (numArray = this.m_Resistances)[2] = numArray[2] + this.BaseColdResistance;
            (numArray = this.m_Resistances)[3] = numArray[3] + this.BasePoisonResistance;
            (numArray = this.m_Resistances)[4] = numArray[4] + this.BaseEnergyResistance;
            for (int j = 0; (this.m_ResistMods != null) && (j < this.m_ResistMods.Count); j++)
            {
                ResistanceMod mod = (ResistanceMod) this.m_ResistMods[j];
                int type = (int) mod.Type;
                if ((type >= 0) && (type < this.m_Resistances.Length))
                {
                    IntPtr ptr;
                    (numArray = this.m_Resistances)[(int) (ptr = (IntPtr) type)] = numArray[(int) ptr] + mod.Offset;
                }
            }
            for (int k = 0; k < this.m_Items.Count; k++)
            {
                Item item = (Item) this.m_Items[k];
                if (!item.CheckPropertyConfliction(this))
                {
                    (numArray = this.m_Resistances)[0] = numArray[0] + item.PhysicalResistance;
                    (numArray = this.m_Resistances)[1] = numArray[1] + item.FireResistance;
                    (numArray = this.m_Resistances)[2] = numArray[2] + item.ColdResistance;
                    (numArray = this.m_Resistances)[3] = numArray[3] + item.PoisonResistance;
                    (numArray = this.m_Resistances)[4] = numArray[4] + item.EnergyResistance;
                }
            }
            for (int m = 0; m < this.m_Resistances.Length; m++)
            {
                int minResistance = this.GetMinResistance((ResistanceType) m);
                int maxResistance = this.GetMaxResistance((ResistanceType) m);
                if (maxResistance < minResistance)
                {
                    maxResistance = minResistance;
                }
                if (this.m_Resistances[m] > maxResistance)
                {
                    this.m_Resistances[m] = maxResistance;
                }
                else if (this.m_Resistances[m] < minResistance)
                {
                    this.m_Resistances[m] = minResistance;
                }
            }
        }

        public virtual void CriminalAction(bool message)
        {
            this.Criminal = true;
            this.m_Region.OnCriminalAction(this, message);
        }

        public virtual bool CurePoison(Mobile from)
        {
            if (this.CheckCure(from))
            {
                Server.Poison oldPoison = this.m_Poison;
                this.Poison = null;
                this.OnCured(from, oldPoison);
                return true;
            }
            this.OnFailedCure(from);
            return false;
        }

        public virtual void Damage(int amount)
        {
            this.Damage(amount, null);
        }

        public virtual void Damage(int amount, Mobile from)
        {
            if ((this.CanBeDamaged() && this.Region.OnDamage(this, ref amount)) && (amount > 0))
            {
                int hits = this.Hits;
                int num2 = hits - amount;
                if (this.m_Spell != null)
                {
                    this.m_Spell.OnCasterHurt();
                }
                if (from != null)
                {
                    this.RegisterDamage(amount, from);
                }
                this.DisruptiveAction();
                this.Paralyzed = false;
                switch (m_VisibleDamageType)
                {
                    case Server.VisibleDamageType.Related:
                    {
                        Server.Network.NetState netState = this.m_NetState;
                        Server.Network.NetState state2 = (from == null) ? null : from.m_NetState;
                        if ((amount > 0) && ((netState != null) || (state2 != null)))
                        {
                            Packet p = new DamagePacket(this, amount);
                            if (netState != null)
                            {
                                netState.Send(p);
                            }
                            if ((state2 != null) && (state2 != netState))
                            {
                                state2.Send(p);
                            }
                        }
                        break;
                    }
                    case Server.VisibleDamageType.Everyone:
                        this.SendDamageToAll(amount);
                        break;
                }
                this.OnDamage(amount, from, num2 < 0);
                if (num2 < 0)
                {
                    this.m_LastKiller = from;
                    this.Hits = 0;
                    if (hits >= 0)
                    {
                        this.Kill();
                    }
                }
                else
                {
                    this.Hits = num2;
                }
            }
        }

        public void DefaultMobileInit()
        {
            this.m_StatCap = 0xe1;
            this.m_FollowersMax = 5;
            this.m_Skills = new Server.Skills(this);
            this.m_Items = new ArrayList(1);
            this.m_StatMods = new ArrayList(1);
            this.Map = Server.Map.Internal;
            this.m_AutoPageNotify = true;
            this.m_Aggressors = new ArrayList(1);
            this.m_Aggressed = new ArrayList(1);
            this.m_Virtues = new VirtueInfo();
            this.m_Stabled = new ArrayList(1);
            this.m_DamageEntries = new ArrayList(1);
            this.m_ExpireCombatant = new ExpireCombatantTimer(this);
            this.m_ExpireCriminal = new ExpireCriminalTimer(this);
            this.m_CombatTimer = new CombatTimer(this);
            this.m_LogoutTimer = new LogoutTimer(this);
            this.m_NextSkillTime = DateTime.MinValue;
            this.m_CreationTime = DateTime.Now;
        }

        public void DelayChangeWarmode(bool value)
        {
            if (this.m_WarmodeTimer != null)
            {
                this.m_WarmodeTimer.Value = value;
            }
            else if (this.m_Warmode != value)
            {
                DateTime now = DateTime.Now;
                DateTime nextWarmodeChange = this.m_NextWarmodeChange;
                if ((now > nextWarmodeChange) || (this.m_WarmodeChanges == 0))
                {
                    this.m_WarmodeChanges = 1;
                    this.m_NextWarmodeChange = now + WarmodeSpamCatch;
                }
                else
                {
                    if (this.m_WarmodeChanges == 4)
                    {
                        this.m_WarmodeTimer = new WarmodeTimer(this, value);
                        this.m_WarmodeTimer.Start();
                        return;
                    }
                    this.m_WarmodeChanges++;
                }
                this.Warmode = value;
            }
        }

        public virtual void Delete()
        {
            if (!this.m_Deleted && World.OnDelete(this))
            {
                if (this.m_NetState != null)
                {
                    this.m_NetState.CancelAllTrades();
                }
                if (this.m_NetState != null)
                {
                    this.m_NetState.Dispose();
                }
                this.DropHolding();
                this.Region.InternalExit(this);
                this.OnDelete();
                for (int i = this.m_Items.Count - 1; i >= 0; i--)
                {
                    if (i < this.m_Items.Count)
                    {
                        ((Item) this.m_Items[i]).OnParentDeleted(this);
                    }
                }
                this.SendRemovePacket();
                if (this.m_Guild != null)
                {
                    this.m_Guild.OnMobileDelete(this);
                }
                this.m_Deleted = true;
                if (this.m_Map != null)
                {
                    this.m_Map.OnLeave(this);
                    this.m_Map = null;
                }

                m_Hair = null;
                m_FacialHair = null;

                this.m_MountItem = null;
                World.RemoveMobile(this);
                this.OnAfterDelete();
                this.m_RemovePacket = null;
                this.m_OPLPacket = null;
                this.m_PropertyList = null;
            }
        }

        public virtual void Delta(MobileDelta flag)
        {
            if ((this.m_Map != null) && (this.m_Map != Server.Map.Internal))
            {
                this.m_DeltaFlags |= flag;
                if (!this.m_InDeltaQueue)
                {
                    this.m_InDeltaQueue = true;
                    m_DeltaQueue.Enqueue(this);
                }
            }
        }
        public virtual void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();

            switch (version)
            {
                case 28:
                    {
                        m_LastStatGain = reader.ReadDeltaTime();

                        goto case 27;
                    }
                case 27:
                    {
                        m_TithingPoints = reader.ReadInt();

                        goto case 26;
                    }
                case 26:
                case 25:
                case 24:
                    {
                        m_Corpse = reader.ReadItem() as Container;

                        goto case 23;
                    }
                case 23:
                    {
                        m_CreationTime = reader.ReadDateTime();

                        goto case 22;
                    }
                case 22: // Just removed followers
                case 21:
                    {
                        m_Stabled = reader.ReadMobileList();

                        goto case 20;
                    }
                case 20:
                    {
                        m_CantWalk = reader.ReadBool();

                        goto case 19;
                    }
                case 19: // Just removed variables
                case 18:
                    {
                        m_Virtues = new VirtueInfo(reader);

                        goto case 17;
                    }
                case 17:
                    {
                        m_Thirst = reader.ReadInt();
                        m_BAC = reader.ReadInt();

                        goto case 16;
                    }
                case 16:
                    {
                        m_ShortTermMurders = reader.ReadInt();

                        if (version <= 24)
                        {
                            reader.ReadDateTime();
                            reader.ReadDateTime();
                        }

                        goto case 15;
                    }
                case 15:
                    {
                        if (version < 22)
                            reader.ReadInt(); // followers

                        m_FollowersMax = reader.ReadInt();

                        goto case 14;
                    }
                case 14:
                    {
                        m_MagicDamageAbsorb = reader.ReadInt();

                        goto case 13;
                    }
                case 13:
                    {
                        m_GuildFealty = reader.ReadMobile();

                        goto case 12;
                    }
                case 12:
                    {
                        m_Guild = reader.ReadGuild();

                        goto case 11;
                    }
                case 11:
                    {
                        m_DisplayGuildTitle = reader.ReadBool();

                        goto case 10;
                    }
                case 10:
                    {
                        m_CanSwim = reader.ReadBool();

                        goto case 9;
                    }
                case 9:
                    {
                        m_Squelched = reader.ReadBool();

                        goto case 8;
                    }
                case 8:
                    {
                        m_Holding = reader.ReadItem();

                        goto case 7;
                    }
                case 7:
                    {
                        m_VirtualArmor = reader.ReadInt();

                        goto case 6;
                    }
                case 6:
                    {
                        m_BaseSoundID = reader.ReadInt();

                        goto case 5;
                    }
                case 5:
                    {
                        m_DisarmReady = reader.ReadBool();
                        m_StunReady = reader.ReadBool();

                        goto case 4;
                    }
                case 4:
                    {
                        if (version <= 25)
                        {
                            Poison.Deserialize(reader);

                            /*if ( m_Poison != null )
                            {
                                m_PoisonTimer = new PoisonTimer( this );
                                m_PoisonTimer.Start();
                            }*/
                        }

                        goto case 3;
                    }
                case 3:
                    {
                        m_StatCap = reader.ReadInt();

                        goto case 2;
                    }
                case 2:
                    {
                        m_NameHue = reader.ReadInt();

                        goto case 1;
                    }
                case 1:
                    {
                        m_Hunger = reader.ReadInt();

                        goto case 0;
                    }
                case 0:
                    {
                        if (version < 21)
                            m_Stabled = new ArrayList();

                        if (version < 18)
                            m_Virtues = new VirtueInfo();

                        if (version < 11)
                            m_DisplayGuildTitle = true;

                        if (version < 3)
                            m_StatCap = 225;

                        if (version < 15)
                        {
                            m_Followers = 0;
                            m_FollowersMax = 5;
                        }

                        m_Location = reader.ReadPoint3D();
                        m_Body = new Body(reader.ReadInt());
                        m_Name = reader.ReadString();
                        m_GuildTitle = reader.ReadString();
                        m_Criminal = reader.ReadBool();
                        m_Kills = reader.ReadInt();
                        m_SpeechHue = reader.ReadInt();
                        m_EmoteHue = reader.ReadInt();
                        m_WhisperHue = reader.ReadInt();
                        m_YellHue = reader.ReadInt();
                        m_Language = reader.ReadString();
                        m_Female = reader.ReadBool();
                        m_Warmode = reader.ReadBool();
                        m_Hidden = reader.ReadBool();
                        m_Direction = (Direction)reader.ReadByte();
                        m_Hue = reader.ReadInt();
                        m_Str = reader.ReadInt();
                        m_Dex = reader.ReadInt();
                        m_Int = reader.ReadInt();
                        m_Hits = reader.ReadInt();
                        m_Stam = reader.ReadInt();
                        m_Mana = reader.ReadInt();
                        m_Map = reader.ReadMap();
                        m_Blessed = reader.ReadBool();
                        m_Fame = reader.ReadInt();
                        m_Karma = reader.ReadInt();
                        m_AccessLevel = (AccessLevel)reader.ReadByte();

                        m_Skills = new Skills(this, reader);

                        int itemCount = reader.ReadInt();

                        m_Items = new ArrayList(itemCount);

                        for (int i = 0; i < itemCount; ++i)
                        {
                            Item item = reader.ReadItem();

                            if (item != null)
                                m_Items.Add(item);
                        }

                        m_Player = reader.ReadBool();
                        m_Title = reader.ReadString();
                        m_Profile = reader.ReadString();
                        m_ProfileLocked = reader.ReadBool();
                        if (version <= 18)
                        {
                            /*m_LightLevel =*/
                            reader.ReadInt();
                            /*m_TotalGold =*/
                            reader.ReadInt();
                            /*m_TotalWeight =*/
                            reader.ReadInt();
                        }
                        m_AutoPageNotify = reader.ReadBool();

                        m_LogoutLocation = reader.ReadPoint3D();
                        m_LogoutMap = reader.ReadMap();

                        m_StrLock = (StatLockType)reader.ReadByte();
                        m_DexLock = (StatLockType)reader.ReadByte();
                        m_IntLock = (StatLockType)reader.ReadByte();

                        m_StatMods = new ArrayList();

                        if (reader.ReadBool())
                        {
                            int StuckMenuLength = reader.ReadInt();
                            m_StuckMenuUses = new DateTime[StuckMenuLength];

                            for (int i = 0; i < m_StuckMenuUses.Length; ++i)
                            {
                                m_StuckMenuUses[i] = reader.ReadDateTime();
                            }
                        }
                        else
                        {
                            m_StuckMenuUses = null;
                        }

                        if (m_Player && m_Map != Map.Internal)
                        {
                            m_LogoutLocation = m_Location;
                            m_LogoutMap = m_Map;

                            m_Map = Map.Internal;
                        }

                        if (m_Map != null)
                            m_Map.OnEnter(this);

                        if (m_Criminal)
                        {
                            if (m_ExpireCriminal == null)
                                m_ExpireCriminal = new ExpireCriminalTimer(this);

                            m_ExpireCriminal.Start();
                        }

                        if (ShouldCheckStatTimers)
                            CheckStatTimers();

                        if (!m_Player && m_Dex <= 100 && m_CombatTimer != null)
                            m_CombatTimer.Priority = TimerPriority.FiftyMS;
                        else if (m_CombatTimer != null)
                            m_CombatTimer.Priority = TimerPriority.EveryTick;

                        m_Region = Region.Find(m_Location, m_Map);
                        //m_Region.InternalEnter(this);
                        UpdateRegion(); // Roman / Kel : Replced by this new method!
                        UpdateResistances();

                        break;
                    }
            }
        }

        /// <summary>
        /// New method, perf work
        /// </summary>
        internal void UpdateRegion()
        {
            if (m_Player)
            {
                m_Region.Players.Add(this);
            }
            m_Region.Mobiles.Add(this);
        }


        public virtual void DisplayPaperdollTo(Mobile to)
        {
            EventSink.InvokePaperdollRequest(new PaperdollRequestEventArgs(to, this));
        }

        public virtual void DisruptiveAction()
        {
            if (this.Meditating)
            {
                this.Meditating = false;
                this.SendLocalizedMessage(0x7a1a6);
            }
        }

        public virtual void DoBeneficial(Mobile target)
        {
            if (target != null)
            {
                this.OnBeneficialAction(target, this.IsBeneficialCriminal(target));
                this.Region.OnBenificialAction(this, target);
                target.Region.OnGotBenificialAction(this, target);
            }
        }

        public virtual void DoHarmful(Mobile target)
        {
            this.DoHarmful(target, false);
        }

        public virtual void DoHarmful(Mobile target, bool indirect)
        {
            if (target != null)
            {
                bool isCriminal = this.IsHarmfulCriminal(target);
                this.OnHarmfulAction(target, isCriminal);
                target.AggressiveAction(this, isCriminal);
                this.Region.OnDidHarmful(this, target);
                target.Region.OnGotHarmful(this, target);
                if (!indirect)
                {
                    this.Combatant = target;
                }
                this.m_ExpireCombatant.Stop();
                this.m_ExpireCombatant.Start();
            }
        }

        public virtual void DoSpeech(string text, int[] keywords, MessageType type, int hue)
        {
            if (!this.m_Deleted && !Commands.Handle(this, text))
            {
                int range = 15;
                switch (type)
                {
                    case MessageType.Regular:
                        this.m_SpeechHue = hue;
                        break;

                    case MessageType.Emote:
                        this.m_EmoteHue = hue;
                        break;

                    case MessageType.Whisper:
                        this.m_WhisperHue = hue;
                        range = 1;
                        break;

                    case MessageType.Yell:
                        this.m_YellHue = hue;
                        range = 0x12;
                        break;

                    default:
                        type = MessageType.Regular;
                        break;
                }
                SpeechEventArgs e = new SpeechEventArgs(this, text, type, hue, keywords);
                EventSink.InvokeSpeech(e);
                this.m_Region.OnSpeech(e);
                this.OnSaid(e);
                if (!e.Blocked)
                {
                    text = e.Speech;
                    if ((text != null) && (text.Length != 0))
                    {
                        if (m_Hears == null)
                        {
                            m_Hears = new ArrayList();
                        }
                        else if (m_Hears.Count > 0)
                        {
                            m_Hears.Clear();
                        }
                        if (m_OnSpeech == null)
                        {
                            m_OnSpeech = new ArrayList();
                        }
                        else if (m_OnSpeech.Count > 0)
                        {
                            m_OnSpeech.Clear();
                        }
                        ArrayList hears = m_Hears;
                        ArrayList onSpeech = m_OnSpeech;
                        if (this.m_Map != null)
                        {
                        Label_023C:
                            foreach (object obj2 in this.m_Map.GetObjectsInRange(this.m_Location, range))
                            {
                                if (obj2 is Mobile)
                                {
                                    Mobile mobile = (Mobile) obj2;
                                    if (mobile.CanSee(this) && ((m_NoSpeechLOS || !mobile.Player) || mobile.InLOS(this)))
                                    {
                                        if (mobile.m_NetState != null)
                                        {
                                            hears.Add(mobile);
                                        }
                                        if (mobile.HandlesOnSpeech(this))
                                        {
                                            onSpeech.Add(mobile);
                                        }
                                        for (int k = 0; k < mobile.Items.Count; k++)
                                        {
                                            Item item = (Item) mobile.Items[k];
                                            if (item.HandlesOnSpeech)
                                            {
                                                onSpeech.Add(item);
                                            }
                                            if (item is Container)
                                            {
                                                this.AddSpeechItemsFrom(onSpeech, (Container) item);
                                            }
                                        }
                                    }
                                    goto Label_023C;
                                }
                                if (obj2 is Item)
                                {
                                    if (((Item) obj2).HandlesOnSpeech)
                                    {
                                        onSpeech.Add(obj2);
                                    }
                                    if (obj2 is Container)
                                    {
                                        this.AddSpeechItemsFrom(onSpeech, (Container) obj2);
                                    }
                                }
                            }
                            object context = null;
                            string str = text;
                            SpeechEventArgs args2 = null;
                            if (this.MutateSpeech(hears, ref str, ref context))
                            {
                                args2 = new SpeechEventArgs(this, str, type, hue, new int[0]);
                            }
                            this.CheckSpeechManifest();
                            this.ProcessDelta();
                            Packet p = null;
                            Packet packet2 = null;
                            for (int i = 0; i < hears.Count; i++)
                            {
                                Mobile m = (Mobile) hears[i];
                                if ((args2 == null) || !this.CheckHearsMutatedSpeech(m, context))
                                {
                                    m.OnSpeech(e);
                                    Server.Network.NetState netState = m.NetState;
                                    if (netState != null)
                                    {
                                        if (p == null)
                                        {
                                            p = new UnicodeMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.m_Language, this.Name, text);
                                        }
                                        netState.Send(p);
                                    }
                                }
                                else
                                {
                                    m.OnSpeech(args2);
                                    Server.Network.NetState state2 = m.NetState;
                                    if (state2 != null)
                                    {
                                        if (packet2 == null)
                                        {
                                            packet2 = new UnicodeMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.m_Language, this.Name, str);
                                        }
                                        state2.Send(packet2);
                                    }
                                }
                            }
                            if (onSpeech.Count > 1)
                            {
                                onSpeech.Sort(new LocationComparer(this));
                            }
                            for (int j = 0; j < onSpeech.Count; j++)
                            {
                                object obj4 = onSpeech[j];
                                if (obj4 is Mobile)
                                {
                                    Mobile mobile3 = (Mobile) obj4;
                                    if ((args2 == null) || !this.CheckHearsMutatedSpeech(mobile3, context))
                                    {
                                        mobile3.OnSpeech(e);
                                    }
                                    else
                                    {
                                        mobile3.OnSpeech(args2);
                                    }
                                }
                                else
                                {
                                    ((Item) obj4).OnSpeech(e);
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual bool Drop(Point3D loc)
        {
            Mobile from = this;
            Item holding = from.Holding;
            if (holding == null)
            {
                return false;
            }
            from.Holding = null;
            bool flag = true;
            holding.SetLastMoved();
            if (!holding.DropToWorld(from, loc))
            {
                holding.Bounce(from);
            }
            else
            {
                flag = false;
            }
            holding.ClearBounce();
            if (!flag)
            {
                this.SendDropEffect(holding);
            }
            return !flag;
        }

        public virtual bool Drop(Item to, Point3D loc)
        {
            Mobile from = this;
            Item holding = from.Holding;
            if (holding == null)
            {
                return false;
            }
            from.Holding = null;
            bool flag = true;
            holding.SetLastMoved();
            if ((to == null) || !holding.DropToItem(from, to, loc))
            {
                holding.Bounce(from);
            }
            else
            {
                flag = false;
            }
            holding.ClearBounce();
            if (!flag)
            {
                this.SendDropEffect(holding);
            }
            return !flag;
        }

        public virtual bool Drop(Mobile to, Point3D loc)
        {
            Mobile from = this;
            Item holding = from.Holding;
            if (holding == null)
            {
                return false;
            }
            from.Holding = null;
            bool flag = true;
            holding.SetLastMoved();
            if ((to == null) || !holding.DropToMobile(from, to, loc))
            {
                holding.Bounce(from);
            }
            else
            {
                flag = false;
            }
            holding.ClearBounce();
            if (!flag)
            {
                this.SendDropEffect(holding);
            }
            return !flag;
        }

        public void DropHolding()
        {
            Item holding = this.m_Holding;
            if (holding != null)
            {
                if (!holding.Deleted && (holding.Map == Server.Map.Internal))
                {
                    holding.MoveToWorld(this.m_Location, this.m_Map);
                }
                holding.ClearBounce();
                this.Holding = null;
            }
        }

        public void Emote(int number)
        {
            this.Emote(number, "");
        }

        public void Emote(string text)
        {
            this.PublicOverheadMessage(MessageType.Emote, this.m_EmoteHue, false, text);
        }

        public void Emote(int number, string args)
        {
            this.PublicOverheadMessage(MessageType.Emote, this.m_EmoteHue, number, args);
        }

        public void Emote(string format, params object[] args)
        {
            this.Emote(string.Format(format, args));
        }

        public void EndAction(object toLock)
        {
            if (this.m_Actions != null)
            {
                this.m_Actions.Remove(toLock);
                if (this.m_Actions.Count == 0)
                {
                    this.m_Actions = null;
                }
            }
        }

        public virtual bool EquipItem(Item item)
        {
            if (((item == null) || item.Deleted) || !item.CanEquip(this))
            {
                return false;
            }
            if ((!this.CheckEquip(item) || !this.OnEquip(item)) || !item.OnEquip(this))
            {
                return false;
            }
            if ((this.m_Spell != null) && !this.m_Spell.OnCasterEquiping(item))
            {
                return false;
            }
            this.AddItem(item);
            return true;
        }

        public Server.Items.BankBox FindBankNoCreate()
        {
            if (((this.m_BankBox == null) || this.m_BankBox.Deleted) || (this.m_BankBox.Parent != this))
            {
                this.m_BankBox = this.FindItemOnLayer(Layer.Bank) as Server.Items.BankBox;
            }
            return this.m_BankBox;
        }

        public DamageEntry FindDamageEntryFor(Mobile m)
        {
            for (int i = this.m_DamageEntries.Count - 1; i >= 0; i--)
            {
                if (i < this.m_DamageEntries.Count)
                {
                    DamageEntry entry = (DamageEntry) this.m_DamageEntries[i];
                    if (entry.HasExpired)
                    {
                        this.m_DamageEntries.RemoveAt(i);
                    }
                    else if (entry.Damager == m)
                    {
                        return entry;
                    }
                }
            }
            return null;
        }

        public Item FindItemOnLayer(Layer layer)
        {
            ArrayList items = this.m_Items;
            int count = items.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = (Item) items[i];
                if (!item.Deleted && (item.Layer == layer))
                {
                    return item;
                }
            }
            return null;
        }

        public DamageEntry FindLeastRecentDamageEntry(bool allowSelf)
        {
            for (int i = 0; i < this.m_DamageEntries.Count; i++)
            {
                if (i >= 0)
                {
                    DamageEntry entry = (DamageEntry) this.m_DamageEntries[i];
                    if (entry.HasExpired)
                    {
                        this.m_DamageEntries.RemoveAt(i);
                        i--;
                    }
                    else if (allowSelf || (entry.Damager != this))
                    {
                        return entry;
                    }
                }
            }
            return null;
        }

        public Mobile FindLeastRecentDamager(bool allowSelf)
        {
            return GetDamagerFrom(this.FindLeastRecentDamageEntry(allowSelf));
        }

        public DamageEntry FindLeastTotalDamageEntry(bool allowSelf)
        {
            DamageEntry entry = null;
            for (int i = this.m_DamageEntries.Count - 1; i >= 0; i--)
            {
                if (i < this.m_DamageEntries.Count)
                {
                    DamageEntry entry2 = (DamageEntry) this.m_DamageEntries[i];
                    if (entry2.HasExpired)
                    {
                        this.m_DamageEntries.RemoveAt(i);
                    }
                    else if ((allowSelf || (entry2.Damager != this)) && ((entry == null) || (entry2.DamageGiven < entry.DamageGiven)))
                    {
                        entry = entry2;
                    }
                }
            }
            return entry;
        }

        public Mobile FindLeastTotalDamger(bool allowSelf)
        {
            return GetDamagerFrom(this.FindLeastTotalDamageEntry(allowSelf));
        }

        public DamageEntry FindMostRecentDamageEntry(bool allowSelf)
        {
            for (int i = this.m_DamageEntries.Count - 1; i >= 0; i--)
            {
                if (i < this.m_DamageEntries.Count)
                {
                    DamageEntry entry = (DamageEntry) this.m_DamageEntries[i];
                    if (entry.HasExpired)
                    {
                        this.m_DamageEntries.RemoveAt(i);
                    }
                    else if (allowSelf || (entry.Damager != this))
                    {
                        return entry;
                    }
                }
            }
            return null;
        }

        public Mobile FindMostRecentDamager(bool allowSelf)
        {
            return GetDamagerFrom(this.FindMostRecentDamageEntry(allowSelf));
        }

        public DamageEntry FindMostTotalDamageEntry(bool allowSelf)
        {
            DamageEntry entry = null;
            for (int i = this.m_DamageEntries.Count - 1; i >= 0; i--)
            {
                if (i < this.m_DamageEntries.Count)
                {
                    DamageEntry entry2 = (DamageEntry) this.m_DamageEntries[i];
                    if (entry2.HasExpired)
                    {
                        this.m_DamageEntries.RemoveAt(i);
                    }
                    else if ((allowSelf || (entry2.Damager != this)) && ((entry == null) || (entry2.DamageGiven > entry.DamageGiven)))
                    {
                        entry = entry2;
                    }
                }
            }
            return entry;
        }

        public Mobile FindMostTotalDamger(bool allowSelf)
        {
            return GetDamagerFrom(this.FindMostTotalDamageEntry(allowSelf));
        }

        public void FixedEffect(int itemID, int speed, int duration)
        {
            Effects.SendTargetEffect(this, itemID, speed, duration, 0, 0);
        }

        public void FixedEffect(int itemID, int speed, int duration, int hue, int renderMode)
        {
            Effects.SendTargetEffect(this, itemID, speed, duration, hue, renderMode);
        }

        public void FixedParticles(int itemID, int speed, int duration, int effect, EffectLayer layer)
        {
            Effects.SendTargetParticles(this, itemID, speed, duration, 0, 0, effect, layer, 0);
        }

        public void FixedParticles(int itemID, int speed, int duration, int effect, EffectLayer layer, int unknown)
        {
            Effects.SendTargetParticles(this, itemID, speed, duration, 0, 0, effect, layer, unknown);
        }

        public void FixedParticles(int itemID, int speed, int duration, int effect, int hue, int renderMode, EffectLayer layer)
        {
            Effects.SendTargetParticles(this, itemID, speed, duration, hue, renderMode, effect, layer, 0);
        }

        public void FixedParticles(int itemID, int speed, int duration, int effect, int hue, int renderMode, EffectLayer layer, int unknown)
        {
            Effects.SendTargetParticles(this, itemID, speed, duration, hue, renderMode, effect, layer, unknown);
        }

        public void ForceRegionReEnter(bool Exit)
        {
            if (!this.m_Deleted)
            {
                Server.Region old = Server.Region.Find(this.m_Location, this.m_Map);
                if (old != this.m_Region)
                {
                    if (Exit)
                    {
                        this.m_Region.InternalExit(this);
                    }
                    this.m_Region = old;
                    this.OnRegionChange(old, this.m_Region);
                    this.m_Region.InternalEnter(this);
                    this.CheckLightLevels(false);
                }
            }
        }

        public void FreeCache()
        {
            this.m_RemovePacket = null;
            this.m_OPLPacket = null;
            this.m_PropertyList = null;
        }

        public void Freeze(TimeSpan duration)
        {
            if (!this.m_Frozen)
            {
                this.m_Frozen = true;
                this.m_FrozenTimer = new FrozenTimer(this, duration);
                this.m_FrozenTimer.Start();
            }
        }

        public static string GetAccessLevelName(Server.AccessLevel level)
        {
            return m_AccessLevelNames[(int) level];
        }

        public virtual int GetAngerSound()
        {
            if (this.m_BaseSoundID != 0)
            {
                return this.m_BaseSoundID;
            }
            return -1;
        }

        public virtual int GetAttackSound()
        {
            if (this.m_BaseSoundID != 0)
            {
                return (this.m_BaseSoundID + 2);
            }
            return -1;
        }

        public virtual void GetChildProperties(ObjectPropertyList list, Item item)
        {
        }

        public IPooledEnumerable GetClientsInRange(int range)
        {
            Server.Map map = this.m_Map;
            if (map == null)
            {
                return Server.Map.NullEnumerable.Instance;
            }
            return map.GetClientsInRange(this.m_Location, range);
        }

        public virtual void GetChildContextMenuEntries(Mobile from, List<ContextMenuEntry> list, Item item)
        {
        }

        public virtual void GetContextMenuEntries(Mobile from, List<ContextMenuEntry> list)
        {
            if (!this.m_Deleted)
            {
                if (this.CanPaperdollBeOpenedBy(from))
                {
                    list.Add(new PaperdollEntry(this));
                }
                if (((from == this) && (this.Backpack != null)) && (this.CanSee((Item) this.Backpack) && this.CheckAlive(false)))
                {
                    list.Add(new OpenBackpackEntry(this));
                }
            }
        }

        public virtual Mobile GetDamageMaster(Mobile damagee)
        {
            return null;
        }

        public static Mobile GetDamagerFrom(DamageEntry de)
        {
            if (de != null)
            {
                return de.Damager;
            }
            return null;
        }

        public virtual int GetDeathSound()
        {
            if (this.m_BaseSoundID != 0)
            {
                return (this.m_BaseSoundID + 4);
            }
            if (this.m_Body.IsHuman)
            {
                return Utility.Random(this.m_Female ? 0x314 : 0x423, this.m_Female ? 4 : 5);
            }
            return -1;
        }

        public virtual IWeapon GetDefaultWeapon()
        {
            return m_DefaultWeapon;
        }

        public Server.Direction GetDirectionTo(IPoint2D p)
        {
            if (p == null)
            {
                return Server.Direction.North;
            }
            return this.GetDirectionTo(p.X, p.Y);
        }

        public Server.Direction GetDirectionTo(Point2D p)
        {
            return this.GetDirectionTo(p.m_X, p.m_Y);
        }

        public Server.Direction GetDirectionTo(Point3D p)
        {
            return this.GetDirectionTo(p.m_X, p.m_Y);
        }

        public Server.Direction GetDirectionTo(int x, int y)
        {
            int num = this.m_Location.m_X - x;
            int num2 = this.m_Location.m_Y - y;
            int num3 = (num - num2) * 0x2c;
            int num4 = (num + num2) * 0x2c;
            int num5 = Math.Abs(num3);
            int num6 = Math.Abs(num4);
            if (((num6 >> 1) - num5) >= 0)
            {
                return ((num4 > 0) ? Server.Direction.Up : Server.Direction.Down);
            }
            if (((num5 >> 1) - num6) >= 0)
            {
                return ((num3 > 0) ? Server.Direction.Left : Server.Direction.Right);
            }
            if ((num3 >= 0) && (num4 >= 0))
            {
                return Server.Direction.West;
            }
            if ((num3 >= 0) && (num4 < 0))
            {
                return Server.Direction.South;
            }
            if ((num3 < 0) && (num4 < 0))
            {
                return Server.Direction.East;
            }
            return Server.Direction.North;
        }

        public double GetDistanceToSqrt(IPoint2D p)
        {
            int num = this.m_Location.m_X - p.X;
            int num2 = this.m_Location.m_Y - p.Y;
            return Math.Sqrt((double) ((num * num) + (num2 * num2)));
        }

        public double GetDistanceToSqrt(Mobile m)
        {
            int num = this.m_Location.m_X - m.m_Location.m_X;
            int num2 = this.m_Location.m_Y - m.m_Location.m_Y;
            return Math.Sqrt((double) ((num * num) + (num2 * num2)));
        }

        public double GetDistanceToSqrt(Point3D p)
        {
            int num = this.m_Location.m_X - p.m_X;
            int num2 = this.m_Location.m_Y - p.m_Y;
            return Math.Sqrt((double) ((num * num) + (num2 * num2)));
        }

        public static TimeSpan GetHitsRegenRate(Mobile m)
        {
            if (m_HitsRegenRate == null)
            {
                return m_DefaultHitsRate;
            }
            return m_HitsRegenRate(m);
        }

        public virtual int GetHurtSound()
        {
            if (this.m_BaseSoundID != 0)
            {
                return (this.m_BaseSoundID + 3);
            }
            return -1;
        }

        public virtual int GetIdleSound()
        {
            if (this.m_BaseSoundID != 0)
            {
                return (this.m_BaseSoundID + 1);
            }
            return -1;
        }

        public virtual DeathMoveResult GetInventoryMoveResultFor(Item item)
        {
            return item.OnInventoryDeath(this);
        }

        public IPooledEnumerable GetItemsInRange(int range)
        {
            Server.Map map = this.m_Map;
            if (map == null)
            {
                return Server.Map.NullEnumerable.Instance;
            }
            return map.GetItemsInRange(this.m_Location, range);
        }

        public virtual TimeSpan GetLogoutDelay()
        {
            return this.Region.GetLogoutDelay(this);
        }

        public static TimeSpan GetManaRegenRate(Mobile m)
        {
            if (m_ManaRegenRate == null)
            {
                return m_DefaultManaRate;
            }
            return m_ManaRegenRate(m);
        }

        public virtual int GetMaxResistance(ResistanceType type)
        {
            if (this.m_Player)
            {
                return m_MaxPlayerResistance;
            }
            return 0x7fffffff;
        }

        public virtual int GetMinResistance(ResistanceType type)
        {
            return -2147483648;
        }

        public IPooledEnumerable GetMobilesInRange(int range)
        {
            Server.Map map = this.m_Map;
            if (map == null)
            {
                return Server.Map.NullEnumerable.Instance;
            }
            return map.GetMobilesInRange(this.m_Location, range);
        }

        public IPooledEnumerable GetObjectsInRange(int range)
        {
            Server.Map map = this.m_Map;
            if (map == null)
            {
                return Server.Map.NullEnumerable.Instance;
            }
            return map.GetObjectsInRange(this.m_Location, range);
        }

        public virtual int GetPacketFlags()
        {
            int num = 0;
            if (this.m_Female)
            {
                num |= 2;
            }
            if (this.m_Poison != null)
            {
                num |= 4;
            }
            if (this.m_Blessed || this.m_YellowHealthbar)
            {
                num |= 8;
            }
            if (this.m_Warmode)
            {
                num |= 0x40;
            }
            if (this.m_Hidden)
            {
                num |= 0x80;
            }
            return num;
        }

        public virtual DeathMoveResult GetParentMoveResultFor(Item item)
        {
            return item.OnParentDeath(this);
        }

        public virtual void GetProperties(ObjectPropertyList list)
        {
            this.AddNameProperties(list);
        }

        public virtual int GetResistance(ResistanceType type)
        {
            if (this.m_Resistances == null)
            {
                this.m_Resistances = new int[] { -2147483648, -2147483648, -2147483648, -2147483648, -2147483648 };
            }
            int index = (int) type;
            if ((index < 0) || (index >= this.m_Resistances.Length))
            {
                return 0;
            }
            int num2 = this.m_Resistances[index];
            if (num2 == -2147483648)
            {
                this.ComputeResistances();
                num2 = this.m_Resistances[index];
            }
            return num2;
        }

        public virtual int GetSeason()
        {
            if (this.m_Map != null)
            {
                return this.m_Map.Season;
            }
            return 1;
        }

        public static TimeSpan GetStamRegenRate(Mobile m)
        {
            if (m_StamRegenRate == null)
            {
                return m_DefaultStamRate;
            }
            return m_StamRegenRate(m);
        }

        public StatMod GetStatMod(string name)
        {
            for (int i = 0; i < this.m_StatMods.Count; i++)
            {
                StatMod mod = (StatMod) this.m_StatMods[i];
                if (mod.Name == name)
                {
                    return mod;
                }
            }
            return null;
        }

        public int GetStatOffset(StatType type)
        {
            int num = 0;
            for (int i = 0; i < this.m_StatMods.Count; i++)
            {
                StatMod mod = (StatMod) this.m_StatMods[i];
                if (mod.HasElapsed())
                {
                    this.m_StatMods.RemoveAt(i);
                    this.Delta(MobileDelta.Stat);
                    this.CheckStatTimers();
                    i--;
                }
                else if ((mod.Type & type) != 0)
                {
                    num += mod.Offset;
                }
            }
            return num;
        }

        public virtual bool HandlesOnSpeech(Mobile from)
        {
            return false;
        }

        public virtual bool HarmfulCheck(Mobile target)
        {
            if (this.CanBeHarmful(target))
            {
                this.DoHarmful(target);
                return true;
            }
            return false;
        }

        public bool HasFreeHand()
        {
            return (this.FindItemOnLayer(Layer.TwoHanded) == null);
        }

        public bool HasGump(Type type)
        {
            return (FindGump(type) != null);

        }

        public Gump FindGump(Type type)
        {
            NetState ns = m_NetState;

            if (ns != null)
            {
                foreach (Gump gump in ns.Gumps)
                {
                    if (type.IsAssignableFrom(gump.GetType()))
                    {
                        return gump;
                    }
                }
            }

            return null;
        }

        public void Heal(int amount)
        {
            if ((this.Alive && !this.IsDeadBondedPet) && this.Region.OnHeal(this, ref amount))
            {
                if ((this.Hits + amount) > this.HitsMax)
                {
                    amount = this.HitsMax - this.Hits;
                }
                this.Hits += amount;
                if ((amount > 0) && (this.m_NetState != null))
                {
                    this.m_NetState.Send(new MessageLocalizedAffix(Server.Serial.MinusOne, -1, MessageType.Label, 0x3b2, 3, 0xf621e, "", AffixType.System, amount.ToString(), ""));
                }
            }
        }

        public void InitStats(int str, int dex, int intel)
        {
            this.m_Str = str;
            this.m_Dex = dex;
            this.m_Int = intel;
            this.Hits = this.HitsMax;
            this.Stam = this.StamMax;
            this.Mana = this.ManaMax;
            this.Delta(MobileDelta.Stat | MobileDelta.Attributes);
        }

        public bool InLOS(Mobile target)
        {
            if (this.m_Deleted || (this.m_Map == null))
            {
                return false;
            }
            if ((target != this) && (this.m_AccessLevel <= Server.AccessLevel.Player))
            {
                return this.m_Map.LineOfSight(this, target);
            }
            return true;
        }

        public bool InLOS(Point3D target)
        {
            if (this.m_Deleted || (this.m_Map == null))
            {
                return false;
            }
            return ((this.m_AccessLevel > Server.AccessLevel.Player) || this.m_Map.LineOfSight(this, target));
        }

        public bool InLOS(object target)
        {
            if (this.m_Deleted || (this.m_Map == null))
            {
                return false;
            }
            return (((target == this) || (this.m_AccessLevel > Server.AccessLevel.Player)) || (((target is Item) && (((Item) target).RootParent == this)) || this.m_Map.LineOfSight(this, target)));
        }

        public bool InRange(IPoint2D p, int range)
        {
            return ((((p.X >= (this.m_Location.m_X - range)) && (p.X <= (this.m_Location.m_X + range))) && (p.Y >= (this.m_Location.m_Y - range))) && (p.Y <= (this.m_Location.m_Y + range)));
        }

        public bool InRange(Point2D p, int range)
        {
            return ((((p.m_X >= (this.m_Location.m_X - range)) && (p.m_X <= (this.m_Location.m_X + range))) && (p.m_Y >= (this.m_Location.m_Y - range))) && (p.m_Y <= (this.m_Location.m_Y + range)));
        }

        public bool InRange(Point3D p, int range)
        {
            return ((((p.m_X >= (this.m_Location.m_X - range)) && (p.m_X <= (this.m_Location.m_X + range))) && (p.m_Y >= (this.m_Location.m_Y - range))) && (p.m_Y <= (this.m_Location.m_Y + range)));
        }

        public void Internalize()
        {
            this.Map = Server.Map.Internal;
        }

        private bool InternalOnMove(Server.Direction d)
        {
            if (!this.OnMove(d))
            {
                return false;
            }
            MovementEventArgs e = MovementEventArgs.Create(this, d);
            EventSink.InvokeMovement(e);
            bool flag = !e.Blocked;
            e.Free();
            return flag;
        }

        private void InternalRemoveSkillMod(SkillMod mod)
        {
            if (this.m_SkillMods.Contains(mod))
            {
                this.m_SkillMods.Remove(mod);
                mod.Owner = null;
                Skill skill = this.m_Skills[mod.Skill];
                if (skill != null)
                {
                    skill.Update();
                }
            }
        }

        public void InvalidateProperties()
        {
            if (Core.AOS)
            {
                if ((this.m_Map != null) && (this.m_Map != Server.Map.Internal))
                {
                    ObjectPropertyList propertyList = this.m_PropertyList;
                    this.m_PropertyList = null;
                    ObjectPropertyList list2 = this.PropertyList;
                    if ((propertyList == null) || (propertyList.Hash != list2.Hash))
                    {
                        this.m_OPLPacket = null;
                        this.Delta(MobileDelta.Properties);
                    }
                }
                else
                {
                    this.m_PropertyList = null;
                    this.m_OPLPacket = null;
                }
            }
        }

        public virtual bool IsBeneficialCriminal(Mobile target)
        {
            if (this == target)
            {
                return false;
            }
            int num = Notoriety.Compute(this, target);
            if (num != 4)
            {
                return (num == 6);
            }
            return true;
        }

        public virtual bool IsHarmfulCriminal(Mobile target)
        {
            if (this == target)
            {
                return false;
            }
            return (Notoriety.Compute(this, target) == 1);
        }

        public virtual bool IsSnoop(Mobile from)
        {
            return (from != this);
        }

        public virtual void Kill()
        {
            if ((((this.CanBeDamaged() && (this.Alive && !this.IsDeadBondedPet)) && !this.m_Deleted) && this.Region.OnDeath(this)) && this.OnBeforeDeath())
            {
                Server.Items.BankBox box = this.FindBankNoCreate();
                if ((box != null) && box.Opened)
                {
                    box.Close();
                }
                if (this.m_NetState != null)
                {
                    this.m_NetState.CancelAllTrades();
                }
                if (this.m_Spell != null)
                {
                    this.m_Spell.OnCasterKilled();
                }
                if (this.m_Target != null)
                {
                    this.m_Target.Cancel(this, TargetCancelType.Canceled);
                }
                this.DisruptiveAction();
                this.Warmode = false;
                this.DropHolding();
                this.Hits = 0;
                this.Stam = 0;
                this.Mana = 0;
                this.Poison = null;
                this.Combatant = null;
                if (this.Paralyzed)
                {
                    this.Paralyzed = false;
                    if (this.m_ParaTimer != null)
                    {
                        this.m_ParaTimer.Stop();
                    }
                }
                if (this.Frozen)
                {
                    this.Frozen = false;
                    if (this.m_FrozenTimer != null)
                    {
                        this.m_FrozenTimer.Stop();
                    }
                }
                ArrayList initialContent = new ArrayList();
                ArrayList equipedItems = new ArrayList();
                ArrayList list3 = new ArrayList();
                ArrayList list4 = new ArrayList(this.m_Items);
                Container backpack = this.Backpack;
                for (int i = 0; i < list4.Count; i++)
                {
                    Item item = (Item) list4[i];
                    if (item != backpack)
                    {
                        switch (this.GetParentMoveResultFor(item))
                        {
                            case DeathMoveResult.MoveToCorpse:
                                initialContent.Add(item);
                                equipedItems.Add(item);
                                break;

                            case DeathMoveResult.MoveToBackpack:
                                list3.Add(item);
                                break;
                        }
                    }
                }
                if (backpack != null)
                {
                    ArrayList list5 = new ArrayList(backpack.Items);
                    for (int j = 0; j < list5.Count; j++)
                    {
                        Item item2 = (Item) list5[j];
                        if (this.GetInventoryMoveResultFor(item2) == DeathMoveResult.MoveToCorpse)
                        {
                            initialContent.Add(item2);
                        }
                        else
                        {
                            list3.Add(item2);
                        }
                    }
                    for (int k = 0; k < list3.Count; k++)
                    {
                        Item dropped = (Item) list3[k];
                        if (!this.RetainPackLocsOnDeath || (dropped.Parent != backpack))
                        {
                            backpack.DropItem(dropped);
                        }
                    }
                }

                HairInfo hair = null;
                if (m_Hair != null)
                    hair = new HairInfo(m_Hair.ItemID, m_Hair.Hue);

                FacialHairInfo facialhair = null;
                if (m_FacialHair != null)
                    facialhair = new FacialHairInfo(m_FacialHair.ItemID, m_FacialHair.Hue);



                Container corpse = (m_CreateCorpse == null) ? null : m_CreateCorpse(this, initialContent, equipedItems);
                if (this.m_Map != null)
                {
                    Packet p = null;
                    Packet removePacket = null;
                    IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                    foreach (Server.Network.NetState state in clientsInRange)
                    {
                        if (state != this.m_NetState)
                        {
                            if (p == null)
                            {
                                p = new DeathAnimation(this, corpse);
                            }
                            state.Send(p);
                            if (!state.Mobile.CanSee(this))
                            {
                                if (removePacket == null)
                                {
                                    removePacket = this.RemovePacket;
                                }
                                state.Send(removePacket);
                            }
                        }
                    }
                    clientsInRange.Free();
                }
                this.OnDeath(corpse);
            }
        }

        public void LaunchBrowser(string url)
        {
            if (this.m_NetState != null)
            {
                this.m_NetState.LaunchBrowser(url);
            }
        }

        public virtual void Lift(Item item, int amount, out bool rejected, out LRReason reject)
        {
            rejected = true;
            reject = LRReason.Inspecific;

            if (item == null)
                return;

            Mobile from = this;
            NetState state = m_NetState;

            if (from.AccessLevel >= AccessLevel.GameMaster || DateTime.Now >= from.NextActionTime)
            {
                if (from.CheckAlive())
                {
                    from.DisruptiveAction();

                    if (from.Holding != null)
                    {
                        reject = LRReason.AreHolding;
                    }
                    else if (from.AccessLevel < AccessLevel.GameMaster && !from.InRange(item.GetWorldLocation(), 2))
                    {
                        reject = LRReason.OutOfRange;
                    }
                    else if (!from.CanSee(item) || !from.InLOS(item))
                    {
                        reject = LRReason.OutOfSight;
                    }
                    else if (!item.VerifyMove(from))
                    {
                        reject = LRReason.CannotLift;
                    }
                    else if (!item.IsAccessibleTo(from))
                    {
                        reject = LRReason.CannotLift;
                    }
                    else if (!item.CheckLift(from, item, ref reject))
                    {
                    }
                    else
                    {
                        object root = item.RootParent;

                        if (root != null && root is Mobile && !((Mobile)root).CheckNonlocalLift(from, item))
                        {
                            reject = LRReason.TryToSteal;
                        }
                        else if (!from.OnDragLift(item) || !item.OnDragLift(from))
                        {
                            reject = LRReason.Inspecific;
                        }
                        else if (!from.CheckAlive())
                        {
                            reject = LRReason.Inspecific;
                        }
                        else
                        {
                            item.SetLastMoved();

                            if (item.Spawner != null)
                            {
                                item.Spawner.Remove(item);
                                item.Spawner = null;
                            }

                            if (amount == 0)
                                amount = 1;

                            if (amount > item.Amount)
                                amount = item.Amount;

                            int oldAmount = item.Amount;
                            //item.Amount = amount; //Set in LiftItemDupe

                            if (amount < oldAmount)
                                LiftItemDupe(item, amount);
                            //item.Dupe( oldAmount - amount );

                            Map map = from.Map;

                            if (Mobile.DragEffects && map != null && (root == null || root is Item))
                            {
                                IPooledEnumerable eable = map.GetClientsInRange(from.Location);
                                Packet p = null;

                                foreach (NetState ns in eable)
                                {
                                    if (!ns.StygianAbyss && ns.Mobile != from && ns.Mobile.CanSee(from))
                                    {
                                        if (p == null)
                                        {
                                            IEntity src;

                                            if (root == null)
                                                src = new Entity(Serial.Zero, item.Location, map);
                                            else
                                                src = new Entity(((Item)root).Serial, ((Item)root).Location, map);

                                            p = Packet.Acquire(new DragEffect(src, from, item.ItemID, item.Hue, amount));
                                        }

                                        ns.Send(p);
                                    }
                                }

                                Packet.Release(p);

                                eable.Free();
                            }

                            Point3D fixLoc = item.Location;
                            Map fixMap = item.Map;
                            bool shouldFix = (item.Parent == null);

                            item.RecordBounce();
                            item.OnItemLifted(from, item);
                            item.Internalize();

                            from.Holding = item;

                            int liftSound = item.GetLiftSound(from);

                            if (liftSound != -1)
                                from.Send(new PlaySound(liftSound, from));

                            from.NextActionTime = DateTime.Now + TimeSpan.FromSeconds(0.5);

                            if (fixMap != null && shouldFix)
                                fixMap.FixColumn(fixLoc.m_X, fixLoc.m_Y);

                            reject = LRReason.Inspecific;
                            rejected = false;
                        }
                    }
                }
                else
                {
                    reject = LRReason.Inspecific;
                }
            }
            else
            {
                SendActionMessage();
                reject = LRReason.Inspecific;
            }

            if (rejected && state != null)
            {
                state.Send(new LiftRej(reject));

                if (item.Parent is Item)
                {
                    if (state.ContainerGridLines)
                        state.Send(new ContainerContentUpdate6017(item));
                    else
                        state.Send(new ContainerContentUpdate(item));
                }
                else if (item.Parent is Mobile)
                    state.Send(new EquipUpdate(item));
                else
                    item.SendInfoTo(state);

                if (ObjectPropertyList.Enabled && item.Parent != null)
                    state.Send(item.OPLPacket);
            }

        }

        public void LocalOverheadMessage(MessageType type, int hue, int number)
        {
            this.LocalOverheadMessage(type, hue, number, "");
        }

        public void LocalOverheadMessage(MessageType type, int hue, bool ascii, string text)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                if (ascii)
                {
                    netState.Send(new AsciiMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.Name, text));
                }
                else
                {
                    netState.Send(new UnicodeMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.m_Language, this.Name, text));
                }
            }
        }

        public void LocalOverheadMessage(MessageType type, int hue, int number, string args)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                netState.Send(new MessageLocalized(this.m_Serial, (int) this.Body, type, hue, 3, number, this.Name, args));
            }
        }

        public virtual void Manifest(TimeSpan delay)
        {
            this.Warmode = true;
            if (this.m_AutoManifestTimer == null)
            {
                this.m_AutoManifestTimer = new AutoManifestTimer(this, delay);
            }
            else
            {
                this.m_AutoManifestTimer.Stop();
            }
            this.m_AutoManifestTimer.Start();
        }

        public virtual bool Move(Server.Direction d)
        {
            if (this.m_Deleted)
            {
                return false;
            }
            Server.Items.BankBox box = this.FindBankNoCreate();
            if ((box != null) && box.Opened)
            {
                box.Close();
            }
            Point3D location = this.Location;
            Point3D oldLocation = location;
            if ((this.m_Direction & Server.Direction.Up) == (d & Server.Direction.Up))
            {
                int num;
                ArrayList mobiles;
                if ((this.m_Spell != null) && !this.m_Spell.OnCasterMoving(d))
                {
                    return false;
                }
                if (this.m_Paralyzed || this.m_Frozen)
                {
                    this.SendLocalizedMessage(0x7a18f);
                    return false;
                }
                if (!this.CheckMovement(d, out num))
                {
                    return false;
                }
                int x = this.m_Location.m_X;
                int y = this.m_Location.m_Y;
                int num4 = x;
                int num5 = y;
                int z = this.m_Location.m_Z;
                switch ((d & Server.Direction.Up))
                {
                    case Server.Direction.North:
                        y--;
                        break;

                    case Server.Direction.Right:
                        x++;
                        y--;
                        break;

                    case Server.Direction.East:
                        x++;
                        break;

                    case Server.Direction.Down:
                        x++;
                        y++;
                        break;

                    case Server.Direction.South:
                        y++;
                        break;

                    case Server.Direction.Left:
                        x--;
                        y++;
                        break;

                    case Server.Direction.West:
                        x--;
                        break;

                    case Server.Direction.Up:
                        x--;
                        y--;
                        break;
                }
                this.m_Pushing = false;
                Server.Map map = this.m_Map;
                if (map == null)
                {
                    return false;
                }
                Sector sector = map.GetSector(num4, num5);
                Sector sector2 = map.GetSector(x, y);
                if (sector != sector2)
                {
                    mobiles = sector.Mobiles;
                    for (int i = 0; i < mobiles.Count; i++)
                    {
                        Mobile mobile = (Mobile) mobiles[i];
                        if ((((mobile != this) && (mobile.X == num4)) && ((mobile.Y == num5) && ((mobile.Z + 15) > z))) && (((z + 15) > mobile.Z) && !mobile.OnMoveOff(this)))
                        {
                            return false;
                        }
                    }
                    mobiles = sector.Items;
                    for (int j = 0; j < mobiles.Count; j++)
                    {
                        Item item = (Item) mobiles[j];
                        if ((item.AtWorldPoint(num4, num5) && ((item.Z == z) || (((item.Z + item.ItemData.Height) > z) && ((z + 15) > item.Z)))) && !item.OnMoveOff(this))
                        {
                            return false;
                        }
                    }
                    mobiles = sector2.Mobiles;
                    for (int k = 0; k < mobiles.Count; k++)
                    {
                        Mobile mobile2 = (Mobile) mobiles[k];
                        if ((((mobile2.X == x) && (mobile2.Y == y)) && (((mobile2.Z + 15) > num) && ((num + 15) > mobile2.Z))) && !mobile2.OnMoveOver(this))
                        {
                            return false;
                        }
                    }
                    mobiles = sector2.Items;
                    for (int m = 0; m < mobiles.Count; m++)
                    {
                        Item item2 = (Item) mobiles[m];
                        if ((item2.AtWorldPoint(x, y) && ((item2.Z == num) || (((item2.Z + item2.ItemData.Height) > num) && ((num + 15) > item2.Z)))) && !item2.OnMoveOver(this))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    mobiles = sector.Mobiles;
                    for (int n = 0; n < mobiles.Count; n++)
                    {
                        Mobile mobile3 = (Mobile) mobiles[n];
                        if ((((mobile3 != this) && (mobile3.X == num4)) && ((mobile3.Y == num5) && ((mobile3.Z + 15) > z))) && (((z + 15) > mobile3.Z) && !mobile3.OnMoveOff(this)))
                        {
                            return false;
                        }
                        if ((((mobile3.X == x) && (mobile3.Y == y)) && (((mobile3.Z + 15) > num) && ((num + 15) > mobile3.Z))) && !mobile3.OnMoveOver(this))
                        {
                            return false;
                        }
                    }
                    mobiles = sector.Items;
                    for (int num12 = 0; num12 < mobiles.Count; num12++)
                    {
                        Item item3 = (Item) mobiles[num12];
                        if ((item3.AtWorldPoint(num4, num5) && ((item3.Z == z) || (((item3.Z + item3.ItemData.Height) > z) && ((z + 15) > item3.Z)))) && !item3.OnMoveOff(this))
                        {
                            return false;
                        }
                        if ((item3.AtWorldPoint(x, y) && ((item3.Z == num) || (((item3.Z + item3.ItemData.Height) > num) && ((num + 15) > item3.Z)))) && !item3.OnMoveOver(this))
                        {
                            return false;
                        }
                    }
                }
                Server.Region region = Server.Region.Find(new Point3D(x, y, num), this.m_Map);
                if ((region != null) && !region.OnMoveInto(this, d, new Point3D(x, y, num), oldLocation))
                {
                    return false;
                }
                if (!this.InternalOnMove(d))
                {
                    return false;
                }
                if (((m_FwdEnabled && (this.m_NetState != null)) && (this.m_AccessLevel < m_FwdAccessOverride)) && (!m_FwdUOTDOverride || ((this.m_NetState.Version != null) && (this.m_NetState.Version.Type != ClientType.UOTD))))
                {
                    TimeSpan span;
                    DateTime time;
                    if (this.m_MoveRecords == null)
                    {
                        this.m_MoveRecords = new Queue(6);
                    }
                    while (this.m_MoveRecords.Count > 0)
                    {
                        MovementRecord record = (MovementRecord) this.m_MoveRecords.Peek();
                        if (!record.Expired())
                        {
                            break;
                        }
                        this.m_MoveRecords.Dequeue();
                    }
                    if (this.m_MoveRecords.Count >= m_FwdMaxSteps)
                    {
                        FastWalkEventArgs e = new FastWalkEventArgs(this.m_NetState);
                        EventSink.InvokeFastWalk(e);
                        if (e.Blocked)
                        {
                            return false;
                        }
                    }
                    if (this.Mounted)
                    {
                        span = ((d & Server.Direction.Running) != Server.Direction.North) ? m_RunMount : m_WalkMount;
                    }
                    else
                    {
                        span = ((d & Server.Direction.Running) != Server.Direction.North) ? m_RunFoot : m_WalkFoot;
                    }
                    if (this.m_MoveRecords.Count > 0)
                    {
                        time = this.m_EndQueue + span;
                    }
                    else
                    {
                        time = DateTime.Now + span;
                    }
                    this.m_MoveRecords.Enqueue(MovementRecord.NewInstance(time));
                    this.m_EndQueue = time;
                }
                this.m_LastMoveTime = DateTime.Now;
                location = new Point3D(x, y, num);
                this.DisruptiveAction();
            }
            if (this.m_NetState != null)
            {
                this.m_NetState.Send(MovementAck.Instantiate(this.m_NetState.Sequence, this));
            }
            this.SetLocation(location, false);
            this.SetDirection(d);
            if (this.m_Map != null)
            {
                MobileMoving[] movingPacketCache = m_MovingPacketCache;
                for (int num13 = 0; num13 < movingPacketCache.Length; num13++)
                {
                    movingPacketCache[num13] = null;
                }
                IPooledEnumerable objectsInRange = this.m_Map.GetObjectsInRange(this.m_Location, Core.GlobalMaxUpdateRange);
                foreach (object obj2 in objectsInRange)
                {
                    if (obj2 != this)
                    {
                        if (obj2 is Mobile)
                        {
                            m_MoveList.Add(obj2);
                        }
                        else if (obj2 is Item)
                        {
                            Item item4 = (Item) obj2;
                            if (item4.HandlesOnMovement)
                            {
                                m_MoveList.Add(item4);
                            }
                        }
                    }
                }
                objectsInRange.Free();
                for (int num14 = 0; num14 < m_MoveList.Count; num14++)
                {
                    object obj3 = m_MoveList[num14];
                    if (obj3 is Mobile)
                    {
                        Mobile source = (Mobile) m_MoveList[num14];
                        Server.Network.NetState netState = source.NetState;
                        if (((netState != null) && Utility.InUpdateRange(this.m_Location, source.m_Location)) && source.CanSee(this))
                        {
                            int index = Notoriety.Compute(source, this);
                            MobileMoving p = movingPacketCache[index];
                            if (p == null)
                            {
                                p = movingPacketCache[index] = new MobileMoving(this, index);
                            }
                            netState.Send(p);
                        }
                        source.OnMovement(this, oldLocation);
                    }
                    else if (obj3 is Item)
                    {
                        ((Item) obj3).OnMovement(this, oldLocation);
                    }
                }
                if (m_MoveList.Count > 0)
                {
                    m_MoveList.Clear();
                }
            }
            return true;
        }

        public virtual void MoveToWorld(Point3D newLocation, Server.Map map)
        {
            if (m_Deleted)
                return;

            if (m_Map == map)
            {
                SetLocation(newLocation, true);
                return;
            }

            BankBox box = FindBankNoCreate();

            if (box != null && box.Opened)
                box.Close();

            Point3D oldLocation = m_Location;
            Map oldMap = m_Map;

            Region oldRegion = m_Region;

            if (oldMap != null)
            {
                oldMap.OnLeave(this);

                ClearScreen();
                SendRemovePacket();
            }

            for (int i = 0; i < m_Items.Count; ++i)
                (m_Items[i] as Item).Map = map;

            m_Map = map;

            m_Location = newLocation;

            NetState ns = m_NetState;

            if (m_Map != null)
            {
                m_Map.OnEnter(this);

                UpdateRegion();

                if (ns != null && m_Map != null)
                {
                    ns.Sequence = 0;
                    ns.Send(new MapChange(this));
                    ns.Send(new MapPatches());
                    ns.Send(SeasonChange.Instantiate(GetSeason(), true));

                    if (ns.StygianAbyss)
                        ns.Send(new MobileUpdate(this));
                    else
                        ns.Send(new MobileUpdateOld(this));

                    ClearFastwalkStack();
                }
            }
            else
            {
                UpdateRegion();
            }

            if (ns != null)
            {
                if (m_Map != null)
                    Send(new ServerChange(this, m_Map));

                ns.Sequence = 0;
                ClearFastwalkStack();

                if (ns.StygianAbyss)
                {
                    Send(new MobileIncoming(this, this));
                    Send(new MobileUpdate(this));
                    CheckLightLevels(true);
                    Send(new MobileUpdate(this));
                }
                else
                {
                    Send(new MobileIncomingOld(this, this));
                    Send(new MobileUpdateOld(this));
                    CheckLightLevels(true);
                    Send(new MobileUpdateOld(this));
                }
            }

            SendEverything();
            SendIncomingPacket();

            if (ns != null)
            {
                ns.Sequence = 0;
                ClearFastwalkStack();

                if (ns.StygianAbyss)
                {
                    Send(new MobileIncoming(this, this));
                    Send(SupportedFeatures.Instantiate(ns));
                    Send(new MobileUpdate(this));
                    Send(new MobileAttributes(this));
                }
                else
                {
                    Send(new MobileIncomingOld(this, this));
                    Send(SupportedFeatures.Instantiate(ns));
                    Send(new MobileUpdateOld(this));
                    Send(new MobileAttributes(this));
                }
            }

            OnMapChange(oldMap);
            OnLocationChange(oldLocation);

            if (m_Region != null)
                m_Region.OnLocationChanged(this, oldLocation);
            /*if (this.m_Map == map)
            {
                this.SetLocation(newLocation, true);
            }
            else
            {
                Server.Items.BankBox box = this.FindBankNoCreate();
                if ((box != null) && box.Opened)
                {
                    box.Close();
                }
                Point3D location = this.m_Location;
                Server.Map oldMap = this.m_Map;
                Server.Region old = this.m_Region;
                if (oldMap != null)
                {
                    oldMap.OnLeave(this);
                    this.ClearScreen();
                    this.SendRemovePacket();
                }
                for (int i = 0; i < this.m_Items.Count; i++)
                {
                    ((Item) this.m_Items[i]).Map = map;
                }
                this.m_Map = map;
                this.m_Region.InternalExit(this);
                this.m_Location = newLocation;
                if (this.m_Map != null)
                {
                    this.m_Map.OnEnter(this);
                    this.m_Region = Server.Region.Find(this.m_Location, this.m_Map);
                    this.OnRegionChange(old, this.m_Region);
                    this.m_Region.InternalEnter(this);
                    Server.Network.NetState netState = this.m_NetState;
                    if ((netState != null) && (this.m_Map != null))
                    {
                        netState.Sequence = 0;
                        netState.Send(new MapChange(this));
                        netState.Send(new MapPatches());
                        netState.Send(SeasonChange.Instantiate(this.GetSeason(), true));
                        netState.Send(new MobileUpdate(this));
                        this.ClearFastwalkStack();
                    }
                }
                if (this.m_NetState != null)
                {
                    if (this.m_Map != null)
                    {
                        this.Send(new ServerChange(this, this.m_Map));
                    }
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.Sequence = 0;
                        this.ClearFastwalkStack();
                    }
                    this.Send(new MobileIncoming(this, this));
                    this.Send(new MobileUpdate(this));
                    this.CheckLightLevels(true);
                    this.Send(new MobileUpdate(this));
                }
                this.SendEverything();
                this.SendIncomingPacket();
                if (this.m_NetState != null)
                {
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.Sequence = 0;
                        this.ClearFastwalkStack();
                    }
                    this.Send(new MobileIncoming(this, this));
                    this.Send(SupportedFeatures.Instantiate());
                    this.Send(new MobileUpdate(this));
                    this.Send(new MobileAttributes(this));
                }
                this.OnMapChange(oldMap);
                this.OnLocationChange(location);
                this.m_Region.OnLocationChanged(this, location);
            }*/
        }

        public void MovingEffect(IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes)
        {
            Effects.SendMovingEffect(this, to, itemID, speed, duration, fixedDirection, explodes, 0, 0);
        }

        public void MovingEffect(IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int hue, int renderMode)
        {
            Effects.SendMovingEffect(this, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode);
        }

        public void MovingParticles(IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int effect, int explodeEffect, int explodeSound)
        {
            Effects.SendMovingParticles(this, to, itemID, speed, duration, fixedDirection, explodes, 0, 0, effect, explodeEffect, explodeSound, 0);
        }

        public void MovingParticles(IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int effect, int explodeEffect, int explodeSound, int unknown)
        {
            Effects.SendMovingParticles(this, to, itemID, speed, duration, fixedDirection, explodes, effect, explodeEffect, explodeSound, unknown);
        }

        public void MovingParticles(IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int hue, int renderMode, int effect, int explodeEffect, int explodeSound, int unknown)
        {
            Effects.SendMovingParticles(this, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode, effect, explodeEffect, explodeSound, (EffectLayer) 0xff, unknown);
        }

        public void MovingParticles(IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int hue, int renderMode, int effect, int explodeEffect, int explodeSound, EffectLayer layer, int unknown)
        {
            Effects.SendMovingParticles(this, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode, effect, explodeEffect, explodeSound, layer, unknown);
        }

        public virtual bool MutateSpeech(ArrayList hears, ref string text, ref object context)
        {
            if (this.Alive)
            {
                return false;
            }
            StringBuilder builder = new StringBuilder(text.Length, text.Length);
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] != ' ')
                {
                    builder.Append(m_GhostChars[Utility.Random(m_GhostChars.Length)]);
                }
                else
                {
                    builder.Append(' ');
                }
            }
            text = builder.ToString();
            context = m_GhostMutateContext;
            return true;
        }

        public void NonlocalOverheadMessage(MessageType type, int hue, int number)
        {
            this.NonlocalOverheadMessage(type, hue, number, "");
        }

        public void NonlocalOverheadMessage(MessageType type, int hue, bool ascii, string text)
        {
            if (this.m_Map != null)
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if ((state == this.m_NetState) || !state.Mobile.CanSee(this))
                    {
                        continue;
                    }
                    if (p == null)
                    {
                        if (ascii)
                        {
                            p = new AsciiMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.Name, text);
                        }
                        else
                        {
                            p = new UnicodeMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.Language, this.Name, text);
                        }
                    }
                    state.Send(p);
                }
                clientsInRange.Free();
            }
        }

        public void NonlocalOverheadMessage(MessageType type, int hue, int number, string args)
        {
            if (this.m_Map != null)
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if ((state != this.m_NetState) && state.Mobile.CanSee(this))
                    {
                        if (p == null)
                        {
                            p = new MessageLocalized(this.m_Serial, (int) this.Body, type, hue, 3, number, this.Name, args);
                        }
                        state.Send(p);
                    }
                }
                clientsInRange.Free();
            }
        }

        public virtual void OnAccessLevelChanged(Server.AccessLevel oldLevel)
        {
        }

        public virtual void OnAfterDelete()
        {
            this.StopAggrExpire();
            this.CheckAggrExpire();
            if (this.m_PoisonTimer != null)
            {
                this.m_PoisonTimer.Stop();
            }
            if (this.m_HitsTimer != null)
            {
                this.m_HitsTimer.Stop();
            }
            if (this.m_StamTimer != null)
            {
                this.m_StamTimer.Stop();
            }
            if (this.m_ManaTimer != null)
            {
                this.m_ManaTimer.Stop();
            }
            this.m_CombatTimer.Stop();
            this.m_ExpireCombatant.Stop();
            this.m_ExpireCriminal.Stop();
            this.m_LogoutTimer.Stop();
            if (this.m_WarmodeTimer != null)
            {
                this.m_WarmodeTimer.Stop();
            }
            if (this.m_ParaTimer != null)
            {
                this.m_ParaTimer.Stop();
            }
            if (this.m_FrozenTimer != null)
            {
                this.m_FrozenTimer.Stop();
            }
        }

        public virtual void OnAfterResurrect()
        {
        }

        public virtual void OnAosSingleClick(Mobile from)
        {
            ObjectPropertyList propertyList = this.PropertyList;
            if (propertyList.Header > 0)
            {
                int nameHue;
                if (this.m_NameHue != -1)
                {
                    nameHue = this.m_NameHue;
                }
                else if (this.m_AccessLevel > Server.AccessLevel.Player)
                {
                    nameHue = 11;
                }
                else
                {
                    nameHue = Notoriety.GetHue(Notoriety.Compute(from, this));
                }
                from.Send(new MessageLocalized(this.m_Serial, (int) this.Body, MessageType.Label, nameHue, 3, propertyList.Header, this.Name, propertyList.HeaderArgs));
            }
        }

        public virtual bool OnBeforeDeath()
        {
            return true;
        }

        public virtual void OnBeforeResurrect()
        {
        }

        public virtual void OnBeneficialAction(Mobile target, bool isCriminal)
        {
            if (isCriminal)
            {
                this.CriminalAction(false);
            }
        }

        public virtual void OnCombatantChange()
        {
        }

        public virtual void OnConnected()
        {
        }

        public virtual void OnCured(Mobile from, Server.Poison oldPoison)
        {
        }

        public virtual void OnDamage(int amount, Mobile from, bool willKill)
        {
        }

        public virtual void OnDeath(Container c)
        {
            int deathSound = this.GetDeathSound();
            if (deathSound >= 0)
            {
                Effects.PlaySound(this, this.Map, deathSound);
            }
            if (!this.m_Player)
            {
                this.Delete();
            }
            else
            {
                this.Send(DeathStatus.Instantiate(true));
                this.Warmode = false;
                this.BodyMod = 0;
                this.Body = this.Female ? 0x193 : 0x192;
                Item item = new Item(0x204e);
                item.Movable = false;
                item.Layer = Layer.OuterTorso;
                this.AddItem(item);
                this.m_Items.Remove(item);
                this.m_Items.Insert(0, item);
                this.Poison = null;
                this.Combatant = null;
                this.Hits = 0;
                this.Stam = 0;
                this.Mana = 0;
                EventSink.InvokePlayerDeath(new PlayerDeathEventArgs(this));
                ProcessDeltaQueue();
                this.Send(DeathStatus.Instantiate(false));
            }
        }

        public virtual void OnDelete()
        {
        }

        public virtual void OnDisconnected()
        {
        }

        public virtual void OnDoubleClick(Mobile from)
        {
            if ((this == from) && (!m_DisableDismountInWarmode || !this.m_Warmode))
            {
                IMount mount = this.Mount;
                if (mount != null)
                {
                    mount.Rider = null;
                    return;
                }
            }
            if (this.CanPaperdollBeOpenedBy(from))
            {
                this.DisplayPaperdollTo(from);
            }
        }

        public virtual void OnDoubleClickCantSee(Mobile from)
        {
        }

        public virtual void OnDoubleClickDead(Mobile from)
        {
            if (this.CanPaperdollBeOpenedBy(from))
            {
                this.DisplayPaperdollTo(from);
            }
        }

        public virtual void OnDoubleClickOutOfRange(Mobile from)
        {
        }

        public virtual bool OnDragDrop(Mobile from, Item dropped)
        {
            if (from == this)
            {
                Container pack = this.Backpack;

                if (pack != null)
                    return dropped.DropToItem(from, pack, new Point3D(-1, -1, 0));

                return false;
            }
            else if (from.Player && this.Player && from.Alive && this.Alive && from.InRange(Location, 2))
            {
                NetState ourState = m_NetState;
                NetState theirState = from.m_NetState;

                if (ourState != null && theirState != null)
                {
                    SecureTradeContainer cont = theirState.FindTradeContainer(this);

                    if (!from.CheckTrade(this, dropped, cont, true, true, 0, 0))
                        return false;

                    if (cont == null)
                        cont = theirState.AddTrade(ourState);

                    cont.DropItem(dropped);

                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public virtual bool CheckTrade(Mobile to, Item item, SecureTradeContainer cont, bool message, bool checkItems, int plusItems, int plusWeight)
        {
            return true;
        }

        public virtual bool AllowEquipFrom(Mobile mob)
        {
            return (mob == this || (mob.AccessLevel >= AccessLevel.GameMaster && mob.AccessLevel > this.AccessLevel));
        }

        public bool HasTrade
        {
            get
            {
                if (m_NetState != null)
                    return m_NetState.Trades.Count > 0;

                return false;
            }
        }

        public virtual bool OnDragLift(Item item)
        {
            return true;
        }

        public virtual bool OnDroppedItemInto(Item item, Container container, Point3D loc)
        {
            return true;
        }

        public virtual bool OnDroppedItemOnto(Item item, Item target)
        {
            return true;
        }

        public virtual bool OnDroppedItemToItem(Item item, Item target, Point3D loc)
        {
            return true;
        }

        public virtual bool OnDroppedItemToMobile(Item item, Mobile target)
        {
            return true;
        }

        public virtual bool OnDroppedItemToWorld(Item item, Point3D location)
        {
            return true;
        }

        public virtual bool OnEquip(Item item)
        {
            return true;
        }

        public virtual void OnFailedCure(Mobile from)
        {
        }

        public virtual void OnFameChange(int oldValue)
        {
        }

        public virtual void OnGenderChanged(bool oldFemale)
        {
        }

        public virtual void OnGuildChange(BaseGuild oldGuild)
        {
        }

        public virtual void OnGuildTitleChange(string oldTitle)
        {
        }

        public virtual void OnHarmfulAction(Mobile target, bool isCriminal)
        {
            if (isCriminal)
            {
                this.CriminalAction(false);
            }
        }

        public virtual void OnHelpRequest(Mobile from)
        {
        }

        public virtual void OnHigherPoison(Mobile from, Server.Poison poison)
        {
        }

        public virtual void OnItemAdded(Item item)
        {
        }

        public virtual void OnItemLifted(Mobile from, Item item)
        {
        }

        public virtual void OnItemRemoved(Item item)
        {
        }

        public virtual void OnItemUsed(Mobile from, Item item)
        {
        }

        public virtual void OnKarmaChange(int oldValue)
        {
        }

        public virtual void OnKillsChange(int oldValue)
        {
        }

        protected virtual void OnLocationChange(Point3D oldLocation)
        {
        }

        protected virtual void OnMapChange(Server.Map oldMap)
        {
        }

        protected virtual bool OnMove(Server.Direction d)
        {
            if ((this.m_Hidden && (this.m_AccessLevel == Server.AccessLevel.Player)) && (((this.m_AllowedStealthSteps-- <= 0) || ((d & Server.Direction.Running) != Server.Direction.North)) || this.Mounted))
            {
                this.RevealingAction();
            }
            return true;
        }

        public virtual void OnMovement(Mobile m, Point3D oldLocation)
        {
        }

        public virtual bool OnMoveOff(Mobile m)
        {
            return true;
        }

        public virtual bool OnMoveOver(Mobile m)
        {
            if (((this.m_Map != null) && !this.m_Deleted) && ((this.m_Map.Rules & MapRules.FreeMovement) == MapRules.FeluccaRules))
            {
                if ((!this.Alive || !m.Alive) || (this.IsDeadBondedPet || m.IsDeadBondedPet))
                {
                    return true;
                }
                if (this.m_Hidden && (this.m_AccessLevel > Server.AccessLevel.Player))
                {
                    return true;
                }
                if (!m.m_Pushing)
                {
                    int num;
                    m.m_Pushing = true;
                    if (m.AccessLevel > Server.AccessLevel.Player)
                    {
                        num = this.m_Hidden ? 0xf8ca1 : 0xf8ca0;
                    }
                    else if (m.Stam == m.StamMax)
                    {
                        num = this.m_Hidden ? 0xf8ca3 : 0xf8ca2;
                        m.Stam -= 10;
                        m.RevealingAction();
                    }
                    else
                    {
                        return false;
                    }
                    m.SendLocalizedMessage(num);
                }
            }
            return true;
        }

        public virtual void OnNetStateChanged()
        {
        }

        public virtual void OnPaperdollRequest()
        {
            if (this.CanPaperdollBeOpenedBy(this))
            {
                this.DisplayPaperdollTo(this);
            }
        }

        public virtual void OnPoisoned(Mobile from, Server.Poison poison, Server.Poison oldPoison)
        {
            if (poison != null)
            {
                this.LocalOverheadMessage(MessageType.Regular, 0x22, 0xfe9a9 + (poison.Level * 2));
                this.NonlocalOverheadMessage(MessageType.Regular, 0x22, (int) (0xfe9aa + (poison.Level * 2)), this.Name);
            }
        }

        public virtual void OnPoisonImmunity(Mobile from, Server.Poison poison)
        {
            this.PublicOverheadMessage(MessageType.Emote, 0x3b2, 0xf57de);
        }

        public virtual void OnRawDexChange(int oldValue)
        {
        }

        public virtual void OnRawIntChange(int oldValue)
        {
        }

        public virtual void OnRawStatChange(StatType stat, int oldValue)
        {
        }

        public virtual void OnRawStrChange(int oldValue)
        {
        }

        public virtual void OnRegionChange(Server.Region Old, Server.Region New)
        {
        }

        public virtual void OnSaid(SpeechEventArgs e)
        {
            if (this.m_Squelched)
            {
                this.SendLocalizedMessage(0x7a1c8);
                e.Blocked = true;
            }
            if (!e.Blocked)
            {
                this.RevealingAction();
            }
        }

        public virtual void OnSectorActivate()
        {
        }

        public virtual void OnSectorDeactivate()
        {
        }

        public virtual void OnSingleClick(Mobile from)
        {
            if (!this.m_Deleted && (((this.AccessLevel != Server.AccessLevel.Player) || !DisableHiddenSelfClick) || (!this.Hidden || (from != this))))
            {
                int nameHue;
                string str5;
                if (m_GuildClickMessage)
                {
                    BaseGuild guild = this.m_Guild;
                    if ((guild != null) && (this.m_DisplayGuildTitle || (this.m_Player && (guild.Type != GuildType.None))))
                    {
                        string str2;
                        string guildTitle = this.GuildTitle;
                        if (guildTitle == null)
                        {
                            guildTitle = "";
                        }
                        else
                        {
                            guildTitle = guildTitle.Trim();
                        }
                        if ((guild.Type >= GuildType.None) && ((int)guild.Type < m_GuildTypes.Length))
                        {
                            str2 = m_GuildTypes[(int) guild.Type];
                        }
                        else
                        {
                            str2 = "";
                        }
                        string text = string.Format((guildTitle.Length <= 0) ? "[{1}]{2}" : "[{0}, {1}]{2}", guildTitle, guild.Abbreviation, str2);
                        this.PrivateOverheadMessage(MessageType.Regular, this.SpeechHue, true, text, from.NetState);
                    }
                }
                if (this.m_NameHue != -1)
                {
                    nameHue = this.m_NameHue;
                }
                else if (this.AccessLevel > Server.AccessLevel.Player)
                {
                    nameHue = 11;
                }
                else
                {
                    nameHue = Notoriety.GetHue(Notoriety.Compute(from, this));
                }
                string name = this.Name;
                if (name == null)
                {
                    name = string.Empty;
                }
                if ((this.ShowFameTitle && (this.m_Player || this.m_Body.IsHuman)) && (this.m_Fame >= 0x2710))
                {
                    str5 = (this.m_Female ? "Lady " : "Lord ") + name;
                }
                else
                {
                    str5 = name;
                }
                if ((this.ClickTitle && (this.Title != null)) && (this.Title.Length > 0))
                {
                    str5 = str5 + " " + this.Title;
                }
                this.PrivateOverheadMessage(MessageType.Label, nameHue, m_AsciiClickMessage, str5, from.NetState);
            }
        }

        public virtual void OnSkillChange(SkillName skill, double oldBase)
        {
        }

        public virtual void OnSkillInvalidated(Skill skill)
        {
        }

        public virtual void OnSkillsQuery(Mobile from)
        {
            if (from == this)
            {
                this.Send(new SkillUpdate(this.m_Skills));
            }
        }

        public virtual void OnSpeech(SpeechEventArgs e)
        {
        }

        public virtual void OnSpellCast(ISpell spell)
        {
        }

        public virtual void OnStatsQuery(Mobile from)
        {
            if (((from.Map == this.Map) && Utility.InUpdateRange(this, from)) && from.CanSee(this))
            {
                from.Send(new MobileStatus(from, this));
            }
            if (from == this)
            {
                this.Send(new StatLockInfo(this));
            }
            IParty party = this.m_Party as IParty;
            if (party != null)
            {
                party.OnStatsQuery(from, this);
            }
        }

        public virtual void OnSubItemAdded(Item item)
        {
        }

        public virtual void OnSubItemRemoved(Item item)
        {
        }

        protected virtual void OnTargetChange()
        {
        }

        public virtual void OnWeightChange(int oldValue)
        {
        }

        public void Paralyze(TimeSpan duration)
        {
            if (!this.m_Paralyzed)
            {
                this.Paralyzed = true;
                this.m_ParaTimer = new ParalyzedTimer(this, duration);
                this.m_ParaTimer.Start();
            }
        }

        public bool PlaceInBackpack(Item item)
        {
            if (item.Deleted)
            {
                return false;
            }
            Container backpack = this.Backpack;
            return ((backpack != null) && backpack.TryDropItem(this, item, false));
        }

        public void PlaySound(int soundID)
        {
            if ((soundID != -1) && (this.m_Map != null))
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if (state.Mobile.CanSee(this))
                    {
                        if (p == null)
                        {
                            p = new Server.Network.PlaySound(soundID, this);
                        }
                        state.Send(p);
                    }
                }
                clientsInRange.Free();
            }
        }

        public void PrivateOverheadMessage(MessageType type, int hue, int number, Server.Network.NetState state)
        {
            this.PrivateOverheadMessage(type, hue, number, "", state);
        }

        public void PrivateOverheadMessage(MessageType type, int hue, bool ascii, string text, Server.Network.NetState state)
        {
            if (state != null)
            {
                if (ascii)
                {
                    state.Send(new AsciiMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.Name, text));
                }
                else
                {
                    state.Send(new UnicodeMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.m_Language, this.Name, text));
                }
            }
        }

        public void PrivateOverheadMessage(MessageType type, int hue, int number, string args, Server.Network.NetState state)
        {
            if (state != null)
            {
                state.Send(new MessageLocalized(this.m_Serial, (int) this.Body, type, hue, 3, number, this.Name, args));
            }
        }

        public virtual void ProcessDelta()
        {
            Mobile m = this;
            MobileDelta deltaFlags = m.m_DeltaFlags;
            if (deltaFlags != MobileDelta.None)
            {
                MobileDelta delta2 = deltaFlags & MobileDelta.Attributes;
                m.m_DeltaFlags = MobileDelta.None;
                m.m_InDeltaQueue = false;
                bool flag = false;
                bool flag2 = false;
                bool flag3 = false;
                bool flag4 = false;
                bool flag5 = false;
                bool flag6 = false;
                bool flag7 = false;
                bool flag8 = false;
                bool flag9 = false;
                bool flag10 = false;
                bool flag11 = false;
                bool flag12 = false;
                bool flag13 = false;
                bool flag14 = Core.AOS && ((deltaFlags & MobileDelta.Properties) != MobileDelta.None);
                if (delta2 != MobileDelta.None)
                {
                    flag5 = true;
                    if (delta2 == MobileDelta.Attributes)
                    {
                        flag4 = true;
                    }
                    else
                    {
                        flag = (delta2 & MobileDelta.Hits) != MobileDelta.None;
                        flag2 = (delta2 & MobileDelta.Stam) != MobileDelta.None;
                        flag3 = (delta2 & MobileDelta.Mana) != MobileDelta.None;
                    }
                }
                if ((deltaFlags & MobileDelta.GhostUpdate) != MobileDelta.None)
                {
                    flag7 = true;
                }
                if ((deltaFlags & MobileDelta.Hue) != MobileDelta.None)
                {
                    flag7 = true;
                    flag8 = true;
                    flag9 = true;
                }
                if ((deltaFlags & MobileDelta.Direction) != MobileDelta.None)
                {
                    flag13 = true;
                    flag8 = true;
                }
                if ((deltaFlags & MobileDelta.Body) != MobileDelta.None)
                {
                    flag8 = true;
                    flag6 = true;
                }
                if ((deltaFlags & (MobileDelta.Noto | MobileDelta.Flags)) != MobileDelta.None)
                {
                    flag12 = true;
                }
                if ((deltaFlags & MobileDelta.Name) != MobileDelta.None)
                {
                    flag4 = false;
                    flag = false;
                    flag5 = flag2 || flag3;
                    flag10 = true;
                }
                if ((deltaFlags & (MobileDelta.WeaponDamage | MobileDelta.Resistances | MobileDelta.TithingPoints | MobileDelta.Followers | MobileDelta.StatCap | MobileDelta.Armor | MobileDelta.Weight | MobileDelta.Gold | MobileDelta.Stat)) != MobileDelta.None)
                {
                    flag11 = true;
                }
                MobileMoving[] movingPacketCache = m_MovingPacketCache;
                if (flag12 || flag13)
                {
                    for (int i = 0; i < movingPacketCache.Length; i++)
                    {
                        movingPacketCache[i] = null;
                    }
                }
                Server.Network.NetState netState = m.m_NetState;
                if (netState != null)
                {
                    if (flag8)
                    {
                        netState.Sequence = 0;
                        netState.Send(new MobileUpdate(m));
                        this.ClearFastwalkStack();
                    }
                    if (flag6)
                    {
                        netState.Send(new MobileIncoming(m, m));
                    }
                    if (flag12)
                    {
                        MobileMoving moving2;
                        int index = Notoriety.Compute(m, m);
                        movingPacketCache[index] = moving2 = new MobileMoving(m, index);
                        netState.Send(moving2);
                    }
                    if (flag10 || flag11)
                    {
                        netState.Send(new MobileStatusExtended(m));
                    }
                    else if (flag4)
                    {
                        netState.Send(new MobileAttributes(m));
                    }
                    else if (flag5)
                    {
                        if (flag)
                        {
                            netState.Send(new MobileHits(m));
                        }
                        if (flag2)
                        {
                            netState.Send(new MobileStam(m));
                        }
                        if (flag3)
                        {
                            netState.Send(new MobileMana(m));
                        }
                    }
                    if (flag2 || flag3)
                    {
                        IParty party = this.m_Party as IParty;
                        if ((party != null) && flag2)
                        {
                            party.OnStamChanged(this);
                        }
                        if ((party != null) && flag3)
                        {
                            party.OnManaChanged(this);
                        }
                    }
                    if (flag14)
                    {
                        netState.Send(this.OPLPacket);
                    }
                }
                flag12 = flag12 || flag13;
                flag6 = flag6 || flag7;
                if ((m.m_Map != null) && (((flag9 || flag6) || (flag10 || flag)) || (flag12 || flag14)))
                {
                    IPooledEnumerable clientsInRange = m.Map.GetClientsInRange(m.m_Location);
                    Packet p = null;
                    Packet packet2 = null;
                    Packet packet3 = null;
                    foreach (Server.Network.NetState state2 in clientsInRange)
                    {
                        Mobile mobile = state2.Mobile;
                        if ((mobile != m) && mobile.CanSee(m))
                        {
                            if (flag9)
                            {
                                state2.Send(m.RemovePacket);
                            }
                            if (flag6)
                            {
                                state2.Send(new MobileIncoming(mobile, m));
                                if (m.IsDeadBondedPet)
                                {
                                    state2.Send(new BondedStatus(0, m.m_Serial, 1));
                                }
                            }
                            if (flag12)
                            {
                                int num3 = Notoriety.Compute(mobile, m);
                                MobileMoving moving = movingPacketCache[num3];
                                if (moving == null)
                                {
                                    movingPacketCache[num3] = moving = new MobileMoving(m, num3);
                                }
                                state2.Send(moving);
                            }
                            if (flag10)
                            {
                                if (m.CanBeRenamedBy(mobile))
                                {
                                    if (packet2 == null)
                                    {
                                        packet2 = new MobileStatusCompact(true, m);
                                    }
                                    state2.Send(packet2);
                                }
                                else
                                {
                                    if (packet3 == null)
                                    {
                                        packet3 = new MobileStatusCompact(false, m);
                                    }
                                    state2.Send(packet3);
                                }
                            }
                            else if (flag)
                            {
                                if (p == null)
                                {
                                    p = new MobileHitsN(m);
                                }
                                state2.Send(p);
                            }
                            if (flag14)
                            {
                                state2.Send(this.OPLPacket);
                            }
                        }
                    }
                    clientsInRange.Free();
                }
            }
        }

        public static void ProcessDeltaQueue()
        {
            int count = m_DeltaQueue.Count;
            int num2 = 0;
            while ((m_DeltaQueue.Count > 0) && (num2++ < count))
            {
                ((Mobile) m_DeltaQueue.Dequeue()).ProcessDelta();
            }
        }

        public void PublicOverheadMessage(MessageType type, int hue, int number)
        {
            this.PublicOverheadMessage(type, hue, number, "", true);
        }

        public void PublicOverheadMessage(MessageType type, int hue, bool ascii, string text)
        {
            this.PublicOverheadMessage(type, hue, ascii, text, true);
        }

        public void PublicOverheadMessage(MessageType type, int hue, int number, string args)
        {
            this.PublicOverheadMessage(type, hue, number, args, true);
        }

        public void PublicOverheadMessage(MessageType type, int hue, bool ascii, string text, bool noLineOfSight)
        {
            if (this.m_Map != null)
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if (!state.Mobile.CanSee(this) || (!noLineOfSight && !state.Mobile.InLOS(this)))
                    {
                        continue;
                    }
                    if (p == null)
                    {
                        if (ascii)
                        {
                            p = new AsciiMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.Name, text);
                        }
                        else
                        {
                            p = new UnicodeMessage(this.m_Serial, (int) this.Body, type, hue, 3, this.m_Language, this.Name, text);
                        }
                    }
                    state.Send(p);
                }
                clientsInRange.Free();
            }
        }

        public void PublicOverheadMessage(MessageType type, int hue, int number, string args, bool noLineOfSight)
        {
            if (this.m_Map != null)
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if (state.Mobile.CanSee(this) && (noLineOfSight || state.Mobile.InLOS(this)))
                    {
                        if (p == null)
                        {
                            p = new MessageLocalized(this.m_Serial, (int) this.Body, type, hue, 3, number, this.Name, args);
                        }
                        state.Send(p);
                    }
                }
                clientsInRange.Free();
            }
        }

        public void PublicOverheadMessage(MessageType type, int hue, int number, AffixType affixType, string affix, string args)
        {
            this.PublicOverheadMessage(type, hue, number, affixType, affix, args, true);
        }

        public void PublicOverheadMessage(MessageType type, int hue, int number, AffixType affixType, string affix, string args, bool noLineOfSight)
        {
            if (this.m_Map != null)
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if (state.Mobile.CanSee(this) && (noLineOfSight || state.Mobile.InLOS(this)))
                    {
                        if (p == null)
                        {
                            p = new MessageLocalizedAffix(this.m_Serial, (int) this.Body, type, hue, 3, number, this.Name, affixType, affix, args);
                        }
                        state.Send(p);
                    }
                }
                clientsInRange.Free();
            }
        }

        public virtual DamageEntry RegisterDamage(int amount, Mobile from)
        {
            DamageEntry entry = this.FindDamageEntryFor(from);
            if (entry == null)
            {
                entry = new DamageEntry(from);
            }
            entry.DamageGiven += amount;
            entry.LastDamage = DateTime.Now;
            this.m_DamageEntries.Remove(entry);
            this.m_DamageEntries.Add(entry);
            Mobile damageMaster = from.GetDamageMaster(this);
            if (damageMaster != null)
            {
                ArrayList responsible = entry.Responsible;
                if (responsible == null)
                {
                    entry.Responsible = responsible = new ArrayList();
                }
                DamageEntry entry2 = null;
                for (int i = 0; i < responsible.Count; i++)
                {
                    DamageEntry entry3 = (DamageEntry) responsible[i];
                    if (entry3.Damager == damageMaster)
                    {
                        entry2 = entry3;
                        break;
                    }
                }
                if (entry2 == null)
                {
                    responsible.Add(entry2 = new DamageEntry(damageMaster));
                }
                entry2.DamageGiven += amount;
                entry2.LastDamage = DateTime.Now;
            }
            return entry;
        }

        public void RemoveAggressed(Mobile aggressed)
        {
            if (!this.m_Deleted)
            {
                ArrayList list = this.m_Aggressed;
                for (int i = 0; i < list.Count; i++)
                {
                    AggressorInfo info = (AggressorInfo) list[i];
                    if (info.Defender == aggressed)
                    {
                        this.m_Aggressed.RemoveAt(i);
                        info.Free();
                        if ((this.m_NetState != null) && this.CanSee(aggressed))
                        {
                            this.m_NetState.Send(new MobileIncoming(this, aggressed));
                        }
                        break;
                    }
                }
                this.UpdateAggrExpire();
            }
        }

        public void RemoveAggressor(Mobile aggressor)
        {
            if (!this.m_Deleted)
            {
                ArrayList aggressors = this.m_Aggressors;
                for (int i = 0; i < aggressors.Count; i++)
                {
                    AggressorInfo info = (AggressorInfo) aggressors[i];
                    if (info.Attacker == aggressor)
                    {
                        this.m_Aggressors.RemoveAt(i);
                        info.Free();
                        if ((this.m_NetState != null) && this.CanSee(aggressor))
                        {
                            this.m_NetState.Send(new MobileIncoming(this, aggressor));
                        }
                        break;
                    }
                }
                this.UpdateAggrExpire();
            }
        }

        public void RemoveItem(Item item)
        {
            if (((item != null) && (this.m_Items != null)) && this.m_Items.Contains(item))
            {
                item.SendRemovePacket();
                int count = this.m_Items.Count;
                this.m_Items.Remove(item);
                if (!(item is Server.Items.BankBox))
                {
                    this.TotalWeight -= item.TotalWeight + item.PileWeight;
                    this.TotalGold -= item.TotalGold;
                }
                item.Parent = null;
                item.OnRemoved(this);
                this.OnItemRemoved(item);
                if (((item.PhysicalResistance != 0) || (item.FireResistance != 0)) || (((item.ColdResistance != 0) || (item.PoisonResistance != 0)) || (item.EnergyResistance != 0)))
                {
                    this.UpdateResistances();
                }
            }
        }

        public virtual void RemoveResistanceMod(ResistanceMod toRemove)
        {
            if (this.m_ResistMods != null)
            {
                this.m_ResistMods.Remove(toRemove);
                if (this.m_ResistMods.Count == 0)
                {
                    this.m_ResistMods = null;
                }
            }
            this.UpdateResistances();
        }

        public virtual void RemoveSkillMod(SkillMod mod)
        {
            if (mod != null)
            {
                this.ValidateSkillMods();
                this.InternalRemoveSkillMod(mod);
            }
        }

        public bool RemoveStatMod(string name)
        {
            for (int i = 0; i < this.m_StatMods.Count; i++)
            {
                StatMod mod = (StatMod) this.m_StatMods[i];
                if (mod.Name == name)
                {
                    this.m_StatMods.RemoveAt(i);
                    this.CheckStatTimers();
                    this.Delta(MobileDelta.Stat);
                    return true;
                }
            }
            return false;
        }

        public virtual void Resurrect()
        {
            if ((!this.Alive && this.Region.OnResurrect(this)) && this.CheckResurrect())
            {
                this.OnBeforeResurrect();
                Server.Items.BankBox box = this.FindBankNoCreate();
                if ((box != null) && box.Opened)
                {
                    box.Close();
                }
                this.Poison = null;
                this.Warmode = false;
                this.Hits = 10;
                this.Stam = this.StamMax;
                this.Mana = 0;
                this.BodyMod = 0;
                this.Body = this.m_Female ? 0x191 : 400;
                ProcessDeltaQueue();
                for (int i = this.m_Items.Count - 1; i >= 0; i--)
                {
                    if (i < this.m_Items.Count)
                    {
                        Item item = (Item) this.m_Items[i];
                        if (item.ItemID == 0x204e)
                        {
                            item.Delete();
                        }
                    }
                }
                this.SendIncomingPacket();
                this.SendIncomingPacket();
                this.OnAfterResurrect();
            }
        }

        public virtual void RevealingAction()
        {
            if (this.m_Hidden && (this.m_AccessLevel == Server.AccessLevel.Player))
            {
                this.Hidden = false;
            }
            this.DisruptiveAction();
        }

        public virtual int SafeBody(int body)
        {
            int num = -1;
            for (int i = 0; (num < 0) && (i < m_InvalidBodies.Length); i++)
            {
                num = m_InvalidBodies[i] - body;
            }
            if (num != 0)
            {
                return body;
            }
            return 0;
        }

        public void Say(int number)
        {
            this.Say(number, "");
        }

        public void Say(string text)
        {
            this.PublicOverheadMessage(MessageType.Regular, this.m_SpeechHue, false, text);
        }

        public void Say(bool ascii, string text)
        {
            this.PublicOverheadMessage(MessageType.Regular, this.m_SpeechHue, ascii, text);
        }

        public void Say(int number, string args)
        {
            this.PublicOverheadMessage(MessageType.Regular, this.m_SpeechHue, number, args);
        }

        public void Say(string format, params object[] args)
        {
            this.Say(string.Format(format, args));
        }

        public void Say(int number, AffixType type, string affix, string args)
        {
            this.PublicOverheadMessage(MessageType.Regular, this.m_SpeechHue, number, type, affix, args);
        }

        public void SayTo(Mobile to, int number)
        {
            to.Send(new MessageLocalized(this.m_Serial, (int) this.Body, MessageType.Regular, this.m_SpeechHue, 3, number, this.Name, ""));
        }

        public void SayTo(Mobile to, string text)
        {
            this.SayTo(to, false, text);
        }

        public void SayTo(Mobile to, bool ascii, string text)
        {
            this.PrivateOverheadMessage(MessageType.Regular, this.m_SpeechHue, ascii, text, to.NetState);
        }

        public void SayTo(Mobile to, int number, string args)
        {
            to.Send(new MessageLocalized(this.m_Serial, (int) this.Body, MessageType.Regular, this.m_SpeechHue, 3, number, this.Name, args));
        }

        public void SayTo(Mobile to, string format, params object[] args)
        {
            this.SayTo(to, false, string.Format(format, args));
        }

        public void SayTo(Mobile to, bool ascii, string format, params object[] args)
        {
            this.SayTo(to, ascii, string.Format(format, args));
        }

        public bool Send(Packet p)
        {
            return this.Send(p, false);
        }

        public bool Send(Packet p, bool throwOnOffline)
        {
            if (this.m_NetState != null)
            {
                this.m_NetState.Send(p);
                return true;
            }
            if (throwOnOffline)
            {
                throw new MobileNotConnectedException(this, "Packet could not be sent.");
            }
            return false;
        }

        public virtual void SendActionMessage()
        {
            if (DateTime.Now >= this.m_NextActionMessage)
            {
                this.m_NextActionMessage = DateTime.Now + m_ActionMessageDelay;
                this.SendLocalizedMessage(0x7a197);
            }
        }

        public void SendAsciiMessage(string text)
        {
            this.SendAsciiMessage(0x3b2, text);
        }

        public void SendAsciiMessage(int hue, string text)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                netState.Send(new AsciiMessage(Server.Serial.MinusOne, -1, MessageType.Regular, hue, 3, "System", text));
            }
        }

        public void SendAsciiMessage(string format, params object[] args)
        {
            this.SendAsciiMessage(0x3b2, string.Format(format, args));
        }

        public void SendAsciiMessage(int hue, string format, params object[] args)
        {
            this.SendAsciiMessage(hue, string.Format(format, args));
        }

        public virtual void SendDamageToAll(int amount)
        {
            if (amount >= 0)
            {
                Server.Map map = this.m_Map;
                if (map != null)
                {
                    IPooledEnumerable clientsInRange = map.GetClientsInRange(this.m_Location);
                    Packet p = null;
                    foreach (Server.Network.NetState state in clientsInRange)
                    {
                        if (state.Mobile.CanSee(this))
                        {
                            if (p == null)
                            {
                                p = new DamagePacket(this, amount);
                            }
                            state.Send(p);
                        }
                    }
                    clientsInRange.Free();
                }
            }
        }

        public virtual void SendDropEffect(Item item)
        {
            if (DragEffects)
            {
                Server.Map map = this.m_Map;
                object rootParent = item.RootParent;
                if ((map != null) && ((rootParent == null) || (rootParent is Item)))
                {
                    IPooledEnumerable clientsInRange = map.GetClientsInRange(this.m_Location);
                    Packet p = null;
                    foreach (Server.Network.NetState state in clientsInRange)
                    {
                        if ((state.Mobile == this) || !state.Mobile.CanSee(this))
                        {
                            continue;
                        }
                        if (p == null)
                        {
                            IEntity entity;
                            if (rootParent == null)
                            {
                                entity = new Entity(Server.Serial.Zero, item.Location, map);
                            }
                            else
                            {
                                entity = new Entity(((Item) rootParent).Serial, ((Item) rootParent).Location, map);
                            }
                            p = new DragEffect(this, entity, item.ItemID, item.Hue, item.Amount);
                        }
                        state.Send(p);
                    }
                    clientsInRange.Free();
                }
            }
        }

        public void SendEverything()
        {
            Server.Network.NetState netState = this.m_NetState;
            if ((this.m_Map != null) && (netState != null))
            {
                IPooledEnumerable objectsInRange = this.m_Map.GetObjectsInRange(this.m_Location, Core.GlobalMaxUpdateRange);
                foreach (object obj2 in objectsInRange)
                {
                    if (obj2 is Item)
                    {
                        Item item = (Item) obj2;
                        if (this.CanSee(item) && this.InRange(item.Location, item.GetUpdateRange(this)))
                        {
                            item.SendInfoTo(netState);
                        }
                    }
                    else if (obj2 is Mobile)
                    {
                        Mobile m = (Mobile) obj2;
                        if (this.CanSee(m) && Utility.InUpdateRange(this.m_Location, m.m_Location))
                        {
                            netState.Send(new MobileIncoming(this, m));
                            if (m.IsDeadBondedPet)
                            {
                                netState.Send(new BondedStatus(0, m.m_Serial, 1));
                            }
                            if (ObjectPropertyList.Enabled)
                            {
                                netState.Send(m.OPLPacket);
                            }
                        }
                    }
                }
                objectsInRange.Free();
            }
        }

        public bool SendGump(Gump g)
        {
            return this.SendGump(g, false);
        }

        public bool SendGump(Gump g, bool throwOnOffline)
        {
            if (this.m_NetState != null)
            {
                g.SendTo(this.m_NetState);
                return true;
            }
            if (throwOnOffline)
            {
                throw new MobileNotConnectedException(this, "Gump could not be sent.");
            }
            return false;
        }

        public bool SendHuePicker(HuePicker p)
        {
            return this.SendHuePicker(p, false);
        }

        public bool SendHuePicker(HuePicker p, bool throwOnOffline)
        {
            if (this.m_NetState != null)
            {
                p.SendTo(this.m_NetState);
                return true;
            }
            if (throwOnOffline)
            {
                throw new MobileNotConnectedException(this, "Hue picker could not be sent.");
            }
            return false;
        }

        public void SendIncomingPacket()
        {
            if (this.m_Map != null)
            {
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if (state.Mobile.CanSee(this))
                    {
                        state.Send(new MobileIncoming(state.Mobile, this));
                        if (this.IsDeadBondedPet)
                        {
                            state.Send(new BondedStatus(0, this.m_Serial, 1));
                        }
                        if (ObjectPropertyList.Enabled)
                        {
                            state.Send(this.OPLPacket);
                        }
                    }
                }
                clientsInRange.Free();
            }
        }

        public void SendLocalizedMessage(int number)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                netState.Send(MessageLocalized.InstantiateGeneric(number));
            }
        }

        public void SendLocalizedMessage(int number, string args)
        {
            if ((args == null) || (args.Length == 0))
            {
                Server.Network.NetState netState = this.m_NetState;
                if (netState != null)
                {
                    netState.Send(MessageLocalized.InstantiateGeneric(number));
                }
            }
            else
            {
                Server.Network.NetState state2 = this.m_NetState;
                if (state2 != null)
                {
                    state2.Send(new MessageLocalized(Server.Serial.MinusOne, -1, MessageType.Regular, 0x3b2, 3, number, "System", args));
                }
            }
        }

        public void SendLocalizedMessage(int number, bool append, string affix)
        {
            this.SendLocalizedMessage(number, append, affix, "", 0x3b2);
        }

        public void SendLocalizedMessage(int number, string args, int hue)
        {
            if ((hue == 0x3b2) && ((args == null) || (args.Length == 0)))
            {
                Server.Network.NetState netState = this.m_NetState;
                if (netState != null)
                {
                    netState.Send(MessageLocalized.InstantiateGeneric(number));
                }
            }
            else
            {
                Server.Network.NetState state2 = this.m_NetState;
                if (state2 != null)
                {
                    state2.Send(new MessageLocalized(Server.Serial.MinusOne, -1, MessageType.Regular, hue, 3, number, "System", args));
                }
            }
        }

        public void SendLocalizedMessage(int number, bool append, string affix, string args)
        {
            this.SendLocalizedMessage(number, append, affix, args);
        }

        public void SendLocalizedMessage(int number, bool append, string affix, string args, int hue)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                netState.Send(new MessageLocalizedAffix(Server.Serial.MinusOne, -1, MessageType.Regular, hue, 3, number, "System", (append ? AffixType.Append : AffixType.Prepend) | AffixType.System, affix, args));
            }
        }

        public bool SendMenu(IMenu m)
        {
            return this.SendMenu(m, false);
        }

        public bool SendMenu(IMenu m, bool throwOnOffline)
        {
            if (this.m_NetState != null)
            {
                m.SendTo(this.m_NetState);
                return true;
            }
            if (throwOnOffline)
            {
                throw new MobileNotConnectedException(this, "Menu could not be sent.");
            }
            return false;
        }

        public void SendMessage(string text)
        {
            this.SendMessage(0x3b2, text);
        }

        public void SendMessage(int hue, string text)
        {
            Server.Network.NetState netState = this.m_NetState;
            if (netState != null)
            {
                netState.Send(new UnicodeMessage(Server.Serial.MinusOne, -1, MessageType.Regular, hue, 3, "ENU", "System", text));
            }
        }

        public void SendMessage(string format, params object[] args)
        {
            this.SendMessage(0x3b2, string.Format(format, args));
        }

        public void SendMessage(int hue, string format, params object[] args)
        {
            this.SendMessage(hue, string.Format(format, args));
        }

        public virtual void SendPropertiesTo(Mobile from)
        {
            from.Send(this.PropertyList);
        }



        public void SendRemovePacket()
        {
            this.SendRemovePacket(true);
        }

        public void SendRemovePacket(bool everyone)
        {
            if (this.m_Map != null)
            {
                Packet p = null;
                IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                foreach (Server.Network.NetState state in clientsInRange)
                {
                    if ((state != this.m_NetState) && (everyone || !state.Mobile.CanSee(this)))
                    {
                        if (p == null)
                        {
                            p = this.RemovePacket;
                        }
                        state.Send(p);
                    }
                }
                clientsInRange.Free();
            }
        }

        public virtual void SendSkillMessage()
        {
            if (DateTime.Now >= this.m_NextActionMessage)
            {
                this.m_NextActionMessage = DateTime.Now + m_ActionMessageDelay;
                this.SendLocalizedMessage(0x7a196);
            }
        }

        public void SendSound(int soundID)
        {
            if ((soundID != -1) && (this.m_NetState != null))
            {
                this.Send(new Server.Network.PlaySound(soundID, this));
            }
        }

        public void SendSound(int soundID, IPoint3D p)
        {
            if ((soundID != -1) && (this.m_NetState != null))
            {
                this.Send(new Server.Network.PlaySound(soundID, p));
            }
        }

        public virtual void Serialize(GenericWriter writer)
        {
            writer.Write(0x1c);
            writer.WriteDeltaTime(this.m_LastStatGain);
            writer.Write(this.m_TithingPoints);
            writer.Write(this.m_Corpse);
            writer.Write(this.m_CreationTime);
            writer.WriteMobileList(this.m_Stabled, true);
            writer.Write(this.m_CantWalk);
            VirtueInfo.Serialize(writer, this.m_Virtues);
            writer.Write(this.m_Thirst);
            writer.Write(this.m_BAC);
            writer.Write(this.m_ShortTermMurders);
            writer.Write(this.m_FollowersMax);
            writer.Write(this.m_MagicDamageAbsorb);
            writer.Write(this.m_GuildFealty);
            writer.Write(this.m_Guild);
            writer.Write(this.m_DisplayGuildTitle);
            writer.Write(this.m_CanSwim);
            writer.Write(this.m_Squelched);
            writer.Write(this.m_Holding);
            writer.Write(this.m_VirtualArmor);
            writer.Write(this.m_BaseSoundID);
            writer.Write(this.m_DisarmReady);
            writer.Write(this.m_StunReady);
            writer.Write(this.m_StatCap);
            writer.Write(this.m_NameHue);
            writer.Write(this.m_Hunger);
            writer.Write(this.m_Location);
            writer.Write((int) this.m_Body);
            writer.Write(this.m_Name);
            writer.Write(this.m_GuildTitle);
            writer.Write(this.m_Criminal);
            writer.Write(this.m_Kills);
            writer.Write(this.m_SpeechHue);
            writer.Write(this.m_EmoteHue);
            writer.Write(this.m_WhisperHue);
            writer.Write(this.m_YellHue);
            writer.Write(this.m_Language);
            writer.Write(this.m_Female);
            writer.Write(this.m_Warmode);
            writer.Write(this.m_Hidden);
            writer.Write((byte) this.m_Direction);
            writer.Write(this.m_Hue);
            writer.Write(this.m_Str);
            writer.Write(this.m_Dex);
            writer.Write(this.m_Int);
            writer.Write(this.m_Hits);
            writer.Write(this.m_Stam);
            writer.Write(this.m_Mana);
            writer.Write(this.m_Map);
            writer.Write(this.m_Blessed);
            writer.Write(this.m_Fame);
            writer.Write(this.m_Karma);
            writer.Write((byte) this.m_AccessLevel);
            this.m_Skills.Serialize(writer);
            writer.Write(this.m_Items.Count);
            for (int i = 0; i < this.m_Items.Count; i++)
            {
                writer.Write((Item) this.m_Items[i]);
            }
            writer.Write(this.m_Player);
            writer.Write(this.m_Title);
            writer.Write(this.m_Profile);
            writer.Write(this.m_ProfileLocked);
            writer.Write(this.m_AutoPageNotify);
            writer.Write(this.m_LogoutLocation);
            writer.Write(this.m_LogoutMap);
            writer.Write((byte) this.m_StrLock);
            writer.Write((byte) this.m_DexLock);
            writer.Write((byte) this.m_IntLock);
            if (this.m_StuckMenuUses != null)
            {
                writer.Write(true);
                writer.Write(this.m_StuckMenuUses.Length);
                for (int j = 0; j < this.m_StuckMenuUses.Length; j++)
                {
                    writer.Write(this.m_StuckMenuUses[j]);
                }
            }
            else
            {
                writer.Write(false);
            }
        }

        public void SetDirection(Server.Direction dir)
        {
            this.m_Direction = dir;
        }

        public virtual void SetLocation(Point3D newLocation, bool isTeleport)
        {
            Point3D location = this.m_Location;
            Server.Region old = this.m_Region;
            if (location != newLocation)
            {
                this.m_Location = newLocation;
                Server.Items.BankBox box = this.FindBankNoCreate();
                if ((box != null) && box.Opened)
                {
                    box.Close();
                }
                if (this.m_NetState != null)
                {
                    this.m_NetState.ValidateAllTrades();
                }
                if (this.m_Map != null)
                {
                    this.m_Map.OnMove(location, this);
                }
                if (isTeleport && (this.m_NetState != null))
                {
                    this.m_NetState.Sequence = 0;
                    this.m_NetState.Send(new MobileUpdate(this));
                    this.ClearFastwalkStack();
                }
                Server.Map map = this.m_Map;
                if (map != null)
                {
                    Packet p = null;
                    IPooledEnumerable clientsInRange = map.GetClientsInRange(location);
                    foreach (Server.Network.NetState state in clientsInRange)
                    {
                        if ((state != this.m_NetState) && !Utility.InUpdateRange(newLocation, state.Mobile.Location))
                        {
                            if (p == null)
                            {
                                p = this.RemovePacket;
                            }
                            state.Send(p);
                        }
                    }
                    clientsInRange.Free();
                    Server.Network.NetState netState = this.m_NetState;
                    if (netState != null)
                    {
                        clientsInRange = map.GetObjectsInRange(newLocation, Core.GlobalMaxUpdateRange);
                        foreach (object obj2 in clientsInRange)
                        {
                            if (obj2 is Item)
                            {
                                Item item = (Item) obj2;
                                int updateRange = item.GetUpdateRange(this);
                                Point3D pointd2 = item.Location;
                                if ((this.CanSee(item) && !Utility.InRange(location, pointd2, updateRange)) && Utility.InRange(newLocation, pointd2, updateRange))
                                {
                                    item.SendInfoTo(netState);
                                }
                                continue;
                            }
                            if ((obj2 != this) && (obj2 is Mobile))
                            {
                                Mobile beholder = (Mobile) obj2;
                                if (Utility.InUpdateRange(newLocation, beholder.m_Location))
                                {
                                    bool flag = Utility.InUpdateRange(location, beholder.m_Location);
                                    if ((isTeleport || !flag) && ((beholder.m_NetState != null) && beholder.CanSee(this)))
                                    {
                                        beholder.m_NetState.Send(new MobileIncoming(beholder, this));
                                        if (this.IsDeadBondedPet)
                                        {
                                            beholder.m_NetState.Send(new BondedStatus(0, this.m_Serial, 1));
                                        }
                                        if (ObjectPropertyList.Enabled)
                                        {
                                            beholder.m_NetState.Send(this.OPLPacket);
                                        }
                                    }
                                    if (!flag && this.CanSee(beholder))
                                    {
                                        netState.Send(new MobileIncoming(this, beholder));
                                        if (beholder.IsDeadBondedPet)
                                        {
                                            netState.Send(new BondedStatus(0, beholder.m_Serial, 1));
                                        }
                                        if (ObjectPropertyList.Enabled)
                                        {
                                            netState.Send(beholder.OPLPacket);
                                        }
                                    }
                                }
                            }
                        }
                        clientsInRange.Free();
                    }
                    else
                    {
                        clientsInRange = map.GetClientsInRange(newLocation);
                        foreach (Server.Network.NetState state3 in clientsInRange)
                        {
                            if ((isTeleport || !Utility.InUpdateRange(location, state3.Mobile.Location)) && state3.Mobile.CanSee(this))
                            {
                                state3.Send(new MobileIncoming(state3.Mobile, this));
                                if (this.IsDeadBondedPet)
                                {
                                    state3.Send(new BondedStatus(0, this.m_Serial, 1));
                                }
                                if (ObjectPropertyList.Enabled)
                                {
                                    state3.Send(this.OPLPacket);
                                }
                            }
                        }
                        clientsInRange.Free();
                    }
                }
                this.m_Region = Server.Region.Find(this.m_Location, this.m_Map);
                if (old != this.m_Region)
                {
                    old.InternalExit(this);
                    this.m_Region.InternalEnter(this);
                    this.OnRegionChange(old, this.m_Region);
                }
                this.OnLocationChange(location);
                this.CheckLightLevels(false);
                this.m_Region.OnLocationChanged(this, location);
            }
        }

        private void StopAggrExpire()
        {
            if (this.m_ExpireAggrTimer != null)
            {
                this.m_ExpireAggrTimer.Stop();
            }
            this.m_ExpireAggrTimer = null;
        }

        public override string ToString()
        {
            return string.Format("0x{0:X} \"{1}\"", this.m_Serial.Value, this.Name);
        }

        private void UpdateAggrExpire()
        {
            if (this.m_Deleted || ((this.m_Aggressors.Count == 0) && (this.m_Aggressed.Count == 0)))
            {
                this.StopAggrExpire();
            }
            else if (this.m_ExpireAggrTimer == null)
            {
                this.m_ExpireAggrTimer = new ExpireAggressorsTimer(this);
                this.m_ExpireAggrTimer.Start();
            }
        }

        public virtual void UpdateResistances()
        {
            if (this.m_Resistances == null)
            {
                this.m_Resistances = new int[] { -2147483648, -2147483648, -2147483648, -2147483648, -2147483648 };
            }
            bool flag = false;
            for (int i = 0; i < this.m_Resistances.Length; i++)
            {
                if (this.m_Resistances[i] != -2147483648)
                {
                    this.m_Resistances[i] = -2147483648;
                    flag = true;
                }
            }
            if (flag)
            {
                this.Delta(MobileDelta.Resistances);
            }
        }

        public virtual void UpdateSkillMods()
        {
            this.ValidateSkillMods();
            for (int i = 0; i < this.m_SkillMods.Count; i++)
            {
                SkillMod mod = (SkillMod) this.m_SkillMods[i];
                Skill skill = this.m_Skills[mod.Skill];
                if (skill != null)
                {
                    skill.Update();
                }
            }
        }

        public virtual void UpdateTotals()
        {
            if (this.m_Items != null)
            {
                int totalWeight = this.m_TotalWeight;
                this.m_TotalGold = 0;
                this.m_TotalWeight = 0;
                for (int i = 0; i < this.m_Items.Count; i++)
                {
                    Item item = (Item) this.m_Items[i];
                    item.UpdateTotals();
                    if (!(item is Server.Items.BankBox))
                    {
                        this.m_TotalGold += item.TotalGold;
                        this.m_TotalWeight += item.TotalWeight + item.PileWeight;
                    }
                }
                if (this.m_Holding != null)
                {
                    this.m_TotalWeight += this.m_Holding.TotalWeight + this.m_Holding.PileWeight;
                }
                this.OnWeightChange(totalWeight);
            }
        }

        public virtual void Use(Item item)
        {
            if ((item != null) && !item.Deleted)
            {
                this.DisruptiveAction();
                if ((this.m_Spell == null) || this.m_Spell.OnCasterUsingObject(item))
                {
                    object rootParent = item.RootParent;
                    bool flag = false;
                    if (!Utility.InUpdateRange(this, item.GetWorldLocation()))
                    {
                        item.OnDoubleClickOutOfRange(this);
                    }
                    else if (!this.CanSee(item))
                    {
                        item.OnDoubleClickCantSee(this);
                    }
                    else if (!item.IsAccessibleTo(this))
                    {
                        Server.Region region = Server.Region.Find(item.GetWorldLocation(), item.Map);
                        if ((region == null) || !region.SendInaccessibleMessage(item, this))
                        {
                            item.OnDoubleClickNotAccessible(this);
                        }
                    }
                    else if (!this.CheckAlive(false))
                    {
                        item.OnDoubleClickDead(this);
                    }
                    else if (item.InSecureTrade)
                    {
                        item.OnDoubleClickSecureTrade(this);
                    }
                    else if (!this.AllowItemUse(item))
                    {
                        flag = false;
                    }
                    else if (!item.CheckItemUse(this, item))
                    {
                        flag = false;
                    }
                    else if (((rootParent != null) && (rootParent is Mobile)) && ((Mobile) rootParent).IsSnoop(this))
                    {
                        item.OnSnoop(this);
                    }
                    else if (this.m_Region.OnDoubleClick(this, item))
                    {
                        flag = true;
                    }
                    if (flag)
                    {
                        if (!item.Deleted)
                        {
                            item.OnItemUsed(this, item);
                        }
                        if (!item.Deleted)
                        {
                            item.OnDoubleClick(this);
                        }
                    }
                }
            }
        }

        public virtual void Use(Mobile m)
        {
            if ((m != null) && !m.Deleted)
            {
                this.DisruptiveAction();
                if ((this.m_Spell == null) || this.m_Spell.OnCasterUsingObject(m))
                {
                    if (!Utility.InUpdateRange(this, m))
                    {
                        m.OnDoubleClickOutOfRange(this);
                    }
                    else if (!this.CanSee(m))
                    {
                        m.OnDoubleClickCantSee(this);
                    }
                    else if (!this.CheckAlive(false))
                    {
                        m.OnDoubleClickDead(this);
                    }
                    else if (this.m_Region.OnDoubleClick(this, m) && !m.Deleted)
                    {
                        m.OnDoubleClick(this);
                    }
                }
            }
        }

        public void UsedStuckMenu()
        {
            if (this.m_StuckMenuUses == null)
            {
                this.m_StuckMenuUses = new DateTime[2];
            }
            for (int i = 0; i < this.m_StuckMenuUses.Length; i++)
            {
                if ((DateTime.Now - this.m_StuckMenuUses[i]) > TimeSpan.FromDays(1.0))
                {
                    this.m_StuckMenuUses[i] = DateTime.Now;
                    return;
                }
            }
        }

        public virtual bool UseSkill(SkillName name)
        {
            return Server.Skills.UseSkill(this, name);
        }

        public virtual bool UseSkill(int skillID)
        {
            return Server.Skills.UseSkill(this, skillID);
        }

        public virtual void ValidateSkillMods()
        {
            int num = 0;
            while (num < this.m_SkillMods.Count)
            {
                SkillMod mod = (SkillMod) this.m_SkillMods[num];
                if (mod.CheckCondition())
                {
                    num++;
                }
                else
                {
                    this.InternalRemoveSkillMod(mod);
                }
            }
        }

        public void Whisper(int number)
        {
            this.Whisper(number, "");
        }

        public void Whisper(string text)
        {
            this.PublicOverheadMessage(MessageType.Whisper, this.m_WhisperHue, false, text);
        }

        public void Whisper(int number, string args)
        {
            this.PublicOverheadMessage(MessageType.Whisper, this.m_WhisperHue, number, args);
        }

        public void Whisper(string format, params object[] args)
        {
            this.Whisper(string.Format(format, args));
        }

        public void Yell(int number)
        {
            this.Yell(number, "");
        }

        public void Yell(string text)
        {
            this.PublicOverheadMessage(MessageType.Yell, this.m_YellHue, false, text);
        }

        public void Yell(int number, string args)
        {
            this.PublicOverheadMessage(MessageType.Yell, this.m_YellHue, number, args);
        }

        public void Yell(string format, params object[] args)
        {
            this.Yell(string.Format(format, args));
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.Administrator)]
        public Server.AccessLevel AccessLevel
        {
            get
            {
                return this.m_AccessLevel;
            }
            set
            {
                Server.AccessLevel accessLevel = this.m_AccessLevel;
                if (accessLevel != value)
                {
                    this.m_AccessLevel = value;
                    this.Delta(MobileDelta.Noto);
                    this.InvalidateProperties();
                    this.SendMessage("Your access level has been changed. You are now {0}.", new object[] { GetAccessLevelName(value) });
                    this.ClearScreen();
                    this.SendEverything();
                    this.OnAccessLevelChanged(accessLevel);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.Administrator | Server.AccessLevel.Counselor)]
        public IAccount Account
        {
            get
            {
                return this.m_Account;
            }
            set
            {
                this.m_Account = value;
            }
        }

        public static TimeSpan ActionMessageDelay
        {
            get
            {
                return m_ActionMessageDelay;
            }
            set
            {
                m_ActionMessageDelay = value;
            }
        }

        public ArrayList Aggressed
        {
            get
            {
                return this.m_Aggressed;
            }
        }

        public ArrayList Aggressors
        {
            get
            {
                return this.m_Aggressors;
            }
        }

        public virtual bool Alive
        {
            get
            {
                if (this.m_Deleted)
                {
                    return false;
                }
                if (this.m_Player)
                {
                    return !this.m_Body.IsGhost;
                }
                return true;
            }
        }

        public static Server.AllowBeneficialHandler AllowBeneficialHandler
        {
            get
            {
                return m_AllowBeneficialHandler;
            }
            set
            {
                m_AllowBeneficialHandler = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int AllowedStealthSteps
        {
            get
            {
                return this.m_AllowedStealthSteps;
            }
            set
            {
                this.m_AllowedStealthSteps = value;
            }
        }

        public static Server.AllowHarmfulHandler AllowHarmfulHandler
        {
            get
            {
                return m_AllowHarmfulHandler;
            }
            set
            {
                m_AllowHarmfulHandler = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public double ArmorRating
        {
            get
            {
                double num = 0.0;
                IArmor neckArmor = this.NeckArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                neckArmor = this.HandArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                neckArmor = this.HeadArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                neckArmor = this.ArmsArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                neckArmor = this.LegsArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                neckArmor = this.ChestArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                neckArmor = this.ShieldArmor;
                if (neckArmor != null)
                {
                    num += neckArmor.ArmorRating;
                }
                return ((this.m_VirtualArmor + this.m_VirtualArmorMod) + num);
            }
        }

        public IArmor ArmsArmor
        {
            get
            {
                return (this.FindItemOnLayer(Layer.Arms) as IArmor);
            }
        }

        public static bool AsciiClickMessage
        {
            get
            {
                return m_AsciiClickMessage;
            }
            set
            {
                m_AsciiClickMessage = value;
            }
        }

        public static TimeSpan AutoManifestTimeout
        {
            get
            {
                return m_AutoManifestTimeout;
            }
            set
            {
                m_AutoManifestTimeout = value;
            }
        }

        [CommandProperty(Server.AccessLevel.Administrator)]
        public bool AutoPageNotify
        {
            get
            {
                return this.m_AutoPageNotify;
            }
            set
            {
                this.m_AutoPageNotify = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int BAC
        {
            get
            {
                return this.m_BAC;
            }
            set
            {
                this.m_BAC = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Container Backpack
        {
            get
            {
                if (((this.m_Backpack != null) && !this.m_Backpack.Deleted) && (this.m_Backpack.Parent == this))
                {
                    return this.m_Backpack;
                }
                return (this.m_Backpack = this.FindItemOnLayer(Layer.Backpack) as Container);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Server.Items.BankBox BankBox
        {
            get
            {
                if (((this.m_BankBox == null) || this.m_BankBox.Deleted) || (this.m_BankBox.Parent != this))
                {
                    this.m_BankBox = this.FindItemOnLayer(Layer.Bank) as Server.Items.BankBox;
                    if (this.m_BankBox == null)
                    {
                        this.AddItem(this.m_BankBox = new Server.Items.BankBox(this));
                    }
                }
                return this.m_BankBox;
            }
        }

        public virtual int BaseColdResistance
        {
            get
            {
                return 0;
            }
        }

        public virtual int BaseEnergyResistance
        {
            get
            {
                return 0;
            }
        }

        public virtual int BaseFireResistance
        {
            get
            {
                return 0;
            }
        }

        public virtual int BasePhysicalResistance
        {
            get
            {
                return 0;
            }
        }

        public virtual int BasePoisonResistance
        {
            get
            {
                return 0;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int BaseSoundID
        {
            get
            {
                return this.m_BaseSoundID;
            }
            set
            {
                this.m_BaseSoundID = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Blessed
        {
            get
            {
                return this.m_Blessed;
            }
            set
            {
                if (this.m_Blessed != value)
                {
                    this.m_Blessed = value;
                    this.Delta(MobileDelta.Flags);
                }
            }
        }

        [Body, CommandProperty(Server.AccessLevel.GameMaster)]
        public Server.Body Body
        {
            get
            {
                if (this.IsBodyMod)
                {
                    return this.m_BodyMod;
                }
                return this.m_Body;
            }
            set
            {
                if ((this.m_Body != value) && !this.IsBodyMod)
                {
                    this.m_Body = this.SafeBody((int) value);
                    this.Delta(MobileDelta.Body);
                    this.InvalidateProperties();
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Server.Body BodyMod
        {
            get
            {
                return this.m_BodyMod;
            }
            set
            {
                if (this.m_BodyMod != value)
                {
                    this.m_BodyMod = value;
                    this.Delta(MobileDelta.Body);
                    this.InvalidateProperties();
                }
            }
        }

        [Body, CommandProperty(Server.AccessLevel.GameMaster)]
        public int BodyValue
        {
            get
            {
                return this.Body.BodyID;
            }
            set
            {
                this.Body = value;
            }
        }

        public static int BodyWeight
        {
            get
            {
                return m_BodyWeight;
            }
            set
            {
                m_BodyWeight = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool CanHearGhosts
        {
            get
            {
                if (!this.m_CanHearGhosts)
                {
                    return (this.AccessLevel >= Server.AccessLevel.Counselor);
                }
                return true;
            }
            set
            {
                this.m_CanHearGhosts = value;
            }
        }

        public virtual bool CanRegenHits
        {
            get
            {
                return (this.Alive && !this.Poisoned);
            }
        }

        public virtual bool CanRegenMana
        {
            get
            {
                return this.Alive;
            }
        }

        public virtual bool CanRegenStam
        {
            get
            {
                return this.Alive;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool CanSwim
        {
            get
            {
                return this.m_CanSwim;
            }
            set
            {
                this.m_CanSwim = value;
            }
        }

        public virtual bool CanTarget
        {
            get
            {
                return true;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool CantWalk
        {
            get
            {
                return this.m_CantWalk;
            }
            set
            {
                this.m_CantWalk = value;
            }
        }

        public bool ChangingCombatant
        {
            get
            {
                return (this.m_ChangingCombatant > 0);
            }
        }

        public IArmor ChestArmor
        {
            get
            {
                IArmor armor = this.FindItemOnLayer(Layer.InnerTorso) as IArmor;
                if (armor == null)
                {
                    armor = this.FindItemOnLayer(Layer.Shirt) as IArmor;
                }
                return armor;
            }
        }

        public virtual bool ClickTitle
        {
            get
            {
                return true;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public virtual int ColdResistance
        {
            get
            {
                return this.GetResistance(ResistanceType.Cold);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public virtual Mobile Combatant
        {
            get
            {
                return this.m_Combatant;
            }
            set
            {
                if ((this.m_Combatant != value) && (value != this))
                {
                    Mobile combatant = this.m_Combatant;
                    this.m_ChangingCombatant++;
                    this.m_Combatant = value;
                    if (((this.m_Combatant != null) && !this.CanBeHarmful(this.m_Combatant, false)) || !this.Region.OnCombatantChange(this, combatant, this.m_Combatant))
                    {
                        this.m_Combatant = combatant;
                        this.m_ChangingCombatant--;
                    }
                    else
                    {
                        if (this.m_NetState != null)
                        {
                            this.m_NetState.Send(new ChangeCombatant(this.m_Combatant));
                        }
                        if (this.m_Combatant == null)
                        {
                            this.m_ExpireCombatant.Stop();
                            this.m_CombatTimer.Stop();
                        }
                        else
                        {
                            this.m_ExpireCombatant.Start();
                            this.m_CombatTimer.Start();
                        }
                        if ((this.m_Combatant != null) && this.CanBeHarmful(this.m_Combatant, false))
                        {
                            this.DoHarmful(this.m_Combatant);
                            if (this.m_Combatant != null)
                            {
                                this.m_Combatant.PlaySound(this.m_Combatant.GetAngerSound());
                            }
                        }
                        this.OnCombatantChange();
                        this.m_ChangingCombatant--;
                    }
                }
            }
        }

        public Server.ContextMenus.ContextMenu ContextMenu
        {
            get
            {
                return this.m_ContextMenu;
            }
            set
            {
                this.m_ContextMenu = value;
                if (this.m_ContextMenu != null)
                {
                    this.Send(new DisplayContextMenu(this.m_ContextMenu));
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Container Corpse
        {
            get
            {
                return this.m_Corpse;
            }
            set
            {
                this.m_Corpse = value;
            }
        }

        public static Server.CreateCorpseHandler CreateCorpseHandler
        {
            get
            {
                return m_CreateCorpse;
            }
            set
            {
                m_CreateCorpse = value;
            }
        }

        public DateTime CreationTime
        {
            get
            {
                return this.m_CreationTime;
            }
            set
            {
                this.m_CreationTime = value;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public bool Criminal
        {
            get
            {
                return this.m_Criminal;
            }
            set
            {
                if (this.m_Criminal != value)
                {
                    this.m_Criminal = value;
                    this.Delta(MobileDelta.Noto);
                    this.InvalidateProperties();
                }
                this.m_ExpireCriminal.Stop();
                if (this.m_Criminal)
                {
                    this.m_ExpireCriminal.Start();
                }
            }
        }

        public ArrayList DamageEntries
        {
            get
            {
                return this.m_DamageEntries;
            }
        }

        public static TimeSpan DefaultHitsRate
        {
            get
            {
                return m_DefaultHitsRate;
            }
            set
            {
                m_DefaultHitsRate = value;
            }
        }

        public static TimeSpan DefaultManaRate
        {
            get
            {
                return m_DefaultManaRate;
            }
            set
            {
                m_DefaultManaRate = value;
            }
        }

        public static TimeSpan DefaultStamRate
        {
            get
            {
                return m_DefaultStamRate;
            }
            set
            {
                m_DefaultStamRate = value;
            }
        }

        public static IWeapon DefaultWeapon
        {
            get
            {
                return m_DefaultWeapon;
            }
            set
            {
                m_DefaultWeapon = value;
            }
        }

        public bool Deleted
        {
            get
            {
                return this.m_Deleted;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Dex
        {
            get
            {
                int num = this.m_Dex + this.GetStatOffset(StatType.Dex);
                if (num < 1)
                {
                    return 1;
                }
                if (num > 0xfde8)
                {
                    num = 0xfde8;
                }
                return num;
            }
            set
            {
                if (this.m_StatMods.Count == 0)
                {
                    this.RawDex = value;
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public StatLockType DexLock
        {
            get
            {
                return this.m_DexLock;
            }
            set
            {
                if (this.m_DexLock != value)
                {
                    this.m_DexLock = value;
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.Send(new StatLockInfo(this));
                    }
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Server.Direction Direction
        {
            get
            {
                return this.m_Direction;
            }
            set
            {
                if (this.m_Direction != value)
                {
                    this.m_Direction = value;
                    this.Delta(MobileDelta.Direction);
                }
            }
        }

        public static bool DisableDismountInWarmode
        {
            get
            {
                return m_DisableDismountInWarmode;
            }
            set
            {
                m_DisableDismountInWarmode = value;
            }
        }

        public static bool DisableHiddenSelfClick
        {
            get
            {
                return m_DisableHiddenSelfClick;
            }
            set
            {
                m_DisableHiddenSelfClick = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool DisarmReady
        {
            get
            {
                return this.m_DisarmReady;
            }
            set
            {
                this.m_DisarmReady = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool DisplayGuildTitle
        {
            get
            {
                return this.m_DisplayGuildTitle;
            }
            set
            {
                this.m_DisplayGuildTitle = value;
                this.InvalidateProperties();
            }
        }

        public static bool DragEffects
        {
            get
            {
                return m_DragEffects;
            }
            set
            {
                m_DragEffects = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int EmoteHue
        {
            get
            {
                return this.m_EmoteHue;
            }
            set
            {
                this.m_EmoteHue = value;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public virtual int EnergyResistance
        {
            get
            {
                return this.GetResistance(ResistanceType.Energy);
            }
        }

        public static TimeSpan ExpireCriminalDelay
        {
            get
            {
                return m_ExpireCriminalDelay;
            }
            set
            {
                m_ExpireCriminalDelay = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Fame
        {
            get
            {
                return this.m_Fame;
            }
            set
            {
                int fame = this.m_Fame;
                if (fame != value)
                {
                    this.m_Fame = value;
                    if ((this.ShowFameTitle && (this.m_Player || this.m_Body.IsHuman)) && ((fame >= 0x2710) != (value >= 0x2710)))
                    {
                        this.InvalidateProperties();
                    }
                    this.OnFameChange(fame);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Female
        {
            get
            {
                return this.m_Female;
            }
            set
            {
                if (this.m_Female != value)
                {
                    this.m_Female = value;
                    this.Delta(MobileDelta.Flags);
                    this.OnGenderChanged(!this.m_Female);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public virtual int FireResistance
        {
            get
            {
                return this.GetResistance(ResistanceType.Fire);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Followers
        {
            get
            {
                return this.m_Followers;
            }
            set
            {
                if (this.m_Followers != value)
                {
                    this.m_Followers = value;
                    this.Delta(MobileDelta.Followers);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int FollowersMax
        {
            get
            {
                return this.m_FollowersMax;
            }
            set
            {
                if (this.m_FollowersMax != value)
                {
                    this.m_FollowersMax = value;
                    this.Delta(MobileDelta.Followers);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Frozen
        {
            get
            {
                return this.m_Frozen;
            }
            set
            {
                if (this.m_Frozen != value)
                {
                    this.m_Frozen = value;
                    if (this.m_FrozenTimer != null)
                    {
                        this.m_FrozenTimer.Stop();
                        this.m_FrozenTimer = null;
                    }
                }
            }
        }

        public static Server.AccessLevel FwdAccessOverride
        {
            get
            {
                return m_FwdAccessOverride;
            }
            set
            {
                m_FwdAccessOverride = value;
            }
        }

        public static bool FwdEnabled
        {
            get
            {
                return m_FwdEnabled;
            }
            set
            {
                m_FwdEnabled = value;
            }
        }

        public static int FwdMaxSteps
        {
            get
            {
                return m_FwdMaxSteps;
            }
            set
            {
                m_FwdMaxSteps = value;
            }
        }

        public static bool FwdUOTDOverride
        {
            get
            {
                return m_FwdUOTDOverride;
            }
            set
            {
                m_FwdUOTDOverride = value;
            }
        }

        public static char[] GhostChars
        {
            get
            {
                return m_GhostChars;
            }
            set
            {
                m_GhostChars = value;
            }
        }

        public BaseGuild Guild
        {
            get
            {
                return this.m_Guild;
            }
            set
            {
                BaseGuild oldGuild = this.m_Guild;
                if (oldGuild != value)
                {
                    if (value == null)
                    {
                        this.GuildTitle = null;
                    }
                    this.m_Guild = value;
                    this.Delta(MobileDelta.Noto);
                    this.InvalidateProperties();
                    this.OnGuildChange(oldGuild);
                }
            }
        }

        public static bool GuildClickMessage
        {
            get
            {
                return m_GuildClickMessage;
            }
            set
            {
                m_GuildClickMessage = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Mobile GuildFealty
        {
            get
            {
                return this.m_GuildFealty;
            }
            set
            {
                this.m_GuildFealty = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public string GuildTitle
        {
            get
            {
                return this.m_GuildTitle;
            }
            set
            {
                string guildTitle = this.m_GuildTitle;
                if (guildTitle != value)
                {
                    this.m_GuildTitle = value;
                    if (((this.m_Guild != null) && !this.m_Guild.Disbanded) && (this.m_GuildTitle != null))
                    {
                        this.SendLocalizedMessage(0xf88aa, true, this.m_GuildTitle);
                    }
                    this.InvalidateProperties();
                    this.OnGuildTitleChange(guildTitle);
                }
            }
        }


        public IArmor HandArmor
        {
            get
            {
                return (this.FindItemOnLayer(Layer.Gloves) as IArmor);
            }
        }

        public IArmor HeadArmor
        {
            get
            {
                return (this.FindItemOnLayer(Layer.Helm) as IArmor);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Hidden
        {
            get
            {
                return this.m_Hidden;
            }
            set
            {
                if (this.m_Hidden != value)
                {
                    this.m_AllowedStealthSteps = 0;
                    this.m_Hidden = value;
                    if (this.m_Map != null)
                    {
                        Packet p = null;
                        IPooledEnumerable clientsInRange = this.m_Map.GetClientsInRange(this.m_Location);
                        foreach (Server.Network.NetState state in clientsInRange)
                        {
                            if (!state.Mobile.CanSee(this))
                            {
                                if (p == null)
                                {
                                    p = this.RemovePacket;
                                }
                                state.Send(p);
                                continue;
                            }
                            state.Send(new MobileIncoming(state.Mobile, this));
                            if (this.IsDeadBondedPet)
                            {
                                state.Send(new BondedStatus(0, this.m_Serial, 1));
                            }
                            if (ObjectPropertyList.Enabled)
                            {
                                state.Send(this.OPLPacket);
                            }
                        }
                        clientsInRange.Free();
                    }
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Hits
        {
            get
            {
                return this.m_Hits;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                else if (value >= this.HitsMax)
                {
                    value = this.HitsMax;
                    if (this.m_HitsTimer != null)
                    {
                        this.m_HitsTimer.Stop();
                    }
                    for (int i = 0; i < this.m_Aggressors.Count; i++)
                    {
                        ((AggressorInfo) this.m_Aggressors[i]).CanReportMurder = false;
                    }
                    if (this.m_DamageEntries.Count > 0)
                    {
                        this.m_DamageEntries.Clear();
                    }
                }
                if (value < this.HitsMax)
                {
                    if (this.m_HitsTimer == null)
                    {
                        this.m_HitsTimer = new HitsTimer(this);
                    }
                    this.m_HitsTimer.Start();
                }
                if (this.m_Hits != value)
                {
                    this.m_Hits = value;
                    this.Delta(MobileDelta.Hits);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public virtual int HitsMax
        {
            get
            {
                return (50 + (this.Str / 2));
            }
        }

        public static RegenRateHandler HitsRegenRateHandler
        {
            get
            {
                return m_HitsRegenRate;
            }
            set
            {
                m_HitsRegenRate = value;
            }
        }

        public Item Holding
        {
            get
            {
                return this.m_Holding;
            }
            set
            {
                if (this.m_Holding != value)
                {
                    if (this.m_Holding != null)
                    {
                        this.TotalWeight -= this.m_Holding.TotalWeight + this.m_Holding.PileWeight;
                        if (this.m_Holding.HeldBy == this)
                        {
                            this.m_Holding.HeldBy = null;
                        }
                    }
                    if ((value != null) && (this.m_Holding != null))
                    {
                        value.ClearBounce();
                        this.DropHolding();
                    }
                    this.m_Holding = value;
                    if (this.m_Holding != null)
                    {
                        this.TotalWeight += this.m_Holding.TotalWeight + this.m_Holding.PileWeight;
                        if (this.m_Holding.HeldBy == null)
                        {
                            this.m_Holding.HeldBy = this;
                        }
                    }
                }
            }
        }

        [Hue, CommandProperty(Server.AccessLevel.GameMaster)]
        public virtual int Hue
        {
            get
            {
                if (this.m_HueMod != -1)
                {
                    return this.m_HueMod;
                }
                return this.m_Hue;
            }
            set
            {
                if (this.m_Hue != value)
                {
                    this.m_Hue = value;
                    this.Delta(MobileDelta.Hue);
                }
            }
        }

        public virtual int HuedItemID
        {
            get
            {
                if (!this.m_Female)
                {
                    return 0x2106;
                }
                return 0x2107;
            }
        }

        [Hue, CommandProperty(Server.AccessLevel.GameMaster)]
        public int HueMod
        {
            get
            {
                return this.m_HueMod;
            }
            set
            {
                if (this.m_HueMod != value)
                {
                    this.m_HueMod = value;
                    this.Delta(MobileDelta.Hue);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Hunger
        {
            get
            {
                return this.m_Hunger;
            }
            set
            {
                int hunger = this.m_Hunger;
                if (hunger != value)
                {
                    this.m_Hunger = value;
                    EventSink.InvokeHungerChanged(new HungerChangedEventArgs(this, hunger));
                }
            }
        }

        public static bool InsuranceEnabled
        {
            get
            {
                return m_InsuranceEnabled;
            }
            set
            {
                m_InsuranceEnabled = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Int
        {
            get
            {
                int num = this.m_Int + this.GetStatOffset(StatType.Int);
                if (num < 1)
                {
                    return 1;
                }
                if (num > 0xfde8)
                {
                    num = 0xfde8;
                }
                return num;
            }
            set
            {
                if (this.m_StatMods.Count == 0)
                {
                    this.RawInt = value;
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public StatLockType IntLock
        {
            get
            {
                return this.m_IntLock;
            }
            set
            {
                if (this.m_IntLock != value)
                {
                    this.m_IntLock = value;
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.Send(new StatLockInfo(this));
                    }
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool IsBodyMod
        {
            get
            {
                return (this.m_BodyMod.BodyID != 0);
            }
        }

        public virtual bool IsDeadBondedPet
        {
            get
            {
                return false;
            }
        }

        public ArrayList Items
        {
            get
            {
                return this.m_Items;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Karma
        {
            get
            {
                return this.m_Karma;
            }
            set
            {
                int karma = this.m_Karma;
                if (karma != value)
                {
                    this.m_Karma = value;
                    this.OnKarmaChange(karma);
                }
            }
        }

        public virtual bool KeepsItemsOnDeath
        {
            get
            {
                return (this.m_AccessLevel > Server.AccessLevel.Player);
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public int Kills
        {
            get
            {
                return this.m_Kills;
            }
            set
            {
                int kills = this.m_Kills;
                if (this.m_Kills != value)
                {
                    this.m_Kills = value;
                    if (this.m_Kills < 0)
                    {
                        this.m_Kills = 0;
                    }
                    if ((kills >= 5) != (this.m_Kills >= 5))
                    {
                        this.Delta(MobileDelta.Noto);
                        this.InvalidateProperties();
                    }
                    this.OnKillsChange(kills);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public string Language
        {
            get
            {
                return this.m_Language;
            }
            set
            {
                this.m_Language = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Mobile LastKiller
        {
            get
            {
                return this.m_LastKiller;
            }
            set
            {
                this.m_LastKiller = value;
            }
        }

        public DateTime LastMoveTime
        {
            get
            {
                return this.m_LastMoveTime;
            }
            set
            {
                this.m_LastMoveTime = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public DateTime LastStatGain
        {
            get
            {
                return this.m_LastStatGain;
            }
            set
            {
                this.m_LastStatGain = value;
            }
        }

        public IArmor LegsArmor
        {
            get
            {
                IArmor armor = this.FindItemOnLayer(Layer.InnerLegs) as IArmor;
                if (armor == null)
                {
                    armor = this.FindItemOnLayer(Layer.Pants) as IArmor;
                }
                return armor;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int LightLevel
        {
            get
            {
                return this.m_LightLevel;
            }
            set
            {
                if (this.m_LightLevel != value)
                {
                    this.m_LightLevel = value;
                    this.CheckLightLevels(false);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public Point3D Location
        {
            get
            {
                return this.m_Location;
            }
            set
            {
                this.SetLocation(value, true);
            }
        }

        public Point3D LogoutLocation
        {
            get
            {
                return this.m_LogoutLocation;
            }
            set
            {
                this.m_LogoutLocation = value;
            }
        }

        public Server.Map LogoutMap
        {
            get
            {
                return this.m_LogoutMap;
            }
            set
            {
                this.m_LogoutMap = value;
            }
        }

        public virtual int Luck
        {
            get
            {
                return 0;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int MagicDamageAbsorb
        {
            get
            {
                return this.m_MagicDamageAbsorb;
            }
            set
            {
                this.m_MagicDamageAbsorb = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Mana
        {
            get
            {
                return this.m_Mana;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                else if (value >= this.ManaMax)
                {
                    value = this.ManaMax;
                    if (this.m_ManaTimer != null)
                    {
                        this.m_ManaTimer.Stop();
                    }
                    if (this.Meditating)
                    {
                        this.Meditating = false;
                        this.SendLocalizedMessage(0x7a856);
                    }
                }
                if (value < this.ManaMax)
                {
                    if (this.m_ManaTimer == null)
                    {
                        this.m_ManaTimer = new ManaTimer(this);
                    }
                    this.m_ManaTimer.Start();
                }
                if (this.m_Mana != value)
                {
                    this.m_Mana = value;
                    this.Delta(MobileDelta.Mana);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public virtual int ManaMax
        {
            get
            {
                return this.Int;
            }
        }

        public static RegenRateHandler ManaRegenRateHandler
        {
            get
            {
                return m_ManaRegenRate;
            }
            set
            {
                m_ManaRegenRate = value;
            }
        }

        /*[CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public Server.Map Map
        {
            get
            {
                return this.m_Map;
            }
            set
            {
                if (this.m_Map != value)
                {
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.ValidateAllTrades();
                    }
                    Server.Map oldMap = this.m_Map;
                    if (this.m_Map != null)
                    {
                        this.m_Map.OnLeave(this);
                        this.ClearScreen();
                        this.SendRemovePacket();
                    }
                    for (int i = 0; i < this.m_Items.Count; i++)
                    {
                        ((Item) this.m_Items[i]).Map = value;
                    }
                    this.m_Map = value;
                    if (this.m_Map != null)
                    {
                        this.m_Map.OnEnter(this);
                    }
                    this.m_Region.InternalExit(this);
                    if (this.m_Map != null)
                    {
                        Server.Region old = this.m_Region;
                        this.m_Region = Server.Region.Find(this.m_Location, this.m_Map);
                        this.OnRegionChange(old, this.m_Region);
                        this.m_Region.InternalEnter(this);
                        Server.Network.NetState netState = this.m_NetState;
                        if ((netState != null) && (this.m_Map != null))
                        {
                            netState.Sequence = 0;
                            netState.Send(new MapChange(this));
                            netState.Send(new MapPatches());
                            netState.Send(SeasonChange.Instantiate(this.GetSeason(), true));
                            netState.Send(new MobileUpdate(this));
                            this.ClearFastwalkStack();
                        }
                    }
                    if (this.m_NetState != null)
                    {
                        if (this.m_Map != null)
                        {
                            this.Send(new ServerChange(this, this.m_Map));
                        }
                        if (this.m_NetState != null)
                        {
                            this.m_NetState.Sequence = 0;
                            this.ClearFastwalkStack();
                        }
                        this.Send(new MobileIncoming(this, this));
                        this.Send(new MobileUpdate(this));
                        this.CheckLightLevels(true);
                        this.Send(new MobileUpdate(this));
                    }
                    this.SendEverything();
                    this.SendIncomingPacket();
                    if (this.m_NetState != null)
                    {
                        if (this.m_NetState != null)
                        {
                            this.m_NetState.Sequence = 0;
                            this.ClearFastwalkStack();
                        }
                        this.Send(new MobileIncoming(this, this));
                        this.Send(SupportedFeatures.Instantiate());
                        this.Send(new MobileUpdate(this));
                        this.Send(new MobileAttributes(this));
                    }
                    this.OnMapChange(oldMap);
                }
            }
        }*/

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public Map Map
        {
            get
            {
                return m_Map;
            }
            set
            {
                if (m_Deleted)
                    return;

                if (m_Map != value)
                {
                    if (m_NetState != null)
                        m_NetState.ValidateAllTrades();

                    Map oldMap = m_Map;

                    if (m_Map != null)
                    {
                        m_Map.OnLeave(this);

                        ClearScreen();
                        SendRemovePacket();
                    }

                    for (int i = 0; i < m_Items.Count; ++i)
                        (m_Items[i] as Item).Map = value;

                    m_Map = value;

                    UpdateRegion();

                    if (m_Map != null)
                        m_Map.OnEnter(this);

                    NetState ns = m_NetState;

                    if (ns != null && m_Map != null)
                    {
                        ns.Sequence = 0;
                        ns.Send(new MapChange(this));
                        ns.Send(new MapPatches());
                        ns.Send(SeasonChange.Instantiate(GetSeason(), true));

                        if (ns.StygianAbyss)
                            ns.Send(new MobileUpdate(this));
                        else
                            ns.Send(new MobileUpdateOld(this));

                        ClearFastwalkStack();
                    }

                    if (ns != null)
                    {
                        if (m_Map != null)
                            Send(new ServerChange(this, m_Map));

                        ns.Sequence = 0;
                        ClearFastwalkStack();

                        if (ns.StygianAbyss)
                        {
                            Send(new MobileIncoming(this, this));
                            Send(new MobileUpdate(this));
                            CheckLightLevels(true);
                            Send(new MobileUpdate(this));
                        }
                        else
                        {
                            Send(new MobileIncomingOld(this, this));
                            Send(new MobileUpdateOld(this));
                            CheckLightLevels(true);
                            Send(new MobileUpdateOld(this));
                        }
                    }

                    SendEverything();
                    SendIncomingPacket();

                    if (ns != null)
                    {
                        ns.Sequence = 0;
                        ClearFastwalkStack();

                        if (ns.StygianAbyss)
                        {
                            Send(new MobileIncoming(this, this));
                            Send(SupportedFeatures.Instantiate(ns));
                            Send(new MobileUpdate(this));
                            Send(new MobileAttributes(this));
                        }
                        else
                        {
                            Send(new MobileIncomingOld(this, this));
                            Send(SupportedFeatures.Instantiate(ns));
                            Send(new MobileUpdateOld(this));
                            Send(new MobileAttributes(this));
                        }
                    }

                    OnMapChange(oldMap);
                }
            }
        }


        public static int MaxPlayerResistance
        {
            get
            {
                return m_MaxPlayerResistance;
            }
            set
            {
                m_MaxPlayerResistance = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Meditating
        {
            get
            {
                return this.m_Meditating;
            }
            set
            {
                this.m_Meditating = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int MeleeDamageAbsorb
        {
            get
            {
                return this.m_MeleeDamageAbsorb;
            }
            set
            {
                this.m_MeleeDamageAbsorb = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public IMount Mount
        {
            get
            {
                IMountItem mountItem = null;
                if (((this.m_MountItem != null) && !this.m_MountItem.Deleted) && (this.m_MountItem.Parent == this))
                {
                    mountItem = (IMountItem) this.m_MountItem;
                }
                if (mountItem == null)
                {
                    this.m_MountItem = (mountItem = this.FindItemOnLayer(Layer.Mount) as IMountItem) as Item;
                }
                if (mountItem != null)
                {
                    return mountItem.Mount;
                }
                return null;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Mounted
        {
            get
            {
                return (this.Mount != null);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public string Name
        {
            get
            {
                if (this.m_NameMod != null)
                {
                    return this.m_NameMod;
                }
                return this.m_Name;
            }
            set
            {
                if (this.m_Name != value)
                {
                    this.m_Name = value;
                    this.Delta(MobileDelta.Name);
                    this.InvalidateProperties();
                }
            }
        }

        public virtual string GetKnownName(Mobile mobile)
        {
            return Name;
        }

        public virtual string GetKnownNameFixed(Mobile mobile)
        {
            var name = GetKnownName(mobile);
            if (name == null) return "";
            if (name.Length > 30)
            {
                var composedParts = name.Split(' ');
                name = composedParts[0];
            }
            if (name.Length > 30)
            {
                name = name.Remove(29, name.Length - 29);
            }
            return name;
        }

        public virtual string RealName
        {
            get
            {
                return this.m_RealName;
            }
            set
            {
                if (this.m_RealName != value)
                {
                    this.m_RealName = value;
                    this.Delta(MobileDelta.Name);
                    this.InvalidateProperties();
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int NameHue
        {
            get
            {
                return this.m_NameHue;
            }
            set
            {
                this.m_NameHue = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public string NameMod
        {
            get
            {
                return this.m_NameMod;
            }
            set
            {
                if (this.m_NameMod != value)
                {
                    this.m_NameMod = value;
                    this.Delta(MobileDelta.Name);
                    this.InvalidateProperties();
                }
            }
        }

        public IArmor NeckArmor
        {
            get
            {
                return (this.FindItemOnLayer(Layer.Neck) as IArmor);
            }
        }

        public Server.Network.NetState NetState
        {
            get
            {
                if ((this.m_NetState != null) && (this.m_NetState.Socket == null))
                {
                    this.NetState = null;
                }
                return this.m_NetState;
            }
            set
            {
                if (this.m_NetState != value)
                {
                    if (this.m_Map != null)
                    {
                        this.m_Map.OnClientChange(this.m_NetState, value, this);
                    }
                    if (this.m_Target != null)
                    {
                        this.m_Target.Cancel(this, TargetCancelType.Disconnected);
                    }
                    if (this.m_QuestArrow != null)
                    {
                        this.QuestArrow = null;
                    }
                    if (this.m_Spell != null)
                    {
                        this.m_Spell.OnConnectionChanged();
                    }
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.CancelAllTrades();
                    }
                    Server.Items.BankBox box = this.FindBankNoCreate();
                    if ((box != null) && box.Opened)
                    {
                        box.Close();
                    }
                    this.m_NetState = value;
                    if (this.m_NetState == null)
                    {
                        this.OnDisconnected();
                        EventSink.InvokeDisconnected(new DisconnectedEventArgs(this));
                        this.m_LogoutTimer.Stop();
                        this.m_LogoutTimer.Delay = this.GetLogoutDelay();
                        this.m_LogoutTimer.Start();
                    }
                    else
                    {
                        this.OnConnected();
                        EventSink.InvokeConnected(new ConnectedEventArgs(this));
                        this.m_LogoutTimer.Stop();
                        if ((this.m_Map == Server.Map.Internal) && (this.m_LogoutMap != null))
                        {
                            this.Map = this.m_LogoutMap;
                            this.Location = this.m_LogoutLocation;
                        }
                    }
                    for (int i = this.m_Items.Count - 1; i >= 0; i--)
                    {
                        if (i < this.m_Items.Count)
                        {
                            Item item = (Item) this.m_Items[i];
                            if (item is SecureTradeContainer)
                            {
                                for (int j = item.Items.Count - 1; j >= 0; j--)
                                {
                                    if (j < item.Items.Count)
                                    {
                                        ((Item) item.Items[j]).OnSecureTrade(this, this, this, false);
                                        this.AddToBackpack((Item) item.Items[j]);
                                    }
                                }
                                item.Delete();
                            }
                        }
                    }
                    this.DropHolding();
                    this.OnNetStateChanged();
                }
            }
        }

        public DateTime NextActionMessage
        {
            get
            {
                return this.m_NextActionMessage;
            }
            set
            {
                this.m_NextActionMessage = value;
            }
        }

        public DateTime NextActionTime
        {
            get
            {
                return this.m_NextActionTime;
            }
            set
            {
                this.m_NextActionTime = value;
            }
        }

        public DateTime NextCombatTime
        {
            get
            {
                return this.m_NextCombatTime;
            }
            set
            {
                this.m_NextCombatTime = value;
            }
        }

        public DateTime NextSkillTime
        {
            get
            {
                return this.m_NextSkillTime;
            }
            set
            {
                this.m_NextSkillTime = value;
            }
        }

        public DateTime NextSpellTime
        {
            get
            {
                return this.m_NextSpellTime;
            }
            set
            {
                this.m_NextSpellTime = value;
            }
        }

        public static bool NoSpeechLOS
        {
            get
            {
                return m_NoSpeechLOS;
            }
            set
            {
                m_NoSpeechLOS = value;
            }
        }

        public Packet OPLPacket
        {
            get
            {
                if (this.m_OPLPacket == null)
                {
                    this.m_OPLPacket = new OPLInfo(this.PropertyList);
                }
                return this.m_OPLPacket;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Paralyzed
        {
            get
            {
                return this.m_Paralyzed;
            }
            set
            {
                if (this.m_Paralyzed != value)
                {
                    this.m_Paralyzed = value;
                    this.SendLocalizedMessage(this.m_Paralyzed ? 0x7aa6d : 0x7aa6e);
                    if (this.m_ParaTimer != null)
                    {
                        this.m_ParaTimer.Stop();
                        this.m_ParaTimer = null;
                    }
                }
            }
        }

        public object Party
        {
            get
            {
                return this.m_Party;
            }
            set
            {
                this.m_Party = value;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public virtual int PhysicalResistance
        {
            get
            {
                return this.GetResistance(ResistanceType.Physical);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster, Server.AccessLevel.Administrator)]
        public bool Player
        {
            get
            {
                return this.m_Player;
            }
            set
            {
                this.m_Player = value;
                this.InvalidateProperties();
                if (!this.m_Player && (this.m_CombatTimer != null))
                {
                    this.m_CombatTimer.Priority = TimerPriority.FiftyMS;
                }
                else if (this.m_CombatTimer != null)
                {
                    this.m_CombatTimer.Priority = TimerPriority.EveryTick;
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public Server.Poison Poison
        {
            get
            {
                return this.m_Poison;
            }
            set
            {
                this.m_Poison = value;
                this.Delta(MobileDelta.Flags);
                if (this.m_PoisonTimer != null)
                {
                    this.m_PoisonTimer.Stop();
                    this.m_PoisonTimer = null;
                }
                if (this.m_Poison != null)
                {
                    this.m_PoisonTimer = this.m_Poison.ConstructTimer(this);
                    if (this.m_PoisonTimer != null)
                    {
                        this.m_PoisonTimer.Start();
                    }
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Poisoned
        {
            get
            {
                return (this.m_Poison != null);
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public virtual int PoisonResistance
        {
            get
            {
                return this.GetResistance(ResistanceType.Poison);
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public string Profile
        {
            get
            {
                return this.m_Profile;
            }
            set
            {
                this.m_Profile = value;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public bool ProfileLocked
        {
            get
            {
                return this.m_ProfileLocked;
            }
            set
            {
                this.m_ProfileLocked = value;
            }
        }

        public Server.Prompts.Prompt Prompt
        {
            get
            {
                return this.m_Prompt;
            }
            set
            {
                Server.Prompts.Prompt prompt = this.m_Prompt;
                Server.Prompts.Prompt prompt2 = value;
                if (prompt != prompt2)
                {
                    this.m_Prompt = null;
                    if ((prompt != null) && (prompt2 != null))
                    {
                        prompt.OnCancel(this);
                    }
                    this.m_Prompt = prompt2;
                    if (prompt2 != null)
                    {
                        this.Send(new UnicodePrompt(prompt2));
                    }
                }
            }
        }

        public ObjectPropertyList PropertyList
        {
            get
            {
                if (this.m_PropertyList == null)
                {
                    this.m_PropertyList = new ObjectPropertyList(this);
                    this.GetProperties(this.m_PropertyList);
                    this.m_PropertyList.Terminate();
                }
                return this.m_PropertyList;
            }
        }

        public bool Pushing
        {
            get
            {
                return this.m_Pushing;
            }
            set
            {
                this.m_Pushing = value;
            }
        }

        public Server.QuestArrow QuestArrow
        {
            get
            {
                return this.m_QuestArrow;
            }
            set
            {
                if (this.m_QuestArrow != value)
                {
                    if (this.m_QuestArrow != null)
                    {
                        this.m_QuestArrow.Stop();
                    }
                    this.m_QuestArrow = value;
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int RawDex
        {
            get
            {
                return this.m_Dex;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }
                else if (value > 0xfde8)
                {
                    value = 0xfde8;
                }
                if (this.m_Dex != value)
                {
                    int dex = this.m_Dex;
                    this.m_Dex = value;
                    this.Delta(MobileDelta.Stat | MobileDelta.Stam);
                    if (this.Stam < this.StamMax)
                    {
                        if (this.m_StamTimer == null)
                        {
                            this.m_StamTimer = new StamTimer(this);
                        }
                        this.m_StamTimer.Start();
                    }
                    else if (this.Stam > this.StamMax)
                    {
                        this.Stam = this.StamMax;
                    }
                    this.OnRawDexChange(dex);
                    this.OnRawStatChange(StatType.Dex, dex);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int RawInt
        {
            get
            {
                return this.m_Int;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }
                else if (value > 0xfde8)
                {
                    value = 0xfde8;
                }
                if (this.m_Int != value)
                {
                    int @int = this.m_Int;
                    this.m_Int = value;
                    this.Delta(MobileDelta.Stat | MobileDelta.Mana);
                    if (this.Mana < this.ManaMax)
                    {
                        if (this.m_ManaTimer == null)
                        {
                            this.m_ManaTimer = new ManaTimer(this);
                        }
                        this.m_ManaTimer.Start();
                    }
                    else if (this.Mana > this.ManaMax)
                    {
                        this.Mana = this.ManaMax;
                    }
                    this.OnRawIntChange(@int);
                    this.OnRawStatChange(StatType.Int, @int);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public string RawName
        {
            get
            {
                return this.m_Name;
            }
            set
            {
                this.Name = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int RawStatTotal
        {
            get
            {
                return ((this.RawStr + this.RawDex) + this.RawInt);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int RawStr
        {
            get
            {
                return this.m_Str;
            }
            set
            {
                if (value < 1)
                {
                    value = 1;
                }
                else if (value > 0xfde8)
                {
                    value = 0xfde8;
                }
                if (this.m_Str != value)
                {
                    int str = this.m_Str;
                    this.m_Str = value;
                    this.Delta(MobileDelta.Stat | MobileDelta.Hits);
                    if (this.Hits < this.HitsMax)
                    {
                        if (this.m_HitsTimer == null)
                        {
                            this.m_HitsTimer = new HitsTimer(this);
                        }
                        this.m_HitsTimer.Start();
                    }
                    else if (this.Hits > this.HitsMax)
                    {
                        this.Hits = this.HitsMax;
                    }
                    this.OnRawStrChange(str);
                    this.OnRawStatChange(StatType.Str, str);
                }
            }
        }

        public Server.Region Region
        {
            get
            {
                return this.m_Region;
            }
        }

        public Packet RemovePacket
        {
            get
            {
                if (this.m_RemovePacket == null)
                {
                    this.m_RemovePacket = new RemoveMobile(this);
                }
                return this.m_RemovePacket;
            }
        }

        public ArrayList ResistanceMods
        {
            get
            {
                return this.m_ResistMods;
            }
            set
            {
                this.m_ResistMods = value;
            }
        }

        public int[] Resistances
        {
            get
            {
                return this.m_Resistances;
            }
        }

        public virtual bool RetainPackLocsOnDeath
        {
            get
            {
                return Core.AOS;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public Server.Serial Serial
        {
            get
            {
                return this.m_Serial;
            }
        }

        Point3D IEntity.Location
        {
            get
            {
                return this.m_Location;
            }
        }

        public IArmor ShieldArmor
        {
            get
            {
                return (this.FindItemOnLayer(Layer.TwoHanded) as IArmor);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int ShortTermMurders
        {
            get
            {
                return this.m_ShortTermMurders;
            }
            set
            {
                if (this.m_ShortTermMurders != value)
                {
                    this.m_ShortTermMurders = value;
                    if (this.m_ShortTermMurders < 0)
                    {
                        this.m_ShortTermMurders = 0;
                    }
                }
            }
        }

        public virtual bool ShouldCheckStatTimers
        {
            get
            {
                return true;
            }
        }

        public virtual bool ShowFameTitle
        {
            get
            {
                return true;
            }
        }

        public static Server.SkillCheckDirectLocationHandler SkillCheckDirectLocationHandler
        {
            get
            {
                return m_SkillCheckDirectLocationHandler;
            }
            set
            {
                m_SkillCheckDirectLocationHandler = value;
            }
        }

        public static Server.SkillCheckDirectTargetHandler SkillCheckDirectTargetHandler
        {
            get
            {
                return m_SkillCheckDirectTargetHandler;
            }
            set
            {
                m_SkillCheckDirectTargetHandler = value;
            }
        }

        public static Server.SkillCheckLocationHandler SkillCheckLocationHandler
        {
            get
            {
                return m_SkillCheckLocationHandler;
            }
            set
            {
                m_SkillCheckLocationHandler = value;
            }
        }

        public static Server.SkillCheckTargetHandler SkillCheckTargetHandler
        {
            get
            {
                return m_SkillCheckTargetHandler;
            }
            set
            {
                m_SkillCheckTargetHandler = value;
            }
        }

        public ArrayList SkillMods
        {
            get
            {
                return this.m_SkillMods;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor)]
        public Server.Skills Skills
        {
            get
            {
                return this.m_Skills;
            }
            set
            {
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int SkillsCap
        {
            get
            {
                if (this.m_Skills != null)
                {
                    return this.m_Skills.Cap;
                }
                return 0;
            }
            set
            {
                if (this.m_Skills != null)
                {
                    this.m_Skills.Cap = value;
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int SkillsTotal
        {
            get
            {
                if (this.m_Skills != null)
                {
                    return this.m_Skills.Total;
                }
                return 0;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int SolidHueOverride
        {
            get
            {
                return this.m_SolidHueOverride;
            }
            set
            {
                if (this.m_SolidHueOverride != value)
                {
                    this.m_SolidHueOverride = value;
                    this.Delta(MobileDelta.Body | MobileDelta.Hue);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int SpeechHue
        {
            get
            {
                return this.m_SpeechHue;
            }
            set
            {
                this.m_SpeechHue = value;
            }
        }

        public ISpell Spell
        {
            get
            {
                return this.m_Spell;
            }
            set
            {
                if ((this.m_Spell != null) && (value != null))
                {
                    Console.WriteLine("Warning: Spell has been overwritten");
                }
                this.m_Spell = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Squelched
        {
            get
            {
                return this.m_Squelched;
            }
            set
            {
                this.m_Squelched = value;
            }
        }

        public ArrayList Stabled
        {
            get
            {
                return this.m_Stabled;
            }
            set
            {
                this.m_Stabled = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Stam
        {
            get
            {
                return this.m_Stam;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                else if (value >= this.StamMax)
                {
                    value = this.StamMax;
                    if (this.m_StamTimer != null)
                    {
                        this.m_StamTimer.Stop();
                    }
                }
                if (value < this.StamMax)
                {
                    if (this.m_StamTimer == null)
                    {
                        this.m_StamTimer = new StamTimer(this);
                    }
                    this.m_StamTimer.Start();
                }
                if (this.m_Stam != value)
                {
                    this.m_Stam = value;
                    this.Delta(MobileDelta.Stam);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public virtual int StamMax
        {
            get
            {
                return this.Dex;
            }
        }

        public static RegenRateHandler StamRegenRateHandler
        {
            get
            {
                return m_StamRegenRate;
            }
            set
            {
                m_StamRegenRate = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int StatCap
        {
            get
            {
                return this.m_StatCap;
            }
            set
            {
                if (this.m_StatCap != value)
                {
                    this.m_StatCap = value;
                    this.Delta(MobileDelta.StatCap);
                }
            }
        }

        public ArrayList StatMods
        {
            get
            {
                return this.m_StatMods;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Str
        {
            get
            {
                int num = this.m_Str + this.GetStatOffset(StatType.Str);
                if (num < 1)
                {
                    return 1;
                }
                if (num > 0xfde8)
                {
                    num = 0xfde8;
                }
                return num;
            }
            set
            {
                if (this.m_StatMods.Count == 0)
                {
                    this.RawStr = value;
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public StatLockType StrLock
        {
            get
            {
                return this.m_StrLock;
            }
            set
            {
                if (this.m_StrLock != value)
                {
                    this.m_StrLock = value;
                    if (this.m_NetState != null)
                    {
                        this.m_NetState.Send(new StatLockInfo(this));
                    }
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool StunReady
        {
            get
            {
                return this.m_StunReady;
            }
            set
            {
                this.m_StunReady = value;
            }
        }

        public Server.Targeting.Target Target
        {
            get
            {
                return this.m_Target;
            }
            set
            {
                Server.Targeting.Target target = this.m_Target;
                Server.Targeting.Target target2 = value;
                if (target != target2)
                {
                    this.m_Target = null;
                    if ((target != null) && (target2 != null))
                    {
                        target.Cancel(this, TargetCancelType.Overriden);
                    }
                    this.m_Target = target2;
                    if (((target2 != null) && (this.m_NetState != null)) && !this.m_TargetLocked)
                    {
                        this.m_NetState.Send(target2.GetPacket());
                    }
                    this.OnTargetChange();
                }
            }
        }

        public bool TargetLocked
        {
            get
            {
                return this.m_TargetLocked;
            }
            set
            {
                this.m_TargetLocked = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int Thirst
        {
            get
            {
                return this.m_Thirst;
            }
            set
            {
                this.m_Thirst = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int TithingPoints
        {
            get
            {
                return this.m_TithingPoints;
            }
            set
            {
                if (this.m_TithingPoints != value)
                {
                    this.m_TithingPoints = value;
                    this.Delta(MobileDelta.TithingPoints);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public string Title
        {
            get
            {
                return this.m_Title;
            }
            set
            {
                this.m_Title = value;
                this.InvalidateProperties();
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int TotalGold
        {
            get
            {
                return this.m_TotalGold;
            }
            set
            {
                if (this.m_TotalGold != value)
                {
                    this.m_TotalGold = value;
                    this.Delta(MobileDelta.Gold);
                }
            }
        }

        public virtual int MaxWeight { get { return int.MaxValue; } }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int TotalWeight
        {
            get
            {
                return this.m_TotalWeight;
            }
            set
            {
                int totalWeight = this.m_TotalWeight;
                if (totalWeight != value)
                {
                    this.m_TotalWeight = value;
                    this.Delta(MobileDelta.Weight);
                    this.OnWeightChange(totalWeight);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int VirtualArmor
        {
            get
            {
                return this.m_VirtualArmor;
            }
            set
            {
                if (this.m_VirtualArmor != value)
                {
                    this.m_VirtualArmor = value;
                    this.Delta(MobileDelta.Armor);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int VirtualArmorMod
        {
            get
            {
                return this.m_VirtualArmorMod;
            }
            set
            {
                if (this.m_VirtualArmorMod != value)
                {
                    this.m_VirtualArmorMod = value;
                    this.Delta(MobileDelta.Armor);
                }
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public VirtueInfo Virtues
        {
            get
            {
                return this.m_Virtues;
            }
            set
            {
            }
        }

        public static Server.VisibleDamageType VisibleDamageType
        {
            get
            {
                return m_VisibleDamageType;
            }
            set
            {
                m_VisibleDamageType = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool Warmode
        {
            get
            {
                return this.m_Warmode;
            }
            set
            {
                if (this.m_Warmode != value)
                {
                    if (this.m_AutoManifestTimer != null)
                    {
                        this.m_AutoManifestTimer.Stop();
                        this.m_AutoManifestTimer = null;
                    }
                    this.m_Warmode = value;
                    this.Delta(MobileDelta.Flags);
                    if (this.m_NetState != null)
                    {
                        this.Send(SetWarMode.Instantiate(value));
                    }
                    if (!this.m_Warmode)
                    {
                        this.Combatant = null;
                    }
                    if (!this.Alive)
                    {
                        if (value)
                        {
                            this.Delta(MobileDelta.GhostUpdate);
                        }
                        else
                        {
                            this.SendRemovePacket(false);
                        }
                    }
                }
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public virtual IWeapon Weapon
        {
            get
            {
                Item weapon = this.m_Weapon as Item;
                if (((weapon != null) && !weapon.Deleted) && ((weapon.Parent == this) && this.CanSee(weapon)))
                {
                    return this.m_Weapon;
                }
                this.m_Weapon = null;
                weapon = this.FindItemOnLayer(Layer.FirstValid);
                if (weapon == null)
                {
                    weapon = this.FindItemOnLayer(Layer.TwoHanded);
                }
                if (weapon is IWeapon)
                {
                    return (this.m_Weapon = (IWeapon) weapon);
                }
                return this.GetDefaultWeapon();
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int WhisperHue
        {
            get
            {
                return this.m_WhisperHue;
            }
            set
            {
                this.m_WhisperHue = value;
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public int X
        {
            get
            {
                return this.m_Location.m_X;
            }
            set
            {
                this.Location = new Point3D(value, this.m_Location.m_Y, this.m_Location.m_Z);
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public int Y
        {
            get
            {
                return this.m_Location.m_Y;
            }
            set
            {
                this.Location = new Point3D(this.m_Location.m_X, value, this.m_Location.m_Z);
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public int YellHue
        {
            get
            {
                return this.m_YellHue;
            }
            set
            {
                this.m_YellHue = value;
            }
        }

        [CommandProperty(Server.AccessLevel.GameMaster)]
        public bool YellowHealthbar
        {
            get
            {
                return this.m_YellowHealthbar;
            }
            set
            {
                this.m_YellowHealthbar = value;
                this.Delta(MobileDelta.Flags);
            }
        }

        [CommandProperty(Server.AccessLevel.Counselor, Server.AccessLevel.GameMaster)]
        public int Z
        {
            get
            {
                return this.m_Location.m_Z;
            }
            set
            {
                this.Location = new Point3D(this.m_Location.m_X, this.m_Location.m_Y, value);
            }
        }

        private class AutoManifestTimer : Timer
        {
            private Mobile m_Mobile;

            public AutoManifestTimer(Mobile m, TimeSpan delay) : base(delay)
            {
                this.m_Mobile = m;
            }

            protected override void OnTick()
            {
                if (!this.m_Mobile.Alive)
                {
                    this.m_Mobile.Warmode = false;
                }
            }
        }

        private class CombatTimer : Timer
        {
            private Mobile m_Mobile;

            public CombatTimer(Mobile m) : base(TimeSpan.FromSeconds(0.0), TimeSpan.FromSeconds(0.01), 0)
            {
                this.m_Mobile = m;
                if (!this.m_Mobile.m_Player && (this.m_Mobile.m_Dex <= 100))
                {
                    base.Priority = TimerPriority.FiftyMS;
                }
            }

            protected override void OnTick()
            {
                if (DateTime.Now > this.m_Mobile.m_NextCombatTime)
                {
                    Mobile combatant = this.m_Mobile.Combatant;
                    if ((((combatant == null) || combatant.m_Deleted) || (this.m_Mobile.m_Deleted || (combatant.m_Map != this.m_Mobile.m_Map))) || ((!combatant.Alive || !this.m_Mobile.Alive) || ((!this.m_Mobile.CanSee(combatant) || combatant.IsDeadBondedPet) || this.m_Mobile.IsDeadBondedPet)))
                    {
                        this.m_Mobile.Combatant = null;
                        base.Stop();
                    }
                    else
                    {
                        IWeapon weapon = this.m_Mobile.Weapon;
                        if (this.m_Mobile.InRange(combatant, weapon.MaxRange) && this.m_Mobile.InLOS(combatant))
                        {
                            this.m_Mobile.RevealingAction();
                            this.m_Mobile.m_NextCombatTime = DateTime.Now + weapon.OnSwing(this.m_Mobile, combatant);
                        }
                    }
                }
            }
        }

        private class ExpireAggressorsTimer : Timer
        {
            private Mobile m_Mobile;

            public ExpireAggressorsTimer(Mobile m) : base(TimeSpan.FromSeconds(5.0), TimeSpan.FromSeconds(5.0))
            {
                this.m_Mobile = m;
                base.Priority = TimerPriority.FiveSeconds;
            }

            protected override void OnTick()
            {
                if (this.m_Mobile.Deleted || ((this.m_Mobile.Aggressors.Count == 0) && (this.m_Mobile.Aggressed.Count == 0)))
                {
                    this.m_Mobile.StopAggrExpire();
                }
                else
                {
                    this.m_Mobile.CheckAggrExpire();
                }
            }
        }

        private class ExpireCombatantTimer : Timer
        {
            private Mobile m_Mobile;

            public ExpireCombatantTimer(Mobile m) : base(TimeSpan.FromMinutes(1.0))
            {
                base.Priority = TimerPriority.FiveSeconds;
                this.m_Mobile = m;
            }

            protected override void OnTick()
            {
                this.m_Mobile.Combatant = null;
            }
        }

        private class ExpireCriminalTimer : Timer
        {
            private Mobile m_Mobile;

            public ExpireCriminalTimer(Mobile m) : base(Mobile.m_ExpireCriminalDelay)
            {
                base.Priority = TimerPriority.FiveSeconds;
                this.m_Mobile = m;
            }

            protected override void OnTick()
            {
                this.m_Mobile.Criminal = false;
            }
        }

        private class FrozenTimer : Timer
        {
            private Mobile m_Mobile;

            public FrozenTimer(Mobile m, TimeSpan duration) : base(duration)
            {
                base.Priority = TimerPriority.TwentyFiveMS;
                this.m_Mobile = m;
            }

            protected override void OnTick()
            {
                this.m_Mobile.Frozen = false;
            }
        }

        private class HitsTimer : Timer
        {
            private Mobile m_Owner;

            public HitsTimer(Mobile m) : base(Mobile.GetHitsRegenRate(m), Mobile.GetHitsRegenRate(m))
            {
                base.Priority = TimerPriority.FiftyMS;
                this.m_Owner = m;
            }

            protected override void OnTick()
            {
                if (this.m_Owner.CanRegenHits)
                {
                    this.m_Owner.Hits++;
                }
                base.Delay = base.Interval = Mobile.GetHitsRegenRate(this.m_Owner);
            }
        }

        private class LocationComparer : IComparer
        {
            private IPoint3D m_RelativeTo;

            public LocationComparer(IPoint3D relativeTo)
            {
                this.m_RelativeTo = relativeTo;
            }

            public int Compare(object x, object y)
            {
                IPoint3D p = x as IPoint3D;
                IPoint3D pointd2 = y as IPoint3D;
                return (this.GetDistance(p) - this.GetDistance(pointd2));
            }

            private int GetDistance(IPoint3D p)
            {
                int num = this.m_RelativeTo.X - p.X;
                int num2 = this.m_RelativeTo.Y - p.Y;
                int num3 = this.m_RelativeTo.Z - p.Z;
                num *= 11;
                num2 *= 11;
                return (((num * num) + (num2 * num2)) + (num3 * num3));
            }
        }

        private class LogoutTimer : Timer
        {
            private Mobile m_Mobile;

            public LogoutTimer(Mobile m) : base(TimeSpan.FromDays(1.0))
            {
                base.Priority = TimerPriority.OneSecond;
                this.m_Mobile = m;
            }

            protected override void OnTick()
            {
                if (this.m_Mobile.m_Map != Map.Internal)
                {
                    EventSink.InvokeLogout(new LogoutEventArgs(this.m_Mobile));
                    this.m_Mobile.m_LogoutLocation = this.m_Mobile.m_Location;
                    this.m_Mobile.m_LogoutMap = this.m_Mobile.m_Map;
                    this.m_Mobile.Internalize();
                }
            }
        }

        private class ManaTimer : Timer
        {
            private Mobile m_Owner;

            public ManaTimer(Mobile m) : base(Mobile.GetManaRegenRate(m), Mobile.GetManaRegenRate(m))
            {
                base.Priority = TimerPriority.FiftyMS;
                this.m_Owner = m;
            }

            protected override void OnTick()
            {
                if (this.m_Owner.CanRegenMana)
                {
                    this.m_Owner.Mana++;
                }
                base.Delay = base.Interval = Mobile.GetManaRegenRate(this.m_Owner);
            }
        }

        private class MovementRecord
        {
            public DateTime m_End;
            private static Queue m_InstancePool = new Queue();

            private MovementRecord(DateTime end)
            {
                this.m_End = end;
            }

            public bool Expired()
            {
                bool flag = DateTime.Now >= this.m_End;
                if (flag)
                {
                    m_InstancePool.Enqueue(this);
                }
                return flag;
            }

            public static Mobile.MovementRecord NewInstance(DateTime end)
            {
                if (m_InstancePool.Count > 0)
                {
                    Mobile.MovementRecord record = (Mobile.MovementRecord) m_InstancePool.Dequeue();
                    record.m_End = end;
                    return record;
                }
                return new Mobile.MovementRecord(end);
            }
        }

        private class ParalyzedTimer : Timer
        {
            private Mobile m_Mobile;

            public ParalyzedTimer(Mobile m, TimeSpan duration) : base(duration)
            {
                base.Priority = TimerPriority.TwentyFiveMS;
                this.m_Mobile = m;
            }

            protected override void OnTick()
            {
                this.m_Mobile.Paralyzed = false;
            }
        }

        private class SimpleStateTarget : Target
        {
            private TargetStateCallback m_Callback;
            private object m_State;

            public SimpleStateTarget(int range, TargetFlags flags, bool allowGround, TargetStateCallback callback, object state) : base(range, allowGround, flags)
            {
                this.m_Callback = callback;
                this.m_State = state;
            }

            protected override void OnTarget(Mobile from, object targeted)
            {
                if (this.m_Callback != null)
                {
                    this.m_Callback(from, targeted, this.m_State);
                }
            }
        }

        private class SimpleTarget : Target
        {
            private TargetCallback m_Callback;

            public SimpleTarget(int range, TargetFlags flags, bool allowGround, TargetCallback callback) : base(range, allowGround, flags)
            {
                this.m_Callback = callback;
            }

            protected override void OnTarget(Mobile from, object targeted)
            {
                if (this.m_Callback != null)
                {
                    this.m_Callback(from, targeted);
                }
            }
        }

        private class StamTimer : Timer
        {
            private Mobile m_Owner;

            public StamTimer(Mobile m) : base(Mobile.GetStamRegenRate(m), Mobile.GetStamRegenRate(m))
            {
                base.Priority = TimerPriority.FiftyMS;
                this.m_Owner = m;
            }

            protected override void OnTick()
            {
                if (this.m_Owner.CanRegenStam)
                {
                    this.m_Owner.Stam++;
                }
                base.Delay = base.Interval = Mobile.GetStamRegenRate(this.m_Owner);
            }
        }

        private class WarmodeTimer : Timer
        {
            private Mobile m_Mobile;
            private bool m_Value;

            public WarmodeTimer(Mobile m, bool value) : base(Mobile.WarmodeSpamDelay)
            {
                this.m_Mobile = m;
                this.m_Value = value;
            }

            protected override void OnTick()
            {
                this.m_Mobile.Warmode = this.m_Value;
                this.m_Mobile.m_WarmodeChanges = 0;
                this.m_Mobile.m_WarmodeTimer = null;
            }

            public bool Value
            {
                get
                {
                    return this.m_Value;
                }
                set
                {
                    this.m_Value = value;
                }
            }
        }
    }
}

