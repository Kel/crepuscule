﻿namespace Server
{
    using System;
    using System.Collections;

    [Parsable]
    public abstract class Poison
    {
        private static ArrayList m_Poisons = new ArrayList();

        protected Poison()
        {
        }

        public abstract Timer ConstructTimer(Mobile m);
        public static Poison Deserialize(GenericReader reader)
        {
            switch (reader.ReadByte())
            {
                case 1:
                    return GetPoison(reader.ReadByte());

                case 2:
                    reader.ReadInt();
                    reader.ReadDouble();
                    reader.ReadInt();
                    reader.ReadTimeSpan();
                    break;
            }
            return null;
        }

        public static Poison GetPoison(int level)
        {
            for (int i = 0; i < m_Poisons.Count; i++)
            {
                Poison poison = (Poison) m_Poisons[i];
                if (poison.Level == level)
                {
                    return poison;
                }
            }
            return null;
        }

        public static Poison GetPoison(string name)
        {
            for (int i = 0; i < m_Poisons.Count; i++)
            {
                Poison poison = (Poison) m_Poisons[i];
                if (Utility.InsensitiveCompare(poison.Name, name) == 0)
                {
                    return poison;
                }
            }
            return null;
        }

        public static Poison Parse(string value)
        {
            Poison poison = null;
            try
            {
                poison = GetPoison(Convert.ToInt32(value));
            }
            catch
            {
            }
            if (poison == null)
            {
                poison = GetPoison(value);
            }
            return poison;
        }

        public static void Register(Poison reg)
        {
            string str = reg.Name.ToLower();
            for (int i = 0; i < m_Poisons.Count; i++)
            {
                if (reg.Level == ((Poison) m_Poisons[i]).Level)
                {
                    throw new Exception("A poison with that level already exists.");
                }
                if (str == ((Poison) m_Poisons[i]).Name.ToLower())
                {
                    throw new Exception("A poison with that name already exists.");
                }
            }
            m_Poisons.Add(reg);
        }

        public static void Serialize(Poison p, GenericWriter writer)
        {
            if (p == null)
            {
                writer.Write((byte) 0);
            }
            else
            {
                writer.Write((byte) 1);
                writer.Write((byte) p.Level);
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static Poison Deadly
        {
            get
            {
                return GetPoison("Deadly");
            }
        }

        public static Poison Greater
        {
            get
            {
                return GetPoison("Greater");
            }
        }

        public static Poison Lesser
        {
            get
            {
                return GetPoison("Lesser");
            }
        }

        public static Poison Lethal
        {
            get
            {
                return GetPoison("Lethal");
            }
        }

        public abstract int Level { get; }

        public abstract string Name { get; }

        public static ArrayList Poisons
        {
            get
            {
                return m_Poisons;
            }
        }

        public static Poison Regular
        {
            get
            {
                return GetPoison("Regular");
            }
        }
    }
}

