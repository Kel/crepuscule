﻿namespace Server
{
    using System;

    public class CommandEntry : IComparable
    {
        private Server.AccessLevel m_AccessLevel;
        private string m_Command;
        private CommandEventHandler m_Handler;

        public CommandEntry(string command, CommandEventHandler handler, Server.AccessLevel accessLevel)
        {
            this.m_Command = command;
            this.m_Handler = handler;
            this.m_AccessLevel = accessLevel;
        }

        public int CompareTo(object obj)
        {
            if (obj == this)
            {
                return 0;
            }
            if (obj == null)
            {
                return 1;
            }
            CommandEntry entry = obj as CommandEntry;
            if (entry == null)
            {
                throw new ArgumentException();
            }
            return this.m_Command.CompareTo(entry.m_Command);
        }

        public Server.AccessLevel AccessLevel
        {
            get
            {
                return this.m_AccessLevel;
            }
        }

        public string Command
        {
            get
            {
                return this.m_Command;
            }
        }

        public CommandEventHandler Handler
        {
            get
            {
                return this.m_Handler;
            }
        }
    }
}

