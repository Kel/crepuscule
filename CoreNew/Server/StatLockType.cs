﻿namespace Server
{
    using System;

    public enum StatLockType : byte
    {
        Down = 1,
        Locked = 2,
        Up = 0
    }
}

