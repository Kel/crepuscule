﻿namespace Server
{
    using System;

    public enum AccessLevel
    {
        Player,
        Counselor,
        GameMaster,
        Seer,
        Administrator
    }
}

