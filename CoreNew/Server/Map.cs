﻿namespace Server
{
    using Server.Items;
    using Server.Network;
    using Server.Targeting;
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;

    [Parsable]
    public sealed class Map
    {
        private static ArrayList m_AllMaps = new ArrayList();
        private Region m_DefaultRegion;
        private int m_FileIndex;
        private int m_Height;
        private static int[] m_InvalidLandTiles = new int[] { 580 };
        private Sector m_InvalidSector;
        private int m_MapID;
        private int m_MapIndex;
        private static string[] m_MapNames;
        private static Map[] m_Maps = new Map[0x100];
        private static Map[] m_MapValues;
        private static int m_MaxLOSDistance = 0x19;
        private string m_Name;
        private static Point3DList m_PathList = new Point3DList();
        private ArrayList m_Regions;
        private MapRules m_Rules;
        private int m_Season;
        private Sector[][] m_Sectors;
        private int m_SectorsHeight;
        private int m_SectorsWidth;
        private TileMatrix m_Tiles;
        private int m_Width;
        public static int SectorActiveRange = 2;
        public const int SectorShift = 4;
        public const int SectorSize = 0x10;

        public Map(int mapID, int mapIndex, int fileIndex, int width, int height, int season, string name, MapRules rules)
        {
            this.m_MapID = mapID;
            this.m_MapIndex = mapIndex;
            this.m_FileIndex = fileIndex;
            this.m_Width = width;
            this.m_Height = height;
            this.m_Season = season;
            this.m_Name = name;
            this.m_Rules = rules;
            this.m_Regions = new ArrayList();
            this.m_InvalidSector = new Sector(0, 0, this);
            this.m_SectorsWidth = width >> 4;
            this.m_SectorsHeight = height >> 4;
            this.m_Sectors = new Sector[this.m_SectorsWidth][];
        }

        public void ActivateSectors(int cx, int cy)
        {
            for (int i = cx - SectorActiveRange; i <= (cx + SectorActiveRange); i++)
            {
                for (int j = cy - SectorActiveRange; j <= (cy + SectorActiveRange); j++)
                {
                    Sector realSector = this.GetRealSector(i, j);
                    if (realSector != this.m_InvalidSector)
                    {
                        realSector.Activate();
                    }
                }
            }
        }

        public void AddMulti(Item item, Sector start, Sector end)
        {
            if (this != Internal)
            {
                for (int i = start.X; i <= end.X; i++)
                {
                    for (int j = start.Y; j <= end.Y; j++)
                    {
                        this.InternalGetSector(i, j).OnMultiEnter(item);
                    }
                }
            }
        }

        public Point2D Bound(Point2D p)
        {
            int x = p.m_X;
            int y = p.m_Y;
            if (x < 0)
            {
                x = 0;
            }
            else if (x >= this.m_Width)
            {
                x = this.m_Width - 1;
            }
            if (y < 0)
            {
                y = 0;
            }
            else if (y >= this.m_Height)
            {
                y = this.m_Height - 1;
            }
            return new Point2D(x, y);
        }

        public void Bound(int x, int y, out int newX, out int newY)
        {
            if (x < 0)
            {
                newX = 0;
            }
            else if (x >= this.m_Width)
            {
                newX = this.m_Width - 1;
            }
            else
            {
                newX = x;
            }
            if (y < 0)
            {
                newY = 0;
            }
            else if (y >= this.m_Height)
            {
                newY = this.m_Height - 1;
            }
            else
            {
                newY = y;
            }
        }

        public bool CanFit(Point3D p, int height)
        {
            return this.CanFit(p.m_X, p.m_Y, p.m_Z, height, false, true, true);
        }

        public bool CanFit(Point2D p, int z, int height)
        {
            return this.CanFit(p.m_X, p.m_Y, z, height, false, true, true);
        }

        public bool CanFit(Point3D p, int height, bool checkBlocksFit)
        {
            return this.CanFit(p.m_X, p.m_Y, p.m_Z, height, checkBlocksFit, true, true);
        }

        public bool CanFit(Point2D p, int z, int height, bool checkBlocksFit)
        {
            return this.CanFit(p.m_X, p.m_Y, z, height, checkBlocksFit, true, true);
        }

        public bool CanFit(Point3D p, int height, bool checkBlocksFit, bool checkMobiles)
        {
            return this.CanFit(p.m_X, p.m_Y, p.m_Z, height, checkBlocksFit, checkMobiles, true);
        }

        public bool CanFit(int x, int y, int z, int height)
        {
            return this.CanFit(x, y, z, height, false, true, true);
        }

        public bool CanFit(int x, int y, int z, int height, bool checksBlocksFit)
        {
            return this.CanFit(x, y, z, height, false, true, true);
        }

        public bool CanFit(int x, int y, int z, int height, bool checkBlocksFit, bool checkMobiles)
        {
            return this.CanFit(x, y, z, height, checkBlocksFit, checkMobiles, true);
        }

        public bool CanFit(int x, int y, int z, int height, bool checkBlocksFit, bool checkMobiles, bool requireSurface)
        {
            bool surface;
            bool impassable;
            if (this == Internal)
            {
                return false;
            }
            if (((x < 0) || (y < 0)) || ((x >= this.m_Width) || (y >= this.m_Height)))
            {
                return false;
            }
            bool flag = false;
            Tile landTile = this.Tiles.GetLandTile(x, y);
            int num = 0;
            int avg = 0;
            int top = 0;
            this.GetAverageZ(x, y, ref num, ref avg, ref top);
            TileFlag flags = TileData.LandTable[landTile.ID & 0x3fff].Flags;
            if ((((flags & TileFlag.Impassable) != TileFlag.None) && (avg > z)) && ((z + height) > num))
            {
                return false;
            }
            if ((((flags & TileFlag.Impassable) == TileFlag.None) && (z == avg)) && !landTile.Ignored)
            {
                flag = true;
            }
            Tile[] tileArray = this.Tiles.GetStaticTiles(x, y, true);
            for (int i = 0; i < tileArray.Length; i++)
            {
                ItemData data = TileData.ItemTable[tileArray[i].ID & 0x3fff];
                surface = data.Surface;
                impassable = data.Impassable;
                if ((surface || impassable) && (((tileArray[i].Z + data.CalcHeight) > z) && ((z + height) > tileArray[i].Z)))
                {
                    return false;
                }
                if ((surface && !impassable) && (z == (tileArray[i].Z + data.CalcHeight)))
                {
                    flag = true;
                }
            }
            Sector sector = this.GetSector(x, y);
            ArrayList items = sector.Items;
            ArrayList mobiles = sector.Mobiles;
            for (int j = 0; j < items.Count; j++)
            {
                Item item = (Item) items[j];
                if ((item.ItemID < 0x4000) && item.AtWorldPoint(x, y))
                {
                    ItemData itemData = item.ItemData;
                    surface = itemData.Surface;
                    impassable = itemData.Impassable;
                    if (((surface || impassable) || (checkBlocksFit && item.BlocksFit)) && (((item.Z + itemData.CalcHeight) > z) && ((z + height) > item.Z)))
                    {
                        return false;
                    }
                    if ((surface && !impassable) && (z == (item.Z + itemData.CalcHeight)))
                    {
                        flag = true;
                    }
                }
            }
            if (checkMobiles)
            {
                for (int k = 0; k < mobiles.Count; k++)
                {
                    Mobile mobile = (Mobile) mobiles[k];
                    if (((mobile.Location.m_X == x) && (mobile.Location.m_Y == y)) && (((mobile.Z + 0x10) > z) && ((z + height) > mobile.Z)))
                    {
                        return false;
                    }
                }
            }
            if (requireSurface)
            {
                return flag;
            }
            return true;
        }

        public bool CanSpawnMobile(Point3D p)
        {
            return this.CanSpawnMobile(p.m_X, p.m_Y, p.m_Z);
        }

        public bool CanSpawnMobile(Point2D p, int z)
        {
            return this.CanSpawnMobile(p.m_X, p.m_Y, z);
        }

        public bool CanSpawnMobile(int x, int y, int z)
        {
            if (!Region.Find(new Point3D(x, y, z), this).AllowSpawn())
            {
                return false;
            }
            return this.CanFit(x, y, z, 0x10);
        }

        private static void CheckNamesAndValues()
        {
            if ((m_MapNames == null) || (m_MapNames.Length != m_AllMaps.Count))
            {
                m_MapNames = new string[m_AllMaps.Count];
                m_MapValues = new Map[m_AllMaps.Count];
                for (int i = 0; i < m_AllMaps.Count; i++)
                {
                    Map map = (Map) m_AllMaps[i];
                    m_MapNames[i] = map.Name;
                    m_MapValues[i] = map;
                }
            }
        }

        public void DeactivateSectors(int cx, int cy)
        {
            for (int i = cx - SectorActiveRange; i <= (cx + SectorActiveRange); i++)
            {
                for (int j = cy - SectorActiveRange; j <= (cy + SectorActiveRange); j++)
                {
                    Sector realSector = this.GetRealSector(i, j);
                    if ((realSector != this.m_InvalidSector) && !this.PlayersInRange(realSector, SectorActiveRange))
                    {
                        realSector.Deactivate();
                    }
                }
            }
        }

        public void FixColumn(int x, int y)
        {
            Tile landTile = this.Tiles.GetLandTile(x, y);
            int z = 0;
            int avg = 0;
            int top = 0;
            this.GetAverageZ(x, y, ref z, ref avg, ref top);
            Tile[] tileArray = this.Tiles.GetStaticTiles(x, y, true);
            ArrayList list = new ArrayList();
            IPooledEnumerable itemsInRange = this.GetItemsInRange(new Point3D(x, y, 0), 0);
            foreach (Item item in itemsInRange)
            {
                if (item.ItemID < 0x4000)
                {
                    list.Add(item);
                    if (list.Count > 100)
                    {
                        break;
                    }
                }
            }
            itemsInRange.Free();
            if (list.Count <= 100)
            {
                list.Sort(ZComparer.Default);
                for (int i = 0; i < list.Count; i++)
                {
                    Item item2 = (Item) list[i];
                    if (item2.Movable)
                    {
                        int num5 = -2147483648;
                        int num6 = item2.Z;
                        if (!landTile.Ignored && (avg <= num6))
                        {
                            num5 = avg;
                        }
                        for (int j = 0; j < tileArray.Length; j++)
                        {
                            Tile tile2 = tileArray[j];
                            ItemData data = TileData.ItemTable[tile2.ID & 0x3fff];
                            int num8 = tile2.Z;
                            int num9 = num8 + data.CalcHeight;
                            if ((num9 == num8) && !data.Surface)
                            {
                                num9++;
                            }
                            if ((num9 > num5) && (num9 <= num6))
                            {
                                num5 = num9;
                            }
                        }
                        for (int k = 0; k < list.Count; k++)
                        {
                            if (k != i)
                            {
                                Item item3 = (Item) list[k];
                                ItemData itemData = item3.ItemData;
                                int num11 = item3.Z;
                                int num12 = num11 + itemData.CalcHeight;
                                if ((num12 == num11) && !itemData.Surface)
                                {
                                    num12++;
                                }
                                if ((num12 > num5) && (num12 <= num6))
                                {
                                    num5 = num12;
                                }
                            }
                        }
                        if (num5 != -2147483648)
                        {
                            item2.Location = new Point3D(item2.X, item2.Y, num5);
                        }
                    }
                }
            }
        }

        public int GetAverageZ(int x, int y)
        {
            int z = 0;
            int avg = 0;
            int top = 0;
            this.GetAverageZ(x, y, ref z, ref avg, ref top);
            return avg;
        }

        public void GetAverageZ(int x, int y, ref int z, ref int avg, ref int top)
        {
            int num = this.Tiles.GetLandTile(x, y).Z;
            int num2 = this.Tiles.GetLandTile(x, y + 1).Z;
            int num3 = this.Tiles.GetLandTile(x + 1, y).Z;
            int num4 = this.Tiles.GetLandTile(x + 1, y + 1).Z;
            z = num;
            if (num2 < z)
            {
                z = num2;
            }
            if (num3 < z)
            {
                z = num3;
            }
            if (num4 < z)
            {
                z = num4;
            }
            top = num;
            if (num2 > top)
            {
                top = num2;
            }
            if (num3 > top)
            {
                top = num3;
            }
            if (num4 > top)
            {
                top = num4;
            }
            if (Math.Abs((int) (num - num4)) > Math.Abs((int) (num2 - num3)))
            {
                avg = (int) Math.Floor((double) (((double) (num2 + num3)) / 2.0));
            }
            else
            {
                avg = (int) Math.Floor((double) (((double) (num + num4)) / 2.0));
            }
        }

        public IPooledEnumerable GetClientsInBounds(Rectangle2D bounds)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, bounds, SectorEnumeratorType.Clients));
        }

        public IPooledEnumerable GetClientsInRange(Point3D p)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, new Rectangle2D(p.m_X - 0x12, p.m_Y - 0x12, 0x25, 0x25), SectorEnumeratorType.Clients));
        }

        public IPooledEnumerable GetClientsInRange(Point3D p, int range)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, new Rectangle2D(p.m_X - range, p.m_Y - range, (range * 2) + 1, (range * 2) + 1), SectorEnumeratorType.Clients));
        }

        public IPooledEnumerable GetItemsInBounds(Rectangle2D bounds)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, bounds, SectorEnumeratorType.Items));
        }

        public IPooledEnumerable GetItemsInRange(Point3D p)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, new Rectangle2D(p.m_X - 0x12, p.m_Y - 0x12, 0x25, 0x25), SectorEnumeratorType.Items));
        }

        public IPooledEnumerable GetItemsInRange(Point3D p, int range)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, new Rectangle2D(p.m_X - range, p.m_Y - range, (range * 2) + 1, (range * 2) + 1), SectorEnumeratorType.Items));
        }

        public static string[] GetMapNames()
        {
            CheckNamesAndValues();
            return m_MapNames;
        }

        public static Map[] GetMapValues()
        {
            CheckNamesAndValues();
            return m_MapValues;
        }

        public IPooledEnumerable GetMobilesInBounds(Rectangle2D bounds)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, bounds, SectorEnumeratorType.Mobiles));
        }

        public IPooledEnumerable GetMobilesInRange(Point3D p)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, new Rectangle2D(p.m_X - 0x12, p.m_Y - 0x12, 0x25, 0x25), SectorEnumeratorType.Mobiles));
        }

        public IPooledEnumerable GetMobilesInRange(Point3D p, int range)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(TypedEnumerator.Instantiate(this, new Rectangle2D(p.m_X - range, p.m_Y - range, (range * 2) + 1, (range * 2) + 1), SectorEnumeratorType.Mobiles));
        }

        public Sector GetMultiMaxSector(Point3D loc, MultiComponentList mcl)
        {
            return this.GetSector(this.Bound(new Point2D(loc.m_X + mcl.Max.m_X, loc.m_Y + mcl.Max.m_Y)));
        }

        public Sector GetMultiMinSector(Point3D loc, MultiComponentList mcl)
        {
            return this.GetSector(this.Bound(new Point2D(loc.m_X + mcl.Min.m_X, loc.m_Y + mcl.Min.m_Y)));
        }

        public IPooledEnumerable GetMultiTilesAt(int x, int y)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            Sector sector = this.GetSector(x, y);
            if (sector.Multis.Count == 0)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(MultiTileEnumerator.Instantiate(sector, new Point2D(x, y)));
        }

        public IPooledEnumerable GetObjectsInBounds(Rectangle2D bounds)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(ObjectEnumerator.Instantiate(this, bounds));
        }

        public IPooledEnumerable GetObjectsInRange(Point3D p)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(ObjectEnumerator.Instantiate(this, new Rectangle2D(p.m_X - 0x12, p.m_Y - 0x12, 0x25, 0x25)));
        }

        public IPooledEnumerable GetObjectsInRange(Point3D p, int range)
        {
            if (this == Internal)
            {
                return NullEnumerable.Instance;
            }
            return PooledEnumerable.Instantiate(ObjectEnumerator.Instantiate(this, new Rectangle2D(p.m_X - range, p.m_Y - range, (range * 2) + 1, (range * 2) + 1)));
        }

        public Point3D GetPoint(object o, bool eye)
        {
            Point3D location;
            if (o is Mobile)
            {
                location = ((Mobile) o).Location;
                location.Z += eye ? 15 : 10;
                return location;
            }
            if (o is Item)
            {
                location = ((Item) o).GetWorldLocation();
                location.Z += (((Item) o).ItemData.Height / 2) + 1;
                return location;
            }
            if (o is Point3D)
            {
                return (Point3D) o;
            }
            if (o is LandTarget)
            {
                location = ((LandTarget) o).Location;
                int z = 0;
                int avg = 0;
                int top = 0;
                this.GetAverageZ(location.X, location.Y, ref z, ref avg, ref top);
                location.Z = top + 1;
                return location;
            }
            if (o is StaticTarget)
            {
                StaticTarget target = (StaticTarget) o;
                ItemData data = TileData.ItemTable[target.ItemID & 0x3fff];
                return new Point3D(target.X, target.Y, ((target.Z - data.CalcHeight) + (data.Height / 2)) + 1);
            }
            if (o is IPoint3D)
            {
                return new Point3D((IPoint3D) o);
            }
            Console.WriteLine("Warning: Invalid object ({0}) in line of sight", o);
            return Point3D.Zero;
        }

        public Sector GetRealSector(int x, int y)
        {
            return this.InternalGetSector(x, y);
        }

        public Sector GetSector(IPoint2D p)
        {
            return this.InternalGetSector(p.X >> 4, p.Y >> 4);
        }

        public Sector GetSector(Point2D p)
        {
            return this.InternalGetSector(p.m_X >> 4, p.m_Y >> 4);
        }

        public Sector GetSector(Point3D p)
        {
            return this.InternalGetSector(p.m_X >> 4, p.m_Y >> 4);
        }

        public Sector GetSector(int x, int y)
        {
            return this.InternalGetSector(x >> 4, y >> 4);
        }

        public ArrayList GetTilesAt(Point2D p, bool items, bool land, bool statics)
        {
            ArrayList list = new ArrayList();
            if (this != Internal)
            {
                if (land)
                {
                    list.Add(this.Tiles.GetLandTile(p.m_X, p.m_Y));
                }
                if (statics)
                {
                    list.AddRange(this.Tiles.GetStaticTiles(p.m_X, p.m_Y, true));
                }
                if (!items)
                {
                    return list;
                }
                foreach (Item item in this.GetSector(p).Items)
                {
                    if (item.AtWorldPoint(p.m_X, p.m_Y))
                    {
                        list.Add(new Tile((short) ((item.ItemID & 0x3fff) + 0x4000), (sbyte) item.Z));
                    }
                }
            }
            return list;
        }

        private Sector InternalGetSector(int x, int y)
        {
            if (((x < 0) || (x >= this.m_SectorsWidth)) || ((y < 0) || (y >= this.m_SectorsHeight)))
            {
                return this.m_InvalidSector;
            }
            Sector[] sectorArray = this.m_Sectors[x];
            if (sectorArray == null)
            {
                this.m_Sectors[x] = sectorArray = new Sector[this.m_SectorsHeight];
            }
            Sector sector = sectorArray[y];
            if (sector == null)
            {
                sectorArray[y] = sector = new Sector(x, y, this);
            }
            return sector;
        }

        public bool LineOfSight(Mobile from, Mobile to)
        {
            if ((from == to) || (from.AccessLevel > AccessLevel.Player))
            {
                return true;
            }
            Point3D location = from.Location;
            Point3D dest = to.Location;
            location.Z += 14;
            dest.Z += 10;
            return this.LineOfSight(location, dest);
        }

        public bool LineOfSight(Mobile from, Point3D target)
        {
            if (from.AccessLevel > AccessLevel.Player)
            {
                return true;
            }
            Point3D location = from.Location;
            location.Z += 14;
            return this.LineOfSight(location, target);
        }

        public bool LineOfSight(Point3D org, Point3D dest)
        {
            if (this == Internal)
            {
                return false;
            }
            if (!Utility.InRange(org, dest, m_MaxLOSDistance))
            {
                return false;
            }
            Point3DList pathList = m_PathList;
            if (org != dest)
            {
                double num4;
                if (pathList.Count > 0)
                {
                    pathList.Clear();
                }
                int num8 = dest.m_X - org.m_X;
                int num9 = dest.m_Y - org.m_Y;
                int num10 = dest.m_Z - org.m_Z;
                double num3 = Math.Sqrt((double) ((num8 * num8) + (num9 * num9)));
                if (num10 != 0)
                {
                    num4 = Math.Sqrt((num3 * num3) + (num10 * num10));
                }
                else
                {
                    num4 = num3;
                }
                double num = ((double) num9) / num4;
                double num2 = ((double) num8) / num4;
                num3 = ((double) num10) / num4;
                double y = org.m_Y;
                double z = org.m_Z;
                double x = org.m_X;
                while ((Utility.NumberBetween(x, dest.m_X, org.m_X, 0.5) && Utility.NumberBetween(y, dest.m_Y, org.m_Y, 0.5)) && Utility.NumberBetween(z, dest.m_Z, org.m_Z, 0.5))
                {
                    int num11 = (int) Math.Round(x);
                    int num12 = (int) Math.Round(y);
                    int num13 = (int) Math.Round(z);
                    if (pathList.Count > 0)
                    {
                        Point3D last = pathList.Last;
                        if (((last.m_X != num11) || (last.m_Y != num12)) || (last.m_Z != num13))
                        {
                            pathList.Add(num11, num12, num13);
                        }
                    }
                    else
                    {
                        pathList.Add(num11, num12, num13);
                    }
                    x += num2;
                    y += num;
                    z += num3;
                }
                if (pathList.Count != 0)
                {
                    int calcHeight;
                    if (pathList.Last != dest)
                    {
                        pathList.Add(dest);
                    }
                    Point3D top = org;
                    Point3D bottom = dest;
                    Utility.FixPoints(ref top, ref bottom);
                    int count = pathList.Count;
                    for (int i = 0; i < count; i++)
                    {
                        Point3D p = pathList[i];
                        Tile landTile = this.Tiles.GetLandTile(p.X, p.Y);
                        int num17 = 0;
                        int avg = 0;
                        int num19 = 0;
                        this.GetAverageZ(p.m_X, p.m_Y, ref num17, ref avg, ref num19);
                        if ((((num17 <= p.m_Z) && (num19 >= p.m_Z)) && (((p.m_X != dest.m_X) || (p.m_Y != dest.m_Y)) || ((num17 > dest.m_Z) || (num19 < dest.m_Z)))) && !landTile.Ignored)
                        {
                            return false;
                        }
                        Tile[] tileArray = this.Tiles.GetStaticTiles(p.m_X, p.m_Y, true);
                        bool flag3 = false;
                        int iD = landTile.ID;
                        for (int j = 0; !flag3 && (j < m_InvalidLandTiles.Length); j++)
                        {
                            flag3 = iD == m_InvalidLandTiles[j];
                        }
                        if (flag3 && (tileArray.Length == 0))
                        {
                            IPooledEnumerable itemsInRange = this.GetItemsInRange(p, 0);
                            foreach (Item item in itemsInRange)
                            {
                                if (item.Visible)
                                {
                                    flag3 = false;
                                }
                                if (!flag3)
                                {
                                    break;
                                }
                            }
                            itemsInRange.Free();
                            if (flag3)
                            {
                                return false;
                            }
                        }
                        for (int k = 0; k < tileArray.Length; k++)
                        {
                            Tile tile2 = tileArray[k];
                            ItemData data = TileData.ItemTable[tile2.ID & 0x3fff];
                            TileFlag flags = data.Flags;
                            calcHeight = data.CalcHeight;
                            if ((((tile2.Z <= p.Z) && ((tile2.Z + calcHeight) >= p.Z)) && ((flags & (TileFlag.NoShoot | TileFlag.Window)) != TileFlag.None)) && (((p.m_X != dest.m_X) || (p.m_Y != dest.m_Y)) || ((tile2.Z > dest.m_Z) || ((tile2.Z + calcHeight) < dest.m_Z))))
                            {
                                return false;
                            }
                        }
                    }
                    Rectangle2D bounds = new Rectangle2D(top.m_X, top.m_Y, (bottom.m_X - top.m_X) + 1, (bottom.m_Y - top.m_Y) + 1);
                    IPooledEnumerable itemsInBounds = this.GetItemsInBounds(bounds);
                    foreach (Item item2 in itemsInBounds)
                    {
                        if (!item2.Visible || (item2.ItemID >= 0x4000))
                        {
                            continue;
                        }
                        ItemData itemData = item2.ItemData;
                        if ((itemData.Flags & (TileFlag.NoShoot | TileFlag.Window)) != TileFlag.None)
                        {
                            calcHeight = itemData.CalcHeight;
                            bool flag = false;
                            int num23 = pathList.Count;
                            for (int m = 0; m < num23; m++)
                            {
                                Point3D pointd5 = pathList[m];
                                Point3D location = item2.Location;
                                if ((((location.m_X == pointd5.m_X) && (location.m_Y == pointd5.m_Y)) && ((location.m_Z <= pointd5.m_Z) && ((location.m_Z + calcHeight) >= pointd5.m_Z))) && (((location.m_X != dest.m_X) || (location.m_Y != dest.m_Y)) || ((location.m_Z > dest.m_Z) || ((location.m_Z + calcHeight) < dest.m_Z))))
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag)
                            {
                                itemsInBounds.Free();
                                return false;
                            }
                        }
                    }
                    itemsInBounds.Free();
                }
            }
            return true;
        }

        public bool LineOfSight(object from, object dest)
        {
            return (((from == dest) || ((from is Mobile) && (((Mobile) from).AccessLevel > AccessLevel.Player))) || ((((dest is Item) && (from is Mobile)) && (((Item) dest).RootParent == from)) || this.LineOfSight(this.GetPoint(from, true), this.GetPoint(dest, false))));
        }

        public void OnClientChange(NetState oldState, NetState newState, Mobile m)
        {
            if (this != Internal)
            {
                this.GetSector(m).OnClientChange(oldState, newState);
            }
        }

        public void OnEnter(Item item)
        {
            if (this != Internal)
            {
                this.GetSector(item).OnEnter(item);
                if (item is BaseMulti)
                {
                    MultiComponentList components = ((BaseMulti) item).Components;
                    Sector multiMinSector = this.GetMultiMinSector(item.Location, components);
                    Sector multiMaxSector = this.GetMultiMaxSector(item.Location, components);
                    this.AddMulti(item, multiMinSector, multiMaxSector);
                }
            }
        }

        public void OnEnter(Mobile m)
        {
            if (this != Internal)
            {
                this.GetSector(m).OnEnter(m);
            }
        }

        public void OnLeave(Item item)
        {
            if (this != Internal)
            {
                this.GetSector(item).OnLeave(item);
                if (item is BaseMulti)
                {
                    MultiComponentList components = ((BaseMulti) item).Components;
                    Sector multiMinSector = this.GetMultiMinSector(item.Location, components);
                    Sector multiMaxSector = this.GetMultiMaxSector(item.Location, components);
                    this.RemoveMulti(item, multiMinSector, multiMaxSector);
                }
            }
        }

        public void OnLeave(Mobile m)
        {
            if (this != Internal)
            {
                this.GetSector(m).OnLeave(m);
            }
        }

        public void OnMove(Point3D oldLocation, Item item)
        {
            if (this != Internal)
            {
                Sector sector = this.GetSector(oldLocation);
                Sector sector2 = this.GetSector(item.Location);
                if (sector != sector2)
                {
                    sector.OnLeave(item);
                    sector2.OnEnter(item);
                }
                if (item is BaseMulti)
                {
                    MultiComponentList components = ((BaseMulti) item).Components;
                    Sector multiMinSector = this.GetMultiMinSector(item.Location, components);
                    Sector multiMaxSector = this.GetMultiMaxSector(item.Location, components);
                    Sector start = this.GetMultiMinSector(oldLocation, components);
                    Sector end = this.GetMultiMaxSector(oldLocation, components);
                    if ((start != multiMinSector) || (end != multiMaxSector))
                    {
                        this.RemoveMulti(item, start, end);
                        this.AddMulti(item, multiMinSector, multiMaxSector);
                    }
                }
            }
        }

        public void OnMove(Point3D oldLocation, Mobile m)
        {
            if (this != Internal)
            {
                Sector sector = this.GetSector(oldLocation);
                Sector sector2 = this.GetSector(m.Location);
                if (sector != sector2)
                {
                    sector.OnLeave(m);
                    sector2.OnEnter(m);
                }
            }
        }

        public static Map Parse(string value)
        {
            CheckNamesAndValues();
            for (int i = 0; i < m_MapNames.Length; i++)
            {
                if (Insensitive.Equals(m_MapNames[i], value))
                {
                    return m_MapValues[i];
                }
            }
            try
            {
                int index = int.Parse(value);
                if (((index >= 0) && (index < m_Maps.Length)) && (m_Maps[index] != null))
                {
                    return m_Maps[index];
                }
            }
            catch
            {
            }
            throw new Exception("Invalid map name");
        }

        private bool PlayersInRange(Sector sect, int range)
        {
            for (int i = sect.X - range; i <= (sect.X + range); i++)
            {
                for (int j = sect.Y - range; j <= (sect.Y + range); j++)
                {
                    Sector realSector = this.GetRealSector(i, j);
                    if ((realSector != this.m_InvalidSector) && (realSector.Players.Count > 0))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void RemoveMulti(Item item, Sector start, Sector end)
        {
            if (this != Internal)
            {
                for (int i = start.X; i <= end.X; i++)
                {
                    for (int j = start.Y; j <= end.Y; j++)
                    {
                        this.InternalGetSector(i, j).OnMultiLeave(item);
                    }
                }
            }
        }

        public override string ToString()
        {
            return this.m_Name;
        }

        public static ArrayList AllMaps
        {
            get
            {
                return m_AllMaps;
            }
        }

        public Region DefaultRegion
        {
            get
            {
                if (this.m_DefaultRegion == null)
                {
                    this.m_DefaultRegion = new Region("", "", this);
                }
                return this.m_DefaultRegion;
            }
            set
            {
                this.m_DefaultRegion = value;
            }
        }

        public static Map Felucca
        {
            get
            {
                return m_Maps[0];
            }
        }

        public int Height
        {
            get
            {
                return this.m_Height;
            }
        }

        public static Map Ilshenar
        {
            get
            {
                return m_Maps[2];
            }
        }

        public static Map Internal
        {
            get
            {
                return m_Maps[0x7f];
            }
        }

        public static int[] InvalidLandTiles
        {
            get
            {
                return m_InvalidLandTiles;
            }
            set
            {
                m_InvalidLandTiles = value;
            }
        }

        public Sector InvalidSector
        {
            get
            {
                return this.m_InvalidSector;
            }
        }

        public static Map Malas
        {
            get
            {
                return m_Maps[3];
            }
        }

        public int MapID
        {
            get
            {
                return this.m_MapID;
            }
        }

        public int MapIndex
        {
            get
            {
                return this.m_MapIndex;
            }
        }

        public static Map[] Maps
        {
            get
            {
                return m_Maps;
            }
        }

        public static int MaxLOSDistance
        {
            get
            {
                return m_MaxLOSDistance;
            }
            set
            {
                m_MaxLOSDistance = value;
            }
        }

        public string Name
        {
            get
            {
                return this.m_Name;
            }
            set
            {
                this.m_Name = value;
            }
        }

        public ArrayList Regions
        {
            get
            {
                return this.m_Regions;
            }
        }

        public MapRules Rules
        {
            get
            {
                return this.m_Rules;
            }
            set
            {
                this.m_Rules = value;
            }
        }

        public int Season
        {
            get
            {
                return this.m_Season;
            }
            set
            {
                this.m_Season = value;
            }
        }

        public TileMatrix Tiles
        {
            get
            {
                if (this.m_Tiles == null)
                {
                    this.m_Tiles = new TileMatrix(this, this.m_FileIndex, this.m_MapID, this.m_Width, this.m_Height);
                }
                return this.m_Tiles;
            }
        }

        public static Map Trammel
        {
            get
            {
                return m_Maps[1];
            }
        }

        public int Width
        {
            get
            {
                return this.m_Width;
            }
        }

        private class MultiTileEnumerator : IPooledEnumerator, IEnumerator, IDisposable
        {
            private object m_Current;
            private IPooledEnumerable m_Enumerable;
            private int m_Index;
            private static Queue m_InstancePool = new Queue();
            private ArrayList m_List;
            private Point2D m_Location;

            private MultiTileEnumerator(Sector sector, Point2D loc)
            {
                this.m_List = sector.Multis;
                this.m_Location = loc;
                this.Reset();
            }

            public void Dispose()
            {
                this.Free();
            }

            public void Free()
            {
                if (this.m_List != null)
                {
                    m_InstancePool.Enqueue(this);
                    this.m_List = null;
                    if (this.m_Enumerable != null)
                    {
                        this.m_Enumerable.Free();
                    }
                }
            }

            public static Map.MultiTileEnumerator Instantiate(Sector sector, Point2D loc)
            {
                if (m_InstancePool.Count > 0)
                {
                    Map.MultiTileEnumerator enumerator = (Map.MultiTileEnumerator) m_InstancePool.Dequeue();
                    enumerator.m_List = sector.Multis;
                    enumerator.m_Location = loc;
                    enumerator.Reset();
                    return enumerator;
                }
                return new Map.MultiTileEnumerator(sector, loc);
            }

            public bool MoveNext()
            {
                while (++this.m_Index < this.m_List.Count)
                {
                    BaseMulti multi = this.m_List[this.m_Index] as BaseMulti;
                    if ((multi != null) && !multi.Deleted)
                    {
                        MultiComponentList components = multi.Components;
                        int index = this.m_Location.m_X - (multi.Location.m_X + components.Min.m_X);
                        int num2 = this.m_Location.m_Y - (multi.Location.m_Y + components.Min.m_Y);
                        if (((index >= 0) && (index < components.Width)) && ((num2 >= 0) && (num2 < components.Height)))
                        {
                            Tile[] tileArray = components.Tiles[index][num2];
                            if (tileArray.Length > 0)
                            {
                                Tile[] tileArray2 = new Tile[tileArray.Length];
                                for (int i = 0; i < tileArray2.Length; i++)
                                {
                                    tileArray2[i] = tileArray[i];
                                    tileArray2[i].Z += multi.Z;
                                }
                                this.m_Current = tileArray2;
                                return true;
                            }
                        }
                    }
                }
                return false;
            }

            public void Reset()
            {
                this.m_Current = null;
                this.m_Index = -1;
            }

            public object Current
            {
                get
                {
                    return this.m_Current;
                }
            }

            public IPooledEnumerable Enumerable
            {
                get
                {
                    return this.m_Enumerable;
                }
                set
                {
                    this.m_Enumerable = value;
                }
            }
        }

        public class NullEnumerable : IPooledEnumerable, IEnumerable
        {
            public static readonly Map.NullEnumerable Instance = new Map.NullEnumerable();
            private InternalEnumerator m_Enumerator = new InternalEnumerator();

            private NullEnumerable()
            {
            }

            public void Free()
            {
            }

            public IEnumerator GetEnumerator()
            {
                return this.m_Enumerator;
            }

            private class InternalEnumerator : IEnumerator
            {
                public bool MoveNext()
                {
                    return false;
                }

                public void Reset()
                {
                }

                public object Current
                {
                    get
                    {
                        return null;
                    }
                }
            }
        }

        private class ObjectEnumerator : IPooledEnumerator, IEnumerator, IDisposable
        {
            private Rectangle2D m_Bounds;
            private object m_Current;
            private IPooledEnumerable m_Enumerable;
            private Map.SectorEnumerator m_Enumerator;
            private static Queue m_InstancePool = new Queue();
            private Map m_Map;
            private int m_Stage;

            private ObjectEnumerator(Map map, Rectangle2D bounds)
            {
                this.m_Map = map;
                this.m_Bounds = bounds;
                this.Reset();
            }

            public void Dispose()
            {
                this.Free();
            }

            public void Free()
            {
                if (this.m_Map != null)
                {
                    m_InstancePool.Enqueue(this);
                    this.m_Map = null;
                    if (this.m_Enumerator != null)
                    {
                        this.m_Enumerator.Free();
                        this.m_Enumerator = null;
                    }
                    if (this.m_Enumerable != null)
                    {
                        this.m_Enumerable.Free();
                    }
                }
            }

            public static Map.ObjectEnumerator Instantiate(Map map, Rectangle2D bounds)
            {
                if (m_InstancePool.Count > 0)
                {
                    Map.ObjectEnumerator enumerator = (Map.ObjectEnumerator) m_InstancePool.Dequeue();
                    enumerator.m_Map = map;
                    enumerator.m_Bounds = bounds;
                    enumerator.Reset();
                    return enumerator;
                }
                return new Map.ObjectEnumerator(map, bounds);
            }

            public bool MoveNext()
            {
                while (true)
                {
                    if (m_Enumerator.MoveNext())
                    {
                        object o = m_Enumerator.Current;

                        if (o is Mobile)
                        {
                            Mobile m = (Mobile)o;

                            if (m_Bounds.Contains(m.Location))
                            {
                                m_Current = o;
                                return true;
                            }
                        }
                        else if (o is Item)
                        {
                            Item item = (Item)o;

                            if (item.Parent == null && m_Bounds.Contains(item.Location))
                            {
                                m_Current = o;
                                return true;
                            }
                        }
                    }
                    else if (m_Stage == 0)
                    {
                        m_Enumerator.Free();
                        m_Enumerator = SectorEnumerator.Instantiate(m_Map, m_Bounds, SectorEnumeratorType.Mobiles);

                        m_Current = null;
                        m_Stage = 1;
                    }
                    else
                    {
                        m_Enumerator.Free();
                        m_Enumerator = null;

                        m_Current = null;
                        m_Stage = -1;

                        return false;
                    }
                }
            }

            public void Reset()
            {
                this.m_Stage = 0;
                this.m_Current = null;
                if (this.m_Enumerator != null)
                {
                    this.m_Enumerator.Free();
                }
                this.m_Enumerator = Map.SectorEnumerator.Instantiate(this.m_Map, this.m_Bounds, Map.SectorEnumeratorType.Items);
            }

            public object Current
            {
                get
                {
                    return this.m_Current;
                }
            }

            public IPooledEnumerable Enumerable
            {
                get
                {
                    return this.m_Enumerable;
                }
                set
                {
                    this.m_Enumerable = value;
                }
            }
        }

        private class PooledEnumerable : IPooledEnumerable, IEnumerable, IDisposable
        {
            private static int m_Depth = 0;
            private IPooledEnumerator m_Enumerator;
            private static Queue m_InstancePool = new Queue();

            private PooledEnumerable(IPooledEnumerator etor)
            {
                this.m_Enumerator = etor;
            }

            public void Dispose()
            {
                this.Free();
            }

            public void Free()
            {
                if (this.m_Enumerator != null)
                {
                    m_InstancePool.Enqueue(this);
                    this.m_Enumerator.Free();
                    this.m_Enumerator = null;
                    m_Depth--;
                }
            }

            public IEnumerator GetEnumerator()
            {
                if (this.m_Enumerator == null)
                {
                    throw new ObjectDisposedException("PooledEnumerable", "GetEnumerator() called after Free()");
                }
                return this.m_Enumerator;
            }

            public static Map.PooledEnumerable Instantiate(IPooledEnumerator etor)
            {
                Map.PooledEnumerable enumerable;
                m_Depth++;
                if (m_Depth >= 5)
                {
                    Console.WriteLine("Warning: Make sure to call .Free() on pooled enumerables.");
                }
                if (m_InstancePool.Count > 0)
                {
                    enumerable = (Map.PooledEnumerable) m_InstancePool.Dequeue();
                    enumerable.m_Enumerator = etor;
                }
                else
                {
                    enumerable = new Map.PooledEnumerable(etor);
                }
                etor.Enumerable = enumerable;
                return enumerable;
            }
        }

        private class SectorEnumerator : IPooledEnumerator, IEnumerator, IDisposable
        {
            private Rectangle2D m_Bounds;
            private int m_CurrentIndex;
            private ArrayList m_CurrentList;
            private IPooledEnumerable m_Enumerable;
            private static Queue m_InstancePool = new Queue();
            private Map m_Map;
            private Map.SectorEnumeratorType m_Type;
            private int m_xSector;
            private int m_xSectorEnd;
            private int m_xSectorStart;
            private int m_ySector;
            private int m_ySectorEnd;
            private int m_ySectorStart;

            private SectorEnumerator(Map map, Rectangle2D bounds, Map.SectorEnumeratorType type)
            {
                this.m_Map = map;
                this.m_Bounds = bounds;
                this.m_Type = type;
                this.Reset();
            }

            public void Dispose()
            {
                this.Free();
            }

            public void Free()
            {
                if (this.m_Map != null)
                {
                    m_InstancePool.Enqueue(this);
                    this.m_Map = null;
                    if (this.m_Enumerable != null)
                    {
                        this.m_Enumerable.Free();
                    }
                }
            }

            private ArrayList GetListForSector(Sector sector)
            {
                switch (this.m_Type)
                {
                    case Map.SectorEnumeratorType.Mobiles:
                        return sector.Mobiles;

                    case Map.SectorEnumeratorType.Items:
                        return sector.Items;

                    case Map.SectorEnumeratorType.Clients:
                        return sector.Clients;
                }
                throw new Exception("Invalid SectorEnumeratorType");
            }

            public static Map.SectorEnumerator Instantiate(Map map, Rectangle2D bounds, Map.SectorEnumeratorType type)
            {
                if (m_InstancePool.Count > 0)
                {
                    Map.SectorEnumerator enumerator = (Map.SectorEnumerator) m_InstancePool.Dequeue();
                    enumerator.m_Map = map;
                    enumerator.m_Bounds = bounds;
                    enumerator.m_Type = type;
                    enumerator.Reset();
                    return enumerator;
                }
                return new Map.SectorEnumerator(map, bounds, type);
            }

            public bool MoveNext()
            {
                while (true)
                {
                    this.m_CurrentIndex++;
                    if (this.m_CurrentIndex != this.m_CurrentList.Count)
                    {
                        return true;
                    }
                    this.m_ySector++;
                    if (this.m_ySector > this.m_ySectorEnd)
                    {
                        this.m_ySector = this.m_ySectorStart;
                        this.m_xSector++;
                        if (this.m_xSector > this.m_xSectorEnd)
                        {
                            this.m_CurrentIndex = -1;
                            this.m_CurrentList = null;
                            return false;
                        }
                    }
                    this.m_CurrentIndex = -1;
                    this.m_CurrentList = this.GetListForSector(this.m_Map.InternalGetSector(this.m_xSector, this.m_ySector));
                }
            }

            public void Reset()
            {
                this.m_Map.Bound(this.m_Bounds.Start.m_X, this.m_Bounds.Start.m_Y, out this.m_xSectorStart, out this.m_ySectorStart);
                this.m_Map.Bound(this.m_Bounds.End.m_X - 1, this.m_Bounds.End.m_Y - 1, out this.m_xSectorEnd, out this.m_ySectorEnd);
                this.m_xSector = this.m_xSectorStart = this.m_xSectorStart >> 4;
                this.m_ySector = this.m_ySectorStart = this.m_ySectorStart >> 4;
                this.m_xSectorEnd = this.m_xSectorEnd >> 4;
                this.m_ySectorEnd = this.m_ySectorEnd >> 4;
                this.m_CurrentIndex = -1;
                this.m_CurrentList = this.GetListForSector(this.m_Map.InternalGetSector(this.m_xSector, this.m_ySector));
            }

            public object Current
            {
                get
                {
                    return this.m_CurrentList[this.m_CurrentIndex];
                }
            }

            public IPooledEnumerable Enumerable
            {
                get
                {
                    return this.m_Enumerable;
                }
                set
                {
                    this.m_Enumerable = value;
                }
            }
        }

        private enum SectorEnumeratorType
        {
            Mobiles,
            Items,
            Clients
        }

        private class TypedEnumerator : IPooledEnumerator, IEnumerator, IDisposable
        {
            private Rectangle2D m_Bounds;
            private object m_Current;
            private IPooledEnumerable m_Enumerable;
            private Map.SectorEnumerator m_Enumerator;
            private static Queue m_InstancePool = new Queue();
            private Map m_Map;
            private Map.SectorEnumeratorType m_Type;

            public TypedEnumerator(Map map, Rectangle2D bounds, Map.SectorEnumeratorType type)
            {
                this.m_Map = map;
                this.m_Bounds = bounds;
                this.m_Type = type;
                this.Reset();
            }

            public void Dispose()
            {
                this.Free();
            }

            public void Free()
            {
                if (this.m_Map != null)
                {
                    m_InstancePool.Enqueue(this);
                    this.m_Map = null;
                    if (this.m_Enumerator != null)
                    {
                        this.m_Enumerator.Free();
                        this.m_Enumerator = null;
                    }
                    if (this.m_Enumerable != null)
                    {
                        this.m_Enumerable.Free();
                    }
                }
            }

            public static Map.TypedEnumerator Instantiate(Map map, Rectangle2D bounds, Map.SectorEnumeratorType type)
            {
                if (m_InstancePool.Count > 0)
                {
                    Map.TypedEnumerator enumerator = (Map.TypedEnumerator) m_InstancePool.Dequeue();
                    enumerator.m_Map = map;
                    enumerator.m_Bounds = bounds;
                    enumerator.m_Type = type;
                    enumerator.Reset();
                    return enumerator;
                }
                return new Map.TypedEnumerator(map, bounds, type);
            }

            public bool MoveNext()
            {
                while (true)
                {
                    if (m_Enumerator.MoveNext())
                    {
                        object o = m_Enumerator.Current;

                        if (o is Mobile)
                        {
                            Mobile m = (Mobile)o;

                            if (!m.Deleted && m_Bounds.Contains(m.Location))
                            {
                                m_Current = o;
                                return true;
                            }
                        }
                        else if (o is Item)
                        {
                            Item item = (Item)o;

                            if (!item.Deleted && item.Parent == null && m_Bounds.Contains(item.Location))
                            {
                                m_Current = o;
                                return true;
                            }
                        }
                        else if (o is NetState)
                        {
                            Mobile m = ((NetState)o).Mobile;

                            if (m != null && !m.Deleted && m_Bounds.Contains(m.Location))
                            {
                                m_Current = o;
                                return true;
                            }
                        }
                    }
                    else
                    {
                        m_Current = null;

                        m_Enumerator.Free();
                        m_Enumerator = null;

                        return false;
                    }
                }
				
            }

            public void Reset()
            {
                this.m_Current = null;
                if (this.m_Enumerator != null)
                {
                    this.m_Enumerator.Free();
                }
                this.m_Enumerator = Map.SectorEnumerator.Instantiate(this.m_Map, this.m_Bounds, this.m_Type);
            }

            public object Current
            {
                get
                {
                    return this.m_Current;
                }
            }

            public IPooledEnumerable Enumerable
            {
                get
                {
                    return this.m_Enumerable;
                }
                set
                {
                    this.m_Enumerable = value;
                }
            }
        }

        private class ZComparer : IComparer
        {
            public static readonly IComparer Default = new Map.ZComparer();

            public int Compare(object x, object y)
            {
                Item item = (Item) x;
                Item item2 = (Item) y;
                return item.Z.CompareTo(item2.Z);
            }
        }
    }
}

