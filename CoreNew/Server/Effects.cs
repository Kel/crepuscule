﻿namespace Server
{
    using Server.Network;
    using System;

    public class Effects
    {
        private static Server.ParticleSupportType m_ParticleSupportType = Server.ParticleSupportType.Detect;

        public static void PlaySound(IPoint3D p, Map map, int soundID)
        {
            if ((soundID != -1) && (map != null))
            {
                Packet packet = null;
                IPooledEnumerable clientsInRange = map.GetClientsInRange(new Point3D(p));
                foreach (NetState state in clientsInRange)
                {
                    state.Mobile.ProcessDelta();
                    if (packet == null)
                    {
                        packet = new Server.Network.PlaySound(soundID, p);
                    }
                    state.Send(packet);
                }
                clientsInRange.Free();
            }
        }

        public static void SendBoltEffect(IEntity e)
        {
            SendBoltEffect(e, true, 0);
        }

        public static void SendBoltEffect(IEntity e, bool sound)
        {
            SendBoltEffect(e, sound, 0);
        }

        public static void SendBoltEffect(IEntity e, bool sound, int hue)
        {
            Map map = e.Map;
            if (map != null)
            {
                if (e is Item)
                {
                    ((Item) e).ProcessDelta();
                }
                else if (e is Mobile)
                {
                    ((Mobile) e).ProcessDelta();
                }
                Packet p = null;
                Packet packet2 = null;
                Packet packet3 = null;
                IPooledEnumerable clientsInRange = map.GetClientsInRange(e.Location);
                foreach (NetState state in clientsInRange)
                {
                    if (!state.Mobile.CanSee(e))
                    {
                        continue;
                    }
                    if (SendParticlesTo(state))
                    {
                        if (p == null)
                        {
                            p = new TargetParticleEffect(e, 0, 10, 5, 0, 0, 0x13a7, 3, 0);
                        }
                        state.Send(p);
                    }
                    if (packet2 == null)
                    {
                        packet2 = new BoltEffect(e, hue);
                    }
                    state.Send(packet2);
                    if (sound)
                    {
                        if (packet3 == null)
                        {
                            packet3 = new Server.Network.PlaySound(0x29, e);
                        }
                        state.Send(packet3);
                    }
                }
                clientsInRange.Free();
            }
        }

        public static void SendLocationEffect(IPoint3D p, Map map, int itemID, int duration)
        {
            SendLocationEffect(p, map, itemID, duration, 10, 0, 0);
        }

        public static void SendLocationEffect(IPoint3D p, Map map, int itemID, int duration, int speed)
        {
            SendLocationEffect(p, map, itemID, duration, speed, 0, 0);
        }

        public static void SendLocationEffect(IPoint3D p, Map map, int itemID, int duration, int hue, int renderMode)
        {
            SendLocationEffect(p, map, itemID, duration, 10, hue, renderMode);
        }

        public static void SendLocationEffect(IPoint3D p, Map map, int itemID, int duration, int speed, int hue, int renderMode)
        {
            SendPacket(p, map, new LocationEffect(p, itemID, speed, duration, hue, renderMode));
        }

        public static void SendLocationParticles(IEntity e, int itemID, int speed, int duration, int effect)
        {
            SendLocationParticles(e, itemID, speed, duration, 0, 0, effect, 0);
        }

        public static void SendLocationParticles(IEntity e, int itemID, int speed, int duration, int effect, int unknown)
        {
            SendLocationParticles(e, itemID, speed, duration, 0, 0, effect, unknown);
        }

        public static void SendLocationParticles(IEntity e, int itemID, int speed, int duration, int hue, int renderMode, int effect, int unknown)
        {
            Map map = e.Map;
            if (map != null)
            {
                Packet p = null;
                Packet packet2 = null;
                IPooledEnumerable clientsInRange = map.GetClientsInRange(e.Location);
                foreach (NetState state in clientsInRange)
                {
                    state.Mobile.ProcessDelta();
                    if (SendParticlesTo(state))
                    {
                        if (p == null)
                        {
                            p = new LocationParticleEffect(e, itemID, speed, duration, hue, renderMode, effect, unknown);
                        }
                        state.Send(p);
                        continue;
                    }
                    if (itemID != 0)
                    {
                        if (packet2 == null)
                        {
                            packet2 = new LocationEffect(e, itemID, speed, duration, hue, renderMode);
                        }
                        state.Send(packet2);
                    }
                }
                clientsInRange.Free();
            }
        }

        public static void SendMovingEffect(IEntity from, IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes)
        {
            SendMovingEffect(from, to, itemID, speed, duration, fixedDirection, explodes, 0, 0);
        }

        public static void SendMovingEffect(IEntity from, IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int hue, int renderMode)
        {
            if (from is Mobile)
            {
                ((Mobile) from).ProcessDelta();
            }
            if (to is Mobile)
            {
                ((Mobile) to).ProcessDelta();
            }
            SendPacket(from.Location, from.Map, new MovingEffect(from, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode));
        }

        public static void SendMovingParticles(IEntity from, IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int effect, int explodeEffect, int explodeSound)
        {
            SendMovingParticles(from, to, itemID, speed, duration, fixedDirection, explodes, 0, 0, effect, explodeEffect, explodeSound, 0);
        }

        public static void SendMovingParticles(IEntity from, IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int effect, int explodeEffect, int explodeSound, int unknown)
        {
            SendMovingParticles(from, to, itemID, speed, duration, fixedDirection, explodes, 0, 0, effect, explodeEffect, explodeSound, unknown);
        }

        public static void SendMovingParticles(IEntity from, IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int hue, int renderMode, int effect, int explodeEffect, int explodeSound, int unknown)
        {
            SendMovingParticles(from, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode, effect, explodeEffect, explodeSound, (EffectLayer) 0xff, unknown);
        }

        public static void SendMovingParticles(IEntity from, IEntity to, int itemID, int speed, int duration, bool fixedDirection, bool explodes, int hue, int renderMode, int effect, int explodeEffect, int explodeSound, EffectLayer layer, int unknown)
        {
            if (from is Mobile)
            {
                ((Mobile) from).ProcessDelta();
            }
            if (to is Mobile)
            {
                ((Mobile) to).ProcessDelta();
            }
            Map map = from.Map;
            if (map != null)
            {
                Packet p = null;
                Packet packet2 = null;
                IPooledEnumerable clientsInRange = map.GetClientsInRange(from.Location);
                foreach (NetState state in clientsInRange)
                {
                    state.Mobile.ProcessDelta();
                    if (SendParticlesTo(state))
                    {
                        if (p == null)
                        {
                            p = new MovingParticleEffect(from, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode, effect, explodeEffect, explodeSound, layer, unknown);
                        }
                        state.Send(p);
                        continue;
                    }
                    if (itemID > 1)
                    {
                        if (packet2 == null)
                        {
                            packet2 = new MovingEffect(from, to, itemID, speed, duration, fixedDirection, explodes, hue, renderMode);
                        }
                        state.Send(packet2);
                    }
                }
                clientsInRange.Free();
            }
        }

        public static void SendPacket(IPoint3D origin, Map map, Packet p)
        {
            if (map != null)
            {
                IPooledEnumerable clientsInRange = map.GetClientsInRange(new Point3D(origin));
                foreach (NetState state in clientsInRange)
                {
                    state.Mobile.ProcessDelta();
                    state.Send(p);
                }
                clientsInRange.Free();
            }
        }

        public static void SendPacket(Point3D origin, Map map, Packet p)
        {
            if (map != null)
            {
                IPooledEnumerable clientsInRange = map.GetClientsInRange(origin);
                foreach (NetState state in clientsInRange)
                {
                    state.Mobile.ProcessDelta();
                    state.Send(p);
                }
                clientsInRange.Free();
            }
        }

        public static bool SendParticlesTo(NetState state)
        {
            return ((m_ParticleSupportType == Server.ParticleSupportType.Full) || (((m_ParticleSupportType == Server.ParticleSupportType.Detect) && (state.Version != null)) && (state.Version.Type == ClientType.UOTD)));
        }

        public static void SendTargetEffect(IEntity target, int itemID, int duration)
        {
            SendTargetEffect(target, itemID, duration, 0, 0);
        }

        public static void SendTargetEffect(IEntity target, int itemID, int speed, int duration)
        {
            SendTargetEffect(target, itemID, speed, duration, 0, 0);
        }

        public static void SendTargetEffect(IEntity target, int itemID, int duration, int hue, int renderMode)
        {
            SendTargetEffect(target, itemID, 10, duration, hue, renderMode);
        }

        public static void SendTargetEffect(IEntity target, int itemID, int speed, int duration, int hue, int renderMode)
        {
            if (target is Mobile)
            {
                ((Mobile) target).ProcessDelta();
            }
            SendPacket(target.Location, target.Map, new TargetEffect(target, itemID, speed, duration, hue, renderMode));
        }

        public static void SendTargetParticles(IEntity target, int itemID, int speed, int duration, int effect, EffectLayer layer)
        {
            SendTargetParticles(target, itemID, speed, duration, 0, 0, effect, layer, 0);
        }

        public static void SendTargetParticles(IEntity target, int itemID, int speed, int duration, int effect, EffectLayer layer, int unknown)
        {
            SendTargetParticles(target, itemID, speed, duration, 0, 0, effect, layer, unknown);
        }

        public static void SendTargetParticles(IEntity target, int itemID, int speed, int duration, int hue, int renderMode, int effect, EffectLayer layer, int unknown)
        {
            if (target is Mobile)
            {
                ((Mobile) target).ProcessDelta();
            }
            Map map = target.Map;
            if (map != null)
            {
                Packet p = null;
                Packet packet2 = null;
                IPooledEnumerable clientsInRange = map.GetClientsInRange(target.Location);
                foreach (NetState state in clientsInRange)
                {
                    state.Mobile.ProcessDelta();
                    if (SendParticlesTo(state))
                    {
                        if (p == null)
                        {
                            p = new TargetParticleEffect(target, itemID, speed, duration, hue, renderMode, effect, (int) layer, unknown);
                        }
                        state.Send(p);
                        continue;
                    }
                    if (itemID != 0)
                    {
                        if (packet2 == null)
                        {
                            packet2 = new TargetEffect(target, itemID, speed, duration, hue, renderMode);
                        }
                        state.Send(packet2);
                    }
                }
                clientsInRange.Free();
            }
        }

        public static Server.ParticleSupportType ParticleSupportType
        {
            get
            {
                return m_ParticleSupportType;
            }
            set
            {
                m_ParticleSupportType = value;
            }
        }
    }
}

