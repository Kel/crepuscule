﻿namespace Server
{
    using System;

    public enum ResistanceType
    {
        Physical,
        Fire,
        Cold,
        Poison,
        Energy
    }
}

