﻿namespace Server.Prompts
{
    using Server;
    using System;

    public abstract class Prompt
    {
        private int m_Serial;
        private static int m_Serials;

        public Prompt()
        {
            do
            {
                this.m_Serial = ++m_Serials;
            }
            while (this.m_Serial == 0);
        }

        public virtual void OnCancel(Mobile from)
        {
        }

        public virtual void OnResponse(Mobile from, string text)
        {
        }

        public int Serial
        {
            get
            {
                return this.m_Serial;
            }
        }
    }
}

