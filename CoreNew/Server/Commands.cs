﻿namespace Server
{
    using System;
    using System.Collections;

    public delegate void CommandEventHandler(CommandEventArgs e);

    public class CommandEventArgs : EventArgs
    {
        private Mobile m_Mobile;
        private string m_Command, m_ArgString;
        private string[] m_Arguments;

        public Mobile Mobile
        {
            get
            {
                return m_Mobile;
            }
        }

        public string Command
        {
            get
            {
                return m_Command;
            }
        }

        public string ArgString
        {
            get
            {
                return m_ArgString;
            }
        }

        public string[] Arguments
        {
            get
            {
                return m_Arguments;
            }
        }

        public int Length
        {
            get
            {
                return m_Arguments.Length;
            }
        }

        public string GetString(int index)
        {
            if (index < 0 || index >= m_Arguments.Length)
                return "";

            return m_Arguments[index];
        }

        public int GetInt32(int index)
        {
            if (index < 0 || index >= m_Arguments.Length)
                return 0;

            return Utility.ToInt32(m_Arguments[index]);
        }

        public bool GetBoolean(int index)
        {
            if (index < 0 || index >= m_Arguments.Length)
                return false;

            return Utility.ToBoolean(m_Arguments[index]);
        }

        public double GetDouble(int index)
        {
            if (index < 0 || index >= m_Arguments.Length)
                return 0.0;

            return Utility.ToDouble(m_Arguments[index]);
        }

        public TimeSpan GetTimeSpan(int index)
        {
            if (index < 0 || index >= m_Arguments.Length)
                return TimeSpan.Zero;

            return Utility.ToTimeSpan(m_Arguments[index]);
        }

        public CommandEventArgs(Mobile mobile, string command, string argString, string[] arguments)
        {
            m_Mobile = mobile;
            m_Command = command;
            m_ArgString = argString;
            m_Arguments = arguments;
        }
    }


    public class Commands
    {
        private static AccessLevel m_BadCommandIngoreLevel = AccessLevel.Player;
        private static string m_CommandPrefix = ".";
        private static Hashtable m_Entries = new Hashtable(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);

        public static bool Handle(Mobile from, string text)
        {
            string str;
            string[] strArray;
            string str2;
            if (!text.StartsWith(m_CommandPrefix))
            {
                return false;
            }
            text = text.Substring(m_CommandPrefix.Length);
            int index = text.IndexOf(' ');
            if (index >= 0)
            {
                str2 = text.Substring(index + 1);
                str = text.Substring(0, index);
                strArray = Split(str2);
            }
            else
            {
                str2 = "";
                str = text.ToLower();
                strArray = new string[0];
            }
            CommandEntry entry = (CommandEntry) m_Entries[str];
            if (entry != null)
            {
                if (from.AccessLevel >= entry.AccessLevel)
                {
                    if (entry.Handler != null)
                    {
                        CommandEventArgs e = new CommandEventArgs(from, str, str2, strArray);
                        entry.Handler(e);
                        EventSink.InvokeCommand(e);
                    }
                }
                else
                {
                    /*if (from.AccessLevel <= m_BadCommandIngoreLevel)
                    {
                        return false;
                    }*/
                    from.SendMessage("Ce n'est pas une commande valide.");
                }
            }
            else
            {
                /*if (from.AccessLevel <= m_BadCommandIngoreLevel)
                {
                    return false;
                }*/
                from.SendMessage("Ce n'est pas une commande valide.");
            }
            return true;
        }

        public static void Register(string command, AccessLevel access, CommandEventHandler handler)
        {
            m_Entries[command] = new CommandEntry(command, handler, access);
        }

        public static string[] Split(string value)
        {
            char[] chArray = value.ToCharArray();
            ArrayList list = new ArrayList();
            int index = 0;
            int num2 = 0;
            while (index < chArray.Length)
            {
                char ch = chArray[index];
                if (ch == '"')
                {
                    index++;
                    num2 = index;
                    while (num2 < chArray.Length)
                    {
                        if ((chArray[num2] == '"') && (chArray[num2 - 1] != '\\'))
                        {
                            break;
                        }
                        num2++;
                    }
                    list.Add(value.Substring(index, num2 - index));
                    index = num2 + 2;
                    continue;
                }
                if (ch != ' ')
                {
                    num2 = index;
                    while (num2 < chArray.Length)
                    {
                        if (chArray[num2] == ' ')
                        {
                            break;
                        }
                        num2++;
                    }
                    list.Add(value.Substring(index, num2 - index));
                    index = num2 + 1;
                    continue;
                }
                index++;
            }
            return (string[]) list.ToArray(typeof(string));
        }

        public static AccessLevel BadCommandIgnoreLevel
        {
            get
            {
                return m_BadCommandIngoreLevel;
            }
            set
            {
                m_BadCommandIngoreLevel = value;
            }
        }

        public static string CommandPrefix
        {
            get
            {
                return m_CommandPrefix;
            }
            set
            {
                m_CommandPrefix = value;
            }
        }

        public static Hashtable Entries
        {
            get
            {
                return m_Entries;
            }
        }
    }
}

