﻿namespace Server
{
    using System;
    using System.Reflection;

    public class TypeCache
    {
        private TypeTable m_FullNames;
        private TypeTable m_Names;
        private Type[] m_Types;

        public TypeCache(Assembly asm)
        {
            if (asm == null)
            {
                this.m_Types = Type.EmptyTypes;
            }
            else
            {
                this.m_Types = asm.GetTypes();
            }
            this.m_Names = new TypeTable(this.m_Types.Length);
            this.m_FullNames = new TypeTable(this.m_Types.Length);
            Type attributeType = typeof(TypeAliasAttribute);
            for (int i = 0; i < this.m_Types.Length; i++)
            {
                Type type = this.m_Types[i];
                this.m_Names.Add(type.Name, type);
                this.m_FullNames.Add(type.FullName, type);
                if (type.IsDefined(attributeType, false))
                {
                    object[] customAttributes = type.GetCustomAttributes(attributeType, false);
                    if ((customAttributes != null) && (customAttributes.Length > 0))
                    {
                        TypeAliasAttribute attribute = customAttributes[0] as TypeAliasAttribute;
                        if (attribute != null)
                        {
                            for (int j = 0; j < attribute.Aliases.Length; j++)
                            {
                                this.m_FullNames.Add(attribute.Aliases[j], type);
                            }
                        }
                    }
                }
            }
        }

        public Type GetTypeByFullName(string fullName, bool ignoreCase)
        {
            return this.m_FullNames.Get(fullName, ignoreCase);
        }

        public Type GetTypeByName(string name, bool ignoreCase)
        {
            return this.m_Names.Get(name, ignoreCase);
        }

        public TypeTable FullNames
        {
            get
            {
                return this.m_FullNames;
            }
        }

        public TypeTable Names
        {
            get
            {
                return this.m_Names;
            }
        }

        public Type[] Types
        {
            get
            {
                return this.m_Types;
            }
        }
    }
}

