﻿namespace Server
{
    using System;

    [AttributeUsage(AttributeTargets.Method)]
    public class CallPriorityAttribute : Attribute
    {
        private int m_Priority;

        public CallPriorityAttribute(int priority)
        {
            this.m_Priority = priority;
        }

        public int Priority
        {
            get
            {
                return this.m_Priority;
            }
            set
            {
                this.m_Priority = value;
            }
        }
    }
}

