﻿namespace Server
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Net;

    public class Firewall
    {
        private static ArrayList m_Blocked = new ArrayList();

        static Firewall()
        {
            string path = "firewall.cfg";
            if (System.IO.File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string str2;
                    while ((str2 = reader.ReadLine()) != null)
                    {
                        str2 = str2.Trim();
                        if (str2.Length != 0)
                        {
                            object obj2;
                            try
                            {
                                obj2 = IPAddress.Parse(str2);
                            }
                            catch
                            {
                                obj2 = str2;
                            }
                            m_Blocked.Add(obj2.ToString());
                        }
                    }
                }
            }
        }

        public static void Add(IPAddress ip)
        {
            if (!m_Blocked.Contains(ip))
            {
                m_Blocked.Add(ip);
            }
            Save();
        }

        public static void Add(object obj)
        {
            if ((obj is IPAddress) || (obj is string))
            {
                if (!m_Blocked.Contains(obj))
                {
                    m_Blocked.Add(obj);
                }
                Save();
            }
        }

        public static void Add(string pattern)
        {
            if (!m_Blocked.Contains(pattern))
            {
                m_Blocked.Add(pattern);
            }
            Save();
        }

        public static bool IsBlocked(IPAddress ip)
        {
            bool flag = false;
            for (int i = 0; !flag && (i < m_Blocked.Count); i++)
            {
                if (m_Blocked[i] is IPAddress)
                {
                    flag = ip.Equals(m_Blocked[i]);
                }
                else if (m_Blocked[i] is string)
                {
                    flag = Utility.IPMatch((string) m_Blocked[i], ip);
                }
            }
            return flag;
        }

        public static void Remove(IPAddress ip)
        {
            m_Blocked.Remove(ip);
            Save();
        }

        public static void Remove(string pattern)
        {
            m_Blocked.Remove(pattern);
            Save();
        }

        public static void RemoveAt(int index)
        {
            m_Blocked.RemoveAt(index);
            Save();
        }

        public static void Save()
        {
            string path = "firewall.cfg";
            using (StreamWriter writer = new StreamWriter(path))
            {
                for (int i = 0; i < m_Blocked.Count; i++)
                {
                    writer.WriteLine(m_Blocked[i]);
                }
            }
        }

        public static ArrayList List
        {
            get
            {
                return m_Blocked;
            }
        }
    }
}

