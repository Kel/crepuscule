﻿namespace Server
{
    using System;
    using System.IO;

    public sealed class MultiComponentList
    {
        public static readonly MultiComponentList Empty = new MultiComponentList();
        private Point2D m_Center;
        private int m_Height;
        private MultiTileEntry[] m_List;
        private Point2D m_Max;
        private Point2D m_Min;
        private Tile[][][] m_Tiles;
        private int m_Width;

        private MultiComponentList()
        {
            this.m_Tiles = new Tile[0][][];
            this.m_List = new MultiTileEntry[0];
        }

        public MultiComponentList(GenericReader reader)
        {
            if (reader.ReadInt() == 0)
            {
                this.m_Min = reader.ReadPoint2D();
                this.m_Max = reader.ReadPoint2D();
                this.m_Center = reader.ReadPoint2D();
                this.m_Width = reader.ReadInt();
                this.m_Height = reader.ReadInt();
                int num2 = reader.ReadInt();
                MultiTileEntry[] entryArray = this.m_List = new MultiTileEntry[num2];
                for (int i = 0; i < num2; i++)
                {
                    entryArray[i].m_ItemID = reader.ReadShort();
                    entryArray[i].m_OffsetX = reader.ReadShort();
                    entryArray[i].m_OffsetY = reader.ReadShort();
                    entryArray[i].m_OffsetZ = reader.ReadShort();
                    entryArray[i].m_Flags = reader.ReadInt();
                }
                TileList[][] listArray = new TileList[this.m_Width][];
                this.m_Tiles = new Tile[this.m_Width][][];
                for (int j = 0; j < this.m_Width; j++)
                {
                    listArray[j] = new TileList[this.m_Height];
                    this.m_Tiles[j] = new Tile[this.m_Height][];
                    for (int n = 0; n < this.m_Height; n++)
                    {
                        listArray[j][n] = new TileList();
                    }
                }
                for (int k = 0; k < entryArray.Length; k++)
                {
                    if ((k == 0) || (entryArray[k].m_Flags != 0))
                    {
                        int index = entryArray[k].m_OffsetX + this.m_Center.m_X;
                        int num8 = entryArray[k].m_OffsetY + this.m_Center.m_Y;
                        listArray[index][num8].Add((short) ((entryArray[k].m_ItemID & 0x3fff) | 0x4000), (sbyte) entryArray[k].m_OffsetZ);
                    }
                }
                for (int m = 0; m < this.m_Width; m++)
                {
                    for (int num10 = 0; num10 < this.m_Height; num10++)
                    {
                        this.m_Tiles[m][num10] = listArray[m][num10].ToArray();
                    }
                }
            }
        }

        public MultiComponentList(MultiComponentList toCopy)
        {
            this.m_Min = toCopy.m_Min;
            this.m_Max = toCopy.m_Max;
            this.m_Center = toCopy.m_Center;
            this.m_Width = toCopy.m_Width;
            this.m_Height = toCopy.m_Height;
            this.m_Tiles = new Tile[this.m_Width][][];
            for (int i = 0; i < this.m_Width; i++)
            {
                this.m_Tiles[i] = new Tile[this.m_Height][];
                for (int k = 0; k < this.m_Height; k++)
                {
                    this.m_Tiles[i][k] = new Tile[toCopy.m_Tiles[i][k].Length];
                    for (int m = 0; m < this.m_Tiles[i][k].Length; m++)
                    {
                        this.m_Tiles[i][k][m] = toCopy.m_Tiles[i][k][m];
                    }
                }
            }
            this.m_List = new MultiTileEntry[toCopy.m_List.Length];
            for (int j = 0; j < this.m_List.Length; j++)
            {
                this.m_List[j] = toCopy.m_List[j];
            }
        }

        public MultiComponentList(BinaryReader reader, int count)
        {
            MultiTileEntry[] entryArray = this.m_List = new MultiTileEntry[count];
            for (int i = 0; i < count; i++)
            {
                entryArray[i].m_ItemID = reader.ReadInt16();
                entryArray[i].m_OffsetX = reader.ReadInt16();
                entryArray[i].m_OffsetY = reader.ReadInt16();
                entryArray[i].m_OffsetZ = reader.ReadInt16();
                entryArray[i].m_Flags = reader.ReadInt32();
                MultiTileEntry entry = entryArray[i];
                if ((i == 0) || (entry.m_Flags != 0))
                {
                    if (entry.m_OffsetX < this.m_Min.m_X)
                    {
                        this.m_Min.m_X = entry.m_OffsetX;
                    }
                    if (entry.m_OffsetY < this.m_Min.m_Y)
                    {
                        this.m_Min.m_Y = entry.m_OffsetY;
                    }
                    if (entry.m_OffsetX > this.m_Max.m_X)
                    {
                        this.m_Max.m_X = entry.m_OffsetX;
                    }
                    if (entry.m_OffsetY > this.m_Max.m_Y)
                    {
                        this.m_Max.m_Y = entry.m_OffsetY;
                    }
                }
            }
            this.m_Center = new Point2D(-this.m_Min.m_X, -this.m_Min.m_Y);
            this.m_Width = (this.m_Max.m_X - this.m_Min.m_X) + 1;
            this.m_Height = (this.m_Max.m_Y - this.m_Min.m_Y) + 1;
            TileList[][] listArray = new TileList[this.m_Width][];
            this.m_Tiles = new Tile[this.m_Width][][];
            for (int j = 0; j < this.m_Width; j++)
            {
                listArray[j] = new TileList[this.m_Height];
                this.m_Tiles[j] = new Tile[this.m_Height][];
                for (int n = 0; n < this.m_Height; n++)
                {
                    listArray[j][n] = new TileList();
                }
            }
            for (int k = 0; k < entryArray.Length; k++)
            {
                if ((k == 0) || (entryArray[k].m_Flags != 0))
                {
                    int index = entryArray[k].m_OffsetX + this.m_Center.m_X;
                    int num6 = entryArray[k].m_OffsetY + this.m_Center.m_Y;
                    listArray[index][num6].Add((short) ((entryArray[k].m_ItemID & 0x3fff) | 0x4000), (sbyte) entryArray[k].m_OffsetZ);
                }
            }
            for (int m = 0; m < this.m_Width; m++)
            {
                for (int num8 = 0; num8 < this.m_Height; num8++)
                {
                    this.m_Tiles[m][num8] = listArray[m][num8].ToArray();
                }
            }
        }

        public void Add(int itemID, int x, int y, int z)
        {
            itemID &= 0x3fff;
            itemID |= 0x4000;
            int index = x + this.m_Center.m_X;
            int num2 = y + this.m_Center.m_Y;
            if (((index >= 0) && (index < this.m_Width)) && ((num2 >= 0) && (num2 < this.m_Height)))
            {
                Tile[] tileArray = this.m_Tiles[index][num2];
                for (int i = tileArray.Length - 1; i >= 0; i--)
                {
                    if ((tileArray[i].Z == z) && ((tileArray[i].Height > 0) == (TileData.ItemTable[itemID & 0x3fff].Height > 0)))
                    {
                        this.Remove(tileArray[i].ID, x, y, z);
                    }
                }
                tileArray = this.m_Tiles[index][num2];
                Tile[] tileArray2 = new Tile[tileArray.Length + 1];
                for (int j = 0; j < tileArray.Length; j++)
                {
                    tileArray2[j] = tileArray[j];
                }
                tileArray2[tileArray.Length] = new Tile((short) itemID, (sbyte) z);
                this.m_Tiles[index][num2] = tileArray2;
                MultiTileEntry[] list = this.m_List;
                MultiTileEntry[] entryArray2 = new MultiTileEntry[list.Length + 1];
                for (int k = 0; k < list.Length; k++)
                {
                    entryArray2[k] = list[k];
                }
                entryArray2[list.Length] = new MultiTileEntry((short) itemID, (short) x, (short) y, (short) z, 1);
                this.m_List = entryArray2;
                if (x < this.m_Min.m_X)
                {
                    this.m_Min.m_X = x;
                }
                if (y < this.m_Min.m_Y)
                {
                    this.m_Min.m_Y = y;
                }
                if (x > this.m_Max.m_X)
                {
                    this.m_Max.m_X = x;
                }
                if (y > this.m_Max.m_Y)
                {
                    this.m_Max.m_Y = y;
                }
            }
        }

        public void Remove(int itemID, int x, int y, int z)
        {
            int index = x + this.m_Center.m_X;
            int num2 = y + this.m_Center.m_Y;
            if (((index >= 0) && (index < this.m_Width)) && ((num2 >= 0) && (num2 < this.m_Height)))
            {
                Tile[] tileArray = this.m_Tiles[index][num2];
                for (int i = 0; i < tileArray.Length; i++)
                {
                    Tile tile = tileArray[i];
                    if (((tile.ID & 0x3fff) == (itemID & 0x3fff)) && (tile.Z == z))
                    {
                        Tile[] tileArray2 = new Tile[tileArray.Length - 1];
                        for (int k = 0; k < i; k++)
                        {
                            tileArray2[k] = tileArray[k];
                        }
                        for (int m = i + 1; m < tileArray.Length; m++)
                        {
                            tileArray2[m - 1] = tileArray[m];
                        }
                        this.m_Tiles[index][num2] = tileArray2;
                        break;
                    }
                }
                MultiTileEntry[] list = this.m_List;
                for (int j = 0; j < list.Length; j++)
                {
                    MultiTileEntry entry = list[j];
                    if ((((entry.m_ItemID & 0x3fff) == ((short) (itemID & 0x3fff))) && (entry.m_OffsetX == ((short) x))) && ((entry.m_OffsetY == ((short) y)) && (entry.m_OffsetZ == ((short) z))))
                    {
                        MultiTileEntry[] entryArray2 = new MultiTileEntry[list.Length - 1];
                        for (int n = 0; n < j; n++)
                        {
                            entryArray2[n] = list[n];
                        }
                        for (int num8 = j + 1; num8 < list.Length; num8++)
                        {
                            entryArray2[num8 - 1] = list[num8];
                        }
                        this.m_List = entryArray2;
                        return;
                    }
                }
            }
        }

        public void RemoveXYZH(int x, int y, int z, int minHeight)
        {
            int index = x + this.m_Center.m_X;
            int num2 = y + this.m_Center.m_Y;
            if (((index >= 0) && (index < this.m_Width)) && ((num2 >= 0) && (num2 < this.m_Height)))
            {
                Tile[] tileArray = this.m_Tiles[index][num2];
                for (int i = 0; i < tileArray.Length; i++)
                {
                    Tile tile = tileArray[i];
                    if ((tile.Z == z) && (tile.Height >= minHeight))
                    {
                        Tile[] tileArray2 = new Tile[tileArray.Length - 1];
                        for (int k = 0; k < i; k++)
                        {
                            tileArray2[k] = tileArray[k];
                        }
                        for (int m = i + 1; m < tileArray.Length; m++)
                        {
                            tileArray2[m - 1] = tileArray[m];
                        }
                        this.m_Tiles[index][num2] = tileArray2;
                        break;
                    }
                }
                MultiTileEntry[] list = this.m_List;
                for (int j = 0; j < list.Length; j++)
                {
                    MultiTileEntry entry = list[j];
                    if (((entry.m_OffsetX == ((short) x)) && (entry.m_OffsetY == ((short) y))) && ((entry.m_OffsetZ == ((short) z)) && (TileData.ItemTable[entry.m_ItemID & 0x3fff].Height >= minHeight)))
                    {
                        MultiTileEntry[] entryArray2 = new MultiTileEntry[list.Length - 1];
                        for (int n = 0; n < j; n++)
                        {
                            entryArray2[n] = list[n];
                        }
                        for (int num8 = j + 1; num8 < list.Length; num8++)
                        {
                            entryArray2[num8 - 1] = list[num8];
                        }
                        this.m_List = entryArray2;
                        return;
                    }
                }
            }
        }

        public void Resize(int newWidth, int newHeight)
        {
            int width = this.m_Width;
            int height = this.m_Height;
            Tile[][][] tiles = this.m_Tiles;
            int num3 = 0;
            Tile[][][] tileArray2 = new Tile[newWidth][][];
            for (int i = 0; i < newWidth; i++)
            {
                tileArray2[i] = new Tile[newHeight][];
                for (int k = 0; k < newHeight; k++)
                {
                    if ((i < width) && (k < height))
                    {
                        tileArray2[i][k] = tiles[i][k];
                    }
                    else
                    {
                        tileArray2[i][k] = new Tile[0];
                    }
                    num3 += tileArray2[i][k].Length;
                }
            }
            this.m_Tiles = tileArray2;
            this.m_List = new MultiTileEntry[num3];
            this.m_Width = newWidth;
            this.m_Height = newHeight;
            this.m_Min = Point2D.Zero;
            this.m_Max = Point2D.Zero;
            int num6 = 0;
            for (int j = 0; j < newWidth; j++)
            {
                for (int m = 0; m < newHeight; m++)
                {
                    foreach (Tile tile in tileArray2[j][m])
                    {
                        int num10 = j - this.m_Center.X;
                        int num11 = m - this.m_Center.Y;
                        if (num10 < this.m_Min.m_X)
                        {
                            this.m_Min.m_X = num10;
                        }
                        if (num11 < this.m_Min.m_Y)
                        {
                            this.m_Min.m_Y = num11;
                        }
                        if (num10 > this.m_Max.m_X)
                        {
                            this.m_Max.m_X = num10;
                        }
                        if (num11 > this.m_Max.m_Y)
                        {
                            this.m_Max.m_Y = num11;
                        }
                        this.m_List[num6++] = new MultiTileEntry((short) tile.ID, (short) num10, (short) num11, (short) tile.Z, 1);
                    }
                }
            }
        }

        public void Serialize(GenericWriter writer)
        {
            writer.Write(0);
            writer.Write(this.m_Min);
            writer.Write(this.m_Max);
            writer.Write(this.m_Center);
            writer.Write(this.m_Width);
            writer.Write(this.m_Height);
            writer.Write(this.m_List.Length);
            for (int i = 0; i < this.m_List.Length; i++)
            {
                MultiTileEntry entry = this.m_List[i];
                writer.Write(entry.m_ItemID);
                writer.Write(entry.m_OffsetX);
                writer.Write(entry.m_OffsetY);
                writer.Write(entry.m_OffsetZ);
                writer.Write(entry.m_Flags);
            }
        }

        public Point2D Center
        {
            get
            {
                return this.m_Center;
            }
        }

        public int Height
        {
            get
            {
                return this.m_Height;
            }
        }

        public MultiTileEntry[] List
        {
            get
            {
                return this.m_List;
            }
        }

        public Point2D Max
        {
            get
            {
                return this.m_Max;
            }
        }

        public Point2D Min
        {
            get
            {
                return this.m_Min;
            }
        }

        public Tile[][][] Tiles
        {
            get
            {
                return this.m_Tiles;
            }
        }

        public int Width
        {
            get
            {
                return this.m_Width;
            }
        }
    }
}

