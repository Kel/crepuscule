﻿namespace Server
{
    using System;
    using System.IO;

    public class MultiData
    {
        private static MultiComponentList[] m_Components;
        private static FileStream m_Index;
        private static BinaryReader m_IndexReader;
        private static FileStream m_Stream;
        private static BinaryReader m_StreamReader;

        static MultiData()
        {
            string path = Core.FindDataFile("Multi.idx");
            string str2 = Core.FindDataFile("Multi.mul");
            if (File.Exists(path) && File.Exists(str2))
            {
                m_Index = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                m_IndexReader = new BinaryReader(m_Index);
                m_Stream = new FileStream(str2, FileMode.Open, FileAccess.Read, FileShare.Read);
                m_StreamReader = new BinaryReader(m_Stream);
                m_Components = new MultiComponentList[(int) (m_Index.Length / 12L)];
                string str3 = Core.FindDataFile("Verdata.mul");
                if (!File.Exists(str3))
                {
                    return;
                }
                using (FileStream stream = new FileStream(str3, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader reader = new BinaryReader(stream);
                    int num = reader.ReadInt32();
                    for (int i = 0; i < num; i++)
                    {
                        int num3 = reader.ReadInt32();
                        int index = reader.ReadInt32();
                        int num5 = reader.ReadInt32();
                        int num6 = reader.ReadInt32();
                        reader.ReadInt32();
                        if ((((num3 == 14) && (index >= 0)) && ((index < m_Components.Length) && (num5 >= 0))) && (num6 > 0))
                        {
                            reader.BaseStream.Seek((long) num5, SeekOrigin.Begin);
                            m_Components[index] = new MultiComponentList(reader, num6 / 12);
                            reader.BaseStream.Seek((long) (0x18 + (i * 20)), SeekOrigin.Begin);
                        }
                    }
                    return;
                }
            }
            Console.WriteLine("Warning: Multi data files not found");
            m_Components = new MultiComponentList[0];
        }

        public static MultiComponentList GetComponents(int multiID)
        {
            multiID &= 0x3fff;
            if ((multiID >= 0) && (multiID < m_Components.Length))
            {
                MultiComponentList list = m_Components[multiID];
                if (list == null)
                {
                    m_Components[multiID] = list = Load(multiID);
                }
                return list;
            }
            return MultiComponentList.Empty;
        }

        public static MultiComponentList Load(int multiID)
        {
            try
            {
                m_IndexReader.BaseStream.Seek((long) (multiID * 12), SeekOrigin.Begin);
                int num = m_IndexReader.ReadInt32();
                int num2 = m_IndexReader.ReadInt32();
                if ((num < 0) || (num2 <= 0))
                {
                    return MultiComponentList.Empty;
                }
                m_StreamReader.BaseStream.Seek((long) num, SeekOrigin.Begin);
                return new MultiComponentList(m_StreamReader, num2 / 12);
            }
            catch
            {
                return MultiComponentList.Empty;
            }
        }
    }
}

