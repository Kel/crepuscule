﻿namespace Server
{
    using System;

    public class KeywordList
    {
        private int m_Count = 0;
        private static int[] m_EmptyInts = new int[0];
        private int[] m_Keywords = new int[8];

        public void Add(int keyword)
        {
            if ((this.m_Count + 1) > this.m_Keywords.Length)
            {
                int[] keywords = this.m_Keywords;
                this.m_Keywords = new int[keywords.Length * 2];
                for (int i = 0; i < keywords.Length; i++)
                {
                    this.m_Keywords[i] = keywords[i];
                }
            }
            this.m_Keywords[this.m_Count++] = keyword;
        }

        public bool Contains(int keyword)
        {
            bool flag = false;
            for (int i = 0; !flag && (i < this.m_Count); i++)
            {
                flag = keyword == this.m_Keywords[i];
            }
            return flag;
        }

        public int[] ToArray()
        {
            if (this.m_Count == 0)
            {
                return m_EmptyInts;
            }
            int[] numArray = new int[this.m_Count];
            for (int i = 0; i < this.m_Count; i++)
            {
                numArray[i] = this.m_Keywords[i];
            }
            this.m_Count = 0;
            return numArray;
        }

        public int Count
        {
            get
            {
                return this.m_Count;
            }
        }
    }
}

