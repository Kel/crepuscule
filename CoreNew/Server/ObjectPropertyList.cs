﻿namespace Server
{
    using Server.Network;
    using System;
    using System.IO;
    using System.Text;

    public class ObjectPropertyList : Packet
    {
        private static byte[] m_Buffer = new byte[0x400];
        private static bool m_Enabled = false;
        private static Encoding m_Encoding = Encoding.Unicode;
        private IEntity m_Entity;
        private int m_Hash;
        private int m_Header;
        private string m_HeaderArgs;

        public ObjectPropertyList(IEntity e) : base(0xd6)
        {
            base.EnsureCapacity(0x80);
            this.m_Entity = e;
            base.m_Stream.Write((short) 1);
            base.m_Stream.Write((int) e.Serial);
            base.m_Stream.Write((byte) 0);
            base.m_Stream.Write((byte) 0);
            base.m_Stream.Write((int) e.Serial);
        }

        public void Add(int number)
        {
            if (number != 0)
            {
                this.AddHash(number);
                if (this.m_Header == 0)
                {
                    this.m_Header = number;
                    this.m_HeaderArgs = "";
                }
                base.m_Stream.Write(number);
                base.m_Stream.Write((short) 0);
            }
        }

        public void Add(string text)
        {
            this.Add(0xfea1b, text);
        }

        public void Add(int number, string arguments)
        {
            if (number != 0)
            {
                if (arguments == null)
                {
                    arguments = "";
                }
                if (this.m_Header == 0)
                {
                    this.m_Header = number;
                    this.m_HeaderArgs = arguments;
                }
                this.AddHash(number);
                this.AddHash(arguments.GetHashCode());
                base.m_Stream.Write(number);
                int byteCount = m_Encoding.GetByteCount(arguments);
                if (byteCount > m_Buffer.Length)
                {
                    m_Buffer = new byte[byteCount];
                }
                byteCount = m_Encoding.GetBytes(arguments, 0, arguments.Length, m_Buffer, 0);
                base.m_Stream.Write((short) byteCount);
                base.m_Stream.Write(m_Buffer, 0, byteCount);
            }
        }

        public void Add(string format, params object[] args)
        {
            this.Add(0xfea1b, string.Format(format, args));
        }

        public void Add(string format, string arg0)
        {
            this.Add(0xfea1b, string.Format(format, arg0));
        }

        public void Add(int number, string format, object arg0)
        {
            this.Add(number, string.Format(format, arg0));
        }

        public void Add(int number, string format, params object[] args)
        {
            this.Add(number, string.Format(format, args));
        }

        public void Add(string format, string arg0, string arg1)
        {
            this.Add(0xfea1b, string.Format(format, arg0, arg1));
        }

        public void Add(int number, string format, object arg0, object arg1)
        {
            this.Add(number, string.Format(format, arg0, arg1));
        }

        public void Add(string format, string arg0, string arg1, string arg2)
        {
            this.Add(0xfea1b, string.Format(format, arg0, arg1, arg2));
        }

        public void Add(int number, string format, object arg0, object arg1, object arg2)
        {
            this.Add(number, string.Format(format, arg0, arg1, arg2));
        }

        public void AddHash(int val)
        {
            this.m_Hash ^= val & 0x3ffffff;
            this.m_Hash ^= (val >> 0x1a) & 0x3f;
        }

        public void Terminate()
        {
            base.m_Stream.Write(0);
            base.m_Stream.Seek(11L, SeekOrigin.Begin);
            base.m_Stream.Write(this.m_Hash);
        }

        public static bool Enabled
        {
            get
            {
                return m_Enabled;
            }
            set
            {
                m_Enabled = value;
            }
        }

        public IEntity Entity
        {
            get
            {
                return this.m_Entity;
            }
        }

        public int Hash
        {
            get
            {
                return (0x40000000 + this.m_Hash);
            }
        }

        public int Header
        {
            get
            {
                return this.m_Header;
            }
            set
            {
                this.m_Header = value;
            }
        }

        public string HeaderArgs
        {
            get
            {
                return this.m_HeaderArgs;
            }
            set
            {
                this.m_HeaderArgs = value;
            }
        }
    }
}

