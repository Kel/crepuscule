﻿namespace Server
{
    using System;
    using System.Collections;

    public interface IPooledEnumerator : IEnumerator
    {
        void Free();

        IPooledEnumerable Enumerable { get; set; }
    }
}

