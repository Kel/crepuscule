﻿namespace Server
{
    using Microsoft.CSharp;
    using Microsoft.VisualBasic;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.IO;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Linq;

    public class ScriptCompiler
    {
        private static bool IsAdding = false;
        private static ArrayList m_AdditionalReferences = new ArrayList();
        private static Assembly[] m_Assemblies;
        private static TypeCache m_NullCache;
        private static Hashtable m_TypeCaches = new Hashtable();

        public static bool Compile()
        {
            return Compile(false);
        }

        public static void AddAssembly(Assembly assembly)
        {
            IsAdding = true;

            var replacement = m_Assemblies.ToList();
            replacement.Add(assembly);
            m_Assemblies = replacement.ToArray();

            IsAdding = false;
        }

        public static bool Compile(bool debug)
        {
            var NewEnumDll = "Server.Generated.newdll";
            var EnumDll = "Server.Generated.dll";

            if (File.Exists(NewEnumDll))
            {
                if (File.Exists(EnumDll))
                    File.Delete(EnumDll);
                File.Move(NewEnumDll, EnumDll);
            }

            EnsureDirectory("Scripts/");
            DeleteFilesInRoot("Scripts.CS*.dll");
            if (m_AdditionalReferences.Count > 0)
            {
                m_AdditionalReferences.Clear();
            }
#if DEBUG
            int index = 0;
            m_Assemblies = new Assembly[1];
            m_Assemblies[index++] = Assembly.GetAssembly(typeof(Server.Accounting.Account));
#else
            CompilerResults results = null;
            results = CompileCSScripts(debug);
            if ((results != null && results.Errors.HasErrors) || results == null)
            {
                return false;
            }
            int index = 0;
            m_Assemblies = new Assembly[1];
            if (results != null)
            {
                m_Assemblies[index++] = results.CompiledAssembly;
            }
#endif

            Console.Write("Scripts: Verifying...");
            Core.VerifySerialization();
            Console.WriteLine("done ({0} items, {1} mobiles)", Core.ScriptItems, Core.ScriptMobiles);
            ArrayList list = new ArrayList();
            for (index = 0; index < m_Assemblies.Length; index++)
            {
                Type[] types = m_Assemblies[index].GetTypes();
                for (int k = 0; k < types.Length; k++)
                {
                    MethodInfo method = types[k].GetMethod("Configure", BindingFlags.Public | BindingFlags.Static);
                    if (method != null)
                    {
                        list.Add(method);
                    }
                }
            }
            list.Sort(new CallPriorityComparer());
            for (int i = 0; i < list.Count; i++)
            {
                ((MethodInfo) list[i]).Invoke(null, null);
            }
            list.Clear();
            World.Load();
            for (index = 0; index < m_Assemblies.Length; index++)
            {
                Type[] typeArray2 = m_Assemblies[index].GetTypes();
                for (int m = 0; m < typeArray2.Length; m++)
                {
                    MethodInfo info2 = typeArray2[m].GetMethod("Initialize", BindingFlags.Public | BindingFlags.Static);
                    if (info2 != null)
                    {
                        list.Add(info2);
                    }
                }
            }
            list.Sort(new CallPriorityComparer());
            for (int j = 0; j < list.Count; j++)
            {
                ((MethodInfo) list[j]).Invoke(null, null);
            }
            return true;
        }

        private static CompilerResults CompileCSScripts()
        {
            return CompileCSScripts(false);
        }

        private static CompilerResults CompileCSScripts(bool debug)
        {
            //ICodeCompiler compiler = new CSharpCodeProvider().CreateCompiler();                       
            ICodeCompiler compiler = new CSharpCodeProvider (new Dictionary<string, string>() { { "CompilerVersion","v4.0" } }).CreateCompiler();
            CSharpCodeProvider csharp = compiler as CSharpCodeProvider;
            
            Console.Write("Scripts: Compiling C# scripts...");
            string[] scripts = GetScripts("*.cs");
            if (scripts.Length == 0)
            {
                Console.WriteLine("no files found.");
                return null;
            }
            string unusedPath = GetUnusedPath("Scripts.CS");

            CompilerParameters compilerParameters = new CompilerParameters(GetReferenceAssemblies(), unusedPath, debug);
            compilerParameters.CompilerOptions = "/optimize /unsafe";

            CompilerResults results = compiler.CompileAssemblyFromFileBatch(compilerParameters, scripts);
            m_AdditionalReferences.Add(unusedPath);
            if (results.Errors.Count > 0)
            {
                int num = 0;
                int num2 = 0;
                foreach (CompilerError error in results.Errors)
                {
                    if (error.IsWarning)
                    {
                        num2++;
                    }
                    else
                    {
                        num++;
                    }
                }
                if (num > 0)
                {
                    Console.WriteLine("failed ({0} errors, {1} warnings)", num, num2);
                }
                else
                {
                    Console.WriteLine("done ({0} errors, {1} warnings)", num, num2);
                }
                foreach (CompilerError error2 in results.Errors)
                {
                    Console.WriteLine(" - {0}: {1}: {2}: (line {3}, column {4}) {5}", new object[] { error2.IsWarning ? "Warning" : "Error", error2.FileName, error2.ErrorNumber, error2.Line, error2.Column, error2.ErrorText });
                }
                return results;
            }
            Console.WriteLine("done (0 errors, 0 warnings)");
            return results;
        }

        private static void DeleteFilesInRoot(string mask)
        {
            try
            {
                foreach (string str in Directory.GetFiles(Core.BaseDirectory, mask))
                {
                    try
                    {
                        File.Delete(str);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

        private static void DeleteFiles(string mask)
        {
            try
            {
                foreach (string str in Directory.GetFiles(Path.Combine(Core.BaseDirectory, "Scripts/Output"), mask))
                {
                    try
                    {
                        File.Delete(str);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

        private static void EnsureDirectory(string dir)
        {
            string path = Path.Combine(Core.BaseDirectory, dir);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static Type FindTypeByFullName(string fullName)
        {
            return FindTypeByFullName(fullName, true);
        }

        public static Type FindTypeByFullName(string fullName, bool ignoreCase)
        {
            if (IsAdding) return null;

            Type typeByFullName = null;
            for (int i = 0; (typeByFullName == null) && (i < m_Assemblies.Length); i++)
            {
                typeByFullName = GetTypeCache(m_Assemblies[i]).GetTypeByFullName(fullName, ignoreCase);
            }
            if (typeByFullName == null)
            {
                typeByFullName = GetTypeCache(Core.Assembly).GetTypeByFullName(fullName, ignoreCase);
            }
            return typeByFullName;
        }

        public static Type FindTypeByName(string name)
        {
            return FindTypeByName(name, true);
        }

        public static Type FindTypeByName(string name, bool ignoreCase)
        {
            if (IsAdding) return null;

            Type typeByName = null;
            for (int i = 0; (typeByName == null) && (i < m_Assemblies.Length); i++)
            {
                typeByName = GetTypeCache(m_Assemblies[i]).GetTypeByName(name, ignoreCase);
            }
            if (typeByName == null)
            {
                typeByName = GetTypeCache(Core.Assembly).GetTypeByName(name, ignoreCase);
            }
            return typeByName;
        }

        public static string[] GetReferenceAssemblies()
        {
            ArrayList list = new ArrayList();
            string path = Path.Combine(Core.BaseDirectory, "Data/Assemblies.cfg");
            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string str2;
                    while ((str2 = reader.ReadLine()) != null)
                    {
                        if ((str2.Length > 0) && !str2.StartsWith("#"))
                        {
                            list.Add(str2);
                        }
                    }
                }
            }
            list.Add(Core.ExePath);
            list.AddRange(m_AdditionalReferences);
            return (string[]) list.ToArray(typeof(string));
        }

        private static string[] GetScripts(string type)
        {
            ArrayList list = new ArrayList();
            GetScripts(list, Path.Combine(Core.BaseDirectory, "Scripts"), type);
            return (string[]) list.ToArray(typeof(string));
        }

        private static void GetScripts(ArrayList list, string path, string type)
        {
            foreach (string str in Directory.GetDirectories(path))
            {
                GetScripts(list, str, type);
            }
            list.AddRange(Directory.GetFiles(path, type));
        }

        public static TypeCache GetTypeCache(Assembly asm)
        {
            if (asm == null)
            {
                if (m_NullCache == null)
                {
                    m_NullCache = new TypeCache(null);
                }
                return m_NullCache;
            }
            TypeCache cache = (TypeCache) m_TypeCaches[asm];
            if (cache == null)
            {
                m_TypeCaches[asm] = cache = new TypeCache(asm);
            }
            return cache;
        }

        private static string GetUnusedPath(string name)
        {
            string path = Path.Combine(Core.BaseDirectory, string.Format("{0}.dll", name));
            for (int i = 2; File.Exists(path) && (i <= 0x3e8); i++)
            {
                path = Path.Combine(Core.BaseDirectory, string.Format("{0}.{1}.dll", name, i));
            }
            return path;
        }

        public static Assembly[] Assemblies
        {
            get
            {
                return m_Assemblies;
            }
            set
            {
                m_Assemblies = value;
            }
        }
    }
}

