﻿namespace Server
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class TypeAliasAttribute : Attribute
    {
        private string[] m_Aliases;

        public TypeAliasAttribute(params string[] aliases)
        {
            this.m_Aliases = aliases;
        }

        public string[] Aliases
        {
            get
            {
                return this.m_Aliases;
            }
        }
    }
}

