﻿namespace Server.Targeting
{
    using Server;
    using Server.Network;
    using System;

    public abstract class Target
    {
        private bool m_AllowGround;
        private bool m_AllowNonlocal;
        private bool m_CheckLOS;
        private bool m_DisallowMultis;
        private TargetFlags m_Flags;
        private static int m_NextTargetID;
        private int m_Range;
        private int m_TargetID = ++m_NextTargetID;
        private DateTime m_TimeoutTime;
        private Timer m_TimeoutTimer;

        public Target(int range, bool allowGround, TargetFlags flags)
        {
            this.m_Range = range;
            this.m_AllowGround = allowGround;
            this.m_Flags = flags;
            this.m_CheckLOS = true;
        }

        public void BeginTimeout(Mobile from, TimeSpan delay)
        {
            this.m_TimeoutTime = DateTime.Now + delay;
            if (this.m_TimeoutTimer != null)
            {
                this.m_TimeoutTimer.Stop();
            }
            this.m_TimeoutTimer = new TimeoutTimer(this, from, delay);
            this.m_TimeoutTimer.Start();
        }

        public static void Cancel(Mobile m)
        {
            NetState netState = m.NetState;
            if (netState != null)
            {
                netState.Send(CancelTarget.Instance);
            }
            Target target = m.Target;
            if (target != null)
            {
                target.OnTargetCancel(m, TargetCancelType.Canceled);
            }
        }

        public void Cancel(Mobile from, TargetCancelType type)
        {
            this.CancelTimeout();
            from.ClearTarget();
            this.OnTargetCancel(from, type);
            this.OnTargetFinish(from);
        }

        public void CancelTimeout()
        {
            if (this.m_TimeoutTimer != null)
            {
                this.m_TimeoutTimer.Stop();
            }
            this.m_TimeoutTimer = null;
        }

        public virtual Packet GetPacket()
        {
            return new TargetReq(this);
        }

        public void Invoke(Mobile from, object targeted)
        {
            this.CancelTimeout();
            from.ClearTarget();
            if (from.Deleted)
            {
                this.OnTargetCancel(from, TargetCancelType.Canceled);
                this.OnTargetFinish(from);
            }
            else
            {
                Point3D location;
                Map map;
                if (targeted is LandTarget)
                {
                    location = ((LandTarget) targeted).Location;
                    map = from.Map;
                }
                else if (targeted is StaticTarget)
                {
                    location = ((StaticTarget) targeted).Location;
                    map = from.Map;
                }
                else if (targeted is Mobile)
                {
                    if (((Mobile) targeted).Deleted)
                    {
                        this.OnTargetDeleted(from, targeted);
                        this.OnTargetFinish(from);
                        return;
                    }
                    if (!((Mobile) targeted).CanTarget)
                    {
                        this.OnTargetUntargetable(from, targeted);
                        this.OnTargetFinish(from);
                        return;
                    }
                    location = ((Mobile) targeted).Location;
                    map = ((Mobile) targeted).Map;
                }
                else if (targeted is Item)
                {
                    Item item = (Item) targeted;
                    if (item.Deleted)
                    {
                        this.OnTargetDeleted(from, targeted);
                        this.OnTargetFinish(from);
                        return;
                    }
                    if (!item.CanTarget)
                    {
                        this.OnTargetUntargetable(from, targeted);
                        this.OnTargetFinish(from);
                        return;
                    }
                    object rootParent = item.RootParent;
                    if ((!this.m_AllowNonlocal && (rootParent is Mobile)) && ((rootParent != from) && (from.AccessLevel == AccessLevel.Player)))
                    {
                        this.OnNonlocalTarget(from, targeted);
                        this.OnTargetFinish(from);
                        return;
                    }
                    location = item.GetWorldLocation();
                    map = item.Map;
                }
                else
                {
                    this.OnTargetCancel(from, TargetCancelType.Canceled);
                    this.OnTargetFinish(from);
                    return;
                }
                if (((map == null) || (map != from.Map)) || ((this.m_Range != -1) && !from.InRange(location, this.m_Range)))
                {
                    this.OnTargetOutOfRange(from, targeted);
                }
                else if (!from.CanSee(targeted))
                {
                    this.OnCantSeeTarget(from, targeted);
                }
                else if (this.m_CheckLOS && !from.InLOS(targeted))
                {
                    this.OnTargetOutOfLOS(from, targeted);
                }
                else if ((targeted is Item) && ((Item) targeted).InSecureTrade)
                {
                    this.OnTargetInSecureTrade(from, targeted);
                }
                else if ((targeted is Item) && !((Item) targeted).IsAccessibleTo(from))
                {
                    this.OnTargetNotAccessible(from, targeted);
                }
                else if ((targeted is Item) && !((Item) targeted).CheckTarget(from, this, targeted))
                {
                    this.OnTargetUntargetable(from, targeted);
                }
                else if ((targeted is Mobile) && !((Mobile) targeted).CheckTarget(from, this, targeted))
                {
                    this.OnTargetUntargetable(from, targeted);
                }
                else if (from.Region.OnTarget(from, this, targeted))
                {
                    this.OnTarget(from, targeted);
                }
                this.OnTargetFinish(from);
            }
        }

        protected virtual void OnCantSeeTarget(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a20d);
        }

        protected virtual void OnNonlocalTarget(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a2df);
        }

        protected virtual void OnTarget(Mobile from, object targeted)
        {
        }

        protected virtual void OnTargetCancel(Mobile from, TargetCancelType cancelType)
        {
        }

        protected virtual void OnTargetDeleted(Mobile from, object targeted)
        {
        }

        protected virtual void OnTargetFinish(Mobile from)
        {
        }

        protected virtual void OnTargetInSecureTrade(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a2df);
        }

        protected virtual void OnTargetNotAccessible(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a2df);
        }

        protected virtual void OnTargetOutOfLOS(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a20d);
        }

        protected virtual void OnTargetOutOfRange(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a2de);
        }

        protected virtual void OnTargetUntargetable(Mobile from, object targeted)
        {
            from.SendLocalizedMessage(0x7a2df);
        }

        public void Timeout(Mobile from)
        {
            this.CancelTimeout();
            from.ClearTarget();
            Cancel(from);
            this.OnTargetCancel(from, TargetCancelType.Timeout);
            this.OnTargetFinish(from);
        }

        public bool AllowGround
        {
            get
            {
                return this.m_AllowGround;
            }
            set
            {
                this.m_AllowGround = value;
            }
        }

        public bool AllowNonlocal
        {
            get
            {
                return this.m_AllowNonlocal;
            }
            set
            {
                this.m_AllowNonlocal = value;
            }
        }

        public bool CheckLOS
        {
            get
            {
                return this.m_CheckLOS;
            }
            set
            {
                this.m_CheckLOS = value;
            }
        }

        public bool DisallowMultis
        {
            get
            {
                return this.m_DisallowMultis;
            }
            set
            {
                this.m_DisallowMultis = value;
            }
        }

        public TargetFlags Flags
        {
            get
            {
                return this.m_Flags;
            }
            set
            {
                this.m_Flags = value;
            }
        }

        public int Range
        {
            get
            {
                return this.m_Range;
            }
            set
            {
                this.m_Range = value;
            }
        }

        public int TargetID
        {
            get
            {
                return this.m_TargetID;
            }
        }

        public DateTime TimeoutTime
        {
            get
            {
                return this.m_TimeoutTime;
            }
        }

        private class TimeoutTimer : Timer
        {
            private Mobile m_Mobile;
            private Target m_Target;
            private static TimeSpan OneSecond = TimeSpan.FromSeconds(1.0);
            private static TimeSpan TenSeconds = TimeSpan.FromSeconds(10.0);
            private static TimeSpan ThirtySeconds = TimeSpan.FromSeconds(30.0);

            public TimeoutTimer(Target target, Mobile m, TimeSpan delay) : base(delay)
            {
                this.m_Target = target;
                this.m_Mobile = m;
                if (delay >= ThirtySeconds)
                {
                    base.Priority = TimerPriority.FiveSeconds;
                }
                else if (delay >= TenSeconds)
                {
                    base.Priority = TimerPriority.OneSecond;
                }
                else if (delay >= OneSecond)
                {
                    base.Priority = TimerPriority.TwoFiftyMS;
                }
                else
                {
                    base.Priority = TimerPriority.TwentyFiveMS;
                }
            }

            protected override void OnTick()
            {
                if (this.m_Mobile.Target == this.m_Target)
                {
                    this.m_Target.Timeout(this.m_Mobile);
                }
            }
        }
    }
}

