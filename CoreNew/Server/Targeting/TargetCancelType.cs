﻿namespace Server.Targeting
{
    using System;

    public enum TargetCancelType
    {
        Overriden,
        Canceled,
        Disconnected,
        Timeout
    }
}

