﻿namespace Server.Targeting
{
    using System;

    public enum TargetFlags : byte
    {
        Beneficial = 2,
        Harmful = 1,
        None = 0
    }
}

