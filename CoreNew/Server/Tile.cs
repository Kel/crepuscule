﻿namespace Server
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct Tile
    {
        internal short m_ID;
        internal sbyte m_Z;
        public int ID
        {
            get
            {
                return this.m_ID;
            }
        }
        public int Z
        {
            get
            {
                return this.m_Z;
            }
            set
            {
                this.m_Z = (sbyte) value;
            }
        }
        public int Height
        {
            get
            {
                if (this.m_ID < 0x4000)
                {
                    return 0;
                }
                return TileData.ItemTable[this.m_ID & 0x3fff].Height;
            }
        }
        public bool Ignored
        {
            get
            {
                return (((this.m_ID == 2) || (this.m_ID == 0x1db)) || ((this.m_ID >= 430) && (this.m_ID <= 0x1b5)));
            }
        }
        public Tile(short id, sbyte z)
        {
            this.m_ID = id;
            this.m_Z = z;
        }

        public void Set(short id, sbyte z)
        {
            this.m_ID = id;
            this.m_Z = z;
        }
    }
}

