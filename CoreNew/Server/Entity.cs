﻿namespace Server
{
    using System;

    public class Entity : IEntity, IPoint3D, IPoint2D
    {
        private Point3D m_Location;
        private Server.Map m_Map;
        private Server.Serial m_Serial;

        public Entity(Server.Serial serial, Point3D loc, Server.Map map)
        {
            this.m_Serial = serial;
            this.m_Location = loc;
            this.m_Map = map;
        }

        public Point3D Location
        {
            get
            {
                return this.m_Location;
            }
        }

        public Server.Map Map
        {
            get
            {
                return this.m_Map;
            }
        }

        public Server.Serial Serial
        {
            get
            {
                return this.m_Serial;
            }
        }

        public int X
        {
            get
            {
                return this.m_Location.X;
            }
        }

        public int Y
        {
            get
            {
                return this.m_Location.Y;
            }
        }

        public int Z
        {
            get
            {
                return this.m_Location.Z;
            }
        }
    }
}

