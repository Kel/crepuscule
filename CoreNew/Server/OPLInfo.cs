﻿namespace Server
{
    using Server.Network;
    using System;

    public class OPLInfo : Packet
    {
        public OPLInfo(ObjectPropertyList list) : base(0xbf)
        {
            base.EnsureCapacity(13);
            base.m_Stream.Write((short) 0x10);
            base.m_Stream.Write((int) list.Entity.Serial);
            base.m_Stream.Write(list.Hash);
        }
    }
}

