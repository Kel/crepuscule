﻿namespace Server.Guilds
{
    using System;

    public enum GuildType
    {
        None,
        Gitans,
        Soleil,
        Confrerie,
        Mercenaires,
        Commercants,
        Ombres,
        Cercle,
        Empire,
        Chene,
        Planaires,
        Assemblee,
        Concervatoire
    }
}

