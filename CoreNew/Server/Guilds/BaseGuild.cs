﻿using System;
using System.Collections;
using System.Collections.Generic;
using Server.Items;
using Server.Mobiles;

namespace Server.Guilds
{

    public abstract class BaseGuild 
    {
        private int m_Id;

        public BaseGuild(GuildType Id) //serialization ctor
        {
            m_Id = (int)Id;
            m_GuildList.Add((GuildType)m_Id, this);
        }

        [CommandProperty(AccessLevel.Counselor)]
        public int Id { get { return m_Id; } }


        public abstract void Deserialize(GenericReader reader);
        public abstract void Serialize(GenericWriter writer);

        public abstract bool Disbanded { get; set; }
        public abstract bool Secret { get; set; }
        public abstract bool Special { get; set; }
        public abstract string Abbreviation { get; set; }
        public abstract string Name { get; set; }
        public abstract GuildType Type { get; set; }
        public abstract void OnMobileDelete(Mobile mob);
        public abstract void OnMemberJoin(Mobile member);
        public abstract void OnMemberLeave(Mobile member);

        private static Dictionary<GuildType, BaseGuild> m_GuildList = new Dictionary<GuildType, BaseGuild>();

        public static Dictionary<GuildType, BaseGuild> List
        {
            get
            {
                return m_GuildList;
            }
        }

        public static BaseGuild Find(GuildType type)
        {
            if (type == GuildType.None)
                return null;

            BaseGuild g;
            m_GuildList.TryGetValue(type, out g);
            return g;
        }

        public static BaseGuild FindByName(string name)
        {
            foreach (BaseGuild g in m_GuildList.Values)
            {
                if (g.Name == name)
                    return g;
            }

            return null;
        }

        public static BaseGuild FindByAbbrev(string abbr)
        {
            foreach (BaseGuild g in m_GuildList.Values)
            {
                if (g.Abbreviation == abbr)
                    return g;
            }

            return null;
        }

        public static List<BaseGuild> Search(string find)
        {
            string[] words = find.ToLower().Split(' ');
            List<BaseGuild> results = new List<BaseGuild>();

            foreach (BaseGuild g in m_GuildList.Values)
            {
                bool match = true;
                string name = g.Name.ToLower();
                for (int i = 0; i < words.Length; i++)
                {
                    if (name.IndexOf(words[i]) == -1)
                    {
                        match = false;
                        break;
                    }
                }

                if (match)
                    results.Add(g);
            }

            return results;
        }

        public override string ToString()
        {
            return String.Format("0x{0:X} \"{1} [{2}]\"", m_Id, Name, Abbreviation);
        }
    }
}
