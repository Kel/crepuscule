﻿namespace Server
{
    using System;

    [PropertyObject]
    public class VirtueInfo
    {
        private int[] m_Values;

        public VirtueInfo()
        {
        }

        public VirtueInfo(GenericReader reader)
        {
            if (reader.ReadByte() == 0)
            {
                int num2 = reader.ReadByte();
                if (num2 != 0)
                {
                    this.m_Values = new int[8];
                    for (int i = 0; i < 8; i++)
                    {
                        if ((num2 & (((int) 1) << i)) != 0)
                        {
                            this.m_Values[i] = reader.ReadInt();
                        }
                    }
                }
            }
        }

        public int GetValue(int index)
        {
            if (this.m_Values == null)
            {
                return 0;
            }
            return this.m_Values[index];
        }

        public static void Serialize(GenericWriter writer, VirtueInfo info)
        {
            writer.Write((byte) 0);
            if (info.m_Values == null)
            {
                writer.Write((byte) 0);
            }
            else
            {
                int num = 0;
                for (int i = 0; i < 8; i++)
                {
                    if (info.m_Values[i] != 0)
                    {
                        num |= ((int) 1) << i;
                    }
                }
                writer.Write((byte) num);
                for (int j = 0; j < 8; j++)
                {
                    if (info.m_Values[j] != 0)
                    {
                        writer.Write(info.m_Values[j]);
                    }
                }
            }
        }

        public void SetValue(int index, int value)
        {
            if (this.m_Values == null)
            {
                this.m_Values = new int[8];
            }
            this.m_Values[index] = value;
        }

        public override string ToString()
        {
            return "...";
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Compassion
        {
            get
            {
                return this.GetValue(2);
            }
            set
            {
                this.SetValue(2, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Honesty
        {
            get
            {
                return this.GetValue(7);
            }
            set
            {
                this.SetValue(7, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Honor
        {
            get
            {
                return this.GetValue(5);
            }
            set
            {
                this.SetValue(5, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Humility
        {
            get
            {
                return this.GetValue(0);
            }
            set
            {
                this.SetValue(0, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Justice
        {
            get
            {
                return this.GetValue(6);
            }
            set
            {
                this.SetValue(6, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Sacrifice
        {
            get
            {
                return this.GetValue(1);
            }
            set
            {
                this.SetValue(1, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Spirituality
        {
            get
            {
                return this.GetValue(3);
            }
            set
            {
                this.SetValue(3, value);
            }
        }

        [CommandProperty(AccessLevel.Counselor, AccessLevel.GameMaster)]
        public int Valor
        {
            get
            {
                return this.GetValue(4);
            }
            set
            {
                this.SetValue(4, value);
            }
        }

        public int[] Values
        {
            get
            {
                return this.m_Values;
            }
        }
    }
}

